require('babel-polyfill');
require('../../shared/less/student.less');
require('extensions');

/*----------------------------------------------------------------------------*/
/*    These statments ensure that webpack bundles the generic code we have    */
/*    written that is common between login.js, student.js, and teacher.js.    */

let translator = require('translator'),
    request = require('Request'),
    Messaging = require('Messaging'),
    utils = require('Utils'),
    platform = require('Platform'),
    user = require('user'),
    room = require('room'),
    Input = require('Input'),
    CloseX = require('CloseX'),
    Dots = require('Dots'),
    popup = require('PopupController'),
    FooterView = require('FooterView'),
    Select = require('Select'),
    DownChevronSmall = require('DownChevronSmall'),
    InfoIcon = require('InfoIcon');

/*----------------------------------------------------------------------------*/

let Backbone = require('backbone');

user.setModeSilent('student');
user.getCookieData();

translator.loadClientTranslations(user.getLanguage(), function() {
    require.ensure(['student-router', 'FooterController'], function(require) {
        SOCRATIVE.checkTime.reset();
        SOCRATIVE.initTemplateLoader.stop(); // Hide the loading message (if it's visible)
        require('student-router');
        
        let footer = require('FooterController');
        footer.render();
        
        Backbone.history.start();
    });
});
