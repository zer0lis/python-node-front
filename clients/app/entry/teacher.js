require('babel-polyfill');
require('../../shared/less/teacher.less');
require('extensions');

/*----------------------------------------------------------------------------*/
/*    These statments ensure that webpack bundles the generic code we have    */
/*    written that is common between login.js, student.js, and teacher.js.    */

let translator = require('translator'),
    request = require('Request'),
    Messaging = require('Messaging'),
    utils = require('Utils'),
    platform = require('Platform'),
    user = require('user'),
    room = require('room'),
    Input = require('Input'),
    CloseX = require('CloseX'),
    Dots = require('Dots'),
    popup = require('PopupController'),
    FooterView = require('FooterView'),
    Select = require('Select'),
    DownChevronSmall = require('DownChevronSmall'),
    InfoIcon = require('InfoIcon');

/*----------------------------------------------------------------------------*/

let Backbone = require('backbone');

function joinSuccess() {
    translator.loadClientTranslations(user.getLanguage(), function() {
        require.ensure(['teacher-router', 'BannerController', 'HeaderController', 'FooterController', 'RenewalController'], function(require) {
            SOCRATIVE.checkTime.reset();
            SOCRATIVE.initTemplateLoader.stop(); // Hide the loading message (if it's visible)
            
            let router  = require('teacher-router'),
                banner  = require('BannerController'),
                header  = require('HeaderController'),
                renewal = require('RenewalController'),
                footer  = require('FooterController');
            
            header.initialize(router);
            banner.initialize(router);
            footer.render(router);
            renewal.initialize(router);

            if (user.wantsPro()) {
                user.setWantsPro(false);
                Backbone.history.start({silent: true});
                router.routeToProfileBilling();
            } else {
                Backbone.history.start();
            }
        });
    });
}

function joinError() {
    user.clearCookieData();
    
    // If the user was trying to import a shared quiz, store the SOC number so we can try the import after login.
    if (hashParts.length === 2 && hashParts[0] === '#import-quiz') {
        user.setSharedSocNumber(hashParts[1]);
    }
    
    if (window.location.pathname !== '/login/teacher/') {
        window.location.href = '/login/teacher/';
    }
}

user.setModeSilent('teacher');
user.getCookieData();

let joinOptions = {
    role: 'teacher',
    roomname: '',
    success: joinSuccess,
    error: joinError
};

// Determine whether this is an MC SSO login. The hash portion of an MC SSO URL looks like this: #teacher-login/2a6933530d954379bd79
let hashParts = window.location.hash.split('/');
if (hashParts.length === 2 && hashParts[0] === '#teacher-login') {
    user.setAuthTokenAndMode(hashParts[1], 'teacher');
    joinOptions.auth_token = hashParts[1];
}

room.join(joinOptions);
