'use strict';

let express = require('express'),
    path = require('path'),
    cookieParser = require('cookie-parser'),
    bodyParser = require('body-parser');

let server = express();
server.set('views', path.join(__dirname, '../templates'));
server.set('view engine', 'pug');
server.use(bodyParser.json());
server.use(bodyParser.urlencoded({extended: false}));
server.use(cookieParser());
server.use(express.static(path.join(__dirname, '../assets')));

let props = {
    BACKEND_HOST: 'http://c.socrative.com:8000',
    MARKETING_HOST: 'https://appstaging-marketing.socrative.com',
    PUBNUB_SUBSCRIBE_KEY: 'sub-c-bd4e8b32-c63a-11e3-b872-02ee2ddab7fe',
    STATIC_URL: '/',
    STRIPE_KEY: 'pk_test_61k3mqhhLBt3E9o1Ltdin58Y',
    TEACHER_SERVICE: 'http://teacher-local.socrative.com:4000',
    VERSION: 'files'
};

let router = express.Router();

router.get('/login/student', (request, response) => {
    response.render('student-login', props);
});

router.get('/login/teacher', (request, response) => {
    response.render('teacher-login', props);
});

router.get('/student', (request, response) => {
    response.render('student', props);
});

router.get('/teacher', (request, response) => {
    response.render('teacher', props);
});

server.use('/', router);

server.use((request, response, next) => {
    let error = new Error(`Not Found: ${request.method} ${request.originalUrl}`);
    error.status = 404;
    next(error);
});

server.use((error, request, response) => {
    response.locals.message = error.message;
    response.locals.error = error;
    response.status(error.status || 500);
    response.render('error');
});

module.exports = server;
