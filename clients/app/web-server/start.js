'use strict';

process.env.NODE_CONFIG_DIR = '../../../shared/config';

let server = require('./server'),
    logger = require('../../../shared/Logger'),
    http = require('http'),
    port = 9000;

server.set('port', port);

let httpServer = http.createServer(server);
httpServer.listen(port);
httpServer.on('listening', onListening);
httpServer.on('error', onError);

function onListening() {
    logger.debug(`Listening on ${httpServer.address().port}`);
}

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }
    
    switch (error.code) {
        case 'EACCES':
            logger.error(`Port ${port} requires elevated privileges`);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            logger.error(`Port ${port} is already in use`);
            process.exit(1);
            break;
        default:
            throw error;
    }
}
