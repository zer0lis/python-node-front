import React from 'react';
import AlertIcon from 'AlertIcon';
import CloseX from 'CloseX';
import LoginLogo from 'LoginLogo';
import Input from 'Input';
import LanguageMenu from 'LanguageMenu';
import FooterView from 'FooterView';
import {translate} from 'translator';

let e = React.createElement;

export default class StudentLoginView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            children = [];
        
        if (this.props.showBanner) {
            children.push(
                e('div', {
                    className: 'login-error-banner',
                    children: [
                        e(AlertIcon, {
                            className: 'login-banner-icon',
                            color: '#ffffff'
                        }),
                        e('span', {
                            className: 'login-banner-text',
                            children: [
                                e('strong', {
                                    children: translate('This URL is no longer valid. ')
                                }),
                                translate('Please enter a new classroom name (or URL) to continue.')
                            ]
                        }),
                        e('div', {
                            className: 'login-banner-error-close',
                            children: [
                                e(CloseX, {
                                    className: 'login-banner-error-close-x'
                                })
                            ],
                            onClick: c.closeBannerClicked
                        })
                    ]
                })
            );
        }
        
        children.push(
            e(LoginLogo),
            e('div', {
                id: 'center-content',
                className: 'vertical-align-middle',
                children: [
                    e('div', {
                        className: 'outer-join-room-container',
                        children: [
                            e('div', {
                                id: 'join-room-container',
                                className: 'single-display',
                                children: [
                                    e('div', {
                                        className: 'student-login-title',
                                        children: [
                                            e('h1', {
                                                children: m.translations.studentLogin
                                            })
                                        ]
                                    }),
                                    e('div', {
                                        id: 'student-login',
                                        className: 'align-center form',
                                        children: [
                                            e(Input, {
                                                autoFocus: true,
                                                id: 'studentRoomName',
                                                label: m.translations.roomName,
                                                maxLength: 32,
                                                onChange: (event) => c.roomNameChanged(event.target.value),
                                                onKeyPress: (event) => {
                                                    if (event.key === 'Enter') {
                                                        c.joinClicked();
                                                    }
                                                },
                                                status: m.roomNameStatus,
                                                tooltipLocation: 'above',
                                                type: 'text',
                                                value: m.roomName
                                            })
                                        ]
                                    }),
                                    e('div', {
                                        className: 'submit-button-container',
                                        children: [
                                            e('button', {
                                                id: 'studentLoginButton',
                                                className: 'button button-primary button-large action-button',
                                                children: m.translations.join,
                                                onClick: c.joinClicked
                                            })
                                        ]
                                    })
                                ]
                            }),
                            e(LanguageMenu, this.props)
                        ]
                    })
                ]
            })
        );
        
        return e('div', {
            id: 'page-container',
            children: [
                e('div', {
                    id: 'main-content',
                    children: children
                }),
                e('footer', {
                    id: 'footer',
                    className: 'vertical-align',
                    children: e(FooterView)
                })
            ]
        });
    }
    
}
