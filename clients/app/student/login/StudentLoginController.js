import React from 'react';
import ReactDOM from 'react-dom';
import StudentLoginModel from 'StudentLoginModel';
import user from 'user';
import room from 'room';
import view from 'StudentLoginView';
import dots from 'Dots';
import Cookies from 'js-cookie';
import translator from 'translator';

let router = null,
    model = null;

class StudentLoginController {
    
    doStudentLogin(studentLoginRouter) {
        router = studentLoginRouter;
        model = new StudentLoginModel();
        user.logout();
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model,
                    languageCode: Cookies.get('socrative_lang'),
                    showBanner: router.invalid_url
                }
            ),
            document.body // TODO .... When we migrate to ES6, render to a <div> in the body instead of directly to the body (React discourages rendering directly to the body).
        );
    }
    
    closeBannerClicked() {
        router.invalid_url = false;
        controller.renderView();
    }
    
    roomNameChanged(roomName) {
        model.roomName = roomName;
        model.roomNameStatus.error = false;
        model.roomNameStatus.message = '';
        controller.renderView();
    }
    
    joinClicked() {
        if (model.roomName.length === 0) {
            model.roomNameStatus.error = true;
            controller.renderView();
        } else if (!/^[a-zA-Z0-9]+$/.test(model.roomName)) {
            model.roomNameStatus.error = true;
            model.roomNameStatus.message = model.translations.invalidCharacters;
            controller.renderView();
        } else {
            dots.start('studentLoginButton');
            user.setMode('student');
            let roomName = model.roomName;
            room.join({
                role: 'student',
                roomname: roomName,
                studentLogin: true,
                success: () => {
                    user.setRoomName(roomName);
                    controller.joinResult(roomName);
                },
                error: () => {
                    controller.joinResult();
                }
            });
        }
    }
    
    joinResult(roomName) {
        if (roomName) {
            if (room.hasRoster()) {
                translator.loadClientTranslations(user.getLanguage(), function() {
                    router.routeToStudentID(roomName);
                });
            } else {
                router.routeToStudentDashboard();
            }
        } else {
            dots.stop();
            model.roomNameStatus.error = true;
            model.roomNameStatus.message = model.translations.invalidRoom;
            controller.renderView();
        }
    }
    
    languageChanged(newLanguageCode) {
        Cookies.set('socrative_lang', newLanguageCode, {path: '/'});
        model.roomNameStatus.error = false;
        model.roomNameStatus.message = '';
        controller.renderView();
    }
    
}

let controller = new StudentLoginController();
module.exports = controller;
