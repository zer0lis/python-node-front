let Cookies = require('js-cookie');

class StudentLoginModel {
    
    constructor() {
        this.roomName = ''; // The room name entered by the student.
        this.roomNameStatus = {error: false, message: ''}; // The status of the room name input.
    }
    
    get translations() {
        let languageCode = Cookies.get('socrative_lang');
        
        if (languageCode === 'es-mx') {
            languageCode = 'es';
        } else if (languageCode === 'fr-ca') {
            languageCode = 'fr';
        }
        
        let translations = {
            en: {
                studentLogin: 'Student Login',
                roomName: 'Room Name',
                join: 'JOIN',
                invalidRoom: 'This room name is not valid.',
                invalidCharacters: 'Only letters (A-Z) and numbers (0-9) are allowed in a room name.'
            },
            da: {
                studentLogin: 'Studerende Log På',
                roomName: 'Værelse Navn',
                join: 'TILSLUTTE',
                invalidRoom: 'Dette værelse er ikke gyldigt.',
                invalidCharacters: 'Kun bogstaver (A-Z) og tal (0-9) er tilladt i et rum navn.'
            },
            de: {
                studentLogin: 'Studenten Anmeldung',
                roomName: 'Raumname',
                join: 'Beitreten',
                invalidRoom: 'Dieses Zimmer Name ist nicht gültig.',
                invalidCharacters: 'Nur Buchstaben (A-Z) und Ziffern (0-9) in einem Raumnamen erlaubt.'
            },
            es: {
                studentLogin: 'Acceso de Estudiante',
                roomName: 'Nombre de la Habitación',
                join: 'UNIRSE',
                invalidRoom: 'Este nombre de la habitación no es válido.',
                invalidCharacters: 'Sólo letras (A-Z) y números (0-9) están permitidos en un nombre de la sala.'
            },
            fi: {
                studentLogin: 'Opiskelijan Kirjautuminen',
                roomName: 'Huoneen Nimi',
                join: 'LIITY',
                invalidRoom: 'Tämä huone nimi ei kelpaa.',
                invalidCharacters: 'Vain kirjaimet (A-Z) ja numeroita (0-9) ovat sallittuja huoneessa nimi.'
            },
            fr: {
                studentLogin: 'Connexion Étudiant',
                roomName: 'Nom de la Salle',
                join: 'JOINDRE',
                invalidRoom: 'Ce nom de la chambre est pas valide.',
                invalidCharacters: 'Seules les lettres (A-Z) et des chiffres (0-9) sont autorisés dans un nom de salle.'
            },
            ko: {
                studentLogin: '학생 로그인',
                roomName: '객실 이름',
                join: '어울리다',
                invalidRoom: '이 객실 이름이 유효하지 않습니다.',
                invalidCharacters: '만 문자 (A-Z)와 숫자 (0-9)는 방 이름을 사용할 수 있습니다.'
            },
            ms: {
                studentLogin: 'Login Pelajar',
                roomName: 'Nama Bilik',
                join: 'SERTAI',
                invalidRoom: 'Nama bilik ini tidak sah.',
                invalidCharacters: 'Hanya huruf (A-Z) dan nombor (0-9) yang dibenarkan dalam nama bilik.'
            },
            nl: {
                studentLogin: 'Studenten Inloggen',
                roomName: 'Kamer Naam',
                join: 'TOETREDEN',
                invalidRoom: 'Deze naam van de ruimte is niet geldig.',
                invalidCharacters: 'Alleen letters (A-Z) en cijfers (0-9) zijn toegelaten in de naam van de kamer.'
            },
            'pt-br': {
                studentLogin: 'Acesso de Estudante',
                roomName: 'Nome da Sala',
                join: 'JUNTAR',
                invalidRoom: 'Este nome do quarto não é válido.',
                invalidCharacters: 'Somente letras (A-Z) e números (0-9) são permitidos em um nome da sala.'
            },
            sv: {
                studentLogin: 'Elevinloggning',
                roomName: 'Rumsnamn',
                join: 'ANSLUT',
                invalidRoom: 'Detta rum namn är inte giltigt.',
                invalidCharacters: 'Endast bokstäver (A-Z) och siffror (0-9) är tillåtna i ett rum namn.'
            },
            th: {
                studentLogin: 'นักศึกษาเข้าสู่ระบบ',
                roomName: 'ชื่อห้อง',
                join: 'ร่วม',
                invalidRoom: 'ชื่อห้องนี้ไม่ถูกต้อง',
                invalidCharacters: 'ตัวอักษร (A-Z) และตัวเลข (0-9) ได้รับอนุญาตให้อยู่ในชื่อห้อง'
            },
            tr: {
                studentLogin: 'Öğrenci Girişi',
                roomName: 'Oda Ismi',
                join: 'KATILMAK',
                invalidRoom: 'Bu oda adı geçerli değil.',
                invalidCharacters: 'Sadece harfler (A-Z) ve rakamlar (0-9) bir oda adına izin verilir.'
            },
            'zh-cn': {
                studentLogin: '学生登录',
                roomName: '房间名称',
                join: '加入',
                invalidRoom: '这个房间名无效。',
                invalidCharacters: '只有字母（A-Z）和数字（0-9）在一个房间里的名字。'
            }
        };
        
        return translations[languageCode] || translations['en'];
    }
    
}

module.exports = StudentLoginModel;
