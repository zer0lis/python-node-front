import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'underscore';
import model from 'StudentHeaderModel';
import user from 'user';
import room from 'room';
import view from 'StudentHeaderView';
import popup from 'PopupController';
import platform from 'Platform';
import {translate} from 'translator';

class StudentHeaderController {
    
    render() {
        let menuText = user.getFirstName() || translate('Menu'),
            roomName = room.getRoomName() || '';
        
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model,
                    logoClass: 'student-header-logo',
                    roomName: roomName.toUpperCase(),
                    allowRoomsMenu: false,
                    userName: menuText,
                    userMenu: 'student',
                    handraiseStatus: user.getHandRaiseStatus(),
                    handraise: user.getStudentHandraise()
                }
            ),
            document.getElementById('student-header-container'),
            controller.afterRender
        );
    }
    
    afterRender() {
        if (model.mobileMenuVisible) {
            let mobileMenu = document.getElementById('mobile-student-header-menu');
            TweenLite.to(mobileMenu, 0.4375, {right: 0, ease: Quint.easeOut});
        }
    }
    
    usernameClicked() {
        if (!model.accountMenuVisible) {
            _.defer(controller.usernameClickedDeferred); // Defer so the document click listener finishes first.
        }
    }
    
    usernameClickedDeferred() {
        model.accountMenuVisible = true;
        controller.render();
        controller.addClickListener();
    }
    
    addClickListener() {
        if (platform.isIos) {
            document.body.style.cursor = 'pointer';
        }
        document.body.addEventListener('click', controller.documentClicked);
    }
    
    documentClicked(event) {
        _.defer(() => {
            controller.removeClickListener(event); // Defer so we don't prematurely close menus (i.e. so other click events can be handled first).
        });
    }
    
    removeClickListener(event) {
        if (event.target.id === 'handraise-overlay') {
            return;
        }
        
        if (platform.isIos) {
            document.body.style.cursor = 'default';
        }
        
        document.body.removeEventListener('click', controller.documentClicked);
        model.accountMenuVisible = false;
        controller.render();
    }
    
    refreshClicked() {
        popup.render({
            title: translate('Please Confirm'),
            message: translate('Are you sure you want to refresh?'),
            buttonText: translate('Yes'),
            cancelText: translate('No'),
            buttonClicked: () => {
                window.location.reload();
            }
        });
    }

    handraiseClicked() {
        model.handraiseClicked(!user.getStudentHandraise(), controller.showStudentMenu);
    }

    showStudentMenu() {
        model.accountMenuVisible = true;
        controller.render();
        controller.addClickListener();
    }
    
    logOutClicked() {
        popup.render({
            title: translate('Please Confirm'),
            message: translate('Are you sure you want to log out?'),
            buttonText: translate('Yes'),
            cancelText: translate('No'),
            buttonClicked: () => {
                user.logout(function() {
                    window.location.href = '/login/student/';
                });
            }
        });
    }
    
    mobileMenuClicked() {
        if (model.mobileMenuVisible) {
            controller.hideMobileMenus();
        } else {
            model.mobileMenuVisible = true;
            document.getElementById('student-header-container').style.position = 'fixed';
            document.getElementById('student-page-container').style.overflow = 'hidden';
            window.addEventListener('orientationchange', controller.orientationChanged);
            controller.render();
        }
    }
    
    /**
     * Hide the mobile header menu immediately, with no animation (see SOC-1093).
     */
    orientationChanged() {
        window.removeEventListener('orientationchange', controller.orientationChanged);
        model.mobileMenuVisible = false;
        document.getElementById('mobile-student-header-menu').style.right = '-100%';
        document.getElementById('student-header-container').style.position = '';
        document.getElementById('student-page-container').style.overflow = '';
        controller.render();
    }
    
    hideMobileMenus() {
        if (model.mobileMenuVisible) {
            model.mobileMenuVisible = false;
            let mobileMenu = document.getElementById('mobile-student-header-menu');
            TweenLite.to(mobileMenu, 0.4375, {right: '-100%', ease: Quint.easeOut, onComplete: () => {
                document.getElementById('student-header-container').style.position = '';
                document.getElementById('student-page-container').style.overflow = '';
                controller.render();
            }});
        }
    }
    
}

let controller = new StudentHeaderController();
module.exports = controller;
