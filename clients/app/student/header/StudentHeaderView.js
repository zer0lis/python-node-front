import React from 'react';
import _ from 'underscore';
import HeaderMenuButton from 'HeaderMenuButton';
import HeaderUserName from 'HeaderUserName';
import MobileStudentHeaderMenu from 'MobileStudentHeaderMenu';
import BaseHeaderView from 'BaseHeaderView';

let e = React.createElement;

export default class StudentHeaderView extends React.Component {
    
    render() {
        let m = this.props.model;
        
        let children = [
            e(HeaderMenuButton, this.props),
            e(HeaderUserName, this.props)
        ];
        
        if (m.mobileMenuVisible) {
            children.push(
                e(MobileStudentHeaderMenu, this.props)
            );
        }
        
        let props = {
            children: children
        };
        
        return e(BaseHeaderView, _.extend(props, this.props));
    }
    
}
