let request = require('Request'),
    room = require('room'),
    user = require('user');

class StudentHeaderModel {
    
    constructor() {
        this.accountMenuVisible = false; // Whether the account menu is currently visible.
        this.mobileMenuVisible = false; // Whether the mobile account menu is currently visible.
    }
    
    handraiseClicked(value, callback) {
        request.post({
            url: `${window.backend_host}/students/api/handraise/`,
            data: {
                active: value,
                id: user.get('id'),
                room_name: room.get('name')
            },
            success: () => {
                user.setStudentHandraise(value);
                
                if (callback) {
                    callback();
                }
            },
            error: (response) => {
                console.error(response);
            }
        });
    }
    
}

module.exports = new StudentHeaderModel();
