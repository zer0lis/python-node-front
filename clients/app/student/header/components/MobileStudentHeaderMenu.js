import React from 'react';
import TeacherIcon from 'TeacherIcon';
import HandraiseIconActive from 'HandraiseIconActive';
import HandraiseIconInactive from 'HandraiseIconInactive';
import {translate} from 'translator';

let e = React.createElement;

export default class MobileStudentHeaderMenu extends React.Component {
    
    render() {
        let c = this.props.controller;
        
        let hand = e(HandraiseIconInactive, {
            className: 'handraise handraise-off'
        });
        
        if (this.props.handraise) {
            hand = e(HandraiseIconActive, {
                className:  'handraise handraise-on'
            });
        }
        
        let handraiseHTML = '';
        
        if (this.props.handraiseStatus) {
            handraiseHTML = e('li', {
                className: 'mobile-student-header-item',
                children: [
                    e('span', {
                        children: [
                            this.props.handraise ? translate('Lower Hand') : translate('Raise Hand'),
                            hand
                        ]
                    })
                ],
                onClick: c.handraiseClicked
            });
        }
        
        return e('div', {
            id: 'mobile-student-header-menu',
            children: [
                e('ul', {
                    id: 'mobile-student-header-items',
                    children: [
                        e('li', {
                            className: 'mobile-student-header-item mobile-student-header-name-row',
                            children: [
                                e(TeacherIcon, {
                                    className: 'mobile-header-icon',
                                    color: '#ccc'
                                }),
                                e('span', {
                                    className: 'mobile-student-header-name-text',
                                    children: this.props.userName
                                })
                            ]
                        }),
                        e('li', {
                            className: 'mobile-student-header-item',
                            children: [
                                e('span', {
                                    children: translate('Refresh')
                                })
                            ],
                            onClick: c.refreshClicked
                        }),
                        handraiseHTML,
                        e('li', {
                            className: 'mobile-student-header-item',
                            children: [
                                e('span', {
                                    children: translate('Log Out')
                                })
                            ],
                            onClick: c.logOutClicked
                        })
                    ]
                })
            ]
        });
    }
    
}
