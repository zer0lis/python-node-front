import React from 'react';
import HandraiseIconActive from 'HandraiseIconActive';
import HandraiseIconInactive from 'HandraiseIconInactive';
import {translate} from 'translator';

let e = React.createElement;

export default class StudentHeaderMenu extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        let hand = e(HandraiseIconInactive, {
            className:  'handraise handraise-off'
        });
        
        if (this.props.handraise) {
            hand = e(HandraiseIconActive, {
                className:  'handraise handraise-on'
            });
        }

        let handraiseHTML = '';
        
        if (this.props.handraiseStatus) {
            handraiseHTML = e('li', {
                className: 'student-handraise',
                children: [
                    e('span', {
                        id:'handraise-overlay',
                    }),
                    this.props.handraise ? translate('LOWER HAND') : translate('RAISE HAND'),
                    hand
                ],
                onClick: c.handraiseClicked
            });
        }
        
        return e('div', {
            id: 'account-menu-container',
            children: [
                e('div', {
                    id: 'account-menu',
                    style: {display: m.accountMenuVisible ? 'block' : 'none'},
                    children: [
                        e('i', {
                            'data-icon': 'f'
                        }),
                        e('ul', {
                            children: [
                                e('li', {
                                    children: translate('REFRESH'),
                                    onClick: c.refreshClicked
                                }),
                                handraiseHTML,
                                e('li', {
                                    children: translate('LOG OUT'),
                                    onClick: c.logOutClicked
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
