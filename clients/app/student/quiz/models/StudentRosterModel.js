'use strict';

let room = require('room'),
    user = require('user'),
    translate = require('translator').translate,
    Constants = require('Constants'),
    request = require('Request');

class StudentRosterModel {
    
    constructor() {
        this.studentId = ''; // The id entered by the student.
        this.studentIdStatus = {error: false, message: ''}; // The status of the student id input.
    }
    
    /*------------------*/
    /*    Validation    */
    /*------------------*/
    
    isValidStudentId() {
        return this.studentId &&
               this.studentId.length > 0 &&
               this.studentId.length <= Constants.MAX_STUDENT_ID_LENGTH;
    }
    
    /*----------------------*/
    /*    Client Request    */
    /*----------------------*/
    
    submitStudentId(callback, force) {
        request.post({
            url: `${window.backend_host}/students/api/login/`,
            data: {
                student_id: this.studentId,
                room_name: room.getRoomName(),
                force: force
            },
            success: (data) => {
                user.set({
                    first_name: data.first_name,
                    last_name: data.last_name,
                    user_input_name: data.first_name + ' ' + data.last_name,
                    id: data.id, // Used to update the handraise in the student list.
                    user_uuid: data.old_uuid, // The user_uuid changes after login to the roster.
                    team: data.team_num
                });
                user.setStudentHandraise(data.handraise);
                callback(true);
            },
            error: (response) => {
                if (response.error && response.error.code === Constants.STUDENT_ALREADY_LOGGED_IN) {
                    callback(false, response.error.first_name + ' ' + response.error.last_name);
                } else {
                    this.studentIdStatus.error = true;
                    this.studentIdStatus.message = translate('Please check your student ID and try again.');
                    callback(false);
                }
            }
        });
    }
    
}

module.exports = StudentRosterModel;
