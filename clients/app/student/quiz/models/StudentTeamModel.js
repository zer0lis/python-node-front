import teams from 'space-race-teams';

class StudentTeamModel {
    
    constructor() {
        this.selectedTeam = -1; // The team currently selected by the student.
        this.startAttempted = false; // Whether the student has attempted to start the activity.
        this.teams = teams; // Collection of space race team objects.
    }
    
    isValidSelectedTeam() {
        return this.selectedTeam >= 0;
    }
    
}

module.exports = StudentTeamModel;
