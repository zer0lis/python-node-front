var Backbone  = require('backbone'),
    _         = require('underscore'),
    user      = require('user'),
    room      = require('room'),
    activity  = require('activity'),
    request   = require('Request'),
    Response  = require('student-response');

var RootStudentModel = Backbone.Model.extend({
    
    initialize: function() {
        this.set('activityId', -1);
    },
    
    /**
     * This method exists solely to support the '/#join-room/:roomName' route.
     * @param roomName The name of the room to join.
     * @param callback The callback to execute when the join attempt is done.
     */
    joinRoom: function(roomName, callback) {
        user.setMode('student');
        room.join({
            role: 'student',
            roomname: roomName,
            success: function() {
                user.setRoomName(roomName.toLowerCase());
                callback(true);
            },
            error: function() {
                callback(false);
            }
        });
    },
    
    /**
     * method used to check if the student has all the details for a rostered room
     * a stud should have: stud ID, first name and last name
     * @return {boolean}
     */
    studentHasId: function() {
        return user.has("studentId") && user.has("first_name") && user.has("last_name")
    },

    initStudentNavIndex: function() {
        var index = this.getFirstUnansweredQuestionIndex();
        if (index >= activity.get('quiz').questions.length) {
            index = activity.get('quiz').questions.length - 1;
        }
        activity.set({studentNavIndex: index}, {silent: true});
    },
    
    studentLoggedIn: function() {
        return user.getCookieData()
            && user.has('room');
    },
    
    isNewActivity: function() {
        return activity.has('id') && this.get('activityId') != activity.get('id');
    },

    setReady: function() {
        this.set('ready', true);
    },
    
    isReady: function() {
        return this.get('ready');
    },
    
    reset: function() {
        if (this.get('activityId') != -1) {
            // Only unset the name, team, sent responses, and studentClickedFinish if the student didn't refresh.
            // And only unset the student name if the room is not rostered.
            if (!room.hasRoster()) {
                user.unset('user_input_name');
            }
            this.unset('rosterName');
            user.unset('team');
            activity.unset('sentResponses');
            activity.unset('studentClickedFinish');
        }
        this.set('activityId', activity.get('id'));
    },
    
    isActivityRunning: function() {
        return activity.has('activity_type');
    },
    
    setRandomName: function() {
        user.setRandomName();
        activity.setStudentName(user.get('user_input_name'));
    },

    hasRosterName: function() {
        return this.has('rosterName');
    },
    
    setRosterName: function(value) {
        this.set('rosterName', value);
    },
    
    getRosterName: function() {
        return this.get('rosterName');
    },
    
    /**
     * Returns true if the user has set a name during the current activity, false otherwise.
     * @returns {Boolean}
     */
    haveStudentInputName: function() {
        return user.has('user_input_name');
    },
    
    shouldPickTeam: function() {
        return activity.has('settings')
            && activity.get('settings').team_assignment_type !== 'auto';
    },
    
    haveStudentTeam: function() {
        return user.has('team') && user.get('team') >= 0;
    },
    
    getAutoAssignedTeam: function(callback) {
        request.get({
            url: `${window.backend_host}/students/api/auto-assign-team/${activity.get('id')}/?rn=${room.get('name')}`,
            success: function(data) {
                user.set('team', data.team_num);
                callback();
            }
        });
    },
    
    hasRandomQuestions: function() {
        return activity.has('settings') 
            && activity.get('settings').random_questions;
    },
    
    shuffleQuestions: function() {
        activity.get('quiz').questions = _.shuffle(activity.get('quiz').questions);
        _.each(activity.get('quiz').questions, function(question, index) {
            question.order = index + 1;
        });
    },
    
    sortQuestions: function() {
        activity.get('quiz').questions = _.sortBy(activity.get('quiz').questions, function(question) {
            return question.order;
        });
    },
    
    hasRandomAnswers: function() {
        return activity.has('settings')
            && activity.get('settings').random_answers;
    },
    
    shuffleAnswers: function(question) {
        if (question.type === 'TF') {
            this.sortAnswers(question); // T/F answers should always just be sorted by order.
            return;
        }
        question.answers = _.shuffle(question.answers);
    },
    
    sortAnswers: function(question) {
        question.answers = _.sortBy(question.answers, function(answer) {
            return answer.order;
        });
    },
    
    setQuestionResponse: function(question, response) {
        question.student_response = new Response(response);
        question.student_response.set('question_id', question.question_id);
        question.student_response.set('activity_instance_id', activity.get('id'));
    },
    
    allowRepeatResponses: function() {
        return activity.has('settings') &&
               activity.get('settings').allow_repeat_responses &&
              !activity.isVote();
    },

    /**
     * Return true if the student has answered the current teacher-paced question, false otherwise.
     * @returns {boolean}
     */
    answeredTeacherPaced: function() {
        var currentQuestion = this.getCurrentQuestion();
        return currentQuestion.hasOwnProperty('student_response') &&
               currentQuestion.student_response.hasAnswer() &&
               !currentQuestion.student_response.changedAttributes().selection_answers &&
               !currentQuestion.student_response.changedAttributes().text_answers;
    },
    
    isStudentNav: function() {
        return activity.has('settings') &&
               activity.get('settings').allow_student_nav;
    },
    
    getResponsesObject: function() {
        var responses = {};
        _.each(activity.get('responses'), function(response){
            responses[response.question] = response;
        });
        return responses;
    },
    
    getQuestions: function() {
        return activity.get('quiz').questions;
    },
    
    getFirstUnansweredQuestionIndex: function() {
        var index = 0;
        var questions = activity.get('quiz').questions;
        for (var i = 0; i < questions.length; i++) {
            var question = questions[i];
            var response = question.student_response;
            var changed  = response && response.changedAttributes();
            if (response &&
                response.hasAnswer() &&
                !changed.selection_answers && // If the response selection or text has changed, don't consider it answered.
                !changed.text_answers) {
                index++;
            } else {
                break;
            }
        }
        return index;
    },
    
    getCurrentQuestionIndex: function() {
        var index = -1;
        
        if (activity.isTeacherPaced()) {
            index = activity.get('activity_state');
        } else if (this.isStudentNav()) {
            index = activity.get('studentNavIndex');
        } else {
            index = this.getFirstUnansweredQuestionIndex();
        }
        
        if (index >= activity.get('quiz').questions.length) {
            index = activity.get('quiz').questions.length - 1;
        } else if (index < 0) {
            index = 0;
        }
        
        return index;
    },
    
    getCurrentQuestion: function() {
        return activity.get('quiz').questions[this.getCurrentQuestionIndex()];
    },
    
    setResourceUrl: function(question) {
        if (question.resources.length > 0) {
            var ww = window.innerWidth;
            var resourcesUrls = {};
            if ((question.resources[0].data != null) && (question.resources[0].data.length != 0)) {
                resourcesUrls = JSON.parse(question.resources[0].data);
            }
            if (!_.isEmpty(resourcesUrls)) {
                if (ww <= 480) {
                    question.resources[0].url = resourcesUrls.S.url;
                } else if (480 < ww && ww <= 800) {
                    question.resources[0].url = resourcesUrls.M.url;
                } else if (800 < ww && ww <= 1200) {
                    question.resources[0].url = resourcesUrls.L.url;
                } else if (ww > 1200) {
                    question.resources[0].url = resourcesUrls.XL.url;
                }
            }
        }
    },
    
    isStudentFinished: function() {
        return activity.get('studentClickedFinish') || (activity.get('finished') && !activity.isQuickQuestion());
    },
    
    allQuestionsAnswered: function() {
        if (!this.isActivityRunning()) {
            return false;
        }
        
        var numAnswered = 0;
        var numQuestions = this.getQuestions().length;
        var questions = this.getQuestions();
        for (var i = 0; i < numQuestions; i++) {
            var question = questions[i];
            var response = question.student_response;
            var changed  = response && response.changedAttributes();
            if (response
                && response.hasAnswer()
                && !changed.selection_answers // If the response selection or text has changed, don't consider it answered.
                && !changed.text_answers) {
                numAnswered++;
            }
        }
        return numAnswered == numQuestions;
    }
    
});

module.exports = RootStudentModel; // Export the class so others can extend/inherit.
