var _                = require('underscore'),
    RootStudentModel = require('root-student-model'),
    user             = require('user'),
    activity         = require('activity'),
    teams            = require('space-race-teams'),
    translate = require('translator').translate,
    request          = require('Request');

var StudentQuizModel = RootStudentModel.extend({
    
    setVisibleQuestion: function() {
        this.set('visibleQuestion', this.getCurrentQuestion());
    },
    
    getVisibleQuestion: function() {
        return this.get('visibleQuestion');
    },
    
    clearTextAnswers: function() {
        var question = this.getVisibleQuestion();
        if (question.hasOwnProperty('student_response')) {
            question.student_response.unset('text_answers');
        }
    },
    
    hasImage: function() {
        var question = this.getVisibleQuestion();
        return question.resources.length > 0 &&
               question.resources[0].type === 'IM' &&
               question.resources[0].hasOwnProperty('url') &&
               question.resources[0].url.length > 0;
    },
    
    getImageUrl: function() {
        var question = this.getVisibleQuestion();
        return question.resources[0].url;
    },
    
    atAnswerLimit: function() {
        var question = this.getVisibleQuestion();
        var selectedAnswers = question.student_response.get('selection_answers');
        return question.total_correct_answers != 0 && question.total_correct_answers == selectedAnswers.length;
    },
    
    isTrueFalseAnswered: function() {
        var question = this.getVisibleQuestion();
        var selectedAnswers = question.student_response.get('selection_answers');
        return question.type === 'TF' && selectedAnswers.length == 1;
    },
    
    getSpaceRaceClass: function() {
        var headerClass = '';
        if (this.haveStudentTeam()) {
            headerClass = teams[user.get('team')].cssStyle;
        }
        return headerClass;
    },
    
    getSpaceRaceTeam: function() {
        var team = '';
        if (this.haveStudentTeam()) {
            team = teams[user.get('team')].name;
        }
        return team;
    },
    
    getQuestionNumber: function() {
        if (this.isStudentNav()) {
            return activity.get('studentNavIndex') + 1;
        } else {
            return this.getVisibleQuestion().order;
        }
    },
    
    getQuizProgress: function() {
        return this.getQuestionNumber() + ' ' + translate('of') + ' ' + this.getQuestions().length;
    },
    
    toggleResponse: function(id) {
        var question = this.getVisibleQuestion();
        var selectedAnswers = question.student_response.get('selection_answers');
        
        if (!selectedAnswers) {
            question.student_response.set('selection_answers', [{answer_id: id}]);
            return;
        }
        
        for (var i = 0; i < selectedAnswers.length; i++) {
            if (selectedAnswers[i].answer_id == id) {
                selectedAnswers.splice(i, 1);
                return;
            }
        }
        
        if (this.atAnswerLimit() || this.isTrueFalseAnswered()) {
            selectedAnswers[selectedAnswers.length - 1] = {answer_id: id};
            return;
        }
        
        selectedAnswers.push({answer_id: id});
    },
    
    setResponseText: function(text) {
        var question = this.getVisibleQuestion();
        var textAnswers = question.student_response.get('text_answers');
        var escapedText = _.escape(text.trim());
        if (!textAnswers) {
            question.student_response.set('text_answers', [{answer_text: escapedText}]);
        } else {
            textAnswers[0].answer_text = escapedText;
        }
    },
    
    isResponseValid: function() {
        var question = this.getVisibleQuestion();
        return question.student_response && question.student_response.hasAnswer();
    },
    
    isResponseChanged: function() {
        if (!activity.get('sentResponses')) {
            return true;
        }
        
        var question = this.getVisibleQuestion();
        var currentResponse = question.student_response;
        var sentResponse = activity.get('sentResponses')[question.question_id];
        if (!sentResponse) {
            return true;
        }
        
        if (question.type !== 'FR' && currentResponse.get('selection_answers')) {
            if (currentResponse.get('selection_answers').length != sentResponse.selection_answers.length) {
                return true;
            }
            for (var i = 0; i < currentResponse.get('selection_answers').length; i++) {
                var currentAnswerId = currentResponse.get('selection_answers')[i].answer_id;
                if (sentResponse.selection_answers.indexOf(currentAnswerId) == -1) {
                    return true;
                }
            }
            return false;
        } else {
            if (currentResponse.get('text_answers').length != sentResponse.text_answers.length) {
                return true;
            }
            for (var j = 0; j < currentResponse.get('text_answers').length; j++) {
                var currentAnswer = currentResponse.get('text_answers')[j].answer_text;
                if (sentResponse.text_answers.indexOf(currentAnswer) == -1) {
                    return true;
                }
            }
            return false;
        }
    },
    
    getTeam: function() {
        return user.get('team');
    },
    
    isFirstQuestion: function() {
        var question = this.getVisibleQuestion();
        return question.order == 1;
    },
    
    isLastQuestion: function() {
        return this.getVisibleQuestion().order == this.getQuestions().length;
    },
    
    getNavProps: function() {
        var navProps = [];
        for (var i = 0; i < this.getQuestions().length; i++) {
            var question = this.getQuestions()[i];
            navProps.push({
                answered: question.answered,
                order: question.order,
                selected: question.selected,
                type:  question.type
            });
        }
        return navProps;
    },
    
    setAllQuestionStates: function() {
        for (var i = 0; i < this.getQuestions().length; i++) {
            var question = this.getQuestions()[i];
            var allSentResponses = activity.get('sentResponses');
            var responseSent = allSentResponses && allSentResponses[question.question_id];
            question.selected = question === this.getVisibleQuestion();
            question.answered = question.student_response.hasAnswer() || responseSent;
        }
    },

    /**
     * If the response for the pending question was cleared after being sent to
     * the server, restore the response to the state it was in when it was sent.
     */
    preparePendingQuestion: function() {
        var question = activity.get('quiz').questions[this.get('pendingNavIndex')];
        if (question) {
            var response = question.student_response;
            if (response && !response.hasAnswer()) {
                var allSentResponses = activity.get('sentResponses');
                if (allSentResponses && allSentResponses[question.question_id]) {
                    var sentResponse = allSentResponses[question.question_id];
                    var i;
                    if (response.has('selection_answers')
                        && sentResponse
                        && sentResponse.selection_answers
                        && sentResponse.selection_answers.length > 0) {
                        for (i = 0; i < sentResponse.selection_answers.length; i++) {
                            response.get('selection_answers').push({answer_id: sentResponse.selection_answers[i]});
                        }
                    } else if (response.has('text_answers')
                                && sentResponse
                                && sentResponse.text_answers) {
                        for (i = 0; i < sentResponse.text_answers.length; i++) {
                            response.get('text_answers')[0] = {answer_text: sentResponse.text_answers[i]};
                        }
                    }
                }
            }
        }
    },
    
    sendResponse: function(callback) {
        if (this.get('sendingResponse')) {
            return;
        } else {
            this.set('sendingResponse', true);
        }
        
        var question = this.getVisibleQuestion();
        var response = question.student_response;
        
        this.setTeam(response);
        this.setMultiResponse(response);
        this.setCheckActivity(response);
        
        if (question.type === 'FR') {
            this.resetSelectionAnswers(response);
            if (this.allowRepeatResponses()) {
                this.unsetResponseId(response);
            }
        } else {
            this.resetTextAnswers(response);
        }
        
        this.set('lastSentQuestion', question);
        
        var model = this;
        response.save(null, {
            success: function() {
                model.unset('sendingResponse');
                callback();
            },
            error: function() {
                model.unset('sendingResponse');
            }
        });
    },
    
    setTeam: function(response) {
        if (activity.isSpaceRace()) {
            response.set('team', this.getTeam());
        }
    },
    
    setMultiResponse: function(response) {
        response.set('multi_response', this.allowRepeatResponses());
    },
    
    setCheckActivity: function(response) {
        if (!this.isStudentNav() && !activity.isSpaceRace() && this.isLastQuestion()) {
            response.set('check_activity', true);
        }
    },
    
    resetSelectionAnswers: function(response) {
        response.set('selection_answers', []);
    },
    
    unsetResponseId: function(response) {
        response.unset('id');
        response.id = null;
    },
    
    resetTextAnswers: function(response) {
        response.set('text_answers', []);
    },
    
    storeSentResponse: function() {
        if (!activity.has('sentResponses')) {
            activity.set('sentResponses', {});
        }
        var question = this.get('lastSentQuestion');
        var response = question.student_response;
        var sentResponse = {selection_answers: [], text_answers: []};
        if (response.has('selection_answers')) {
            for (var i = 0; i < response.get('selection_answers').length; i++) {
                sentResponse.selection_answers.push(response.get('selection_answers')[i].answer_id);
            }
        }
        if (response.has('text_answers')) {
            for (var j = 0; j < response.get('text_answers').length; j++) {
                sentResponse.text_answers.push(response.get('text_answers')[j].answer_text);
            }
        }
        activity.get('sentResponses')[question.question_id] = sentResponse;
    },
    
    getSentResponses: function() {
        return activity.get('sentResponses');
    },
    
    setPendingNavIndex: function(index) {
        this.set('pendingNavIndex', index);
    },

    updateStudentNavIndex: function() {
        activity.set({studentNavIndex: this.get('pendingNavIndex')}, {silent: true});
    },
    
    shouldShowFeedback: function() {
        return activity.has('settings') &&
               activity.get('settings').show_feedback;
    },
    
    shouldShowResults: function() {
        return activity.has('settings') &&
               activity.get('settings').show_results;
    },
    
    getUnansweredQuestions: function(callback) {
        request.get({
            url: `${window.backend_host}/students/api/student-activity-status/${activity.get('id')}/`,
            success: function(data) {
                callback(data);
            },
            error: function(response) {
                console.error('Error getting unanswered questions:', response);
            }
        });
    },
    
    getUnansweredMessage: function(unanswered) {
        var questions = _.filter(this.getQuestions(), function(question) { 
            return unanswered.indexOf(question.question_id) != -1;
        });

        var message = translate('Wait! These questions are unanswered: ');

        var unansweredQuestions = "";
        for (var i = 0; i < questions.length; i++) {
            unansweredQuestions += questions[i].order + ', ';
        }

        unansweredQuestions = unansweredQuestions.slice(0, -2); // Remove the last comma.

        message += unansweredQuestions;
        return message;
    },
    
    finishActivity: function(callback) {
        activity.set({studentClickedFinish: true}, {silent: true});
        
        request.post({
            url: `${window.backend_host}/students/api/finish-activity/`,
            data: {
                activity_instance_id: activity.get('id')
            },
            success: function() {
                callback();
            },
            error: function(response) {
                console.error('Error finishing activity:', response);
            }
        });
    },
    
    getResults: function(callback) {
        request.get({
            url: `${window.backend_host}/students/api/student-score/${activity.get('id')}/`,
            success: function(data) {
                callback(data);
            },
            error: function(response) {
                console.error('Error fetching student results:', response);
            }
        });
    }
    
});

module.exports = new StudentQuizModel();
