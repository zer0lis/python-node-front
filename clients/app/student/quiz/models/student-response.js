var Backbone        = require('backbone'),
    _               = require('underscore'),
    offlineStorage 	= require('offline-storage');

var StudentResponse = Backbone.Model.extend({
    
    urlRoot: window.backend_host + "/students/api/responses/",
    
    defaults: {
        multi_response: false
    },
    
    save: function(attributes, options){
        if( _.isObject(attributes) ) {
            this.set(attributes);
        }

        var selectedIds = _.pluck(this.get("selection_answers"), "answer_id");
        this.set("answer_ids", selectedIds.join(","));
        
        if( this.has("text_answers") && this.get("text_answers").length > 0 ) {
            this.set("answer_text", this.get("text_answers")[0].answer_text);
        }

        //var that = this;
        if( this.has("id") ) {
            this.set({id: null}, {silent: true});
            // this.urlRoot += this.get("id");
        }

        return offlineStorage.save(
            this, 
            attributes, 
            {
                success: function(data, request){
                    
                    if( _.isObject(options) && _.isFunction(options.success) ) {
                        options.success(data, request);
                    }
                }
            }
        );
    },
    
    hasAnswer: function() {
        var hasText = this.has('text_answers') && 
                      this.get('text_answers').length > 0 && 
                      !_.isUndefined(this.get('text_answers')[0].answer_text) &&
                      this.get('text_answers')[0].answer_text.length > 0 &&
                      !/^\s+$/.test(this.get('text_answers')[0].answer_text);
            
        var hasSelections = this.has("selection_answers") && this.get("selection_answers").length > 0;
        return hasText || hasSelections;
    }
    
});

module.exports = StudentResponse;
