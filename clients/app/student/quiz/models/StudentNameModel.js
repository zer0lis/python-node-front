class StudentNameModel {
    
    constructor() {
        this.error = false; // Whether the name input is displaying an error state.
        this.name = ''; // The name entered by the student.
        this.settingName = false; // Whether the student is setting a name (a network request is in progress).
    }
    
    isValidName() {
        return this.name && this.name.length > 0;
    }
    
}

module.exports = StudentNameModel;
