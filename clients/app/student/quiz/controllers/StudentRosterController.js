import React from 'react';
import ReactDOM from 'react-dom';
import room from 'room';
import user from 'user';
import StudentRosterModel from 'StudentRosterModel';
import view from 'StudentRosterView';
import popup from 'PopupController';
import dots from 'Dots';
import {translate} from 'translator';

let router = null,
    model = null;

class StudentRosterController {
    
    doJoinRoom(studentLoginRouter, roomName) {
        router = studentLoginRouter;
        model = new StudentRosterModel();
        
        if (user.getRoomName()) {
            controller.joinRoomResult(true);
        } else {
            user.setMode('student');
            room.join({
                role: 'student',
                roomname: roomName,
                studentLogin: true,
                success: () => {
                    user.setRoomName(roomName.toLowerCase());
                    controller.joinRoomResult(true);
                },
                error: () => {
                    controller.joinRoomResult();
                }
            });
        }
    }
    
    joinRoomResult(success) {
        if (success) {
            if (!room.hasRoster()) {
                router.routeToStudentDashboard();
                return;
            }
            
            controller.renderView();
        } else {
            popup.render({
                title: translate('Invalid Classroom'),
                message: translate('Please check the classroom name and try again.'),
                buttonClicked: () => {
                    router.routeToIndex();
                },
                cancelClicked: () => {
                    router.routeToIndex();
                }
            });
        }
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model,
                    roomName: room.getRoomName()
                }
            ),
            document.getElementById('body')
        );
    }
    
    changeRoomClicked() {
        user.logout(() => {
            router.routeToIndex();
        });
    }
    
    studentIdChanged(studentId) {
        model.studentId = studentId;
        model.studentIdStatus.error = false;
        controller.renderView();
    }
    
    submitClicked() {
        if (model.isValidStudentId()) {
            dots.start('studentLoginButton');
            model.submitStudentId(controller.submitResult, false);
        } else {
            model.studentIdStatus.error = true;
            controller.renderView();
        }
    }
    
    submitResult(success, name) {
        if (success) {
            user.set('studentId', model.studentId);
            router.routeToStudentDashboard();
        } else {
            if (name) {
                popup.render({
                    title: translate('Login Verification'),
                    message: translate('Are you {0}?').format(name),
                    buttonText: translate('Yes'),
                    cancelText: translate('No'),
                    buttonClicked: () => {
                        model.submitStudentId(controller.submitForceResult, true);
                    },
                    cancelClicked: () => {
                        controller.renderView();
                    }
                });
            } else {
                dots.stop();
                controller.renderView();
            }
        }
    }
    
    submitForceResult(success) {
        if (success) {
            user.set('studentId', model.studentId);
            router.routeToStudentDashboard();
        } else {
            dots.stop();
            controller.renderView();
        }
    }
    
}

let controller = new StudentRosterController();
module.exports = controller;
