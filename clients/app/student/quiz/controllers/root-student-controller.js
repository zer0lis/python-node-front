/*
 * This is a special controller designed to listen for activity changes on behalf
 * of all other student controllers. This controller renders no view. Instead, it
 * redirects to appropriate routes in response to activity changes.
 */

var router           = null, // To avoid a direct circular dependecy, we don't require('student-router').
    RootStudentModel = require('root-student-model'),
    model            = new RootStudentModel(),
    Constants        = require('Constants'),
    messaging        = require('Messaging'),
    activity         = require('activity'),
    room             = require('room'),
    user             = require('user'),
    header           = require('StudentHeaderController'),
    popup            = require('PopupController'),
    ReactDOM         = require('react-dom'),
    utils            = require('Utils'),
    translate        = require('translator').translate;

function RootStudentController() {}

RootStudentController.prototype = {
    
    doRootStudent: function(studentRouter) {
        router = studentRouter;
        if (!controller.isReady()) {
            controller.initialize();
            return;
        }
        controller.doNextRoute();
    },
    
    doJoinRoom: function(studentRouter, roomName) {
        router = studentRouter;
        model.joinRoom(roomName, controller.joinRoomResult);
    },
    
    joinRoomResult: function(success) {
        if (success) {
            controller.initialize();
        } else {
            popup.render({
                title: translate('Invalid Classoom'),
                message: translate('Please check the classroom name and try again.'),
                buttonClicked: function() {
                    router.routeToStudentLogin();
                },
                cancelClicked: function() {
                    router.routeToStudentLogin();
                }
            });
        }
    },
    
    isReady: function() {
        return model.isReady();
    },
    
    initialize: function() {
        user.setModeSilent('student');
        
        if (!model.studentLoggedIn()) {
            router.routeToStudentLogin();
            return;
        }
        
        // TODO .... Add a proper check for rostered rooms and student IDs here.
        //           It will be necessary to set something on the user model after joining a 
        //           rostered room (e.g. user.setInRosteredRoom(true)) because room.hasRoster() 
        //           is undefined at this point (it was defined on the /login/ path, but you're 
        //           redirected to the /student/ path after successful login).
        
        room.setRoomName(user.getRoomName());
        
        if (!model.get('listening')) {
            model.listenTo(activity, 'change', controller.activityChanged);
            
            if (model.studentHasId()) {
                messaging.addListener({
                    message: Constants.HAND_RAISE,
                    callback: controller.handRaiseChanged
                });
            }
            
            messaging.addListener({
                message: Constants.LOG_STUDENT_OUT,
                callback: controller.studentLogoutConfirmed
            });
            
            messaging.subscribe({
                channel: `${room.getRoomName().toLowerCase()}-student`,
                messages: true
            });
            
            model.set('listening', true);
        }
        
        activity.refresh(controller.activityCheckDone);
        header.render();
    },
    
    activityChanged: function() {
        controller.closeModal('feedback-shim');
        controller.closeModal('results-shim');
        
        if (model.isNewActivity()) {
            model.reset();
            activity.loadQuizAndResponses(controller.prepareQuizAndResponses);
        } else if (controller.isReady()) {
            controller.doNextRoute();
        }
    },
    
    closeModal: function(id) {
        let element = document.getElementById(id);
        
        if (element) {
            ReactDOM.unmountComponentAtNode(element);
            element.parentNode.removeChild(element);
            utils.unblurPage();
        }
    },
    
    handRaiseChanged: function(data) {
        try {
            data = JSON.parse(data);
        } catch(e) {
            console.warn(e);
        }
        
        if (data.state === 'enabled' || data.state === 'disabled') {
            user.setHandraiseStatus(data.state)
        }
        
        if (data.state === 'on' || data.state === 'off') {
            if (user.get('id') === data.id) {
                if (data.state === 'on') {
                    user.setStudentHandraise(true);
                } else {
                    user.setStudentHandraise(false);
                }
            }
        }
        
        header.render();
    },
    
    activityCheckDone: function() {
        if (!model.isActivityRunning()) {
            model.setReady();
            controller.doNextRoute();
        }
    },
    
    prepareQuizAndResponses: function() {
        if (model.hasRandomQuestions()) {
            model.shuffleQuestions();
        } else {
            model.sortQuestions();
        }
        
        var questions = model.getQuestions();
        var responses = model.getResponsesObject();
        for (var i = 0; i < questions.length; i++) {
            var question = questions[i];
            var response = responses[question.question_id];
            model.setQuestionResponse(question, response);
            if (model.hasRandomAnswers()) {
                model.shuffleAnswers(question);
            } else {
                model.sortAnswers(question);
            }
            model.setResourceUrl(question);
        }
        
        model.setReady();
        controller.doNextRoute();
    },
    
    doneResponding: function() {
        return model.isStudentFinished() || (model.allQuestionsAnswered() && !(model.allowRepeatResponses() || model.isStudentNav()));
    },
    
    shouldWaitForTeacherPaced: function() {
        if (activity.isSpaceRace() || model.allowRepeatResponses() || !activity.isTeacherPaced()) {
            return false;
        }
        return model.answeredTeacherPaced();
    },
    
    shouldDoWait: function() {
        return !model.isActivityRunning() || controller.doneResponding() || controller.shouldWaitForTeacherPaced();
    },
    
    shouldDoName: function() {
        return !activity.isVote() && activity.needsStudentName() && !(model.hasRosterName() || model.haveStudentInputName());
    },
    
    shouldDoTeam: function() {
        return !controller.shouldDoName() && activity.isSpaceRace() && model.shouldPickTeam() && !model.haveStudentTeam();
    },
    
    shouldDoQuiz: function() {
        return !controller.shouldDoWait() && !controller.shouldDoName() && !controller.shouldDoTeam();
    },
    
    showFooter: function() {
        var footer = document.getElementById('footer');
        if (footer) {
            footer.style.visibility = 'visible';
        }
    },
    
    doNextRoute: function() {
        if (controller.shouldDoWait()) {
            controller.showFooter();
            router.routeToWait();
            return;
        }
        
        if (!activity.isVote()) {
            if (activity.needsStudentName()) {
                if (user.hasRosterName()) {
                    if (!model.hasRosterName()) {
                        model.setRosterName(user.getRosterName());
                        activity.setStudentName(user.getRosterName(), controller.doNextRoute);
                        return;
                    }
                } else if (!model.haveStudentInputName()) {
                    controller.showFooter();
                    router.routeToName();
                    return;
                }
            } else if (!model.haveStudentInputName()) {
                model.setRandomName();
            }
        }
        
        if (activity.isSpaceRace()) {
            if (model.shouldPickTeam() && !model.haveStudentTeam()) {
                controller.showFooter();
                router.routeToTeam();
                return;
            } else {
                if (!model.haveStudentTeam()) {
                    model.getAutoAssignedTeam(controller.doNextRoute);
                    return;
                }
            }
        }
        
        if (model.isStudentNav()) {
            model.initStudentNavIndex();
        }
        
        router.routeToQuiz();
    },
    
    confirmLogout: function() {
        popup.render({
            title: translate('Please Confirm'),
            message: translate('Are you sure you want to sign out?'),
            buttonText: translate('Yes'),
            cancelText: translate('No'),
            buttonClicked: function() {
                controller.studentLogoutConfirmed();
            }
        });
    },
    
    studentLogoutConfirmed: function(message) {
        if( message === "room_cleared" ) {
            user.logout(router.routeToStudentLogin);
        } else {
            
            try {
                message = JSON.parse(message);
            } catch(e) {
                console.warn(e);
            }

            if( message.kick && message.kick == user.getUserUuid() ) {
                user.logout(router.routeToStudentLogin);
            }
        }
    }
    
};

var controller = new RootStudentController();
module.exports = controller;
