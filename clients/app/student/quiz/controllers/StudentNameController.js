import rootStudentController from 'root-student-controller';
import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'underscore';
import user from 'user';
import activity from 'activity';
import StudentNameModel from 'StudentNameModel';
import view from 'StudentNameView';
import dots from 'Dots';
import {translate} from 'translator';

let model = null;

function StudentNameController() {}

StudentNameController.prototype = _.extend(Object.create(Object.getPrototypeOf(rootStudentController)), {
    
    doName: function() {
        if (!controller.shouldDoName()) {
            controller.doNextRoute();
        } else {
            model = new StudentNameModel();
            controller.renderView();
        }
    },
    
    renderView: function() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('main-content')
        );
    },
    
    nameChanged: function(name) {
        model.name = name;
        model.error = false;
        controller.renderView();
    },
    
    doneClicked: function() {
        if (model.settingName) {
            return;
        } else {
            model.settingName = true;
        }
        
        dots.start('submit-name-button');
        
        model.name = _.escape(model.name.trim());
        
        if (model.isValidName()) {
            user.set('user_input_name', model.name);

            activity.setStudentName(model.name, () => {
                controller.doNextRoute();
            });
        } else {
            model.error = true;
            dots.stop();
            model.settingName = false;
            controller.renderView();
        }
    }
    
});

StudentNameController.prototype.constructor = StudentNameController;
let controller = new StudentNameController();

module.exports = controller;
