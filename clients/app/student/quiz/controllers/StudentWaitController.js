import rootStudentController from 'root-student-controller';
import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'underscore';
import RootStudentModel from 'root-student-model';
import activity from 'activity';
import view from 'StudentWaitView';
import {translate} from 'translator';

let model = new RootStudentModel(); // No separate model is needed; just reuse the root model.

function StudentWaitController() {}

StudentWaitController.prototype = _.extend(Object.create(Object.getPrototypeOf(rootStudentController)), {
    
    doWait: function() {
        if (!controller.shouldDoWait()) {
            controller.doNextRoute();
        } else {
            controller.renderView();
        }
    },
    
    renderView: function() {
        let date = new Date();
        ReactDOM.render(
            React.createElement(
                view, {
                    allQuestionsAnswered: model.allQuestionsAnswered() || (!activity.isSpaceRace() && activity.isTeacherPaced()),
                    waitIcon:             controller.getWaitIcon(date),
                    waitText:             controller.getWaitText(date)
                }
            ),
            document.getElementById('main-content')
        );
    },
    
    getWaitIcon: function(date) {
        if (date.getMonth() === 9) {
            return 'pumpkin.png';
        } else if (date.getMonth() === 10) {
            return 'turkey-waiting@2x.gif';
        } else if (date.getMonth() === 11) {
            return 'snowman-waiting@2x.png';
        }
        return 'hex.png'; // The default icon if no other date range is matched above.
    },
    
    getWaitText: function(date) {
        if (date.getMonth() === 9) {
            return translate('Waiting for the next <i>{0}</i> activity to begin...').format(translate('spooky'));
        }
        return translate('Waiting for the next activity to begin...');
    }
    
});

StudentWaitController.prototype.constructor = StudentWaitController;
let controller = new StudentWaitController();

module.exports = controller;
