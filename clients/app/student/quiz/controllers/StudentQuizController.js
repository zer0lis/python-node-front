import rootStudentController from 'root-student-controller';
import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'underscore';
import model from 'StudentQuizModel';
import activity from 'activity';
import view from 'StudentQuizView';
import QuestionFeedbackView from 'QuestionFeedbackView';
import QuizResultsView from 'QuizResultsView';
import popup from 'PopupController';
import dots from 'Dots';
import platform from 'Platform';
import utils from 'Utils';
import {translate} from 'translator';

let e = React.createElement;

function StudentQuizController() {}

StudentQuizController.prototype = _.extend(Object.create(Object.getPrototypeOf(rootStudentController)), {
    
    doQuiz: function() {
        if (!controller.shouldDoQuiz()) {
            controller.doNextRoute();
        } else {
            model.setVisibleQuestion();
            if (model.allowRepeatResponses()) {
                model.clearTextAnswers();
            }
            controller.renderView();
        }
    },
    
    renderView: function() {
        let viewProps = {
            controller: controller,
            isSpaceRace: activity.isSpaceRace(),
            spaceRaceClass: model.getSpaceRaceClass(),
            spaceRaceTeam: model.getSpaceRaceTeam(),
            quizProgress: model.getQuizProgress(),
            showSubmit: !model.isStudentNav(),
            isResponseValid: model.isResponseValid(),
            studentNav: model.isStudentNav() && !activity.isSpaceRace(),
            currentQuestion: model.getQuestionNumber(),
            totalQuestions: model.getQuestions().length
        };
        
        let footer = document.getElementById('footer');
        
        if (model.isStudentNav()) {
            model.setAllQuestionStates();
            viewProps.navProps = model.getNavProps();
            if (platform.isPhone() && footer) {
                footer.style.visibility = 'hidden';
            }
        } else {
            if (platform.isPhone() && footer) {
                footer.style.visibility = 'visible';
            }
        }
        
        if (model.hasImage()) {
            viewProps.imageUrl = model.getImageUrl();
        }
        
        _.extend(viewProps, model.getVisibleQuestion());
        
        ReactDOM.render(
            e(view, viewProps),
            document.getElementById('main-content')
        );
    },
    
    answerClicked: function(id) {
        model.toggleResponse(id);
        controller.renderView();
    },

    answerTextChanged: function(text) {
        model.setResponseText(text);
    },

    submitAnswerClicked: function() {
        if (model.isResponseValid()) {
            dots.start('submit-button');
            model.sendResponse(controller.responseSent);
        }
    },
    
    nextClicked: function() {
        model.setPendingNavIndex(model.getVisibleQuestion().order);
        controller.handleStudentNav();
    },
    
    previousClicked: function() {
        model.setPendingNavIndex(model.getVisibleQuestion().order - 2);
        controller.handleStudentNav();
    },
    
    questionNumberClicked: function(number) {
        model.setPendingNavIndex(number - 1);
        controller.handleStudentNav();
    },
    
    handleStudentNav: function() {
        model.preparePendingQuestion();
        if (model.isResponseValid() && model.isResponseChanged()) {
            model.sendResponse(controller.studentNavResponseSent);
        } else {
            controller.studentNavResponseSent(true);
        }
    },
    
    studentNavResponseSent: function(skipStorage) {
        if (!skipStorage) {
            model.storeSentResponse();
        }
        model.updateStudentNavIndex();
        controller.doQuiz();
    },
    
    responseSent: function() {
        dots.stop();
        if (!model.allowRepeatResponses() && (model.isLastQuestion() || controller.shouldWaitForTeacherPaced())) {
            if (model.shouldShowFeedback()) {
                controller.renderFeedback(model.isLastQuestion() ? controller.checkShowResults : controller.doNextRoute);
            } else {
                if (model.isLastQuestion()) {
                     controller.checkShowResults();
                } else {
                    controller.doNextRoute();
                }
            }
        } else {
            if (model.shouldShowFeedback()) {
                controller.renderFeedback(controller.doQuiz);
            } else {
                controller.doQuiz();
            }
        }
    },
    
    renderFeedback: function(callback) {
        ReactDOM.unmountComponentAtNode(document.getElementById('main-content'));
        
        let question = model.getVisibleQuestion(),
            response = question.student_response;
        
        let feedbackProps = {
            isCorrect:    response.get('is_correct'),
            questionText: question.question_text,
            answers:      response.get('correct_answers'),
            explanation:  response.get('explanation'),
            okClicked:    function() {
                controller.closeModal('feedback-shim');
                callback();
            }
        };
        
        let feedBackShim = document.createElement('div');
        feedBackShim.id = 'feedback-shim';
        document.body.appendChild(feedBackShim);
        
        ReactDOM.render(
            e(QuestionFeedbackView, feedbackProps),
            document.getElementById('feedback-shim'),
            function() {
                utils.blurPage();
            }
        );
    },
    
    checkShowResults: function() {
        if (model.shouldShowResults()) {
            model.getResults(controller.renderResults);
        } else {
            controller.doNextRoute();
        }
    },
    
    renderResults: function(results) {
        ReactDOM.unmountComponentAtNode(document.getElementById('main-content'));
        
        let resultsShim = document.createElement('div');
        resultsShim.id = 'results-shim';
        document.body.appendChild(resultsShim);
        
        let divisor = results.total - results.neutral;
        let percent = divisor !== 0 ? (results.correct / divisor) : 0;
        percent = (percent * 100).toFixed(0) + '%';
        
        ReactDOM.render(
            e(QuizResultsView, {
                score: results.correct + '/' + divisor,
                percent: percent,
                neutral: results.neutral,
                okClicked: controller.closeResults
            }),
            resultsShim,
            function() {
                utils.blurPage();
            }
        );
    },
    
    closeResults: function() {
        controller.closeModal('results-shim');
        controller.doNextRoute();
    },
    
    finishQuizClicked: function() {
        dots.start('student-finish-quiz');
        if (model.isResponseValid() && model.isResponseChanged()) {
            model.sendResponse(controller.getUnansweredQuestions);
        } else {
            controller.getUnansweredQuestions();
        }
    },
    
    getUnansweredQuestions: function() {
        model.getUnansweredQuestions(controller.confirmFinishQuiz);
    },
    
    confirmFinishQuiz: function(unanswered) {
        popup.render({
            title: translate('Please Confirm'),
            message:    unanswered.length > 0 ? model.getUnansweredMessage(unanswered) : translate('Are you sure you want to finish the quiz?'),
            buttonText: unanswered.length > 0 ? translate('Finish')                      : translate('Yes'),
            cancelText: unanswered.length > 0 ? translate('Cancel')                      : translate('No'),
            buttonClicked: function() {
                model.finishActivity(controller.activityFinished);
            },
            cancelClicked: function() {
                dots.stop();
                if (unanswered.length > 0) {
                    model.initStudentNavIndex(); // Go to the highest unanswered question.
                    controller.doQuiz();
                }
            }
        });
    },
    
    activityFinished: function() {
        dots.stop();
        controller.checkShowResults();
    }
    
});

StudentQuizController.prototype.constructor = StudentQuizController;
let controller = new StudentQuizController();

module.exports = controller;
