import rootStudentController from 'root-student-controller';
import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'underscore';
import StudentTeamModel from 'StudentTeamModel';
import activity from 'activity';
import user from 'user';
import view from 'StudentTeamView';

let model = null;

function StudentTeamController() {}

StudentTeamController.prototype = _.extend(Object.create(Object.getPrototypeOf(rootStudentController)), {
    
    doTeam: function () {
        if (!controller.shouldDoTeam()) {
            controller.doNextRoute();
        } else {
            model = new StudentTeamModel();
            controller.renderView();
        }
    },
    
    renderView: function() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model,
                    teamCount: activity.get('settings').team_count
                }
            ),
            document.getElementById('main-content')
        );
    },
    
    teamChanged: function(team) {
        model.selectedTeam = team;
        controller.renderView();
    },
    
    startRacingClicked: function() {
        model.startAttempted = true;
        if (model.isValidSelectedTeam()) {
            user.set('team', model.selectedTeam); // The user model has a 'change' listener that updates the cookie.
            controller.doNextRoute();
        } else {
            controller.renderView();
        }
    }
    
});

let controller = new StudentTeamController();
module.exports = controller;
