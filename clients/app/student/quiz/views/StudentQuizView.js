import React from 'react';
import _ from 'underscore';
import ActivityImage from 'ActivityImage';
import StudentRocketIcon from 'StudentRocketIcon';
import QuizNav from 'QuizNav';
import platform from 'Platform';
import {translate} from 'translator';

let e = React.createElement,
    c = null;

export default class StudentQuizView extends React.Component {
    
    componentDidUpdate() {
        if (this.props.type === 'FR') {
            if (this.props.studentNav) {
                let response = this.props.student_response;
                
                if (response && response.has('text_answers') && response.get('text_answers').length > 0) {
                    this.refs.saTextArea.value = _.unescape(response.get('text_answers')[0].answer_text);
                    return;
                }
            }
            this.refs.saTextArea.value = '';
        }
    }
    
    createAnswerLetter(number, isTrueFalse) {
        if (isTrueFalse) {
            return number === 1 ? 'T' : 'F';
        }
        
        if (number > 26) {
            let baseChar = ('A').charCodeAt(0),
                letters = '';
            
            do {
                number -= 1;
                letters = String.fromCharCode(baseChar + (number % 26)) + letters;
                number = (number / 26) >> 0;
            } while (number > 0);
            
            return letters;
        }
        
        return String.fromCharCode('A'.charCodeAt(0) + number - 1);
    }
    
    createAnswerOption(answer, number, isTrueFalse) {
        let selectedClass = '',
            selectionAnswers = this.props.student_response ? this.props.student_response.get('selection_answers') : [];
        
        if (selectionAnswers && selectionAnswers.length) {
            for (let i = 0; i < selectionAnswers.length; i++) {
                if (selectionAnswers[i].answer_id === answer.id) {
                    selectedClass = ' selected';
                    break;
                }
            }
        }
        
        return e('div', {
            className: 'mc-answer-option' + selectedClass,
            children: [
                e('div', {
                    className: 'mc-answer-option-container',
                    children: [
                        e('span', {
                            className: 'answer-option-letter',
                            children: this.createAnswerLetter(number, isTrueFalse)
                        }),
                        e('div', {
                            className: 'mc-answer-option-text' + (answer.text.length > 50 ? ' wrap-text' : ''),
                            children: [
                                e('pre', {
                                    dangerouslySetInnerHTML: {__html: isTrueFalse ? translate(answer.text) : answer.text}
                                })
                            ]
                        })
                    ],
                    onClick: () => c.answerClicked(answer.id)
                })
            ]
        })
    }
    
    answerTextFocused() {
        if (platform.isPhone()) {
            let nav = document.getElementById('student-nav-container');
            
            if (nav) {
                nav.style.bottom = '-70px';
                nav.style.position = 'absolute';
            }
        }
    }
    
    answerTextBlurred() {
        if (platform.isPhone()) {
            let nav = document.getElementById('student-nav-container');
            
            if (nav) {
                nav.style.bottom = '0';
                nav.style.position = 'fixed';
            }
        }
    }
    
    render() {
        c = this.props.controller;
        
        let questionHeaderChildren = [];
        
        if (this.props.imageUrl) {
            questionHeaderChildren.push(
                e('div', {
                    className: 'student-quiz-image-container',
                    key: `student-quiz-image-container-${this.props.imageUrl}`,
                    children: [
                        e(ActivityImage, {
                            imageUrl: this.props.imageUrl
                        })
                    ]
                })
            );
        }
        
        if (this.props.question_text && this.props.question_text.length > 0) {
            questionHeaderChildren.push(
                e('div', {
                    className: 'question-explanation-container clearfix' + (!this.props.imageUrl ? ' no-image' : ''),
                    children: [
                        e('div', {
                            className: 'question-text',
                            children: [
                                e('pre', {
                                    dangerouslySetInnerHTML: {__html: this.props.question_text}
                                })
                            ]
                        })
                    ]
                })
            );
        }
        
        let questionAnswerContainerChildren = [];
        
        if (this.props.type === 'FR') {
            questionAnswerContainerChildren.push(
                e('textarea', {
                    className: 'fr-question-textarea',
                    autoCapitalize: 'off',
                    autoComplete: 'off',
                    autoCorrect: 'off',
                    maxLength: 65000,
                    placeholder: translate('Enter Answer Here'),
                    ref: 'saTextArea',
                    spellCheck: false,
                    onFocus: this.answerTextFocused,
                    onBlur: this.answerTextBlurred,
                    onChange: (event) => c.answerTextChanged(event.target.value)
                })
            );
        } else if (this.props.type === 'MC') {
            let mcAnswerAreaChildren = [];
            
            for (let j = 0; j < this.props.answers.length; j++) {
                mcAnswerAreaChildren.push(this.createAnswerOption(this.props.answers[j], j + 1));
            }
            
            questionAnswerContainerChildren.push(
                e('div', {
                    className: 'mc-answer-area',
                    children: mcAnswerAreaChildren
                })
            );
        } else if (this.props.type === 'TF') {
            questionAnswerContainerChildren.push(
                e('div', {
                    className: 'mc-answer-area',
                    children: [
                        this.createAnswerOption(this.props.answers[0], 1, true),
                        this.createAnswerOption(this.props.answers[1], 2, true)
                    ]
                })
            );
        }
        
        let navContainerChildren = [],
            navPaginationChildren = [];
        
        if (this.props.showSubmit) {
            navContainerChildren.push(
                e('button', {
                    id: 'submit-button',
                    className: 'button button-primary button-large',
                    children: translate('Submit Answer'),
                    disabled: !this.props.isResponseValid && this.props.type !== 'FR',
                    onClick: c.submitAnswerClicked
                })
            );
        } else {
            navPaginationChildren.push(
                e(QuizNav, {
                    currentQuestion: this.props.currentQuestion,
                    nextClicked: c.nextClicked,
                    questionClicked: c.questionNumberClicked,
                    navProps:        this.props.navProps,
                    previousClicked: c.previousClicked,
                    totalQuestions: this.props.totalQuestions
                })
            );
        }
        
        let progressChildren = [];
        
        if (this.props.isSpaceRace) {
            progressChildren.push(
                e('div', {
                    className: 'space-race-progress-container clearfix ' + this.props.spaceRaceClass,
                    children: [
                        e('div', {
                            className: 'space-race-progress',
                            children: [
                                e('span', {
                                    className: 'space-race-progress-text',
                                    children: this.props.quizProgress
                                }),
                                e('div', {
                                    className: 'space-race-team',
                                    children: [
                                        e(StudentRocketIcon, {
                                            className: 'student-rocket-icon'
                                        }),
                                        e('span', {
                                            className: 'space-race-team-text',
                                            children: translate('Team') + ' ' + this.props.spaceRaceTeam
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            );
        } else {
            let quizProgressLeftChildren = [
                e('span', {
                    className: 'quiz-progress-text',
                    children: this.props.quizProgress
                })
            ];
            
            if (this.props.studentNav) {
                let questionTypeText = translate('SHORT ANSWER');
                
                if (this.props.type === 'MC') {
                    questionTypeText = translate('MULTIPLE CHOICE');
                } else if (this.props.type === 'TF') {
                    questionTypeText = translate('TRUE / FALSE');
                }
                
                quizProgressLeftChildren.push(
                    e('div', {
                        className: 'question-type-container',
                        children: [
                            e('span', {
                                className: 'question-type-dot ' + this.props.type.toLowerCase() + '-question-type-dot'
                            }),
                            e('span', {
                                className: 'question-type-text ' + this.props.type.toLowerCase() + '-question-type-text',
                                children: questionTypeText
                            })
                        ]
                    })
                );
            }
            
            let quizProgressChildren = [
                e('div', {
                    className: 'quiz-progress-container',
                    children: quizProgressLeftChildren
                })
            ];
            
            if (this.props.studentNav) {
                quizProgressChildren.push(
                    e('button', {
                        className: 'button button-primary button-small pill student-nav-finish-button',
                        id: 'student-finish-quiz',
                        onClick: c.finishQuizClicked,
                        children: translate('FINISH QUIZ')
                    })
                );
            }
            
            progressChildren.push(
                e('div', {
                    className: 'progress-container',
                    children: [
                        e('div', {
                            className: 'quiz-progress clearfix',
                            children: quizProgressChildren
                        })
                    ]
                })
            );
        }
        
        return e('div', {
            children: [
                progressChildren,
                e('div', {
                    className: 'clearfix',
                    children: [
                        e('div', {
                            id: 'take-quiz-master-container',
                            className: 'take-quiz-master-container' + (this.props.studentNav ? ' take-quiz-master-container-nav' : ''),
                            children: [
                                e('div', {
                                    id: 'question-container',
                                    children: [
                                        e('div', {
                                            className: this.props.type.toLowerCase() + '-question',
                                            children: [
                                                e('div', {
                                                    className: 'question-header-container clearfix' + (!this.props.imageUrl ? ' no-image' : ''),
                                                    children: questionHeaderChildren
                                                }),
                                                e('div', {
                                                    className: 'question-answer-container',
                                                    children: questionAnswerContainerChildren
                                                }),
                                                e('div', {
                                                    className: 'nav-container',
                                                    children: navContainerChildren
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                }),
                e('div', {
                    className: 'nav-pagination',
                    children: navPaginationChildren
                })
            ]
        });
    }
    
}
