import React from 'react';
import _ from 'underscore';
import {translate} from 'translator';

let e = React.createElement;

export default class QuestionFeedbackView extends React.Component {
    
    render() {
        let containerClass = 'feedback-container-',
            headerChildren = [];
        
        if (this.props.isCorrect === null) {
            containerClass += 'nocorrect';
            headerChildren.push(
                e('div', {
                    className: 'no-correctness-title-bar',
                    children: [
                        e('h4', {
                            children: translate('Submitted')
                        })
                    ]
                })
            );
        } else if (this.props.isCorrect) {
            containerClass += 'correct';
            headerChildren.push(
                e('div', {
                    className: 'correct-title-bar',
                    children: [
                        e('h4', {
                            children: [
                                e('i', {
                                    'data-icon': 'd'
                                }),
                                ' ', // <= This whitespace is required beteen the green checkmark and the text.
                                translate('Correct!')
                            ]
                        })
                    ]
                })
            );
        } else {
            containerClass += 'incorrect';
            headerChildren.push(
                e('div', {
                    className: 'incorrect-title-bar',
                    children: [
                        e('h4', {
                            children: [
                                e('i', {
                                    'data-icon': 'h'
                                }),
                                ' ', // <= This whitespace is required beteen the red X and the text.
                                translate('Incorrect')
                            ]
                        })
                    ]
                })
            );
        }
        
        let bodyChildren = [
            e('span', {
                id: 'question-text',
                children: [
                    e('b', {
                        children: translate('Question:')
                    }),
                    e('pre', {
                        dangerouslySetInnerHTML: {__html: this.props.questionText}
                    })
                ]
            })
        ];
        
        let answers = this.props.answers;
        
        if (answers && answers.length > 0) {
            let answerTextChildren = [
                e('div', {
                    className: 'answer-header',
                    children: answers.length > 1 ? translate('Correct Answers:') : translate('Correct Answer:')
                })
            ];
            
            _.each(answers, function(answer) {
                answerTextChildren.push(
                    e('span', {
                        className: 'answer',
                        dangerouslySetInnerHTML: {__html: answer + ' '}
                    })
                );
            });
            
            bodyChildren.push(
                e('div', {
                    id: 'answer-text',
                    children: answerTextChildren
                })
            );
        }
        
        let explanation = this.props.explanation;
        
        if (explanation && explanation.length > 0) {
            bodyChildren.push(
                e('span', {
                    id: 'explanation-text',
                    children: [
                        e('b', {
                            children: translate('Explanation:')
                        }),
                        e('pre', {
                            dangerouslySetInnerHTML: {__html: explanation}
                        })
                    ]
                })
            );
        }
        
        return e('div', {
            id: 'feedback-popup',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'feedback-container',
                    className: 'modal-dialog ' + containerClass,
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: headerChildren
                        }),
                        e('div', {
                            className: 'modal-body',
                            children: bodyChildren
                        }),
                        e('div', {
                            className: 'modal-footer',
                            children: [
                                e('button', {
                                    id: 'submit-feedback-button',
                                    className: 'button button-large button-primary',
                                    children: translate('OK'),
                                    onClick: this.props.okClicked
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
