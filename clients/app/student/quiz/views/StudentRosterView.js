import React from 'react';
import InfoIcon from 'InfoIcon';
import Input from 'Input';
import LoginLogo from 'LoginLogo';
import FooterView from 'FooterView';
import {translate} from 'translator';

let e = React.createElement;

export default class StudentRosterView extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            loading: false
        };
    }
    
    componentWillReceiveProps() {
        this.setState({
            loading: false
        });
    }
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        return e('div', {
            id: 'page-container-student-id',
            children: [
                e('div', {
                    id: 'main-content-student-id',
                    children: [
                        e(LoginLogo),
                        e('div', {
                            id: 'center-content',
                            className: 'vertical-align-middle',
                            children: [
                                e('div', {
                                    id: 'student-login-container',
                                    className: 'form single-display',
                                    children: [
                                        e('div', {
                                            className: 'student-login-title',
                                            children: [
                                                e('h1', {
                                                    children: translate('Student Login')
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            children: [
                                                e('p', {
                                                    children: translate('Classroom Name')
                                                }),
                                                e('p', {
                                                    className: 'student-roster-room-name',
                                                    children: [
                                                        this.props.roomName,
                                                        e('a', {
                                                            className: 'cancel-button',
                                                            disabled: this.state.loading,
                                                            children: translate('Change Classroom'),
                                                            onClick: c.changeRoomClicked
                                                        })
                                                    ]
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            className: 'info-block',
                                            children: [
                                                e('div', {
                                                    className: 'info-icon',
                                                    children: e(InfoIcon)
                                                }),
                                                e('div', {
                                                    className: 'info-text',
                                                    children: e('span', {
                                                        children: translate('This classroom requires a student ID. Please enter your student ID to continue.')
                                                    })
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            id: 'student-login',
                                            className: 'align-center form',
                                            children: [
                                                e(Input, {
                                                    autoFocus: true,
                                                    id: 'studentRoomName',
                                                    label: translate('Student ID'),
                                                    maxLength: 32,
                                                    onChange: (event) => c.studentIdChanged(event.target.value),
                                                    onKeyPress: (event) => {
                                                        if (event.key === 'Enter' && !this.state.loading) {
                                                            this.setState({
                                                                loading: true
                                                            });
                                                            c.submitClicked();
                                                        }
                                                    },
                                                    status: m.studentIdStatus,
                                                    tooltipLocation: 'above',
                                                    value: m.studentId
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            children: [
                                                e('button', {
                                                    id: 'studentLoginButton',
                                                    className: 'button button-primary button-large action-button',
                                                    children: translate('Submit'),
                                                    onClick: () => {
                                                        this.setState({
                                                            loading: true
                                                        });
                                                        c.submitClicked();
                                                    }
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                }),
                e('footer', {
                    id: 'footer',
                    className: 'vertical-align',
                    children: e(FooterView)
                })
            ]
        });
        
    }
    
}
