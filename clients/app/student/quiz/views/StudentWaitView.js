import React from 'react';
import {translate} from 'translator';

let e = React.createElement;

export default class StudentWaitView extends React.Component {
    
    componentDidMount() {
        this.startAnimation();
    }
    
    componentWillUnmount() {
        this.stopAnimation();
    }
    
    startAnimation() {
        let id = {},
            cir = document.getElementById('cir'),
            hex = document.getElementById('hex'),
            colors = ['#ffa10a', '#91c15a', '#8db5d3', '#ff5a28'],
            colorIndex = 0,
            angle = 0;

        let start = () => {
            if (this.shouldAnimate(id)) {
                TweenLite.to(cir, 0.15, {scale: 0, transformOrigin: '50% 50%', onComplete: inflate});
            }
        };

        let inflate = () => {
            if (this.shouldAnimate(id)) {
                let color = colors[colorIndex++];
                if (colorIndex === colors.length) {
                    colorIndex = 0;
                }
                cir.style.backgroundColor = color;
                TweenLite.to(cir, 0.78, {delay: 0.0, scale: 1, ease: Cubic.easeOut, transformOrigin: '50% 50%', onComplete: deflate});
                TweenLite.to(hex, 0.68, {delay: 0.2, scale: 1.07, transformOrigin: '50% 50%'});
            }
        };

        let deflate = () => {
            if (this.shouldAnimate(id)) {
                TweenLite.to(cir, 0.78, {scale: 0, ease: Cubic.easeIn, transformOrigin: '50% 50%'});
                TweenLite.to(hex, 0.68, {delay: 0.2, scale: 1, transformOrigin: '50% 50%', onStart: rotateBack});
            }
        };

        let rotateBack = () => {
            if (this.shouldAnimate(id)) {
                TweenLite.to(hex, 0.15, {delay: 0.4, rotation: '-=5', transformOrigin: '50% 50%', onComplete: rotateForward});
            }
        };

        let rotateForward = () => {
            if (this.shouldAnimate(id)) {
                let rotation = '+=35';
                if (angle === 360) {
                    angle = 0;
                    rotation = 30;
                    TweenLite.to(hex, 0, {rotation: 0, transformOrigin: '50% 50%'}); // Quickly reset to zero.
                }
                TweenLite.to(hex, 0.15, {rotation: rotation, transformOrigin: '50% 50%', onComplete: start});
                angle += 30;
            }
        };

        start();
    }
    
    shouldAnimate() {
        return /#wait/.test(window.location.hash);
    }

    stopAnimation() {
        TweenLite.killTweensOf(document.getElementById('cir'));
        TweenLite.killTweensOf(document.getElementById('hex'));
    }
    
    render() {
        let waitingText = [];
        
        if (this.props.allQuestionsAnswered) {
            waitingText.push(
                e('p', {
                    id: 'already-responded-text',
                    children: [
                        translate("You've completed the current activity.")
                    ]
                })
            );
        }
        
        waitingText.push(
            e('p', {
                id: 'waiting-for-activity-text',
                dangerouslySetInnerHTML: {__html: this.props.waitText}
            })
        );
        
        return e('div', {
            id: 'waiting-for-teacher-container',
            children: [
                e('div', {
                    className: 'waiting-inside-container',
                    children: [
                        e('div', {
                            className: 'hex-loading-container',
                            children: [
                                e('div', {
                                    id: 'cir'
                                }),
                                e('img', {
                                    id: 'hex',
                                    src: window.static_url + 'img/' + this.props.waitIcon,
                                    width: 118,
                                    height: 103
                                })
                            ]
                        }),
                        e('div', {
                            className: 'waiting-text',
                            children: waitingText
                        })
                    ]
                })
            ]
        });
    }
    
}
