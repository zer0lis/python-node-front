import React from 'react';
import {translate} from 'translator';

let e = React.createElement;

export default class QuizResultsView extends React.Component {
    
    render() {
        let bodyChildren = [
            e('span', {
                children: [
                    e('b', {
                        children: translate('Score:')
                    }),
                    e('span', {
                        className: 'result-value',
                        children: this.props.score
                    })
                ]
            }),
            e('div', {
                id: 'percent-text',
                children: [
                    e('b', {
                        children: translate('Percent:')
                    }),
                    e('span', {
                        className: 'result-value',
                        children: this.props.percent
                    })
                ]
            })
        ];
        
        if (this.props.neutral) {
            bodyChildren.push(
                e('span', {
                    id: 'neutral-text',
                    children: [
                        e('b', {
                            children: translate('Non-gradable questions:')
                        }),
                        e('span', {
                            className: 'result-value',
                            children: this.props.neutral
                        })
                    ]
                })
            );
        }
        
        return e('div', {
            className: 'modal open',
            children: [
                e('div', {
                    id: 'results-container',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('div', {
                                    className: 'results-title-bar',
                                    children: [
                                        e('h3', {
                                            children: translate('Finished!')
                                        })
                                    ]
                                })
                            ]
                        }),
                        e('div', {
                            className: 'modal-body',
                            children: bodyChildren
                        }),
                        e('div', {
                            className: 'modal-footer',
                            children: [
                                e('button', {
                                    className: 'button button-large button-primary',
                                    children: translate('OK'),
                                    onClick: this.props.okClicked
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
