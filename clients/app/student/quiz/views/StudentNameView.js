import React from 'react';
import Input from 'Input';
import {translate} from 'translator';

let e = React.createElement,
    names = ['Pond, Amy', 'Tyler, Rose', 'Williams, Rory', 'Noble, Donna', 'Mott, Wilfred', 'Harkness, Jack', 'Smith, Mickey', 'Oswald, Clara'];

export default class StudentNameView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        return e('div', {
            className: 'take-quiz-container',
            children: [
                e('div', {
                    id: 'enter-name-container',
                    className: 'form vertical-align',
                    children: [
                        ' ', // <-- This whitespace is required to preserve the layout between this element and its parent.
                        e('div', {
                            className: 'enter-name vertical-align-middle',
                            children: [
                                e('h3', {
                                    children: translate('Enter your name')
                                }),
                                e(Input, {
                                    id: 'student-name-input',
                                    autoCorrect: 'off',
                                    autoFocus: true,
                                    maxLength: 32,
                                    placeholder: names[Math.floor(Math.random() * names.length)],
                                    status: {error: m.error},
                                    value: m.name,
                                    onChange: (event) => c.nameChanged(event.target.value),
                                    onKeyPress: (event) => {
                                        if (event.key === 'Enter') {
                                            c.doneClicked();
                                        }
                                    }
                                }),
                                e('button', {
                                    id: 'submit-name-button',
                                    className: 'button button-primary button-large',
                                    children: translate('Done'),
                                    onClick: c.doneClicked
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
