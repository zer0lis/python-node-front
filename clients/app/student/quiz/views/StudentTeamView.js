import React from 'react';
import Select from 'Select';
import {translate} from 'translator';

let e = React.createElement;

export default class StudentTeamView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            teamChildren = [];
        
        for (let i = 0; i < this.props.teamCount; i++) {
            let team = m.teams[i];
            teamChildren.push(
                {title: team.name, value: i, selected: i === m.selectedTeam}
            );
        }
        
        return e('div', {
            className: 'take-quiz-container',
            children: [
                e('div', {
                    id: 'select-team-container',
                    className: 'form',
                    children: [
                        e(Select, {
                            error: !m.isValidSelectedTeam() && m.startAttempted,
                            label: translate('Select Your Team'),
                            onSelect: (name, value) => c.teamChanged(value),
                            children: teamChildren
                        }),
                        e('div', {
                            className: 'start-racing-container',
                            children: [
                                e('button', {
                                    id: 'submit-team-button',
                                    className: 'action-button action-button-200',
                                    children: translate('Start Racing'),
                                    onClick: c.startRacingClicked
                                })
                            ]
                        })
                    ]
                })
            ]
        })
    }
    
}
