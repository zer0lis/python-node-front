/**
 * Socrative Student Quiz Navigation
 * 
 * This component provides the following features:
 *
 *     1. Text for the current question and total number of questions (e.g. 1 of 12).
 *     2. First and last buttons for jumping to the first/last questions of a quiz.
 *          - These buttons are hidden on mobile.
 *     3. Previous and next buttons for moving back and forth one question at a time.
 *     4. Up to 9 question number buttons for jumping directly to specific questions.
 *          - On mobile, only 5 question number buttons are visible
 * 
 * When a question jump occurs, the target question number is centered (if there are enough
 * questions before and after). Here are examples after jumping on desktop/mobile:
 * 
 * Desktop: Jump from question 1 to 9, when there are a total of 15 questions:
 * 
 *                  9 of 15
 *     [<<][<] 6 7 8 [9] 10 11 12 [>][>>]
 * 
 * Mobile: Jump from question 1 to 5, when there are a total of 15 questions:
 * 
 *           5 of 15
 *     [<] 3 4 [5] 6 7 [>]
 * 
 * Properties:
 * 
 * currentQuestion {number}   The question number currently being displayed
 * nextClicked     {function} Callback invoked when the next button is clicked
 * questionClicked {function} Callback invoked when any question number is clicked
 * navProps        {object}   An object with the type and state of each question
 * previousClicked {function} Callback invoked when the previous button is clicked
 * totalQuestions  {number}   The total number of questions to be navigated
 * 
 * This component is used by the student quiz view during an open navigation quiz.
 */

import React from 'react';
import FirstPageIcon from 'FirstPageIcon';
import PreviousArrowIcon from 'PreviousArrowIcon';
import NextArrowIcon from 'NextArrowIcon';
import LastPageIcon from 'LastPageIcon';
import platform from 'Platform';
import {translate} from 'translator';

let e = React.createElement;

export default class QuizNav extends React.Component {
    
    canClickPrevious() {
        return this.props.currentQuestion > 1;
    }
    
    canClickNext() {
        return this.props.currentQuestion < this.props.totalQuestions;
    }
    
    getQuestionsToDisplay() {
        let minCenteredQuestion = platform.isPhone() ? 3 : 5,
            maxQuestions = platform.isPhone() ? 5 : 9,
            questionPadding = platform.isPhone() ? 2 : 4,
            questionsToDisplay = [],
            i = 1;
        
        if (this.props.currentQuestion <= minCenteredQuestion || this.props.totalQuestions <= maxQuestions) {
            for (; i <= Math.min(maxQuestions, this.props.totalQuestions); i++) {
                questionsToDisplay.push(i);
            }
        } else {
            let questionCount = 0;
            
            for (i = this.props.currentQuestion + questionPadding; questionCount < maxQuestions; i--) {
                if (i <= this.props.totalQuestions) {
                    questionsToDisplay.push(i);
                    questionCount++;
                }
            }
            
            questionsToDisplay.reverse();
        }
        
        return questionsToDisplay;
    }
    
    render() {
        let buttonsToDisplay = [];
        
        if (!platform.isPhone()) {
            buttonsToDisplay.push(
                e('button', {
                    className: 'quiz-nav-button first-question-button' + (this.canClickPrevious() ? '' : ' disabled'),
                    children: [
                        e(FirstPageIcon, {
                            className: 'first-question-arrow'
                        })
                    ],
                    onClick: () => {
                        if (this.props.questionClicked && this.canClickPrevious()) {
                            this.props.questionClicked(1);
                        }
                    }
                })
            );
        }
        
        buttonsToDisplay.push(
            e('button', {
                className: 'quiz-nav-button previous-question-button' + (this.canClickPrevious() ? '' : ' disabled'),
                children: [
                    e(PreviousArrowIcon, {
                        className: 'previous-question-arrow'
                    })
                ],
                onClick: () => {
                    if (this.props.previousClicked && this.canClickPrevious()) {
                        this.props.previousClicked();
                    }
                }
            })
        );
        
        let questionNumbers = this.getQuestionsToDisplay();
        
        for (let i = 0; i < questionNumbers.length; i++) {
            let questionNumber = questionNumbers[i],
                type = this.props.navProps[questionNumber - 1].type.toLowerCase(),
                selected = this.props.navProps[questionNumber - 1].selected,
                answered = this.props.navProps[questionNumber - 1].answered;
            
            let buttonClass = 'quiz-nav-button question-number-button';
            buttonClass += answered ? (' ' + type + '-question-answered') : (' ' + type + '-question-unanswered');
            buttonClass += selected ? (' ' + type + '-question-selected') : (' ' + type + '-question-unselected');
            
            buttonsToDisplay.push(
                e('div', {
                    className: 'question-number-container' + (selected ? (' ' + type + '-container-selected') : ''),
                    key: 'question-number-container-' + questionNumber,
                    children: [
                        e('button', {
                            className: buttonClass,
                            children: questionNumber,
                            key: 'question-number-' + questionNumber,
                            onClick: ((questionNumber) => {
                                return () => {
                                    if (this.props.questionClicked && questionNumber !== this.props.currentQuestion) {
                                        this.props.questionClicked(questionNumber);
                                    }
                                }
                            })(questionNumber) // Capture the question number in the closure.
                        })
                    ]
                })
            );
        }
        
        buttonsToDisplay.push(
            e('button', {
                className: 'quiz-nav-button next-question-button' + (this.canClickNext() ? '' : ' disabled'),
                children: [
                    e(NextArrowIcon, {
                        className: 'next-question-arrow'
                    })
                ],
                onClick: () => {
                    if (this.props.nextClicked && this.canClickNext()) {
                        this.props.nextClicked();
                    }
                }
            })
        );
        
        if (!platform.isPhone()) {
            buttonsToDisplay.push(
                e('button', {
                    className: 'quiz-nav-button last-question-button' + (this.canClickNext() ? '' : ' disabled'),
                    children: [
                        e(LastPageIcon, {
                            className: 'last-question-arrow'
                        })
                    ],
                    onClick: () => {
                        if (this.props.questionClicked && this.canClickNext()) {
                            this.props.questionClicked(this.props.totalQuestions);
                        }
                    }
                })
            );
        }
        
        return e('div', {
            id: 'student-nav-container',
            className: 'student-nav-container' + (this.props.className ? (' ' + this.props.className) : ''),
            children: [
                e('div', {
                    className: 'student-nav-x-of-x-container',
                    children: [
                        e('span', {
                            className: 'question-range',
                            dangerouslySetInnerHTML: {__html: '<b>{0}</b> <i>{1}</i> <b>{2}</b>'.format(this.props.currentQuestion, translate('of'), this.props.totalQuestions)}
                        })
                    ]
                }),
                e('div', {
                    className: 'nav-buttons-container',
                    children: buttonsToDisplay
                })
            ]
        })
    }
    
}
