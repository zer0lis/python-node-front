import Backbone from 'backbone';
import studentLoginController from 'StudentLoginController';
import studentRosterController from 'StudentRosterController';

let StudentLoginRouter = Backbone.Router.extend({
    
    routes: {
        '': 'login',
        'join-room/:roomName': 'studentJoinRoom',
        'student-id/:roomName': 'studentJoinRoom',
        '*bad': 'bad' // This handles all invalid routes.
    },
    
    hashes: {
        'invalid_url': 'code=401' // when the share room url is no longer valid
    },

    
/*-----------------------------*/
/*    Route Handler Methods    */
/*-----------------------------*/
    
    login: function() {
        studentLoginController.doStudentLogin(router);
    },
    
    studentJoinRoom: function(roomName) {
        this.invalid_url = false;
        studentRosterController.doJoinRoom(this, roomName);
    },
    
    bad: function() {
        if (window.top.location.hash && window.top.location.hash.indexOf(this.hashes.invalid_url) > -1) {
            this.invalid_url = true;
        } 
        this.routeToIndex(); 
    },


/*--------------------------*/
/*    Navigation Methods    */
/*--------------------------*/
    
    routeToIndex: function() {
        this.navigate('', {trigger: true});
    },
    
    routeToStudentDashboard: function() {
        window.location.href = '/student/';
    },
    
    routeToStudentID: function(roomName) {
        this.navigate('#student-id/' + roomName, {trigger: true});
    }
    
});

let router = new StudentLoginRouter();
module.exports = router;
