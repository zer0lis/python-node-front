let Backbone = require('backbone'),
    launchController          = require('LaunchController'),
    quizzesController         = require('QuizzesController'),
    roomsController           = require('RoomsController'),
    reportsController         = require('ReportsController'),
    resultsTableController    = require('results-table-controller'),
    resultsQuestionController = require('results-question-controller'),
    spaceRaceController       = require('SpaceRaceController'),
    editQuizController = require('EditQuizController'),
    profileController = require('ProfileController'),
    demographicsController = require('DemographicsController'),
    accountController = require('AccountController');

import user from 'user';

let TeacherRouter = Backbone.Router.extend({
    
    routes: {
        '': 'teacher',
        'teacher-login/:authToken': 'mcSsoLogin',
        'launch': 'launch',
        'quizzes': 'quizzes',
        'import-quiz/:socNumber': 'importQuiz',
        'edit-quiz/:socNumber': 'editQuiz',
        'space-race': 'spaceRace',
        'profile/info': 'profileInfo',
        'profile/demo': 'profileDemo',
        'profile/billing': 'profileBilling',
        'live-results': 'liveResultsTable',
        'live-results/table': 'liveResultsTable',
        'live-results/question/:number': 'liveResultsQuestion',
        'final-results/:activityId/:quizId/table': 'finalResultsTable',
        'final-results/:activityId/:quizId/question/:number': 'finalResultsQuestion',
        'reports': 'reports',
        'rooms': 'rooms',
        '*bad': 'teacher' // This handles all invalid routes.
    },
    
    initialize: function() {
        this.lastRoute = null;
        this.listenTo(this, 'route', this.onRouteChange);
    },

    /**
     * When the route changes, remember the last route.
     * Note: This executes after the new route's callback method has been executed. However, if the callback
     *       loads a controller on demand, this will be executed before the asynchronous load finishes.
     * @param callbackName The name of the route callback function
     * @param callbackArgs The arguments, if any, to the route callback function
     */
    onRouteChange: function(callbackName, callbackArgs) {
        router.lastRoute = Backbone.history.fragment;
    },

    /**
     * Return true if the app is routing into the profile from outside the profile, false otherwise.
     * @returns {boolean}
     */
    isEnteringProfile: function() {
        return router.lastRoute === null || !/profile/.test(router.lastRoute);
    },
    
    
    /*-----------------------------*/
    /*    Route Handler Methods    */
    /*-----------------------------*/
    
    teacher: function() {
        router.routeToLaunchAndReplace();
    },

    mcSsoLogin: function() {
        // MC SSO login is handled in the teacher.js entry point.
        if (user.getSharedSocNumber()) {
            router.routeToImportQuiz(user.getSharedSocNumber());
        } else {
            router.routeToLaunchAndReplace();
        }
    },
    
    launch: function() {
        launchController.doLaunch(router);
    },
    
    quizzes: function() {
        quizzesController.doQuizzes(router);
    },
    
    importQuiz: function(sharedSocNumber) {
        quizzesController.doImportSharedQuiz(router, sharedSocNumber);
    },
    
    editQuiz: function(socNumber) {
        editQuizController.doEditQuiz(socNumber, router);
    },

    spaceRace: function() {
        spaceRaceController.doSpaceRace(router);
    },
    
    reports: function() {
        reportsController.doReports(router);
    },

    rooms: function() {
        roomsController.doRooms(router);
    },

    profileInfo: function() {
        profileController.doProfile(router);
    },
    
    profileDemo: function() {
        demographicsController.doDemo(router);
    },
    
    profileBilling: function() {
        accountController.doAccount(router);
    },
    
    liveResultsTable: function() {
        resultsTableController.doResultsTable(router, 'live');
    },

    liveResultsQuestion: function(number) {
        resultsQuestionController.doResultsQuestion(router, 'live', number);
    },

    finalResultsTable: function(activityId, quizId) {
        resultsTableController.doResultsTable(router, 'final', activityId, quizId);
    },

    finalResultsQuestion: function(activityId, quizId, number) {
        resultsQuestionController.doResultsQuestion(router, 'final', number, activityId, quizId);
    },


    /*--------------------------*/
    /*    Navigation Methods    */
    /*--------------------------*/

    routeToTeacherLogin: function() {
        window.location.href = '/login/teacher/';
    },

    routeToProfileInfo: function() {
        router.navigate('profile/info', {trigger: true});
    },
    
    routeToProfileDemo: function() {
        router.navigate('profile/demo', {trigger: true});
    },
    
    routeToProfileBilling: function() {
        router.navigate('profile/billing', {trigger: true});
    },
    
    routeToLaunch: function() {
        router.navigate('launch', {trigger: true});
    },
    
    routeToLaunchAndReplace: function() {
        router.navigate('launch', {trigger: true, replace: true});
    },
    
    routeToLiveResults: function(data) {
        if (data && data.number) {
            router.navigate('live-results/question/' + data.number, {trigger: true});
        } else {
            // Ideally the router would not be aware of activity logic, but this code provides a central
            // location for determining where to route when any activity is in progress (see SOC-566).
            let activity = require('activity');
            if (activity.isNonSpaceRace()) {
                let route = 'live-results/table';
                if (activity.isTeacherPaced()) {
                    route = 'live-results/question/' + activity.getCurrentQuestionNumber();
                }
                router.navigate(route, {trigger: true});
            } else if (activity.isSpaceRace()) {
                router.routeToSpaceRace();
            }
        }
    },

    routeToFinalResultsTable: function(data) {
        router.navigate('final-results/'+data.activityId+'/'+data.quizId+'/table', {trigger: true});
    },

    routeToFinalResultsQuestion: function(data) {
        router.navigate('final-results/'+data.activityId+'/'+data.quizId+'/question/'+data.number, {trigger: true});
    },

    routeToEditQuiz: function(socNumber) {
        router.navigate('edit-quiz/' + socNumber, {trigger: true});
    },
    
    routeToQuizzes: function() {
        router.navigate('quizzes', {trigger: true});
    },
    
    routeToQuizzesAndReplace: function() {
        router.navigate('quizzes', {trigger: true, replace: true});
    },

    routeToRooms: function() {
        router.navigate('rooms', {trigger: true});
    },

    routeToReports: function() {
        router.navigate('reports', {trigger: true});
    },

    routeToSpaceRace: function() {
        router.navigate('space-race', {trigger: true});
    },

    routeToDynamic: function(data) {
        router.navigate(data.route, {trigger: true});
    }

});

let router = new TeacherRouter();
module.exports = router;
