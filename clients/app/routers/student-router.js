var Backbone                = require('backbone'),
    rootStudentController   = require('root-student-controller'),
    studentWaitController   = require('StudentWaitController'),
    studentNameController   = require('StudentNameController'),
    studentTeamController   = require('StudentTeamController'),
    studentQuizController   = require('StudentQuizController');

var StudentRouter = Backbone.Router.extend({

    routes: {
        '':     'root',
        'wait': 'wait',
        'name': 'name',
        'team': 'team',
        'quiz': 'quiz',
        'join-room/:roomName': 'joinRoom',
        '*bad': 'root' // This handles all invalid routes.
    },
    
    root: function() {
        rootStudentController.doRootStudent(this);
    },
    
    wait: function() {
        studentWaitController.doWait();
    },

    name: function() {
        studentNameController.doName();
    },
    
    team: function() {
        studentTeamController.doTeam();
    },
    
    quiz: function() {
        studentQuizController.doQuiz();
    },
    
    joinRoom: function(roomName) {
        rootStudentController.doJoinRoom(this, roomName);
    },
    
    /**
     * When the requested route matches the existing route, execute  
     * the route directly. Otherwise, simply navigate to the route.
     * @param props An object containing the route's properties.
     */
    executeOrNavigate: function(props) {
        if (Backbone.history.fragment === props.fragment) {
            this.execute(props.callback, props.callbackArgs, props.callbackName);
        } else {
            this.navigate(props.fragment, {trigger: true});
        }
    },
    
    execute: function(callback, callbackArgs, callbackName) {
        if (callbackName !== 'joinRoom' && callbackName !== 'root' && !rootStudentController.isReady()) {
            this.root();
            return false;
        } else {
            callback.apply(this, callbackArgs);
        }
    },

    routeToWait: function() {
        var props = {
            fragment:     'wait',
            callback:     this.wait,
            callbackArgs: [],
            callbackName: 'wait'
        };
        this.executeOrNavigate(props);
    },

    routeToName: function() {
        var props = {
            fragment:     'name',
            callback:     this.name,
            callbackArgs: [],
            callbackName: 'name'
        };
        this.executeOrNavigate(props);
    },
    
    routeToTeam: function() {
        var props = {
            fragment:     'team',
            callback:     this.team,
            callbackArgs: [],
            callbackName: 'team'
        };
        this.executeOrNavigate(props);
    },
    
    routeToQuiz: function() {
        var props = {
            fragment:     'quiz',
            callback:     this.quiz,
            callbackArgs: [],
            callbackName: 'quiz'
        };
        this.executeOrNavigate(props);
    },
    
    routeToStudentLogin: function() {
        window.location.href = '/login/student/';
    }

});

module.exports = new StudentRouter();
