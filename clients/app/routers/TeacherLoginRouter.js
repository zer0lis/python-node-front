import Backbone from 'backbone';
import loginController from 'LoginController';
import createAccountProfileController from 'CreateAccountProfileController';
import createAccountDemographicsController from 'CreateAccountDemographicsController';
import createAccountTypeController from 'CreateAccountTypeController';

// These controllers are initially null because they're loaded on demand.
let forgotPasswordController = null,
    resetPasswordController = null,
    googleLoginController = null;

let TeacherLoginRouter = Backbone.Router.extend({
    
    routes: {
        '': 'login',
        'zendesk-sso': 'login',
        'get-pro': 'login',
        'logout': 'logout',
        'forgot-password': 'forgotPassword',
        'create-password/*token': 'resetPassword',
        'reset-password/*token': 'resetPassword',
        'oauthlogin': 'OAuthLogin',
        'oauthlogin-zendesk-sso': 'OAuthLoginZendeskSSO',
        'google-login-success/:email': 'googleLoginSuccess',
        'google-error/:error': 'OAuthError',
        'google-create/:email': 'OAuthLoginCreateAccount',
        'google-approval/:state': 'OAuthLoginPromptApproval',
        'register/info': 'registerInfo',
        'register/demo': 'registerDemo',
        'register/account': 'registerAccount',
        'terms': 'terms',
        'teacher-login/:authToken': 'teacherLogin',
        '*bad': 'bad' // This handles all invalid routes.
    },

    
/*-----------------------------*/
/*    Route Handler Methods    */
/*-----------------------------*/
    
    login: function() {
        loginController.doLogin(this);
    },
    
    logout: function() {
        loginController.doLogout(this);
    },
    
    forgotPassword: function() {
        this.loadForgotPasswordController(() => {
            forgotPasswordController.doForgotPassword(this);
        });
    },
    
    resetPassword: function(token) {
        this.loadResetPasswordController(() => {
            resetPasswordController.doResetPassword(this, token);
        });
    },
    
    OAuthLogin: function() {
        this.loadGoogleLoginController(() => {
            googleLoginController.doOAuthLogin(this);
        });
    },
    
    OAuthLoginZendeskSSO: function() {
        this.loadGoogleLoginController(() => {
            googleLoginController.doOAuthLogin(this, true);
        });
    },
    
    googleLoginSuccess: function() {
        this.loadGoogleLoginController(() => {
            googleLoginController.doLoginSuccess(this);
        });
    },
    
    OAuthError: function(error) {
        this.loadGoogleLoginController(() => {
            googleLoginController.doOAuthError(this, error);
        });
    },
    
    OAuthLoginCreateAccount: function (email) {
        this.loadGoogleLoginController(() => {
           googleLoginController.showCreateAccountPopup(this, email);
        });
    },
    
    OAuthLoginPromptApproval: function (state) {
        this.loadGoogleLoginController(() => {
            googleLoginController.promptForApproval(this, state);
        });
    },
    
    registerInfo: function() {
        createAccountProfileController.doCreateAccountProfile(this);
    },
    
    registerDemo: function() {
        createAccountDemographicsController.doCreateAccountDemographics(this);
    },
    
    registerAccount: function() {
        createAccountTypeController.doAccountType(this);
    },
    
    terms: function() {
        window.location.replace('https://www.socrative.com/terms.php');
    },
    
    // Route used by MasteryConnect.
    teacherLogin: function(authToken) {
        window.location.href = '/teacher/#teacher-login/' + authToken;
    },
    
    bad: function() {
        this.routeToIndex(); 
    },


/*--------------------*/
/*    Load Methods    */
/*--------------------*/

    loadForgotPasswordController: function(callback) {
        if (forgotPasswordController === null) {
            require.ensure(['ForgotPasswordController'], (require) => {
                forgotPasswordController = require('ForgotPasswordController');
                if (callback) {
                    callback();
                }
            });
        } else {
            if (callback) {
                callback();
            }
        }
    },
    
    loadResetPasswordController: function(callback) {
        if (resetPasswordController === null) {
            require.ensure(['ResetPasswordController'], (require) => {
                resetPasswordController = require('ResetPasswordController');
                callback();
            });
        } else {
            callback();
        }
    },
    
    loadGoogleLoginController: function(callback) {
        if (googleLoginController === null) {
            require.ensure(['GoogleLoginController'], (require) => {
                googleLoginController = require('GoogleLoginController');
                if (callback) {
                    callback();
                }
            });
        } else {
            if (callback) {
                callback();
            }
        }
    },


/*--------------------------*/
/*    Navigation Methods    */
/*--------------------------*/

    routeToIndex: function() {
        this.navigate('', {trigger: true});
    },
    
    routeToTeacherLogin: function() {
        window.location.href = '/login/teacher/';
    },
    
    routeToForgotPassword: function() {
        this.navigate('forgot-password', {trigger: true});
    },
    
    routeToTeacherLaunch: function() {
        window.location.href = '/teacher/#launch';
    },
    
    routeToTeacherLaunchAndReplace: function() {
        let protocol = window.location.protocol,
            hostname = window.location.hostname,
            port     = window.location.port;
        window.location.replace(protocol + '//' + hostname + (port ? (':' + port) : '') + '/teacher/#launch');
    },
    
    routeToImportQuiz: function(sharedSocNumber) {
        window.location.href = `/teacher/#import-quiz/${sharedSocNumber}`;
    },
    
    routeToOAuthLogin: function() {
        this.navigate('oauthlogin', {trigger: true});
    },
    
    routeToOAuthZendeskLogin: function() {
        this.navigate('oauthlogin-zendesk-sso', {trigger: true});
    },
    
    routeToRegisterInfo: function() {
        this.navigate('register/info', {trigger: true});
    },
    
    routeToRegisterDemo: function() {
        this.navigate('register/demo', {trigger: true});
    },
    
    routeToRegisterAccount: function() {
        this.navigate('register/account', {trigger: true});
    }
    
});

module.exports = new TeacherLoginRouter();
