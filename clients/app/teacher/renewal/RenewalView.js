import React     from 'react';
import CloseX    from 'CloseX';
import InfoIcon  from 'InfoIcon';
import Constants from 'Constants';
import {translate} from 'translator';

let e = React.createElement;

export default class RenewalView extends React.Component {

    render() {
        let c = this.props.controller,
            m = this.props.model,
            message = m.type === Constants.PRO_ENDING ? translate('Your PRO access will expire on') : translate('Your PRO access expired on');
        
        return e('div', {
            id: 'renewal-notify',
            className: m.type === Constants.PRO_ENDING ? '' : 'expired',
            children: [
                e(InfoIcon, {
                    className: 'info-renewal-icon'
                }),
                e('div', {
                    className: 'renewal-content clearfix',
                    children: [
                        e('div', {
                            children: [
                                e('span', {
                                    children: [
                                        e('strong', {
                                            children: `${message} ${m.expirationDateFormatted}.`
                                        })
                                    ]
                                }),
                                e(CloseX, {
                                    className: 'close-renewal-icon align-right',
                                    onClick: c.closeClicked
                                }),
                                e('div', {
                                    className: 'actions-container clearfix',
                                    children: [
                                        e('button', {
                                            className: 'renew-now-button align-right',
                                            children: translate('Renew Now!'),
                                            onClick: c.renewNowClicked
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
