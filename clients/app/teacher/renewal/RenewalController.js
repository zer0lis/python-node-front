import React    from 'react';
import ReactDOM from 'react-dom';
import model    from 'RenewalModel';
import view     from 'RenewalView';
import header   from 'HeaderController';

let router = null;

class RenewalController {
    
    initialize(teacherRouter) {
        router = teacherRouter;
        
        if (model.show) {
            controller.render();
        }
    }

    render() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('renewal-notify-container')
        );
    }
    
    renewNowClicked() {
        router.routeToProfileBilling();
    }
    
    closeClicked() {
        model.show = false;
        
        let notify = document.getElementById('renewal-notify');
        
        if (notify) {
            TweenLite.to(notify, 1, {
                marginTop: '-200px', ease: Quint.easeOut, onComplete: () => {
                    model.closeBanner();
                }
            });
        }
        
        header.hideMobileMenus();
    }
    
    // This method is used externally to dismiss the notice after successful renewal.
    closeAfterRenewal() {
        ReactDOM.unmountComponentAtNode(document.getElementById('renewal-notify-container'));
        model.clear();
        header.rerender();
    }
}

let controller = new RenewalController();
module.exports = controller;
