import React from 'react';
import CloseX from 'CloseX';
import SuccessIcon from 'LoggedInIcon';
import {translate} from 'translator';

let e = React.createElement;

export default class ShareQuizView extends React.Component {
    
    componentDidMount() {
        let nameInput = document.getElementById('copyUrl');
        
        if (nameInput) {
            nameInput.select();
            nameInput.focus();
        }
    }
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            protocol = window.location.protocol,
            host = window.location.host,
            url = `${protocol}//${host}/teacher/#import-quiz/${m.socNumber}`;
        
        let buttonChildren = [];
        
        if (m.copied) {
            buttonChildren.push(
                e(SuccessIcon, {
                    color: '#ffffff',
                    svgClass: 'share-url-copied-icon'
                }),
                translate('COPIED')
            );
        } else {
            buttonChildren.push(
                translate('COPY')
            );
        }
        
        return e('div', {
            id: 'share-quiz-modal-background',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'share-quiz-modal-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('span', {
                                    className: 'popup-title',
                                    children: translate('Share Quiz')
                                }),
                                e('div', {
                                    className: 'cancel-button',
                                    children: [
                                        e(CloseX, {
                                            className: 'cancel-button-x',
                                            color: '#cccccc'
                                        })
                                    ],
                                    onClick: c.cancelClicked
                                })
                            ]
                        }),
                        e('div', {
                            id: 'copy-quiz-modal-content',
                            className: 'form',
                            children: [
                                e('span', {
                                    className: 'share-quiz-explanation',
                                    children: translate('Share this URL with teachers so they can directly import a copy of this quiz into their accounts.')
                                }),
                                e('span', {
                                    className: 'share-quiz-name-and-number',
                                    children: translate('Quiz Name & Number')
                                }),
                                e('span', {
                                    className: 'share-quiz-name',
                                    children: m.quizName
                                }),
                                e('span', {
                                    className: 'share-quiz-soc-number',
                                    children: `SOC-${m.socNumber}`
                                }),
                                e('span', {
                                    className: 'share-quiz-copy-url',
                                    children: translate('Copy quiz URL')
                                }),
                                e('div', {
                                    className: 'share-quiz-copy-url-container',
                                    children: [
                                        e('input', {
                                            className: 'share-quiz-copy-url-input' + (m.copied ? ' share-quiz-copy-url-input-copied' : ''),
                                            id: 'copyUrl',
                                            readOnly: true,
                                            value: url,
                                            onFocus: () => {
                                                let input = document.getElementById('copyUrl');
                                                input.focus();
                                                input.select();
                                            }
                                        }),
                                        e('button', {
                                            className: 'share-quiz-copy-url-button' + (m.copied ? ' share-quiz-copy-url-button-copied' : ''),
                                            children: buttonChildren,
                                            onClick: () => {
                                                let input = document.getElementById('copyUrl');
                                                input.focus();
                                                input.setSelectionRange(0, input.value.length);
                                                document.execCommand('copy');
                                                c.copyClicked();
                                                input.blur();
                                            }
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
