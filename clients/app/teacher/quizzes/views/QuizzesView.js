import React from 'react';
import CrumbNav from 'CrumbNav';
import MenuButton from 'MenuButton';
import PlusIcon from 'PlusIcon';
import SuccessIcon from 'LoggedInIcon';
import AlertIcon from 'AlertIcon';
import SearchIcon from 'SearchIcon';
import Input from 'Input';
import CloseX from 'CloseX';
import ToggleFolderNavIcon from 'ToggleFolderNavIcon';
import MoveIcon from 'MoveIcon';
import NewFolderIcon from 'NewFolderIcon';
import NavFolder from 'NavFolder';
import NavTrash from 'NavTrash';
import TrashIcon from 'TrashIcon';
import TrashIconLarge from 'TrashIconLarge';
import RestoreIcon from 'RestoreIcon';
import SortDownIcon from 'SortDownIcon';
import SortUpIcon from 'SortUpIcon';
import ProBadgeIcon from 'ProBadgeIcon';
import Anchor from 'Anchor';
import CopyIcon from 'CopyIcon';
import DownloadIcon from 'DownloadIcon';
import ShareIcon from 'ShareIcon';
import MergeIcon from 'MergeIcon';
import IconLabelButton from 'IconLabelButton'
import Tooltip from 'Tooltip';
import MoreButton from 'MoreButton';
import Loader from 'Loader';
import NoResultsIcon from 'NoResultsIcon';
import EmptyFolderIcon from 'EmptyFolderIcon';
import FolderIcon from 'FolderIcon';
import EditButton from 'EditButton';
import platform from 'Platform';
import Constants from 'Constants';
import {translate} from 'translator';

let e = React.createElement;

export default class QuizzesView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            folderNav = [],
            quizzesContentChildren = [],
            searchContainerChildren = [];
        
        if (m.showBanner && m.bannerText) {
            let Icon = m.bannerIsError ? AlertIcon : SuccessIcon;
            
            searchContainerChildren.push(
                e('div', {
                    className: 'quizzes-banner' + (m.bannerIsError ? ' quizzes-banner-error' : ''),
                    children: [
                        e(Icon, {
                            color: '#ffffff',
                            svgClass: 'quizzes-banner-icon'
                        }),
                        e('span', {
                            className: 'quizzes-banner-text',
                            children: m.bannerText
                        }),
                        e('span', {
                            className: 'quizzes-banner-sub-text',
                            children: m.bannerSubText
                        })
                    ]
                })
            );
        }
        
        searchContainerChildren.push(
            e(SearchIcon, {
                className: 'quizzes-search-icon'
            }),
            e(Input, {
                id: 'quizzes-search-input',
                maxLength: 255,
                onChange: (event) => c.searchTextChanged(event.target.value),
                onKeyPress: (event) => {
                    if (event.key === 'Enter' && platform.isMobile()) {
                        event.target.blur();
                    }
                },
                placeholder: translate('Search Quizzes'),
                type: 'text',
                value: m.searchText || ''
            })
        );
        
        if (m.searchText.length > 0) {
            searchContainerChildren.push(
                e('div', {
                    className: 'clear-search',
                    children: [
                        e(CloseX, {
                            className: 'clear-search-x',
                            color: Constants.CLEAR_INPUT_X
                        })
                    ],
                    onClick: c.clearSearchClicked
                })
            );
        }
        
        if (this.props.showTrashTooltip) {
            searchContainerChildren.push(
                e(Tooltip, {
                    className: 'quizzes-trash-tooltip',
                    location: 'above',
                    text: translate('One or more items in the trash matched your search.')
                })
            );
        }
        
        if (!m.waiting) {
            let folderNavChildren = [];

            if (this.props.quizFolderNav) {
                this.props.quizFolderNav.traverse((folder) => {
                    if (this.props.folderNavOpen && folder.visible && folder.level + 1 <= Constants.MAX_FOLDER_TREE_DEPTH) {
                        if (folder.id === Constants.ROOT_FOLDER || this.props.isPro) {
                            folderNavChildren.push(
                                e(NavFolder, {
                                    className: '',
                                    current: folder.id === this.props.currentFolderId,
                                    expanded: folder.expanded,
                                    id: folder.id,
                                    level: folder.level,
                                    name: folder.name,
                                    onClick: () => c.navFolderClicked(folder.id),
                                    onDoubleClick: () => c.expanderClicked(folder.id),
                                    onExpanderClick: () => c.expanderClicked(folder.id)
                                })
                            );
                        }
                    }
                });
                
                if (this.props.folderNavOpen) {
                    folderNavChildren.push(
                        e(NavTrash, {
                            current: this.props.currentFolderId === Constants.TRASH,
                            onClick: c.navTrashClicked
                        })
                    );
                }
            }

            folderNav.push(
                e('div', {
                    className: 'quizzes-folder-nav-container' + (!this.props.folderNavOpen ? ' quizzes-folder-nav-container-closed' : ''),
                    children: [
                        e(IconLabelButton, {
                            className: 'state-change-button toggle-folders-button',
                            icon: ToggleFolderNavIcon,
                            iconClass: 'quizzes-folders-toggle-icon',
                            label: this.props.folderNavOpen ? translate('FOLDERS') : '',
                            onClick: c.toggleFolderNavClicked,
                            title: this.props.folderNavOpen ? '' : translate('Folders Tree View')
                        }),
                        e('div', {
                            className: 'quizzes-folder-nav',
                            children: folderNavChildren
                        })
                    ]
                })
            );
            
            let topButtons = [];
            
            if (this.props.currentFolderId !== Constants.TRASH) {
                let currentFolder = this.props.quizFolderNav.getFolder(this.props.currentFolderId);
                
                topButtons.push(
                    e(IconLabelButton, {
                        className: 'state-change-button',
                        disabled: !this.props.anyChecked,
                        icon: TrashIcon,
                        iconClass: 'quizzes-delete-button-icon',
                        label: translate('DELETE'),
                        onClick: c.deleteClicked
                    }),
                    e(IconLabelButton, {
                        className: 'state-change-button',
                        disabled: !this.props.canMerge,
                        icon: MergeIcon,
                        iconClass: 'quizzes-merge-icon',
                        label: translate('MERGE'),
                        onClick: c.mergeClicked,
                        tooltipTitle: translate('Select Two Quizzes'),
                        tooltipText: translate('Please select two quizzes in the same folder.')
                    }),
                    e(IconLabelButton, {
                        className: 'state-change-button',
                        disabled: !this.props.anyChecked,
                        icon: MoveIcon,
                        iconClass: 'quizzes-move-icon',
                        label: translate('MOVE'),
                        onClick: c.moveClicked
                    }),
                    e(IconLabelButton, {
                        className: 'state-change-button quizzes-new-folder-button',
                        icon: NewFolderIcon,
                        iconClass: 'quizzes-new-folder-button-icon',
                        label: translate('CREATE FOLDER'),
                        onClick: c.createFolderClicked
                    })
                );
            } else {
                topButtons.push(
                    e(IconLabelButton, {
                        className: 'state-change-button',
                        disabled: !this.props.anyChecked,
                        icon: RestoreIcon,
                        label: translate('RESTORE'),
                        onClick: c.restoreClicked
                    }),
                    e(IconLabelButton, {
                        className: 'state-change-button',
                        disabled: !this.props.anyChecked,
                        icon: TrashIcon,
                        label: translate('DELETE FOREVER'),
                        onClick: c.purgeClicked
                    })
                );
            }
            
            quizzesContentChildren.push(
                e('div', {
                    className: 'top-buttons',
                    children: topButtons
                })
            );
        }
        
        if (m.waiting) {
            quizzesContentChildren.push(
                e(Loader, {
                    className: 'quizzes-loader',
                    text: m.waitingText
                })
            );
        } else if (this.props.visibleFolders.length === 0 && this.props.visibleQuizzes.length === 0) {
            let noQuizzesChildren = [],
                NoQuizzesIcon = NoResultsIcon,
                noQuizzesClass = 'no-quizzes-found-icon',
                noQuizzesText = translate('Nothing found matching "{0}".').format(m.searchText);
            
            if (m.searchText.length === 0) {
                NoQuizzesIcon = this.props.currentFolderId === Constants.TRASH ? TrashIconLarge : EmptyFolderIcon;
                noQuizzesClass = 'empty-folder-icon-default';
                noQuizzesText = this.props.currentFolderId === Constants.TRASH ? translate('The trash is empty') : translate('This folder is empty');
            }
            
            noQuizzesChildren.push(
                e(NoQuizzesIcon, {
                    className: noQuizzesClass
                }),
                e('span', {
                    className: 'no-quizzes-found-large-text',
                    children: noQuizzesText
                })
            );
            
            if (m.searchText.length > 0) {
                noQuizzesChildren.push(
                    e('span', {
                        className: 'start-over-text',
                        children: translate('Delete search terms to start over.')
                    })
                );
            } else if (this.props.currentFolderId !== Constants.TRASH) {
                noQuizzesChildren.push(
                    e(MenuButton, {
                        className: 'add-something-menu-button',
                        icon: e(PlusIcon, {
                            innerClass: 'create-quiz-plus-icon-color',
                            outerClass: 'create-quiz-plus-icon'
                        }),
                        label: translate('ADD SOMETHING'),
                        loading: m.creatingQuiz,
                        menuItems: [
                            {
                                keyValue: 'create-quiz',
                                label: translate('Create New Quiz'),
                                onClick: c.createQuizClicked
                            },
                            {
                                keyValue: 'import-quiz',
                                label: translate('Import Quiz'),
                                onClick: c.importClicked
                            },
                            {
                                keyValue: 'create-folder',
                                label: translate('Create New Folder'),
                                onClick: c.createFolderClicked
                            }
                        ]
                    })
                );
            }
            
            quizzesContentChildren.push(
                e('div', {
                    className: 'no-quizzes-found-container',
                    children: noQuizzesChildren
                })
            );
        }
        
        if (!m.waiting && (this.props.visibleFolders.length > 0 || this.props.visibleQuizzes.length > 0)) {
            let NameSortIcon = m.sortDirection === 'asc' ? SortUpIcon : SortDownIcon;
            
            if (m.sortColumn !== 'name') {
                NameSortIcon = SortUpIcon;
            }
            
            let DateSortIcon = m.sortDirection === 'asc' ? SortUpIcon : SortDownIcon;
            
            if (m.sortColumn !== 'date') {
                DateSortIcon = SortDownIcon;
            }
            
            let tableChildren = [
                e('tr', {
                    children: [
                        e('th', {
                            className: 'quizzes-all-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                            children: [
                                e('div', {
                                    children: [
                                        e('input', {
                                            className: 'vertical-align-middle',
                                            type: 'checkbox',
                                            id: 'quizzes-all-check',
                                            checked: m.allChecked,
                                            onChange: c.allChanged
                                        }),
                                        e('label', {
                                            className: 'vertical-align-middle quizzes-all-label',
                                            htmlFor: 'quizzes-all-check',
                                            children: translate('ALL')
                                        })
                                    ]
                                })
                            ]
                        }),
                        e('th', {
                            className: 'quizzes-name-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                            children: [
                                e('span', {
                                    className: 'quizzes-sort-header' + (m.sortColumn === 'name' ? ' quizzes-active-sort-header' : ''),
                                    children: [
                                        e('span', {
                                            className: 'quizzes-column-header-text',
                                            children: translate('NAME')
                                        }),
                                        e(NameSortIcon, {
                                            className: 'quizzes-sort-icon' + (m.sortColumn === 'name' ? ' quizzes-active-sort-icon' : '')
                                        })
                                    ],
                                    title: translate('Sort by name'),
                                    onClick: () => c.columnClicked('name')
                                })
                            ]
                        }),
                        e('th', {
                            className: 'quizzes-date-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                            children: [
                                e('span', {
                                    className: 'quizzes-sort-header' + (m.sortColumn === 'date' ? ' quizzes-active-sort-header' : ''),
                                    children: [
                                        e('span', {
                                            className: 'quizzes-column-header-text',
                                            children: translate('DATE')
                                        }),
                                        e(DateSortIcon, {
                                            className: 'quizzes-sort-icon' + (m.sortColumn === 'date' ? ' quizzes-active-sort-icon' : '')
                                        })
                                    ],
                                    title: translate('Sort by date'),
                                    onClick: () => c.columnClicked('date')
                                })
                            ]
                        }),
                        e('th', {
                            className: 'quizzes-copy-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                            children: this.props.currentFolderId === Constants.TRASH ? '' : translate('COPY')
                        }),
                        e('th', {
                            className: 'quizzes-download-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                            children: this.props.currentFolderId === Constants.TRASH ? '' : translate('DOWNLOAD')
                        }),
                        e('th', {
                            className: 'quizzes-share-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                            children: this.props.currentFolderId === Constants.TRASH ? '' : translate('SHARE')
                        }),
                        e('th', {
                            className: 'quizzes-more-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                            children: ''
                        })
                    ]
                })
            ];
            
            for (let folder of this.props.visibleFolders) {
                let folderNameChildren = [
                    e(FolderIcon, {
                        className: 'quiz-list-folder-icon'
                    }),
                    e(Anchor, {
                        disabled: folder.deleted,
                        keyValue: `folder-${folder.id}`,
                        label: folder.name,
                        onClick: () => c.navFolderClicked(folder.id),
                        tooltipTitle: translate('Deleted Item'),
                        tooltipText: translate('Items in the trash must be restored before they can be accessed.')
                    })
                ];
                
                if (!folder.deleted) {
                    folderNameChildren.push(
                        e(EditButton, {
                            className: 'edit-quiz-folder-button',
                            onClick: () => c.renameFolderClicked(folder.id, folder.name)
                        })
                    );
                }
                
                tableChildren.push(
                    e('tr', {
                        className: 'quizzes-folder-row',
                        children: [
                            e('td', {
                                className: 'quizzes-checkbox-column',
                                children: [
                                    e('input', {
                                        className: 'vertical-align-middle',
                                        type: 'checkbox',
                                        checked: folder.checked,
                                        onChange: () => c.folderToggled(folder.id)
                                    })
                                ]
                            }),
                            e('td', {
                                className: 'quizzes-name-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                                children: folderNameChildren
                            }),
                            e('td', {
                                className: 'quizzes-date-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                                children: '{0}/{1}/{2}'.format(folder.date.getMonth() + 1, folder.date.getDate(), folder.date.getFullYear() - 2000)
                            }),
                            e('td', {
                                className: 'quizzes-copy-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                                children: []
                            }),
                            e('td', {
                                className: 'quizzes-download-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                                children: []
                            }),
                            e('td', {
                                className: 'quizzes-share-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                                children: []
                            }),
                            e('td', {
                                className: 'quizzes-more-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                                children: []
                            })
                        ]
                    })
                );
            }
            
            if (!this.props.isPro) {
                tableChildren.push(
                    e('tr', {
                        className: 'quizzes-go-pro-row',
                        children: [
                            e('td', {
                                className: 'quizzes-go-pro-cell',
                                colSpan: 6,
                                children: [
                                    e('div', {
                                        className: 'quizzes-go-pro-cell-container',
                                        children: [
                                            e(NewFolderIcon, {
                                                className: 'quizzes-go-pro-cell-folder-icon'
                                            }),
                                            e('span', {
                                                dangerouslySetInnerHTML: {__html: translate('Create <strong>folders</strong> and so much more!')}
                                            }),
                                            e(ProBadgeIcon, {
                                                className: 'quizzes-go-pro-badge-icon',
                                                color: '#f89e1b',
                                                size: '24'
                                            }),
                                            e('span', {
                                                dangerouslySetInnerHTML: {__html: translate('Go <strong>PRO</strong>!')}
                                            }),
                                            e('span', {
                                                className: 'link quizzes-go-pro-cell-learn-more-text',
                                                children: translate('Learn More')
                                            })
                                        ]
                                    })
                                ]
                            })
                        ],
                        onClick: c.createFolderClicked
                    })
                );
            }
            
            for (let quiz of this.props.visibleQuizzes) {
                let nameChildren = [
                    e(Anchor, {
                        disabled: quiz.deleted,
                        href: `#edit-quiz/${quiz.soc}`,
                        keyValue: `quiz-${quiz.id}`,
                        label: quiz.name,
                        tooltipTitle: translate('Deleted Item'),
                        tooltipText: translate('Items in the trash must be restored before they can be accessed.')
                    })
                ];
                
                if (quiz.standard && quiz.standard.length > 0) {
                    nameChildren.push(
                        e('div', {
                            className: 'quiz-name-sub-children',
                            children: [
                                e('span', {
                                    className: 'quiz-standard-text',
                                    children: quiz.standard
                                })
                            ]
                        })
                    );
                }
                
                let shareChildren = [
                    e(ShareIcon, {
                        className: 'share-quiz-icon' + (!quiz.sharable ? ' share-quiz-icon-disabled' : '')
                    })
                ];
                
                if (m.showSharableTooltip && m.nonSharableSoc === quiz.soc) {
                    shareChildren.push(
                        e(Tooltip, {
                            location: 'above',
                            title: translate('Sharing Disabled'),
                            text: translate('You have disabled sharing for this quiz. Edit the quiz to enable sharing.')
                        })
                    );
                }
                
                let copyChildren = [],
                    downloadChildren = [],
                    shareColumnChildren = [],
                    moreChildren = [];
                
                if (!quiz.deleted) {
                    copyChildren.push(
                        e('div', {
                            className: 'copy-quiz-icon-button',
                            children: [
                                e(CopyIcon, {
                                    className: 'copy-quiz-icon'
                                })
                            ],
                            title: translate('Copy Quiz'),
                            onClick: () => c.copyClicked(quiz.soc, quiz.name)
                        })
                    );
                    
                    downloadChildren.push(
                        e('div', {
                            className: 'download-quiz-icon-button' + (platform.isIos ? ' disabled' : ''),
                            children: [
                                e(DownloadIcon, {
                                    className: 'download-quiz-icon'
                                })
                            ],
                            title: translate('Download Quiz'),
                            onClick: () => c.downloadClicked(quiz.soc)
                        })
                    );
                    
                    shareColumnChildren.push(
                        e('div', {
                            className: 'share-quiz-icon-button' + (!quiz.sharable ? ' share-quiz-icon-button-disabled' : ''),
                            children: shareChildren,
                            title: translate('Share Quiz URL'),
                            onClick: () => c.shareClicked(quiz.sharable, quiz.name, quiz.soc)
                        })
                    );
                    
                    moreChildren.push(
                        e(MoreButton, {
                            className: 'quizzes-more-button',
                            arrowClass: 'quizzes-more-menu-arrow',
                            menuClass: 'quizzes-more-menu',
                            menuItems: [
                                e('div', {
                                    className: 'quizzes-more-menu-item',
                                    children: [
                                        e(CopyIcon, {
                                            className: 'more-item-icon',
                                            color: '#8cb4d2'
                                        }),
                                        e('span', {
                                            className: 'more-menu-text',
                                            children: translate('COPY')
                                        })
                                    ],
                                    onClick: () => c.copyClicked(quiz.soc, quiz.name)
                                }),
                                e('div', {
                                    className: 'quizzes-more-menu-item' + (platform.isIos ? ' disabled' : ''),
                                    children: [
                                        e(DownloadIcon, {
                                            className: 'more-item-icon',
                                            color: '#8cb4d2'
                                        }),
                                        e('span', {
                                            className: 'more-menu-text',
                                            children: translate('DOWNLOAD')
                                        })
                                    ],
                                    onClick: () => c.downloadClicked(quiz.soc)
                                }),
                                e('div', {
                                    className: 'quizzes-more-menu-item' + (!quiz.sharable ? ' quizzes-more-menu-item-disabled' : ''),
                                    children: [
                                        e(ShareIcon, {
                                            className: 'more-item-icon more-share-quiz-icon' + (!quiz.sharable ? ' more-share-quiz-icon-disabled' : '')
                                        }),
                                        e('span', {
                                            className: 'more-menu-text' + (!quiz.sharable ? ' more-menu-text-disabled' : ''),
                                            children: translate('SHARE')
                                        })
                                    ],
                                    onClick: () => {
                                        if (quiz.sharable) {
                                            c.shareClicked(quiz.sharable, quiz.name, quiz.soc);
                                        }
                                    }
                                })
                            ]
                        })
                    );
                }
                
                tableChildren.push(
                    e('tr', {
                        children: [
                            e('td', {
                                className: 'quizzes-checkbox-column',
                                children: [
                                    e('input', {
                                        className: 'vertical-align-middle',
                                        type: 'checkbox',
                                        checked: quiz.checked,
                                        onChange: () => c.quizToggled(quiz.id)
                                    })
                                ]
                            }),
                            e('td', {
                                className: 'quizzes-name-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                                children: nameChildren
                            }),
                            e('td', {
                                className: 'quizzes-date-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                                children: '{0}/{1}/{2}'.format(quiz.date.getMonth() + 1, quiz.date.getDate(), quiz.date.getFullYear() - 2000)
                            }),
                            e('td', {
                                className: 'quizzes-copy-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                                children: copyChildren
                            }),
                            e('td', {
                                className: 'quizzes-download-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                                children: downloadChildren
                            }),
                            e('td', {
                                className: 'quizzes-share-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                                children: shareColumnChildren
                            }),
                            e('td', {
                                className: 'quizzes-more-column' + (this.props.folderNavOpen ? ' quizzes-folder-nav-open' : ''),
                                children: moreChildren
                            })
                        ]
                    })
                );
            }
            
            quizzesContentChildren.push(
                e('table', {
                    className: 'quizzes-table',
                    children: [
                        e('tbody', {
                            children: tableChildren
                        })
                    ]
                })
            );
        }
        
        let quizzesContentClass = 'quizzes-content-container';
        
        if (!m.waiting) {
            quizzesContentClass += this.props.folderNavOpen ? ' quizzes-content-container-with-folder-nav' : ' quizzes-content-container-without-folder-nav';
        }
        
        let headerChildren = [];
        
        if (m.searchText.length === 0 &&
            this.props.quizFolderNav &&
            this.props.currentFolderId !== Constants.ROOT_FOLDER &&
            this.props.currentFolderId !== Constants.TRASH) {
            headerChildren.push(
                e(CrumbNav, {
                    currentFolder: this.props.quizFolderNav.getFolder(this.props.currentFolderId),
                    folderNavModel: this.props.quizFolderNav,
                    onClick: (id) => c.navFolderClicked(id)
                })
            );
        } else {
            headerChildren.push(
                e('span', {
                    className: 'quizzes-header-text',
                    children: translate('Quizzes')
                })
            );
        }
        
        headerChildren.push(
            e(MenuButton, {
                className: 'add-quiz-menu-button',
                icon: e(PlusIcon, {
                    innerClass: 'create-quiz-plus-icon-color',
                    outerClass: 'create-quiz-plus-icon'
                }),
                label: translate('ADD QUIZ'),
                loading: m.creatingQuiz,
                menuItems: [
                    {
                        keyValue: 'create-quiz',
                        label: translate('Create New'),
                        onClick: c.createQuizClicked
                    },
                    {
                        keyValue: 'import-quiz',
                        label: translate('Import'),
                        onClick: c.importClicked
                    }
                ]
            })
        );
        
        return e('div', {
            id: 'quizzes-container',
            className: 'form',
            children: [
                e('div', {
                    children: headerChildren
                }),
                e('div', {
                    className: 'quizzes-search-bar',
                    children: [
                        e('div', {
                            className: 'quizzes-search-container',
                            children: searchContainerChildren
                        })
                    ]
                }),
                folderNav,
                e('div', {
                    className: quizzesContentClass,
                    children: quizzesContentChildren
                })
            ]
        });
    }
    
}
