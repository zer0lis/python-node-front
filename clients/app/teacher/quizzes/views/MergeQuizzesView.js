import React       from 'react';
import CloseX      from 'CloseX';
import Input       from 'Input';
import RadioButton from 'RadioButton';
import {translate} from 'translator';

let e = React.createElement;

export default class MergeQuizzesView extends React.Component {
    
    componentDidMount() {
        let nameInput = document.getElementById('quizName');
        
        if (nameInput) {
            nameInput.select();
            nameInput.focus();
        }
    }
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        return e('div', {
            id: 'merge-quizzes-modal-background',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'merge-quizzes-modal-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('span', {
                                    className: 'popup-title',
                                    children: translate('Merge Quizzes')
                                }),
                                e('div', {
                                    className: 'cancel-button',
                                    children: [
                                        e(CloseX, {
                                            className: 'cancel-button-x',
                                            color: '#cccccc'
                                        })
                                    ],
                                    onClick: c.cancelClicked
                                })
                            ]
                        }),
                        e('div', {
                            id: 'merge-quizzes-modal-content',
                            className: 'form',
                            children: [
                                e(Input, {
                                    id: 'quizName',
                                    label: translate('Merged Quiz Name'),
                                    maxLength: 255,
                                    value: m.name,
                                    onChange: (event) => c.nameChanged(event.target.value)
                                }),
                                e('span', {
                                    className: 'merge-quizzes-question',
                                    children: translate('Which questions should come first in the merged quiz?')
                                }),
                                e('div', {
                                    children: [
                                        e(RadioButton, {
                                            className: 'merge-quizzes-radio',
                                            label: m.quizzes[0].name,
                                            selected: m.quizzes[0].soc === m.selectedQuiz,
                                            onClick: () => c.quizChanged(m.quizzes[0].soc)
                                        })
                                    ]
                                }),
                                e('div', {
                                    children: [
                                        e(RadioButton, {
                                            className: 'merge-quizzes-radio',
                                            label: m.quizzes[1].name,
                                            selected: m.quizzes[1].soc === m.selectedQuiz,
                                            onClick: () => c.quizChanged(m.quizzes[1].soc)
                                        })
                                    ]
                                }),
                                e('span', {
                                    className: 'merge-quizzes-note',
                                    children: translate('Note: Standards will not transfer to the merged quiz.')
                                }),
                                e('div', {
                                    className: 'merge-quizzes-buttons-container',
                                    children: [
                                        e('button', {
                                            id: 'merge-quizzes-button',
                                            className: 'merge-quizzes-button',
                                            children: translate('MERGE'),
                                            disabled: !m.selectedQuiz,
                                            onClick: c.mergeClicked
                                        }),
                                        e('button', {
                                            className: 'merge-quizzes-button merge-quizzes-cancel-button',
                                            children: translate('CANCEL'),
                                            onClick: c.cancelClicked
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
