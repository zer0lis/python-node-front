import React  from 'react';
import CloseX from 'CloseX';
import Input  from 'Input';
import {translate} from 'translator';

let e = React.createElement;

export default class CopyQuizView extends React.Component {
    
    componentDidMount() {
        let nameInput = document.getElementById('quizName');
        
        if (nameInput) {
            nameInput.select();
            nameInput.focus();
        }
    }
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        return e('div', {
            id: 'copy-quiz-modal-background',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'copy-quiz-modal-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('span', {
                                    className: 'popup-title',
                                    children: translate('Copy Quiz')
                                }),
                                e('div', {
                                    className: 'cancel-button',
                                    children: [
                                        e(CloseX, {
                                            className: 'cancel-button-x',
                                            color: '#cccccc'
                                        })
                                    ],
                                    onClick: c.cancelClicked
                                })
                            ]
                        }),
                        e('div', {
                            id: 'copy-quiz-modal-content',
                            className: 'form',
                            children: [
                                e(Input, {
                                    id: 'quizName',
                                    label: translate('Quiz Name'),
                                    maxLength: 255,
                                    value: m.name,
                                    onChange: (event) => c.nameChanged(event.target.value),
                                    onKeyPress: (event) => {
                                        if (event.key === 'Enter') {
                                            c.copyClicked();
                                        }
                                    }
                                }),
                                e('div', {
                                    className: 'copy-quiz-buttons-container',
                                    children: [
                                        e('button', {
                                            id: 'copyQuizButton',
                                            className: 'copy-quiz-button',
                                            children: translate('COPY'),
                                            onClick: c.copyClicked
                                        }),
                                        e('button', {
                                            className: 'copy-quiz-button copy-quiz-cancel-button',
                                            children: translate('CANCEL'),
                                            onClick: c.cancelClicked
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
