import React     from 'react';
import CloseX    from 'CloseX'
import Input     from 'Input';
import AlertIcon from 'AlertIcon';
import Constants from 'Constants';
import {translate} from 'translator';

let e = React.createElement;

export default class ImportQuizView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            excelChildren = [];
        
        if (m.showProgress) {
            excelChildren.push(
                e('span', {
                    className: 'excel-filename',
                    children: m.fileName
                }),
                e('div', {
                    className: 'progress-bar',
                    children: [
                        e('div', {
                            className: 'progress',
                            style: {width: m.progress + '%'}
                        })
                    ]
                })
            );
        } else {
            excelChildren.push(
                e(Input, {
                    accept: '.xls, .xlsx',
                    disabled: m.importingBySoc,
                    id: 'quiz-upload',
                    label: translate('CHOOSE FILE'),
                    type: 'file',
                    onChange: (event) => c.fileChosen(event.target.files[0])
                }),
                e('a', {
                    className: 'link import-by-excel-link',
                    children: translate('Download Template'),
                    href: window.static_url + 'data/socrativeQuizTemplate.xlsx'
                })
            );
            
            if (m.excelErrors) {
                let errors = m.excelErrors,
                    errorChildren = [];
                
                if (errors[Constants.FILE_TOO_LARGE]) {
                    errorChildren.push(
                        e('li', {
                            className: 'excel-error-text',
                            children: translate('The file size limit is 1 megabyte.')
                        })
                    );
                }
                
                if (errors[Constants.QUIZ_NAME_MISSING]) {
                    errorChildren.push(
                        e('li', {
                            className: 'excel-error-text',
                            children: translate('Your Excel template is missing a quiz name at cell {0}.').format(errors[Constants.QUIZ_NAME_MISSING])
                        })
                    );
                } else if (errors[Constants.QUIZ_NAME_TOO_LONG]) {
                    errorChildren.push(
                        e('li', {
                            className: 'excel-error-text',
                            children: translate('The quiz name at cell {0} must not exceed {1} characters.').format(errors[Constants.QUIZ_NAME_TOO_LONG], Constants.MAX_QUIZ_NAME_LENGTH)
                        })
                    );
                }
                
                if (errors[Constants.QUESTIONS_MISSING]) {
                    errorChildren.push(
                        e('li', {
                            className: 'excel-error-text',
                            children: translate('The quiz has no questions.')
                        })
                    );
                } else if (errors[Constants.TOO_MANY_QUESTIONS]) {
                    errorChildren.push(
                        e('li', {
                            className: 'excel-error-text',
                            children: translate('The quiz must not exceed {0} questions.').format(Constants.EXCEL_QUIZ_MAX_QUESTIONS)
                        })
                    );
                }
                
                if (errors[Constants.QUESTION_TYPE_MISSING]) {
                    errorChildren.push(
                        e('li', {
                            className: 'excel-error-text',
                            children: translate('Please choose a question type at cell(s) {0}.').format(errors[Constants.QUESTION_TYPE_MISSING].join(', '))
                        })
                    );
                }
                
                if (errors[Constants.QUESTION_TEXT_MISSING]) {
                    errorChildren.push(
                        e('li', {
                            className: 'excel-error-text',
                            children: translate('The question text is empty at cell(s) {0}.').format(errors[Constants.QUESTION_TEXT_MISSING].join(', '))
                        })
                    );
                }
                
                if (errors[Constants.QUIZ_QUESTION_TOO_LONG]) {
                    errorChildren.push(
                        e('li', {
                            className: 'excel-error-text',
                            children: translate('The question text at cell(s) {0} exceeds the maximum number of characters ({1}).').format(
                                errors[Constants.QUIZ_QUESTION_TOO_LONG].join(', '),
                                Constants.MAX_QUESTION_LENGTH
                            )
                        })
                    );
                }
                
                if (errors[Constants.QUIZ_ANSWER_TOO_LONG]) {
                    errorChildren.push(
                        e('li', {
                            className: 'excel-error-text',
                            children: translate('The answer text at cell(s) {0} exceeds the maximum number of characters ({1}).').format(
                                errors[Constants.QUIZ_ANSWER_TOO_LONG].join(', '),
                                Constants.MAX_ANSWER_LENGTH
                            )
                        })
                    );
                }
                
                if (errors[Constants.NOT_ENOUGH_ANSWERS]) {
                    errorChildren.push(
                        e('li', {
                            className: 'excel-error-text',
                            children: translate('The multiple-choice question at row(s) {0} must have at least two answers.').format(errors[Constants.NOT_ENOUGH_ANSWERS].join(', '))
                        })
                    );
                }
                
                if (errors[Constants.INVALID_QUESTION_TYPE]) {
                    errorChildren.push(
                        e('li', {
                            className: 'excel-error-text',
                            children: translate('The question type at cell(s) {0} is not valid.').format(errors[Constants.INVALID_QUESTION_TYPE].join(', '))
                        })
                    );
                }
                
                if (errorChildren.length === 0) {
                    errorChildren.push(
                        e('li', {
                            className: 'excel-error-text',
                            children: translate('Please check the file and try again.')
                        })
                    );
                }
                
                excelChildren.push(
                    e('div', {
                        className: 'excel-error-container',
                        children: [
                            e(AlertIcon, {
                                className: 'excel-error-icon'
                            }),
                            e('span', {
                                className: 'excel-error-header',
                                children: translate('Excel Error(s)')
                            }),
                            e('ul', {
                                children: errorChildren
                            })
                        ]
                    })
                );
            }
        }
        
        return e('div', {
            id: 'import-quiz-modal-background',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'import-quiz-modal-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('span', {
                                    className: 'popup-title',
                                    children: translate('Import Quiz')
                                }),
                                e('div', {
                                    className: 'cancel-button',
                                    children: [
                                        e(CloseX, {
                                            className: 'cancel-button-x',
                                            color: '#cccccc'
                                        })
                                    ],
                                    onClick: c.cancelClicked
                                })
                            ]
                        }),
                        e('div', {
                            id: 'import-quiz-modal-content',
                            className: 'form',
                            children: [
                                e('div', {
                                    children: [
                                        e('span', {
                                            className: 'from-another-teacher-text',
                                            children: translate('From Another Teacher')
                                        })
                                    ]
                                }),
                                e('div', {
                                    children: [
                                        e('span', {
                                            className: 'enter-soc-number-text',
                                            children: translate('Enter the "SOC" number of a shared quiz to import it into your account.')
                                        })
                                    ]
                                }),
                                e('div', {
                                    className: 'import-by-soc-container',
                                    children: [
                                        e(Input, {
                                            id: 'socNumber',
                                            maxLength: 255,
                                            onChange: (event) => c.socNumberChanged(event.target.value),
                                            placeholder: translate('Example: SOC-XXXXX'),
                                            status: m.socNumberStatus,
                                            tooltipLocation: 'above',
                                            value: m.socNumber
                                        }),
                                        e('button', {
                                            id: 'importBySocButton',
                                            className: 'import-quiz-button',
                                            children: translate('IMPORT QUIZ'),
                                            disabled: m.importingByExcel,
                                            onClick: c.importClicked
                                        })
                                    ]
                                }),
                                e('div', {
                                    children: [
                                        e('span', {
                                            className: 'from-excel-text',
                                            children: translate('From Excel (xls)')
                                        })
                                    ]
                                }),
                                e('div', {
                                    className: 'import-by-excel-container',
                                    children: excelChildren
                                })
                            ]
                        })
                    ]
                })
            ]
        })
    }
    
}
