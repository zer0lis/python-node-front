class ShareQuizModel {
    
    constructor() {
        this.copied = false; // Whether the quiz URL has been copied.
        this.pageBlurred = false; // Whether the page has been blurred after opening the modal.
        this.quizName = ''; // The name of the quiz being shared.
        this.socNumber = -1; // The SOC number of the quiz being shared.
    }
    
}

module.exports = ShareQuizModel;
