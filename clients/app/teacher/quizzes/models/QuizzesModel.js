'use strict';

let _ = require('underscore'),
    Constants = require('Constants'),
    teacher = require('TeacherModel'),
    user = require('user'),
    request = require('Request');

class QuizzesModel {
    
    constructor() {
        this._allChecked = false; // Whether the "ALL" box is checked. When this is true, all folders and quizzes in the current folder are checked.
        this.bannerIsError = false; // Whether the banner is depicting error (with a red background) or success (with a green background).
        this.bannerSubText = ''; // The optional secondary text to display to the right of this.bannerText in the success/error banner across the top.
        this.bannerText = ''; // The primary text to display in the success/error banner across the top.
        this.bannerTimeoutId = 0; // The id of the timer that counts how long the banner is displayed. Pass this to window.clearTimeout() to stop the timer.
        this.creatingQuiz = false; // Whether a new quiz is being created (network request in progress). When this is true, the "ADD QUIZ" button is disabled and shows the loader.
        this.nonSharableSoc = -1; // The SOC number of a quiz that is not sharable. This is used for the "Sharing Disabled" tooltip.
        this.searchText = ''; // The text in the "Search Quizzes" input.
        this.showBanner = false; // Whether to show the success/error banner across the top.
        this.showSharableTooltip = false; // Whether to show the sharable tooltip.
        this.showTrashTooltip = false; // Whether the trash should show a tooltip indicating that it contains something that matches the search text.
        this._sortColumn = user.getQuizSortColumn() || 'date'; // The column by which quizzes and folders are sorted.
        this._sortDirection = user.getQuizSortDirection() || 'desc'; // The direction in which quizzes and folders are sorted.
        this.waiting = false; // Whether the client is waiting to display the folders/quizzes (usually because a network request in progress).
        this.waitingText = ''; // The text to display instead of the folders/quizzes while the client is waiting.
    }
    
    /*-----------------------*/
    /*    Getters/Setters    */
    /*-----------------------*/
    
    get allChecked() {
        return this._allChecked;
    }
    
    set allChecked(checked) {
        this._allChecked = checked;
        
        if (checked) {
            // Make sure only the visible folders and quizzes are checked.
            for (let folder of this.visibleFolders) {
                folder.checked = true;
            }

            for (let quiz of this.visibleQuizzes) {
                quiz.checked = true;
            }
        } else {
            // Uncheck literally every folder and quiz, not just the visible ones.
            if (teacher.quizFolderNav) {
                for (let folder of teacher.quizFolderNav.idFolderMap.values()) {
                    folder.checked = false;
                }
            }
            
            for (let quiz of teacher.quizzes) {
                quiz.checked = false;
            }
        }
    }
    
    get sortColumn() {
        return this._sortColumn;
    }
    
    set sortColumn(value) {
        this._sortColumn = value;
        user.setQuizSortColumn(value);
    }
    
    get sortDirection() {
        return this._sortDirection;
    }
    
    set sortDirection(value) {
        this._sortDirection = value;
        user.setQuizSortDirection(value);
    }
    
    /*----------------------*/
    /*    Helper Methods    */
    /*----------------------*/
    
    toggleFolder(id) {
        for (let folder of this.visibleFolders) {
            if (folder.id === id) {
                folder.checked = !folder.checked;
                return;
            }
        }
    }
    
    toggleQuiz(id) {
        for (let quiz of this.visibleQuizzes) {
            if (quiz.id === id) {
                quiz.checked = !quiz.checked;
                return;
            }
        }
    }
    
    get anyChecked() {
        for (let folder of this.visibleFolders) {
            if (folder.checked) {
                return true;
            }
        }
        
        for (let quiz of this.visibleQuizzes) {
            if (quiz.checked) {
                return true;
            }
        }
        
        return false;
    }
    
    get canMerge() {
        let checked = 0,
            parentIds = new Map();
        
        for (let quiz of this.visibleQuizzes) {
            if (quiz.checked) {
                checked++;
                parentIds.set(quiz.parent_id, true);
            }
            
            if (checked > 2) {
                return false;
            }
        }
        
        return checked === 2 && parentIds.size === 1;
    }
    
    /**
     * Get an array of the folders that will appear in the
     * content area to the right of the navigation sidebar.
     * @returns {array}
     */
    get visibleFolders() {
        let visible = [];
        
        if (!user.isPro()) {
            return visible;
        }
        
        if (teacher.quizFolderNav) {
            let searchText = this.searchText.toLowerCase();

            if (searchText.length > 0) {
                for (let folder of teacher.quizFolderNav.idFolderMap.values()) {
                    if (folder.name.toLowerCase().indexOf(searchText) !== -1) {
                        if (folder.deleted || teacher.quizFolderNav.hasDeletedAncestor(folder)) {
                            this.showTrashTooltip = true;
                        } else {
                            visible.push(folder);
                        }
                    }
                }
            } else {
                let children = teacher.quizFolderNav.getChildren(teacher.currentQuizFolderId);
                
                for (let folder of children) {
                    if (folder.deleted && teacher.currentQuizFolderId !== Constants.TRASH) {
                        continue;
                    }
                    visible.push(folder); // Push the child folders onto the visible array so it can be sorted separately.
                }
            }
        }
        
        if (this.sortColumn === 'name') {
            visible = _.sortBy(visible, (folder) => {
                return folder.name.toLowerCase();
            });
        } else {
            visible = _.sortBy(visible, 'date');
        }
        
        if (this.sortDirection === 'desc') {
            visible.reverse(); // Underscore sorts ascending by default, so reverse if needed.
        }
        
        return visible;
    }
    
    /**
     * Get an array of the quizzes that will appear in the
     * content area to the right of the navigation sidebar.
     * @returns {array}
     */
    get visibleQuizzes() {
        let visible = [];
        
        for (let quiz of teacher.quizzes) {
            let searchText = this.searchText.toLowerCase();
            
            if (searchText.length > 0) {
                if (quiz.name.toLowerCase().indexOf(searchText) !== -1) {
                    let parent = teacher.quizFolderNav.getFolder(quiz.parent_id);
                    
                    if (quiz.deleted || parent.deleted || teacher.quizFolderNav.hasDeletedAncestor(parent)) {
                        this.showTrashTooltip = true;
                    } else {
                        visible.push(quiz);
                    }
                }
            } else {
                // Handle free users first because they might have downgraded from pro, in
                // which case we need to show all their quizzes in the root Quizzes folder.
                if (!user.isPro() && !quiz.deleted && teacher.currentQuizFolderId === Constants.ROOT_FOLDER) {
                    visible.push(quiz);
                } else if (!quiz.deleted && quiz.parent_id === teacher.currentQuizFolderId) {
                    visible.push(quiz);
                } else if (quiz.deleted && teacher.currentQuizFolderId === Constants.TRASH) {
                    visible.push(quiz);
                }
            }
        }
        
        if (this.sortColumn === 'name') {
            visible = _.sortBy(visible, (quiz) => {
                return quiz.name.toLowerCase();
            });
        } else {
            visible = _.sortBy(visible, 'date');
        }
        
        if (this.sortDirection === 'desc') {
            visible.reverse(); // Underscore sorts ascending by default, so reverse if needed.
        }
        
        return visible;
    }
    
    get checkedItems() {
        let checked = [];
        
        for (let folder of this.visibleFolders) {
            if (folder.checked) {
                checked.push(folder);
            }
        }
        
        for (let quiz of this.visibleQuizzes) {
            if (quiz.checked) {
                checked.push(quiz);
            }
        }
        
        return checked;
    }
    
    get checkedFolders() {
        let checked = [];
        
        for (let folder of this.visibleFolders) {
            if (folder.checked) {
                checked.push(folder);
            }
        }
        
        return checked;
    }
    
    get checkedQuizzes() {
        let checked = [];
        
        for (let quiz of this.visibleQuizzes) {
            if (quiz.checked) {
                checked.push(quiz);
            }
        }
        
        return checked;
    }
    
    getQuizById(id) {
        for (let quiz of teacher.quizzes) {
            if (quiz.id === id) {
                return quiz;
            }
        }
    }
    
    toggleSortDirection() {
        this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    }
    
    purgeQuizzes(quizIds) {
        let quizzes = [];
        
        for (let quiz of teacher.quizzes) {
            let purged = false;
            
            for (let id of quizIds) {
                if (quiz.id === id) {
                    purged = true;
                    break;
                }
            }
            
            if (!purged) {
                quizzes.push(quiz);
            }
        }
        
        teacher.quizzes = quizzes;
    }
    
    /*-----------------------*/
    /*    Client Requests    */
    /*-----------------------*/
    
    importSharedQuiz(socNumber, callback) {
        request.post({
            url: `${window.teacher_service}/v3/quizzes/import?type=${Constants.SOC_TYPE}`,
            data: {
                parentId: Constants.ROOT_FOLDER,
                socNumber: socNumber.replace(/[A-Za-z$-]/g, '')
            },
            success: () => {
                callback(true);
            },
            error: () => {
                callback(false);
            }
        });
    }
    
    createQuiz(callback) {
        let parentId = teacher.currentQuizFolderId;
        
        if (parentId === Constants.NO_FOLDER || parentId === Constants.TRASH) {
            parentId = Constants.ROOT_FOLDER;
        }
        
        request.post({
            url: `${window.teacher_service}/v3/quizzes/`,
            data: {
                parentId: parentId
            },
            success: (data) => {
                callback(data.soc_number);
            },
            error: () => {
                callback();
            }
        });
    }
    
    deleteOrRestoreItems(shouldDelete, items, callback) {
        if (this.waiting) {
            return;
        }
        
        this.waiting = true;
        
        let folderIds = [],
            quizIds = [];
        
        for (let item of items) {
            if (item.soc) {
                quizIds.push(item.id);
            } else {
                folderIds.push(item.id);
            }
        }
        
        request.put({
            url: `${window.teacher_service}/v3/quizzes/move/`,
            data: {
                folderIds: folderIds,
                quizIds: quizIds,
                parentId: null,
                task: shouldDelete ? Constants.DELETE_TASK : Constants.RESTORE_TASK
            },
            success: () => {
                this.waiting = false;
                callback(shouldDelete, folderIds, quizIds);
            },
            error: () => {
                this.waiting = false;
                callback();
            }
        });
    }
    
    purgeItems(items, callback) {
        if (this.waiting) {
            return;
        }
        
        this.waiting = true;
        
        let folderIds = [],
            quizIds = [];
        
        for (let item of items) {
            if (item.soc) {
                quizIds.push(item.id);
            } else {
                folderIds.push(item.id);
            }
        }
        
        request.put({
            url: `${window.teacher_service}/v3/quizzes/purge/`,
            data: {
                folderIds: folderIds,
                quizIds: quizIds
            },
            success: () => {
                this.waiting = false;
                callback(folderIds, quizIds);
            },
            error: () => {
                this.waiting = false;
                callback();
            }
        });
    }
    
}

module.exports = QuizzesModel;
