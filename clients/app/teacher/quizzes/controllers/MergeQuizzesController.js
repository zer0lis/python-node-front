import React             from 'react';
import ReactDOM          from 'react-dom';
import MergeQuizzesModel from 'MergeQuizzesModel';
import view              from 'MergeQuizzesView';
import utils             from 'Utils';
import dots from 'Dots';

let model = null;

class MergeQuizzesController {
    
    open(props) {
        model = new MergeQuizzesModel();
        model.quizzes = props.quizzes;
        model.mergeCallback = props.mergeCallback;
        
        let container = document.createElement('div');
        container.id = 'merge-quizzes-modal-container';
        document.body.appendChild(container);
        
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('merge-quizzes-modal-container'),
            () => {
                if (!model.pageBlurred) {
                    model.pageBlurred = true;
                    utils.blurPage();
                }
            }
        );
    }
    
    nameChanged(name) {
        model.name = name;
        controller.renderView();
    }
    
    quizChanged(quiz) {
        model.selectedQuiz = quiz;
        controller.renderView();
    }
    
    mergeClicked() {
        dots.start('merge-quizzes-button');
        model.mergeQuizzes(controller.quizzesMerged);
    }
    
    quizzesMerged(quiz) {
        controller.cancelClicked();
        model.mergeCallback(quiz);
    }
    
    cancelClicked() {
        let container = document.getElementById('merge-quizzes-modal-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
        utils.unblurPage();
    }
    
}

let controller = new MergeQuizzesController();
module.exports = controller;
