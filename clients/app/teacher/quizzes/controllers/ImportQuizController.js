import React from 'react';
import ReactDOM from 'react-dom';
import ImportQuizModel from 'ImportQuizModel';
import view from 'ImportQuizView';
import dots from 'Dots';
import utils from 'Utils';
import {translate} from 'translator';

let model = null;

class ImportQuizController {
    
    open(props) {
        model = new ImportQuizModel();
        model.importResultCallback = props.importResult;
        
        let container = document.createElement('div');
        container.id = 'import-quiz-modal-container';
        document.body.appendChild(container);
        
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('import-quiz-modal-container'),
            () => {
                if (!model.pageBlurred) {
                    model.pageBlurred = true;
                    utils.blurPage(document.getElementById('import-quiz-modal-box'));
                }
            }
        );
    }
    
    cancelClicked() {
        let container = document.getElementById('import-quiz-modal-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
        utils.unblurPage();
    }
    
    socNumberChanged(value) {
        model.socNumber = value;
        model.socNumberStatus = {};
        controller.renderView();
    }
    
    importClicked() {
        if (!model.isValidSocNumber()) {
            model.socNumberStatus = {error: true};
            controller.renderView();
        } else {
            dots.start('importBySocButton');
            model.importingBySoc = true;
            model.importBySoc(controller.importBySocResult);
        }
    }
    
    importBySocResult(quiz) {
        dots.stop();
        
        model.importingBySoc = false;
        
        if (quiz) {
            controller.cancelClicked();
            model.importResultCallback(quiz);
        } else {
            model.socNumberStatus = {
                error: true,
                message: translate('Please check the SOC number and try again.')
            };
            
            controller.renderView();
        }
    }
    
    fileChosen(file) {
        model.importingByExcel = true;
        model.excelErrors = null;
        model.showProgress = true;
        model.progress = 0;
        model.fileName = file.name || '';
        model.importByExcel(file, controller.fileUploadProgress, controller.fileUploadResult);
        controller.renderView();
    }
    
    fileUploadProgress(progress) {
        if (progress > 100) {
            progress = 100;
        }
        
        model.progress = progress;
        controller.renderView();
    }
    
    fileUploadResult(errors, quiz) {
        if (errors) {
            model.importingByExcel = false;
            model.showProgress = false;
            model.excelErrors = errors;
            controller.renderView();
        } else {
            controller.cancelClicked();
            model.importResultCallback(quiz);
        }
    }
    
}

let controller = new ImportQuizController();
module.exports = controller;
