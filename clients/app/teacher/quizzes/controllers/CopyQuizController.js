import React from 'react';
import ReactDOM from 'react-dom';
import QuizModel from 'QuizModel';
import view from 'CopyQuizView';
import utils from 'Utils';
import {translate} from 'translator';
import dots from 'Dots'

let model = null;

class CopyQuizController {
    
    render(props) {
        model = new QuizModel();
        model.socNumber = props.socNumber;
        model.name = `${props.name} (${translate('copy')})`;
        model.callback = props.callback;
        
        let container = document.createElement('div');
        container.id = 'copy-quiz-modal-container';
        document.body.appendChild(container);
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('copy-quiz-modal-container'),
            () => {
                if (!model.pageBlurred) {
                    model.pageBlurred = true;
                    utils.blurPage();
                }
            }
        );
    }
    
    nameChanged(newName) {
        model.name = newName;
        controller.renderView();
    }
    
    copyClicked() {
        model.name = model.name.trim();
        dots.start('copyQuizButton');
        model.copyQuiz(controller.copyResult);
    }
    
    copyResult(quiz) {
        controller.cancelClicked();
        model.callback(quiz);
    }
    
    cancelClicked() {
        let container = document.getElementById('copy-quiz-modal-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
        utils.unblurPage();
    }
    
}

let controller = new CopyQuizController();
module.exports = controller;
