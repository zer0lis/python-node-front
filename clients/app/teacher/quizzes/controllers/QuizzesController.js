import React       from 'react';
import ReactDOM    from 'react-dom';
import Backbone    from 'backbone';
import _           from 'underscore';
import header      from 'HeaderController';
import QuizzesModel from 'QuizzesModel';
import view        from 'QuizzesView';
import importModal from 'ImportQuizController';
import mergeModal  from 'MergeQuizzesController';
import moveModal from 'MoveController';
import goPro  from 'GoProController';
import copyModal   from 'CopyQuizController';
import shareModal  from 'ShareQuizController';
import createFolderModal from 'CreateFolderController';
import renameFolderModal from 'RenameFolderController';
import popup       from 'PopupController';
import user        from 'user';
import teacher     from 'TeacherModel';
import platform    from 'Platform';
import Constants from 'Constants';
import {translate} from 'translator';

let router = null,
    model = null;

class QuizzesController {
    
    doQuizzes(teacherRouter) {
        router = teacherRouter;
        header.render('quizzes');
        
        model = new QuizzesModel();
        model.allChecked = false;
        
        if (teacher.showSharedImportSuccess) {
            teacher.showSharedImportSuccess = false;
            model.showBanner = true;
            model.bannerText = translate('Quiz Imported!');
            controller.startBannerCountdown();
        } else if (teacher.showSharedImportFailure) {
            teacher.showSharedImportFailure = false;
            model.showBanner = true;
            model.bannerText = translate('Import Failed.');
            model.bannerSubText = translate('Check the quiz URL and try again.');
            model.bannerIsError = true;
            controller.startBannerCountdown();
        } else if (teacher.showQuizSaved) {
            teacher.showQuizSaved = false;
            model.showBanner = true;
            model.bannerText = translate('Quiz Saved!');
            controller.startBannerCountdown();
        }
        
        if (teacher.currentQuizFolderId === Constants.NO_FOLDER) {
            teacher.currentQuizFolderId = Constants.ROOT_FOLDER;
        }
        
        if (teacher.shouldFetchQuizzes) {
            if (!model.waiting) {
                teacher.fetchQuizzes(controller.quizzesFetched);
            }
            model.waiting = true;
            model.waitingText = translate('Loading Quizzes...');
        }
        
        controller.renderView();
    }
    
    quizzesFetched() {
        model.waiting = false;
        teacher.shouldFetchQuizzes = false;
        controller.renderView();
    }
    
    doImportSharedQuiz(teacherRouter, sharedSocNumber) {
        router = teacherRouter;
        header.render('quizzes');
        model = new QuizzesModel();
        model.waiting = true;
        model.waitingText = translate('Importing Shared Quiz...');
        controller.renderView();
        model.importSharedQuiz(sharedSocNumber, controller.importSharedQuizResult);
    }
    
    importSharedQuizResult(success) {
        user.unsetSharedSocNumber();
        if (success) {
            teacher.showSharedImportSuccess = true;
        } else {
            teacher.showSharedImportFailure = true;
        }
        teacher.shouldFetchQuizzes = true;
        router.routeToQuizzesAndReplace();
    }
    
    renderView() {
        let route = Backbone.history.fragment;
        if (/quizzes/.test(route) || /import-quiz/.test(route)) {
            ReactDOM.render(
                React.createElement(
                    view, {
                        controller: controller,
                        model: model,
                        isPro: user.isPro(),
                        folderNavOpen: user.isQuizFolderNavOpen(),
                        quizFolderNav: teacher.quizFolderNav,
                        currentFolderId: teacher.currentQuizFolderId,
                        
                        // Pass these getters as properties so
                        // they only execute once per render:
                        anyChecked: model.anyChecked,
                        canMerge: model.canMerge,
                        visibleFolders: model.visibleFolders,
                        visibleQuizzes: model.visibleQuizzes,
                        showTrashTooltip: model.showTrashTooltip, // This value can be changed by the model's visibleFolders/visibleQuizzes
                                                                  // getters, so it must come after them when passed to the view.
                    }
                ),
                document.getElementById('main-content')
            );
        }
    }
    
    createQuizClicked() {
        model.creatingQuiz = true;
        controller.renderView();
        model.createQuiz(controller.createResult);
    }
    
    createResult(socNumber) {
        if (socNumber) {
            router.routeToEditQuiz(socNumber);
        } else {
            model.creatingQuiz = false;
            controller.renderView();
            popup.render({
                title: translate('Unknown Error'),
                message: translate('An unknown error occurred. Please try again later.')
            });
        }
    }
    
    importClicked() {
        importModal.open({
            importResult: controller.importResult
        });
    }
    
    importResult(quiz) {
        if (quiz) {
            quiz.date = new Date(quiz.date);
            teacher.quizzes.push(quiz);
            
            model.showBanner = true;
            model.bannerText = translate('Quiz Imported!');
            
            if (model.searchText.length > 0) {
                controller.searchTextChanged(''); // If there was search text, the quiz was imported into the root.
            } else {
                controller.renderView();
            }
            
            controller.startBannerCountdown();
        } else {
            popup.render({
                title: translate('Unknown Error'),
                message: translate('An unknown error occurred. Please try again later.')
            });
        }
    }
    
    searchTextChanged(searchText) {
        model.allChecked = false;
        model.showTrashTooltip = false;
        
        if (searchText.length === 0) {
            teacher.currentQuizFolderId = Constants.ROOT_FOLDER;
        } else {
            teacher.currentQuizFolderId = Constants.NO_FOLDER;
        }
        
        model.searchText = searchText;
        controller.renderView();
    }
    
    clearSearchClicked() {
        controller.searchTextChanged('');
        var searchInput = document.getElementById('quizzes-search-input');
        if (searchInput) {
            searchInput.focus();
        }
    }
    
    toggleFolderNavClicked() {
        user.setQuizFolderNavOpen(!user.isQuizFolderNavOpen());
        controller.renderView();
    }
    
    expanderClicked(folderId) {
        teacher.quizFolderNav.toggleExpanded(folderId);
        controller.renderView();
    }
    
    navFolderClicked(folderId) {
        model.allChecked = false;
        model.searchText = '';
        model.showTrashTooltip = false;
        teacher.currentQuizFolderId = folderId;
        teacher.quizFolderNav.expandAllAncestors(folderId);
        controller.renderView();
    }
    
    navTrashClicked() {
        model.allChecked = false;
        model.searchText = '';
        model.showTrashTooltip = false;
        teacher.currentQuizFolderId = Constants.TRASH;
        controller.renderView();
    }
    
    deleteClicked() {
        let checkedItems = model.checkedItems;
        
        if (checkedItems.length === 0) {
            return;
        }
        
        model.deleteOrRestoreItems(true, checkedItems, controller.deleteOrRestoreResult);
        model.allChecked = false;
        model.waiting = true;
        model.waitingText = translate('Deleting...');
        controller.renderView();
    }
    
    deleteOrRestoreResult(deleted, folderIds, quizIds) {
        if (!(folderIds && quizIds)) {
            popup.render({
                title: translate('Unknown Error'),
                message: translate('An unknown error occurred. Please try again later.')
            });
        } else {
            let count = 0,
                now = new Date();
            
            for (let id of folderIds) {
                let folder = teacher.quizFolderNav.getFolder(id);
                count++;
                folder.checked = false;
                folder.date = now;
                folder.deleted = deleted;
            }
            
            for (let id of quizIds) {
                for (let quiz of teacher.quizzes) {
                    if (quiz.id === id) {
                        count++;
                        quiz.checked = false;
                        quiz.date = now;
                        quiz.deleted = deleted;
                    }
                }
            }
            
            let bannerSubText = '';
            
            if (deleted) {
                bannerSubText = count > 1 ? translate('The items are now in your trash.') : translate('The item is now in your trash.');
            }
            
            model.showBanner = true;
            model.bannerText = deleted ? translate('Successfully Deleted.') : translate('Successfully Restored!');
            model.bannerSubText = bannerSubText;
            controller.startBannerCountdown();
        }
        
        controller.renderView();
    }
    
    mergeClicked() {
        if (!user.isPro()) {
            goPro.render({router: router});
            return;
        }
        
        mergeModal.open({
            quizzes: model.checkedQuizzes,
            mergeCallback: controller.mergeResult
        });
    }
    
    mergeResult(quiz) {
        if (quiz) {
            teacher.quizzes.push(quiz);
            
            model.allChecked = false;
            model.showBanner = true;
            model.bannerText = translate('Quizzes Merged!');
            
            controller.navFolderClicked(quiz.parent_id);
            controller.startBannerCountdown();
        } else {
            popup.render({
                title: translate('Unknown Error'),
                message: translate('An unknown error occurred. Please try again later.')
            });
        }
    }
    
    moveClicked() {
        if (!user.isPro()) {
            goPro.render({router: router});
            return;
        }
        
        let checkedItems = model.checkedItems;
        
        if (checkedItems.length === 0) {
            return;
        }
        
        moveModal.open({
            callback: controller.moveResult,
            folderNavModel: teacher.quizFolderNav,
            startingFolder: teacher.quizFolderNav.getFolder(teacher.currentQuizFolderId),
            folderIds: model.checkedFolders.map(folder => folder.id),
            quizIds: model.checkedQuizzes.map(quiz => quiz.id),
            type: Constants.QUIZ_FOLDER
        });
    }
    
    moveResult(result) {
        if (result) {
            let now = new Date();
            
            for (let id of result.folderIds) {
                let folder = teacher.quizFolderNav.getFolder(id);
                folder.checked = false;
                folder.date = now;
                teacher.quizFolderNav.moveFolder(id, result.newParentId);
            }
            
            for (let id of result.quizIds) {
                for (let quiz of teacher.quizzes) {
                    if (quiz.id === id) {
                        quiz.checked = false;
                        quiz.date = now;
                        quiz.parent_id = result.newParentId;
                    }
                }
            }
            
            model._allChecked = false; // Make sure the "ALL" box is unchecked without looping over all folders/quizzes again.
        }
        
        model.showBanner = true;
        model.bannerIsError = result === null;
        model.bannerText = result ? translate('Successfully Moved!') : translate('Unknown Error.');
        controller.renderView();
        controller.startBannerCountdown();
    }
    
    createFolderClicked() {
        if (!user.isPro()) {
            goPro.render({router: router});
            return;
        }
        
        let currentFolder = teacher.quizFolderNav.getFolder(teacher.currentQuizFolderId);
        
        createFolderModal.open({
            callback: controller.createFolderResult,
            parentId: teacher.currentQuizFolderId,
            type: Constants.QUIZ_FOLDER
        });
    }
    
    createFolderResult(id, name) {
        if (!(id && name)) {
            popup.render({
                title: translate('Unknown Error'),
                message: translate('An unknown error occurred. Please try again later.')
            });
            return;
        }
        
        let now = new Date();
        
        let parentId = teacher.currentQuizFolderId;
        
        if (parentId === Constants.NO_FOLDER || parentId === Constants.TRASH) {
            parentId = Constants.ROOT_FOLDER;
        }
        
        teacher.quizFolderNav.addFolder({
            children: [],
            date: now,
            deleted: false,
            expanded: false,
            id: id,
            lastUpdated: now,
            name: name,
            parentId: parentId,
            type: Constants.QUIZ_FOLDER,
            userId: user.getId()
        }, teacher.currentQuizFolderId);
        
        model.showBanner = true;
        model.bannerText = translate('Folder Created!');
        
        if (model.searchText.length > 0) {
            controller.searchTextChanged(''); // If there was search text, the folder was created in the root.
        } else {
            controller.renderView();
        }
        
        controller.startBannerCountdown();
    }
    
    restoreClicked() {
        let checkedItems = model.checkedItems;
        
        if (checkedItems.length === 0) {
            return;
        }
        
        model.deleteOrRestoreItems(false, checkedItems, controller.deleteOrRestoreResult);
        model.allChecked = false;
        model.waiting = true;
        model.waitingText = translate('Restoring...');
        controller.renderView();
    }
    
    purgeClicked() {
        let checkedItems = model.checkedItems;
        
        if (checkedItems.length === 0) {
            return;
        }
        
        popup.render({
            title: translate('Delete Forever?'),
            message: checkedItems.length > 1 ? translate('Are you sure you want to permanently delete the selected items and their contents?') : translate('Are you sure you want to permanently delete the selected item and its contents?'),
            buttonText: translate('Delete'),
            cancelText: translate('Cancel'),
            buttonClicked: () => {
                controller.purgeConfirmed(checkedItems);
            }
        });
    }
    
    purgeConfirmed(checkedItems) {
        model.purgeItems(checkedItems, controller.purgeResult);
        model.allChecked = false;
        model.waiting = true;
        model.waitingText = translate('Deleting Forever...');
        controller.renderView();
    }
    
    purgeResult(folderIds, quizIds) {
        if (!(folderIds && quizIds)) {
            popup.render({
                title: translate('Unknown Error'),
                message: translate('An unknown error occurred. Please try again later.')
            });
        } else {
            let purgingIdFolderMap = new Map(),
                purgingIdQuizMap = new Map();
            
            if (folderIds.length > 0) {
                for (let id of folderIds) {
                    if (!purgingIdFolderMap.has(id)) {
                        let folderinMap = teacher.quizFolderNav.idFolderMap.get(id);
                        teacher.quizFolderNav.depthFirstPreOrder(folderinMap, folderinMap.level, (folder) => {
                            purgingIdFolderMap.set(folder.id, true);
                        });
                    }
                }
                
                for (let id of purgingIdFolderMap.keys()) {
                    teacher.quizFolderNav.purgeChild(id);
                    teacher.quizFolderNav.idFolderMap.delete(id);
                }
            }
            
            for (let id of purgingIdFolderMap.keys()) {
                for (let quiz of teacher.quizzes) {
                    if (quiz.parent_id === id) {
                        purgingIdQuizMap.set(quiz.id, true);
                    }
                }
            }
            
            for (let id of quizIds) {
                purgingIdQuizMap.set(id, true);
            }
            
            if (purgingIdQuizMap.size > 0) {
                model.purgeQuizzes(Array.from(purgingIdQuizMap.keys()));
            }
            
            model.showBanner = true;
            model.bannerText = translate('Successfully Deleted Forever.');
            controller.startBannerCountdown();
        }
        
        controller.renderView();
    }
    
    startBannerCountdown() {
        if (model.bannerTimeoutId) {
            window.clearTimeout(model.bannerTimeoutId);
            model.bannerTimeoutId = 0;
        }
        model.bannerTimeoutId = window.setTimeout(() => {
            controller.clearBanner();
            controller.renderView();
        }, Constants.BANNER_TIMEOUT * 1.25);
    }
    
    clearBanner() {
        model.bannerText = '';
        model.bannerSubText = '';
        model.showBanner = false;
        model.bannerIsError = false;
    }
    
    allChanged() {
        model.allChecked = !model.allChecked;
        controller.renderView();
    }
    
    columnClicked(column) {
        if (column === model.sortColumn) {
            model.toggleSortDirection();
        } else if (model.sortColumn !== 'name' && column === 'name') {
            model.sortDirection = 'asc';
        } else if (model.sortColumn !== 'date' && column === 'date') {
            model.sortDirection = 'desc';
        }
        model.sortColumn = column;
        controller.renderView();
    }
    
    folderToggled(id) {
        model.toggleFolder(id);
        controller.renderView();
    }
    
    quizToggled(id) {
        model.toggleQuiz(id);
        controller.renderView();
    }
    
    renameFolderClicked(id, name) {
        renameFolderModal.open({
            callback: controller.renameFolderResult,
            id: id,
            name: name
        });
    }
    
    renameFolderResult(id, newName) {
        if (!(id && newName)) {
            popup.render({
                title: translate('Unknown Error'),
                message: translate('An unknown error occurred. Please try again later.')
            });
            return;
        }
        
        teacher.quizFolderNav.renameFolder(id, newName);
        model.showBanner = true;
        model.bannerText = translate('Folder Renamed!');
        controller.renderView();
        controller.startBannerCountdown();
    }
    
    copyClicked(socNumber, name) {
        copyModal.render({
            socNumber: socNumber,
            name: name,
            callback: controller.copyResult
        });
    }
    
    copyResult(quiz) {
        if (quiz) {
            teacher.quizzes.push(quiz);
            
            model.showBanner = true;
            model.bannerText = translate('Quiz Copied!');
            
            if (model.searchText.length > 0) {
                controller.searchTextChanged(''); // If there was search text, the quiz was copied into the root.
            } else {
                controller.renderView();
            }
            
            controller.startBannerCountdown();
        } else {
            popup.render({
                title: translate('Unknown Error'),
                message: translate('An unknown error occurred. Please try again later.')
            });
        }
    }
    
    downloadClicked(soc) {
        if (!platform.isIos) {
            let hiddenIFrameID = 'reportDownloader',
                iframe = document.getElementById(hiddenIFrameID);
            
            if (!iframe) {
                iframe = document.createElement('iframe');
                iframe.id = hiddenIFrameID;
                iframe.style.display = 'none';
                document.body.appendChild(iframe);
            }
            
            iframe.src = `${window.backend_host}/quizzes/api/activity-report/?dt=dl&di=${soc}&rt=unanswered`;
        }
    }
    
    shareClicked(sharable, name, soc) {
        if (sharable) {
            shareModal.render({
                quizName: name,
                socNumber: soc
            });
        } else {
            if (!model.showSharableTooltip) {
                model.showSharableTooltip = true;
                model.nonSharableSoc = soc;
                controller.renderView();
                controller.addClickListener();
            }
        }
    }
    
    addClickListener() {
        if (platform.isIos) {
            document.body.style.cursor = 'pointer'; // This makes any element respond to click events in iOS.
        }
        document.body.addEventListener('click', controller.bodyClicked);
    }
    
    bodyClicked() {
        if (platform.isIos) {
            document.body.style.cursor = 'default';
        }
        _.defer(() => {
            document.body.removeEventListener('click', controller.bodyClicked);
            model.showSharableTooltip = false;
            model.nonSharableSoc = -1;
            controller.renderView();
        });
    }
    
}

let controller = new QuizzesController();
module.exports = controller;
