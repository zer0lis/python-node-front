import React from 'react';
import ReactDOM from 'react-dom';
import ShareQuizModel from 'ShareQuizModel';
import view from 'ShareQuizView';
import utils from 'Utils';

let model = null;

class ShareQuizController {
    
    render(props) {
        model = new ShareQuizModel();
        model.quizName = props.quizName;
        model.socNumber = props.socNumber;
        
        let container = document.createElement('div');
        container.id = 'share-quiz-modal-container';
        document.body.appendChild(container);
        
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('share-quiz-modal-container'),
            () => {
                if (!model.pageBlurred) {
                    model.pageBlurred = true;
                    utils.blurPage();
                }
            }
        );
    }
    
    copyClicked() {
        model.copied = true;
        controller.renderView();
    }
    
    cancelClicked() {
        let container = document.getElementById('share-quiz-modal-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
        utils.unblurPage();
    }
    
}

let controller = new ShareQuizController();
module.exports = controller;
