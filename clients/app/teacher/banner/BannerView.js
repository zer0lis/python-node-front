import React from 'react';
import InfoIcon from 'InfoIcon';
import CloseX from 'CloseX';
import {translate} from 'translator';

let e = React.createElement;

export default class BannerView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        return e('div', {
            id: 'info-banner',
            children: [
            	e(InfoIcon, {
            		className: 'info-banner-icon'
            	}),
            	e(CloseX, {
            		className: 'close-banner-icon',
            		onClick: c.closeBannerClicked
            	}),
            	e('div', {
            		className: 'banner-content clearfix',
            		children: [
            			e('div', {
            				className: 'align-left content',
            				children: [
            					e('h1', {
            						children: m.banner_title
            					}),
            					e('div', {
            						children: [
                                        m.banner_content,
                                        e('a', {
                                            className: 'link learn-more',
                                            children: translate('Learn More'),
                                            onClick: c.learnMoreClicked,
                                            style: {display: m.type !== 'none' ? 'inline-block' : 'none'}
                                        })
                                    ]
            					})
            				]
            			}),
            			e('a', {
                            className: 'link remind-later',
                            children: translate('Remind me later'),
                            onClick: c.remindMeClicked
                        })
            		]
            	})
            ]
        });
    }
    
}
