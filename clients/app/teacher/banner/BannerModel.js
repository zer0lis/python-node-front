'use strict';

let request = require('Request');

class BannerModel {
    
    constructor() {
        this.banner_title = '';  // The title of the banner.
        this.banner_content = ''; // The content of the banner.
        this.type = -1; // The type of the banner
        this.banner_id = -1; // The banner id, for further update operations.
        this.banner_url = '' // The url of the button from the banner, which can be empty.
    }
    
    setData(data) {
        this.banner_title = data.title;
        this.banner_content = data.content;
        this.type = data.action_type;
        this.banner_id = data.id;
        this.banner_url = data.url;
    }
    
    hasBanner() {
        return this.banner_title &&
               this.banner_content &&
               this.banner_title.trim().length > 0 &&
               this.banner_content.trim().length > 0;
    }
    
    hasStatus() {
        return this.status && this.status.trim().length > 0;
    }
    
    updateStatus(type, callback) {
        this.status = type;
        
        request.put({
            url: `${window.backend_host}/users/api/user-sys-msg/${this.banner_id}/`,
            data: {
                status: this.status
            },
            
            // TODO .... By design the Request module requires success and error callbacks.
            //           We should always handle the results of HTTP requests! These are
            //           placeholders until the teacher banner has a better design:
            
            success: () => {},
            error: () => {},
            
            complete() {
                if (callback) {
                    callback()
                }
            }
        });
    }
    
    getBanner(callback) {
        request.get({
            url: `${window.backend_host}/users/api/user-sys-msg/`,
            success: (data) => {
                this.setData(data);
                callback(true);
            },
            error: () => {
                callback(false);
            }
        });
    }
    
}

module.exports = new BannerModel();
