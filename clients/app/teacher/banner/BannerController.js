import React from 'react';
import ReactDOM from 'react-dom';
import model from 'BannerModel';
import view from 'BannerView';
import header from 'HeaderController';
import goPro from 'GoProController';

let router = null;

class BannerController {
    
    initialize(teacherRouter) {
        router = teacherRouter;
        
        if (model.hasBanner() && !model.hasStatus()) {
            controller.render();
        }
    }
    
    render() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('banner-container')
        );
    }
    
    closeBanner(type) {
        let banner = document.getElementById('info-banner');
        
        if (banner) {
            TweenLite.to(banner, 1, {
                marginTop: '-200px', ease: Quint.easeOut, onComplete: () => {
                    if (type) {
                        model.updateStatus(type, controller.initialize);
                    }
                }
            });
        }
        
        header.hideMobileMenus();
    }
    
    closeBannerClicked() {
        controller.closeBanner('dismissed');
    }
    
    getBannerInfo() {
        model.getBanner(controller.bannerCallback);
    }
    
    bannerCallback(success) {
       if (success && model.hasBanner()) {
           controller.render();
           return;
       }

       controller.closeBanner();
    }
    
    learnMoreClicked() {
        controller.closeBanner('read');
        
        if (model.type === 'goProPopup') {
            goPro.render({router: router});
        }

        if (model.type === 'externalResource') {
            let win = window.open(model.banner_url, '_blank');
            win.focus();
        }

    }
    
    remindMeClicked() {
        controller.closeBanner('remind_me');
    }
    
}

let controller = new BannerController();
module.exports = controller;
