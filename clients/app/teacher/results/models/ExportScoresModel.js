'use strict';

let translate = require('translator').translate;

import Constants from 'Constants';
import request   from 'Request';

class ExportScoresModel {
    
    constructor() {
        this.reset();
        this._gradecamAdded = false;
        this._gradecamLoaded = false;
        this._gradecamUrl = 'https://downloads.gradecam.com/';
        this._settingsFetched = false;
        this._showSettings = true;
        this._exportScoreId = 0;
        this._exportKey = 8;
        this._nextLine = Constants.ENTER;
        this._speed = Constants.MEDIUM;
        this._method = Constants.PERCENTAGE;
        this._savedExportKey = null;
        this._savedNextLine = null;
        this._savedSpeed = null;
        this._savedMethod = null;
        this._userProfileFetched = false;
        this._userState = '';
        this._userSchool = '';
        this._userDistrict = '';
        this._userBillMonth = 1;
    }
    
    reset() {
        this._numericScores = [];
        this._percentageScores = [];
        this._installTitle = translate('INSTALL GRADECAM PLUGIN');
        this._installText = translate('To export scores you must install the GradeCam plugin.');
        this._installSteps = [
            translate('Click the link below to download the Gradecam plugin.'),
            translate('Double-click the plugin and follow the instructions.'),
            translate('Restart your web browser.')
        ];
        this._installLabel = translate('DOWNLOAD GRADECAM');
        this._download = true;
        this._pageBlurred = false;
        this._loading = true;
        this._exportKey = this._savedExportKey || this._exportKey;
        this._nextLine = this._savedNextLine || this._nextLine;
        this._speed = this._savedSpeed || this._speed;
        this._method = this._savedMethod || this._method;
    }
    
    get numericScores() {return this._numericScores}
    set numericScores(value) {this._numericScores = value}
    
    get percentageScores() {return this._percentageScores}
    set percentageScores(value) {this._percentageScores = value}
    
    get gradecamAdded() {return this._gradecamAdded}
    set gradecamAdded(value) {this._gradecamAdded = value}
    
    get gradecamLoaded() {return this._gradecamLoaded}
    set gradecamLoaded(value) {this._gradecamLoaded = value}
    
    get gradecamUrl() {return this._gradecamUrl}
    set gradecamUrl(value) {this._gradecamUrl = value}
    
    get installTitle() {return this._installTitle}
    set installTitle(value) {this._installTitle = value}
    
    get installText() {return this._installText}
    set installText(value) {this._installText = value}
    
    get installSteps() {return this._installSteps}
    set installSteps(value) {this._installSteps = value}
    
    get installLabel() {return this._installLabel}
    set installLabel(value) {this._installLabel = value}
    
    get download() {return this._download}
    set download(value) {this._download = value}
    
    get pageBlurred() {return this._pageBlurred}
    set pageBlurred(value) {this._pageBlurred = value}
    
    get loading() {return this._loading}
    set loading(value) {this._loading = value}
    
    get settingsFetched() {return this._settingsFetched}
    set settingsFetched(value) {this._settingsFetched = value}
    
    get showSettings() {return this._showSettings}
    set showSettings(value) {this._showSettings = value}
    
    get exportScoreId() {return this._exportScoreId}
    set exportScoreId(value) {this._exportScoreId = value}
    
    get exportKey() {return this._exportKey}
    set exportKey(value) {this._exportKey = value}
    
    get nextLine() {return this._nextLine}
    set nextLine(value) {this._nextLine = value}
    
    get speed() {return this._speed}
    set speed(value) {this._speed = value}
    
    get method() {return this._method}
    set method(value) {this._method = value}
    
    get savedExportKey() {return this._savedExportKey}
    set savedExportKey(value) {this._savedExportKey = value}
    
    get savedNextLine() {return this._savedNextLine}
    set savedNextLine(value) {this._savedNextLine = value}
    
    get savedSpeed() {return this._savedSpeed}
    set savedSpeed(value) {this._savedSpeed = value}
    
    get savedMethod() {return this._savedMethod}
    set savedMethod(value) {this._savedMethod = value}
    
    get userProfileFetched() {return this._userProfileFetched}
    set userProfileFetched(value) {this._userProfileFetched = value}
    
    get userState() {return this._userState}
    set userState(value) {this._userState = value}
    
    get userSchool() {return this._userSchool}
    set userSchool(value) {this._userSchool = value}
    
    get userDistrict() {return this._userDistrict}
    set userDistrict(value) {this._userDistrict = value}
    
    get userBillMonth() {return this._userBillMonth}
    set userBillMonth(value) {this._userBillMonth = value}
    
    get hasExportSettings() {
        return this.exportScoreId !== 0;
    }
    
    get gradecamInstalled() {
        let typerOptions = gradecam.getTyperOptions();
        return typerOptions && typerOptions.length > 0 && typerOptions[0].installed;
    }
    
    get settingsChanged() {
        return this.savedExportKey != this.exportKey ||
               this.savedNextLine  != this.nextLine  ||
               this.savedSpeed     != this.speed     ||
               this.savedMethod    != this.method;
    }
    
    get methodText() {
        this.method == Constants.NUMERIC ? translate('Numeric') : translate('Percentaged');
        return translate("Place your cursor in the first scoring field of your grade book and press the F{0} key. If you're using a Mac, you may need to hold down both the fn and F{0} keys.").format(this.exportKey);
    }
    
    fetchSettings(callback) {
        request.get({
            url: `${window.backend_host}/lecturers/api/score-settings/`,
            success: (settings) => {
                if (settings) {
                    if (settings.id)        {this.exportScoreId  = settings.id}
                    if (settings.key)       {this.savedExportKey = this.exportKey = settings.key}
                    if (settings.next_line) {this.savedNextLine  = this.nextLine  = settings.next_line}
                    if (settings.speed)     {this.savedSpeed     = this.speed     = settings.speed}
                    if (settings.method)    {this.savedMethod    = this.method    = settings.method}
                }
                this.settingsFetched = true;
            },
            error: (response) => {
                console.log('Error fetching score export settings:', response);
            },
            complete: () => {
                callback();
            }
        });
    }
    
    fetchUserProfile(callback) {
        request.get({
            url: `${window.backend_host}/lecturers/teacher-profile/`,
            success: (data) => {
                if (data) {
                    this.userState = data.org_state || data.country;
                    this.userSchool = data.school_name || '';
                    this.userDistrict = data.district_name || '';
                    if (data.expiration_date) {
                        let expirationDate = new Date(data.expiration_date * 1000);
                        this.userBillMonth = expirationDate.getMonth() + 1;
                    }
                }
                this.userProfileFetched = true;
            },
            error: (response) => {
                console.log('Error fetching user profile for score export:', response);
            },
            complete: () => {
                callback();
            }
        });
    }
    
    saveSettings(callback) {
        let url = `${window.backend_host}/lecturers/api/score-settings/`,
            type = 'POST';
        
        if (this.exportScoreId !== 0) {
            url += `${this.exportScoreId}/`;
            type = 'PUT';
        }
        
        request.send(type, {
            url: url,
            data: {
                key: this.exportKey,
                next_line: this.nextLine,
                speed: this.speed,
                prefix: '',
                method: this.method
            },
            success: () => {
                this.savedExportKey = this.exportKey;
                this.savedNextLine  = this.nextLine;
                this.savedSpeed     = this.speed;
                this.savedMethod    = this.method;
            },
            error: (response) => {
                console.log('Error saving/updating score export settings:', response);
            },
            complete: () => {
                callback();
            }
        });
    }
    
}

module.exports = new ExportScoresModel();
