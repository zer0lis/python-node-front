var Backbone       = require('backbone'),
    _              = require('underscore'),
    activity       = require('activity'),
    spaceRaceTeams = require('space-race-teams'),
    Quiz           = require('models/quiz-model');


// Results is just a collection of functions used to organize the 
// response data for use in different views
var Results = Backbone.Model.extend({
    
    getCurrentQuestion: function(questionNumber, live) {
        var questionIndex = questionNumber - 1;

        var quizType = live ? 'quiz' : 'final_result';
        if (!activity.has(quizType) || _.isUndefined(questionIndex) || questionIndex === null) {
            return null;
        }

        var questions = activity.get(quizType).questions;
        if (questionIndex > questions.length - 1) {
            return null;
        }

        var question = questions[questionIndex];
        if (_.isUndefined(question)) {
            return null;
        }
        
        if (!_.isUndefined(question.resources[0])) {
            var file = question.resources[0].get('url');
            if (file.indexOf('s3.amazonaws.com') == -1 &&
                file.indexOf('uploads.socrative.com') == -1) {
                question.resources[0].set('url', 'https://uploads.socrative.com/' + file);
            }
        }

        if (_.isFunction(question.toJSON)) {
            question = question.toJSON(true);
        }

        return question;
    },

    buildQuestionDetail: function(questionNumber, isLive) {
        var question = this.getCurrentQuestion(questionNumber, isLive);

        if (question === null) {
            return null;
        }
        
        // Filter out responses for different questions than the one we're showing.
        var responses = _.filter(activity.getResponses(isLive), (response) => {
            return response.question === question.question_id && response.hidden === false;
        });

        var showNames = true;
        var showAnswers = true;

        var studentNameProp = isLive ? 'student_names' : 'final_result_student_names';
        var activityTypeProp = isLive ? 'activity_type' : 'final_result_activity_type';
        var studentsInResponseSet = _.uniq(_.pluck(responses, 'user_uuid'));
        var context = {
            question: question,
            responses: responses,
            totalStudents: Object.keys(activity.get(studentNameProp)).length || studentsInResponseSet.length,
            showNames: showNames,
            showAnswers: showAnswers,
            activityType: activity.get(activityTypeProp)
        };
        context.studentsAnswered = studentsInResponseSet.length;

        
        // MC and TF respones are compiled from all the class's responses
        // FR just shows the responses inline, so they don't need to be modified
        if (question.type !== 'FR') {
            
            // Initialize all the response arrays
            var responsesByAnswerId = {};
            _.each(question.answers, function(answer) {
                responsesByAnswerId[answer.id] = [];
            });
            
            var totalResponses = 0;
            _.each(responses, function(response){
                _.each(response.selection_answers, function(selection){
                    responsesByAnswerId[selection.answer_id].push(response);
                    totalResponses++;
                });

                response._showName = showNames;
                response._showAnswer = showAnswers;
            });

            // Aggregate all the responses and calculate percentages
            context.aggregate_responses = [];
            var i = 0;
            _.each(question.answers, function(answer){
                var aggregateResponse = {};
                aggregateResponse.percent_selected = responsesByAnswerId[answer.id].length / totalResponses;
                aggregateResponse.order = answer.order;
                aggregateResponse.is_correct = answer.is_correct;
                aggregateResponse.text = answer.text;
                
                context.aggregate_responses[i] = aggregateResponse;
                ++i;
            });
            // if we are on a single question MC
            if (context.activityType === activity.NO_REPORT && context.question.type === 'MC') {

                context.aggregate_responses = _.sortBy(context.aggregate_responses, 'order');
                
                // if we have a quick question MC quiz, which has 5 answers and all with empty responses
                // sort those answers by their order
                var ans = _.countBy(question.answers, function(a){ return a.text === ''  });
                if( question.answers.length !== 5 || ans['true'] !== 5 ) {
                    
                    // sort the answers; but to do that we first need to groupd them 
                    var response = _.groupBy(context.aggregate_responses, function(a){ 
                        return isNaN(a.percent_selected) || a.percent_selected === 0 
                    });
                    
                    // the sorting should be done for options:
                    // 		with answers: by their percentage
                    // 		without answers: by order
                    response["false"] = _.sortBy(response["false"], 'percent_selected');
                    response["false"].reverse();

                    context.aggregate_responses = [];
                    context.aggregate_responses.push.apply(context.aggregate_responses, response['false']); 
                    context.aggregate_responses.push.apply(context.aggregate_responses, response['true']);
                }
                
            } else {
                context.aggregate_responses = _.sortBy(context.aggregate_responses, function(response) {
                    return response.order;
                });
            }
            
        } else {
            activity.setResponseNames(isLive, context.responses);
            
            if (!isLive) { // Fix SOC-1731
                let students = activity.get('final_result_students');
                for (let resp of context.responses) {
                    if (!resp.student_name) {
                        for (let student of students) {
                            if (student.user_uuid && student.user_uuid === resp.user_uuid) {
                                resp.student_name = student.name;
                                break;
                            }
                        }
                    }
                }
            }
        }
        
        return context;
    },

    buildSpaceRaceContext: function() {
        if (!activity.has('quiz')) {
            return null;
        }

        var responsesByTeamColor = [];
        _.each(activity.getResponses(true), (response) => {
            var responses = responsesByTeamColor[response.team];
            if( _.isUndefined(responses) ) {
                responsesByTeamColor[response.team] = [response];
            } else {
                responses.push(response);
            }
        });

        var teamCountSetting;
        var teamIconSetting;

        if (activity.has('settings')) {
            teamCountSetting = activity.get('settings').team_count;
            teamIconSetting = activity.get('settings').icon_type;
        }
        
        var teamCountSettingValue = teamCountSetting || 0;
        var teamIconSettingValue = teamIconSetting || '';

        var context = {
            teams: []
        };
        
        // we only care about questions that could be correct
        var questions = _.filter(activity.get('quiz').toJSON(true).questions, function(q) {
            return q.has_correct_answer;
        }),
        
        maxPercent = 1,
        correctAnswerIncrement = maxPercent / questions.length;
        

        for( var i = 0; i < teamCountSettingValue; ++i ) {
            var spaceRaceTeam = spaceRaceTeams[i];
            var responses = responsesByTeamColor[i];

            // compute answered percentages
            var completePercentage = _.reduce(questions, function(percent, question) {
                // Did anyone on this team get this question correct?
                var correctAnswer = _.findWhere(responses, {
                    is_correct: true,
                    question: question.question_id
                });

                if( !_.isUndefined(correctAnswer) ) {
                    return percent + correctAnswerIncrement;
                }
                return percent;
            }, 0);

            completePercentage = completePercentage.clamp(0, maxPercent);
            
            context.teams.push({
                name: spaceRaceTeam.name,
                color: spaceRaceTeam.color,
                secondaryColor: spaceRaceTeam.secondaryColor,
                completePercentage: completePercentage
            });
        }
        
        context.iconType = teamIconSettingValue;
        
        // console.log("Space Race Context", context);

        return context;
    }
});

module.exports = Results;
