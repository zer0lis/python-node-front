var Backbone  = require('backbone'),
    _         = require('underscore'),
    user      = require('user'),
    Quiz      = require('quiz-model'),
    activity  = require('activity'),
    room      = require('room'),
    utils     = require('Utils');

var ResultsTableModel = Backbone.Model.extend({
    
    initialize: function() {
        this.reset();
    },

    reset: function() {
        this.set('hideStudentResponses', true);
        this.set('hideStudentName', true);
    },
    
    getGradecamError: function() {
        return this.get('gradecamError');
    },
    
    setGradecamError: function(value) {
        return this.set('gradecamError', value);
    },
    
    toggleSortDirection: function() {
        this.set('sortDirection', this.get('sortDirection') === 'asc' ? 'desc' : 'asc');
    },
    
    buildResultsTable: function(isLive) {
        let quizType = isLive ? 'quiz' : 'final_result',
            quiz = new Quiz(activity.get(quizType)),
            questions = activity.get(quizType) ? activity.get(quizType).questions : '',
            gradable = 0,
            answers = {};
        
        for (let question of questions) {
            if (question.get('has_correct_answer')) {
                gradable++;
            }
            question = question.toJSON(true);
            for (let answer of question.answers) {
                answers[answer.id] = {
                    text: answer.text,
                    order: answer.order,
                    type: question.type
                };
            }
        }
        
        let responsesByUser = {};
        for (let response of activity.getResponses(isLive)) {
            let userResponses = responsesByUser[response.user_uuid];
            if (!userResponses) {
                responsesByUser[response.user_uuid] = [response];
            } else {
                userResponses.push(response);
            }
            
            if (response.selection_answers.length > 0) {
                response.text = _.map(response.selection_answers, (selection) => {
                    let answer = answers[selection.answer_id];
                    if (_.isUndefined(answer)) {
                        return '';
                    }
                    if (answer.type === 'MC') {
                        return utils.convertToLetter(answers[selection.answer_id].order);
                    }
                    return answer.text;
                }).join(', ');
            } else if (response.text_answers.length > 0 ) {
                response.text = _.pluck(response.text_answers, 'answer_text').join(', ');
            }
        }
        
        let students = [],
            studentNames = {},
            here = {},
            hereCount = 0;

        if (isLive) {
            if (room.hasRoster()) {
                for (let student of room.getStudents()) {
                    let uuid = student.user_uuid || student.id; // A student has no user_uuid until he logs in.
                    studentNames[uuid] = `${student.last_name}, ${student.first_name}`;
                    here[uuid] = student.logged_in;
                    if (student.logged_in) {
                        hereCount++;
                    }
                }
            } else if (activity.has('student_names')) {
                studentNames = activity.get('student_names');
            }
        } else if (activity.has('final_result_student_names')) {
            if (activity.get('final_result_students')) {
                for (let student of activity.get('final_result_students')) {
                    let uuid = student.user_uuid || student.student_id;
                    studentNames[uuid] = student.name;
                    here[uuid] = student.present;
                    hereCount++;
                }
            }
        }
        
        for (let uuid in studentNames) {
            if (!studentNames.hasOwnProperty(uuid)) {
                continue;
            }
            
            let score = 0,
                finalProgress = 0,
                answeredGraded = false;
        
            if (!_.isUndefined(responsesByUser[uuid])) {
                _.each(responsesByUser[uuid], (resp) => {
                    if (resp.is_correct === true) {
                        score++;
                    }
                    if (resp.is_correct === true || resp.is_correct === null || resp.is_correct === false) {
                        finalProgress++;
                    }
                    if (resp.is_correct !== null) {
                        answeredGraded = true;
                    }
                });
            }
        
            if (gradable > 0 && !isLive) {
                answeredGraded = true;
            }
            
            students.push({
                here: here[uuid],
                name: studentNames[uuid],
                grade: score / gradable,
                numericScore: score,
                uuid: uuid,
                finalProgress: finalProgress / questions.length,
                answeredGraded: answeredGraded
            });
        }
        
        for (let question of questions) {
            var incorrectOrCorrectAnswers = 0.0;
            var averageCorrect = 0.0;
            
            for (let student of students) {
                let response = _.find(responsesByUser[student.uuid], (resp) => resp.question === question.id);
                if (!_.isUndefined(response)) {
                    if (response.is_correct === true) {
                        incorrectOrCorrectAnswers += 1.0;
                        averageCorrect += 1.0;
                    } else if (response.is_correct === false) {
                        incorrectOrCorrectAnswers += 1.0;
                    }
                }
            }
        
            averageCorrect /= incorrectOrCorrectAnswers;
            question.set('averageCorrect', averageCorrect);
            question.set('incorrectOrCorrectAnswers', incorrectOrCorrectAnswers);
        }
        
        students = _.sortBy(students, (student) => student.name.toLowerCase());
        
        if (this.get('sortDirection') !== 'asc') {
            students.reverse();
        }
        
        activity.setResponseNames(isLive, responsesByUser);
        
        this.set('students', students);
        this.set('quiz', quiz.toJSON(true));
        this.set('responses', responsesByUser);
        this.set('us_k12', user.get('us_k12'));
        this.set('hereCount', hereCount);
    },

    getHideStudentResponses: function() {
        if (this.get('mode') === 'live') {
            return activity.get('hideStudentResponses');
        } else {
            return this.get('hideStudentResponses');
        }
    },

    getHideStudentName: function() {
        if (this.get('mode') === 'live') {
            return activity.get('hideStudentName');
        } else {
            return this.get('hideStudentName');
        }
    },

    toggleStudentResponses: function() {
        if (this.get('mode') === 'live') {
            activity.set('hideStudentResponses', !activity.get('hideStudentResponses'));
        } else {
            this.set('hideStudentResponses', !this.get('hideStudentResponses'));
        }
    },

    toggleStudentName: function() {
        if (this.get('mode') === 'live') {
            activity.set('hideStudentName', !activity.get('hideStudentName'));
        } else {
            this.set('hideStudentName', !this.get('hideStudentName'));
        }
    }

});

module.exports = new ResultsTableModel();
