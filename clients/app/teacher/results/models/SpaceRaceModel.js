// TODO .... This model has hard dependency on Backbone's listening methods. When the activity model is refactored
//           not to depend on Backbone, we should also refactor this model into a proper class.

import Backbone from 'backbone';
import activity from 'activity';
import Results from 'results';

let SpaceRaceModel = Backbone.Model.extend({
    
    initialize: function() {
        this.setMinHeight(-1);
    },
    
    startListening: function(callback) {
        this.listenTo(activity, 'change', callback); // Backbone method.
        this.set('isListening', true);
    },
    
    isListening: function() {
        return this.get('isListening');
    },
    
    endListening: function() {
        this.stopListening(); // Backbone method.
        this.set('isListening', false);
    },
    
    buildContext: function(callback) {
        activity.getCurrentQuiz(() => {
            let results = new Results();
            this.set('context', results.buildSpaceRaceContext());
            callback();
        });
    },
    
    setFinishedByCountdown: function(finishedByCountdown) {
        this.set('finishedByCountdown', finishedByCountdown);
    },
    
    getFinishedByCountdown: function() {
        return this.get('finishedByCountdown');
    },
    
    setReportSettingsRoute: function(route) {
        this.set('reportSettingsRoute', route);
    },
    
    getReportSettingsRoute: function() {
        return this.get('reportSettingsRoute');
    },
    
    setReportSettingsCallback: function(callback) {
        this.set('reportSettingsCallback', callback);
    },
    
    getReportSettingsCallback: function() {
        return this.get('reportSettingsCallback');
    },

    setMinHeight: function(value) {
        this.set('minHeight', value);
    },

    getMinHeight: function() {
        return this.get('minHeight');
    }
    
});

module.exports = new SpaceRaceModel();
