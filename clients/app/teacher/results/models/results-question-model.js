var Backbone  = require('backbone'),
    Results   = require('results'),
    request   = require('Request'),
    localUser = require('user'),
    activity  = require('activity'),
    room = require('room');

var ResultsQuestionModel = Backbone.Model.extend({

    initialize: function() {
        // Set these during initialization so they persist during the session.
        this.set('showAnswers', true);
        this.set('showNames', false);
        this.set('darkTheme', false);
        this.set('us_k12', localUser.get('us_k12'));
    },

    buildQuestionDetail: function(questionNumber, live) {
        var results = new Results();
        var questionDetail = results.buildQuestionDetail(questionNumber, live);
        if (questionDetail !== null) {
            this.set('question', questionDetail.question);
            this.set('studentsAnswered', questionDetail.studentsAnswered);
            this.set('totalStudents', questionDetail.totalStudents);
            this.set('responses', questionDetail.responses);
            this.set('aggregateResponses', questionDetail.aggregate_responses);
        } else {
            this.set('error', true);
        }
    },

    goToQuestion: function(number, callback) {
        let questions = activity.get('quiz').questions,
            index = number - 1;
        
        index = index.clamp(0, questions.length - 1);
        
        let data = new FormData();
        data.append('room_name', room.get('name'));
        data.append('state_data', index);
        
        request.post({
            url: `${window.backend_host}/rooms/api/update-activity-instance-state/`,
            data: data,
            success: () => {
                callback(number);
            },
            error: (response) => {
                console.error(response);
                callback();
            }
        });
    },

    startQuestion: function(questionType, callback) {
        activity.startQuickQuestion({
            type: questionType,
            require_names: false,
            callback: callback
        });
    }

});

module.exports = new ResultsQuestionModel();
