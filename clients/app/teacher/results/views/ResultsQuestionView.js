import React from 'react';
import _ from 'underscore';
import utils from 'Utils';
import uuid from 'uuid';
import ActivityImage from 'ActivityImage';
import LaunchButton from 'LaunchButton';
import {translate} from 'translator';

let e = React.createElement;

class NavButtons extends React.Component {
    
    render() {
        return e('div', {
            className: 'buttons-container clearfix',
            children: [
                e('button', {
                    id: 'previous-button',
                    className: this.props.showBackToResultsButton ? 'tab-button special' : 'tab-button',
                    style: {visibility: this.props.showPreviousButton ? 'visible' : 'hidden'},
                    children: translate('PREVIOUS'),
                    onClick: this.props.previousClicked
                }),
                e('button', {
                    id: 'back-to-live-results-button',
                    className: 'tab-button',
                    style: {display: this.props.showBackToResultsButton ? 'inline-block' : 'none'},
                    children: translate('BACK TO RESULTS TABLE'),
                    onClick: this.props.backToResultsClicked
                }),
                e('button', {
                    id: 'next-button',
                    className: (this.props.showBackToResultsButton || this.props.showPreviousButton) ? 'tab-button special' : 'tab-button',
                    style: {display: this.props.showNextButton ? 'inline-block' : 'none'},
                    children: translate('NEXT'),
                    onClick: this.props.nextClicked
                })
            ]
        });
    }
    
}

class QuestionParagraph extends React.Component {
    
    render() {
        let children = [];
        
        if (this.props.showQuestionNumber) {
            children.push(
                e('div', {
                    className: 'question-number',
                    children: e('h1', {
                        className: 'no-top-margin-question-number',
                        children: this.props.question.order
                    })
                })
            );
        }
        
        children.push(
            e('pre', {
                dangerouslySetInnerHTML: {__html: this.props.question.question_text}
            })
        );
        
        return e('div', {
            className: 'question-paragraph',
            children: children
        });
    }
    
}

class QuestionDisplay extends React.Component {
    
    render() {
        let children = [];
        
        if (this.props.imageUrl.length > 0) {
            children.push(
                e(ActivityImage, {
                    className: 'teacher-activity-image',
                    imageUrl: utils.convertImageUrl(this.props.imageUrl) || this.props.imageUrl
                })
            );
        }
        
        children.push(e(QuestionParagraph, this.props));
        
        return e('div', {
            id: 'lr-question-display-container',
            children: [
                e('div', {
                    id: 'question-display',
                    children: [
                        e('div', {
                            className: this.props.question.type.toLowerCase() + '-question clearfix',
                            key: `question-display-${this.props.question.question_id}`,
                            children: children
                        })
                    ]
                })
            ]
        });
    }
    
}

class ActionButtons extends React.Component {
    
    render() {
        let children = [];
        
        if (this.props.allowToggleHowDo   ||
            this.props.allowToggleAnswers ||
            this.props.allowStartVote     ||
            this.props.allowToggleNames) {
            let buttons = [];
            
            if (this.props.allowToggleHowDo) {
                buttons.push(
                    e('button', {
                        id: 'howd-we-do-button',
                        className: 'button button-primary button-large',
                        children: translate("How'd we do?"),
                        onClick: this.props.toggleHowDo
                    })
                );
            }
            
            if (this.props.allowStartVote) {
                buttons.push(
                    e('button', {
                        id: 'convert-to-survey-button',
                        className: 'button button-primary button-large',
                        children: translate('Start Vote'),
                        onClick: this.props.startVote
                    }),
                    ' ' // This preserves whitespace after the button (https://github.com/facebook/react/issues/1643).
                );
            }
            
            if (this.props.allowToggleAnswers) {
                buttons.push(
                    e('button', {
                        className: 'button button-primary button-large',
                        children: this.props.showAnswers ? translate('Hide Answers') : translate('Show Answers'),
                        onClick: this.props.toggleAnswers
                    }),
                    ' ' // This preserves whitespace after the button (https://github.com/facebook/react/issues/1643).
                );
            }
            
            if (this.props.allowToggleNames) {
                buttons.push(
                    e('button', {
                        className: 'button button-primary button-large',
                        children: this.props.showNames ? translate('Hide Names') : translate('Show Names'),
                        onClick: this.props.toggleNames
                    })
                );
            }
            
            children.push(
                e('div', {
                    id: 'show-hide-names-container',
                    className: 'align-left',
                    children: buttons
                })
            );
        }
        
        children.push(
            e('div', {
                id: 'question-results-table-header',
                className: 'align-left',
                children: [
                    e('span', {
                        className: 'student-count',
                        children: this.props.studentCountText
                    }),
                    e('span', {
                        className: 'student-count-label',
                        children: this.props.studentCountLabelText
                    })
                ]
            })
        );
        
        if (this.props.allowThemeSwitcher) {
            children.push(
                e('button', {
                    id: 'switch-theme',
                    className: 'button',
                    'data-icon': 'n',
                    onClick: this.props.switchTheme
                })
            );
        }
        
        return e('div', {
            className: 'clearfix action-buttons-container',
            children: children
        });
    }
    
}

class LetterResponse extends React.Component {
    
    getLetter(number) {
        let letters = String.fromCharCode('A'.charCodeAt(0) + number - 1);
        
        if (number > 26) {
            let baseChar = ('A').charCodeAt(0);
            letters  = '';
            
            do {
                number -= 1;
                letters = String.fromCharCode(baseChar + (number % 26)) + letters;
                number = (number / 26) >> 0;
            } while (number > 0);
        }
        return letters;
    }

    getPercent() {
        let percentage = 0,
            percentageLabel = '';
        
        if (!_.isUndefined(this.props.response.percent_selected) && !_.isNaN(this.props.response.percent_selected)) {
            percentage = (this.props.response.percent_selected * 100).toFixed(0);
            percentageLabel = percentage + '%';
        }
        
        return percentageLabel;
    }

    render() {
        let children = [],
            correctClass = '',
            correctPercentClass = '';
        
        if (this.props.showCorrect) {
            correctClass = this.props.response.is_correct ? 'correct' : 'incorrect';
            correctPercentClass = (this.props.response.is_correct || this.props.response.is_correct === null) ? 'correct-percent' : 'incorrect-percent';
            
            // SOC-252: Due to some legacy quizzes, we perform an extra check to set the correct class only if the answer is set as correct
            if (this.props.question.has_correct_answer) {
                correctClass = (!this.props.response.is_correct || this.props.response.is_correct === null) ? 'incorrect' : 'correct';
                correctPercentClass = (!this.props.response.is_correct || !this.props.response.is_correct === null) ? 'incorrect-percent' : 'correct-percent';
            }
            
            let percentSelected = this.props.response.percent_selected * 95;
            
            if (isNaN(percentSelected)) {
                percentSelected = 0;
            }
            
            children.push(
                e('div', {
                    className: 'percentage-selected ' + correctPercentClass,
                    style: {width: percentSelected + '%'}
                }),
                e('span', {
                    className: 'percentage-label',
                    children: this.getPercent()
                })
            );
        }
        
        children.push(
            e('h3', {
                className: correctClass,
                children: this.getLetter(this.props.response.order)
            }),
            e('div', {
                className: 'results-answer-text',
                children: e('pre', {
                    dangerouslySetInnerHTML: {__html: this.props.question.type === 'TF' ? translate(this.props.response.text) : this.props.response.text}
                })
            })
        );
        
        return e('div', {
            className: this.props.question.type.toLowerCase() + '-selection-row',
            children: children
        });
    }
    
}

class ShortAnswerResponse extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            showThisAnswer: this.props.showAnswers
        };
    }
    
    componentWillReceiveProps(nextProps) {
        if (this.props.showAnswers !== nextProps.showAnswers) {
            this.setState({
                showThisAnswer: nextProps.showAnswers
            });
        }
    }
    
    render() {
        let responseProps = {
            id: 'student'+this.props.response.id+'-response',
            className: 'student-response'
        };
        
        if (this.state.showThisAnswer) {
            responseProps.children = _.unescape(this.props.response.text_answers[0].answer_text);
        } else {
            responseProps.dangerouslySetInnerHTML = {__html: '&#x25CF;&#x25CF;&#x25CF;&#x25CF;&#x25CF;'};
        }
        
        let children = [
            e('div', {
                className: 'fr-response-line',
                children: [
                    e('span', {
                        id: 'student'+this.props.response.id+'-name',
                        className: 'student-name',
                        children: this.props.response.student_name + ': ',
                        style: {display: this.props.showNames ? 'inline' : 'none'}
                    }),
                    e('span', responseProps)
                ]
            })
        ];
        
        if (this.props.allowRemoveAnswers) {
            children.push(
                e('button', {
                    className: 'remove-free-response-button button button-small',
                    children: translate('Remove'),
                    onClick: () => this.props.removeClicked(this.props.response.id)
                })
            );
        } else {
            children.push(
                e('button', {
                    className: 'button button-small',
                    children: this.state.showThisAnswer ? translate('hide answer') : translate('show answer'),
                    onClick: () => this.setState({showThisAnswer: !this.state.showThisAnswer})
                })
            );
        }
        
        return e('div', {
            className: 'fr-response-block',
            children: children
        });
    }
    
}

class QuestionResults extends React.Component {
    
    render() {
        let resultsContentClass = '',
            children = [];
        
        if (this.props.letterResponses) {
            for (let response of this.props.aggregateResponses) {
                children.push(
                    e(LetterResponse, _.extend({}, this.props, {response: response}))
                );
            }
        } else {
            resultsContentClass = 'fr-response-container';
            
            for (let response of this.props.responses.reverse()) {
                children.push(
                    e(ShortAnswerResponse, _.extend({}, this.props, {response: response}))
                );
            }
            
            if (this.props.responses.length < 1) {
                children.push(
                    e('p', {
                        className: 'no-answers',
                        children: translate('No answers yet')
                    })
                );
            }
        }
        
        return e('div', {
            id: 'lr-question-results-container',
            children: [
                e('div', {
                    id: 'question-results',
                    children: [
                        e(ActionButtons, this.props),
                        e('div', {
                            id: 'results',
                            children: e('div', {
                                id: 'results-content',
                                className: resultsContentClass,
                                children: children
                            })
                        })
                    ]
                })
            ]
        });
    }
    
}

class Explanation extends React.Component {
    
    render() {
        let children = [
            e('button', {
                id: 'less-explanation-button',
                className: 'button button-link-style',
                dangerouslySetInnerHTML: {__html: this.props.showExplanation ? '&#x25B2; ' + translate('hide explanation') : '&#x25BC; ' + translate('show explanation')},
                ref: (button) => this.toggleExplanationButton = button,
                onClick: () => {
                    this.toggleExplanationButton.blur(); // Otherwise the button stays blue.
                    this.props.toggleExplanation();
                }
            })
        ];
        
        if (this.props.showExplanation) {
            let explanation = this.props.question.explanation;
            
            if (_.isUndefined(explanation) || explanation.length < 1) {
                explanation = translate('You have not added an explanation for this question.');
            }
            
            children.push(
                e('div', {
                    id: 'question-explanation-display',
                    children: e('pre', {
                        dangerouslySetInnerHTML: {__html: explanation}
                    })
                })
            );
        }
        
        return e('div', {
            children: children
        });
    }
    
}

class QuestionContainer extends React.Component {
    
   render() {
       let children = [];
       
       if (this.props.question.question_text.length > 0 || this.props.imageUrl.length > 0) {
           children.push(e(QuestionDisplay, this.props));
       }
       
       children.push(e(QuestionResults, this.props));
       
       if (this.props.allowExplanation) {
           children.push(e(Explanation, this.props));
       }
       
       return e('div', {
           className: 'lr-question-container' + (this.props.darkTheme ? ' dark-theme' : ''),
           children: children
       });
   }
    
}

class QuickQuestionButtons extends React.Component {
    
    render() {
        return e('div', {
            children: [
                e('div', {
                    className: 'quick-question-container lr-view clearfix',
                    children: [
                        e('h2', {
                            children: translate('ANOTHER QUESTION')
                        }),
                        e(LaunchButton, {
                            className: 'mc-button',
                            content: 'MC',
                            key: uuid.v4(),
                            label: translate('Multiple Choice'),
                            onClick: this.props.mcClicked
                        }),
                        e(LaunchButton, {
                            className: 'tf-button',
                            content: 'TF',
                            key: uuid.v4(),
                            label: translate('True / False'),
                            onClick: this.props.tfClicked
                        }),
                        e(LaunchButton, {
                            className: 'sa-button',
                            content: 'SA',
                            key: uuid.v4(),
                            label: translate('Short Answer'),
                            onClick: this.props.saClicked
                        })
                    ]
                })
            ]
        });
    }
    
}

export default class ResultsQuestionView extends React.Component {
    
    render() {
        let children = [
            e('h2', {
                className: 'title',
                children: [
                    e('span', {}, this.props.quizTitle),
                    e('span', {
                        className: 'standard-display',
                        }, this.props.quizStandard)
                ]
            }),
            e('div', {
                className: 'align-right button-container',
                children: [
                    e('button', {
                        className: 'button-primary button-large pill button',
                        children: [
                            e('span', {
                                children: this.props.live ? translate('FINISH') : translate('REPORT')
                            })
                        ],
                        type: 'button',
                        onClick: this.props.mainButtonAction
                    })
                ]
            })
        ];

        if (this.props.showNavButtons) {
            children.push(e(NavButtons, this.props));
        }
        
        children.push(e(QuestionContainer, this.props));
        
        if (this.props.showQuickQuestionButtons) {
            children.push(e(QuickQuestionButtons, this.props));
        }
        
        return e('div', {
            id: 'live-results-main-content',
            children: children
        });
    }
    
}
