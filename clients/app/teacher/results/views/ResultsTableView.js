import React from 'react';
import _ from 'underscore';
import ReportsIcon from 'ReportsIcon';
import LoggedInIcon from 'LoggedInIcon';
import LoggedOutIcon from 'LoggedOutIcon';
import ExportIcon from 'ExportIcon';
import Tooltip from 'Tooltip';
import SortDownIcon from 'SortDownIcon';
import SortUpIcon from 'SortUpIcon';
import {translate} from 'translator';
// import platform from 'Platform'; // Uncomment this when the powers that be so demand.

let e = React.createElement;

class HereColumn extends React.Component {
    
    render() {
        let hereCells = [];

        for (let student of this.props.students) {
            let Icon = student.here ? LoggedInIcon : LoggedOutIcon;
            
            hereCells.push(
                e('div', {
                    className: 'here-cell clearfix' + (student.here ? '' : ' here-cell-logged-out'),
                    children: [
                        e(Icon, {
                            svgClass: 'here-cell-icon',
                            pathClass: 'here-icon-path'
                        })
                    ]
                })
            );
        }
        
        return e('div', {
            className: 'here-column clearfix' + (this.props.showHereColumn || this.props.showHereFinalColumn ? '' : ' hidden'),
            children: [
                e('div', {
                    className: 'column-title clearfix here-column-title',
                    children: translate('Here')
                }),
                hereCells,
                e('div', {
                    className: 'result-row row-total here-total',
                    children: e('p', {
                        children: this.props.hereCount
                    })
                })
            ]
        });
    }
    
}

class NamesHeader extends React.Component {
    
    render() {
        let SortIcon = this.props.sortDirection === 'asc' ? SortUpIcon : SortDownIcon;
        
        return e('div', {
            className: 'column-title clearfix',
            children: [
                e('span', {
                    id: 'student-name-title',
                    children: [
                        e('span', {
                            children: translate('Name')
                        }),
                        e(SortIcon, {
                            className: 'results-sort-icon'
                        })
                    ],
                    onClick: this.props.nameSortChanged
                }),
                e('span', {
                    id: 'student-progress-title',
                    children: [
                        e('select', {
                            id: 'student-progress-sort',
                            value: this.props.resultType,
                            onChange: (event) => this.props.resultTypeChanged(event.target.value),
                            children: [
                                e('option', {
                                    children: translate('Progress') + ' (%)',
                                    key: 'Progress',
                                    value: 'progress-result-type'
                                }),
                                e('option', {
                                    children: translate('Score') + ' (%)',
                                    key: 'Score%',
                                    value: 'percentage-score-result-type'
                                }),
                                e('option', {
                                    children: translate('Score') + ' (#)',
                                    key: 'Score#',
                                    value: 'numeric-score-result-type'
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}

class StudentNameCell extends React.Component {
    
    render() {
        let value = null;
        
        if (this.props.resultType === 'percentage-score-result-type') {
            if (isNaN(this.props.grade) || !this.props.answeredGraded) {
                value = '';
            } else {
                value = (this.props.grade * 100).toFixed(0) + '%';
            }
        } else if (this.props.resultType === 'numeric-score-result-type') {
            value = this.props.answeredGraded ? this.props.numericScore : '';
        } else {
            value = (this.props.progress * 100).toFixed(0) + '%';
        }
        
        return e('div', {
            className:'result-row clearfix',
            children: [
                e('div', {
                    className: 'status-column' + (this.props.finished ? ' finished' : '')
                }),
                e('div', {
                    className: 'student-row-name',
                    children: [
                        e('p', {
                            dangerouslySetInnerHTML: {__html: this.props.studentName}
                        })
                    ]
                }),
                e('div', {
                    className: 'percent-complete',
                    children: value
                })
            ]
        })
    }
    
}

class NamesColumn extends React.Component {
    
    render() {
        let studentNameCells = [];
        
        for (let student of this.props.students) {
            studentNameCells.push(e(StudentNameCell, {
                grade: student.grade,
                numericScore: student.numericScore,
                finished: this.props.studentsFinished ? this.props.studentsFinished.indexOf(student.uuid) > -1 : false,
                progress: student.finalProgress,
                resultType: this.props.resultType,
                studentName: (this.props.hideStudentName) ? '*****' : student.name,
                key: student.uuid,
                answeredGraded: student.answeredGraded
            }));
        }
        
        return e('div', {
            className: 'name-column clearfix',
            children: [
                e(NamesHeader, this.props),
                studentNameCells,
                e('div', {
                    className:'result-row row-total',
                    children: [
                        e('p', {
                            children: translate('Class Total')
                        })
                    ]
                })
            ]
        })
    }
    
}

class StudentResponseCell extends React.Component {
    
    render() {
        let text = this.props.response && this.props.response.text ? this.props.response.text : '',
            className = 'result-row';
        
        if (this.props.hideStudentResponses && text.length > 0) {
            text = '';
            className += ' hidden-response-table-cell';
        } else {
            if (this.props.response) {
                let cellClass = !this.props.response.text ? '' : ' blue-table-cell';
                
                if (!_.isNull(this.props.response.is_correct) && !_.isUndefined(this.props.response.is_correct)) {
                    cellClass = this.props.response.is_correct ? ' green-table-cell' : ' red-table-cell';
                }
                
                className += cellClass;
            }
        }

        // Tranlsate the response if it's True/False.
        if (text.length > 0 && _.indexOf(['True', 'False'], text) !== -1) {
            text = translate(text);
        }

        return e('div', {
            className: className,
            children: e('p', {
                children: _.unescape(text).substring(0,8)
            })
        });
    }
    
}

class QuestionColumn extends React.Component {
    
    render() {
        let question = this.props.question,
            studentResponses = [];
        
        for (let student of this.props.students) {
            let response = _.find(this.props.responses[student.uuid], (response) => {
                return response.question === question.id;
            });
            
            studentResponses.push(
                e(StudentResponseCell, _.extend({}, this.props, {response: response, key: student.uuid+'-'+question.id}))
            );
        }
        
        return e('div', {
            className: 'question-column',
            children: [
                e('div', {
                    className: 'column-title',
                    onClick: () => this.props.questionClicked(this.props.question.get('order')),
                    children: [
                        e('div', {
                            className: 'question-number-' + question.get('type').toLowerCase(),
                            children: e('h1', {
                                children: question.get('order')
                            })
                        })
                    ]
                }),
                studentResponses,
                e('div', {
                    className: 'result-row row-total',
                    onClick: () => this.props.questionClicked(this.props.question.get('order')),
                    children: e('p', {
                        children: question.get('incorrectOrCorrectAnswers') > 0 ? '{0}%'.format((question.get('averageCorrect') * 100).toFixed(0)) : ''
                    })
                })
            ]
        })
    }
    
}

class ScrollableQuestions extends React.Component {
    
    render() {
        let questionColumns = [],
            realativeWidth = 83.5 * this.props.quiz.questions.length * 1.01;
        
        for (let question of this.props.quiz.questions) {
            questionColumns.push(
                e(QuestionColumn, _.extend({}, this.props, {question: question, key: question.id}))
            );
        }
        
        return e('div', {
            className: 'relative-area' + (this.props.showHereColumn ? ' relative-area-right' : ''),
            children: [
                e('div', {
                    id: 'scroll-area',
                    style: {width: realativeWidth},
                    children: questionColumns
                })
            ]
        });
    }
    
}

export default class ResultsTableView extends React.Component {
    
    render() {
        let topRightButtons = [
            e('div', {
                className: 'align-right button-container',
                children: [
                    e('button', {
                        className: 'button-primary button-large pill button',
                        children: [
                            e('span', {
                                children: translate('FINISH')
                            })
                        ],
                        type: 'button',
                        onClick: this.props.finishClicked
                    })
                ]
            })
        ];
        
        if (this.props.isFinalResults) {
            let exportScoresChildren = [
                e(ExportIcon, {
                    className: 'get-reports-button-icon'
                }),
                e('span', {
                    className: 'final-results-button-label',
                    children: translate('EXPORT SCORES')
                })
            ];
            if (this.props.gradecamError) {
                exportScoresChildren.push(
                    e(Tooltip, {
                        className: 'export-scores-tooltip',
                        location: 'above',
                        title: translate('Unsupported Browser'),
                        text: translate('Unfortunately your browser is incapable of exporting scores. Please use Chrome or Firefox.')
                    })
                );
            }
            topRightButtons = [
                e('div', {
                    className: 'final-results-button-container',
                    children: [
                        e('div', {
                            className: 'final-results-button',
                            children: [
                                e(ReportsIcon, {
                                    className: 'get-reports-button-icon'
                                }),
                                e('span', {
                                    className: 'final-results-button-label',
                                    children: translate('REPORTS')
                                })
                            ],
                            onClick: this.props.getReportsClicked
                        }),
                        
                        // Uncomment this button when the powers that be so demand.
                        
                        //e('div', {
                        //    className: 'final-results-button right-final-results-button' + (platform.isMobile() ? ' hidden' : ''),
                        //    children: exportScoresChildren,
                        //    onClick: this.props.exportScoresClicked
                        //})
                        
                    ]
                })
            ];
        }
        
        return e('div', {
            id: 'live-results-main-content',
            children: [
                e('h2', {
                    className: 'title',
                    children: [
                        e('span', {}, this.props.quizTitle),
                        e('span', {
                            className: 'standard-display',
                            children: this.props.quizStandard
                        })
                    ]
                }),

                ...topRightButtons,
                
                e('hr', {
                    className: 'final-results-divider'
                }),
                
                e('div', {
                    className: 'clearfix toggle-content',
                    children: [
                        e('div', {
                            className: 'align-left toggle-button',
                            children: [
                                e('div', {
                                    className: 'toggle-slider float-left',
                                    children: [
                                        e('input', {
                                            type: 'checkbox',
                                            id: 'show_hide_students_names',
                                            name: 'show_hide_students_names',
                                            className: 'toggle-slider-checkbox',
                                            checked: this.props.hideStudentName ? '' : true,
                                            onChange: this.props.toggleStudentName
                                        }),
                                        e('label', {
                                            className: 'toggle-slider-label',
                                            htmlFor: 'show_hide_students_names',
                                            children: [
                                                e('div', {className: 'toggle-slider-inner'}),
                                                e('div', {className: 'toggle-slider-switch'})
                                            ]
                                        })
                                    ]
                                }),
                                e('label', {
                                    className: 'align-left toggle-text',
                                    htmlFor: 'show_hide_students_names',
                                    children: translate('Show Names')
                                })
                            ]
                        }),
                        e('div', {
                            className: 'align-left toggle-button',
                            children: [
                                e('div', {
                                    className: 'toggle-slider float-left',
                                    children: [
                                        e('input', {
                                            type: 'checkbox',
                                            id: 'show_hide_students_responses',
                                            name: 'show_hide_students_responses',
                                            className: 'toggle-slider-checkbox',
                                            checked: this.props.hideStudentResponses ? '' : true,
                                            onChange: this.props.toggleStudentResponses
                                        }),
                                        e('label', {
                                            className: 'toggle-slider-label',
                                            htmlFor: 'show_hide_students_responses',
                                            children: [
                                                e('div', {className: 'toggle-slider-inner'}),
                                                e('div', {className: 'toggle-slider-switch'})
                                            ]
                                        })
                                    ]
                                }),
                                e('label', {
                                    className: 'align-left toggle-text',
                                    htmlFor: 'show_hide_students_responses',
                                    children: translate('Show Answers')
                                })
                            ]
                        })
                    ]
                }),

                e('div', {
                    id: 'lr-sp-result-table-container',
                    children: [
                        e('div', {
                            children: [
                                e('div', {
                                    id: 'results-table',
                                    className: this.props.showHereColumn || this.props.showHereFinalColumn ? 'with-presence' : '',
                                    children: [
                                        e(HereColumn, this.props),
                                        e(NamesColumn, this.props),
                                        e(ScrollableQuestions, this.props)
                                    ]
                                }),
                                e('p', {
                                    children: translate('Click question numbers or class total percentages for detailed views.')
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
