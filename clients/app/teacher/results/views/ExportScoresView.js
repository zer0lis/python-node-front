import React       from 'react';
import CloseX      from 'CloseX';
import Loader      from 'Loader';
import Select      from 'Select';
import Constants   from 'Constants';
import RadioButton from 'RadioButton';
import Checkmark   from 'LoggedInIcon';
import {translate} from 'translator';

let e = React.createElement;

export default class ExportScoresView extends React.Component {
    
    render() {
        let showSettings = this.props.showSettings;
        
        let modalContentChildren = [];
        
        if (this.props.loading) {
            modalContentChildren.push(
                e(Loader, {
                    className: 'export-scores-loader',
                    text: translate('Loading...')
                })
            );
        } else {
            let settingsChildren = [
                e('span', {
                    className: 'settings-text' + (showSettings ? '' : ' settings-text-summary'),
                    children: translate('SETTINGS')
                })
            ];
            
            let exportChildren = [];
            
            if (showSettings) {
                let exportKeyOptions = [];
                
                for (let i = 1; i <= 12; i++) {
                    exportKeyOptions.push(
                        {title: `F${i}`, value: i, selected: this.props.exportKey === i}
                    );
                }
                
                settingsChildren.push(
                    e('div', {
                        className: 'export-scores-select-container',
                        children: [
                            e('p', {
                                className: 'export-scores-settings-label',
                                children: translate('Export Key')
                            }),
                            e(Select, {
                                children: exportKeyOptions,
                                onSelect: (changed, value) => {
                                    this.props.exportKeyChanged(value);
                                }
                            })
                        ]
                    })
                );
                
                settingsChildren.push(
                    e('div', {
                        className: 'export-scores-select-container',
                        children: [
                            e('p', {
                                className: 'export-scores-settings-label',
                                children: translate('Next Line Key')
                            }),
                            e(Select, {
                                children: [
                                    {title: translate('Enter (Return)'), value: Constants.ENTER,       selected: this.props.nextLine === Constants.ENTER},
                                    {title: translate('Tab'),            value: Constants.TAB,         selected: this.props.nextLine === Constants.TAB},
                                    {title: translate('Enter Twice'),    value: Constants.ENTER_TWICE, selected: this.props.nextLine === Constants.ENTER_TWICE},
                                    {title: translate('Tab Twice'),      value: Constants.TAB_TWICE,   selected: this.props.nextLine === Constants.TAB_TWICE},
                                    {title: translate('Down Arrow'),     value: Constants.DOWN_ARROW,  selected: this.props.nextLine === Constants.DOWN_ARROW}
                                ],
                                onSelect: (changed, value) => {
                                    this.props.nextLineChanged(value);
                                }
                            })
                        ]
                    }),
                    e('div', {
                        className: 'export-scores-select-container export-scores-keystroke-container',
                        children: [
                            e('p', {
                                className: 'export-scores-settings-label',
                                children: translate('Keystroke Speed')
                            }),
                            e(Select, {
                                children: [
                                    {title: translate('Fast'),      value: Constants.FAST,      selected: this.props.speed === Constants.FAST},
                                    {title: translate('Medium'),    value: Constants.MEDIUM,    selected: this.props.speed === Constants.MEDIUM},
                                    {title: translate('Slow'),      value: Constants.SLOW,      selected: this.props.speed === Constants.SLOW},
                                    {title: translate('Very Slow'), value: Constants.VERY_SLOW, selected: this.props.speed === Constants.VERY_SLOW}
                                ],
                                onSelect: (changed, value) => {
                                    this.props.speedChanged(value);
                                }
                            })
                        ]
                    }),
                    e('div', {
                        className: 'export-scores-radio-buttons-container',
                        children: [
                            e(RadioButton, {
                                className: 'numeric-scores-radio',
                                label: translate('Numeric Scores'),
                                selected: this.props.method === Constants.NUMERIC,
                                onClick: () => {
                                    this.props.methodChanged(Constants.NUMERIC);
                                }
                            }),
                            e(RadioButton, {
                                className: 'percentage-scores-radio',
                                label: translate('Percentaged Scores'),
                                selected: this.props.method === Constants.PERCENTAGE,
                                onClick: () => {
                                    this.props.methodChanged(Constants.PERCENTAGE);
                                }
                            })
                        ]
                    })
                );
            } else {
                settingsChildren.push(
                    e('div', {
                        className: 'export-scores-change-container',
                        children: [
                            e('span', {
                                className: 'export-score-settings-change-text link',
                                children: translate('Change'),
                                onClick: this.props.changeClicked
                            })
                        ]
                    })
                );
                
                let nextLineKey = translate('Enter (Return)');
                if (this.props.nextLine == Constants.TAB)         {nextLineKey = translate('Tab')}
                if (this.props.nextLine == Constants.ENTER_TWICE) {nextLineKey = translate('Enter Twice')}
                if (this.props.nextLine == Constants.TAB_TWICE)   {nextLineKey = translate('Tab Twice')}
                if (this.props.nextLine == Constants.DOWN_ARROW)  {nextLineKey = translate('Down Arrow')}
                
                let keyStrokeSpeed = translate('Fast');
                if (this.props.speed == Constants.MEDIUM)    {keyStrokeSpeed = translate('Medium')}
                if (this.props.speed == Constants.SLOW)      {keyStrokeSpeed = translate('Slow')}
                if (this.props.speed == Constants.VERY_SLOW) {keyStrokeSpeed = translate('Very Slow')}
                
                settingsChildren.push(
                    e('table', {
                        className: 'export-scores-settings-table',
                        children: [
                            e('tbody', {
                                children: [
                                    e('tr', {
                                        children: [
                                            e('td', {
                                                className: 'export-scores-settings-descriptor',
                                                children: translate('Export Key') + ':'
                                            }),
                                            e('td', {
                                                className: 'export-scores-settings-text',
                                                children: `F${this.props.exportKey}`
                                            })
                                        ]
                                    }),
                                    e('tr', {
                                        children: [
                                            e('td', {
                                                className: 'export-scores-settings-descriptor',
                                                children: translate('Next Line Key') + ':'
                                            }),
                                            e('td', {
                                                className: 'export-scores-settings-text',
                                                children: nextLineKey
                                            })
                                        ]
                                    }),
                                    e('tr', {
                                        children: [
                                            e('td', {
                                                className: 'export-scores-settings-descriptor',
                                                children: translate('Keystroke Speed') + ':'
                                            }),
                                            e('td', {
                                                className: 'export-scores-settings-text',
                                                children: keyStrokeSpeed
                                            })
                                        ]
                                    })
                                ]
                            })
                        ]
                    })
                );
                
                exportChildren.push(
                    e(Checkmark, {
                        svgClass: 'ready-for-export-checkmark'
                    }),
                    e('div', {
                        className: 'ready-for-export-text-container',
                        children: [
                            e('span', {
                                className: 'ready-for-export-text',
                                children: translate('Scores are ready for export!')
                            })
                        ]
                    }),
                    e('span', {
                        className: 'export-scores-method-text',
                        children: this.props.methodText
                    }),
                    e('span', {
                        className: 'export-scores-note-text',
                        dangerouslySetInnerHTML: {__html: translate('<b>Note:</b> Open your grade book in another browser window, instead of another tab, for better results.')}
                    })
                );
            }
            
            modalContentChildren.push(
                e('div', {
                    className: 'export-scores-settings-container',
                    children: settingsChildren
                }),
                e('div', {
                    className: 'export-container',
                    children: exportChildren
                })
            );
        }
        
        return e('div', {
            id: 'export-scores-modal-background',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'export-scores-modal-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('span', {
                                    className: 'popup-title',
                                    children: translate('Export Scores')
                                }),
                                e('div', {
                                    className: 'cancel-button',
                                    children: [
                                        e(CloseX, {
                                            className: 'cancel-button-x',
                                            color: '#cccccc'
                                        })
                                    ],
                                    onClick: this.props.cancelExportClicked
                                })
                            ]
                        }),
                        e('div', {
                            id: 'export-scores-modal-content',
                            className: 'form',
                            children: modalContentChildren
                        }),
                        e('div', {
                            id: 'export-scores-footer',
                            className: 'export-scores-footer form' + (this.props.loading ? ' hidden' : ''),
                            children: [
                                e('button', {
                                    className: 'export-scores-footer-button export-scores-cancel-button',
                                    children: translate('CANCEL'),
                                    onClick: this.props.cancelExportClicked
                                }),
                                e('button', {
                                    id: 'export-scores-primary',
                                    className: 'export-scores-footer-button export-scores-primary-button',
                                    children: this.props.primaryButtonLabel,
                                    disabled: !showSettings || !this.props.settingsChanged,
                                    onClick: this.props.primaryButtonClicked
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
