import React  from 'react';
import CloseX from 'CloseX';
import {translate} from 'translator';

let e = React.createElement;

export default class InstallPluginView extends React.Component {
    
    render() {
        let steps = [];
        
        for (let step of this.props.installSteps) {
            steps.push(
                e('li', {
                    children: step
                })
            )
        }
        
        return e('div', {
            id: 'install-plugin-modal-background',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'install-plugin-modal-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('span', {
                                    className: 'popup-title',
                                    children: translate('Export Scores')
                                }),
                                e('div', {
                                    className: 'cancel-button',
                                    children: [
                                        e(CloseX, {
                                            className: 'cancel-button-x',
                                            color: '#cccccc'
                                        })
                                    ],
                                    onClick: this.props.cancelInstallClicked
                                })
                            ]
                        }),
                        e('div', {
                            id: 'install-plugin-modal-content',
                            className: 'form',
                            children: [
                                e('span', {
                                    className: 'install-plugin-title',
                                    children: this.props.installTitle
                                }),
                                e('span', {
                                    className: 'install-plugin-explanation',
                                    children: this.props.installText
                                }),
                                e('div', {
                                    className: 'install-plugin-steps-container',
                                    children: [
                                        e('ol', {
                                            className: 'install-plugin-steps',
                                            children: steps
                                        })
                                    ]
                                }),
                                e('span', {
                                    className: 'install-plugin-explanation install-plugin-explanation-lower',
                                    children: translate('When installation is complete, simply click "EXPORT SCORES" again to begin exporting scores.')
                                }),
                                e('div', {
                                    children: [
                                        e('button', {
                                            className: 'secondary-action-button action-button-132',
                                            children: translate('CANCEL'),
                                            onClick: this.props.cancelInstallClicked
                                        }),
                                        e('a', {
                                            className: 'action-button install-plugin-link',
                                            href: this.props.url,
                                            target: this.props.download ? '_self' : '_blank',
                                            children: this.props.installLabel
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
