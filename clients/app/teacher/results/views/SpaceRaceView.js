import React from 'react';
import CountdownIcon from 'CountdownIcon';
import CloseX from 'CloseX';
import {translate} from 'translator';

let e = React.createElement;

class TeamRocket extends React.Component {
    
    render() {
        let team = this.props.team,
            teamCount = this.props.teamCount,
            size = '';
        
        if (teamCount < 15)
            size = 'large';
        if (teamCount < 12)
            size = 'larger';
        if (teamCount < 9)
            size = 'largest';
        if (teamCount < 6)
            size = 'x_large';

        return e('div', {
            className: 'rocket-wrap ' + size,
            children: [
                e('div', {
                    className: 'team-color-row',
                    children: team.name
                }),
                e('div', {
                    className: 'progress-wrapper',
                    children: e('div', {
                        className: 'rocket-path-row',
                        children: e('div', {
                            className: 'progress-color-row',
                            style: {
                                width: team.completePercentage * 100 + '%',
                                backgroundColor: team.color,
                                borderLeft: '0.3em solid ' + team.secondaryColor
                            },
                            children: e('div', {
                                className: 'team-icon',
                                // Make sure the teams icons don't breach the space race container.
                                // This overrides the 'right' rule in css for .team-icon.
                                style: {right: (team.completePercentage > 0.9) ? 0 : null},
                                children: e('img', {
                                    src: `${window.static_url}img/space-race/${this.props.iconType}-large.png`
                                })
                            })
                        })
                    })
                })
            ]
        });
    }
    
}

export default class SpaceRaceView extends React.Component {
    
    render() {
        let spaceRaceChildren = [],
            countdownChildren = [];
        
        if (this.props.showCountdown) {
            countdownChildren.push(
                e('div', {
                    className: 'time-remaining',
                    children: [
                        e(CountdownIcon, {
                            className: 'time-remaining-icon'
                        }),
                        e('span', {
                            className: 'time-remaining-text',
                            children: this.props.countdownText
                        })
                    ]
                }),
                e('span', {
                    className: 'countdown-time',
                    children: this.props.countdownTime
                })
            );
            
            if (this.props.showCountdownFinish) {
                countdownChildren.push(
                    e('button', {
                        className: 'button button-primary button-small space-race-countdown-finish-button',
                        onClick: this.props.finishClicked,
                        children: translate('Finish')
                    })
                );
            } else {
                countdownChildren.push(
                    e('div', {
                        className: 'stop-countdown',
                        children: [
                            e(CloseX, {
                                className: 'stop-countdown-x'
                            })
                        ],
                        onClick: this.props.stopCountdownClicked
                    })
                );
            }
            
            spaceRaceChildren.push(
                e('div', {
                    id: 'countdown-bar',
                    className: 'countdown-bar-' + this.props.countdownColor,
                    children: countdownChildren
                })
            );
        }
        
        let teams = this.props.context.teams,
            teamsToRender = [];
        
        for (let team of teams) {
            teamsToRender.push(e(TeamRocket, {
                team:        team,
                teamCount: teams.length,
                iconType:    this.props.context.iconType
            }));
        }
        
        spaceRaceChildren.push(
            e('div', {
                className: 'race-module-background',
                style: {minHeight: this.props.minHeight + 'px'}
            }),
            e('div', {
                id: this.props.showCountdown ? 'race-module-with-countdown' : 'race-module',
                children: [
                    e('div', {
                        className: 'space-race-header',
                        children: [
                            e('span', {
                                className: 'space-race-header-text',
                                children: translate('Spaceship Game')
                            }),
                            e('button', {
                                className: 'button button-primary button-small pill space-race-finish-button',
                                onClick: this.props.finishClicked,
                                children: translate('Finish')
                            })
                        ]
                    }),
                    teamsToRender
                ]
            })
        );
        
        return e('div', {
            children: spaceRaceChildren
        });
    }
    
}
