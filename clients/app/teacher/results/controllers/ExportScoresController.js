import React       from 'react';
import ReactDOM    from 'react-dom';
import popup       from 'PopupController';
import model       from 'ExportScoresModel';
import platform    from 'Platform';
import utils       from 'Utils';
import installView from 'InstallPluginView';
import exportView  from 'ExportScoresView';
import Constants   from 'Constants';
import dots from 'Dots';
import user        from 'user';
import {translate} from 'translator';

class ExportScoresController {
    
    render(numericScores, percentageScores, reloadedAfterSave) {
        if (platform.isFirefox() && /^52/.test(platform.browserVersion)) {
            popup.render({
                title: translate('Browser Problem'),
                message: translate('Score export is not currently working in FireFox version 52. We are working to fix this as soon as possible. In the meantime we recommend using another browser, such as Chrome. If you have additional questions, please submit a support ticket through the help center.')
            });
            return;
        }
        
        model.reset();
        model.numericScores = numericScores;
        model.percentageScores = percentageScores;
        
        if (reloadedAfterSave) {
            model.loading = true;
            controller.renderExportModal();
            model.fetchSettings(controller.checkUserProfile);
        } else {
            if (!model.gradecamInstalled) {
                if (!model.gradecamAdded) {
                    controller.addGradecamForInstall();
                } else {
                    controller.renderInstallModal();
                }
            } else {
                model.loading = true;
                controller.renderExportModal();
                if (!model.settingsFetched) {
                    model.fetchSettings(controller.checkUserProfile);
                } else {
                    controller.checkUserProfile();
                }
            }
        }
    }
    
    addGradecamForInstall() {
        gradecam.setModuleOrder(['plugin']);
        gradecam.bind('install', controller.renderInstallModal);
        controller.addGradecam();
    }
    
    checkUserProfile() {
        if (!model.userProfileFetched) {
            model.fetchUserProfile(controller.addGradecamForExport);
        } else {
            controller.addGradecamForExport();
        }
    }
    
    addGradecamForExport() {
        if (!model.gradecamAdded) {
            gradecam.bind('pluginLoad', controller.pluginLoaded);
            gradecam.bind('hotkey', controller.hotkeyPressed);
            controller.addGradecam();
        } else {
            model.loading = false;
            controller.renderExportModal();
        }
    }
    
    addGradecam() {
        let gradecamContainer = document.createElement('div');
        gradecamContainer.id = 'gradecam-container';
        document.body.appendChild(gradecamContainer);
        gradecamContainer.appendChild(gradecam.getElement());
        model.gradecamAdded = true;
    }
    
    renderInstallModal() {
        if (platform.isChrome() || platform.isFirefox()) {
            let browserTool = platform.isChrome() ? translate('Chrome extension') : translate('Firefox addon');
            model.installTitle = translate('INSTALL GRADECAM TOOLS');
            model.installText = translate('To export scores you must install the GradeCam {0} and the GradeCam plugin.').format(browserTool);
            model.installSteps = [
                translate('Click the button below to install GradeCam.'),
                translate('Follow the instructions on the GradeCam website.'),
                translate('Restart your web browser.')
            ];
            model.installLabel = translate('INSTALL GRADECAM');
            model.download = false;
        } else {
            model.gradecamUrl = `https://downloads.gradecam.com/plugin/2.0.2.10/${platform.OS.toLowerCase().indexOf('windows') === -1 ? 'mac' : 'win'}-standard`
        }
        let container = document.createElement('div');
        container.id = 'install-plugin-modal-container';
        document.body.appendChild(container);
        ReactDOM.render(
            React.createElement(
                installView, {
                    cancelInstallClicked: controller.cancelInstallClicked,
                    installTitle: model.installTitle,
                    installText: model.installText,
                    installSteps: model.installSteps,
                    installLabel: model.installLabel,
                    url: model.gradecamUrl,
                    download: model.download
                }
            ),
            container,
            () => {
                if (!model.pageBlurred) {
                    model.pageBlurred = true;
                    utils.blurPage();
                }
            }
        );
    }
    
    cancelInstallClicked() {
        let container = document.getElementById('install-plugin-modal-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
        utils.unblurPage();
        model.pageBlurred = false;
    }
    
    pluginLoaded() {
        model.gradecamLoaded = true;
        model.loading = false;
        model.showSettings = !model.hasExportSettings;
        gradecam.stopCamera();
        let nextLine = '\n';
        if (model.nextLine === Constants.ENTER_TWICE) {nextLine = '\n\n'}
        if (model.nextLine === Constants.TAB)         {nextLine = '\t'}
        if (model.nextLine === Constants.TAB_TWICE)   {nextLine = '\t\t'}
        if (model.nextLine === Constants.DOWN_ARROW)  {nextLine = 'down'}
        gradecam.initTyper(`F${model.exportKey}`, model.speed, nextLine);
        controller.renderExportModal();
    }
    
    renderExportModal() {
        let container = document.getElementById('export-scores-modal-container');
        if (!container) {
            container = document.createElement('div');
            container.id = 'export-scores-modal-container';
            document.body.appendChild(container);
        }
        ReactDOM.render(
            React.createElement(
                exportView, {
                    cancelExportClicked: controller.cancelExportClicked,
                    loading: model.loading,
                    showSettings: model.showSettings,
                    changeClicked: controller.changeClicked,
                    exportKey: model.exportKey,
                    exportKeyChanged: controller.exportKeyChanged,
                    nextLine: model.nextLine,
                    nextLineChanged: controller.nextLineChanged,
                    speed: model.speed,
                    speedChanged: controller.speedChanged,
                    method: model.method,
                    methodChanged: controller.methodChanged,
                    methodText: model.methodText,
                    settingsChanged: model.settingsChanged,
                    primaryButtonLabel: model.hasExportSettings ? translate('UPDATE') : translate('SAVE & CONTINUE'),
                    primaryButtonClicked: controller.primaryButtonClicked
                }
            ),
            container,
            () => {
                if (!model.pageBlurred) {
                    model.pageBlurred = true;
                    utils.blurPage();
                }
            }
        );
    }
    
    cancelExportClicked() {
        model.showSettings = !model.hasExportSettings;
        let container = document.getElementById('export-scores-modal-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
        utils.unblurPage();
        model.pageBlurred = false;
    }
    
    exportKeyChanged(value) {
        model.exportKey = value;
        controller.renderExportModal();
    }
    
    nextLineChanged(value) {
        model.nextLine = value;
        controller.renderExportModal();
    }
    
    speedChanged(value) {
        model.speed = value;
        controller.renderExportModal();
    }
    
    changeClicked() {
        model.showSettings = true;
        controller.renderExportModal();
    }
    
    primaryButtonClicked() {
        if (model.settingsChanged) {
            dots.start('export-scores-primary');
            model.saveSettings(controller.reloadAfterSave);
        } else {
            model.showSettings = false;
            controller.renderExportModal();
        }
    }
    
    reloadAfterSave() {
        user.setExportingScores(true);
        window.location.reload();
    }
    
    methodChanged(value) {
        model.method = value;
        controller.renderExportModal();
    }
    
    hotkeyPressed() {
        let scores = model.method === Constants.NUMERIC ? model.numericScores : model.percentageScores;
        gradecam.typeScores(scores);
        gradecam.setLicensingInfo({
            licensee: 'Socrative',
            cust_type: 'teacher',
            cust_id: user.get('id'),
            cust_state: model.userState,
            cust_school: model.userSchool,
            cust_district: model.userDistrict,
            cust_studentcount: scores.length,
            cust_free: user.isPro(),
            cust_billMonth: model.userBillMonth
        });
    }
    
    
    
}

let controller = new ExportScoresController();
module.exports = controller;
