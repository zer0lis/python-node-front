import Backbone from 'backbone';
import React from 'react';
import ReactDOM from 'react-dom';
import activity from 'activity';
import store from 'store';
import model from 'SpaceRaceModel';
import view from 'SpaceRaceView';
import header from 'HeaderController';
import popup from 'PopupController';
import reportSettings from 'ReportSettingsController';
import {translate} from 'translator';

let router = null;

class SpaceRaceController {
    
    doSpaceRace(teacherRouter) {
        router = teacherRouter;
        activity.refresh(controller.activityRefreshed);
        if (model.getMinHeight() === -1) {
            window.addEventListener('resize', controller.setMinHeight);
        }
    }
    
    activityRefreshed() {
        if (activity.isSpaceRace() && activity.isInProgress()) {
            model.buildContext(controller.contextBuilt);
            controller.renderHeader();
        } else {
            router.routeToLaunch();
        }
    }
    
    contextBuilt() {
        if (!model.isListening()) {
            model.startListening(controller.activityChanged);
        }
        controller.setMinHeight();
        controller.renderView();
    }
    
    activityChanged() {
        if (activity.isSpaceRace() && Backbone.history.fragment === 'space-race') {
            model.buildContext(controller.contextBuilt);
        } else {
            model.endListening();
        }
    }

    setMinHeight() {
        if (Backbone.history.fragment !== 'space-race') {
            window.removeEventListener('resize', controller.setMinHeight);
            model.setMinHeight(-1);
            return;
        }

        let pageContainer = document.getElementById('page-container').offsetHeight,
            bannerContainer = document.getElementById('banner-container').offsetHeight,
            headerContainer = document.getElementById('header-container').offsetHeight,
            footerContainer = document.getElementById('footer').offsetHeight;
            
        let backgroundMinHeight = pageContainer - bannerContainer - headerContainer - footerContainer;
        model.setMinHeight(backgroundMinHeight);
        controller.renderView();
    }
    
    renderView() {
        if (activity.isSpaceRace() && Backbone.history.fragment === 'space-race') {
            let countdownColor = 'green';
            
            if (activity.getCountdownMinutes() === 0) {
                countdownColor = activity.getCountdownSeconds() > 0 ? 'red' : 'orange';
            }
            
            ReactDOM.render(
                React.createElement(
                    view, {
                        minHeight:            model.getMinHeight(),
                        context:              model.get('context'),
                        showCountdown:        activity.shouldShowCountdown(),
                        countdownColor:       countdownColor,
                        countdownText:        activity.isCountdownDone() ? translate('Spaceship Game complete!') : translate('Spaceship Game time remaining'),
                        countdownTime:        activity.getCountdownDisplay(),
                        stopCountdownClicked: controller.stopCountdownClicked,
                        showCountdownFinish:  activity.isCountdownDone(),
                        finishClicked:        controller.finishClicked
                    }
                ),
                document.getElementById('main-content'),
                controller.checkCountdown
            );
        }
    }
    
    checkCountdown() {
        if (activity.shouldShowCountdown()) {
            activity.addCountdownCallback(controller.countdownChanged);
            if (activity.shouldStartCountdown()) {
                activity.startCountdown();
            }
        }
    }
    
    countdownChanged() {
        if (activity.isCountdownDone()) {
            controller.finishByCountdown();
        }
        controller.renderView();
    }
    
    stopCountdownClicked() {
        if (!activity.isCountdownDone()) {
            popup.render({
                title: translate('Please Confirm'),
                message: translate('Are you sure you want to stop the countdown?'),
                buttonText: translate('Yes'),
                cancelText: translate('No'),
                buttonClicked: controller.stopCountdown
            });
        } else {
            controller.stopCountdown();
        }
    }
    
    stopCountdown() {
        store.set('countdownStopped', true);
        activity.resetCountdown();
        controller.renderView();
    }
    
    finishByCountdown() {
        model.endListening();
        model.setFinishedByCountdown(true);
    }
    
    renderHeader() {
        if (Backbone.history.fragment === 'space-race') {
            header.render('results');
        }
    }

    finishClicked() {
        controller.showReportPopup();
    }

    showReportPopup() {
        reportSettings.open({
            router: router,
            activityId: activity.getId(),
            quizId: activity.getQuizId(),
            activityRunning: true,
            allowViewChart: true,
            allowPdf: true,
            exitCallback: controller.reportSettingsClosed
        });
    }
    
    reportSettingsClosed(route, callback) {
        model.endListening();
        model.setReportSettingsRoute(route);
        model.setReportSettingsCallback(callback);
        if (model.getFinishedByCountdown() || activity.getFinishedOnRefresh()) {
            controller.activityFinished();
        } else {
            activity.endCurrentActivity(controller.activityFinished);
        }
    }
    
    activityFinished() {
        let reportSettingsCallback = model.getReportSettingsCallback();
        
        if (reportSettingsCallback) {
            reportSettingsCallback();
        }
        
        let reportSettingsRoute = model.getReportSettingsRoute();
        model.clear();
        activity.clear();
        router.routeToDynamic({route: reportSettingsRoute || 'launch'});
    }
    
}

let controller = new SpaceRaceController();
module.exports = controller;
