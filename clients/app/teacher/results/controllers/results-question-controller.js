var BaseController = require('base-results-controller'),
    Backbone       = require('backbone'),
    _              = require('underscore'),
    React          = require('react'),
    ReactDOM       = require('react-dom'),
    model          = require('results-question-model'),
    router         = null,
    popup          = require('PopupController'),
    activity       = require('activity'),
    translate      = require('translator').translate,
    saPopup        = require('ShortAnswerController');

import view from 'ResultsQuestionView';

function ResultsQuestionController(){}

// Inherit from the base results controller.
ResultsQuestionController.prototype = _.extend(Object.create(BaseController.prototype), {

    /**
     * Entry point called by TeacherRouter.
     * @param {object} teacherRouter The teacher router.
     * @param {string} mode Either 'live' for a running quiz, or 'final' for a finished quiz report.
     * @param {number} questionNumber Question number to display.
     * @param {number} activityId Optional activity ID, used for looking up final results.
     * @param {number} quizId Optional quiz ID, used for looking up final results.
     */
    doResultsQuestion: function(teacherRouter, mode, questionNumber, activityId, quizId) {
        // Set the model, view, and router for the base results controller.
        controller.setModel(model);
        controller.setView(view);
        controller.setRouter(teacherRouter);
        router = teacherRouter;

        // Set some properties that are common whether the results are live or final.
        model.set('mode', mode);
        model.set('questionNumber', questionNumber);
        model.set('activityId', activityId);
        model.set('quizId', quizId);
        model.set('showCorrect', false);
        model.set('showExplanation', false);

        this.getResult(false);
    },

    renderView: function() {
        // When the user switches between the results table view and the results question view, the models are still listening
        // for local activity changes. As a result, we only want to render this view if we're on the question route.
        if (!/question/.test(Backbone.history.fragment)) {
            return;
        }
        var live = model.get('mode') === 'live';
        model.buildQuestionDetail(model.get('questionNumber'), live); // Set various properties on the model for live or final.
        if (model.get('error')) {
            // The question number was likely invalid (e.g. the user manually changed the question number in the URL).
            model.set('error', false);
            router.routeToLaunch();
            return;
        }
        controller.setImageUrl();
        var activityType = activity.get(live ? 'activity_type' : 'final_result_activity_type');
        var questionType = model.get('question').type;
        var qqSa = activityType === activity.QQ_SA && questionType === 'FR';
        var isQQ = activityType === activity.QQ_SA || activityType === activity.NO_REPORT;
        var isVote = activityType === activity.VOTE;
        var numResponses = model.get('responses').length;
        
        var pacing = activity.has('settings') ? activity.get('settings').pacing : '';

        var standardName;
        if (model.get('mode') === 'final') {
            standardName = model.get('standardName');
        } else {
            standardName = activity.has('quiz') ? activity.get('quiz').getStandardName() : '';
        }
        
        ReactDOM.render(
            React.createElement(
                view, {
                    quizTitle:                model.get('quizTitle'),
                    quizStandard:             standardName,
                    mainButtonAction:         controller.mainButtonAction,
                    live:                     live,
                    showNavButtons:           !isQQ && !isVote,
                    showPreviousButton:       model.get('questionNumber') != 1,
                    previousClicked:          controller.previousClicked,
                    showBackToResultsButton:  pacing === 'student' || !live,
                    backToResultsClicked:     controller.backToResultsClicked,
                    showNextButton:           activity.get(live ? 'quiz' : 'final_result').questions.length > model.get('questionNumber'),
                    nextClicked:              controller.nextClicked,
                    imageUrl:                 model.get('imageUrl'),
                    showQuestionNumber:       !isQQ && !isVote,
                    question:                 model.get('question'),
                    allowToggleHowDo:         questionType !== 'FR' && !isQQ && !isVote,
                    allowToggleAnswers:       questionType === 'FR',
                    allowStartVote:           qqSa,
                    allowToggleNames:         questionType === 'FR',
                    toggleHowDo:              controller.toggleHowDo,
                    showCorrect:              model.get('showCorrect') || isVote || (isQQ && questionType !== 'FR'),
                    showAnswers:              model.get('showAnswers'),
                    toggleAnswers:            controller.toggleAnswers,
                    removeClicked:            controller.removeClicked,
                    startVote:                controller.startVote,
                    showNames:                model.get('showNames'),
                    toggleNames:              controller.toggleNames,
                    studentCountText:         qqSa ? numResponses : model.get('studentsAnswered') + '/' + model.get('totalStudents'),
                    studentCountLabelText:    ' ' + (qqSa ? (numResponses == 1 ? translate('answer') : translate('answers')) : translate('students answered')),
                    allowThemeSwitcher:       questionType === 'FR',
                    darkTheme:                questionType === 'FR' && model.get('darkTheme'),
                    switchTheme:              controller.switchTheme,
                    allowRemoveAnswers:       questionType === 'FR' && isQQ,
                    letterResponses:          questionType === 'MC' || questionType === 'TF',
                    aggregateResponses:       model.get('aggregateResponses'),
                    responses:                model.get('responses'),
                    allowExplanation:         !isQQ && !isVote,
                    showExplanation:          model.get('showExplanation'),
                    toggleExplanation:        controller.toggleExplanation,
                    mcClicked:                controller.mcClicked,
                    tfClicked:                controller.tfClicked,
                    saClicked:                controller.saClicked,
                    showQuickQuestionButtons: isQQ || isVote
                }
            ),
            document.getElementById('main-content')
        );
    },

    setImageUrl: function() {
        var imageUrl = '';
        var question = model.get('question');
        if (question.resources.length > 0) {
            imageUrl = question.resources[0].url;
            var resourcesUrls = {};
            if (question.resources[0].data !== null)
                resourcesUrls = JSON.parse(question.resources[0].data);
            if (!_.isEmpty(resourcesUrls)) {
                var ww = window.innerWidth;
                if (ww <= 480) {
                    imageUrl = resourcesUrls.S.url;
                } else if (480 < ww && ww <= 800) {
                    imageUrl = resourcesUrls.M.url;
                } else if (800 < ww && ww <= 1200) {
                    imageUrl = resourcesUrls.L.url;
                } else if (ww > 1200) {
                    imageUrl = resourcesUrls.XL.url;
                }
            }
        }
        model.set('imageUrl', imageUrl);
    },

    mainButtonAction: function() {
        BaseController.prototype.mainButtonAction.call(controller);
    },

    reloadFirst: function() {
        // There are cases where we need to route to question 1 but we're already at question 1: going from an
        // existing quick question to a new one, or starting a vote from short answer. Backbone won't reload a
        // page when the current route and new route are the same, so we must call doResultsQuestion() directly.
        controller.doResultsQuestion(router, 'live', 1);
    },

    nextOrPreviousQuestion: function(number) {
        var pacing = activity.has('settings') ? activity.get('settings').pacing : '';
        if (pacing === 'teacher' && /^live\-results/.test(Backbone.history.fragment)) {
            model.goToQuestion(number, controller.goToQuestionResult); // This is a live, teacher-paced quiz. Go to the next or previous question for both teacher and students.
        } else {
            controller.routeToNewQuestion(number);
        }
    },

    goToQuestionResult: function(number) {
        if (number) {
            router.routeToLiveResults({number: number});
        } else {
            console.log('Error going to specific question.');
        }
    },

    previousClicked: function() {
        controller.nextOrPreviousQuestion(Number(model.get('questionNumber')) - 1);
    },

    backToResultsClicked: function() {
        if (model.get('mode') === 'live') {
            router.routeToLiveResults();
        } else {
            router.routeToFinalResultsTable({
                activityId: model.get('activityId'),
                quizId: model.get('quizId')
            });
        }
    },

    nextClicked: function() {
        controller.nextOrPreviousQuestion(Number(model.get('questionNumber')) + 1);
    },

    toggleHowDo: function() {
        model.set('showCorrect', !model.get('showCorrect'));
        controller.renderView();
    },

    toggleAnswers: function() {
        model.set('showAnswers', !model.get('showAnswers'));
        controller.renderView();
    },

    removeClicked: function(id) {
        activity.hideResponse(id, controller.renderView);
    },

    startVote: function() {
        if (model.get('responses') && model.get('responses').length && model.get('responses').length >= 2) {
            popup.render({
                title: translate('Please Confirm'),
                message: translate('Would you like to start a vote?'),
                buttonText: translate('Yes'),
                cancelText: translate('No'),
                buttonClicked: function () {
                    activity.convertCurrentQuickQuestionToSurvey(() => {
                        controller.reloadFirst();
                    });
                }
            })
        } else {
            popup.render({
                title: translate('More Responses!'),
                message: translate('You must have at least two student responses to start a vote.')
            });
        }
    },

    toggleNames: function() {
        model.set('showNames', !model.get('showNames'));
        controller.renderView();
    },

    switchTheme: function() {
        model.set('darkTheme', !model.get('darkTheme'));
        controller.renderView();
    },

    toggleExplanation: function() {
        model.set('showExplanation', !model.get('showExplanation'));
        controller.renderView();
    },
    
    mcClicked: function() {
        model.startQuestion('mc', controller.reloadFirst);
    },

    tfClicked: function() {
        model.startQuestion('tf', controller.reloadFirst);
    },

    saClicked: function() {
        activity.checkBeforeStartingActivity(controller.startSaConfirmed);
    },
    
    startSaConfirmed: function() {
        saPopup.render({
            startCallback: controller.reloadFirst
        });
    }

});

ResultsQuestionController.prototype.constructor = ResultsQuestionController;
var controller = new ResultsQuestionController();
module.exports = controller;
