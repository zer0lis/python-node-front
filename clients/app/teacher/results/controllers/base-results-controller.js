let Backbone       = require('backbone'),
    _              = require('underscore'),
    translate = require('translator').translate,
    header         = require('HeaderController'),
    reportSettings = require('ReportSettingsController'),
    room           = require('room'),
    activity       = require('activity'),
    model          = null,
    view           = null,
    router         = null;

/*
    Base controller extended by both result controllers: table and question.
*/

function BaseResultsController(){}

BaseResultsController.prototype = {

    setModel: function(externalModel) {
        model = externalModel;
    },

    setView: function(externalView) {
        view = externalView;
    },
    
    setRouter: function(teacherRouter) {
        router = teacherRouter;
    },

    getResult: function(showingTable) {
        if (model.get('mode') === 'live') {
            if (room.hasRoster()) {
                room.getStudentList({
                    success: () => {
                        this.loadQuiz(showingTable);
                    }
                });
            } else {
                this.loadQuiz(showingTable);
            }
        } else {
            activity.loadFinalResult(model.get('activityId'), model.get('quizId'), (success) => {
                this.finalResultLoaded(success);
            })
        }
    },
    
    loadQuiz: function(showingTable) {
        activity.refresh(() => {
            if (activity.isInProgress()) {
                activity.getCurrentQuiz(() => {
                    this.quizLoaded(showingTable);
                });
                header.render('results');
            } else {
                router.routeToLaunch();
            }
        });
    },
    
    quizLoaded: function(showingTable) {
        let quizTitle = activity.get('quiz').get('name');
        if (activity.isQuickQuestion() || activity.isVote()) {
            quizTitle = translate('Polls');
        } else if (quizTitle === 'Exit Ticket Quiz') {
            quizTitle = translate('Final Survey Quiz');
        }
        model.set('quizTitle', quizTitle);
        model.stopListening(activity, 'change', this.activityChanged);
        model.listenTo(activity, 'change', () => this.activityChanged());
        if (showingTable) {
            model.set('studentsFinished', []);
            if (activity.get('students_finished') && activity.get('students_finished').length > 0) {
                model.set('studentsFinished', activity.get('students_finished'));
            }
        }
        this.renderView();
    },
    
    activityChanged: function() {
        if (!activity.has('quiz') || !activity.isInProgress()) {
            return;
        }
        if (/^live\-results/.test(Backbone.history.fragment)) {
            this.renderView();
        }
    },
    
    mainButtonAction: function() {
        if (model.get('mode') === 'live') {
            this.endCurrentActivity();
        } else {
            this.showFinalReportSettings();
        }
    },
    
    endCurrentActivity: function() {
        let quiz = activity.get('quiz');
        
        if (_.isUndefined(quiz) || _.isUndefined(quiz.questions)) {
            router.routeToLaunch();
            return;
        }
        
        if (activity.isQuiz() || activity.isQuickQuestionShortAnswer() || activity.isVote()) {
            this.showLiveReportSettings((route, callback) => {
                this.finishLiveActivity(() => {
                    if (_.isFunction(callback)) {
                        callback();
                    }
                    router.routeToDynamic({route: route || 'launch'});
                });
            });
            return;
        }
        
        this.finishLiveActivity(function() {
            router.routeToLaunch();
        });
    },

    showLiveReportSettings: function(finishFunction) {
        reportSettings.open({
            router: router,
            activityId: activity.get('id'),
            quizId: activity.get('quiz').id,
            activityRunning: true,
            allowViewChart: !activity.isQuickQuestion() && !activity.isVote(),
            allowPdf: !activity.isQuickQuestion() && !activity.isVote(),
            exitCallback: finishFunction
        });
    },

    finishLiveActivity: function(onComplete) {
        activity.endCurrentActivity(function() {
            if (_.isFunction(onComplete)) {
                onComplete();
            }
        });
    },
    
    finalResultLoaded: function(success) {
        if (success) {
            header.render('results');
            let quizTitle = activity.get('final_result').get('name');
            if (quizTitle === 'Exit Ticket Quiz') {
                quizTitle = translate('Final Survey Quiz');
            }
            quizTitle += ` - ${activity.get('final_result_date')}`;
            model.set('quizTitle', quizTitle);
            model.set('studentsFinished', []);
            model.set('standardName', activity.get('final_result').getStandardName());
            this.renderView();
        } else {
            router.routeToLaunch();
        }
    },
    
    showFinalReportSettings: function() {
        reportSettings.open({
            router: router,
            activityId: activity.getFinalResultActivityId(),
            quizId: activity.getFinalResultQuizId(),
            activityRunning: true,
            allowViewChart: false,
            allowPdf: true
        });
    },

    routeToNewQuestion: function(number) {
        let routeMethod = router.routeToLiveResults,
            routeData = {number: number};
        if (model.get('mode') !== 'live') {
            routeMethod = router.routeToFinalResultsQuestion;
            routeData.activityId = model.get('activityId');
            routeData.quizId = model.get('quizId');
        }
        routeMethod(routeData);
    }
};

// Export the constructor function itself (not a new instance) so subclasses have access to the prototype property.
module.exports = BaseResultsController;
