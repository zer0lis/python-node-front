let BaseController = require('base-results-controller'),
    Backbone       = require('backbone'),
    _              = require('underscore'),
    Constants      = require('Constants'),
    messaging      = require('Messaging'),
    activity       = require('activity'),
    React          = require('react'),
    ReactDOM       = require('react-dom'),
    model          = require('results-table-model'),
    user           = require('user'),
    platform       = require('Platform'),
    exportModal    = require('ExportScoresController'),
    room           = require('room');

import view from 'ResultsTableView';

function ResultsTableController() {
    messaging.addListener({
        message: Constants.STUDENT_FINISHED,
        callback: (data) => this.handleStudentFinished(data)
    });
    messaging.addListener({
        message: Constants.PRESENCE,
        callback: () => {
            if (room.hasRoster()) {
                this.renderView();
            }
        }
    });

    messaging.addListener({
        message: Constants.STUDENT_STARTED_QUIZ,
        callback: (data) => this.handlePresence(data)
    });
}

// Inherit from the base results controller.
ResultsTableController.prototype = _.extend(Object.create(BaseController.prototype), {
    
    /**
     * Entry point called by TeacherRouter.
     * @param {object} teacherRouter The teacher router.
     * @param {string} mode Either 'live' for a running quiz, or 'final' for a finished quiz report.
     * @param {number} activityId Optional activity ID, used for looking up final results.
     * @param {number} quizId Optional quiz ID, used for looking up final results.
     */
    doResultsTable: function(teacherRouter, mode, activityId, quizId) {
        // Set the model, view, and router for the base results controller.
        controller.setModel(model);
        controller.setView(view);
        controller.setRouter(teacherRouter);

        // Set some properties that are common whether the results are live or final.
        model.set('mode', mode);
        model.set('activityId', activityId);
        model.set('sortDirection', 'asc');
        model.set('resultType', model.get('mode') === 'final' ? 'percentage-score-result-type' : 'progress-result-type');
        
        if (model.get('quizId') !== quizId) {
            model.reset();
        }

        model.set('quizId', quizId);

        this.getResult(true);
    },

    renderView: function() {
        // When the user switches between the results table view and the results question view, the models are still listening
        // for local activity changes. As a result, we only want to render this view if we're on the table route.
        if (!/table/.test(Backbone.history.fragment)) {
            return;
        }
        
        let isLive = model.get('mode') === 'live';
        model.buildResultsTable(isLive); // Set quiz, students, and responses on the model for live or final.
        
        let standardName = model.get('standardName');
        
        if (isLive) {
            standardName = activity.has('quiz') ? activity.get('quiz').getStandardName() : '';
        }
        
        ReactDOM.render(
            React.createElement(
                view, {
                    quizTitle:                  model.get('quizTitle'),
                    quizStandard:               standardName,
                    finishClicked:              () => controller.endCurrentActivity(),
                    isFinalResults:             model.get('mode') === 'final',
                    getReportsClicked:          () => controller.showFinalReportSettings(),
                    gradecamError:              model.getGradecamError(),
                    exportScoresClicked:        controller.exportScoresClicked,
                    hideStudentResponses:       model.getHideStudentResponses(),
                    hideStudentName:            model.getHideStudentName(),
                    toggleStudentResponses:     controller.toggleStudentResponses,
                    toggleStudentName:          controller.toggleStudentName,
                    showHereColumn:             isLive && room.hasRoster(),
                    showHereFinalColumn:        model.get('mode') === 'final' && activity.get('final_result_rostered'),
                    hereCount:                  model.get('hereCount'),
                    students:                   model.get('students'),
                    studentsFinished:           model.get('studentsFinished'),
                    sortDirection:              model.get('sortDirection'),
                    nameSortChanged:            controller.nameSortChanged,
                    resultType:                 model.get('resultType'),
                    resultTypeChanged:          controller.resultTypeChanged,
                    quiz:                       model.get('quiz'),
                    questionClicked:            controller.questionClicked,
                    responses:                  model.get('responses')
                }
            ),
            document.getElementById('main-content'),
            () => {
                if (!isLive && user.isExportingScores()) {
                    controller.exportScoresClicked();
                    user.setExportingScores(false);
                }
            }
        );
    },
    
    exportScoresClicked: function() {
        if (platform.isIE() || platform.isEdge()) {
            model.setGradecamError(true);
            document.body.addEventListener('click', controller.bodyClicked);
            controller.renderView();
        } else {
            let numericScores = [];
            let percentageScores = [];
            for (let student of model.get('students')) {
                numericScores.push(student.numericScore);
                percentageScores.push((student.grade * 100).toFixed(0));
            }
            exportModal.render(numericScores, percentageScores, user.isExportingScores());
        }
    },
    
    bodyClicked: function() {
        _.defer(() => {
            document.body.removeEventListener('click', controller.bodyClicked);
            model.setGradecamError(false);
            controller.renderView();
        });
    },
    
    toggleStudentResponses: function() {
        model.toggleStudentResponses();
        controller.renderView();
    },

    toggleStudentName: function() {
        model.toggleStudentName();
        controller.renderView();
    },

    nameSortChanged: function() {
        model.toggleSortDirection();
        controller.renderView();
    },
    
    resultTypeChanged: function(type) {
        model.set('resultType', type);
        controller.renderView();
    },
    
    questionClicked: function(questionNumber) {
        controller.routeToNewQuestion(questionNumber);
    },
    
    handleStudentFinished: function(data) {
        // Add a finished student from the back end to the local models.
        let studentsFinished = model.get('studentsFinished') || [];
        studentsFinished.push(data.user_uuid);
        model.set('studentsFinished', studentsFinished);
        activity.set('students_finished', studentsFinished);
        this.renderView();
    },

    handlePresence: function(data) {
        if (data.room === room.getRoomName().toLowerCase()) {
            let loggedInStudent = _.findWhere(room.getStudents(), {student_id: data.id});
            loggedInStudent.logged_in = true;

            this.renderView();
        }
    }

});

ResultsTableController.prototype.constructor = ResultsTableController;
let controller = new ResultsTableController();
module.exports = controller;
