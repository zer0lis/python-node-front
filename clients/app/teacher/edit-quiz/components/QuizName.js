import React from 'react';
import {translate} from 'translator';

let e = React.createElement;

export default class QuizName extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            quizName: this.props.model.quiz.get('name')
        };
    }
    
    componentWillReceiveProps(newProps){
        this.setState({quizName: newProps.model.quiz.get('name')});
    }
    
    render() {
        let cssClass = 'textfield text-entry-toggle-textbox';
        
        if (!this.state.quizName || this.state.quizName.trim().length < 1) {
            cssClass += ' form-danger';
        }
        
        return e('div', {
            id: 'create-quiz-top-row',
            children: e('div', {
                id: 'quizname-setup-widget',
                className: 'text-entry-toggle',
                children: e('div', {
                    id: 'quizname-setup-container',
                    className: 'text-entry-toggle-textbox-container form',
                    children:[
                        e('input', {
                            id: 'quizname-textbox',
                            type: 'text',
                            className: cssClass,
                            value: this.state.quizName,
                            autoComplete: 'off',
                            autoCorrect: 'off',
                            autoCapitalize: 'off',
                            placeholder: translate('Name Your Quiz...'),
                            maxLength: 255,
                            onChange: (event) => {
                                this.setState({
                                    quizName: event.target.value
                                });
                                this.props.controller.quizNameChanged(event.target.value);
                            }
                        })
                    ]
                })
            })
        });
    }
    
}
