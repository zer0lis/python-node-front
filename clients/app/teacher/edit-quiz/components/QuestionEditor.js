import React from 'react';
import popup from 'PopupController';
import Utils from 'Utils';
import {translate} from 'translator';

let e = React.createElement;

export default class QuestionEditor extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            isInDisplayMode = this.props.questionState === 'displayContainer';

        return e('div', {
            className: 'align-right',
            children: [
                e('button', {
                    className: 'button-primary button-small save-question align-right',
                    children: [
                        e('i', {
                            'data-icon': isInDisplayMode ? 'r' : 't'
                        }),
                        e('span', {
                            children: isInDisplayMode ? translate('Edit') : translate('Save')
                        })
                    ],
                    onClick: () => {
                        if (this.props.questionState === 'displayContainer') {
                            if (!c.canEditQuestion()) {
                                return;
                            }
                            
                            this.props.question.set('dirty', true);
                            
                            if (Utils.questionHasContent(this.props.question) && Utils.questionHasTags(this.props.question)) {
                                this.props.editQuestionCallback({
                                    questionState: 'formatContainer'
                                });
                            } else {
                                this.props.editQuestionCallback({
                                    questionState: 'editContainer'
                                });
                            }
                        } else {
                            c.saveQuestionClicked(this.props.question, () => {
                                this.props.editQuestionCallback({
                                    answers: this.props.question.answers,
                                    questionState: 'displayContainer'
                                });
                            });
                        }
                    }
                }),
                e('div', {
                    className: 'formatting-container align-right',
                    style: {
                        display: isInDisplayMode ? 'none' : 'inline-block'
                    },
                    children: [
                        e('span', {
                            className: 'formatting',
                            children: translate('Formatting: ')
                        }),
                        e('div', {
                            className: 'toggle-slider ' + (this.props.questionHasContent ? 'disabled' : ''),
                            children: [
                                e('input', {
                                    id: 'format-toggle-question-' + this.props.questionOrder,
                                    type: 'checkbox',
                                    className: 'toggle-slider-checkbox',
                                    checked: this.props.questionState === 'formatContainer',
                                    onChange: (event) => {
                                        if (event.target.checked) {
                                            this.props.formatToggleCallback('formatContainer');
                                        } else if (!Utils.questionHasTags(this.props.question)) {
                                            event.target.parentElement.classList.remove('disabled');
                                            this.props.formatToggleCallback('editContainer');
                                        } else {
                                            event.target.parentElement.classList.add('disabled');
                                            popup.render({
                                                title: translate('Remove Formatting'),
                                                message: translate('Please remove all text formatting from this question before turning the advanced editor off.')
                                            });
                                        }
                                    }
                                }),
                                e('label', {
                                    className: 'toggle-slider-label',
                                    htmlFor: 'format-toggle-question-' + this.props.questionOrder,
                                    children: [
                                        e('div', {className: 'toggle-slider-inner'}),
                                        e('div', {className: 'toggle-slider-switch'})
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}

