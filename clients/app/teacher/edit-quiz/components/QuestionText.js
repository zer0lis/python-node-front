/**
 * Component used by every question to create the question text toggle
 * 
 * Receives:
 *      the question content
 *      the callback funtion called when the content changes
 *      the question state
 */

import React from 'react';
import TextEntryToggle from 'TextEntryToggle';

let e = React.createElement;

export default class QuestionText extends React.Component {
    
    render() {
        return e('div', {
            children: e(TextEntryToggle, {
                questionTextContent:  this.props.questionTextContent,
                questionTextOnChange: this.props.questionTextCallback,
                questionState:        this.props.questionTextState,
                focus:                true,
                higherLimit:          true
            }),
            className: 'question-text-entry text-entry-toggle align-left'
        });
    }
    
}
