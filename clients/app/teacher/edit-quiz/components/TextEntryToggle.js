import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'underscore';
import autosize from 'autosize';
import uuid from 'uuid';
import utils from 'Utils';
import setUpTinyMCE from 'tinymce-editor';

let e = React.createElement;

class QuestionTextarea extends React.Component {
    
    constructor(props) {
        super(props);
        
        let content = this.props.questionTextContent || '';
		
		if (this.props.questionState === 'editContainer') {
            content = this.stripTags(content);
        } else {
            content = content.replace(new RegExp('(\r|\n)', 'g'), '<br>');
        }
        
        this.state = {
            questionText: utils.unescape(content),
            contentChanged: false
        };
        
        this.textareaDomNode = null;
    }
    
	getTextareaDomNode() {
		if (!this.textareaDomNode) {
		    this.textareaDomNode = ReactDOM.findDOMNode(this).querySelectorAll('textarea');
		}
		
		return this.textareaDomNode;
	}
	
	stripTags(content) {
        if (content) {
            // Replace the ending tag with a new line character. The last p ending tag should not be replaced.
            content = content.replace(new RegExp('<\/p>([^<\/p>]*)$'), '');
            content = content.replace(new RegExp('<\/p>', 'g'), '\n');
            content = content.replace(new RegExp('<p>', 'g'), '');
            content = content.replace(new RegExp('&nbsp;', 'g'), ' ');
            content = content.replace(new RegExp('(<br\ ?\b[^>]*\/?>)+', 'g'), '\r\n');
        }
        
        return content;
    }
    
    updateEditor(id) {
        if (this.props.questionState === 'formatContainer') {
            let textareaPath = id ? `#${id}` : `#${this.props.id}`;
            
            try {
                setUpTinyMCE(textareaPath, this.props.questionTextOnChange);
            } catch (error) {
                console.warn(error);
            }
        } else {
            // If the questionState is not format, remove all tinyMCE instances.
            if (typeof(tinyMCE) !== 'undefined') {
                let length = tinyMCE.editors.length;
                
                for (let i = length; i > 0; i--) {
                    try {
                        tinyMCE.editors[i-1].remove();
                    } catch (error) {
                        console.warn(error);
                    }
                }
            }
        }
    }
    
    componentWillReceiveProps(nextProps) {
        let content = nextProps.questionTextContent || '';
        
        if (nextProps.questionState === 'editContainer') {
            content = this.stripTags(content);
        } else {
            content = content.replace(new RegExp('(\n)', 'g'), '<br>');
        }
        
        this.setState({
            questionText: nextProps.questionState === 'editContainer' ? utils.unescape(content) : content,
            contentChanged: true
        });
        
        // Only update the editor if the question state has changed.
        // This avoids calling setUpTinyMCE() when the editor is already open.
        if (this.props.questionState !== nextProps.questionState) {
            let id = this.props.id;
            this.props = nextProps;
            this.updateEditor(id);
        } else {
            this.props = nextProps;
        }
    }
    
    // When deleting an MC answer the state for this component gets changed with the new content (the answer above it),
    // but the actual dom content doesn't change. We call setState in componentWillReceiveProps but tinymce reads the 
    // old content.
    componentDidUpdate(prevProps, prevState) {
        if (this.props.questionState === 'formatContainer' && prevState.questionText !== this.state.questionText) {
            let textareaPath = `#${this.props.id}`,
                length = tinyMCE.editors.length;
            
            // Go through all the opened editors and search for the one that
            // had the same css path at init as the current textarea.
            for (let i = length; i > 0; i--) {
                try {
                    if (tinyMCE.editors[i-1].settings.selector === textareaPath) {
                        tinyMCE.editors[i-1].setContent(this.state.questionText);
                    }
                } catch (error) {
                    console.warn(error);
                    return;
                }
            }
        }
        
        // If we are on the editorContainer, reactivate autosize
        if (this.props.questionState === 'editContainer' && this.state.contentChanged === true) {
            try {
                autosize.update(this.getTextareaDomNode()[0]);
            } catch (error) {
                console.warn(error);
            }
            
            let textArea = document.getElementById(this.props.id);
            
            if (textArea && textArea.style.display === 'none') {
                textArea.style.display = '';
            }
        }

    }
    
    componentDidMount() {
        this.updateEditor();
        
        try {
            autosize(this.getTextareaDomNode()[0]); // After mounting, try activating the autosize for the textarea.
        } catch (error) {
            console.warn(error);
        }
    }
    
    render() {
	    return e('div', {
	    	className: 'text-entry-toggle-textbox-container question-text-setup-container',
	    	children: e('textarea', {
                id: this.props.id,
	    		style: 			{visibility: 'visible'},
				className: 		'textfield question-textfield text-entry-toggle-textbox',
				value: 			this.state.questionText,
				autoComplete: 	'off', 
				autoCorrect: 	'off', 
				autoCapitalize: 'off', 
				type: 			'text',
				maxLength: (this.props.higherLimit) ? '65000' : '10000',
				autoFocus: this.props.focus,
				onChange: (event) => {
                    this.setState({
                        questionText: event.target.value,
                        contentChanged: false
                    });
                    
                    this.props.questionTextOnChange(utils.escape(event.target.value.trim()));
                }
			})
		});
	}
	
}

class QuestionTextDisplayContainer extends React.Component {
    
    render() {
        let content = this.props.questionTextContent;
        content = content.replace(new RegExp('(\n)', 'g'), '<br>');
        
        return e('div', {
            className: 'text-entry-toggle-display-container',
            children : e('div', {
                className: 'question-explanation-wrapper',
                children: e('div', {
                    className: 'edit-question-text text-entry-toggle-display',
                    dangerouslySetInnerHTML: {__html: content}
                })
            })
        });
    }
    
}

export default class TextEntryToggle extends React.Component {
    
	render() {
        let content = null;
        
        if (this.props.questionState === 'displayContainer') {
            content = e(QuestionTextDisplayContainer, {
                questionTextContent: this.props.questionTextContent
            });

		} else if( _.contains(['editContainer', 'formatContainer'], this.props.questionState) ) {
			content = e(QuestionTextarea, {
                questionTextContent:  this.props.questionTextContent,
                questionTextOnChange: this.props.questionTextOnChange,
                questionState:        this.props.questionState,
                higherLimit:          this.props.higherLimit,
                id:                   uuid.v4(),
                focus:                this.props.focus
            });
		} else {
			throw 'error: ' + this.props.questionState // TODO .... This is ridiculous.
		}

        return content;
    }
    
}
