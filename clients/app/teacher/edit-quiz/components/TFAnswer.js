import React from 'react';
import _ from 'underscore';
import {translate} from 'translator';

let e = React.createElement;

export default class TFAnswer extends React.Component {
    
    constructor(props) {
        super(props);
        
        let trueAnswer = _.find(this.props.answers, (answer) => {
            return answer.get('text') === 'True';
        });
        
        let falseAnswer = _.find(this.props.answers, (answer) => {
            return answer.get('text') === 'False';
        });
        
        this.state = {
            trueAnswerSelected: !!trueAnswer.get('is_correct'),
            falseAnswerSelected: !!falseAnswer.get('is_correct')
        };
    }
    
    componentWillReceiveProps(newProps) {
        let trueAnswer = _.find(newProps.answers, function(answer) {
            return answer.get('text') === 'True';
        });
        
        let falseAnswer = _.find(newProps.answers, function(answer) {
            return answer.get('text') === 'False';
        });

        this.setState({
            trueAnswerSelected: !!trueAnswer.get('is_correct'),
            falseAnswerSelected: !!falseAnswer.get('is_correct')
        });
    }
    
    componentDidMount() {
        if (_.isUndefined( this.props.trueAnswer.get('is_correct'))) {
            this.props.updateQuestionAnswer(this.props.trueAnswer,  {is_correct: true});
            this.props.updateQuestionAnswer(this.props.falseAnswer, {is_correct: false});
        }
    }
    
    render() {
        return e('div', {
            className:'edit-question-answers-tf',
            children: [
                e('span', {
                    children: translate('Correct Answer') + ':'
                }),
                e('div', {
                    className: 'true-false-toggle',
                    children: [
                        e('button', { 
                            className: 'true-toggle ' + ( this.state.trueAnswerSelected ? 'true-false-toggle-on':'' ),
                            children: e('span', {
                                className:'true-toggle-text', children: translate('True')
                            }),
                            tabIndex: -1,
                            onClick: () => {
                                if (this.props.questionState === 'displayContainer') {
                                    return;
                                }
                        
                                // if the user clicks again on the same button, don't save anything because nothing changed
                                if (this.props.trueAnswer.get('is_correct') !== true) {
                                    
                                    // first update the view
                                    this.setState({
                                        trueAnswerSelected: true,
                                        falseAnswerSelected: false
                                    });
                        
                                    // save the true answer as correct
                                    this.props.updateQuestionAnswer( this.props.trueAnswer,  {is_correct: true} );
                                    this.props.updateQuestionAnswer( this.props.falseAnswer, {is_correct: false} );
                                }
                            }
                        }),
                        e('button', { 
                            className: 'false-toggle ' + ( this.state.falseAnswerSelected ? 'true-false-toggle-on':'' ),
                            children: e('span', {
                                className:'false-toggle-text', children: translate('False')
                            }),
                            tabIndex: -1,
                            onClick: () => {
                                if (this.props.questionState === 'displayContainer') {
                                    return;
                                }
                        
                                // If the user clicks again on the same button, don't save anything because nothing changed.
                                if (this.props.falseAnswer.get('is_correct') !== true) {
                                    this.setState({
                                        trueAnswerSelected: false,
                                        falseAnswerSelected: true
                                    });
                        
                                    this.props.updateQuestionAnswer(this.props.falseAnswer, {is_correct: true});
                                    this.props.updateQuestionAnswer(this.props.trueAnswer,  {is_correct: false});
                                }
                            }
                        })
                    ]
                })
            ]
        });
    }
    
}
