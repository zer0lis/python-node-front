import React from 'react';
import AnswerModel from 'AnswerModel';
import QuestionEditor from 'QuestionEditor';
import ResourceContainer from 'ResourceContainer';
import QuestionText from 'QuestionText';
import MCAnswer from 'MCAnswer';
import Explanation from 'Explanation';
import SwapQuestion from 'SwapQuestion';
import Utils from 'Utils';
import {translate} from 'translator';

let e = React.createElement;

export default class MCQuestion extends React.Component {
    
    constructor(props) {
        super(props);
        
        let question = this.props.question;
        
        if (question.answers.length === 0) {
            for (let i = 0; i < 5; ++i) {
                let answer = new AnswerModel({
                    text: '',
                    is_correct: false,
                    order: i + 1,
                });
                
                question.addAnswer(answer);
            }
        }
        
        this.state = {
            answers: question.answers,
            questionState: Utils.getNewQuestionState(question)
        };
    }
    
    componentWillReceiveProps(newProps) {
        let newState = {answers: newProps.question.answers},
            newQuestion = newProps.question;
        
        if (newQuestion.get('dirty') === true || newQuestion.get('duplicate') === true) {
            newQuestion.unset('duplicate');
            
            if (Utils.questionHasContent(newProps.question) && Utils.questionHasTags(newProps.question)) {
                newState.questionState = 'formatContainer';
            } else {
                newState.questionState = 'editContainer';
            }
        } else if (this.state.questionState !== 'displayContainer') {
            newState.questionState = 'displayContainer';
        }
        
        this.setState(newState);
    }
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            question = this.props.question,
            answersToRender = [];
        
        for (let answer of this.state.answers) {
            answersToRender.push(e(MCAnswer, {
                answer: answer,
                deleteAnswerCallback: (answerOrder) => {
                    c.deleteAnswer(this.props.question, answerOrder);
                    this.setState({
                        answers: this.props.question.answers
                    });
                },
                questionState: this.state.questionState,
                updateQuestionAnswer: (answer, answerChange) => c.updateAnswer(this.props.question, answer, answerChange)
            }));
        }

        return e('div', {
            id: 'question-' + question.get('order'),
            className: 'question edit-multiple-choice-question ' + (this.state.questionState !== 'displayContainer' ?  'edit-mode' : ''),
            children: [
                e('div', { 
                    className: 'edit-question-number clearfix', 
                    children: [
                        e('span', {
                            className: 'question-number multiple-choice-question',
                            children: '#' + question.get('order')
                        }),
                        e(QuestionEditor, Object.assign({
                            question: question,
                            questionOrder: question.get('order'),
                            questionState: this.state.questionState,
                            editQuestionCallback: (newState) => {
                                this.setState(newState);
                            },
                            formatToggleCallback: (newQuestionState) => {
                                this.setState({
                                    questionState: newQuestionState
                                });
                            },
                            questionHasContent: Utils.questionHasTags(question)
                        }, this.props))
                    ]
                }),
                
                e('div', {
                    className: 'clearfix',
                    children: [
                        e(ResourceContainer, Object.assign({
                            question: question,
                            questionState: this.state.questionState
                        }, this.props)),
                        e(QuestionText, {
                            questionTextContent: question.get('question_text'),
                            questionTextCallback: (newContent) => c.updateQuestion(this.props.question, {'question_text': newContent}),
                            questionTextState: this.state.questionState
                        })
                    ]
                }),
                e('div', {
                    className:'edit-question-answers', 
                    children: e('div', {
                        className: 'multiple-choice-menu',
                        children: [
                            e('div', {
                                className: 'clearfix',
                                children: [
                                    e('div', { 
                                        className: 'multiple-choice-header align-left', 
                                        children: translate('Answer Choice')
                                    }),
                                    e('div', { 
                                        className: 'multiple-choice-header align-right',
                                        style: {
                                            display: this.state.questionState === 'displayContainer' ? 'none' : 'block'
                                        },
                                        children: translate('Correct?')
                                    })
                                ]
                            }),
                            e('div', {
                                className: 'mc-choice-container clearfix',
                                children: answersToRender
                            }),
                            e('span', { 
                                className: 'add-choice',
                                style: {
                                    display: this.state.questionState === 'displayContainer' ? 'none' : 'inline-block'
                                },
                                children: [
                                    e('span', {
                                        className: 'add-choice-icon',
                                        children: '+ ' + translate('ADD ANSWER')
                                    })
                                ], 
                                onClick: () => {
                                    c.addAnswer(this.props.question);
                                    this.setState({
                                        answers: this.props.question.answers
                                    });
                                }
                            })
                        ]
                    })
                }),
                e(Explanation, {
                    questionExplanationContent: question.get('explanation'),
                    questionExplanationCallback: (newContent) => c.updateQuestion(this.props.question, {'explanation': newContent}),
                    questionState: this.state.questionState
                }),
                e(SwapQuestion, Object.assign({
                    hideButtons: m.questionInEditMode || this.state.questionState !== 'displayContainer'
                }, this.props))
            ]
        });
    }
    
}
