import React from 'react';
import _ from 'underscore';
import Select from 'Select';
import {translate} from 'translator';

const SUBJECT_MATH = 1;
const SUBJECT_SCIENCE = 2;
const SUBJECT_LANGUAGE_ARTS = 3;
const SUBJECT_SOCIAL_STUDIES = 4;

let e = React.createElement,
    c = null;

class NoAPIAccessError extends React.Component {
    
    render() {
        return e('div', {
            className: 'standard-container standards-no-access',
            children: [
                e('span', {
                    className: 'info-icon',
                    'data-icon': 'z'
                }),
                e('span', {
                    children: [
                        e('strong', {
                            children: translate('Standards are currently unavailable.')
                        }),
                        translate('Check back later or add a question to continue without standards.')
                    ]
                })
            ]
        });
    }
    
}

class StandardView extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            displayDescription: false
        };
    }
    
    render() {
        let standardDescription = this.props.standard['short_description'];

        return e('div', {
            className: 'standard-name-container',
            children: [
                e('div', {
                    children: [
                        e('strong', {
                            className: 'standard-name',
                            children: this.props.standard['name']
                        }),
                        e('span', {
                            className: 'standard-description',
                            dangerouslySetInnerHTML: {__html: standardDescription}
                        }),
                        e('div', {
                            className: 'standard-buttons',
                            children:[
                                e('button', {
                                    className: 'standard-change-button',
                                    children: [
                                        e('i', {
                                            'data-icon': 'C'
                                        }),
                                        translate('Change')
                                    ],
                                    onClick: c.editStandardClicked
                                }),
                                e('button', {
                                    className: 'standard-delete-button',
                                    children: [
                                        e('i', {
                                            'data-icon': 'L'
                                        }),
                                        translate('Delete')
                                    ],
                                    onClick: c.deleteStandardClicked
                                }),
                                e('button', {
                                    className: 'standard-show-description-button',
                                    children: e('i', {
                                        'data-icon': !this.state.displayDescription ? 'E' : 'F'
                                    }),
                                    onClick: () => {
                                        this.setState({
                                            displayDescription: !this.state.displayDescription
                                        });
                                    }
                                })
                            ]
                        })
                    ]
                }),
                e('p', {
                    className: 'second-standard-description' + (!this.state.displayDescription ? ' hidden' : ''),
                    dangerouslySetInnerHTML: {__html: standardDescription}
                }),
            ]
        });
    }
    
}

class StandardList extends React.Component {
    
    /**
     * When the standard is created from the blueprint, each of its child objects is null. In this case the loader
     * should not be displayed. After a selection is made, the next option value becomes an empty array, meaning
     * it is waiting for a list of values. In this case the loader should be displayed until the option receives
     * its values.
     * @param {string} name The name of the option
     * @returns {boolean} true if the loader should be shown, false otherwise
     */
    shouldShowLoadingIndicator(name) {
        let obj = this.props.standard.standardsObject[name];
        return _.isNull(obj) ? false : _.isEmpty(obj);
    }
    
    shouldBeDisabled(name) {
        return _.isNull(this.props.standard.standardsObject[name]);
    }
    
    createSubjectList() {
        let newStandard = this.props.standard,
            standardsObject = newStandard.standardsObject;
        
        let subjectList = {
            id: 'subject-select',
            name: 'subject',
            label: translate('Subject'),
            onSelect: (changed, newValue) => c.standardChanged(changed, newValue),
            children: []
        };
        
        _.each(standardsObject['subject'], function(value) {
            subjectList.children.push({
                value: value['id'],
                selected: newStandard['subject'] && value.id === newStandard['subject'],
                selectable: true,
                title: value['name']
            });
        });

        return subjectList;
    }
    
    createCoreList() {
        let newStandard = this.props.standard,
            standardsObject = newStandard.standardsObject,
            customList = [];
        
        let coreList = {
            id: 'core-select',
            name: 'core',
            label: translate('Select Core'),
            loading: this.shouldShowLoadingIndicator('core'),
            disabled: this.shouldBeDisabled('core'),
            opened: standardsObject.noResponseData === 'core',
            onSelect: (changed, newValue) => c.standardChanged(changed, newValue),
            children: []
        };

        if (newStandard['subject']) {
            let states = _.keys(standardsObject['core']);
            states.sort();
            
            _.each(states, (state) => {
                let value = standardsObject['core'][state],
                    options = [];
                
                _.each(value, (core) => {
                    options.push({
                        title: core['name'],
                        value: core['id'],
                        selectable: true,
                        selected: newStandard['core'] && core['id'] === String(newStandard['core'])
                    });
                });

                let listTitleEl = {
                    title: state,
                    selectable: false,
                    children: options
                };

                if (state === 'Common') {
                    customList.push(listTitleEl);
                } else if (state === 'Custom') {
                    // Ignore custom cores for now.
                } else {
                    coreList.children.push(listTitleEl);
                }
            });

            /**
             * After creating the common list of standards, we need them in a special order deppending on the subject.
             * Check to see if the list is empty. It's empty when the MC API is called and we're waiting for a response.
             */
            if (!_.isEmpty(customList)) {
                let sortCores = function(cores, coreIds) {
                    try {
                        let finalArray = [],
                            arr = cores;
                        
                        // Go through each coreid and group the common core list into two lists.
                        // The first one will have our core which will be added to the new array in our desired position
                        // The second half will have the rest of the cores.
                        // If necessary go through the remaining list of cores again.
                        _.each(coreIds, function(coreid) {
                            let good = _.groupBy(arr, function(core) {return String(core['value']) === coreid});
                            
                            if (good['true']) {
                                finalArray.push(good['true'][0]);
                            }
                            
                            arr = good['false'];
                        });

                        // If there are cores left, just add them to the end. The order here doesn't matter.
                        if (_.isArray(arr)) {
                            finalArray = finalArray.concat(arr);
                        }

                        return finalArray
                    } catch (error) {
                        console.warn('Error reordering cores:', error);
                        return cores;
                    }
                };

                if (newStandard['subject'] === SUBJECT_MATH) {
                    /**
                     * the common core should have the following order
                     * 4        - Traditional
                     * 6        - Integrated
                     * 1774     - Spanish: Matematicas (2014)
                     * 596      - High School Standards by Domain
                     */
                    customList[0].children = sortCores(customList[0].children, ['4', '6', '1774', '596']);
                } else if (newStandard['subject'] === SUBJECT_SCIENCE) {
                    /**
                     * the common core should have the following order
                     * 143  - Science and Technical Studies
                     * 1773 - Lecto-escritura para Ciencias y Materias Técnicas (2013)
                     */
                    customList[0].children = sortCores(customList[0].children, ['143', '1773']);
                } else if (newStandard['subject'] === SUBJECT_LANGUAGE_ARTS) {
                    /**
                     * the common core should have the following order
                     * 5        - Language Arts
                     * 1752     - Artes del Lenguaje en Español (2013)
                     */
                    customList[0].children = sortCores(customList[0].children, ['5', '1752']);
                } else if (newStandard['subject'] === SUBJECT_SOCIAL_STUDIES) {
                    /**
                     * the common core should have the following order
                     * 142      - History/Social Studies
                     * 1772     - Lecto-escritura en Historia y Estudios Sociales (2013)
                     */
                    customList[0].children = sortCores(customList[0].children, ['142', '1772']);
                }
            }
            
            coreList.children = _.union(customList, coreList.children);
        }
        
        return coreList;
    }
    
    createGradeList() {
        let newStandard = this.props.standard,
            standardsObject = newStandard.standardsObject;
        
        let classList = {
            id: 'grade-select',
            name: 'class',
            label: translate('Select Grade'),
            loading: this.shouldShowLoadingIndicator('class'),
            disabled: this.shouldBeDisabled('class'),
            opened: standardsObject.noResponseData === 'class',
            onSelect: (changed, newValue) => c.standardChanged(changed, newValue),
            children: []
        };
        
        if (newStandard['core']) {
            _.each( standardsObject['class'], function(value) {
                classList.children.push({
                    value: value['class_objective']['id'],
                    title: value['class_objective']['name'],
                    selectable: true,
                    selected: newStandard['class'] && value['class_objective']['id'] === String( newStandard['class'] )
                });
            });
        }
        
        return classList;
    }
    
    createStandardsList() {
        let newStandard = this.props.standard,
            standardsObject = newStandard.standardsObject;
        
        let standardsList = {
            id: 'standard-select',
            name: 'standard',
            label: translate('Select Standard'),
            loading: this.shouldShowLoadingIndicator('standard'),
            disabled: this.shouldBeDisabled('standard'),
            opened: standardsObject.noResponseData === 'standard',
            onSelect: (changed, newValue) => c.standardChanged(changed, newValue),
            children: []
        };
        
        if (newStandard['class']) {
            _.each(standardsObject['standard'], function(value) {
                standardsList.children.push({
                    title: String(value['name']).toUpperCase(),
                    value: value['id'],
                    description: value['short_description'],
                    selectable: true,
                    selected: newStandard['standard'] && value['id'] === String(newStandard['standard']['id'])
                });
            });
        }
        
        if (standardsList.children.length < 1) {
            standardsList.disabled = true;
        }
        
        return standardsList;
    }

    render() {
        let newStandard = this.props.standard,
            coreList = this.createCoreList(),
            classList = this.createGradeList(),
            standardsList = this.createStandardsList();
        
        return e('ul', {
            style: {display: this.props.standard ? 'block' : 'none'},
            children: [
                e('li', {
                    children: [
                        e('p', {
                            className: 'cores-title',
                            children: translate('Subject')
                        }),
                        e(Select, this.createSubjectList())
                    ]
                }),
                e('li', {
                    className: coreList.disabled ? 'disabled-list' : '',
                    children: [
                        e('p', {
                            className: 'cores-title',
                            children: translate('Core')
                        }),
                        e(Select, coreList)
                    ]
                }),
                e('li', {
                    className: classList.disabled ? 'disabled-list' : '',
                    children: [
                        e('p', {
                            className: 'cores-title',
                            children: translate('Class/Grade')
                        }),
                        e(Select, classList)
                    ]
                }),
                e('li', {
                    className: standardsList.disabled ? 'disabled-list' : '',
                    children: [
                        e('p', {
                            className: 'cores-title',
                            children: translate('Standard')
                        }),
                        e(Select, standardsList)
                    ]
                }),
                e('li', {
                    style: {
                        // If the standard is complete but dirty, it is in edit mode. In this case, show the buttons.
                        display: (!newStandard.complete && newStandard.dirty) ? 'none' : 'block'
                    },
                    children: [
                        e('button', {
                            className: 'button button-primary button-small',
                            children: translate('Save'),
                            onClick: () => c.standardChanged('save', null)
                        }),
                        e('button', {
                            className: 'button button-link standards-cancel-button',
                            children: translate('Cancel'),
                            onClick: c.cancelEditStandardClicked
                        }),
                    ]
                }),

            ]
        });
    }
    
}

export default class Standards extends React.Component {
    
    render() {
        c = this.props.controller;
        
        let standard = this.props.standard;

        // We have a special flag set when we have an API problem. Simply remove all options.
        if (standard && standard.noAccess === true) {
            return e(NoAPIAccessError);
        }

        // If the quiz has a complete standard attached to it, display that.
        if (standard && standard.complete === true && standard.dirty !== true) {
            return e(StandardView, {
                standard: standard['standard'],
            });
        }

        // If the quiz has an empty standard attached, the user wants to attach one. In this case show the drop downs.
        let newStandard = null;
        
        if (this.props.standard) {
            newStandard = e(StandardList, this.props);
        }

        return e('div', {
            className: 'standard-container form',
            children: [
                e('div', {
                    id: 'standard-input-container',
                    children: [
                        e('input', {
                            id: 'standards-checkbox',
                            checked: this.props.standard,
                            type: 'checkbox',
                            onChange: (event) => c.alignToStandardChanged(event.target.checked)
                        }),
                        e('label', {
                            htmlFor:'standards-checkbox',
                            children: translate('Align quiz to standard')
                        })
                    ]
                }),
                newStandard
            ]
        });
    }
    
}
