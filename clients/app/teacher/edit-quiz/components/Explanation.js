import React from 'react';
import TextEntryToggle from 'TextEntryToggle';
import {translate} from 'translator';

let e = React.createElement;

export default class Explanation extends React.Component {
    
    render() {
        // Only render the explanation if it is not empty, and if the current question is not in display mode.
        if (this.props.questionExplanationContent !== '' || this.props.questionState !== 'displayContainer') {
            return e('div', {
                children:[
                    e('span', {
                        className:'explanation-title',
                        children: translate('Explanation') + ':'
                    }),
                    e('div', {
                        className: 'edit-question-explanation text-entry-toggle',
                        children: e(TextEntryToggle, {
                            questionTextContent: this.props.questionExplanationContent,
                            questionTextOnChange: this.props.questionExplanationCallback,
                            questionState: this.props.questionState,
                            higherLimit: true
                        })
                    })
                ]
            });
        }
        
        return e('div', {});
    }
    
}
