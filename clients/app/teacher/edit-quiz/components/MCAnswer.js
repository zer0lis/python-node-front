import React from 'react';
import TextEntryToggle from 'TextEntryToggle';

let e = React.createElement;

class MCAnswerTextEntryRightSide extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            value: (!!this.props.checked) || false
        };
    }
    
    componentWillReceiveProps(newProps) {
        this.setState({
            value: newProps.checked
        });
    }
    
    render() {
        return e('div', {
            className: 'mc-answer-right-side',
            children: [
                e('div', { 
                    className:'edit-mc-answer-correctness',
                    children: e('input', {
                        className: 'mc-correct-checkbox',
                        type: 'checkbox',
                        onChange: (event) => {
                            this.setState({
                                value: event.target.checked
                            });
                            this.props.answerSelectCorrect(event);
                        },
                        tabIndex: -1,
                        checked: this.state.value
                    })
                }),
                e('span', {
                    className: 'delete-option-answer',
                    onClick: () => this.props.deleteAnswerCallback(this.props.answerOrder),
                    children: e('i', {'data-icon': 'h'})
                })
            ]
        });
    }
    
}

export default class MCAnswer extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            questionState: this.props.questionState
        };
    }
    
    componentWillReceiveProps() {
        this.setState({
            questionState: 'editContainer'
        });
    }
    
    convertToNumbering(number) {
        let letters = '';
        
        if (number > 26) {
            let baseChar = ('A').charCodeAt(0);

            do {
                number -= 1;
                letters = String.fromCharCode(baseChar + (number % 26)) + letters;
                number = (number / 26) >> 0;
            } while(number > 0);
        } else {
            letters = String.fromCharCode('A'.charCodeAt(0) + number - 1);
        }
        
        return letters;
    }
    
    render() {
        let answer = this.props.answer,
            htmlContent = null;
        
        // Show the right side buttons only if the question is in edit mode.
        if (this.props.questionState !== 'displayContainer') {
            htmlContent = e(MCAnswerTextEntryRightSide, {
                checked: this.props.answer.get('is_correct'),
                answerSelectCorrect: (event) => {
                    this.props.updateQuestionAnswer(this.props.answer, {is_correct: event.target.checked});
                    this.setState({
                        is_correct: event.target.checked
                    });
                },
                questionState: this.props.questionState,
                answerOrder: this.props.answer.get('order'),
                deleteAnswerCallback: (answerOrder) => this.props.deleteAnswerCallback(answerOrder)
            });
        }

        return e('div', {
            className: 'mc-answer mc-answer-' + answer.get('order') + (this.props.answer.get('is_correct')  ? ' is-correct' : ''),
            children: [
                e('span', { 
                    className: 'edit-question-number', 
                    children: this.convertToNumbering(answer.get('order'))
                }),
                e(TextEntryToggle, {
                    questionTextContent: this.props.answer.get('text'),
                    questionTextOnChange: (newContent) => this.props.updateQuestionAnswer(this.props.answer, {text: newContent}),
                    questionState: this.props.questionState
                }),
                htmlContent
            ]
        });
    }
    
}
