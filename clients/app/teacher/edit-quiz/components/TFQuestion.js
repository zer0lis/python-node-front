import React from 'react';
import _ from 'underscore';
import AnswerModel from 'AnswerModel';
import QuestionEditor from 'QuestionEditor';
import ResourceContainer from 'ResourceContainer';
import QuestionText from 'QuestionText';
import TFAnswer from 'TFAnswer';
import Explanation from 'Explanation';
import SwapQuestion from 'SwapQuestion';
import Utils from 'Utils';

let e = React.createElement;

export default class TFQuestion extends React.Component {
    
    constructor(props) {
        super(props);
        
        let question = this.props.question;
        
        let answerTrue = _.find(question.answers, (answer) => {
            return answer.get('text') === 'True';
        });
        
        let answerFalse = _.find(question.answers, (answer) =>{
            return answer.get('text') === 'False';
        });

        if (_.isUndefined(answerTrue)) {
            answerTrue = new AnswerModel({
                text: 'True',
                order: 1,
                is_correct: true
            });
            
            question.addAnswer(answerTrue);
        }
        
        if (_.isUndefined(answerFalse)) {
            answerFalse = new AnswerModel({
                text: 'False',
                order: 2,
                is_correct: false
            });
            
            question.addAnswer(answerFalse);
        }
        
        this.state = {
            answers: question.answers,
            questionState: Utils.getNewQuestionState(question)
        };
    }
    
    componentWillReceiveProps(newProps) {
        let newState = {answers: newProps.question.answers},
            newQuestion = newProps.question;
        
        if (newQuestion.get('dirty') === true || newQuestion.get('duplicate') === true) {
            newQuestion.unset('duplicate');
            
            if (Utils.questionHasContent(newProps.question) && Utils.questionHasTags(newProps.question)) {
                newState.questionState = 'formatContainer';
            } else {
                newState.questionState = 'editContainer';
            }
        } else if (this.state.questionState !== 'displayContainer') {
            newState.questionState = 'displayContainer';
        }
        
        this.setState(newState);
    }
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            question = this.props.question;
        
        let trueAnswer = _.find(this.state.answers, function(answer) {
            return answer.get('text') === 'True';
        });
        
        let falseAnswer = _.find(this.state.answers, function(answer) {
            return answer.get('text') === 'False';
        });

        return e('div', {
            id: 'question-' + question.get('order'),
            className: 'question edit-true-false-question ' + ((this.state.questionState !== 'displayContainer') ? 'edit-mode' : ''),
            children: [
                e('div', {
                    className: 'edit-question-number clearfix', 
                    children: [
                        e('span', {
                            className: 'question-number true-false-question',
                            children: '#' + question.get('order')
                        }),
                        e(QuestionEditor, Object.assign({
                            question: question,
                            questionOrder: question.get('order'),
                            questionState: this.state.questionState,
                            editQuestionCallback: (newState) => {
                                this.setState(newState);
                            },
                            formatToggleCallback: (newQuestionState) => {
                                this.setState({
                                    questionState: newQuestionState
                                });
                            },
                            questionHasContent: Utils.questionHasTags(question)
                        }, this.props))
                    ]
                }),
                e('div', {
                    className: 'clearfix',
                    children: [
                        e(ResourceContainer, Object.assign({
                            question: question,
                            questionState: this.state.questionState
                        }, this.props)),
                        e(QuestionText, {
                            questionTextContent: question.get('question_text'),
                            questionTextCallback: (newContent) => c.updateQuestion(this.props.question, {'question_text': newContent}),
                            questionTextState: this.state.questionState
                        })
                    ]
                }),
                e(TFAnswer, {
                    answers: this.state.answers,
                    trueAnswer: trueAnswer,
                    falseAnswer: falseAnswer,
                    questionState: this.state.questionState,
                    updateQuestionAnswer: (answer, answerChange) => c.updateAnswer(this.props.question, answer, answerChange)
                }),
                e(Explanation, {
                    questionExplanationContent: question.get('explanation'),
                    questionExplanationCallback: (newContent) => c.updateQuestion(this.props.question, {'explanation': newContent}),
                    questionState: this.state.questionState
                }),
                e(SwapQuestion, Object.assign({
                    hideButtons: m.questionInEditMode || this.state.questionState !== 'displayContainer'
                }, this.props))
            ]
        });
    }
    
}
