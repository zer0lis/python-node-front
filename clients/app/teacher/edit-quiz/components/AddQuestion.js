import React from 'react';
import PlusIcon from 'PlusIcon';
import {translate} from 'translator';

let e = React.createElement;

export default class AddQuestion extends React.Component {
    
	render() {
	    let c = this.props.controller;
	    
		return e('div', {
			className: 'edit-quiz-header',
			children: [
				e('h4', {
					id:'add-quiz-objects-title',
					children: translate('QUESTIONS')
				}),
				e('div', {
					id: 'add-question-container',
					children: [
                        e('button', {
                            className: 'add-question-button add-mc-question-button',
                            children: [
                                e(PlusIcon, {
                                    innerClass: 'add-mc-question-plus-icon-inner',
                                    outerClass: 'add-mc-question-plus-icon'
                                }),
                                translate('MULTIPLE CHOICE')
                            ],
                            onClick: () => c.addQuestion('MC')
                        }),
                        e('button', {
                            className: 'add-question-button add-tf-question-button',
                            children: [
                                e(PlusIcon, {
                                    innerClass: 'add-tf-question-plus-icon-inner',
                                    outerClass: 'add-tf-question-plus-icon'
                                }),
                                translate('TRUE / FALSE')
                            ],
                            onClick: () => c.addQuestion('TF')
                        }),
                        e('button', {
                            className: 'add-question-button add-sa-question-button',
                            children: [
                                e(PlusIcon, {
                                    innerClass: 'add-sa-question-plus-icon-inner',
                                    outerClass: 'add-sa-question-plus-icon'
                                }),
                                translate('SHORT ANSWER')
                            ],
                            onClick:  () => c.addQuestion('FR')
                        })
					]
				})
			]
		});
	}
    
}
