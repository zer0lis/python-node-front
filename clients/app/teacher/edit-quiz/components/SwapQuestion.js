import React from 'react';

let e = React.createElement;

export default class SwapQuestion extends React.Component {
    
    render() {
        let c = this.props.controller,
            question = this.props.question,
            order = question.get('order');
        
        return e('div', {
            className:'relocate-question-container',
            children: [
                e('button', { 
                    className: 'delete-button button', 
                    children: e('i', {'data-icon': 'w'}),
                    tabIndex: -1,
                    onClick: () => c.deleteQuestion(question)
                }),
                e('button', { 
                    className: 'move-up-button button', 
                    children: e('i', {'data-icon': 'k'}),
                    onClick: () => c.changeQuestionOrder(order, order - 1),
                    disabled: this.props.hideButtons ? 'disabled' : ''
                }),
                e('button', { 
                    className: 'move-down-button button',
                    children: e('i', {'data-icon': 'm'}),
                    onClick: () => c.changeQuestionOrder(order, order + 1),
                    disabled: this.props.hideButtons ? 'disabled' : ''
                }),
                e('button', { 
                    className: 'duplicate-question-button button',
                    children: e('i', {'data-icon': 'y'}),
                    onClick: () => c.copyQuestion(question),
                    disabled: this.props.hideButtons ? 'disabled' : ''
                })
            ]
        })
    }
    
}
