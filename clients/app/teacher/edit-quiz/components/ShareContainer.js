import React from 'react';
import {translate} from 'translator';

let e = React.createElement;

export default class ShareContainer extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            sharable: this.props.model.quiz.get('sharable')
        };
    }
    
    render() {
        return e('div', {
            id: 'soc-number-container',
            className: 'clearfix align-right',
            children: [ 
                e('div', {
                    className: 'toggle-slider align-right',
                    id: 'soc-number-button',
                    children: [
                        e('input', {
                            type: 'checkbox',
                            id: 'share-quiz',
                            className: 'toggle-slider-checkbox',
                            checked: this.state.sharable,
                            onChange: () => {
                                this.props.controller.shareButtonClicked();
                                this.setState({
                                    sharable: !this.state.sharable
                                });
                            }
                        }),
                        e('label', {
                            className: 'toggle-slider-label',
                            htmlFor: 'share-quiz',
                            children: [
                                e('div', {
                                    className: 'toggle-slider-inner'
                                }),
                                e('div', {
                                    className: 'toggle-slider-switch'
                                })
                            ]
                        })
                    ]
                }),
                e('div', {
                    id: 'soc-container',
                    style: { 
                        opacity: (this.state.sharable) ? 1 : 0.75
                    },
                    children: [
                        e('label', {
                            id: 'soc-number-label',
                            htmlFor: 'share-quiz',
                            children: [
                                translate('Enable Sharing'),
                                e('span', {id: 'soc-number'}, 'SOC-' + this.props.model.quiz.get('soc_number'))
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
