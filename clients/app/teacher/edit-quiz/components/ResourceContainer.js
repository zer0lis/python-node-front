import React from 'react';
import _ from 'underscore';
import popup from 'PopupController';
import utils from 'Utils';
import {translate} from 'translator';

let e = React.createElement;

class QuestionResourceContainer extends React.Component {
    
    componentWillReceiveProps(newProps){
        return {resourceUrl: newProps.resourceUrl}
    }
    
    render() {
        return e('div', {
            className: 'image-container',
            children : [
                e('img', { 
                    id: 'question-image-preview',
                    src: utils.convertImageUrl(this.props.resourceUrl) || this.props.resourceUrl
                }),
                e('span', {
                    className: 'remove-resource-button',
                    children: e('i', {'data-icon': 'w'}),
                    onClick: this.props.deleteImageClicked
                })
            ]
        })
    }
    
}

export default class ResourceContainer extends React.Component {
    
    constructor(props) {
        super(props);
        
        let question = this.props.question;
        
        let state = {
            resourceContainerContent: 'addQuestionResourceButton'
        };
        
        if (!_.isUndefined(question.resources) && question.resources.length > 0) {
            state = {
                resourceUrl: question.resources[0].get('url'), 
                resourceContainerContent:   'removeQuestionResourceButton'
            };
        }
        
        this.state = state;
    }
    
    componentWillReceiveProps(newProps) {
        if (!_.isUndefined( newProps.question.resources ) && newProps.question.resources.length > 0) {
            this.setState({ 
                resourceContainerContent:   'removeQuestionResourceButton',
                resourceUrl:                newProps.question.resources[0].get('url')
            });
        } else {
            this.setState({ 
                resourceContainerContent: 'addQuestionResourceButton',
                resourceUrl: ''
            });
        }
    }
    
    render() {
        let c = this.props.controller,
            children = null;
        
        if (this.props.questionState === 'displayContainer') {
            children = e('span');
            
            if (this.state.resourceUrl) {
                children = e('div', {
                    className: 'add-resource-button',
                    children: e('img', {
                        className: 'question-image-preview',
                        src: utils.convertImageUrl(this.state.resourceUrl) || this.state.resourceUrl
                    })
                });
            }
        } else {
            if (this.state.resourceContainerContent === 'addQuestionResourceButton') {
                children = e('div', {
                    className: 'add-resource-button',
                    children: [
                        e('span', { 
                            children: [
                                e('i', {'data-icon': 'e'}),
                                e('i', {
                                    className: 'add-image-button',
                                    'data-icon': 'u'
                                })
                            ] 
                        }),
                        e('input', {
                            className: 'add-resource-input',
                            type:'file', 
                            accept:'image/*',
                            onChange: (event) => {
                                let isImage = false;
                                
                                if (event.target.files) {
                                    isImage = event.target.files[0].type.split('/')[0] === 'image';
                                } else {
                                    let imageExtention = _.last(event.target.value.split('.'));
                                    isImage = _.contains(['png', 'jpg', 'jpeg', 'gif', 'bmp', 'tiff'], imageExtention)
                                }
                                
                                if (!isImage) {
                                    popup.render({
                                        title: translate('Missing Image'),
                                        message: translate('Please upload an image.')
                                    });
                                    
                                    return;
                                }
                                
                                c.imageSelected(event, this.props.question, (url) => {
                                    this.setState({
                                        resourceUrl: url,
                                        resourceContainerContent: 'removeQuestionResourceButton'
                                    });
                                });
                            },
                            onClick: (event) => {
                                let ua = navigator.userAgent,
                                    matches = ua.match(/^.*(iPad).*(OS\s[0-9]).*(CriOS|Version)\/[.0-9]*\sMobile.*$/i);
                        
                                // Upload doesn't work in the ios app.
                                if (matches && matches[3] !== 'CriOS' && ua.indexOf('Safari') === -1) {
                                    event.preventDefault();
                                    popup.render({
                                        title: translate('Unavailable'),
                                        message: translate('This feature is unavailable on iPad. Please upload images using a browser or a different device.')
                                    });
                                }
                        
                                // Check for android 4.4.2 and let the user know that the file upload is not working and it's Google's fault
                                if (ua.match(/Android\s+([\d\.]+)/) && ua.match(/Android\s+([\d\.]+)/)[1] === '4.4.2') {
                                    event.preventDefault();
                                    popup.render({
                                        title: translate('Unavailable'),
                                        message: translate('This feature is unavailable on your version of Android. Please upload images using a browser or a different device.')
                                    });
                                }
                            },
                        })
                    ]
                });
            } else if (this.state.resourceContainerContent === 'removeQuestionResourceButton') {
                children = e(QuestionResourceContainer, {
                    resourceUrl: this.state.resourceUrl,
                    deleteImageClicked: () => {
                        c.deleteImage(this.props.question, () => {
                            this.setState({resourceContainerContent: 'addQuestionResourceButton'});
                        });
                    }
                });
            } else if (this.state.resourceContainerContent === 'loadingIndicator') {
                children = e('div', {
                    id: 'question-image-preview-loading',
                    children : [
                        e('img', { 
                            src: window.static_url + 'img/loading-indicator.gif' 
                        }),
                        e('p', { 
                            children: translate('Loading') + '...'
                        })
                    ]
                });
            } else {
                throw 'odd state found' // TODO .... This is ridiculous garbage.
            }
        }
        
        return e('div', {
            className:'resources-container align-left',
            children: children
        });
    }
    
}
