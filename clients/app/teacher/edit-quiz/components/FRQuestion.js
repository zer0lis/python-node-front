import React from 'react';
import QuestionEditor from 'QuestionEditor';
import ResourceContainer from 'ResourceContainer';
import QuestionText from 'QuestionText';
import FRAnswer from 'FRAnswer';
import Explanation from 'Explanation';
import SwapQuestion from 'SwapQuestion';
import Utils from 'Utils';
import {translate} from 'translator';

let e = React.createElement;

export default class FRQuestion extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            answers: this.props.question.answers,
            questionState: Utils.getNewQuestionState(this.props.question)
        };
    }
    
    componentWillReceiveProps(newProps) {
        let newState = {answers: newProps.question.answers},
            newQuestion = newProps.question;
        
        if (newQuestion.get('dirty') === true || newQuestion.get('duplicate') === true) {
            newQuestion.unset('duplicate');
            
            if (Utils.questionHasContent(newProps.question) && Utils.questionHasTags(newProps.question)) {
                newState.questionState = 'formatContainer';
            } else {
                newState.questionState = 'editContainer';
            }
        } else if (this.state.questionState !== 'displayContainer') {
            newState.questionState = 'displayContainer';
        }
        
        this.setState(newState);
    }
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            question = this.props.question,
            answersToRender = [];
        
        for (let answer of question.answers) {
            answersToRender.push(e(FRAnswer, {
                answer: answer,
                questionState: this.state.questionState,
                updateQuestionAnswer: (answer, answerChange) => c.updateAnswer(this.props.question, answer, answerChange)
            }));
        }

        return e('div', {
            id: 'question-' + question.get('order'),
            className: 'question edit-free-response-question ' + (this.state.questionState === 'displayContainer' ? '' : ' edit-mode'),
            children: [
                e('div', { 
                    className: 'edit-question-number clearfix', 
                    children: [
                        e('span', {
                            className: 'question-number short-answer-question',
                            children: '#' + question.get('order')
                        }),
                        e(QuestionEditor, Object.assign({
                            question: question,
                            questionOrder: question.get('order'),
                            questionState: this.state.questionState,
                            editQuestionCallback: (newState) => {
                                this.setState(newState);
                            },
                            formatToggleCallback: (newQuestionState) => {
                                this.setState({
                                    questionState: newQuestionState
                                });
                            },
                            questionHasContent: Utils.questionHasTags(question)
                        }, this.props))
                    ]
                }),
                e('div', {
                    className: 'clearfix',
                    children: [
                        e(ResourceContainer, Object.assign({
                            question: question,
                            questionState: this.state.questionState
                        }, this.props)),
                        e(QuestionText, {
                            questionTextContent: question.get('question_text'),
                            questionTextCallback: (newContent) => c.updateQuestion(this.props.question, {'question_text': newContent}),
                            questionTextState: this.state.questionState
                        }),
                    ]
                }),
                e('div', {
                    className:'edit-question-answers', 
                    children: e('div', {
                        className: 'free-response-menu',
                        children: [
                            e('div', {
                                style: {
                                    display: this.state.questionState === 'displayContainer' ? 'none' : 'block'
                                },
                                className:'menu-header', 
                                children: e('span', {
                                    children: translate('Correct Answers (Optional)')
                                }) 
                            }),
                            e('form', {
                                className:'fr-answers-container form',
                                children: answersToRender
                            }),
                            e('div', {
                                style: {
                                    display: this.state.questionState === 'displayContainer' ? 'none' : 'block'
                                },
                                children: [
                                    e('button', {
                                        className: 'add-choice-button button button-small',
                                        children: '+' + translate('Add'),
                                        tabIndex: -1,
                                        onClick: () => {
                                            c.addAnswer(this.props.question);
                                            this.setState({
                                                answers: this.props.question.answers
                                            });
                                        }
                                    }),
                                    e('button', {
                                        className: 'remove-choice-button button button-small',
                                        children: '-' + translate('Delete'),
                                        tabIndex: -1,
                                        onClick: (answerOrder) => {
                                            c.deleteAnswer(this.props.question, answerOrder);
                                            this.setState({
                                                answers: this.props.question.answers
                                            });
                                        }
                                    })
                                ]
                            })
                        ]
                    })
                }),
                e(Explanation, {
                    questionExplanationContent: question.get('explanation'),
                    questionExplanationCallback: (newContent) => c.updateQuestion(this.props.question, {'explanation': newContent}),
                    questionState: this.state.questionState
                }), 
                e(SwapQuestion, Object.assign({
                    hideButtons: m.questionInEditMode || this.state.questionState !== 'displayContainer'
                }, this.props))
            ]
        });
    }
    
}

