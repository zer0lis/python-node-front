let setUpTinyMCE = function(absolutePath, saveFunctionCallback) {
	// valid_elements: blockquote, emphasis, em, strong, b, sub, sup, br, p, u, strike, span[style='text-decoration:underline'],
	tinymce.init({
		selector: absolutePath,
	    theme: 'modern',
		toolbar: 'bold italic underline | subscript superscript',
		font_formats: 'Arial=arial,helvetica,sans-serif;',
		valid_elements: 'blockquote,emphasis,em,strong,b,sub,sup,br,p,u,strike,span[class=underline]',
		formats: {
			underline : {inline : 'span', 'classes' : 'underline', exact : true}
		},
		plugins : 'paste',
		paste_as_text: true,
	    save_enablewhendirty: false,
	    menubar: false,
	    statusbar: false,
	    content_css : window.libs_url + 'tinymce/skins/lightgray/content.min.css',
	    setup: function(ed) {
	    	ed.on('change', function(e) {
	    		saveFunctionCallback(e.level.content.trim());
	    	});
	    }
	});
};

module.exports = setUpTinyMCE;
