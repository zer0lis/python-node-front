import React from 'react';
import Utils from 'Utils';

let e = React.createElement;

class FRAnswerInput extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            value: Utils.unescape(this.props.frAnswer)
        }
    }
    
    componentWillReceiveProps(nextProps) {
        this.setState({
            value: Utils.unescape(nextProps.frAnswer)
        });
    }
    
    componentDidMount() {
        if (this.state.value === '') {
            this.input.focus();
        }
    }
    
    render() {
        return e('input', {
            type: 'text',
            className: 'textfield text-entry-toggle-textbox',
            value: this.state.value,
            maxLength: 255,
            autoComplete: 'off',
            autoCorrect: 'off',
            autoCapitalize: 'off',
            disabled: this.props.questionState === 'displayContainer',
            ref: (input) => this.input = input,
            onChange: (event) => {
                this.setState({
                    value: event.target.value
                });
                this.props.frAnswerOnChange(Utils.escape(event.target.value));
            }
        });
    }
    
}

export default class FRAnswer extends React.Component {
    
    render() {
        let answer = this.props.answer;
        
        return e('div', {
            className: 'answer fr-answer-' + answer.get('order'),
            children: e('div', {
                className: 'text-entry-toggle',
                children: e('div', {
                    className: 'text-entry-toggle-textbox-container',
                    children: e(FRAnswerInput, {
                        questionState: this.props.questionState,
                        frAnswer: answer.get('text'),
                        frAnswerOnChange: (newContent) => {
                            if (newContent !== this.props.answer.get('text')) {
                                this.props.updateQuestionAnswer(this.props.answer, {text: newContent, is_correct: true});
                            }
                        }
                    })
                })
            })
        });
    }
    
}
