import React from 'react';
import _ from 'underscore';
import QuizName from 'QuizName';
import ShareContainer from 'ShareContainer';
import Standards from 'Standards';
import MCQuestion from 'MCQuestion';
import TFQuestion from 'TFQuestion';
import FRQuestion from 'FRQuestion';
import AddQuestion from 'AddQuestion';
import {translate} from 'translator';

let e = React.createElement;

export default class EditQuizView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            questionViews = [];
        
        m.quiz.questions.forEach((question, index) => {
            let questionNum = index + 1;
            
            question.set('question_number', questionNum);
            
            let questionObject = Object.assign({
                question: question
            }, this.props);
            
            let questionType = question.get('type').toUpperCase(),
                questionView = null;
            
            if (questionType === 'MC') {
                questionView = e(MCQuestion, questionObject);
            } else if (questionType === 'TF') {
                questionView = e(TFQuestion, questionObject);
            } else {
                questionView = e(FRQuestion, questionObject);
            }
            
            questionViews.push(e('div', {
                id: 'question' + questionNum,
                children: questionView
            }));
        });
        
        let children = [];
        
        children.push(
            e('div', {
                className: 'header-action-button-container',
                children: [
                    e('span', {
                        className: 'align-left create-edit-title',
                        children: _.isNull(m.quiz.get('last_updated')) ? translate('Create Quiz') : translate('Edit Quiz')
                    }),
                    e('div', {
                        className: 'align-right button-container',
                        children: [
                            e('button', {
                                id: 'saveAndExitButton',
                                className: 'button-primary button-large pill button',
                                children: [
                                    e('span', {
                                        children: translate('SAVE & EXIT')
                                    })
                                ],
                                type: 'button',
                                onClick: c.saveAndExitClicked
                            })
                        ]
                    })
                ]
            }),
            e('div', {
                className: 'edit-quiz-header',
                children: [
                    e(ShareContainer, this.props)
                ]
            }),
            e(QuizName, this.props),
            e('div', {
                style: {clear: 'both'}
            }),
            e(Standards, this.props),
            e('div', {
                id: 'questions',
                children: questionViews
            }),
            e(AddQuestion, this.props)
        );
        
        return e('div', {
            id: 'edit-quiz-container',
            children: children
        });
    }
    
}
