let Backbone = require('backbone'),
    localUser = require('user'),
    question = require('LegacyQuestionModel');

let MediaResourceModel = Backbone.Model.extend({
    
    urlRoot: function(){
        if (localUser.isTeacher()) {
            return window.backend_host + '/common/api/media-resources/{0}/'.format(question.get('question_id'));
        } else {
            return window.backend_host + '/common/api/media-resources/';
        }
    }
    
});

module.exports = MediaResourceModel;
