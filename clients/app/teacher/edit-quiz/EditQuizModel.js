let Backbone    = require('backbone'),
    _           = require('underscore'),
    Quiz        = require('quiz-model'),
    Question    = require('LegacyQuestionModel'),
    popup       = require('PopupController'),
    translate = require('translator').translate,
    request     = require('Request');

let mockStandard = null;

let EditQuizModel = Backbone.Model.extend({
    
    currentUrl: '',
    saveAndExitClicked: false,
    questionInEditMode: false,
    
    reset: function() {
        this.saveAndExitClicked = false;
    },
    
    getQuiz: function(socNumber, successCallback, errorCallback) {
        if (socNumber) {
            request.get({
                url: `${window.backend_host}/quizzes/api/get-quiz-by-soc/${socNumber}`,
                success: (data) => {
                    this.quiz = new Quiz(data);
                    
		            if (this.quiz.standard) {
		                this.quiz.standard.standardsObject = _.clone(this.blueprint);
		                this.quiz.standard.dirty = false;
		                this.quiz.standard.complete = true;
		            }
                    
                    this.questionInEditMode = false;
                    
                    successCallback();
                },
                error: function (response) {
                    errorCallback();
                    console.error(response);
                }
            });
        } else {
            errorCallback();
        }
    },
    
    saveQuiz: function(callback) {
        if (this.quiz.get('dirty')) {
            this.quiz.unset('created_by');
            this.quiz.unset('created_date');
            this.quiz.unset('last_updated');
            this.quiz.save(null, {success: () => {
                delete this.quiz.questions;
                this.quiz = null;
                callback();
            }});
        } else {
            callback();
        }
    },
    
    removeQuiz: function(callback) {
        request.post({
            url: `${window.backend_host}/quizzes/api/quizzes/purge/`,
            data: {
                ids: [this.quiz.id]
            },
            success: () => {
                console.log('Remove unsaved quiz SUCCESS');
            },
            error: (response) => {
                console.log('Remove unsaved quiz error:', response);
            },
            complete: callback
        });
    },
    
    validateQuizQuestions: function() {
        let validQuiz = true;
        
        _.each(this.quiz.questions, (question) => {
            if (validQuiz && !this.validateQuestion(question)) {
                this.trigger('scrollToQuestion', question);
                validQuiz = false;
            }
        });
        
        return validQuiz;
    },
    
    validateQuestion: function(question, callback) {
        let errorMessage = [];
        
        if (!question) {
            return false;
        }
        
        let questionText = question.get('question_text');
        
        if (!questionText) {
            errorMessage.push('noQuestionText');
        }
        
        let validAnswers = _.filter(question.answers, (answer) => {
            if (!!answer.get('text')) {
                if (/<([A-Z][A-Z0-9]*)\b[^>]*>(.*?)<\/\1>/i.test(answer.get('text'))) {
                    return _.escape(answer.get('text'));
                } else {
                    return true;
                }
            } else {
                return false;
            }
        });
        
        if (question.get('type') === 'MC' && validAnswers.length < 2) {
            errorMessage.push('notEnoughtAnswers');
        }
        
        if (question.get('type') === 'TF' && validAnswers.length !== 2) {
            errorMessage.push('invalidTFQuestion');
        }
        
        if (errorMessage.length > 0) {
            question.set('dirty', true);
            this.trigger('error:display', errorMessage);
            return false
        } else {
            if (_.contains(['MC', 'FR'], question.get('type'))) {
                question.answers = validAnswers;
            }
            
            question.answers.forEach((ans, index) => {
                ans.set('order', index + 1);
            });
            
            if (question.has('question_id')) {
                question.save(null, { 
                    success: function(attrs, data) {
                        if (question.get('type') === 'MC') {
                            if (question.answers.length === data.answers.length) {
                                data.answers.forEach((ans) => {
                                    question.answers.forEach((answer) => {
                                        if (ans['id'] === answer.get('id')) {
                                            answer.set({'order': ans.order});
                                        }
                                    });
                                });
                            } else {
                                // TODO .... This is not cool.
                            }
                        }
                    }
                });
                
                if (callback) {
                    callback();
                }
            } else {
                question.save(null, {
                    success: () => {
                        question.unset('answers'); // We don't want Backbone setting answers on the model.
                        
                        if (callback) {
                            callback();
                        }
                    }
                });
                
                return true;
            }
            
            return true;
        }
    },
    
    addNewQuestion: function (type) {
        let quiz = this.quiz;
        
        let data = {
            type: type,
            order: quiz.questions.length + 1,
            quiz_id: quiz.get('id'),
            answers: [],
            explanation: '',
            grading_weight: 1,
            has_correct_answer: false,
            question_text: '',
            resources: []
        };
        
        let question = new Question(data);
        question.set('dirty', true);
        
        this.questionInEditMode = true;
        
        quiz.questions.push(question);
        
        this.reorderQuestions();
    },
    
    removeQuestion: function (question) {
        if (question instanceof Backbone.Model) {
            let questions = this.quiz.questions;
            
            this.quiz.questions = _.filter(questions, (q) => {
                return q.cid !== question.cid;
            });
            
            question.off();
            question.destroy();
            
            this.reorderQuestions();
        }
    },
    
    copyQuestion: function (question) {
        if (!(question instanceof Backbone.Model)) {
            return;
        }
        
        let questions = this.quiz.questions,
            clonedAnswers = [];
        
        _.each(question.answers, (answer) => {
            let newAnswer = answer.toJSON();
            delete newAnswer.id; // Delete the id of the answer so it doesn't go to the new question.
            clonedAnswers.push(newAnswer);
        });
        
        let clonedResource = [];
        
        if (question.resources && question.resources.length !== 0) {
            clonedResource.push({'url': question.resources[0].get('url')});
        }
        
        let data = {
            type: question.get('type').toUpperCase(),
            order: question.get('order') + 1,
            quiz_id: this.quiz.get('id'),
            answers: clonedAnswers,
            explanation: question.get('explanation'),
            grading_weight: 1,
            has_correct_answer: question.get('has_correct_answer'),
            question_text: question.get('question_text'),
            resources: clonedResource,
            duplicate: true
        };
        
        let newQuestion = new Question(data);
        newQuestion.set('dirty', true);
        
        questions.splice(question.get('order'), 0, newQuestion);
        
        this.reorderQuestions();
        
        newQuestion.save(null, {
            success: () => {
                this.sortQuestions();
            }
        });
    },
    
    sortQuestions: function (param) {
        if (param) {
            this.quiz.questions = _.sortBy(this.quiz.questions, (question) => {
                return question.get('order');
            });
        }
        
        this.reorderQuestions();
        
        let questionOrder = [];
        
        _.each(this.quiz.questions, (question) => {
            questionOrder.push({
                question_id: question.get('question_id'),
                order: question.get('order')
            });
        });
        
        request.post({
            url: `${window.backend_host}/quizzes/api/question-reorder/`,
            data: {
                quiz_id: this.quiz.get('id'),
                orders: questionOrder
            },
            success: () => {
                console.log('Reorder success');
            },
            error: (response) => {
                console.log('Reorder error:', response);
            }
        });
    },
    
    reorderQuestions: function() {
        let questions = this.quiz.questions;
        
        for (let i = 0; i < questions.length; ++i) {
            let newOrder = i + 1;
            
            if (questions[i].get('order') !== newOrder) {
                questions[i].set('order', newOrder);
            }
        }
    },
    
    blueprint: {
        subject: [
            {id: 1, name: 'Math'}, 
            {id: 2, name: 'Science'},
            {id: 3, name: 'Language Arts'}, 
            {id: 4, name: 'Social Studies'}, 
            {id: 5, name: 'Other Subjects' }
        ],
        core: null,
        "class": null,
        standard: null
    },
    
    standardsObject: {},
    
    standardURL: '//app.masteryconnect.com/api/query_types/{0}?TOKEN=', // The url-encoded token to be sent when requesting standards.
    
    token: 'q7%2Ak%2F%40u-%5DQsY%2C%60%3FR+%2CF1P-yNq%3C9k%28%7BW%7CF89oh%3E%5ETP.Yft%2C%261OtaUY+%29_Fft%7Buq3zD%7CjdY%5B%3AVR%26%7C%3Ak%3FwGvXng.Z%3B%2Be%2A%5D2-_%2BU-+BQ%2AYM%7BTUC%2FEKPy%2A2%7B%2F4Rdcau%216vW%5B%40',
    
    // The queries that will be made when an option is chosen from the dropdowns:
    queries: {
        initial: ['subjects',                 'query Subjects  { subjects                                  { id name } }'],
        subject: ['pathways',                 'query Cores     { pathways(subject_id: {0})                 { id name available state { id name code} } }'],
        core:    ['class_objective_pathways', 'query Classes   { class_objective_pathway(pathway_id: {0})  { id class_objective { id name } }}'],
        "class": ['class_objectives',         'query Standards { class_objective(id: {0})                  { id name objectives { id name short_description objective_id sub_objectives } } }']
    },
    
    addNewStandard: function() {
        this.quiz.standard = {
            dirty: true,
            complete: false,
            standardsObject: _.clone(this.blueprint)
        };
    },
    
    setStandards: function(changed, standard) {
        let quizStandard = this.quiz.standard;
        
        if (changed === 'subject') {
            quizStandard['core'] = null;
            quizStandard['class'] = null;
            quizStandard['standard'] = null;
            quizStandard.standardsObject['core'] = [];
            quizStandard.standardsObject['class'] = null;
            quizStandard.standardsObject['standard'] = null;
        } else if (changed === 'core') {
            quizStandard['class'] = null;
            quizStandard['standard'] = null;
            quizStandard.standardsObject['class'] = [];
            quizStandard.standardsObject['standard'] = null;
        } else if (changed === 'class') {
            quizStandard['standard'] = null;
            quizStandard.standardsObject['standard'] = [];
        } else if (changed === 'standard') {
            _.each(quizStandard.standardsObject['standard'], (std) => {
                if (String(std.id) === standard) {
                    std.name = String(std.name).toUpperCase();
                    quizStandard['standard'] = std;
                }
            });
            
            // If the standard is already completed, the user is in edit mode:
            if (!quizStandard.complete) {
                this.checkStandard();
            } else {
                this.trigger('render');
            }
            
            return;
        } else if (changed === 'save') {
            if (this.checkStandard()) {
                this.quiz.set('dirty', true);
            } else {
                popup.render({
                    title: translate('Incomplete Standard'),
                    message: translate('The quiz standard is not complete.')
                });
            }
            
            return;
        } else {
            console.warn('Strange changed event name received:', changed, standard);
            return;
        }
        
        quizStandard[changed] = parseInt(standard);
        
        this.getStandardData(changed, standard);
    },
    
    getStandardList: function(callback) {
        let standard = this.quiz.standard;
        
        if (_.isEmpty(standard.standardsObject['core'])) {
            this.getStandardData('subject', standard['subject']);
        }
        
        if (_.isEmpty(standard.standardsObject['class'])) {
            this.getStandardData('core', standard['core']);
        }
        
        if (_.isEmpty(standard.standardsObject['standard'])) {
            this.getStandardData('class', standard['class']);
        }
        
        callback();
    },
    
    getStandardData: function(changed, standard) {
        let that = this;
        let quizStandard = this.quiz.standard;
        
        let successFunction = function(data) {
            if (_.isEmpty(data['data'])) {
                that.trigger('error:emptyApiResponse', changed);
                return;
            }
            
            if (changed === 'initial') {
                if (_.isEmpty(data['data']['subjects'])) {
                    that.trigger('error:emptyApiResponse', changed);
                    return;
                }
                
                quizStandard['standardsObject']['subject'] = data['data']['subjects'];
            } else if (changed === 'subject') {
                if (_.isEmpty(data['data']['pathways'])) {
                    that.trigger('error:emptyApiResponse', changed);
                    return;
                }
                
                // Filter out the cores that shouldn't be in the list.
                quizStandard['standardsObject']['core'] = _.filter(data['data']['pathways'], function(a) {return a.available});
                
                // Group the cores by state.
                quizStandard['standardsObject']['core'] = _.groupBy(quizStandard['standardsObject']['core'], function(a) {return a.state.name});
                
                // Sort the cores in each state. The states are sorted in the standards view.
                _.each(quizStandard['standardsObject']['core'], (coreList, index) => {
                    quizStandard['standardsObject']['core'][index] = _.sortBy(coreList, 'name');
                });
            } else if (changed === 'core') {
                if (_.isEmpty(data['data']['class_objective_pathway'])) {
                    that.trigger('error:emptyApiResponse', changed);
                    return;
                }
                
                quizStandard['standardsObject']['class'] = data['data']['class_objective_pathway']; // The list of classrooms.
            } else if (changed === 'class') {
                if (_.isEmpty(data['data']['class_objective']['objectives'])) {
                    that.trigger('error:emptyApiResponse', changed);
                    return;
                }
                
                quizStandard['standardsObject']['standard'] = data['data']['class_objective']['objectives'];
                
                // Some standards have sub objectives that are being sent as a JSON string.
                _.each(quizStandard['standardsObject']['standard'], (standard, key) => {
                    // Override the sub_objectives string with the parsed object.
                    quizStandard['standardsObject']['standard'][key]['sub_objectives'] = [];
                });
            }
            
            that.trigger('render');
        };
        
        if (window.location.hostname === 'a.socrative.com') {
            if (!mockStandard) {
                require.ensure(['MockStandard'], (require) => {
                    mockStandard = require('MockStandard');
                    successFunction(mockStandard[changed]);
                });
            } else {
                setTimeout(() => { // Simulate the standard being loaded.
                    successFunction(mockStandard[changed]);
                }, 375);
            }
        } else {
            let queryOptions = this.queries[changed],
                query = queryOptions[1].format(standard); // Get the query from our dictionary.
            
            request.post({
                url: this.standardURL.format(queryOptions[0]) + this.token,
                data: query,
                success: successFunction,
                error: () => {
                    quizStandard.dirty = false;
                    quizStandard.complete = false;
                    quizStandard.noAccess = true;
                    that.trigger('render');
                }
            });
        }
        
        that.trigger('render');
    },
    
    checkStandard: function() {
        let quizStandard = this.quiz.standard;
        
        if (quizStandard) {
            if (quizStandard['subject'] && quizStandard['core'] && quizStandard['class'] && quizStandard['standard']) {
                quizStandard.complete = true;
                quizStandard.dirty = false;
                
                this.trigger('render');
                return true;
            } else {
                return quizStandard.noAccess
            }
        }
        
        return true;
    }
    
});

module.exports = new EditQuizModel();
