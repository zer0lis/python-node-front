import Backbone from 'backbone';
import _ from 'underscore';
import React from 'react';
import ReactDOM from 'react-dom';
import model from 'EditQuizModel';
import teacher from 'TeacherModel';
import MediaResourceModel from 'MediaResourceModel';
import AnswerModel from 'AnswerModel';
import header from 'HeaderController';
import view from 'EditQuizView';
import popup from 'PopupController';
import dots from 'Dots';
import Constants from 'Constants';
import utils from 'Utils';
import {translate} from 'translator';

let router = null,
    e = React.createElement;

function EditQuizController() {
    _.extend(this, Backbone.Events);
}

EditQuizController.prototype = {
    
    doEditQuiz: function(socNumber, teacherRouter) {
        router = teacherRouter;
        
        let createQuizContainer = document.getElementById('main-content');
        ReactDOM.unmountComponentAtNode(createQuizContainer);
        
        header.render();
        model.reset();
        teacher.shouldFetchQuizzes = true;
        
        if (teacher.currentQuizFolderId === Constants.TRASH) {
            teacher.currentQuizFolderId = Constants.ROOT_FOLDER;
        }
        
        if (createQuizContainer !== null) {
            model.getQuiz(socNumber, 
            () => { // success callback
                controller.render();
                
                window.addEventListener('beforeunload', controller.handleBeforeUnload);

                // If the browsere supports it, add a hash change event listener.
                // This is useful for when the user hits the back button.
                if (window.history && window.history.pushState) {
                    model.currentUrl = window.location.hash;
                    window.addEventListener('popstate', controller.handlePopState);
                }
            },
            () => { // error callback
                router.routeToQuizzes(); // If we have an error, like not findig the quiz, redirect to quizzes.
            });
        } else {
            window.location.reload(); // Should we refresh?
        }
    },
    
    handleBeforeUnload: function(event) {
        if (!model.quiz.has('last_updated') && !model.quiz.has('dirty') && !model.quiz.questions.length) {
            return; // The quiz is new and absolutly nothing was changed, so do nothing.
        }
        
        if (!controller.validateQuiz()) {
            popup.closePopup();
            let question = translate('Are you sure you want to leave without saving this quiz?');
            event.returnValue = question;
            return question;
        } else {
            model.saveQuiz(() => {
                window.removeEventListener('popstate', controller.handlePopState);
                window.removeEventListener('beforeunload', controller.handleBeforeUnload);
            });
        }
    },
    
    handlePopState: function() {
        if (model.quiz) {
            if (!model.quiz.has('last_updated') && !model.quiz.has('dirty') && !model.quiz.questions.length) {
                model.removeQuiz(() => {
                    window.removeEventListener('beforeunload', controller.handleBeforeUnload);
                    window.removeEventListener('popstate', controller.handlePopState);
                });
            } else {
                if (!controller.checkQuizState(Backbone.history.fragment)) {
                    // This is a hack to keep the url updated. We don't go through the router because we
                    // don't want a re-render. The user will not leave the page if the quiz is invalid.
                    // Check if the user is pressing the back button and prevent dropping popstate:
                    if (Backbone.history.fragment.indexOf('edit-quiz') === -1) {
                        window.removeEventListener('popstate', controller.handlePopState);
                    }
                    
                    window.history.pushState(null, null, model.currentUrl);
                }
            }
        }
    },
    
    render: function(callback) {
        if (/^edit\-quiz/.test(Backbone.history.fragment)) {
            ReactDOM.render(
                React.createElement(
                    view, {
                        controller: controller,
                        model: model,
                        standard: model.quiz.standard // This is so deeply entagled in the edit quiz view hierarcy that it must be passed separately.
                    }
                ),
                document.getElementById('main-content'),
                callback
            );
        }
    },
    
    quizNameChanged: function(newName) {
        model.quiz.set({
            name: newName,
            dirty: true
        });
    },
    
    shareButtonClicked: function() {
        model.quiz.set({
            sharable: !model.quiz.get('sharable'),
            dirty: true
        });
    },
    
    alignToStandardChanged: function(checked) {
        if (checked) {
            model.addNewStandard();
        } else {
            delete model.quiz.standard;
        }
        
        model.quiz.set('dirty', true);
        controller.render();
    },
    
    standardChanged: function(changed, standard) {
        model.quiz.standard.standardsObject.noResponseData = null;
        model.setStandards(changed, standard);
    },
    
    editStandardClicked: function() {
        model.getStandardList(() => {
            model.quiz.standard.backup = _.clone(model.quiz.standard); // Clone the existing standard in case the teacher cancels.
            model.quiz.standard.dirty = true;
            model.quiz.set('dirty', true);
            controller.render();
        });
    },
    
    cancelEditStandardClicked: function() {
        model.quiz.standard = model.quiz.standard.backup;
        model.quiz.standard.backup = null;
        model.quiz.standard.standardsObject = _.clone(model.blueprint);
        model.quiz.standard.dirty = false;
        controller.render();
    },
    
    deleteStandardClicked: function() {
        popup.render({
            title: translate('Please Confirm'),
            buttonText: translate('Yes'),
            cancelText: translate('No'),
            message: translate('Are you sure you want to delete this standard?'),
            buttonClicked: () => {
                delete model.quiz.standard;
                model.quiz.set('dirty', true);
                controller.render();
            }
        });
    },
    
    copyQuestion: function(question) {
        model.copyQuestion(question);
        model.questionInEditMode = true;
        
        controller.render(() => {
            controller.scrollToQuestion(model.quiz.questions[question.get('order')]);
        });
    },
    
    changeQuestionOrder: function(oldOrder, newOrder) {
        if (newOrder < 1 || newOrder > model.quiz.questions.length) {
            return;
        }
        
        model.quiz.questions[oldOrder-1].set({order: newOrder, dirty: false});
        model.quiz.questions[newOrder-1].set({order: oldOrder, dirty: false});
        model.sortQuestions(true);
        
        controller.render();
        controller.scrollToQuestion(model.quiz.questions[newOrder-1]);
    },
    
    deleteQuestion: function(question) {
        popup.render({
            title: translate('Please Confirm'),
            message: translate('Are you sure you want to delete this question?'),
            buttonText: translate('Yes'),
            cancelText: translate('No'),
            buttonClicked: () => {
                model.removeQuestion(question);
                model.questionInEditMode = false;
                controller.render();
                
                if (model.quiz.questions.length > 0 && question.get('order') - 2 >= 0) {
                    controller.scrollToQuestion(model.quiz.questions[question.get('order') - 2]);
                }
            }
        });
    },
    
    canEditQuestion: function() {
        let validQuiz = true;
        
        _.forEach(model.quiz.questions, (question) => {
            if (validQuiz && !model.validateQuestion(question)) {
                controller.scrollToQuestion(question);
                validQuiz = false;
            }
        });
        
        model.questionInEditMode = true;
        controller.render();
        return validQuiz;
    },
    
    updateQuestion: function(question, content) {
        question.set(content);
        question.set('dirty', true);
    },
    
    saveQuestionClicked: function(question, callback) {
        _.defer(() => { // Defer to fix iOS bug SOC-651.
            model.validateQuestion(question, () => {
                callback();
                model.questionInEditMode = false;
                controller.render();
            });
        });
        
    },
    
    addQuestion: function(type) {
        if (model.validateQuizQuestions()) {
            model.addNewQuestion(type);
            controller.render();
        }
    },
    
    scrollToQuestion: function(question) {
        if (!question) {
            return;
        }
        
        let questionDiv = null;
        
        if (question.has('order')) {
            questionDiv = document.getElementById(`question-${question.get('order')}`);
        } else if (question.has('question_number')) {
            questionDiv = document.getElementById(`question-${question.get('question_number')}`);
        }
        
        if (questionDiv) {
            TweenLite.to(window, 0.75, {
                scrollTo: questionDiv.getBoundingClientRect().top + window.pageYOffset - document.documentElement.clientTop,
                ease: Quint.easeOut
            });
        }
    },
    
    addAnswer: function(question) {
        let newAnswer = new AnswerModel({
            order: question.nextAnswerOrder(),
            is_correct: false
        });
        
        question.addAnswer(newAnswer);
    },
    
    deleteAnswer: function(question, answerOrder) {
        let questionType = question.get('type').toUpperCase();
        
        if (questionType === 'MC') {
            if(question.answers.length <= 2) {
                popup.render({
                    title: translate('More Answers!'),
                    message: translate('Multiple-choice questions must have at least two answers.')
                });
            } else {
                let ans = _.filter(question.answers, (answer) => {
                    return answer.get('order') === answerOrder;
                });
                
                question.removeAnswer(ans[0]);
            }
        } else if (questionType === 'FR') {
            question.removeAnswer();
        }
    },
    
    updateAnswer: function(question, answer, answersObject) {
        answer.set(answersObject);
        question.set('dirty', true);
    },
    
    imageSelected: function(event, question, callback) {
        let options = {
            success: (fileUrl) => {
                question.set('dirty', true);
                question.resources.push(new MediaResourceModel({
                    url: fileUrl
                }));

                if (callback) {
                    callback(fileUrl);
                }
            }, 
            error: (error) => {
                // TODO: show an error here
            }
        };
        
        utils.upload(event, options);
    },
    
    deleteImage: function(question, callback) {
        question.resources.length = 0;
        question.set('dirty', true);
        
        if (callback) {
            callback();
        }
    },
    
    errorList: {
        noQuestionText: translate('There is a question without text in the question body.'),
        notEnoughtAnswers: translate('All Multiple Choice questions must have at least 2 answer choices.')
    },
    
    showError: function(errorMessages) {
        let errorList = this.errorList,
            fullError = [];
        
        _.each(errorMessages, (message) => {
            fullError.push(errorList[ message ], e('br'));
        });
        
        popup.render({
            title: translate('Quiz Problem'),
            message: e('span', {}, fullError)
        });
    },
    
    showApiError: function(changed) {
        this.render(); // Show the loading indicator for the drop down that has no options.
        
        popup.render({
            title: translate('Try Again'),
            message: translate('There are no options available. Please make another selection.'),
            buttonClicked: () => {
                model.quiz.standard.standardsObject.noResponseData = changed;
                this.render();
            }
        });
    },
    
    validateQuiz: function(param) {
        let quiz = model.quiz;
        
        if (!quiz.get('name')) {
            popup.render({
                title: translate('Quiz Name'),
                message: translate('Please enter a name for your quiz.'),
                buttonClicked: () => {
                    TweenLite.to(window, 0.75, {
                        scrollTo: 0,
                        ease: Quint.easeOut
                    });
                }
            });
            return false;
        }
        
        if (!model.checkStandard()) {
            popup.render({
                title: translate('Incomplete Standard'),
                message: translate('The quiz standard is not complete.')
            });
            
            return false;
        }
        
        if (quiz.questions.length === 0) {
            popup.render({
                title: translate('Incomplete Quiz'),
                message: translate('A quiz must have at least one question. Would you like to delete this quiz?'),
                buttonText: translate('Yes'),
                cancelText: translate('No'),
                buttonClicked: () => {
                    model.removeQuiz(() => {
                        window.removeEventListener('beforeunload', controller.handleBeforeUnload);
                        window.removeEventListener('popstate', controller.handlePopState);
                        controller.redirect(param);
                    });
                }
            });
            return false;
        } else {
            return model.validateQuizQuestions();
        }
    },
    
    saveAndExitClicked: function() {
        dots.start('saveAndExitButton');
        model.saveAndExitClicked = true;
        controller.checkQuizState();
    },
    
    checkQuizState: function(param) {
        if (!model.quiz.has('last_updated') && !model.quiz.has('dirty') && !model.quiz.questions.length) {
            model.removeQuiz(() => {
                window.removeEventListener('popstate', controller.handlePopState);
                window.removeEventListener('beforeunload', controller.handleBeforeUnload);
                controller.redirect(param);
            });
            return false;
        }
        
        if (controller.validateQuiz(param)) {
            model.saveQuiz(() => {
                window.removeEventListener('popstate', controller.handlePopState);
                window.removeEventListener('beforeunload', controller.handleBeforeUnload);
                if (model.saveAndExitClicked || param === 'quizzes') {
                    teacher.showQuizSaved = true;
                }
                controller.redirect(param);
            });
            return true;
        }
        
        return false;
    },
    
    redirect: function(param) {
        if (!param || !_.isString(param)) {
            router.routeToQuizzes();
        } else {
            router.routeToDynamic({route: param});
        }
    }
    
};

let controller = new EditQuizController();

controller.listenTo(model, 'error:display',          controller.showError);
controller.listenTo(model, 'error:emptyApiResponse', controller.showApiError);
controller.listenTo(model, 'scrollToQuestion',       controller.scrollToQuestion);
controller.listenTo(model, 'render',                 controller.render);

module.exports = controller;
