import React from 'react';
import Input from 'Input';
import Select from 'Select';
import CloseX from 'CloseX';
import SuccessIcon from 'LoggedInIcon';
import SuccessIconLarge from 'SuccessIconLarge';
import HelpTooltip from 'HelpTooltip';
import GenericCardIcon from 'GenericCardIcon';
import AmexIcon from 'AmexIcon';
import MasterCardIcon from 'MasterCardIcon';
import VisaIcon from 'VisaIcon';
import Constants from 'Constants';
import {translate} from 'translator';

let e = React.createElement;

export default class LicenseView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            l = m.licenseModel,
            couponInputClass = '',
            couponInputChildren = [],
            couponButtonClass = 'apply-coupon-button',
            couponButtonChildren = translate('APPLY');
        
        if (l.couponSuccess) {
            couponInputClass = 'coupon-input-success';
            couponInputChildren.push(
                e(CloseX, {
                    className: 'clear-coupon-x',
                    color: Constants.CLEAR_INPUT_X,
                    onClick: c.clearCouponClicked
                })
            );
            
            couponButtonClass = 'apply-coupon-button-success';
            couponButtonChildren = e(SuccessIcon, {
                color: '#ffffff',
                svgClass: 'coupon-success-icon'
            });
        }
        
        couponInputChildren.push(
            e(Input, {
                className: couponInputClass,
                containerClass: 'coupon-name-input-container',
                id: 'couponName',
                label: translate('Coupon Code'),
                maxLength: 255,
                onChange: (event) => c.inputChanged(event.target.id, event.target.value),
                onKeyPress: (event) => {
                    if (event.key === 'Enter') {
                        c.applyCouponClicked();
                    }
                },
                readOnly: l.couponSuccess,
                status: l.errors.couponName,
                tooltipLocation: 'above',
                value: l.couponName
            })
        );
        
        let children = [];
        
        if (l.licenseStep === Constants.LICENSE_STEP) {
            children = [
                e('div', {
                    className: 'activate-link-container',
                    children: [
                        e('span', {
                            className: 'link',
                            children: translate('Enter License Key'),
                            onClick: c.enterLicenseKeyClicked
                        })
                    ]
                }),
                e('span', {
                    className: 'license-information-header',
                    children: translate('LICENSE INFORMATION')
                }),
                e('div', {
                    className: 'license-information-details',
                    children: [
                        e('div', {
                            className: 'license-information-item',
                            children: [
                                e(Input, {
                                    className: 'license-seats-input',
                                    containerClass: 'license-seats-input-container',
                                    id: 'licenseSeats',
                                    label: translate('Seats'),
                                    onBlur: c.seatsBlurred,
                                    onChange: (event) => c.inputChanged(event.target.id, event.target.value),
                                    value: l.seats
                                })
                            ]
                        }),
                        e('div', {
                            className: 'license-information-item license-years-select',
                            children: [
                                e('p', {
                                    className: 'license-years-select-label',
                                    children: translate('Years')
                                }),
                                e(Select, {
                                    children: [
                                        {title: 1, value: 1, selected: 1 === l.years},
                                        {title: 2, value: 2, selected: 2 === l.years},
                                        {title: 3, value: 3, selected: 3 === l.years}
                                    ],
                                    label: translate('Years'),
                                    onSelect: (name, value) => c.yearsChanged(value)
                                })
                            ]
                        }),
                        e('div', {
                            className: 'license-information-item',
                            children: couponInputChildren
                        }),
                        e('div', {
                            className: 'license-information-item',
                            children: [
                                e('button', {
                                    className: couponButtonClass,
                                    children: couponButtonChildren,
                                    disabled: l.couponSuccess,
                                    id: 'applyCouponButton',
                                    onClick: c.applyCouponClicked
                                })
                            ]
                        }),
                        e('div', {
                            className: 'license-information-total-container',
                            children: [
                                e('div', {
                                    children: [
                                        e('span', {
                                            className: 'license-information-total-header',
                                            children: translate('TOTAL')
                                        })
                                    ]
                                }),
                                e('span', {
                                    className: 'license-information-total-amount',
                                    children: l.totalPriceFormatted
                                })
                            ]
                        })
                    ]
                }),
                e('div', {
                    className: 'apply-license-container',
                    children: [
                        e('input', {
                            className: 'apply-license-checkbox',
                            type: 'checkbox',
                            id: 'applyNow',
                            checked: l.applyNow,
                            onChange: c.applyNowChanged
                        }),
                        e('label', {
                            className: 'apply-license-label',
                            htmlFor: 'applyNow',
                            children: translate('Apply one seat to my account')
                        }),
                        e(HelpTooltip, {
                            above: true,
                            className: 'license-information-tooltip apply-now-tooltip',
                            title: translate('Upgrade My Account'),
                            text: translate('The number of seats corresponds to the number of teachers that will use this license (students are free). Select this option to use one seat for your account.')
                        })
                    ]
                }),
                e('div', {
                    className: 'auto-renew-container',
                    children: [
                        e('input', {
                            className: 'auto-renew-checkbox',
                            type: 'checkbox',
                            id: 'autoRenew',
                            checked: l.autoRenew,
                            onChange: c.autoRenewChanged
                        }),
                        e('label', {
                            className: 'auto-renew-label',
                            htmlFor: 'autoRenew',
                            children: translate('Auto Renew')
                        }),
                        e(HelpTooltip, {
                            above: true,
                            className: 'license-information-tooltip auto-renew-tooltip',
                            title: translate('Auto Renew'),
                            text: translate('Select this option if you would like this license to automatically renew in {0}.').format(`${l.years} ${l.years > 1 ? 'years' : 'year'}`)
                        })
                    ]
                }),
                e('div', {
                    className: 'review-and-pay-container',
                    children: [
                        e('button', {
                            id: 'reviewAndPayButton',
                            className: 'action-button action-button-200',
                            children: translate('REVIEW & PAY'),
                            onClick: c.reviewAndPayClicked
                        })
                    ]
                })

            ];
        }
        
        if (l.licenseStep === Constants.PAYMENT_STEP) {
            let CreditCardIcon = GenericCardIcon;
            
            if (l.cardType === Constants.AMERICAN_EXPRESS) {
                CreditCardIcon = AmexIcon;
            } else if (l.cardType === Constants.MASTERCARD) {
                CreditCardIcon = MasterCardIcon;
            } else if (l.cardType === Constants.VISA) {
                CreditCardIcon = VisaIcon;
            }
            
            let i = 1,
                monthChildren = [];
            
            while (i <= 12) {
                let displayMonth = i.toString();
                
                if (/^\d$/.test(displayMonth)) {
                    displayMonth = '0' + displayMonth;
                }
                
                monthChildren.push(
                    {title: displayMonth, value: displayMonth, selected: displayMonth === l.month}
                );
                
                i++;
            }
            
            let now = new Date(),
                currentYear = now.getFullYear(),
                yearChildren = [];
            
            for (i = 0; i <= 10; i++) {
                yearChildren.push(
                    {title: currentYear + i, value: currentYear + i, selected: currentYear + i === l.year}
                );
            }
            
            let paymentInformationChildren = [
                e('span', {
                    className: 'payment-information-header-text',
                    children: translate('PAYMENT INFORMATION')
                })
            ];
            
            if (l.seats >= Constants.BULK_SEATS_THRESHOLD) {
                paymentInformationChildren.push(
                    e('span', {
                        className: 'other-payment-options-link',
                        children: translate('Other Payment Options'),
                        onClick: c.otherPaymentOptionsClicked
                    })
                );
            }
            
            children = [
                e('div', {
                    className: 'order-summary-header',
                    children: [
                        e('span', {
                            className: 'order-summary-header-text',
                            children: translate('ORDER SUMMARY')
                        }),
                        e('span', {
                            className: 'order-summary-right-column link',
                            children: translate('Change'),
                            onClick: () => {
                                if (!l.makingPurchase) {
                                    c.previousClicked();
                                }
                            }
                        })
                    ]
                }),
                e('div', {
                    className: 'order-summary-row first-order-summary-row',
                    children: [
                        e('span', {
                            children: translate('Price Per Seat')
                        }),
                        e('span', {
                            className: 'order-summary-right-column',
                            children: l.pricePerSeatFullFormatted
                        })
                    ]
                }),
                e('div', {
                    className: 'order-summary-row' + (l.couponSavings === 0 ? ' hidden' : ''),
                    children: [
                        e('span', {
                            children: translate('Coupon Discount Per Seat')
                        }),
                        e('span', {
                            className: 'order-summary-right-column',
                            children: l.couponSavings
                        })
                    ]
                }),
                e('div', {
                    className: 'order-summary-row',
                    children: [
                        e('span', {
                            children: translate('Seats')
                        }),
                        e('span', {
                            className: 'order-summary-right-column',
                            children: l.seats
                        })
                    ]
                }),
                e('div', {
                    className: 'order-summary-row',
                    children: [
                        e('span', {
                            children: translate('Years')
                        }),
                        e('span', {
                            className: 'order-summary-right-column',
                            children: l.years
                        })
                    ]
                }),
                e('div', {
                    className: 'order-summary-row',
                    children: [
                        e('span', {
                            children: translate('Renewal Date')
                        }),
                        e('span', {
                            className: 'order-summary-right-column',
                            children: l.renewalDateFormatted
                        })
                    ]
                }),
                e('div', {
                    className: 'order-summary-row',
                    children: [
                        e('span', {
                            children: translate('Auto Renew')
                        }),
                        e('span', {
                            className: 'order-summary-right-column',
                            children: l.autoRenew ? translate('Yes') : translate('No')
                        })
                    ]
                }),
                e('div', {
                    className: 'order-summary-total',
                    children: [
                        e('span', {
                            className: 'order-summary-total-column',
                            children: translate('TOTAL')
                        }),
                        e('span', {
                            className: 'order-summary-total-column order-summary-right-column',
                            children: l.totalPriceFormatted
                        })
                    ]
                }),
                e('div', {
                    className: 'payment-information-header',
                    children: paymentInformationChildren
                }),
                e('div', {
                    className: 'name-on-card-container',
                    children: [
                        e(Input, {
                            autoFocus: true,
                            containerClass: 'name-on-card-input-container',
                            disabled: l.makingPurchase,
                            id: 'nameOnCard',
                            label: translate('Name On Card'),
                            maxLength: 255,
                            onChange: (event) => c.inputChanged(event.target.id, event.target.value),
                            status: l.errors.nameOnCard,
                            value: l.nameOnCard
                        })
                    ]
                }),
                e('div', {
                    className: 'card-details',
                    children: [
                        e('div', {
                            className: 'card-detail-container',
                            children: [
                                e(Input, {
                                    containerClass: 'card-number-input-container',
                                    disabled: l.makingPurchase,
                                    id: 'cardNumber',
                                    label: translate('Card Number'),
                                    maxLength: 255,
                                    onChange: (event) => c.inputChanged(event.target.id, event.target.value),
                                    status: l.errors.cardNumber,
                                    value: l.cardNumber
                                }),
                                e(CreditCardIcon, {
                                    className: 'credit-card-icon'
                                })
                            ]
                        }),
                        e('div', {
                            className: 'card-detail-container card-month-container',
                            children: [
                                e('p', {
                                    className: '',
                                    children: translate('Expiration')
                                }),
                                e(Select, {
                                    children: monthChildren,
                                    disabled: l.makingPurchase,
                                    error: l.errors.month.error,
                                    label: translate('Month'),
                                    onSelect: (name, value) => c.monthChanged(value)
                                })
                            ]
                        }),
                        e('div', {
                            className: 'card-detail-container card-year-container',
                            children: [
                                e('p', {
                                    className: '',
                                    dangerouslySetInnerHTML: {__html: '&nbsp;'} // Hack to help Firefox keep things lined up.
                                }),
                                e(Select, {
                                    children: yearChildren,
                                    disabled: l.makingPurchase,
                                    error:  l.errors.year.error,
                                    label: translate('Year'),
                                    onSelect: (name, value) => c.yearChanged(value)
                                })
                            ]
                        }),
                        e('div', {
                            className: 'card-detail-container card-detail-container-cvc',
                            children: [
                                e(Input, {
                                    containerClass: 'card-cvc-input-container',
                                    disabled: l.makingPurchase,
                                    id: 'cvc',
                                    label: translate('CVC'),
                                    maxLength: 10,
                                    onChange: (event) => c.inputChanged(event.target.id, event.target.value),
                                    status: l.errors.cvc,
                                    value: l.cvc
                                })
                            ]
                        })
                    ]
                }),
                e('div', {
                    className: 'license-buttons-container',
                    children: [
                        e('button', {
                            className: 'license-previous-button action-button action-button-200 secondary-action-button',
                            children: translate('PREVIOUS'),
                            disabled: l.makingPurchase,
                            id: 'licensePreviousButton',
                            onClick: c.previousClicked
                        }),
                        e('button', {
                            className: 'license-purchase-button action-button action-button-200',
                            children: translate('PURCHASE'),
                            disabled: l.makingPurchase,
                            id: 'licensePurchaseButton',
                            onClick: c.purchaseClicked
                        })
                    ]
                }),
                e('div', {
                    className: 'wa-tax-text-container',
                    children: [
                        e('span', {
                            className: 'wa-tax-text-label',
                            children: translate('Washington State Sales')
                        }),
                        e('span', {
                            className: 'wa-tax-text-content',
                            children: translate('MasteryConnect is not required to, and does not, collect Washington sales or use tax. Under Washington law, purchases are not tax-exempt merely because a seller is not required to collect Washington’s tax. Washington law requires Washington purchasers to review untaxed purchases and, if any tax is owed, file a Washington use tax return and pay any tax due. Visit https://dor.wa.gov/consumerusetax for more information.')
                        })
                    ]
                })
            ];
        }
        
        if (l.licenseStep === Constants.COMPLETE_STEP) {
            children = [
                e('div', {
                    className: 'payment-receipt-container',
                    children: [
                        e('div', {
                            className: 'payment-complete-container',
                            children: [
                                e(SuccessIconLarge, {
                                    className: 'payment-complete-icon'
                                }),
                                e('span', {
                                    className: 'payment-complete-header',
                                    children: translate('Payment Complete!')
                                })
                            ]
                        }),
                        e('div', {
                            className: 'my-account-button-container' + (this.props.registration ? '' : ' hidden'),
                            children: [
                                e('button', {
                                    className: 'action-button action-button-200',
                                    children: translate('MY ACCOUNT'),
                                    onClick: c.myAccountClicked
                                })
                            ]
                        }),
                        e('span', {
                            className: 'license-receipt-sent',
                            children: translate('A copy of this receipt has been sent to your registered email: {0}').format(m.email)
                        }),
                        e('span', {
                            className: 'license-receipt-divider'
                        }),
                        e('span', {
                            className: 'account-information-header',
                            children: translate('ACCOUNT INFORMATION')
                        }),
                        e('div', {
                            className: 'order-summary-row first-order-summary-row',
                            children: [
                                e('span', {
                                    className: 'license-receipt-left-column',
                                    children: translate('Name')
                                }),
                                e('span', {
                                    className: 'order-summary-right-column',
                                    children: `${m.lastName}, ${m.firstName}`
                                })
                            ]
                        }),
                        e('div', {
                            className: 'order-summary-row' + (m.schoolName ? '' : ' hidden'),
                            children: [
                                e('span', {
                                    className: 'license-receipt-left-column',
                                    children: translate('School')
                                }),
                                e('span', {
                                    className: 'order-summary-right-column',
                                    children: m.schoolName
                                })
                            ]
                        }),
                        e('div', {
                            className: 'order-summary-row',
                            children: [
                                e('span', {
                                    className: 'license-receipt-left-column',
                                    children: translate('Email')
                                }),
                                e('span', {
                                    className: 'order-summary-right-column',
                                    children: m.email
                                })
                            ]
                        }),
                        e('span', {
                            className: 'license-receipt-information-header',
                            children: translate('LICENSE INFORMATION')
                        }),
                        e('div', {
                            className: 'order-summary-row first-order-summary-row',
                            children: [
                                e('span', {
                                    className: 'license-receipt-left-column',
                                    children: translate('License Key')
                                }),
                                e('span', {
                                    className: 'order-summary-right-column license-key-right-column',
                                    children: l.key
                                })
                            ]
                        }),
                        e('div', {
                            className: 'order-summary-row',
                            children: [
                                e('span', {
                                    className: 'license-receipt-left-column',
                                    children: translate('Number of Seats')
                                }),
                                e('span', {
                                    className: 'order-summary-right-column',
                                    children: l.seats
                                })
                            ]
                        }),
                        e('div', {
                            className: 'order-summary-row',
                            children: [
                                e('span', {
                                    className: 'license-receipt-left-column',
                                    children: translate('Number of Years')
                                }),
                                e('span', {
                                    className: 'order-summary-right-column',
                                    children: l.years
                                })
                            ]
                        }),
                        e('div', {
                            className: 'order-summary-row',
                            children: [
                                e('span', {
                                    className: 'license-receipt-left-column',
                                    children: translate('Renewal Date')
                                }),
                                e('span', {
                                    className: 'order-summary-right-column',
                                    children: l.renewalDateFormatted
                                })
                            ]
                        }),
                        e('div', {
                            className: 'order-summary-row',
                            children: [
                                e('span', {
                                    className: 'license-receipt-left-column',
                                    children: translate('Auto Renew')
                                }),
                                e('span', {
                                    className: 'order-summary-right-column',
                                    children: l.autoRenew ? translate('Yes') : translate('No')
                                })
                            ]
                        }),
                        e('span', {
                            className: 'license-receipt-information-header',
                            children: translate('PAYMENT INFORMATION')
                        }),
                        e('div', {
                            className: 'order-summary-row first-order-summary-row',
                            children: [
                                e('span', {
                                    className: 'license-receipt-left-column',
                                    children: translate('Order Number')
                                }),
                                e('span', {
                                    className: 'order-summary-right-column',
                                    children: `SOC${l.id}`
                                })
                            ]
                        }),
                        e('div', {
                            className: 'order-summary-row',
                            children: [
                                e('span', {
                                    className: 'license-receipt-left-column',
                                    children: translate('Payment Method')
                                }),
                                e('span', {
                                    className: 'order-summary-right-column',
                                    children: `Credit Card (**** ${l.cardNumber.substr(-4, 4)})`
                                })
                            ]
                        }),
                        e('div', {
                            className: 'order-summary-row',
                            children: [
                                e('span', {
                                    className: 'license-receipt-left-column',
                                    children: translate('Price Per Seat')
                                }),
                                e('span', {
                                    className: 'order-summary-right-column',
                                    children: l.pricePerSeatFullFormatted
                                })
                            ]
                        }),
                        e('div', {
                            className: 'order-summary-row' + (l.couponSuccess ? '' : ' hidden'),
                            children: [
                                e('span', {
                                    className: 'license-receipt-left-column',
                                    children: translate('Coupon Discount Per Seat')
                                }),
                                e('span', {
                                    className: 'order-summary-right-column',
                                    children: l.couponSavings
                                })
                            ]
                        }),
                        e('div', {
                            className: 'order-summary-row',
                            children: [
                                e('span', {
                                    className: 'license-receipt-left-column license-receipt-total',
                                    children: translate('Order Total')
                                }),
                                e('span', {
                                    className: 'order-summary-right-column',
                                    children: l.totalPriceFormatted
                                })
                            ]
                        }),
                        e('span', {
                            className: 'license-receipt-divider'
                        })
                    ]
                })
            ];
        }
        
        return e('div', {
            className: 'purchase-license-container',
            children: children
        });
    }
    
}
