var Backbone 	= require('backbone'),
    _           = require('underscore'),
    roomModel   = require('room'),
    header      = require('HeaderController'),
    Constants   = require('Constants'),
    platform    = require('Platform'),
    translate = require('translator').translate,
	request 	= require('Request');

var RoomsModel = Backbone.Model.extend({
    defaults: function() {
        return {
            /**
             * the number of seconds that we display the green notifier when some is saved
             * also used by the error notifier on the add students popup
             * @type {Number}
             */
            notify_timer: 2000,
            /**
             * The keys of this object are the IDs of rooms that were not the "current room" when their roster was viewed. 
             * We track these so we can unsubscribe from their pubnub channels when presence is no longer needed.
             */
            listeners: {}
        }
    },

    initialize: function() {
        this.filtersApplied = false; // Whether the teacher has applied a room filter. When this is false, the default room is at the top.
        this.maxStudents = 150; // Maximum number of students that can be added to a roster. Computed anew for rosters that already have students.
        this.newStudents = []; // The list of new students to be added to a roster.
        this.roomFilter = 'roomNameASC'; // The filter applied to the rooms view.
        this.roomsList = []; // The list of room objects.
        this.studentFilter = 'nameASC'; // The filter applied to the current student roster.
    },

    // ==================================================== 
    // =================== manage rooms ===================
    // ====================================================    

    /**
     * method used to update the the list of rooms on the page
     * this happens when filtering or when adding/deleting rooms
     * it will filter the list and then trigger a render
     * 
     * @param  {Array} list     the list of rooms (optional)
     */
    updateRoomsList: function(list) {
        let roomsList = list || this.roomsList;
        
        switch (this.roomFilter) {
            case 'roomNameASC':
                roomsList = _.sortBy(roomsList, 'name');
                break;
            case 'roomNameDSC':
                roomsList = _.sortBy(roomsList, 'name').reverse();
                break;
            case 'inMenuASC':
                roomsList = _.sortBy(( _.sortBy(roomsList, 'name')), 'in_menu').reverse();
                break;
            case 'inMenuDSC':
                roomsList = _.sortBy(( _.sortBy(roomsList, 'name')), 'in_menu');
                break;
            case 'statusASC':
                roomsList = _.sortBy(( _.sortBy(roomsList, 'name')), 'active').reverse();
                break;
            case 'statusDSC':
                roomsList = _.sortBy(( _.sortBy(roomsList, 'name')), 'active');
                break;
        }
        
        this.roomsList = roomsList;
        
        this.trigger('roomsList:updated');
    },

    getRoomsList: function(callback) {
        let route = Backbone.history.fragment;
        
        this.filtersApplied = false;
        this.roomFilter = 'roomNameASC';
        
        request.get({
            url: `${window.backend_host}/rooms/api/list/`,
            success: (data) => {
                if (Backbone.history.fragment !== route) {
                    return;
                }
                
                if ('rooms' in data) {
                    _.each(data.rooms, function(room) {
                        room.name = room.name.toLowerCase();
                    });
                    
                    // If a callback is passed, it will handle the result explicitly. One case for this is
                    // updating the header rooms menu, when adding/deleting a room causes the user to have
                    // more than one room or only one room.
                    if (callback) {
                        callback(true);
                    } else {
                        this.updateRoomsList(data.rooms);
                    }
                } else {
                    if (_.isFunction(options.error)) {
                        options.error(data);
                    }
                }
            },
            error: () => {
                if (callback) {
                    callback(false);
                }
            }
        });
    },

    shareRoom: function(roomName, generateCode) {
        let sharedRoom = _.findWhere(this.roomsList, {name: roomName});
        // if the room was't shared before and it doesn't have a share code, send request to api

        if (_.isNull(sharedRoom.share_url) || _.isUndefined(sharedRoom.share_url) || generateCode) {
            request.post({
                url: `${window.backend_host}/rooms/api/share/`,
                data: {
                    room_name: roomName
                },
                success: (data) => {
                    sharedRoom.share_url = data.url;
                    var obj = {};
                    obj.roomName = roomName;
                    obj.url = data.url;
                    obj.showInfo = generateCode;
                    this.trigger('share_room:success', obj);
                },
                error: (response) => {
                    console.error(response);
                }
            });
        } else {
            var obj = {};
            obj.roomName = roomName;
            obj.url = sharedRoom.share_url;
            obj.showInfo = generateCode;
            this.trigger('share_room:success', obj);
        }        
    },

    deleteRoom: function(room_name, callback) {
        request.del({
            url: `${window.backend_host}/rooms/api/room/${room_name}/`,
            success: () => {
                let deletedRoom = _.findWhere(this.roomsList, {name: room_name});
                deletedRoom.notify = {
                    status: true,
                    message: translate('Classroom deleted!')
                };
                
                this.updateRoomsList();

                setTimeout(() => {
                    let newRoomsList = _.without(this.roomsList, _.findWhere(this.roomsList, {name: room_name}));
                    this.updateRoomsList(newRoomsList);
                    
                    // If the user deleted the current room, change to the default room, which is now the current room.
                    if (roomModel.getRoomName().toLowerCase() === room_name.toLowerCase()) {
                        this.changeCurrentRoom(roomModel.get('name'));
                    }
                }, this.get('notify_timer'));
                
                if (callback) {
                    callback(true);
                }
            },
            error: (response) => {
                console.error(response);
                if (callback) {
                    callback(false);
                }
            }
        });
    },
    
    changeCurrentRoom: function() {
        this.trigger('current_room:changed');
    },

    changeRoomStatus: function(room_id, status, callback) {
        request.post({
            url: `${window.backend_host}/rooms/api/status/`,
            data: {
                statuses: [{
                    room_id: room_id,
                    in_menu: status
                }]
            },
            success: () => {
                let changedRoom = _.findWhere(this.roomsList, {id: room_id});
                changedRoom.in_menu = status;
                
                if (roomModel.getRooms()) {
                    changedRoom = _.findWhere(roomModel.getRooms(), {id: room_id});
                    
                    if (changedRoom) {
                        changedRoom.in_menu = status;
                    }
                }
                
                callback(true);
            },
            error: (response) => {
                console.error(response);
                callback();
            }
        });
    },

    // check if values of list parameter are different
    allValuesSame: function(list) {

        for(var i = 1; i < list.length; i++)
        {
            if(list[i] !== list[0])
                return false;
        }

        return true;
    },

    // check if all rooms are in menu or not
    checkListOfRooms: function(type) {
        var model = this;

        var list = [];
        
        if(type === 'in-menu') {
            _.each(model.roomsList, function(room) {
                list.push(room.in_menu);
            })
        } else if (type === 'status') {
            _.each(model.roomsList, function(room) {
                list.push(room.active);
            })
        } else {
            _.each(model.roomsList, function(room) {
                list.push(room.students_logged_in);
            })
        }
        

        return this.allValuesSame(list);
    },

        

    filterRooms: function(type) {
        if (!this.checkListOfRooms(type) || type === 'room-name') {
            if (type === 'room-name') {
                this.roomFilter = this.roomFilter === 'roomNameASC' ? 'roomNameDSC' : 'roomNameASC';
            } else if (type === 'in-menu') {
                this.roomFilter = this.roomFilter === 'inMenuASC' ? 'inMenuDSC' : 'inMenuASC';
            } else if (type === 'status') {
                this.roomFilter = this.roomFilter === 'statusASC' ? 'statusDSC' : 'statusASC';
            }
            
            this.filtersApplied = true;
            this.updateRoomsList();
        }
    },

    manageRoster: function(room_name) {
        var activeRoster = _.findWhere(this.roomsList, {name: room_name});
        this.set('roster_in_edit', activeRoster);

        if (!activeRoster.rosterInEdit) {
            for (var item of this.roomsList) {
                item.rosterInEdit = false;
            }
            activeRoster.rosterInEdit = true;
        } else {
            activeRoster.rosterInEdit = false;
        }
        this.updateRoomsList();
    },
    
    /**
     * Called after the teacher imports a roster using excel or csv.
     * @param  {String} roomName the name of the room
     * @param  {Array} students  the list of students
     */
    rosterImported: function(roomName, students) {
        var activeRoom = _.findWhere(this.roomsList, {name: roomName});
        activeRoom.rostered = true;
        activeRoom.rosterInEdit = true;
        activeRoom.roster = students;
        activeRoom.student_count = students.length;
        activeRoom.notify = {
            status: true,
            message: translate('{0} students imported!').format(students.length)
        };
        
        if (activeRoom.name === roomModel.getRoomName()) {
            roomModel.set('rostered', true);
            roomModel.set('students', students);
        }

        this.studentFilter = 'nameDSC';
        this.filterStudents('name');
        this.set('roster_in_edit', activeRoom);
        this.updateRoomsList();
        setTimeout(() => {
            activeRoom.notify = '';
            this.updateRoomsList();
        }, Constants.BANNER_TIMEOUT);
    },

    deleteRoster: function() {
        let deletedRoster = this.get('roster_in_edit');

        request.del({
            url: `${window.backend_host}/rooms/api/${deletedRoster.name}/roster/`,
            success: () => {
                deletedRoster.roster = [];
                deletedRoster.student_count = 0;
                deletedRoster.rostered = false;
                deletedRoster.rosterInEdit = false;
                deletedRoster.notify = {
                    'status': true,
                    'message': translate('Roster deleted!')
                };
                
                this.updateRoomsList(this.roomsList);

                if (deletedRoster.name === roomModel.getRoomName()) {
                    roomModel.set('rostered', false);
                    header.render('rooms');
                }
                
                setTimeout(() => {
                    deletedRoster.notify = '';
                    this.updateRoomsList();
                }, this.get('notify_timer'));
                
                this.updateRoomsList(this.roomsList);
            },
            error: (response) => {
                console.log('Error deleting roster:', response);
            }
        });
    },

// ==================================================== 
// =================== manage students ================
// ==================================================== 
    filterFunction: function(type) {
        var rosterInEdit = this.get('roster_in_edit');
        var numbers = [];
        var strings = [];
        _.each(rosterInEdit.roster, function(student) {
            if (_.isNaN(Number(student.student_id))) {
                strings.push(student);
            } else {
                numbers.push(student);
            }
        })

        numbers = _.sortBy(numbers, function(number) {
            return Number(number.student_id)
        })
        strings = _.sortBy(strings, function(string) {
            return string.student_id
        })

        if (type === 'DSC') {
            numbers = numbers.reverse();
            strings = strings.reverse();
        }

        return numbers.concat(strings)
    },

    // check if all rooms are in menu or not
    checkListOfstudentsInRoom: function() {
        var model = this;

        var rosterInEdit = model.get('roster_in_edit');
        var list = [];
        _.each(rosterInEdit.roster, function(student) {
            list.push(student.logged_in);
        })
        
        return this.allValuesSame(list);
    },

    filterStudents: function(type) {
        let rosterInEdit = this.get('roster_in_edit');
        
        if (type === 'name') {
            this.studentFilter = this.studentFilter === 'nameASC' ? 'nameDSC' : 'nameASC';
        } else if (type === 'student_id') {
            this.studentFilter = this.studentFilter === 'student_idASC' ? 'student_idDSC' : 'student_idASC';
        } else if (type === 'presence') {
            // do the sort by presence only if the values are different
            if (this.checkListOfstudentsInRoom()) {
                return;
            } else {
                this.studentFilter = this.studentFilter === 'presenceASC' ? 'presenceDSC' : 'presenceASC';
            }
        }
        
        switch (this.studentFilter) {
            case 'nameASC':
                rosterInEdit.roster = _.sortBy(rosterInEdit.roster, function(num) {
                    return num.last_name.toLowerCase();
                });
                break;
            case 'nameDSC':
                let newList = _.sortBy(rosterInEdit.roster, function(num) {
                    return num.last_name.toLowerCase();
                });
                rosterInEdit.roster = newList.reverse();
                break;
            case 'presenceASC':
                rosterInEdit.roster = _.sortBy(_.sortBy(rosterInEdit.roster, 'last_name'), "logged_in").reverse();
                break;
            case 'presenceDSC':
                rosterInEdit.roster = _.sortBy(_.sortBy(rosterInEdit.roster, 'last_name'), "logged_in");
                break;
            case 'student_idASC':
                rosterInEdit.roster = this.filterFunction('ASC');
                break;
            case 'student_idDSC':
                rosterInEdit.roster = this.filterFunction('DSC');
                break;
        }
        
        this.updateRoomsList();
    },

    editStudentButtonClicked: function(studentID) {
        
        var activeRoster = this.get('roster_in_edit');
        var editedStudent =  _.findWhere(activeRoster.roster, {student_id: studentID});
        var clonedStudent = {
            first_name: editedStudent.first_name,
            last_name: editedStudent.last_name,
            student_id: editedStudent.student_id
        }
        editedStudent.studentClone = _.clone(clonedStudent)
        editedStudent.inEdit = true;
        this.updateRoomsList();
    },

    cancelEditStudentButtonClicked: function(studentID, singular) {       
        var activeRoster = this.get('roster_in_edit');
        // if new singular student
        if (singular) {
            activeRoster.new_students_list = [];
            // set the roster in edit false
            // if adding a new student on mobile to a non rostered room
            if (platform.isMobile() && !platform.isTablet && !activeRoster.rostered) {
                activeRoster.rosterInEdit = false;
            }
        } else {
            var editedStudent =  _.findWhere(activeRoster.roster, {id: studentID});

            editedStudent.first_name = editedStudent.studentClone.first_name;
            editedStudent.last_name = editedStudent.studentClone.last_name;
            editedStudent.student_id = editedStudent.studentClone.student_id;

            editedStudent.inEdit = false;
            editedStudent.errors = {};
        }

        this.updateRoomsList();
    },

    isIdDuplicated: function(studentId, updateStudent) {
        let room = this.get('roster_in_edit');
        
        let rosterCount = _.countBy(room.roster, function(student) {
            return student.student_id === studentId
        });
        
        // Remove the current ID from the checklist when editing an existing student.
        let condition = rosterCount['true'] > 0;
        
        if (updateStudent) {
            condition = rosterCount['true'] - 1 > 0;
        }
        
        if (condition) {
            return true
        } else {
            rosterCount = _.countBy(this.newStudents, function(student) {
                return student.student_id === studentId
            });
            
            return rosterCount['true'] - 1 > 0;
        }
    },

    validateStudent: function(element, value, indexPos, oldStudentID) {
        let activeRoster = this.get('roster_in_edit'),
            student = {};
        
        if (indexPos === 'noIndex') {
            // Validating an existing student
            student = _.findWhere(activeRoster.roster, {id: oldStudentID});
        } else {
            // Validating a new student
            student = _.findWhere(this.newStudents, {indexPos: indexPos});
            student.hideTooltip = this.newStudents.length > 1;
        }
        
        let errors = student.errors ? student.errors : {};
        
        switch(element) {
            case 'first_name':
                if (!value) {
                    errors.first_name = translate('Please provide a first name.');
                } else {
                    errors.first_name = null;
                }
                student.errors = errors;
                student.first_name = value;
                break;
            case 'last_name':
                if (!value) {
                    errors.last_name = translate('Please provide a last name.');
                } else {
                    errors.last_name = null;
                }
                student.errors = errors;
                student.last_name = value;
                break;
            case 'student_id':
                if (!value) {
                    errors.student_id = translate('Please provide an ID.');
                } else {
                    if( this.isIdDuplicated(value) ) {
                        errors.student_id = translate('This ID is already in use.');
                    } else {
                        errors.student_id = null;
                    }
                }
                student.errors = errors;
                student.student_id = value;
                break;
        }
        
        return student;
    },

    updateStudent: function(roomName, student_id) {
        if (_.isBoolean(student_id) && student_id) {
            this.saveNewStudents('single');
        } else {
            this.updateExistingStudent(roomName, student_id);
        }
    },

    updateExistingStudent: function(roomName, student_id) {
        let errors = {},
            activeRoster = this.get('roster_in_edit'),
            editedStudent = _.findWhere(activeRoster.roster, {id: student_id});
        
        // Revalidate the student.
        if (editedStudent.first_name === '') {
            errors.first_name = {'isValid': false, 'errorMsg': translate('Please provide a first name.')};
        }
        
        if (editedStudent.last_name === '') {
            errors.last_name = {'isValid': false, 'errorMsg': translate('Please provide a last name.')};
        }
        
        if (editedStudent.student_id === '') {
            errors.student_id = {'isValid': false, 'errorMsg': translate('Please provide an ID.')};
        } else {
            if (this.isIdDuplicated(editedStudent.student_id, true)) {
                errors.student_id = {'isValid': false, 'errorMsg': translate('This ID is already in use.')};
            }
        }

        editedStudent.errors = errors;
        
        if (_.isEmpty(errors)) {
            request.put({
                url: `${window.backend_host}/rooms/api/student/${editedStudent.id}/${roomName}`,
                data: editedStudent,
                success: () => {
                    editedStudent.inEdit = false;
                },
                error: (response) => {
                    console.error(response);
                },
                complete: () => {
                    this.updateRoomsList();
                }
            });
        } else {
            this.updateRoomsList();
        }
    },

    deleteStudent: function(student, room_name) {
        let roomInEdit = this.get('roster_in_edit');

        request.del({
            url: `${window.backend_host}/rooms/api/student/${student.id}/${room_name}`,
            success: () => {
                roomInEdit.roster = _.without(roomInEdit.roster, _.findWhere(roomInEdit.roster, {id: student.id}));

                roomInEdit.notify = {
                    status: true,
                    message: translate('Student deleted!')
                };
                
                roomInEdit.student_count--;
                
                if (roomInEdit.student_count === 0) {
                    roomInEdit.rostered = false;
                    roomInEdit.rosterInEdit = false;
                }

                // if a student was deleted from the current room
                if (room_name == roomModel.getRoomName()) {
                    roomModel.set({
                        'students': roomInEdit.roster,
                        'student_count': roomInEdit.student_count,
                        'rostered': roomInEdit.student_count !== 0
                    });
                    header.render('rooms');    
                }

                this.updateRoomsList();

                setTimeout(() => {
                    roomInEdit.notify = '';
                    this.updateRoomsList();
                }, this.get('notify_timer'))
            },
            error: (response) => {
                console.error(response);
            }
        });
    },

    /**
     * =========================
     * ADD NEW STUDENTS ACTIONS
     * =========================
     */

     /**
      * method used to show when the user chooses to manually add new students
      * get the max number of new students from the model 
      * and trigger the select new students popup
      */
    addStudentsManually: function() {
        let activeRoster = this.get('roster_in_edit');
        
        if (activeRoster.rostered) {
            this.maxStudents = 150 - activeRoster.roster.length;
        } else {
            this.maxStudents = 150;
        }
        
        this.trigger('select_new_students_popup:success', activeRoster.name); 
    },

    addOneStudent: function() {
        this.addStudents(1, 'oneStudent');
    },

    addStudents: function(students, param) {
        let newStudents = [];
        
        for (let i = 1; i <= students; i++) {
            newStudents.push({
                first_name : '',
                last_name : '',
                student_id : '',
                inEdit : true,
                indexPos: i,
                newStudent: true
            });
        }
        
        this.newStudents = newStudents;
        
        if (param === 'oneStudent') {
            let activeRoster = this.get('roster_in_edit');
            activeRoster.new_students_list = newStudents;

            if (platform.isMobile() && !platform.isTablet) {
                activeRoster.message = true;
            }
            
            this.updateRoomsList();
        } else {
            this.trigger('new_students_popup:success');
        }
    },

    addOneNewStudent: function() {
        let newStudents = this.newStudents,
            listLength = newStudents.length;
        
        newStudents.push({
            first_name : '',
            last_name : '',
            student_id : '',
            inEdit : true,
            indexPos: listLength + 1,
            newStudent: true
        });
        
        // If the initial list had only one student and we added another, remove the error tooltip from the list. 
        if (newStudents.length === 2) {
            _.each(newStudents, function(student) {
                student.hideTooltip = true;
            });
        }
        
        this.newStudents = newStudents;
        return newStudents;
    },

    removeNewStudent: function(index) {
        // Remove the student at index and reorder the list of students.
        let newStudents = this.newStudents;

        newStudents = _.without(newStudents, _.findWhere(newStudents, {indexPos: index}));
        _.each(newStudents, function(newStudent, index) {
            newStudent.indexPos = index + 1;
        });

        // If there is only one student in the modal, show the tooltip error.
        if (newStudents.length === 1) {
            _.each(newStudents, function(student) {
                student.hideTooltip = false;
            })
        }

        // When only one student is in the modal and the teacher removes it, close the modal.
        if (newStudents.length === 0) {
            this.trigger('modal:close');
            return;
        }

        this.newStudents = newStudents;
        return newStudents;
    },

    saveNewStudents: function(param) {
        let newStudents = this.newStudents,
            activeRoster = this.get('roster_in_edit'),
            allStudentsInvalid = true;

        // Iterate over the students and revalidate each.
        for (let student of newStudents) {
            let errors = {};
            
            if (student.first_name === '') {
                errors.first_name = translate('Please provide a first name.');
            }

            if (student.last_name === '') {
                errors.last_name = translate('Please provide a last name.');
            }

            if (student.student_id === '') {
                errors.student_id = translate('Please provide an ID.');
            } else if (this.isIdDuplicated(student.student_id)) {
                errors.student_id = translate('This ID is already in use.');
            }

            if (!param) {
                // Remove new students that have all fields invalid.
                if (errors.first_name && errors.last_name && errors.student_id) {
                    this.removeNewStudent(student.indexPos);
                } else {
                    allStudentsInvalid = false;
                }
            }

            student.errors = errors;
        }

        newStudents = this.newStudents;

        let activeErrors = _.filter(newStudents, function(student) {
            return !_.isEmpty(student.errors)
        });
        
        if (activeErrors.length < 1) {
            request.post({
                url: `${window.backend_host}/rooms/api/students/`,
                data: {
                    students: this.newStudents,
                    room_name: activeRoster.name
                },
                success: (data) => {
                    let studentCount = 0;
                    
                    _.each(data.students, function(student) {
                        student.inEdit = false;
                        studentCount ++;
                    });

                    if (activeRoster.roster) {
                        activeRoster.roster = data.students.concat(activeRoster.roster);
                        activeRoster.student_count += studentCount;
                    } else {
                        activeRoster.roster = data.students;
                        activeRoster.student_count = studentCount;
                    }

                    activeRoster.newStudents = [];
                    activeRoster.rostered = true;

                    // If we added students to the current room, update the room model.
                    if (activeRoster.name === roomModel.getRoomName()) {
                        roomModel.set({
                            'students': activeRoster.roster,
                            'student_count': activeRoster.student_count,
                            'rostered': true
                        });
                        header.render('rooms');
                    }

                    if (param === 'single') {
                        activeRoster.new_students_list = [];
                    } else {
                        this.trigger('modal:close');
                    }

                    activeRoster.notify = {
                        status: true,
                        message: translate('Students added!')
                    };

                    this.updateRoomsList();

                    setTimeout(() => {
                        activeRoster.notify = '';
                        this.updateRoomsList();
                    }, this.get('notify_timer'))
                },
                error: (response) => {
                    console.error(response);
                }
            });
        } else {
            // If we're adding a single student inline, force a rerender of the page.
            if (param === 'single') {
                this.updateRoomsList();
            } else {
                if (allStudentsInvalid) {
                    this.trigger('modal:close');
                } else {
                    return newStudents;
                }
            }
        }
    },

    cancelAddingNewStudents: function (room_name) {
        var model = this;

        var activeRoster = model.get('roster_in_edit');
        activeRoster.newStudents = [];
        model.updateRoomsList();
    }
});

module.exports = new RoomsModel();
