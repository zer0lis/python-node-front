'use strict';

import _ from 'underscore';
import teacher from 'TeacherModel';
import Constants from 'Constants';
import request from 'Request';

class RosterFileModel {
    
    constructor() {
        /*--------------*/
        /*    Client    */
        /*--------------*/
        this.fileName = ''; // The name of the roster file selected for upload by the teacher.
        this.importing = false; // Whether the modal is being used for import (true) or export (false).
        this.pageBlurred = false; // Whether the page has been blurred after opening the modal.
        this.progress = 0; // The progress of the current roster upload (0% => 100%).
        this.showExportSummary = this.hasExportSettings; // Whether the teacher has exported a roster before.
        this.uploadErrors = []; // List of errors encountered during roster upload.
        this.uploading = false; // Whether the teacher is currently uploading a roster.
        
        /*--------------*/
        /*    Shared    */
        /*--------------*/
        this.exportHeader = _.isBoolean(teacher.rosterExportHeader) ? teacher.rosterExportHeader : true; // Whether the first line of an exported roster should be a header.
        this.exportOrder = teacher.rosterExportOrder || Constants.LAST_FIRST_ID; // The column order in exported rosters.
        this.exportType = teacher.rosterExportType || Constants.CSV; // The roster export type.
        this.importHeader = _.isBoolean(teacher.rosterImportHeader) ? teacher.rosterImportHeader : true; // Whether the first line of the roster import file is a header.
        this.importOrder = teacher.rosterImportOrder || Constants.LAST_FIRST_ID; // The column order in imported rosters.
        this.importType = teacher.rosterImportType || Constants.CSV; // The roster import type.
        this.roomName = ''; // The name of the room associated with the roster.
        this.students = []; // The student objects returned by the upload API.
    }
    
    get importSettingsChanged() {
        return teacher.rosterImportOrder  !== this.importOrder  ||
               teacher.rosterImportType   !== this.importType   ||
               teacher.rosterImportHeader !== this.importHeader;
    }
    
    get exportSettingsChanged() {
        return teacher.rosterExportOrder  !== this.exportOrder  ||
               teacher.rosterExportType   !== this.exportType   ||
               teacher.rosterExportHeader !== this.exportHeader;
    }
    
    get hasExportSettings() {
        return teacher.rosterExportOrder  !== null &&
               teacher.rosterExportType   !== null &&
               teacher.rosterExportHeader !== null;
    }
    
    fetchSettings(callback) {
        request.get({
            url: `${window.backend_host}/rooms/api/roster-settings/`,
            success: (settings) => {
                if (settings) {
                    if (settings.id)            {teacher.rosterSettingsId   = settings.id}
                    if (settings.import_order)  {teacher.rosterImportOrder  = this.importOrder      = settings.import_order}
                    if (settings.import_type)   {teacher.rosterImportType   = this.importType       = settings.import_type}
                    if (settings.import_header) {teacher.rosterImportHeader = this.importHeader     = settings.import_header}
                    if (settings.export_order)  {teacher.rosterExportOrder  = this.exportOrder      = settings.export_order}
                    if (settings.export_type)   {teacher.rosterExportType   = this.exportType       = settings.export_type}
                    if (settings.export_header) {teacher.rosterExportHeader = this.exportHeader     = settings.export_header}
                }
                
                teacher.rosterSettingsFetched = true;
                this.showExportSummary = this.hasExportSettings;
            },
            error: (response) => {
                console.log('Error fetching roster file settings:', response);
            },
            complete: () => {
                callback();
            }
        });
    }
    
    saveSettings(type, callback) {
        let url = `${window.backend_host}/rooms/api/roster-settings/`,
            method = 'POST',
            data = {
                import_order: this.importOrder,
                import_type: this.importType,
                import_header: this.importHeader,
                export_order: this.exportOrder,
                export_type: this.exportType,
                export_header: this.exportHeader
            };
        
        if (teacher.rosterSettingsId !== 0) {
            url += `${type}/${teacher.rosterSettingsId}/`;
            method = 'PUT';
            if (type === 'import') {
                delete data.export_header;
                delete data.export_type;
                delete data.export_header;
            } else {
                delete data.import_header;
                delete data.import_type;
                delete data.import_header;
            }
        }
        
        request.send(method, {
            url: url,
            data: data,
            success: (response) => {
                let newSettings = method === 'POST';
                
                if (newSettings) {
                    teacher.rosterSettingsId = response.id;
                }
                
                let importSaved = newSettings || type === 'import',
                    exportSaved = newSettings || type === 'export';
                
                if (importSaved) {
                    teacher.rosterImportOrder = this.importOrder;
                    teacher.rosterImportType = this.importType;
                    teacher.rosterImportHeader = this.importHeader;
                }
                
                if (exportSaved) {
                    teacher.rosterExportOrder = this.exportOrder;
                    teacher.rosterExportType = this.exportType;
                    teacher.rosterExportHeader = this.exportHeader;
                    this.showExportSummary = true;
                }
            },
            error: (response) => {
                console.log('Error saving/updating roster file import settings:', response);
            },
            complete: () => {
                if (callback) {
                    callback();
                }
            }
        });
    }
    
    uploadRoster(file, progressCallback, doneCallback) {
        let data = new FormData();
        data.append('0', file); // The back end expects '0' for the file's parameter name.
        data.append('room_name', this.roomName);
        
        request.post({
            url: `${window.backend_host}/rooms/api/students/import/`,
            data: data,
            progress: (event) => {
                progressCallback(event.loaded / event.total * 100);
            },
            success: (response) => {
                this.students = response.students;
                doneCallback();
            },
            error: (response) => {
                doneCallback(response.error);
            }
        });
    }
    
}

module.exports = RosterFileModel;
