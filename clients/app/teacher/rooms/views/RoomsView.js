import React from 'react';
import _ from 'underscore';
import ProIcon from 'ProIcon';
import RoomItem from 'RoomItem';
import Tooltip from 'Tooltip';
import user from 'user';
import {translate} from 'translator';

let e = React.createElement,
    c = null,
    m = null;

class AddRoomButton extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            bodyClicked: this.bodyClicked.bind(this), // Make it possible to remove the bodyClicked listener.
            error: null
        };
    }
    
    bodyClicked() {
        document.removeEventListener('click', this.state.bodyClicked);
        
        this.setState({
            error: null
        });
    }

    render() {
        return e('span', {
            className: this.props.extraClasses || '',
            style: {position: 'relative'},
            children: [
                e('button', {
                    className: 'button button-primary button-small pill' + (this.props.disabled ? ' disabled' : ''),
                    children: [
                        e('svg', {
                            className: 'add-room-icon',
                            width: '16px',
                            height: '16px',
                            viewBox: '0 0 16 16',
                            dangerouslySetInnerHTML: {__html: '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"><g transform="translate(-991.000000, -182.000000)" stroke="#FFFFFF" stroke-width="2"><g transform="translate(966.000000, 168.000000)"><g transform="translate(33.000000, 22.000000) rotate(-315.000000) translate(-33.000000, -22.000000) translate(28.000000, 17.000000)"><path d="M8.984375,0.384615385 L4.42312853e-13,9.23076923"></path><path d="M4.42312853e-13,0.769230769 L8.984375,9.61538462"></path></g></g></g></g>'}
                        }),
                        translate('Add Classroom')
                    ],
                    onClick: () => {
                        if (user.isPro()) {
                            if (this.props.disabled) {
                                this.setState({
                                    error: e(Tooltip, {
                                        location: 'above',
                                        title: translate('Classroom Limit'),
                                        text: translate('You have reached the maximum number of classrooms.')
                                    })
                                });
                
                                document.addEventListener('click', this.state.bodyClicked);
                            } else {
                                c.addRoomClicked();
                            }
                        } else {
                            c.goProClicked();
                        }
                    }
                }),
                this.state.error
            ]
        });
    }
    
}

class RoomsFilters extends React.Component {
    
    render() {
        return e('div', {
            className: user.isPro() ? 'rooms-filters' : 'rooms-filters disabled',
            children: [
                e('span', {
                    className: 'filter in-menu-checkbox' + (m.roomFilter === 'inMenuASC' || m.roomFilter === 'inMenuDSC' ? ' active' : ''),
                    onClick: () => {
                        if (user.isPro()) {
                            c.filterRooms('in-menu');
                        }
                    },
                    children: [
                        translate('In Menu'),
                        e('i', {
                            'data-icon': m.roomFilter === 'inMenuASC' ? 'k' : 'm'
                        })
                    ]
                }),
                e('span', {
                    className: 'filter room-status-column' + (m.roomFilter === 'statusASC' || m.roomFilter === 'statusDSC' ? ' active' : ''),
                    onClick: () => {
                        if (user.isPro()) {
                            c.filterRooms('status');
                        }
                    },
                    children: [
                        translate('Status'),
                        e('i', {
                            'data-icon': m.roomFilter === 'statusASC' ? 'k' : 'm'
                        })
                    ]
                }),
                e('span', {
                    className: 'filter filter-room-name' + (m.roomFilter === 'roomNameASC' || m.roomFilter === 'roomNameDSC' ? ' active' : ''),
                    children: e('span', {
                        onClick: () => {
                            if (user.isPro()) {
                                c.filterRooms('room-name');
                            }
                        },
                        children: [
                            translate('Classroom Name'),
                            e('i', {
                                'data-icon': m.roomFilter === 'roomNameASC' ? 'k' : 'm'
                            })
                        ]
                    })
                }),
                e('span', {
                    className: 'right-buttons',
                    children: [
                        e('span', {
                            className: 'right-side',
                            children: [
                                translate('Share')
                            ]
                        }),
                        e('span', {
                            className: 'right-side',
                            children: [
                                translate('Roster')
                            ]
                        }),
                        e('span', {
                            className: 'right-side',
                            children: [
                                translate('Delete')
                            ]
                        })
                    ]
                })
            ]
        });
    }
}

export default class RoomsView extends React.Component {
    
    render() {
        c = this.props.controller;
        m = this.props.model;
        
        let roomsListChildren = [],
            roomsList = m.roomsList;

        // If no filter is applied, make the default room first in the list.
        if (!m.filtersApplied) {
            let defaultRoom = _.findWhere(roomsList, {'default': true});
            roomsList = _.without(roomsList, defaultRoom);
            roomsList.unshift(defaultRoom);
        }

        for (let room of roomsList) {
            let aRoom = e(RoomItem, Object.assign({
                room: room
            }, this.props));

            roomsListChildren.push(aRoom);
        }
        
        let roomsExtraChildren = '';
        
        if (!user.isPro()) {
            roomsExtraChildren = e('div', {
                className: 'non-premium-info',
                children: [
                    e(ProIcon),
                    e('p', {
                        children: translate('Go Pro to deliver multiple activities at the same time in up to 10 classrooms, and much more!')
                    }),
                    e('button', {
                        className: 'button button-primary button-large pill',
                        onClick: c.goProClicked,
                        children: translate('LEARN MORE')
                    })
                ]
            });
        }
        
        return e('div', {
            className: 'rooms-container form',
            children: [
                e('div', {
                    className: 'clearfix',
                    children: [
                        e('h1', {
                            className: 'page-title',
                            children: translate('Classrooms')
                        }),
                        e(AddRoomButton, {
                            disabled: roomsList.length === 10,
                            extraClasses: ' align-right'
                        })
                    ]
                }),
                e(RoomsFilters),
                e('ul', {
                    children: roomsListChildren
                }),
                roomsExtraChildren
            ]
        });
    }

}
