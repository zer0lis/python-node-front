import React from 'react';
import _ from 'underscore';
import DropdownList from 'DropdownList';
import StudentCountIcon from 'StudentCountIcon';
import ShareIcon from 'ShareIcon';
import RosterView from 'RosterView';
import ActivityStatus from 'ActivityStatus';
import Tooltip from 'Tooltip';
import globalRoom from 'room';
import platform from 'Platform';
import {translate} from 'translator';

let e = React.createElement,
    c = null,
    m = null;

class RoomEntry extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            bodyClicked: this.bodyClicked.bind(this), // Make it possible to remove the bodyClicked listener.
            error: false,
            inMenu: this.props.roomData.in_menu,
            moreButtonClicked: false,
            roomError: '',
            roomName: this.props.roomData.name
        };
    }
    
    componentWillReceiveProps(newProps) {
        this.setState({
            roomName: newProps.roomData.name,
            roomError: newProps.roomData.errorMessage ? newProps.roomData.errorMessage : '',
            inMenu: newProps.roomData.in_menu
        });
    }
    
    bodyClicked(event) {
        let node = event.target;
        
        if (node.className === 'more-actions-container') {
            return;
        }
        
        while (node = node.parentNode) {
            if (node.className === 'more-actions-container') {
                return;
            }
        }
        
        if (platform.isIos) {
            document.body.style.cursor = 'default';
        }
        
        document.body.removeEventListener('click', this.state.bodyClicked);

        this.setState({
            moreButtonClicked: false,
            error: false
        });
    }
    
    deleteRoom(data, event) {
        if (!event) {
            event = data;
        }
        
        event.nativeEvent.stopImmediatePropagation();
        
        if (this.props.roomData.default) {
            this.setState({
                 error: !this.state.error
             });
        } else if( !this.props.roomData.active ) {
            this.props.deleteRoom();
        } else {
            this.setState({
                 error: !this.state.error
             });
        }
    }
    
    inMenuClicked(event) {
        event.nativeEvent.stopImmediatePropagation();
        
        if (this.props.roomData.default) {
            return;
        }

        this.setState({
            inMenu: !this.state.inMenu
        });
        
        c.inMenuChanged(this.props.roomData.id, !this.state.inMenu);
    }
    
    render() {
        let isRoomDefault = !!this.props.roomData.default,
            manageRosterClass = 'roster-action-button';
        
        if (this.props.roomData.active || isRoomDefault) {
            manageRosterClass += ' disabled';
        }
        
        if (platform.isMobile()) {
            manageRosterClass += ' mobile'
        }

        let rosterActionButtonChildren = e('span', {
            children: [
                e('div', {
                    className: 'icon-class',
                    children: e(StudentCountIcon, {
                        color: this.props.roomData.active ? '#cccccc' : '#6D9EC4'
                    })
                }),
                e('span', {
                    className: 'in-roster-students',
                    children: this.props.roomData.student_count
                }),
                e('svg', {
                    className: 'in-edit-icon',
                    width: '10px',
                    height: '6px',
                    viewBox: '0 0 10 6',
                    style: {transform: this.props.roomData.rosterInEdit ? 'rotate(180deg)' : ''},
                    dangerouslySetInnerHTML: {__html: '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"><g transform="translate(-994.000000, -392.000000)" stroke="#8CB4D2" stroke-width="2"><g transform="translate(98.000000, 319.000000)"><g transform="translate(739.000000, 39.000000)"><g><g><g transform="translate(114.000000, 5.000000)"><g><g transform="translate(9.000000, 5.000000)"><g transform="translate(0.000000, 18.000000)"><g transform="translate(39.000000, 9.000000) rotate(-90.000000) translate(-39.000000, -9.000000) translate(37.000000, 5.000000)"><path style="font-size:medium;font-style:normal;font-variant:normal;font-weight:normal;font-stretch:normal;text-indent:0;text-align:start;text-decoration:none;line-height:normal;letter-spacing:normal;word-spacing:normal;text-transform:none;direction:ltr;block-progression:tb;writing-mode:lr-tb;text-anchor:start;baseline-shift:baseline;color:#000000;fill-opacity:1;stroke:none;stroke-width:2;marker:none;visibility:visible;display:inline;overflow:visible;enable-background:accumulate;font-family:Sans;-inkscape-font-specification:Sans;" d="M 3.96875,8.6875 A 1.0001,1.0001 0 0 1 3.28125,8.375 L -0.375,4.71875 a 1.0001,1.0001 0 0 1 0,-1.4375 L 3.28125,-0.375 a 1.0054782,1.0054782 0 1 1 1.4375,1.40625 L 1.75,4 4.71875,6.96875 a 1.0001,1.0001 0 0 1 -0.75,1.71875 z" /></g></g></g></g></g></g></g></g></g></g></g>'}
                })
            ]
        });
        
        if (!this.props.roomData.rostered || this.props.roomData.student_count < 1) {
            rosterActionButtonChildren = e('svg', {
                className: 'add-roster-icon',
                width: '29px',
                height: '27px',
                viewBox: '0 0 29 27',
                dangerouslySetInnerHTML: {__html: '<path d="M20,18.1c1.1,0,2.1-0.2,3-0.5c0.6,0.5,1,1.2,1,1.9v1.8c0,0.6-0.2,1.2-0.7,1.7c-0.5,0.5-1.1,0.8-1.7,0.8h-1.9 c0-0.2,0.1-0.4,0.1-0.5v-1.5h1.8c0.1,0,0.2,0,0.3-0.1s0.1-0.2,0.1-0.3v-1.8c0-0.2-0.1-0.3-0.2-0.4l-1.6-0.8l-0.5-0.3 C19.7,18.1,19.8,18.1,20,18.1z M3.8,21.8H2.4c-0.3,0-0.4-0.1-0.4-0.4v-1.8c0-0.2,0.1-0.3,0.2-0.4l1.7-0.8L4.6,18 c-0.3-0.2-0.5-0.4-0.7-0.8c-0.2-0.3-0.2-0.6-0.2-1c0-0.2,0-0.4,0.1-0.6c0-0.2,0.2-0.7,0.6-1c0.4-0.3,0.8-0.4,1-0.4 c0.4,0,0.7,0.1,1,0.4c0.1,0.1,0.1,0.1,0.2,0.2c0-0.3,0.1-0.7,0.2-1c0,0,0,0,0-0.1c0.1-0.4,0.3-0.7,0.5-1.1c-0.6-0.3-1.2-0.5-1.8-0.5 c-0.9,0-1.7,0.3-2.3,0.9c-0.6,0.5-1,1.2-1.2,2c0,0,0,0,0,0.1c-0.1,0.3-0.1,0.7-0.1,1.1c0,0.3,0,0.6,0.1,0.9l-0.3,0.2c0,0,0,0,0,0 C0.5,17.7,0,18.5,0,19.5v1.8c0,1.4,1,2.4,2.4,2.4h1.4c0-0.2-0.1-0.4-0.1-0.5V21.8z M14.2,16.8c-0.2,0.5-0.6,0.9-1,1.2l1,0.5l2.3,1.1 c0.2,0.1,0.4,0.3,0.4,0.6v2.6c0,0.2-0.1,0.3-0.2,0.5c0,0,0,0,0,0c0,0,0,0,0,0c-0.1,0.1-0.3,0.2-0.4,0.2l-6.1,0H7.7c0,0-0.1,0-0.1,0 l-0.1,0c-0.4,0-0.6-0.2-0.6-0.6l0-2.6c0-0.3,0.1-0.5,0.3-0.6l2.5-1.1l1-0.4c-0.4-0.3-0.8-0.6-1-1.1c-0.2-0.4-0.3-0.9-0.3-1.4 c0-0.3,0-0.6,0.1-0.8c0.2-0.6,0.5-1.1,0.9-1.5c0.4-0.3,0.8-0.5,1.2-0.5c-0.3-0.6-0.5-1.2-0.6-1.9c-0.7,0.2-1.3,0.5-1.9,1 c-0.2,0.2-0.5,0.5-0.6,0.7c0,0.1-0.1,0.1-0.1,0.2c-0.2,0.3-0.3,0.5-0.5,0.8c0,0,0,0,0,0c-0.1,0.2-0.2,0.5-0.2,0.7c0,0,0,0,0,0.1 c-0.1,0.4-0.2,0.9-0.2,1.3c0,0.6,0.1,1.2,0.3,1.7l-1.2,0.5c0,0,0,0,0,0c-0.1,0.1-0.2,0.2-0.4,0.2c0,0,0,0,0,0 c-0.2,0.1-0.3,0.2-0.4,0.4c0,0,0,0,0,0.1c-0.1,0.1-0.2,0.3-0.3,0.4c0,0.1-0.1,0.1-0.1,0.2c0,0.1-0.1,0.2-0.1,0.4 c-0.1,0.2-0.1,0.4-0.1,0.7v1.6v1c0,0.2,0,0.3,0,0.5c0,0.3,0.1,0.5,0.2,0.7c0,0,0,0.1,0.1,0.1c0,0.1,0.1,0.2,0.1,0.2 c0,0,0.1,0.1,0.1,0.1c0.1,0.1,0.1,0.1,0.2,0.2C6,24.8,6.1,24.9,6.2,25c0,0,0.1,0.1,0.1,0.1c0.1,0,0.2,0.1,0.3,0.1 c0.1,0,0.1,0,0.2,0.1c0.1,0,0.2,0.1,0.3,0.1c0,0,0.1,0,0.1,0c0.2,0,0.3,0,0.5,0h8.5c0.7,0,1.4-0.3,1.9-0.8c0.5-0.5,0.7-1.1,0.7-1.8 v-2.6c0-1-0.5-1.9-1.4-2.3c0,0-0.1,0-0.1-0.1l-0.6-0.3c-0.8-0.3-1.6-0.8-2.3-1.3C14.3,16.3,14.3,16.5,14.2,16.8z M28,9 c0,4.4-3.6,8-8,8s-8-3.6-8-8s3.6-8,8-8S28,4.6,28,9z M26,9c0-3.3-2.7-6-6-6s-6,2.7-6,6s2.7,6,6,6S26,12.3,26,9z M23.5,8.2l-2.4,0 l0-2.4c0-0.6-0.5-1-1-1c-0.6,0-1,0.5-1,1l0,2.4l-2.4,0c-1.3,0-1.3,2,0,2l2.4,0l0,2.4c0,0.6,0.5,1,1,1c0.6,0,1-0.5,1-1l0-2.4l2.4,0 C24.8,10.2,24.8,8.2,23.5,8.2z"/>'}
            });
        }

        let rightSideChildren = e('span', {
            className: 'right-buttons',
            children: [
                e('span', {
                    className: 'share-button-container',
                    onClick: (event) => c.shareRoomClicked(event, this.props.roomData.name),
                    children: e(ShareIcon)
                }),
                e('span', {
                    className: manageRosterClass,
                    onClick: () => this.props.manageRosterClicked(),
                    children: [
                        this.props.errors.editRoster,
                        rosterActionButtonChildren
                    ]
                }),
                e('span', {
                    className: 'delete-button-container' + (this.props.roomData.active || isRoomDefault ? ' disabled':''),
                    onClick: this.props.deleteRoom,
                    children: [
                        e('i', {
                            className: 'delete-room action-icon-button',
                            'data-icon': 'L',
                        }),
                        this.props.errors.deleteRoom
                    ]
                }),
                this.state.moreButtonClicked ? e('div', {
                    className: 'more-actions-container',
                    children: [
                        e('i', {
                            'data-icon': 'f',
                            className: 'more-actions-container-icon'
                        }),
                        e(DropdownList, {
                            children: [
                                {
                                    text: [
                                        e('span', {
                                            className: 'share-button-container-mobile',
                                            children: [
                                                e(ShareIcon, {
                                                    width: '20px',
                                                    height: '20px',
                                                    viewBox: '0 0 23 23'
                                                })
                                            ]
                                        }),
                                        translate('SHARE CLASSROOM')
                                    ],
                                    onClick: (event) => c.shareRoomClicked(event, this.props.roomData.name)
                                },
                                {
                                    text: [
                                        e('input', {
                                            type: 'checkbox',
                                            className: 'in-room-mobile-checkbox',
                                            checked: this.state.inMenu
                                        }),
                                        translate('IN MENU')
                                    ],
                                    onClick: (event) => this.inMenuClicked(event)
                                },
                                {
                                    text: [
                                        e('i', {
                                            'data-icon': 'L',
                                            className: 'delete-room action-icon-button' + (this.props.roomData.active || this.props.roomData.default ? ' disabled' : '')
                                        }), 
                                        translate('DELETE'),
                                        this.state.error ? e(Tooltip, {
                                            location: 'above',
                                            title: this.props.roomData.default ? translate('Not Allowed') : translate('Activity Running'),
                                            text: this.props.roomData.default ? translate('The default classroom cannot be deleted.') : translate('A classroom cannot be deleted while it has an activity running.')
                                        }) : null
                                    ],
                                    onClick: (event) => this.deleteRoom(event)
                                }
                            ]
                        })
                    ]
                }): null,
                e('span', {
                    className: 'more-actions-button',
                    children: [
                        e('div', {
                            className: 'more-actions-button-icon',
                            children: e('svg', {
                                width: '24px',
                                height: '7px',
                                viewBox: '0 0 24 7',
                                style:{fill: '#8cb4d2'},
                                dangerouslySetInnerHTML: {__html: '<path d="M3.5,0C1.6,0,0,1.6,0,3.5S1.6,7,3.5,7S7,5.4,7,3.5S5.4,0,3.5,0z M3.5,5C2.7,5,2,4.3,2,3.5S2.7,2,3.5,2  S5,2.7,5,3.5S4.3,5,3.5,5z M12.5,0C10.6,0,9,1.6,9,3.5S10.6,7,12.5,7S16,5.4,16,3.5S14.4,0,12.5,0z M12.5,5C11.7,5,11,4.3,11,3.5  S11.7,2,12.5,2S14,2.7,14,3.5S13.3,5,12.5,5z M20.5,0C18.6,0,17,1.6,17,3.5S18.6,7,20.5,7S24,5.4,24,3.5S22.4,0,20.5,0z M20.5,5  C19.7,5,19,4.3,19,3.5S19.7,2,20.5,2S22,2.7,22,3.5S21.3,5,20.5,5z"/>'}
                            })
                        }),
                        e('span', {
                            className: 'more-actions-button-text',
                            children: translate('More')
                        })
                    ],
                    onClick: () => {
                        if (this.state.moreButtonClicked === false) {
                            _.defer(() => {
                                this.setState({
                                    moreButtonClicked: !this.state.moreButtonClicked
                                });
                            });
                        }
                        
                        if (platform.isIos) {
                            document.body.style.cursor = 'pointer'; // This makes any element respond to click events in iOS.
                        }
                        
                        document.body.addEventListener('click', this.state.bodyClicked);
                    },
                })
            ]
        });

        let roomNameChildren = e('span', {
            className: 'room-name',
            onClick: () => c.roomNameClicked(this.state.roomName),
            children: this.state.roomName
        });
        
        if (isRoomDefault) {
            roomNameChildren = e('span', {
                className: 'default-room-container',
                children: [
                    e('span', {
                        className: 'room-name',
                        onClick: () => c.roomNameClicked(this.state.roomName),
                        children: this.state.roomName
                    }),
                    e('span', {
                        className: 'default-room-info',
                        children: translate('Default Classroom')
                    })
                ]
            });
        }

        return e('div', {
            children: [
                e('span', {
                    className: 'in-menu-checkbox',
                    children: e('input', {
                        type: 'checkbox',
                        onChange: (event) => this.inMenuClicked(event),
                        checked: isRoomDefault ? true : this.state.inMenu,
                        disabled: isRoomDefault
                    })
                }),
                e('span', {
                    className: 'activity-status-column',
                    children: e(ActivityStatus, {
                        className: 'activity-status',
                        animated: this.props.roomData.active
                    })
                }),
                e('span', {
                    className: 'room-name-column',
                    children: [
                        roomNameChildren,
                        e('span', {
                            className: 'edit-room-name-container',
                            style: {
                                display: 'inline-block'
                            },
                            children: [
                                e('i', {
                                    'data-icon': 'C',
                                    className: 'edit-room-name action-icon-button' + (this.props.roomData.active ? ' disabled' : ''),
                                    onClick: this.props.editRoomClicked
                                }),
                                this.props.errors.editName
                            ]
                        })
                    ]
                }),
                rightSideChildren
            ]
        });
    }
    
}

export default class RoomItem extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            bodyClicked: this.bodyClicked.bind(this), // Make it possible to remove the bodyClicked listener.
            showDropDown: false,
            errors: {
                editName: null,
                editRoster: null,
                deleteRoom: null
            }
        };
    }
    
    editRoomClicked() {
        if (!this.props.room.active) {
            c.editRoomClicked(this.props.room.id, this.props.room.name);
            
            this.setState({
                errors: {
                    editName: null,
                    editRoster: null,
                    deleteRoom: null
                }
            });
        } else if (!this.state.errors.editName) {
            _.defer(() => {
                this.setState({
                    errors: {
                        editName: e(Tooltip, {
                            location: 'above',
                            title: translate('Activity Running'),
                            text: translate('A classroom cannot be changed while it has an activity running.')
                        })
                    }
                });

                document.addEventListener('click', this.state.bodyClicked);
            });
        } else {
            this.setState({
                errors: {
                    editName: null,
                    editRoster: null,
                    deleteRoom: null
                } 
            });
        }
    }
    
    manageRosterClicked() {
        if (this.props.room.default) {
            if (!this.state.errors.editRoster) {
                _.defer(() => {
                    this.setState({
                        errors: {
                            editRoster: e(Tooltip, {
                                location: 'above',
                                title: translate('Not Allowed'),
                                text: translate('Rosters are not allowed in the default classroom.')
                            })
                        }
                    });
                    document.addEventListener('click', this.state.bodyClicked);
                });
            } else {
                this.setState({
                    errors: {
                        editRoster: null
                    } 
                });
            }
        } else if (!this.props.room.active) {
            c.manageRosterClicked(this.props.room.name);

            if (!this.props.room.rostered && platform.isMobile() && !platform.isTablet) {
                c.addOneStudentClicked();
            } else {
                if (this.state.showDropDown === false) {
                    this.setState({
                        showDropDown: true
                    });

                    document.addEventListener('click', this.state.bodyClicked);
                } else {
                    this.setState({
                        showDropDown: false
                    });
                }
            }
        } else if (!this.state.errors.editRoster) {
            _.defer(() => {
                this.setState({
                    errors: {
                        editRoster: e(Tooltip, {
                            location: 'above',
                            title: translate('Activity Running'),
                            text: translate('A roster cannot be added while an activity is running.')
                        })
                    }
                });
                document.addEventListener('click', this.state.bodyClicked);
            });
        } else {
            this.setState({
                errors: {
                    editName: null,
                    editRoster: null,
                    deleteRoom: null
                } 
            });
        }
    }
    
    deleteRoom() {
        if (this.props.room.default) {
            if (!this.state.errors.deleteRoom) {
                _.defer(() => {
                    this.setState({
                        errors: {
                            deleteRoom: e(Tooltip, {
                                location: 'above',
                                title: translate('Not Allowed'),
                                text: translate('The default classroom cannot be deleted.')
                            })
                        }
                    });
                    document.addEventListener('click', this.state.bodyClicked);
                });                
            } else {
                this.setState({
                    errors: {
                        deleteRoom: null
                    } 
                });
            }
        } else if (!this.props.room.active) {
            c.deleteRoom(this.props.room.name);
        } else if (!this.state.errors.deleteRoom) {
            _.defer(() => {
                this.setState({
                    errors: {
                        deleteRoom: e(Tooltip, {
                            location: 'above',
                            title: translate('Activity Running'),
                            text: translate('A classroom cannot be deleted while it has an activity running.')
                        })
                    }
                });
                document.addEventListener('click', this.state.bodyClicked);
            });
        } else {
            this.setState({
                errors: {
                    editName: null,
                    editRoster: null,
                    deleteRoom: null
                } 
            });
        }
    }
    
    bodyClicked() {
        document.removeEventListener('click', this.state.bodyClicked);
        
        this.setState({
            errors: {
                editName: null,
                editRoster: null,
                deleteRoom: null
            },
            showDropDown: false
        });
    }
    
    render() {
        c = this.props.controller;
        m = this.props.model;
        
        let roomsListChildren = [],
            room = this.props.room;

        if (room.notify && room.notify.status) {
            roomsListChildren.push(
                e('div', {
                    className: 'notify',
                    children: [
                        e('i', {
                            className: 'icon',
                            'data-icon': 'J'
                        }),
                        e('strong', {
                            className: 'notify-content',
                            children: room.notify.message
                        })
                    ]
                })
            );
        }
        
        roomsListChildren.push(
            e(RoomEntry, {
                roomData:            room,
                errors:              this.state.errors,
                manageRosterClicked: () => this.manageRosterClicked(),
                deleteRoom:          () => this.deleteRoom(),
                editRoomClicked:     () => this.editRoomClicked()
            })
        );

        if (!_.isEmpty(room.roster) && room.rosterInEdit) {
            roomsListChildren.push(
                e(RosterView, Object.assign({
                    roomName: room.name,
                    roster: room.roster,
                    newStudents: room.new_students_list,
                    students_logged_in: room.students_logged_in
                }, this.props))
            );
        } else if (room.new_students_list && room.new_students_list.length > 0 && room.rosterInEdit) {
            roomsListChildren.push(
                e(RosterView, Object.assign({
                    roomName: room.name,
                    newStudents: room.new_students_list,
                    message: room.message,
                    hideRosterData: true
                }, this.props))
            );
        }
        
        let roomEntryClass = 'room-entry';
        
        if (room.default) {
            roomEntryClass += ' default-room'
        }
        
        if (globalRoom.get('name').toLowerCase() === this.props.room.name.toLowerCase()) {
            roomEntryClass += ' current-room'
        }
            
        return e('li', {
            className: roomEntryClass,
            children: [
                roomsListChildren,
                e('div', {
                    className: 'add-students-container',
                    style: {display: !this.props.room.rostered && this.state.showDropDown ? 'block' : 'none'},
                    children: e(DropdownList, {
                        children: [
                            {
                                text: translate('Import'),
                                onClick: () => {
                                    c.importClicked(this.props.room.name);
                                    this.setState({
                                        showDropDown: !this.state.showDropDown
                                    });
                                }
                            },
                            {
                                text: translate('Manual (Type In)'),
                                onClick: () => {
                                    c.manualStudentsClicked();
                                    this.setState({
                                        showDropDown: !this.state.showDropDown
                                    });
                                }
                            }
                        ]
                    })
                })
            ]
        });
    }
    
}
