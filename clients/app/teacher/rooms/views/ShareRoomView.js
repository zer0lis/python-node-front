import React from 'react';
import InfoIcon from 'InfoIcon';
import OkIconCircle from 'OkIconCircle';
import GenerateCodeIcon from 'GenerateCodeIcon';
import platform from 'Platform';
import {translate} from 'translator';

let e = React.createElement;

export default class ShareRoomView extends React.Component {
    
    constructor(props) {
        super(props);
        
        this.state = {
            highlighted: false,
            newLink: false,
            selected: false
        };
    }
    
    linkCopied() {
        this.urlFocus();
        
        document.execCommand('copy');
        
        this.setState({
            selected: true,
            highlighted: true
        });
        
        this.shareInput.blur();
    }

    urlFocus() {
        this.shareInput.focus();
        this.shareInput.setSelectionRange(0, this.shareInput.value.length);
    }

    componentDidMount() {
        this.shareInput.focus();
        
        if (this.props.showInfo) {
            this.setState({
                highlighted: true
            });
            
            setTimeout(() => {
                this.setState({
                    highlighted: false
                });
            }, 800);
        }
    }

    render() {
        let copyButtonHTML = translate('Copy');
        
        if (this.state.selected) {
            copyButtonHTML = e('span', {
                children: [
                    e(OkIconCircle),
                    translate('Copied')
                ]
            })
        }
        
        return e('div', {
            className: platform.isMobile() && !platform.isTablet ? 'share-room-container mobile-form form' : 'share-room-container form',
            children: [
                e('p', {
                    className: this.state.highlighted ? 'share-room-copy' : 'share-room-copy test',
                    children: translate('Share this URL with students to give them direct access to this classroom.')
                }),
                e('p', {
                    className: 'current-room',
                    children: translate('Copy classroom URL for "{0}"').format(this.props.roomName.toUpperCase())
                }),
                e('div', {
                    className: this.state.highlighted ? 'share-room-form highlighted' : 'share-room-form',
                    children: [
                        e('div', {
                            ref: 'urlCode',
                            className: this.state.selected ? 'share-url copied' : 'share-url',
                            children: [
                                e('input', {
                                    className: this.state.selected ? 'url copied' : 'url',
                                    value: this.props.url,
                                    readOnly: true,
                                    ref: (input) => this.shareInput = input,
                                    onFocus: () => this.urlFocus()
                                }),
                                e('button', {
                                    className: this.state.selected ? 'button-secondary button-large copied' : 'button-secondary button-large',
                                    children: copyButtonHTML,
                                    onClick: () => this.linkCopied()
                                })
                            ]
                        })
                    ]
                }),
                e('div', {
                    className: 'share-room-bottom clearfix',
                    children: [
                        e('a', {
                            className: 'generate-new-button',
                            onClick: () => this.props.regenerateLink(this.props.roomName),
                            children: [
                                e(GenerateCodeIcon),
                                e('span', {
                                    children: translate('Generate New')
                                })
                            ]
                        }),
                        e('span', {
                            className: 'share-info-text align-left',
                            style: {
                                display: this.props.showInfo ? 'block' : 'none'
                            },
                            children: [
                                e(InfoIcon, {
                                    color: '#309bff'
                                }),
                                e('span', {
                                    children: translate('Previous URL is no longer active.')
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
}
