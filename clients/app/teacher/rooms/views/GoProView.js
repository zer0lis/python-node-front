import React from 'react';
import GoProIcon from 'GoProIcon';
import platform from 'Platform';
import {translate} from 'translator';

let e = React.createElement;

export default class GoProView extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            color: '#444444'
        };
    }
    
    calculateModalSize() {
        let height = Math.max(window.innerHeight || 200),
            modalWrapper = document.getElementById('modal-wrapper'),
            modalContent = document.getElementById('modal-content');

        if ((modalContent.offsetHeight + 200) > height) {
            modalWrapper.setAttribute('class', 'full-height');
            modalContent.style.height = (height - 40) + 'px';
        } else {
            modalWrapper.setAttribute('class', '');
            modalContent.style.height = '';
        }
    }
    
    componentDidMount() {
        this.calculateModalSize();
        window.addEventListener('orientationchange', this.calculateModalSize);
    }
    
    componentWillUnmount() {
        window.removeEventListener('orientationchange', this.calculateModalSize);
    }
    
    renderListItem(text) {
        return (
            e('li', {
                children: [
                    e('i', {'data-icon': 'I'}),
                    text
                ]
            })
        );
    }
    
    render() {
        let goProChildren = [
            e('div', {
                className: 'go-pro-icon-container',
                children: [
                    e('span', {
                        className: 'pro-badge-icon',
                        children: e(GoProIcon)
                    })
                ]
            }),
            e('div', {
                className: 'clearfix' + (this.props.hideGoProButton ? ' no-go-pro-button premium-popup-content' : ' premium-popup-content'),
                children: [
                    e('div', {
                        className: 'col col-left',
                        children: [
                            e('ul', {
                                children: [
                                    this.renderListItem(translate('Up to 10 private or public classrooms')),
                                    this.renderListItem(translate('150 student capacity')),
                                    this.renderListItem(translate('Spaceship Game countdown timer')),
                                    this.renderListItem(translate('Roster import via CSV or Excel')),
                                    this.renderListItem(translate('Restricted classroom access via student IDs')),
                                    this.renderListItem(translate('Personalized header for students'))
                                ]
                            })
                        ]
                    }),
                    e('div', {
                        className: 'col',
                        children: [
                            e('ul', {
                                children: [
                                    this.renderListItem(translate('Organize quizzes in folders')),
                                    this.renderListItem(translate('Silent student hand raise')),
                                    this.renderListItem(translate('Merge quizzes')),
                                    this.renderListItem(translate('Control quiz attempts')),
                                    this.renderListItem(translate('Dedicated help center & email support'))
                                ]
                            })
                        ]
                    })
                ]
            })
        ];
        
        if (!this.props.hideGoProButton) {
            goProChildren.push(
                e('div', {
                    className: platform.browser === 'IE' ? 'btn-container modal-footer ie-browser' : 'btn-container modal-footer',
                    children: e('button', {
                        className: 'premium-pill-button',
                        children: e('svg', {
                            width: '83px',
                            height: '13px',
                            viewBox: '0 0 83 13',
                            fill: 'inherit',
                            version: "1.1",
                            dangerouslySetInnerHTML: { __html: '<g stroke="none" stroke-width="1" fill-rule="evenodd"><g transform="translate(-550.000000, -716.000000)"><g><path d="M556.342,728.192 C558.39,728.192 559.974,727.392 561.126,726.416 L561.126,721.552 L556.262,721.552 L556.262,723.68 L558.742,723.68 L558.742,725.28 C558.118,725.728 557.318,725.952 556.422,725.952 C554.438,725.952 553.03,724.448 553.03,722.4 L553.03,722.368 C553.03,720.464 554.454,718.88 556.246,718.88 C557.542,718.88 558.31,719.296 559.174,720.016 L560.726,718.144 C559.558,717.152 558.342,716.608 556.326,716.608 C552.934,716.608 550.454,719.216 550.454,722.4 L550.454,722.432 C550.454,725.744 552.854,728.192 556.342,728.192 L556.342,728.192 Z M564.994,728 L573.522,728 L573.522,725.808 L567.442,725.808 L567.442,723.456 L572.722,723.456 L572.722,721.264 L567.442,721.264 L567.442,718.992 L573.442,718.992 L573.442,716.8 L564.994,716.8 L564.994,728 Z M579.822,728 L582.286,728 L582.286,719.072 L585.694,719.072 L585.694,716.8 L576.414,716.8 L576.414,719.072 L579.822,719.072 L579.822,728 Z M595.382,728 L597.846,728 L597.846,724.64 L599.718,724.64 C602.23,724.64 604.246,723.296 604.246,720.704 L604.246,720.672 C604.246,718.384 602.63,716.8 599.958,716.8 L595.382,716.8 L595.382,728 Z M597.846,722.448 L597.846,719.024 L599.75,719.024 C600.982,719.024 601.75,719.616 601.75,720.72 L601.75,720.752 C601.75,721.712 601.03,722.448 599.798,722.448 L597.846,722.448 Z M607.57,728 L610.034,728 L610.034,724.416 L611.97,724.416 L614.37,728 L617.25,728 L614.514,724 C615.938,723.472 616.914,722.336 616.914,720.528 L616.914,720.496 C616.914,719.44 616.578,718.56 615.954,717.936 C615.218,717.2 614.114,716.8 612.69,716.8 L607.57,716.8 L607.57,728 Z M610.034,722.24 L610.034,719.024 L612.482,719.024 C613.682,719.024 614.418,719.568 614.418,720.624 L614.418,720.656 C614.418,721.6 613.73,722.24 612.53,722.24 L610.034,722.24 Z M626.078,728.192 C629.534,728.192 632.046,725.584 632.046,722.4 L632.046,722.368 C632.046,719.184 629.566,716.608 626.11,716.608 C622.654,716.608 620.142,719.216 620.142,722.4 L620.142,722.432 C620.142,725.616 622.622,728.192 626.078,728.192 L626.078,728.192 Z M626.11,725.92 C624.126,725.92 622.718,724.32 622.718,722.4 L622.718,722.368 C622.718,720.448 624.094,718.88 626.078,718.88 C628.062,718.88 629.47,720.48 629.47,722.4 L629.47,722.432 C629.47,724.352 628.094,725.92 626.11,725.92 L626.11,725.92 Z"/></g></g></g>'}
                        }),
                        onClick: () => this.props.goProButtonCliked()
                    })
                })
            );
        }
        
        return e('div', {
            className: 'migrate-to-premium' + (this.props.hideGoProButton ? ' no-go-pro-button' : ''),
            children: goProChildren
        })
    }
    
}
