import React  from 'react';
import CloseX from 'CloseX';
import Input  from 'Input';
import Constants from 'Constants';
import {translate} from 'translator';

let e = React.createElement;

export default class AddRoomView extends React.Component {
    
    componentDidMount() {
        let nameInput = document.getElementById('roomName');
        
        if (nameInput) {
            nameInput.focus();
        }
    }
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        return e('div', {
            id: 'add-room-modal-background',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'add-room-modal-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('span', {
                                    className: 'popup-title',
                                    children: translate('Add Classroom')
                                }),
                                e('div', {
                                    className: 'cancel-button',
                                    children: [
                                        e(CloseX, {
                                            className: 'cancel-button-x',
                                            color: '#cccccc'
                                        })
                                    ],
                                    onClick: c.cancelClicked
                                })
                            ]
                        }),
                        e('div', {
                            id: 'add-room-modal-content',
                            className: 'form',
                            children: [
                                e(Input, {
                                    id: 'roomName',
                                    label: translate('Classroom Name'),
                                    maxLength: Constants.MAX_ROOM_NAME_LENGTH,
                                    status: m.nameStatus,
                                    tooltipLocation: 'above',
                                    value: m.name,
                                    onChange: (event) => c.nameChanged(event.target.value),
                                    onKeyPress: (event) => {
                                        if (event.key === 'Enter') {
                                            c.addClicked();
                                        }
                                    }
                                }),
                                e('div', {
                                    className: 'add-room-buttons-container',
                                    children: [
                                        e('button', {
                                            id: 'addRoomButton',
                                            className: 'add-room-button',
                                            children: translate('ADD'),
                                            onClick: c.addClicked
                                        }),
                                        e('button', {
                                            className: 'add-room-button add-room-cancel-button',
                                            children: translate('CANCEL'),
                                            onClick: c.cancelClicked
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
