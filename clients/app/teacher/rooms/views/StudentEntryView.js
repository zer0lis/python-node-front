import React from 'react';
import _ from 'underscore';
import DropdownList from 'DropdownList';
import Input from 'Input';
import RemoveIcon from 'RemoveIcon';
import {translate} from 'translator';

let e = React.createElement,
    c = null,
    m = null;

export default class StudentEntryView extends React.Component {
    
    constructor(props) {
        super(props);
        
        let student = this.props.studentData;
        
        this.state = {
            bodyClicked: this.bodyClicked.bind(this), // Make it possible to remove the bodyClicked listener.
            editMode: student.inEdit || false,
            errors: student.errors || {},
            indexPos: student.indexPos || 'noIndex',
            moreButtonClicked: false,
            studentDBID: student.id || '',
            studentID: student.student_id || '',
            studentFirstName: student.first_name || '',
            studentLastName: student.last_name || ''
        };
    }
    
    componentWillReceiveProps(newProps) {
        let studentData = newProps.studentData || {};
        
        this.setState({
            studentID: studentData.student_id || '',
            studentDBID: studentData.id || '',
            studentFirstName: studentData.first_name || '',
            studentLastName: studentData.last_name || '',
            editMode: studentData.inEdit || false,
            indexPos: studentData.indexPos || 'noIndex',
            errors: studentData.errors || {}
        });
    }
    
    // Validate on blur and send the value of the field. If the student is new, send
    // the position of the edited student in the new student's list (1, 5, 10 chunks).
    
    firstNameBlur() {
        if (this.state.studentFirstName !== this.props.studentData.first_name) {
            let student = c.validateStudent('first_name', this.state.studentFirstName, this.state.indexPos, this.state.studentDBID);
            this.setState({
                errors: student.errors
            });
        }
    }
    
    lastNameBlur() {
        if (this.state.studentLastName !== this.props.studentData.last_name) {
            let student = c.validateStudent('last_name', this.state.studentLastName, this.state.indexPos, this.state.studentDBID);
            this.setState({
                errors: student.errors
            });
        }
    }
    
    studentIdBlur() {
        if (this.state.studentID !== this.props.studentData.student_id) {
            let student = c.validateStudent('student_id', this.state.studentID, this.state.indexPos, this.state.studentDBID);
            this.setState({
                errors: student.errors
            });
        }
    }
    
    firstNameChanged(event) {
        if (this.props.studentData.errors) {
            this.props.studentData.errors.first_name = null;
        }
        
        this.setState({
            studentFirstName: event.target.value
        });
    }
    
    lastNameChanged(event) {
        if (this.props.studentData.errors) {
            this.props.studentData.errors.last_name = null;
        }
        
        this.setState({
            studentLastName: event.target.value
        });
    }
    
    studentIdChanged(event) {
        if (this.props.studentData.errors) {
            this.props.studentData.errors.student_id = null;
        }
        
        this.setState({
            studentID: event.target.value.toUpperCase()
        });
    }
    
    bodyClicked() {
        document.body.removeEventListener('click', this.state.bodyClicked);
        
        _.defer(() => {
            this.setState({
                moreButtonClicked: false
            });
        });
    }
    
    render() {
        c = this.props.controller;
        m = this.props.model;
        
        let studentData = this.props.studentData || {},
            studentBullet = null,
            studentActionButtons = null;
        
        if (studentData.newStudent && !this.props.singular) {
            // If only one student is in the popup, don't show the index number and remove the student button.
            if (!this.props.disableListIndex) {
                studentBullet = e('span', {
                    className: 'student-index',
                    children: studentData.indexPos
                });
                
                studentActionButtons = e('span', {
                    className: 'align-right remove-new-student-container',
                    children: e(RemoveIcon, {
                        className: 'align-right remove-new-student'
                    }),
                    onClick: () => this.props.deleteNewStudent(this.props.studentData.indexPos)
                });
            }

        } else {
            if (this.state.editMode) {
                studentActionButtons = e('div', {
                    className: 'align-right edit-students-btns',
                    children: [
                        e('button', {
                            className: 'button button-secondary button-large',
                            children: translate('Save'),
                            onClick: () => {
                                // TODO .... Passing different argument types based on context is garbage:
                                if (this.props.singular) {
                                    c.updateStudent(null, true);
                                } else {
                                    c.updateStudent(this.props.roomName, this.state.studentDBID);
                                }
                            }
                        }),
                        e('button', {
                            onClick: () => c.cancelEditStudentButtonClicked(this.props.studentData.id, this.props.singular),
                            className: 'button button-secondary-common button-large',
                            children: translate('Cancel')
                        })
                    ]
                });
            } else {
                studentBullet = e('span', {
                    className: studentData && studentData.logged_in ? 'presence present' : 'presence absent',
                    children: studentData && studentData.logged_in ? 
                        e('i', {
                            className: 'student-presence',
                            'data-icon': 'J'
                        }):
                        e('svg', {
                            fill: '#cccccc',
                            dangerouslySetInnerHTML: {__html: '<path class="st0" d="M12,0C5.4,0,0,5.4,0,12c0,6.6,5.4,12,12,12c6.6,0,12-5.4,12-12C24,5.4,18.6,0,12,0z M21.9,12 c0,2.3-0.8,4.5-2.2,6.2L6.2,3.9C7.9,2.8,9.9,2.1,12,2.1C17.5,2.1,21.9,6.5,21.9,12z M2.1,12c0-2.6,1-4.9,2.6-6.7l13.6,14.3 c-1.7,1.4-3.9,2.3-6.3,2.3C6.5,21.9,2.1,17.5,2.1,12z"/>'}
                        })
                });
                
                studentActionButtons = e('div', {
                    className: 'right-buttons',
                    children: [
                        e('span', {
                            className: 'delete-student-container',
                            onClick: () => c.deleteStudent(this.props.studentData, this.props.roomName),
                            children: e('i', {
                                className: 'student-action',
                                'data-icon': 'L'
                            })
                        }),
                        e('span', {
                            className: 'edit-student-container',
                            onClick: (event) => c.editStudentButtonClicked(event, this.state.studentID),
                            children: e('i', {
                                className: 'student-action',
                                'data-icon': 'C'
                            })
                        }),
                        e('span', {
                            className: 'more-actions-button',
                            children: [
                                e('div', {
                                    className: 'more-actions-button-icon',
                                    dangerouslySetInnerHTML: {__html: '<svg width="24px" height="8px" viewBox="0 0 24 8" version="1.1"><defs><path d="M3.42857143,15.4285714 C5.32211914,15.4285714 6.85714286,13.8935477 6.85714286,12 C6.85714286,10.1064523 5.32211914,8.57142857 3.42857143,8.57142857 C1.53502371,8.57142857 0,10.1064523 0,12 C0,13.8935477 1.53502371,15.4285714 3.42857143,15.4285714 Z" id="path-1"/><mask id="mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="6.85714286" height="6.85714286" fill="white"><use xlink:href="#path-1"/></mask><path d="M12,15.4285714 C13.8935477,15.4285714 15.4285714,13.8935477 15.4285714,12 C15.4285714,10.1064523 13.8935477,8.57142857 12,8.57142857 C10.1064523,8.57142857 8.57142857,10.1064523 8.57142857,12 C8.57142857,13.8935477 10.1064523,15.4285714 12,15.4285714 Z" id="path-3"/><mask id="mask-4" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="6.85714286" height="6.85714286" fill="white"><use xlink:href="#path-3"/></mask><path d="M20.5714286,15.4285714 C22.4649763,15.4285714 24,13.8935477 24,12 C24,10.1064523 22.4649763,8.57142857 20.5714286,8.57142857 C18.6778809,8.57142857 17.1428571,10.1064523 17.1428571,12 C17.1428571,13.8935477 18.6778809,15.4285714 20.5714286,15.4285714 Z" id="path-5"/><mask id="mask-6" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="6.85714286" height="6.85714286" fill="white"><use xlink:href="#path-5"/></mask></defs><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-268.000000, -334.000000)"><g transform="translate(17.000000, 313.000000)"><g><g transform="translate(251.000000, 13.000000)"><rect x="0" y="0" width="24" height="24"/><use stroke="#8CB4D2" mask="url(#mask-2)" stroke-width="4" xlink:href="#path-1"/><use stroke="#8CB4D2" mask="url(#mask-4)" stroke-width="4" xlink:href="#path-3"/><use stroke="#8CB4D2" mask="url(#mask-6)" stroke-width="4" xlink:href="#path-5"/></g></g></g></g></g></svg>'}
                                }),
                                e('span', {
                                    className: 'more-actions-button-text',
                                    children: translate('More')
                                }),
                                this.state.moreButtonClicked ? e('div', {
                                    className: 'student-more-actions-container',
                                    children: [
                                        e('i', {
                                            'data-icon': 'f',
                                            className: 'more-actions-container-icon'
                                        }),
                                        e(DropdownList, {
                                            children: [
                                                {
                                                    text: [
                                                        e('i', {
                                                            'data-icon': 'C',
                                                            className: 'student-action'
                                                        }), 
                                                        translate('EDIT')
                                                    ],
                                                    onClick: (event) => c.editStudentButtonClicked(event, this.state.studentID)
                                                },
                                                {
                                                    text: [
                                                        e('i', {
                                                            'data-icon': 'L',
                                                            className: 'delete-room action-icon-button'
                                                        }), 
                                                        translate('DELETE')
                                                    ],
                                                    onClick: () => c.deleteStudent(this.props.studentData, this.props.roomName)
                                                }
                                            ]
                                        })
                                    ]
                                }): null,
                            ],
                            onClick: () => {
                                if (this.state.moreButtonClicked === false) {
                                    _.defer(() => {
                                        this.setState({
                                            moreButtonClicked: !this.state.moreButtonClicked
                                        });
                                    });
                                }
                                
                                document.body.addEventListener('click', this.state.bodyClicked);
                            }
                        })
                    ]
                });
            }
        }
        
        let errors = this.state.errors;
        
        // The error might simply be a boolean value.
        let lastNameError  = errors && errors.last_name,
            firstNameError = errors && errors.first_name,
            studentIdError = errors && errors.student_id;
        
        // If there's an error, see if there's a message too.
        if (lastNameError && errors.last_name.errorMsg) {
            lastNameError = errors.last_name.errorMsg;
        }
        
        if (firstNameError && errors.first_name.errorMsg) {
            firstNameError = errors.first_name.errorMsg
        }
        
        if (studentIdError && errors.student_id.errorMsg) {
            studentIdError = errors.student_id.errorMsg;
        }
        
        return e('div', {
            className: 'add-student clearfix',
            children: [
                e('div', {
                    className: 'add-student-form clearfix' + ( !this.state.editMode ? ' list-view' : ' edit-view' ),
                    children: [
                        studentBullet,
                        e('span', {
                            className: 'student-data-wrapper',
                            children: [
                                e(Input, {
                                    placeholder: translate('Last Name'),
                                    className: 'last-name',
                                    value: this.state.editMode ? this.state.studentLastName : this.state.studentLastName + ', ' + this.state.studentFirstName,
                                    onChange: (event) => this.lastNameChanged(event),
                                    onBlur: () => this.lastNameBlur(),
                                    error: lastNameError,
                                    disabled: !this.state.editMode,
                                    hideTooltip: this.props.studentData && this.props.studentData.hideTooltip,
                                    tooltipLocation: 'below'
                                }),
                                this.state.editMode ? e(Input, {
                                    placeholder: translate('First Name'),
                                    value: this.state.studentFirstName,
                                    onChange: (event) => this.firstNameChanged(event),
                                    onBlur: () => this.firstNameBlur(),
                                    error: firstNameError,
                                    disabled: !this.state.editMode,
                                    hideTooltip: this.props.studentData && this.props.studentData.hideTooltip,
                                    tooltipLocation: 'below'
                                }) : null,
                                e(Input, {
                                    placeholder: translate('Student ID'),
                                    className: 'student-id',
                                    value: this.state.studentID,
                                    onChange: (event) => this.studentIdChanged(event),
                                    onBlur: () => this.studentIdBlur(),
                                    error: studentIdError,
                                    disabled: !this.state.editMode,
                                    hideTooltip: this.props.studentData && this.props.studentData.hideTooltip,
                                    tooltipLocation: 'below'
                                })
                            ]
                        }),
                        studentActionButtons
                    ]
                })
            ]
        });
    }
}
