import React from 'react';
import _ from 'underscore';
import StudentEntryView from 'StudentEntryView';
import AddIcon from 'AddIcon';
import platform from 'Platform';
import {translate} from 'translator';

let e = React.createElement,
    c = null,
    m = null;

export default class AddNewStudentsView extends React.Component {
    
    constructor(props) {
        super(props);
        
        this.state = {
            errorNotify: false,
            modalClass: '',
            maxModalHeight: 'auto',
            newStudents: this.props.model.newStudents
        };
    }
    
    calculateModalSize() {
        let w = Math.max(window.innerWidth || 200);
        let h = Math.max(window.innerHeight || 200);
        let modalWrapper = document.getElementById('modal-wrapper');
        let modalContent = document.getElementById("modal-content");
        let entry = 60;

        if (w <= 768) {
            entry = 195;
        }

        if ((this.state.newStudents.length * entry + 300) > h) {
            modalWrapper.setAttribute('class', 'full-height');
            modalContent.style.height = (h - 40) + 'px';
        } else {
            modalWrapper.setAttribute('class', '');
            modalContent.style.height = '';
        }     
    }
    
    componentDidMount() {
        this.calculateModalSize();
        window.addEventListener('orientationchange', this.calculateModalSize);
    }
    
    componentWillUnmount() {
        window.removeEventListener('orientationchange', this.calculateModalSize);
    }
    
    componentDidUpdate() {
        this.calculateModalSize();
    }
    
    render() {
        c = this.props.controller;
        m = this.props.model;
        
        let newStudentsChildren = [];
        
        if (this.state.newStudents.length >  0) {
            for (let student of this.state.newStudents) {
                let newStudent = e(StudentEntryView, Object.assign({
                    disableListIndex: this.state.newStudents.length === 1,
                    studentData: student,
                    deleteNewStudent: (index) => {
                        let newStudents = c.removeNewStudent(index);
                        this.setState({
                            newStudents: newStudents
                        });
                    }
                }, this.props));
                
                newStudentsChildren.push(newStudent);
            }
        }
        
        return e('div', {
            className: 'add-students-list-container',
            children: [
                e('div', {
                    className: 'modal-errors-container',
                    style: {
                        display: this.state.errorNotify ? 'block' : 'none'
                    },
                    children: [
                        e('i', {
                            'data-icon': '&'
                        }),
                        e('strong', {
                            children: translate('Missing Student Information')
                        })
                    ]
                }),
                e('div', {
                    className: this.state.newStudents.length === 1 ? 'students-header just-one' : 'students-header',
                    children: [
                        e('span', {
                            children: translate('Last Name')
                        }),
                        e('span', {
                            children: translate('First Name')
                        }),
                        e('span', {
                            children: translate('Student ID')
                        }),
                        e('span', {
                            className: 'remove-header',
                            children: translate('Remove'),
                            style: {display: this.state.newStudents.length === 1 ? 'none' : 'block'}
                        })
                    ]
                }),
                e('div', {
                    className: 'form',
                    children: newStudentsChildren
                }),
                e('div', {
                    className: platform.browser === 'IE' ? 'add-students-buttons clearfix modal-footer ie-browser' : 'add-students-buttons clearfix modal-footer',
                    children: [
                        e('a', {
                            style: {display: platform.isMobile() && !platform.isTablet ? 'none' : 'block'},
                            className: 'align-left add-another-student',
                            disabled: this.state.newStudents.length === m.maxStudents,
                            children: [
                                e(AddIcon),
                                e('span', {
                                    children: translate('Add Another')
                                })
                            ],
                            onClick: () => {
                                let newStudents = c.addOneNewStudent();
                                this.setState({
                                    newStudents: newStudents
                                });
                            },
                        }),
                        e('button', {
                            className: 'button button-secondary-common button-large align-right',
                            onClick: c.closePopup,
                            children: translate('Cancel')
                        }),
                        e('button', {
                            className: 'button button-secondary button-large align-right',
                            children: translate('Save'),
                            onClick: () => {
                                let newStudents = c.saveNewStudents();
                                
                                let activeErrors = _.filter(newStudents, (student) => {
                                    return !_.isEmpty(student.errors)
                                });
                        
                                if (activeErrors.length > 0) {
                                    this.setState({
                                        errorNotify: true,
                                        newStudents: newStudents
                                    });
                        
                                    setTimeout(() => {
                                        this.setState({
                                            errorNotify: false // Remove the top banner after 2 seconds.
                                        });
                                    }, 2000);
                                }
                            }
                        })
                    ]
                })
            ]
        });
    }
    
}
