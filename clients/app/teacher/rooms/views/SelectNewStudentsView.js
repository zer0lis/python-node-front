import React from 'react';
import Loader from 'Loader';
import ImportRosterMessage from 'ImportRosterMessage';
import Input from 'Input';
import {translate} from 'translator';

let e = React.createElement;

export default class SelectNewStudentsView extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            newStudents: ''
        };
    }
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        let isInRange = 0 < Number(this.state.newStudents) && Number(this.state.newStudents) <= m.maxStudents,
            message = null;
        
        if (this.props.loader) {
            return e('div', {
                className: 'add-students-container',
                children: e(Loader)
            });
        }

        if (this.props.studentsInRoom) {
            message = e(ImportRosterMessage, {
                name: this.props.name
            });
        }

        return e('div', {
                className: 'add-students-container form',
                children: [
                    message,
                    e('p', {
                        style: {
                            'margin-top': this.props.studentsInRoom ? '20px' : '0'
                        },
                        children: translate('How many students would you like to add?')
                    }),
                    e('div', {
                        className: 'add-students-form',
                        children: [
                            e(Input, {
                                autoFocus: true,
                                type: 'tel',
                                value: this.state.newStudents,
                                onChange: (event) => {
                                    let value = event.target.value;
                            
                                    if (Number(value) > m.maxStudents) {
                                        value = m.maxStudents;
                                    }
                            
                                    this.setState({
                                        newStudents: value
                                    });
                                }
                            }),
                            e('div', {
                                class: 'button-container',
                                children: [
                                    e('button', {
                                        className: 'button-secondary button-large button',
                                        children: [
                                            e('span', {
                                                children: translate('Create Form')
                                            })
                                        ],
                                        disabled: !isInRange,
                                        type: 'button',
                                        onClick: () => c.selectedNewClicked(this.state.newStudents)
                                    })
                                ]
                            })
                        ]
                    }),
                    e('span', {
                        className: 'max-students-info',
                        children: translate('Maximum of {0} Students').format(m.maxStudents)
                    })
                ]
            })
    }
    
}
