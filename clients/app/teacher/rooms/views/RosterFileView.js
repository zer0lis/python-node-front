import React     from 'react';
import CloseX    from 'CloseX';
import Loader    from 'Loader';
import InfoIcon  from 'InfoIcon'
import Select    from 'Select';
import Constants from 'Constants';
import Input     from 'Input';
import AlertIcon from 'AlertIcon';
import {translate} from 'translator';

let e = React.createElement,
    c = null,
    m = null,
    t = null;

export default class RosterFileView extends React.Component {
    
    renderSettings() {
        let columnOrder = translate('Last Name, First Name, Student ID'),
            fileType = 'CSV',
            tableClass = 'roster-file-settings-table';
        
        if (m.importing) {
            if (m.importOrder === Constants.FIRST_LAST_ID) {
                columnOrder = translate('First Name, Last Name, Student ID');
            }
            
            if (m.importType === Constants.XLSX) {
                fileType = 'XLSX';
            }
        } else {
            if (m.exportOrder === Constants.FIRST_LAST_ID) {
                columnOrder = translate('First Name, Last Name, Student ID');
            }
            
            if (m.exportType === Constants.XLSX) {
                fileType = 'XLSX';
            }
            
            if (m.showExportSummary) {
                tableClass = 'roster-file-settings-table-export-summary';
            }
        }
        
        return (
            e('table', {
                className: tableClass,
                children: [
                    e('tbody', {
                        children: [
                            e('tr', {
                                children: [
                                    e('td', {
                                        className: 'roster-file-settings-descriptor',
                                        children: translate('Column Order') + ':'
                                    }),
                                    e('td', {
                                        className: 'roster-file-settings-text',
                                        children: columnOrder
                                    })
                                ]
                            }),
                            e('tr', {
                                children: [
                                    e('td', {
                                        className: 'roster-file-settings-descriptor',
                                        children: translate('File Type') + ':'
                                    }),
                                    e('td', {
                                        className: 'roster-file-settings-text',
                                        children: fileType
                                    })
                                ]
                            })
                        ]
                    })
                ]
            })
        );
    }
    
    render() {
        c = this.props.controller;
        m = this.props.model;
        t = this.props.teacher;
        
        let modalContentChildren = [];
        
        if (!t.rosterSettingsFetched) {
            modalContentChildren.push(
                e(Loader, {
                    className: 'roster-file-loader',
                    text: translate('Loading...')
                })
            );
        } else {
            modalContentChildren.push(
                e('div', {
                    className: 'roster-file-subtitle-container' + (m.uploading ? ' roster-file-subtitle-container-uploading' : ''),
                    children: [
                        e('span', {
                            className: 'roster-file-subtitle-text',
                            children: translate('SETTINGS')
                        }),
                        e('span', {
                            className: 'roster-file-change-text link' + (m.uploadErrors.length > 0 || (!m.importing && m.showExportSummary) ? '' : ' hidden'),
                            children: translate('Change'),
                            onClick: c.changeClicked
                        })
                    ]
                })
            );
            
            if (m.uploadErrors.length > 0) {
                modalContentChildren.push(
                    this.renderSettings(),
                    e('div', {
                        className: 'roster-error-header',
                        children: [
                            e(AlertIcon, {
                                className: 'roster-error-icon'
                            }),
                            e('span', {
                                className: 'roster-error-header-text',
                                children: translate('Roster Error')
                            })
                        ]
                    }),
                    e('span', {
                        className: 'roster-error-list-header',
                        children: translate('Correct the following error(s) and import again:')
                    })
                );
                
                modalContentChildren.push(
                    e('ul', {
                        className: 'roster-error-list',
                        children: m.uploadErrors.map((errorString) => {
                            return e('li', {
                                className: 'roster-error-item',
                                children: errorString
                            });
                        })
                    })
                );
            } else if (m.uploading) {
                modalContentChildren.push(
                    this.renderSettings(),
                    e('span', {
                        className: 'roster-filename',
                        children: m.fileName
                    }),
                    e('div', {
                        className: 'progress-bar',
                        children: [
                            e('div', {
                                className: 'progress',
                                style: {width: m.progress + '%'}
                            })
                        ]
                    }),
                    e('span', {
                        className: 'progress-percent',
                        children: `${m.progress}%`
                    })
                )
            } else {
                if (!m.importing && m.showExportSummary) {
                    modalContentChildren.push(
                        this.renderSettings()
                    );
                } else {
                    modalContentChildren.push(
                        e('div', {
                            className: 'roster-file-select-container',
                            children: [
                                e('p', {
                                    className: 'roster-file-settings-label',
                                    children: translate('Column Order')
                                }),
                                e(Select, {
                                    children: [
                                        {
                                            title: translate('Last Name, First Name, Student ID'),
                                            value: Constants.LAST_FIRST_ID,
                                            selected: m.importing ? m.importOrder === Constants.LAST_FIRST_ID : m.exportOrder === Constants.LAST_FIRST_ID
                                        },
                                        {
                                            title: translate('First Name, Last Name, Student ID'),
                                            value: Constants.FIRST_LAST_ID,
                                            selected: m.importing ? m.importOrder === Constants.FIRST_LAST_ID : m.exportOrder === Constants.FIRST_LAST_ID
                                        }
                                    ],
                                    onSelect: (name, value) => c.columnOrderChanged(value)
                                })
                            ]
                        }),
                        e('div', {
                            className: 'roster-file-select-container' + (m.importing ? ' roster-file-type-select-container' : ''),
                            children: [
                                e('p', {
                                    className: 'roster-file-settings-label',
                                    children: translate('File Type')
                                }),
                                e(Select, {
                                    children: [
                                        {
                                            title: 'CSV',
                                            value: Constants.CSV,
                                            selected: m.importing ? m.importType === Constants.CSV : m.exportType === Constants.CSV
                                        },
                                        {
                                            title: 'XLSX',
                                            value: Constants.XLSX,
                                            selected: m.importing ? m.importType === Constants.XLSX : m.exportType === Constants.XLSX
                                        }
                                    ],
                                    onSelect: (name, value) => c.fileTypeChanged(value)
                                })
                            ]
                        })
                    );

                    if (m.importing) {
                        modalContentChildren.push(
                            e('a', {
                                className: 'link roster-file-template-link',
                                href: `${window.static_url}data/roster_template.${m.importType === Constants.CSV ? 'csv' : 'xlsx'}`,
                                children: translate('Download Template')
                            })
                        );
                    }

                    modalContentChildren.push(
                        e('div', {
                            id: 'includes-headers-container',
                            className: 'includes-headers-container',
                            children: [
                                e('input', {
                                    className: 'vertical-align-middle',
                                    type: 'checkbox',
                                    id: 'includes-headers',
                                    checked: m.importing ? m.importHeader : m.exportHeader,
                                    onChange: c.includeHeaderChanged
                                }),
                                e('label', {
                                    className: 'vertical-align-middle includes-headers-label',
                                    htmlFor: 'includes-headers',
                                    children: translate('Includes Column Headers')
                                })
                            ]
                        })
                    );
                }
            }
        }
        
        let footerButtons = [
            e('button', {
                className: 'roster-file-footer-button roster-file-cancel-button',
                children: translate('CANCEL'),
                onClick: c.cancelClicked
            })
        ];
        
        if (m.importing) {
            footerButtons.push(
                e(Input, {
                    accept: m.importType === Constants.CSV ? '.csv' : '.xls, .xlsx',
                    disabled: m.uploading,
                    id: 'roster-upload',
                    label: translate('CHOOSE FILE'),
                    type: 'file',
                    refValue: (input) => {this.fileInput = input},
                    onChange: (event) => {
                        c.fileChosen(event.target.files[0]);
                        this.fileInput.value = null; // Clear the value so onChange() will fire if the same file is selected again.
                    }
                })
            );
        } else {
            footerButtons.push(
                e('button', {
                    id: 'roster-file-export-button',
                    className: 'roster-file-footer-button roster-file-export-button',
                    children: translate('EXPORT'),
                    onClick: c.exportClicked
                })
            );
        }
        
        return e('div', {
            id: 'roster-file-modal-background',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'roster-file-modal-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('span', {
                                    className: 'popup-title',
                                    children: m.importing ? translate('Import Roster') : translate('Export Roster')
                                }),
                                e('div', {
                                    className: 'cancel-button',
                                    children: [
                                        e(CloseX, {
                                            className: 'cancel-button-x',
                                            color: '#cccccc'
                                        })
                                    ],
                                    onClick: c.cancelClicked
                                })
                            ]
                        }),
                        e('div', {
                            className: 'roster-file-import-banner' + (!t.rosterSettingsFetched || m.uploading || !m.importing || m.uploadErrors.length > 0 ? ' hidden' : ''),
                            children: [
                                e(InfoIcon, {
                                    className: 'roster-file-info-icon'
                                }),
                                e('span', {
                                    className: 'roster-file-import-message',
                                    children: translate('Importing a roster will clear all students from the {0} classroom.').format(m.roomName.toUpperCase())
                                })
                            ]
                        }),
                        e('div', {
                            id: 'roster-file-modal-content',
                            className: 'form',
                            children: modalContentChildren
                        }),
                        e('div', {
                            id: 'roster-file-footer',
                            className: 'roster-file-footer form' + (!t.rosterSettingsFetched || m.uploading ? ' hidden' : ''),
                            children: footerButtons
                        })
                    ]
                })
            ]
        });
    }
    
}
