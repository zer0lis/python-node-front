import React from 'react';
import StudentEntryView from 'StudentEntryView';
import ArrowDown from 'ArrowDown';
import ImportRosterMessage from 'ImportRosterMessage';
import MenuButton from 'MenuButton';
import platform from 'Platform';
import {translate} from 'translator';

const MAX_STUDENTS_ON_PAGE = 30;

let e = React.createElement,
    c = null,
    m = null;

class StudentsInRosterList extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            studentsList: this.props.studentsList
        };
    }
    
    componentWillReceiveProps(newProps) {
        this.setState({
            studentsList: newProps.studentsList
        });
    }
    
    render() {
        let rosterChildren = [];
        
        if (this.state.studentsList) {
            for (let student of this.state.studentsList) {
                rosterChildren.push(
                    e('li', {
                        children: [
                            e(StudentEntryView, Object.assign({
                                studentData: student,
                                editMode: student.inEdit,
                                newStudent: false,
                                roomName: this.props.roomName
                            }, this.props))
                        ]
                    })
                )
            }
        }
        
        let presenceFilter = m.studentFilter === 'presenceASC' || m.studentFilter === 'presenceDSC',
            nameFilter = m.studentFilter === 'nameASC' || m.studentFilter === 'nameDSC',
            studentIdFilter = m.studentFilter === 'student_idASC' || m.studentFilter === 'student_idDSC';
        
        return e('div', {
            style: {display: this.props.hideRosterData ? 'none' : 'block'},
            className: 'roster-wrapper',
            children: [
                e('div', {
                    className: 'roster-header clearfix',
                    children: [
                        e('span', {
                            className: 'roster-filter col in-room' + (presenceFilter ? ' active' : ''),
                            children: [
                                e('span', {
                                    children: translate('HERE')
                                }),
                                e('span', {
                                    className: 'logged-in-students',
                                    children: this.props.students_logged_in || 0
                                }),
                                e('i', {
                                    'data-icon': m.studentFilter === 'presenceASC' ? 'k' : 'm'
                                })
                            ],
                            onClick: () => c.filterStudents('presence')
                        }),
                        e('span', {
                            className: 'roster-filter col student-name' + (nameFilter ? ' active' : ''),
                            children: [
                                e('span', {
                                    children: translate('Student Name')
                                }),
                                e('i', {
                                    'data-icon': m.studentFilter === 'nameASC' ? 'k' : 'm'
                                })
                            ],
                            onClick: () => c.filterStudents('name')
                        }),
                        e('span', {
                            className: 'roster-filter col student-id' + (studentIdFilter ? ' active' : ''),
                            children: [
                                e('span', {
                                    children: translate('Student ID')
                                }),
                                e('i', {
                                    'data-icon': m.studentFilter === 'student_idASC' ? 'k' : 'm'
                                })
                            ],
                            onClick: () => c.filterStudents('student_id')
                        }),
                        e('span', {
                            className: 'align-right student-btn-container',
                            children: [
                                e('span', {
                                    className: 'edit-button',
                                    children: translate('Edit')
                                }),
                                e('span', {
                                    className: 'delete-button',
                                    children: translate('Delete')
                                })
                            ]
                        })
                    ]
                }),
                e('ul', {
                    className: 'roster-list',
                    children: rosterChildren
                })
            ]
        });
    }
    
}

class RosterActionButtons extends React.Component {

    downloadRoster(type) {
        let hiddenIFrameID = 'reportDownloader',
            iframe = document.getElementById(hiddenIFrameID);
        
        if (!iframe) {
            iframe = document.createElement('iframe');
            iframe.id = hiddenIFrameID;
            iframe.style.display = 'none';
            document.body.appendChild(iframe);
        }
        
        iframe.src = window.backend_host + '/rooms/api/students/export/' + this.props.roomName + '?type=' + type;
    }
    
    render() {
        let numberOfPages = Math.floor(this.props.numberOfStudents / MAX_STUDENTS_ON_PAGE) + 1;

        // If the total number of students is evenly divisible by the students on the page, we don't need the last page.
        if (this.props.numberOfStudents % MAX_STUDENTS_ON_PAGE === 0) {
            numberOfPages--;
        }
        
        let firstStudentOrder = ((this.props.currentPage - 1) * MAX_STUDENTS_ON_PAGE + 1),
            lastStudentOrder = Math.min(firstStudentOrder + 29, this.props.numberOfStudents),
            pagination = [];
        
        if (numberOfPages > 1) {
            for (let i = 1; i < numberOfPages + 1; i++) {
                let pageNumber = e('span', {
                    className: 'pagination-number' + (i === this.props.currentPage ? ' active' : ''),
                    'data-page': i,
                    children: i,
                    onClick: (event) => this.props.changePage(Number(event.target.dataset.page))
                });
                
                pagination.push(pageNumber);
            }
        }
        
        let addStudentButton = e(MenuButton, {
            buttonClass: 'button-secondary button-large padding-right-30 button',
            className: 'align-right button-container',
            disabled: this.props.numberOfStudents === 150,
            icon: e(ArrowDown, {
                      className: 'add-student-icon'
                  }),
            label: translate('Add Students'),
            menuItems: [
                {
                    keyValue: 'add-one-student-item',
                    label: translate('Add One Student'),
                    onClick: c.addOneStudentClicked
                },
                {
                    keyValue: 'add-multiple-students-item',
                    label: translate('Add Multiple Students'),
                    onClick: c.manualStudentsClicked
                }
            ]
        });
        
        if (platform.isMobile() && !platform.isTablet) {
            addStudentButton = e('button', {
                className: 'align-right button-secondary button-large button',
                onClick: c.addOneStudentClicked,
                children: [
                    translate('Add')
                ]
            });
        }
        
        return e('div', {
            className: 'manage-roster-buttons clearfix',
            children: [
                e('div', {
                    className: 'roster-action-buttons',
                    style: {display: this.props.showMainButtons === false || this.props.hideRosterData ? 'none' : 'inline-block'},
                    children: [
                        e('div', {
                            className: 'delete-list-btn button-container',
                            children: [
                                e('button', {
                                    className: 'button button-secondary-common button-large',
                                    children: [
                                        e('span', {
                                            children: translate('Delete')
                                        })
                                    ],
                                    type: 'button',
                                    onClick: c.deleteRoster
                                })
                            ]
                        }),
                        e('div', {
                            className: 'download-list-btn button-container',
                            children: [
                                e('button', {
                                    className: 'button button-secondary-common button-large',
                                    children: [
                                        e('span', {
                                            children: translate('Export')
                                        })
                                    ],
                                    type: 'button',
                                    onClick: () => c.exportClicked(this.props.roomName)
                                })
                            ]
                        }),
                        addStudentButton
                    ]
                }),
                e('div', {
                    className: 'pagination-container',
                    style: {display: numberOfPages > 1 ? 'inline-block' : 'none'},
                    children: [
                        e('button', {
                            className: 'button button-secondary-common previous-page',
                            disabled: this.props.currentPage === 1,
                            onClick: () => this.props.changePage(this.props.currentPage - 1),
                            children: e('i', {'data-icon': 'A'})
                        }),
                        pagination,
                        e('button', {
                            className: 'button button-secondary-common next-page',
                            disabled: this.props.currentPage === numberOfPages,
                            onClick: () => this.props.changePage(this.props.currentPage + 1),
                            children: e('i', {'data-icon': 'a'})
                        }),
                        e('span', {
                            className: 'students-on-page',
                            children: `(${firstStudentOrder}/${lastStudentOrder})`
                        })
                    ]
                })
            ]
        });
    }
    
}

export default class RosterView extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            page: 1
        };
    }
    
    cancelAddingNewStudents() {
        this.props.cancelAddingNewStudents( this.props.roomName );
    }

    render() {
        c = this.props.controller;
        m = this.props.model;
        
        let numberOfStudents = this.props.roster ? this.props.roster.length : 0;
        
        let topPagination = e('div', {
            children: e(RosterActionButtons, {
                numberOfStudents: numberOfStudents,
                changePage: (page) => this.setState({page: page}),
                currentPage: this.state.page,
                roomName: this.props.roomName,
                hideRosterData: this.props.hideRosterData
            })
        });
        
        let bottomPagination = e('div', {
            children: e(RosterActionButtons, {
                showMainButtons: false,
                hideRosterData: this.props.hideRosterData,
                numberOfStudents: numberOfStudents,
                changePage: (page) => this.setState({page: page}),
                currentPage: this.state.page,
                roomName: this.props.roomName
            })
        });
        
        let message = null;
        
        if (this.props.message) {
            message = e(ImportRosterMessage, {
                name: this.props.roomName
            });
        }
        
        // add only one student in the roster
        // this will not be displayed in popup
        let newStudentHTML = null;
        
        if (this.props.newStudents && this.props.newStudents.length > 0) {
            newStudentHTML = e(StudentEntryView, Object.assign({
                studentData: this.props.newStudents[0],
                roomName: this.props.roomName,
                editMode: true,
                singular: true
            }, this.props));
        }

        let studentsList = this.props.roster;
        
        if (studentsList && studentsList.length > 0) {
            let aux = (this.state.page - 1) * MAX_STUDENTS_ON_PAGE;
            studentsList = studentsList.slice(aux, aux + MAX_STUDENTS_ON_PAGE);
        }
        
        return e('div', {
            className: 'roster-container',
            children: [
                topPagination,
                message,
                newStudentHTML,
                e(StudentsInRosterList, Object.assign({
                    roomName: this.props.roomName,
                    logged_in: this.props.logged_in,
                    studentsList: studentsList,
                    students_logged_in: this.props.students_logged_in,
                    hideRosterData: this.props.hideRosterData
                }, this.props)),
                bottomPagination
            ]
        });
    }
}
