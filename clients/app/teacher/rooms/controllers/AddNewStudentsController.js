import React from 'react';
import Backbone from 'backbone';
import _ from 'underscore';
import model from 'RoomsModel';
import view from 'AddNewStudentsView';
import modal from 'ModalController';
import {translate} from 'translator';

function AddNewStudentsController() {
    _.extend(this, Backbone.Events);
}

AddNewStudentsController.prototype = {

    render: function() {
        modal.render({
            title: translate('Add Students'),
            content: React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
        });
    },

    validateStudent: function(field, value, indexPos, oldStudentID) {
        return model.validateStudent(field, value, indexPos, oldStudentID);
    },

    saveNewStudents: function() {
        return model.saveNewStudents();
    },

    cancelAddingNewStudents: function(roomName) {
        model.cancelAddingNewStudents(roomName);
    },

    addOneNewStudent: function() {
        return model.addOneNewStudent();
    },

    removeNewStudent: function(index) {
        return model.removeNewStudent(index);
    },

    closePopup: function() {
        modal.closeModal();
    },

};

let controller = new AddNewStudentsController();

controller.listenTo(model, 'modal:close', controller.closePopup);

module.exports = controller;
