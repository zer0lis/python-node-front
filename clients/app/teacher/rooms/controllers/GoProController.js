import React from 'react';
import view from 'GoProView';
import platform from 'Platform';
import modal from 'ModalController';

class GoProController {

    render(props) {
        modal.render({
            modalWidth: '740',
            modalClass: 'go-pro-modal',
            content: React.createElement(
                view, {
                    hideGoProButton: props && props.hideGoProButton || platform.isIosApp,
                    goProButtonCliked: () => {
                        modal.closeModal();
                        props.router.routeToProfileBilling();
                    }
                }
            )
        });
    }

}

let controller = new GoProController();
module.exports = controller;
