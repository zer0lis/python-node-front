import React      from 'react';
import ReactDOM   from 'react-dom';
import _          from 'underscore';
import teacher from 'TeacherModel';
import RosterFileModel from 'RosterFileModel';
import view       from 'RosterFileView';
import utils      from 'Utils';
import Constants  from 'Constants';
import roomsModel from 'RoomsModel';
import dots    from 'Dots';
import {translate} from 'translator';

let model = null;

class RosterFileController {
    
    open(importing, roomName) {
        model = new RosterFileModel();
        model.importing = importing;
        model.roomName = roomName;
        
        let container = document.createElement('div');
        container.id = 'roster-file-modal-container';
        document.body.appendChild(container);
        
        if (!teacher.rosterSettingsFetched) {
            model.fetchSettings(controller.renderView);
        }
        
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model,
                    teacher: teacher
                }
            ),
            document.getElementById('roster-file-modal-container'),
            () => {
                if (!model.pageBlurred) {
                    model.pageBlurred = true;
                    utils.blurPage();
                }
            }
        );
    }
    
    columnOrderChanged(value) {
        if (model.importing) {
            model.importOrder = value;
        } else {
            model.exportOrder = value;
        }
        
        controller.renderView();
    }
    
    fileTypeChanged(value) {
        if (model.importing) {
            model.importType = value;
        } else {
            model.exportType = value;
        }
        
        controller.renderView();
    }
    
    includeHeaderChanged() {
        if (model.importing) {
            model.importHeader = !model.importHeader;
        } else {
            model.exportHeader = !model.exportHeader;
        }
        
        controller.renderView();
    }
    
    cancelClicked() {
        let container = document.getElementById('roster-file-modal-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
        utils.unblurPage();
        model.pageBlurred = false;
    }
    
    fileChosen(file) {
        if (model.importSettingsChanged) {
            model.saveSettings('import');
        }
        
        model.uploadErrors = [];
        model.uploading = true;
        model.fileName = file.name || '';
        model.progress = 0;
        model.uploadRoster(file, controller.fileUploadProgress, controller.fileUploadResult);
        controller.renderView();
    }
    
    fileUploadProgress(progress) {
        if (progress > 100) {
            progress = 100;
        }
        model.progress = progress;
        controller.renderView();
    }
    
    fileUploadResult(error) {
        model.uploading = false;
        
        if (error) {
            model.uploadErrors = [];
            
            let genericError = translate('There was a problem during the upload. Please check the file and your connection.');
            
            if (error.code) {
                if (error.code === Constants.FILE_UPLOAD_ERROR ||
                    error.code === Constants.FILE_CORRUPT ||
                    error.code === Constants.ROSTER_FILE_NOT_IMPORTABLE) {
                    model.uploadErrors.push(genericError);
                } else if (error.code === Constants.ACTIVITY_RUNNING) {
                    model.uploadErrors.push(translate('An activity is currently running in that classroom.'));
                } else if (error.code === Constants.FILE_TOO_LARGE || error.code === Constants.TOO_MANY_STUDENTS) {
                    model.uploadErrors.push(translate('The roster file is too large or has too many students.'));
                } else if (error.code === Constants.EMPTY_ROSTER_FILE) {
                    model.uploadErrors.push(translate('The roster file has no students.'));
                }
            } else {
                _.each(error, function(value, key) {
                    if (key === 'student_id') {
                        model.uploadErrors.push(translate(`Student ID is missing or longer than ${Constants.MAX_STUDENT_ID_LENGTH} characters in row(s): `) + value.join(', '));
                    } else if (key === 'last_name') {
                        model.uploadErrors.push(translate(`Student last name is missing or longer than ${Constants.MAX_STUDENT_NAME_LENGTH} characters in row(s): `) + value.join(', '));
                    } else if (key === 'first_name') {
                        model.uploadErrors.push(translate(`Student first name is missing or longer than ${Constants.MAX_STUDENT_NAME_LENGTH} characters in row(s): `) + value.join(', '));
                    } else if (key === 'duplicate_student_id') {
                        model.uploadErrors.push(translate('Duplicate student ID in rows: ') + value.join(', '));
                    }
                });
            }
            
            if (model.uploadErrors.length === 0) {
                model.uploadErrors.push(genericError);
            }
            
            controller.renderView();
        } else {
            controller.cancelClicked();
            roomsModel.rosterImported(model.roomName, model.students);
        }
    }
    
    changeClicked() {
        if (model.importing) {
            model.uploadErrors = [];
            model.uploading = false;
        } else {
            model.showExportSummary = false;
        }
        
        controller.renderView();
    }
    
    exportClicked() {
        dots.start('roster-file-export-button');
        
        if (model.exportSettingsChanged) {
            model.saveSettings('export', controller.exportSettingsSaved);
        } else {
            controller.exportSettingsSaved();
        }
    }
    
    exportSettingsSaved() {
        let hiddenIFrameID = 'rosterDownloader',
            iframe = document.getElementById(hiddenIFrameID);
        
        if (iframe === null) {
            iframe = document.createElement('iframe');
            iframe.id = hiddenIFrameID;
            iframe.style.display = 'none';
            document.body.appendChild(iframe);
        }
        
        iframe.src = `${window.backend_host}/rooms/api/students/export/${model.roomName}?type=${model.exportType === Constants.CSV ? 'csv' : 'xlsx'}`;
        
        setTimeout(() => {
            controller.cancelClicked();
        }, 1000);
    }
    
}

let controller = new RosterFileController();
module.exports = controller;
