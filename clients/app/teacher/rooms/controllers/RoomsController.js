import Backbone from 'backbone';
import _ from 'underscore';
import React from 'react';
import ReactDOM from 'react-dom';
import messaging from 'Messaging';
import Constants from 'Constants';
import header from 'HeaderController';
import model from 'RoomsModel';
import view from 'RoomsView';
import user from 'user';
import globalRoom from 'room';
import platform from 'Platform';
import shareRoomModal from 'ShareRoomController';
import selectNewStudentsModal from 'SelectNewStudentsController';
import addNewStudentsModal from 'AddNewStudentsController';
import goPro from 'GoProController';
import popup from 'PopupController';
import modal from 'ModalController';
import rosterFileController from 'RosterFileController';
import addRoomModal from 'AddRoomController';
import renameRoomModal from 'RenameRoomController';
import {translate} from 'translator';

let router = null;

function RoomsController() {
    _.extend(this, Backbone.Events);
    messaging.addListener({
        message: Constants.PRESENCE,
        callback: this.presenceChanged
    });
}

RoomsController.prototype = {
    
    doRooms: function(teacherRouter) {
        router = teacherRouter;
        model.getRoomsList();
    },

    renderView: function() {
        if (Backbone.history.fragment === 'rooms') {
            header.render('rooms');
            ReactDOM.render(
                React.createElement(
                    view, {
                        controller: controller,
                        model: model
                    }
                ),
                document.getElementById('main-content')
            )
        }
    },

    addRoomClicked: function() {
        addRoomModal.open({
            callback: controller.roomAdded
        });
    },
    
    roomAdded: function(room) {
        room.notify = {
            status: true,
            message: translate('Classroom added!')
        };
        
        model.roomsList.push(room);
        
        model.updateRoomsList();

        setTimeout(() => {
            room.notify = '';
            model.updateRoomsList();
        }, model.get('notify_timer'));
        
        model.getRoomsList(controller.getRoomsListResult);
    },
    
    editRoomClicked: function(id, name) {
        renameRoomModal.open({
            callback: controller.roomRenamed,
            id: id,
            name: name
        });
    },
    
    roomRenamed: function(id, newRoomName) {
        let changedRoom = _.findWhere(model.roomsList, {id: id}),
            oldRoomName = changedRoom.name.toLowerCase(),
            currentRoomName = globalRoom.getRoomName().toLowerCase();
        
        if (currentRoomName === oldRoomName) {
            globalRoom.set('name', newRoomName);
            globalRoom.clearAllHandraises();
            
            messaging.unsubscribe({
                channel: [`${oldRoomName}-teacher`, `${oldRoomName}-student`]
            });
            
            messaging.subscribe({
                channel: `${newRoomName}-teacher`,
                messages: true
            });
            
            messaging.subscribe({
                channel: `${newRoomName}-student`,
                presence: true
            });
        }
        
        changedRoom.name = newRoomName;
        changedRoom.errorMessage = null;
        changedRoom.notify = {
            'status': true,
            'message': translate('Classroom name changed!')
        };
        
        model.updateRoomsList(model.roomsList);
        
        setTimeout(() => {
            changedRoom.notify = '';
            model.updateRoomsList();
        }, model.get('notify_timer'));
        
        header.roomJoinSuccess();
        model.set('listeners', {});
        controller.presenceChanged();
    },
    
    shareRoomClicked: function(event, roomName) {
        if (!user.isPro()) {
            goPro.render({router: router});
        } else {
            model.shareRoom(roomName)
        }
    },

    shareRoomModal: function(obj) {
        shareRoomModal.render({
            roomName: obj.roomName,
            url: obj.url,
            showInfo: obj.showInfo
        });
    },
    
    deleteRoom: function(room_name) {
        popup.render({
            title: translate('Please Confirm'),
            message: translate('Are you sure you want to delete this classroom? (Reports and quizzes will not be deleted.)'),
            buttonText: translate('Yes'),
            cancelText: translate('No'),
            buttonClicked: function() {
                model.deleteRoom(room_name, controller.addDeleteRoomResult);
            }
        });
            
    },
    
    addDeleteRoomResult: function(success) {
        if (success) {
            model.getRoomsList(controller.getRoomsListResult);
        }
    },
    
    getRoomsListResult: function(success) {
        if (success) {
            header.render('rooms');
        }
    },

    filterStudents: function(type) {
        model.filterStudents(type);
    },

    filterRooms: function(type) {
        model.filterRooms(type)
    },

    inMenuChanged: function(room_id, status) {
        model.changeRoomStatus(room_id, status, controller.roomStatusChanged);
    },
    
    roomStatusChanged: function(success) {
        if (success) {
            header.render('rooms');
        }
    },
    
    manageRosterClicked: function(room_name) {
        model.manageRoster(room_name);
        controller.presenceChanged(); // Fetch the roster and subscribe for presence changes, if necessary.
    },
    
    presenceChanged: function() {
        if (!user.isPro()) {
            return;
        }
        
        let rooms = model.roomsList;
        let listeners = model.get('listeners');
        
        if (Backbone.history.fragment !== 'rooms') {
            for (let room of rooms) {
                if (listeners[room.id]) {
                    delete listeners[room.id];
                    messaging.unsubscribe({
                        channel: `${room.name.toLowerCase()}-student`
                    });
                }
            }
            return;
        }
        
        let targetRoom = null;

        for (let room of rooms) {
            if (room.rosterInEdit && room.rostered) {
                if (room.id === globalRoom.get('id')) {
                    room.roster = globalRoom.getStudents(); // The student list for the current room will have been fetched already.
                    room.students_logged_in = globalRoom.get('students_logged_in') || 0;
                    model.studentFilter = 'nameDSC';
                    model.filterStudents('name');
                    model.updateRoomsList();
                    continue;
                }

                targetRoom = room;

                if (listeners[room.id]) {
                    continue;
                }
                
                listeners[room.id] = true;

                messaging.subscribe({
                    channel: `${room.name.toLowerCase()}-student`,
                    presence: true
                });
            } else {
                if (room.id !== globalRoom.get('id')) {
                    if (listeners[room.id]) {
                        delete listeners[room.id];
                        messaging.unsubscribe({
                            channel: `${room.name.toLowerCase()}-student`
                        });
                    }
                }
            }
        }
        
        if (targetRoom) {
            globalRoom.getStudentList({
                roomName: targetRoom.name,
                success: (students) => {
                    targetRoom.roster = students;
                    let count = 0;
                    for (let student of students) {
                        if (student.logged_in) {
                            count++;
                        }
                    }
                    targetRoom.students_logged_in = count;
                    model.studentFilter = 'nameDSC';
                    model.filterStudents('name');
                    model.updateRoomsList();
                }
            });
        }
    },
    
    deleteRoster: function() {
        popup.render({
            title: translate('Please Confirm'),
            message: translate('Are you sure you want to delete this roster? (Reports and quizzes will not be deleted.)'),
            buttonText: translate('Yes'),
            cancelText: translate('No'),
            buttonClicked: function() {
                model.deleteRoster();
            }
        });
    },

    editStudentButtonClicked: function(event, studentID) {
        model.editStudentButtonClicked(studentID);
    },

    cancelEditStudentButtonClicked: function(studentID, singular) {
        model.cancelEditStudentButtonClicked(studentID, singular);
    },

    importClicked: function(roomName) {
        rosterFileController.open(true, roomName);
    },
    
    exportClicked: function(roomName) {
        rosterFileController.open(false, roomName);
    },
    
    validateStudent: function(field, value, indexPos, oldStudentID) {
        return model.validateStudent(field, value, indexPos, oldStudentID)
    },

    updateStudent: function(roomName, oldStudentID) {
        model.updateStudent(roomName, oldStudentID)
    },

    deleteStudent: function(student, roomName) {
        popup.render({
            title: translate('Please Confirm'),
            message: translate('Are you sure you want to delete this student?'),
            buttonText: translate('Yes'),
            cancelText: translate('No'),
            buttonClicked: function() {
                return model.deleteStudent(student, roomName)
            }
        });
        
    },

    manualStudentsClicked: function() {
        // get the max number of new students from the model and trigger the select new students popup
        model.addStudentsManually()
    },

    /**
     * methods used when the room has a roster and the teacher chooses to add 1 student
     * button found in the Add Student dropdown
     */
    addOneStudentClicked: function() {
        model.addOneStudent();
    },

    /**
     * method used to show the pop up where the user can write the number of students it wants to add to a roster
     * method called when the room is public and when it already has a roster
     * @param  {String} room_name The room name
     */
    selectManualStudentsPopup: function(room_name) {
        selectNewStudentsModal.render({
            room_name: room_name,
            studentsInRoom: true,
            loader: false
        });
    },

    addStudentsPopup: function() {
        if (platform.isMobileBrowser) {
            // let the mobile keyboard close
            setTimeout(function(){ 
                modal.closeModal();
                addNewStudentsModal.render();
            }, 500);
        } else {
            modal.closeModal();
            addNewStudentsModal.render();
        }
    },

    goProClicked: function() {
        goPro.render({router: router});
    },

    routeToBilling: function() {
        router.routeToProfileBilling();
    },

    rerenderRoomList: function() {
        model.getRoomsList();
    },

    closePopup: function() {
        popup.closePopup();
    },

    roomNameClicked: function(roomName) {
        if (globalRoom.getRoomName().toUpperCase() !== roomName.toUpperCase()) {
            popup.render({
                title: translate('Please Confirm'),
                message: translate('Are you sure you want to change to the {0} classroom?').format(roomName.toUpperCase()),
                buttonText: translate('Yes'),
                cancelText: translate('No'),
                buttonClicked: function() {
                    controller.switchRoom(roomName);
                }
            });
        }
    },

    switchRoom: function(roomName) {
        header.switchRoom(roomName);
    }
    
};

var controller = new RoomsController();

/* Events from RoomsListModel */
controller.listenTo(model, 'roomsList:updated', controller.renderView);
controller.listenTo(model, 'new_room_added:success', controller.rerenderRoomList);
controller.listenTo(model, 'select_new_students_popup:success', controller.selectManualStudentsPopup);
controller.listenTo(model, 'new_students_popup:success', controller.addStudentsPopup);
controller.listenTo(model, 'current_room:changed', controller.switchRoom);
controller.listenTo(model, 'share_room:success', controller.shareRoomModal);

module.exports = controller;
