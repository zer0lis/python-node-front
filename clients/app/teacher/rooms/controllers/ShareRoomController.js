import React from 'react';
import model from 'RoomsModel';
import view from 'ShareRoomView';
import modal from 'ModalController';
import {translate} from 'translator';

class ShareRoomController {

    render(options) {
        modal.render({
            title: translate('Share Classroom'),
            modalWidth: '436',
            content: React.createElement(view, {
                roomName: options.roomName,
                url: options.url,
                showInfo: options.showInfo,
                regenerateLink: controller.regenerateLink
            })
        });
    }

    closePopup() {
        modal.closeModal();
    }

    regenerateLink(roomName) {
        model.shareRoom(roomName, true);
    }

}

let controller = new ShareRoomController();
module.exports = controller;
