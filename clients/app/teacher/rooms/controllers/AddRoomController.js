import React from 'react';
import ReactDOM from 'react-dom';
import RoomModel from 'RoomModel';
import view from 'AddRoomView';
import utils from 'Utils';
import dots from 'Dots';
import Constants from 'Constants';
import {translate} from 'translator';

let model = null;

class AddRoomController {
    
    open(props) {
        model = new RoomModel();
        model.callback = props.callback;
        
        let container = document.createElement('div');
        container.id = 'add-room-modal-container';
        document.body.appendChild(container);
        
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('add-room-modal-container'),
            () => {
                if (!model.pageBlurred) {
                    model.pageBlurred = true;
                    utils.blurPage();
                }
            }
        );
    }
    
    nameChanged(name) {
        model.name = name;
        model.nameStatus.error = false;
        model.nameStatus.message = '';
        controller.renderView();
    }
    
    addClicked() {
        model.name = model.name.trim();
        
        if (model.name.length === 0) {
            model.nameStatus.error = true;
            model.nameStatus.message = '';
        } else if (!model.isValidName()) {
            model.nameStatus.error = true;
            model.nameStatus.message = translate('Only letters (A-Z) and numbers are allowed in a classroom name.');
        } else {
            dots.start('addRoomButton');
            model.addRoom(controller.addRoomResult);
        }
        
        controller.renderView();
    }
    
    addRoomResult(error, room) {
        if (error) {
            model.nameStatus.error = true;
            
            if (error.code === Constants.ROOM_ALREADY_EXISTS) {
                model.nameStatus.message = translate('That classroom name is not available.');
            } else {
                model.nameStatus.message = translate('An unknown error occurred. Please try again later.');
            }
            
            dots.stop();
            controller.renderView();
        } else {
            controller.cancelClicked();
            model.callback(room);
        }
    }
    
    cancelClicked() {
        let container = document.getElementById('add-room-modal-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
        utils.unblurPage();
    }
    
}

let controller = new AddRoomController();
module.exports = controller;
