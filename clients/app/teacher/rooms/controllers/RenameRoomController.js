import React from 'react';
import ReactDOM from 'react-dom';
import RoomModel from 'RoomModel';
import view from 'RenameRoomView';
import utils from 'Utils';
import dots from 'Dots';
import Constants from 'Constants';
import {translate} from 'translator';

let model = null;

class RenameRoomController {
    
    open(props) {
        model = new RoomModel();
        model.callback = props.callback;
        model.id = props.id;
        model.originalName = model.name = props.name;
        
        let container = document.createElement('div');
        container.id = 'rename-room-modal-container';
        document.body.appendChild(container);
        
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('rename-room-modal-container'),
            () => {
                if (!model.pageBlurred) {
                    model.pageBlurred = true;
                    utils.blurPage();
                }
            }
        );
    }
    
    nameChanged(name) {
        model.name = name;
        model.nameStatus.error = false;
        model.nameStatus.message = '';
        controller.renderView();
    }
    
    renameClicked() {
        model.name = model.name.trim();
        
        if (model.name.length === 0) {
            model.nameStatus.error = true;
            model.nameStatus.message = '';
        } else if (!model.isValidName()) {
            model.nameStatus.error = true;
            model.nameStatus.message = translate('Only letters (A-Z) and numbers are allowed in a classroom name.');
        } else {
            dots.start('renameRoomButton');
            model.renameRoom(controller.renameRoomResult);
        }
        
        controller.renderView();
    }
    
    renameRoomResult(error, id, newName) {
        if (error) {
            model.nameStatus.error = true;
            
            if (error.code === Constants.ROOM_ALREADY_TAKEN) {
                model.nameStatus.message = translate('That classroom name is not available.');
            } else {
                model.nameStatus.message = translate('An unknown error occurred. Please try again later.');
            }
            
            dots.stop();
            controller.renderView();
        } else {
            controller.cancelClicked();
            model.callback(id, newName);
        }
    }
    
    cancelClicked() {
        let container = document.getElementById('rename-room-modal-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
        utils.unblurPage();
    }
    
}

let controller = new RenameRoomController();
module.exports = controller;
