import React from 'react';
import model from 'RoomsModel';
import view from 'SelectNewStudentsView';
import modal from 'ModalController';
import {translate} from 'translator';

class SelectNewStudentsController {

    render(options) {
        modal.render({
            title: translate('Add Students'),
            content: React.createElement(
                view, {
                    controller: controller,
                    model: model,
                    name: options.room_name,
                    studentsInRoom: options.studentsInRoom,
                    loader: options.loader
                }
            )
        });
    }

    selectedNewClicked(nrOfStudentsToAdd) {
        model.addStudents(nrOfStudentsToAdd);
    }

    closePopup() {
        modal.closeModal();
    }

}

let controller = new SelectNewStudentsController();
module.exports = controller;
