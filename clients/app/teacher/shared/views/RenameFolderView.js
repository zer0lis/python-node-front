import React  from 'react';
import CloseX from 'CloseX';
import Input  from 'Input';
import {translate} from 'translator';

let e = React.createElement;

export default class RenameFolderView extends React.Component {
    
    componentDidMount() {
        let nameInput = document.getElementById('folderName');
        
        if (nameInput) {
            nameInput.select();
            nameInput.focus();
        }
    }
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        return e('div', {
            id: 'rename-folder-modal-background',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'rename-folder-modal-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('span', {
                                    className: 'popup-title',
                                    children: translate('Rename Folder')
                                }),
                                e('div', {
                                    className: 'cancel-button',
                                    children: [
                                        React.createElement(
                                            CloseX, {
                                                className: 'cancel-button-x',
                                                color: '#cccccc'
                                            }
                                        )
                                    ],
                                    onClick: c.cancelClicked
                                })
                            ]
                        }),
                        e('div', {
                            id: 'rename-folder-modal-content',
                            className: 'form',
                            children: [
                                React.createElement(
                                    Input, {
                                        id: 'folderName',
                                        label: translate('Folder Name'),
                                        maxLength: 255,
                                        value: m.name,
                                        onChange: (event) => c.nameChanged(event.target.value),
                                        onKeyPress: (event) => {
                                            if (event.key === 'Enter' && m.name.length > 0) {
                                                c.renameClicked();
                                            }
                                        }
                                    }
                                ),
                                e('div', {
                                    className: 'rename-folder-buttons-container',
                                    children: [
                                        e('button', {
                                            id: 'renameFolderButton',
                                            className: 'rename-folder-button',
                                            children: translate('RENAME'),
                                            disabled: m.name.length === 0,
                                            onClick: c.renameClicked
                                        }),
                                        e('button', {
                                            className: 'rename-folder-button rename-folder-cancel-button',
                                            children: translate('CANCEL'),
                                            onClick: c.cancelClicked
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
