import React from 'react';
import SuccessIcon from 'LoggedInIcon';
import AlertIcon from 'AlertIcon';
import CloseX from 'CloseX';
import BackArrowIcon from 'BackArrowIcon';
import IconLabelButton from 'IconLabelButton';
import NewFolderIcon from 'NewFolderIcon';
import Input from 'Input';
import ConfirmIcon from 'OkIcon';
import CloseIcon from 'CloseIcon';
import ListFolder from 'ListFolder';
import Loader from 'Loader';
import Constants from 'Constants';
import {translate} from 'translator';

let e = React.createElement;

export default class MoveView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            headerChildren = [],
            actionRowChildren = [],
            rootFolder = m.currentFolder.id === null;
        
        if (m.bannerText) {
            let BannerIcon = m.bannerIsError ? AlertIcon : SuccessIcon;
            
            headerChildren.push(
                e('div', {
                    className: 'move-modal-banner' + (m.bannerIsError ? ' move-modal-banner-error' : ''),
                    children: [
                        e(BannerIcon, {
                            color: '#ffffff',
                            svgClass: 'move-modal-banner-icon'
                        }),
                        e('span', {
                            className: 'move-modal-banner-text',
                            children: m.bannerText
                        })
                    ]
                })
            );
        }
        
        headerChildren.push(
            e('span', {
                className: 'popup-title',
                children: translate('Move to Folder')
            }),
            e('div', {
                className: 'cancel-button' + (m.creatingFolder ? ' cancel-button-disabled' : ''),
                children: [
                    e(CloseX, {
                        className: 'cancel-button-x',
                        color: '#cccccc'
                    })
                ],
                onClick: c.cancelClicked
            })
        );
        
        if (!rootFolder) {
            actionRowChildren.push(
                e('button', {
                    className: 'move-modal-back-button',
                    disabled: m.creatingFolder,
                    children: [
                        e(BackArrowIcon, {
                            className: 'move-modal-back-button-icon'
                        })
                    ],
                    onClick: c.backClicked
                })
            );
        }
        
        let currentLabelClass = 'move-modal-current-label';
        
        if (rootFolder) {
            currentLabelClass += ' move-modal-current-label-root';
        }
        
        if (m.typingFolderName && !rootFolder) {
            currentLabelClass += ' move-modal-current-label-truncated';
        }
        
        actionRowChildren.push(
            e('span', {
                className: currentLabelClass,
                children: m.currentFolder.name
            })
        );
        
        let newFolderContainer = [],
            createConfirmDisabled = m.newFolderName.length === 0 || m.creatingFolder;
        
        if (m.typingFolderName) {
            newFolderContainer.push(
                e(Input, {
                    id: 'newFolderName',
                    disabled: m.creatingFolder,
                    maxLength: 255,
                    value: m.newFolderName,
                    onChange: (event) => c.newFolderNameChanged(event.target.value),
                    onKeyPress: (event) => {
                        if (event.key === 'Enter' && !createConfirmDisabled) {
                            c.confirmCreateClicked();
                        }
                    }
                }),
                e('button', {
                    className: 'move-modal-confirm-create-folder-button',
                    disabled: createConfirmDisabled,
                    children: [
                        e(ConfirmIcon, {
                            color: createConfirmDisabled ? '#cccccc' : '#8cb4d2'
                        })
                    ],
                    onClick: c.confirmCreateClicked
                }),
                e('button', {
                    className: 'move-modal-cancel-create-folder-button',
                    disabled: m.creatingFolder,
                    children: [
                        e(CloseIcon, {
                            className: 'move-modal-cancel-create-folder-button-icon',
                            color: m.creatingFolder ? '#cccccc' : '#8cb4d2'
                        })
                    ],
                    onClick: c.cancelCreateClicked
                })
            );
        } else {
            newFolderContainer.push(
                e(IconLabelButton, {
                    className: 'move-modal-create-folder-button',
                    icon: NewFolderIcon,
                    iconClass: 'move-modal-create-folder-button-icon',
                    label: translate('CREATE FOLDER'),
                    onClick: c.createFolderClicked
                })
            );
        }
        
        actionRowChildren.push(
            e('div', {
                className: 'move-modal-new-folder-container',
                children: newFolderContainer
            })
        );
        
        let folderListChildren  = [];
        
        if (m.creatingFolder) {
            folderListChildren.push(
                e(Loader, {
                    className: 'move-modal-loader',
                    text: translate('Creating Folder...')
                })
            );
        } else {
            for (let folder of m.currentFolder.children) {
                let selectable = folder !== m.startingFolder,
                    hideButton = false,
                    selectError = !selectable ? translate('Item is already in this folder') : '';
                
                if (selectable) {
                    for (let id of m.folderIds) {
                        if (id === folder.id) {
                            selectable = false;
                            hideButton = true;
                            selectError = translate('Cannot move a folder into itself');
                            break;
                        }
                    }
                }
                
                if (!folder.deleted) {
                    folderListChildren.push(
                        e(ListFolder, {
                            hideButton: hideButton,
                            id: folder.id,
                            name: folder.name,
                            selectable: selectable,
                            selected: folder === m.selectedFolder,
                            selectError: selectError,
                            onClick: () => c.goButtonClicked(folder),
                            onSelect: () => c.folderSelected(folder)
                        })
                    );
                }
            }

            if (folderListChildren.length === 0) {
                folderListChildren.push(
                    e('span', {
                        className: 'move-modal-folder-list-empty',
                        children: translate('This folder has no subfolders')
                    })
                );
            }
        }
        
        let moveButtonDisabled = m.creatingFolder ||
                                 m.destinationFolder === m.startingFolder ||
                                 m.selectedFolder === m.startingFolder;
        
        if (moveButtonDisabled && !m.creatingFolder && m.destinationFolder.id === Constants.ROOT_FOLDER) {
            moveButtonDisabled = false;
        }
        
        return e('div', {
            id: 'mov-modal-background',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'move-modal-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: headerChildren
                        }),
                        e('div', {
                            id: 'move-modal-content',
                            className: 'form',
                            children: [
                                e('div', {
                                    className: 'move-modal-action-row' + (rootFolder ? ' move-modal-action-row-root' : ''),
                                    children: actionRowChildren
                                }),
                                e('div', {
                                    className: 'move-modal-folder-list-container',
                                    children: folderListChildren
                                }),
                                e('div', {
                                    className: 'move-modal-buttons-container',
                                    children: [
                                        e('button', {
                                            id: 'moveButton',
                                            className: 'move-modal-button',
                                            children: m.selectedFolder ? translate('MOVE') : translate('MOVE HERE'),
                                            disabled: moveButtonDisabled,
                                            onClick: c.moveClicked
                                        }),
                                        e('button', {
                                            className: 'move-modal-button move-modal-cancel-button',
                                            children: translate('CANCEL'),
                                            disabled: m.creatingFolder,
                                            onClick: c.cancelClicked
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
