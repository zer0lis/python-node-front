import React  from 'react';
import CloseX from 'CloseX';
import Input  from 'Input';
import {translate} from 'translator';

let e = React.createElement;

export default class CreateFolderView extends React.Component {
    
    componentDidMount() {
        let nameInput = document.getElementById('folderName');
        if (nameInput) {
            nameInput.select();
            nameInput.focus();
        }
    }
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        return e('div', {
            id: 'create-folder-modal-background',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'create-folder-modal-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('span', {
                                    className: 'popup-title',
                                    children: translate('Create Folder')
                                }),
                                e('div', {
                                    className: 'cancel-button',
                                    children: [
                                        e(CloseX, {
                                            className: 'cancel-button-x',
                                            color: '#cccccc'
                                        })
                                    ],
                                    onClick: c.cancelClicked
                                })
                            ]
                        }),
                        e('div', {
                            id: 'create-folder-modal-content',
                            className: 'form',
                            children: [
                                e(Input, {
                                    id: 'folderName',
                                    label: translate('Folder Name'),
                                    maxLength: 255,
                                    value: m.name,
                                    onChange: (event) => c.nameChanged(event.target.value),
                                    onKeyPress: (event) => {
                                        if (event.key === 'Enter') {
                                            c.createClicked();
                                        }
                                    }
                                }),
                                e('div', {
                                    className: 'create-folder-buttons-container',
                                    children: [
                                        e('button', {
                                            id: 'createFolderButton',
                                            className: 'create-folder-button',
                                            children: translate('CREATE'),
                                            onClick: c.createClicked
                                        }),
                                        e('button', {
                                            className: 'create-folder-button create-folder-cancel-button',
                                            children: translate('CANCEL'),
                                            onClick: c.cancelClicked
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
