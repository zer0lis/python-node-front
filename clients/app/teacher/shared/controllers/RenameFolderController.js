import React from 'react';
import ReactDOM from 'react-dom';
import FolderModel from 'FolderModel';
import view from 'RenameFolderView';
import utils from 'Utils';
import dots from 'Dots'

let model = null;

class RenameFolderController {
    
    open(props) {
        model = new FolderModel();
        model.callback = props.callback;
        model.id = props.id;
        model.name = props.name;
        
        let container = document.createElement('div');
        container.id = 'rename-folder-modal-container';
        document.body.appendChild(container);
        
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('rename-folder-modal-container'),
            () => {
                if (!model.pageBlurred) {
                    model.pageBlurred = true;
                    utils.blurPage();
                }
            }
        );
    }
    
    nameChanged(name) {
        model.name = name;
        controller.renderView();
    }
    
    renameClicked() {
        dots.start('renameFolderButton');
        model.renameFolder(controller.renameResult);
    }
    
    renameResult(id, newName) {
        controller.cancelClicked();
        model.callback(id, newName);
    }
    
    cancelClicked() {
        let container = document.getElementById('rename-folder-modal-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
        utils.unblurPage();
    }
    
}

let controller = new RenameFolderController();
module.exports = controller;
