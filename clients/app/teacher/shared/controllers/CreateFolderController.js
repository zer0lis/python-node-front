import React from 'react';
import ReactDOM from 'react-dom';
import FolderModel from 'FolderModel';
import view from 'CreateFolderView';
import utils from 'Utils';
import dots from 'Dots'

let model = null;

class CreateFolderController {
    
    open(props) {
        model = new FolderModel();
        model.callback = props.callback;
        model.parentId = props.parentId;
        model.type = props.type;
        
        let container = document.createElement('div');
        container.id = 'create-folder-modal-container';
        document.body.appendChild(container);
        
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('create-folder-modal-container'),
            () => {
                if (!model.pageBlurred) {
                    model.pageBlurred = true;
                    utils.blurPage();
                }
            }
        );
    }
    
    nameChanged(name) {
        model.name = name;
        controller.renderView();
    }
    
    createClicked() {
        dots.start('createFolderButton');
        model.createFolder(controller.createResult);
    }
    
    createResult(id, name) {
        controller.cancelClicked();
        model.callback(id, name);
    }
    
    cancelClicked() {
        let container = document.getElementById('create-folder-modal-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
        utils.unblurPage();
    }
    
}

let controller = new CreateFolderController();
module.exports = controller;
