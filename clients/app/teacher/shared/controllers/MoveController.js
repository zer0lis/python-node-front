import React from 'react';
import ReactDOM from 'react-dom';
import MoveModel from 'MoveModel';
import FolderModel from 'FolderModel';
import view from 'MoveView';
import utils from 'Utils';
import dots from 'Dots';
import {translate} from 'translator';
import teacher from 'TeacherModel';
import user from 'user';
import Constants from 'Constants';

let model = null;

class MoveController {
    
    open(props) {
        model = new MoveModel();
        model.callback = props.callback;
        model.folderNavModel = props.folderNavModel;
        model.currentFolder = model.destinationFolder = model.startingFolder = props.startingFolder;
        model.folderIds = props.folderIds;
        model.quizIds = props.quizIds;
        model.type = props.type;
        
        let container = document.createElement('div');
        container.id = 'move-modal-container';
        document.body.appendChild(container);
        
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('move-modal-container'),
            () => {
                if (!model.pageBlurred) {
                    model.pageBlurred = true;
                    utils.blurPage();
                }
                
                if (!model.newFolderNameFocused) {
                    let nameInput = document.getElementById('newFolderName');
                    if (nameInput) {
                        nameInput.focus();
                        model.newFolderNameFocused = true;
                    }
                }
            }
        );
    }
    
    backClicked() {
        let parentFolder = model.folderNavModel.getFolder(model.currentFolder.parentId);
        model.currentFolder = model.destinationFolder = parentFolder;
        model.selectedFolder = null;
        controller.renderView();
    }
    
    createFolderClicked() {
        model.typingFolderName = true;
        model.newFolderNameFocused = false;
        controller.renderView();
    }
    
    newFolderNameChanged(name) {
        model.newFolderName = name;
        controller.renderView();
    }
    
    confirmCreateClicked() {
        model.creatingFolder = true;
        controller.renderView();
        
        // Send the request through FolderModel to avoid client-side duplication of API request logic.
        let folderModel = new FolderModel();
        folderModel.name = model.newFolderName;
        folderModel.parentId = model.currentFolder.id;
        folderModel.type = model.type;
        folderModel.createFolder(controller.createFolderResult);
    }
    
    createFolderResult(id) {
        model.creatingFolder = false;
        
        if (!id) {
            model.bannerText = translate('Unknown Error. Please try again later.');
            model.bannerIsError = true;
        } else {
            model.bannerText = translate('Folder Created!');
            model.bannerIsError = false;
            
            let now = new Date();
            
            teacher.quizFolderNav.addFolder({
                children: [],
                date: now,
                deleted: false,
                expanded: false,
                id: id,
                lastUpdated: now,
                name: model.newFolderName,
                parentId: model.currentFolder.id,
                type: model.type,
                userId: user.getId()
            }, model.currentFolder.id);
            
            model.newFolderName = '';
            model.typingFolderName = false;
        }
        
        controller.startBannerCountdown();
        controller.renderView();
    }
    
    startBannerCountdown() {
        if (model.bannerTimeoutId) {
            window.clearTimeout(model.bannerTimeoutId);
            model.bannerTimeoutId = 0;
        }
        model.bannerTimeoutId = window.setTimeout(() => {
            model.bannerText = '';
            controller.renderView();
        }, Constants.BANNER_TIMEOUT * 1.25);
    }
    
    cancelCreateClicked() {
        model.newFolderName = '';
        model.typingFolderName = false;
        controller.renderView();
    }
    
    folderSelected(folder) {
        if (folder === model.selectedFolder) {
            model.selectedFolder = null;
            model.destinationFolder = model.currentFolder;
        } else {
            model.selectedFolder = model.destinationFolder = folder;
        }
        controller.renderView();
    }
    
    goButtonClicked(folder) {
        model.currentFolder = model.destinationFolder = folder;
        model.selectedFolder = null;
        controller.renderView();
    }
    
    moveClicked() {
        dots.start('moveButton');
        model.moveQuizzes(controller.moveResult);
        model.moving = true;
        controller.renderView();
    }
    
    moveResult(success) {
        let result = null;
        
        if (success) {
            result = {
                newParentId: model.destinationFolder.id,
                folderIds: model.folderIds,
                quizIds: model.quizIds
            };
        }
        
        controller.cancelClicked();
        model.callback(result);
    }
    
    cancelClicked() {
        if (model.creatingFolder) {
            return;
        }
        
        if (model.bannerTimeoutId) {
            window.clearTimeout(model.bannerTimeoutId);
        }
        
        let container = document.getElementById('move-modal-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
        utils.unblurPage();
    }
    
}

let controller = new MoveController();
module.exports = controller;
