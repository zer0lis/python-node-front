let _        = require('underscore'),
    utils    = require('Utils'),
    request  = require('Request'),
    room     = require('room'),
    Constants = require('Constants'),
    translate = require('translator').translate;

class ReportsModel {
    
    constructor() {
        this.allChecked = false; // Whether the "ALL" box is checked.
        this.archivedTotal = -1; // The total number of archived reports.
        this.activeTotal = -1; // The total number of active reports.
        this.bannerText = ''; // The text currently appearing in the reports banner.
        this.bannerSubText = ''; // The sub text currently appearing in the reports banner.
        this.bannerTimeoutId = null; // The id returned by setTimeout() when showing the banner.
        this.fetchingReports = false; // Whether reports are being fetched (i.e. a network request is in progress).
        this.hasSearched = false; // Whether the teacher has searched.
        this.lastSearchText = ''; // The text used in the "nothing found" message (in case the user changes the search text while the search is in progress).
        this.page = 1; // The current page of reports being displayed.
        this.reports = {}; // The report object with numeric keys representing page numbers and arrays of reports for values.
        this.roomNames = {}; // An object with room name keys and report number values: {room1: 12, room2: 5, deleted_rooms: 3, ...}
        this.roomTotal = -1; // The total number of reports in the currently selected room.
        this.searchText = '';  // The text in the "Search Reports" input.
        this.showBanner = false; // Whether the banner should be shown.
        this.sortColumn = 'date'; // The column by which the teacher is currently sorting.
        this.sortDirection = 'desc'; // The direction of the sort ('asc' or 'desc').
        this.state = room.getRoomName() || 'active'; // The value of the currently selected item in the filter select.
        this.totalResults = 0; // The total number of reports found by a search.
        this.trashedTotal = -1; // The total number of reports in the trash.
        this.waitingText = translate('Searching Reports...');
    }
    
    /*----------------------*/
    /*    Helper Methods    */
    /*----------------------*/
    
    // Reset the total number of reports of each type to -1, which means we don't currently know what the totals are.
    resetTotals() {
        this.activeTotal = -1;
        this.roomTotal = -1;
        this.archivedTotal = -1;
        this.trashedTotal = -1;
    }
    
    // Check if the report is from the rooms listing.
    checkRoomTotal(room) {
        let roomsList = this.roomNames;
        
        if (room in roomsList) {
            this.roomTotal = roomsList[room];
            return true;
        } else {
            return false;
        }
    }
    
    resetReports() {
        this.reports = {};
        this.setAllChecked(false);
    }
    
    getVisibleReports() {
        let visibleReports = this.reports[this.page] || [];
        
        if (visibleReports.length > 0) {
            if (this.sortColumn === 'type') {
                visibleReports = _.sortBy(visibleReports, 'displayType');
            } else if (this.sortColumn === 'date') {
                visibleReports = _.sortBy(visibleReports, 'date');
            } else {
                visibleReports = _.sortBy(visibleReports, (item) => {
                    let value = item[this.sortColumn] || '';
                    return value.toLowerCase();
                });
            }
            
            if (this.sortDirection === 'desc') {
                visibleReports.reverse(); // Underscore sorts ascending by default, so reverse if needed.
            }
        }
        
        return visibleReports;
    }
    
    getSelectedReports() {
        let selectedReports = [];
        
        for (let report of this.getVisibleReports()) {
            if (report.selected) {
                selectedReports.push(report);
            }
        }
        
        return selectedReports;
    }
    
    getReportByActivityId(activityId) {
        for (let report of this.getVisibleReports()) {
            if (report.activityId === activityId) {
                return report;
            }
        }
    }
    
    toggleSortDirection() {
        this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    }
    
    setAllChecked(value) {
        this.allChecked = value;
        
        if (this.allChecked) {
            this.checkAll();
        } else {
            this.uncheckAll();
        }
    }
    
    checkAll() {
        for (let report of this.getVisibleReports()) {
            report.selected = true;
        }
    }
    
    uncheckAll() {
        for (let report of this.getVisibleReports()) {
            report.selected = false;
        }
    }
    
    anySelected() {
        for (let report of this.getVisibleReports()) {
            if (report.selected) {
                return true;
            }
        }
        
        return false;
    }
    
    toggleSelected(activityId) {
        for (let report of this.getVisibleReports()) {
            if (report.activityId === activityId) {
                report.selected = !report.selected;
                break;
            }
        }
    }
    
    getDisplayName(report) {
        if (report.name.length > 0) {
            return report.name;
        } else if (report.activity_type === 'short_answer') {
            return translate('Short Answer');
        }
        return '';
    }
    
    getDisplayType(type) {
        if (type === 'quiz') {
            return translate('Quiz');
        } else if (type === 'space_race') {
            return translate('Spaceship Game');
        } else if (type === 'short_answer') {
            return translate('Short Answer');
        } else if (type === 'exit_ticket') {
            return translate('Final Survey');
        }
        return '';
    }
    
    /*-----------------------*/
    /*    Client Requests    */
    /*-----------------------*/
    
    fetchReports(page, callback) {
        if (this.fetchingReports) {
            return;
        }
        
        this.fetchingReports = true;
        
        this.waitingText = translate('Searching Reports...');
        
        let state = this.state;
        
        let offset = page - 1 < 0 ? 0 : page - 1;
        offset *= Constants.REPORTS_SEARCH_LIMIT;
        
        let url = window.backend_host + '/activities/api/reports/?state={0}&offset={1}&limit={2}'.format(state, offset, Constants.REPORTS_SEARCH_LIMIT);
        
        if (state !== 'active' && state !== 'archived' && state !== 'trashed') {
            if (state === 'deleted_rooms') {
                url = window.backend_host + '/activities/api/reports/?state=active&deleted_rooms=true&offset={0}&limit={1}'.format(offset, Constants.REPORTS_SEARCH_LIMIT);
            } else {
                url = window.backend_host + '/activities/api/reports/?state=active&room_name={0}&offset={1}&limit={2}'.format(state, offset, Constants.REPORTS_SEARCH_LIMIT);
            }
        }
        
        if (this.searchText.length > 0) {
            this.lastSearchText = this.searchText;
            url += '&terms=' + utils.encodeForUrl(this.searchText);
        }
        
        request.get({
            url: url,
            success: (data) => {
                if (data && data.reports && data.reports.length) {
                    let reports = [];
                    for (let i = 0; i < data.reports.length; i++) {
                        let roomName = data.reports[i].room;
                        if (roomName) {
                            roomName = roomName.toUpperCase();
                        }
                        reports.push({
                            selected: false,
                            date: new Date(1000 * data.reports[i].start_time),
                            standard: data.reports[i].standard,
                            type: data.reports[i].activity_type,
                            displayType: this.getDisplayType(data.reports[i].activity_type),
                            room: roomName,
                            name: this.getDisplayName(data.reports[i]),
                            daysRemaining: data.reports[i].days_remaining,
                            quizId: data.reports[i].activity_id,
                            activityId: data.reports[i].id
                        });
                    }
                    this.reports[page] = reports;
                    this.totalResults = data.total_results;
                }
                
                if (data && data.metadata) {
                    this.roomNames = data.metadata.rooms || {};
                    this.activeTotal = data.metadata.active;
                    this.archivedTotal = data.metadata.archived;
                    this.trashedTotal = data.metadata.trashed;
                } else {
                    this.resetTotals();
                }
                
                this.fetchingReports = false;
                
                if (callback) {
                    callback(true);
                }
            },
            error: () => {
                this.fetchingReports = false;
                
                if (callback) {
                    callback(false);
                }
            }
        });
    }
    
    changeState(reports, newState, callback) {
        if (this.fetchingReports) {
            return;
        }
        
        this.fetchingReports = true;
        
        let activityIds = [];
        
        for (let i = 0; i < reports.length; i++) {
            activityIds.push(reports[i].activityId);
        }
        
        request.put({
            url: `${window.backend_host}/activities/api/reports/`,
            data: {
                state: newState,
                activity_instance_ids: activityIds
            },
            success: () => {
                callback(true);
            },
            error: (response) => {
                console.error(response);
                callback(false);
            }
        });
    }
    
    purgeReports(reports, callback) {
        if (this.fetchingReports) {
            return;
        }
        
        this.fetchingReports = true;
        
        let activityIds = [];
        
        for (let i = 0; i < reports.length; i++) {
            activityIds.push(reports[i].activityId);
        }
        
        request.post({
            url: `${window.backend_host}/activities/api/reports/purge/`,
            data: {
                activity_instance_ids: activityIds
            },
            success: () => {
                callback(true);
            },
            error: (response) => {
                console.error(response);
                callback(false);
            }
        });
    }

}

module.exports = ReportsModel;
