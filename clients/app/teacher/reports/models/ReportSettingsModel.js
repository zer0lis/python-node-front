'use strict';

let request = require('Request');

class ReportSettingsModel {
    
    constructor() {
        this.activityId = 0; // The id of the activity associated with this report.
        this.activityRunning = false; // Whether an activity is currently running.
        this.allowPdf = false; // Whether the teacher can click the PDF options.
        this.allowViewChart = false; // Whether the teacher can click the "View Chart" option.
        this.excelSelected = true; // Whether the Excel option is selected.
        this.exitCallback = null; // Optional function executed when the report settings modal is closed.
        this.expanded = false; // Whether the modal has been expanded (by clicking a report viewing option).
        this.studentPdfSelected = false; // Whether the student PDF option is selected.
        this.questionPdfSelected = false; // Whether the question PDF option is selected.
        this.quizId = 0; // The id of the quiz associated with this report.
    }
    
    /*---------------*/
    /*    Getters    */
    /*---------------*/
    
    get anyReportSelected() {
        return this.excelSelected ||
               this.studentPdfSelected ||
               this.questionPdfSelected;
    }
    
    /**
     * Generate the paramter string needed for the report request. 
     * @param {string} deliveryType The delivery type. Must be one of: <code>em</code> for email, <code>dl</code> for download, or <code>go</code> for Google Drive.
     * @return {string} The string of parameter names and values, separated by ampersands.
     */
    getRequestParams(deliveryType) {
        let reportType = this.excelSelected ? 'whole-class-excel' : '';
        
        if (this.studentPdfSelected) {
            if (reportType.length > 0) {
                reportType += '+';
            }
            
            reportType += 'all-students';
        }
        
        if (this.questionPdfSelected) {
            if (reportType.length > 0) {
                reportType += '+';
            }
            
            reportType += 'whole-class';
        }
        
        /*
            For reference, this is what the entire URL looks like when the delivery type is email and all reports are selected:
            
            https://api.socrative.com/quizzes/api/activity-report/?dt=em&di=21173726&rt=whole-class-excel+all-students+whole-class
         */
        
        return `dt=${deliveryType}&di=${this.activityId}&rt=${reportType}`;
    }
    
    sendReport(type, callback) {
        request.get({
            url: `${window.backend_host}/quizzes/api/activity-report/?${this.getRequestParams(type)}`,
            success: () => {
                callback(true, type);
            },
            error: (response) => {
                console.error('Error sending report:', response);
                callback(false, type);
            }
        });
    }
    
}

module.exports = ReportSettingsModel;
