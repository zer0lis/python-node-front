import React from 'react';
import Toggle from 'Toggle';
import platform from 'Platform';
import {translate} from 'translator';

let e = React.createElement;

export default class ReportSettingsView extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            isDisabled: false
        };
    }
    
    calculateModalSize() {
        let height = Math.max(window.innerHeight || 200),
            modalWrapper = document.getElementById('modal-wrapper'),
            modalContent = document.getElementById("modal-content");

        if ((modalContent.offsetHeight + 200) > height) {
            modalWrapper.setAttribute('class', 'full-height');
            modalContent.style.height = (height - 40) + 'px';
        } else {
            modalWrapper.setAttribute('class', '');
            modalContent.style.height = '';
        }
    }

    componentDidMount() {
        this.calculateModalSize();
        window.addEventListener('orientationchange', this.calculateModalSize);
    }

    componentWillUnmount() {
        window.removeEventListener('orientationchange', this.calculateModalSize);
    }
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            chartClass ='',
            dashboardClass = '';
        
        if (!m.allowViewChart)
            chartClass += ' not-allowed';
        if (!m.activityRunning)
            dashboardClass += ' not-allowed';
        if (this.state.isDisabled){
            chartClass += ' disabled';
            dashboardClass += ' disabled';
        }

        return e('div', {
            id: 'report-settings',
            children: [
                e('div', {
                    id: 'report-modal',
                    children: [
                        e('div', {
                            className: 'reports-header',
                            children: [
                                e('div', {
                                    className: 'report-settings-heading-icons',
                                    children: [
                                        e('div', {
                                            id: 'get-report',
                                            className: m.expanded ? 'selected' : '',
                                            children: [
                                                e('img', {
                                                    src: window.static_url + 'img/reports-icon.png'
                                                }),
                                                e('p', {
                                                    children: [
                                                        e('span', {
                                                            children: translate('Get ')
                                                        }),
                                                        e('span', {
                                                            children: translate('Reports')
                                                        })
                                                    ]
                                                })
                                            ],
                                            onClick: () => {
                                                c.getReportsClicked();
                                                this.setState({
                                                    isDisabled: true
                                                });
                                            }
                                        }),
                                        e('div', {
                                            id: 'report-view-chart',
                                            className: chartClass,
                                            children: [
                                                e('img', {
                                                    src: window.static_url + 'img/chart-icon.png'
                                                }),
                                                e('p', {
                                                    children: [
                                                        e('span', {
                                                            children: translate('View ')
                                                        }),
                                                        e('span', {
                                                            children: translate('Chart')
                                                        })
                                                    ]
                                                })
                                            ],
                                            onClick: () => {
                                                if (this.state.isDisabled){
                                                    this.setState({
                                                        isDisabled: false
                                                    });
                                                    c.toggleReportsSettings();
                                                } else {
                                                    c.viewChartClicked();
                                                }
                                            }
                                        }),
                                        e('div', {
                                            id: 'report-view-later',
                                            className: dashboardClass,
                                            children: [
                                                e('img', {
                                                    src: window.static_url + 'img/dashboard-icon.png'
                                                }),
                                                e('p', {
                                                    children: [
                                                        e('span', {
                                                            children: translate('To ')
                                                        }),
                                                        e('span', {
                                                            children: translate('Launch')
                                                        })
                                                    ]
                                                })
                                            ],
                                            onClick: () => {
                                                if (this.state.isDisabled){
                                                    this.setState({
                                                        isDisabled: false
                                                    });
                                                    c.toggleReportsSettings();
                                                } else {
                                                    c.toLaunchClicked();
                                                }
                                            }
                                        })
                                    ]
                                })
                            ]
                        }),
                        e('div', {
                            className: 'report-type',
                            style: {display: m.expanded ? 'block' : 'none'},
                            children: [
                                e('h2', {
                                    children: translate('Which report(s) would you like?')
                                }),
                                e('div', {
                                    className: 'all-report-settings',
                                    children: [
                                        e('div', {
                                            className: 'report-settings-option',
                                            children: [
                                                e('div', {
                                                    className: 'report-settings-option-control',
                                                    children: [
                                                        e(Toggle, {
                                                            id: 'whole_class_excel_toggle',
                                                            label: translate('Whole Class Excel'),
                                                            checked: m.excelSelected,
                                                            onChange: c.excelClicked,
                                                            alignment: 'align-right'
                                                        })
                                                    ]
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            className: 'report-settings-option' + (m.allowPdf ? '' : ' disabled'),
                                            children: [
                                                e('div', {
                                                    className: 'report-settings-option-control',
                                                    children: [
                                                        e(Toggle, {
                                                            id: 'student_pdf_toggle',
                                                            label: translate('Individual Student(s) PDF'),
                                                            disabled: !m.allowPdf,
                                                            checked: m.studentPdfSelected,
                                                            onChange: c.studentPdfClicked,
                                                            alignment: 'align-right'
                                                        })
                                                    ]
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            className: 'report-settings-option' + (m.allowPdf ? '' : ' disabled'),
                                            children: [
                                                e('div', {
                                                    className: 'report-settings-option-control',
                                                    children: [
                                                        e(Toggle, {
                                                            id: 'question_pdf_toggle',
                                                            label: translate('Question Specific PDF'),
                                                            disabled: !m.allowPdf,
                                                            checked: m.questionPdfSelected,
                                                            onChange: c.questionPdfClicked,
                                                            alignment: 'align-right'
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    ]
                                }),
                                e('div', {
                                    className: 'report-type-container clearfix',
                                    children: [
                                        e('h2', {
                                            children: translate('How would you like your report(s)?')
                                        }),
                                        e('div', {
                                            id: 'report-email',
                                            className: 'report-option' + (m.anyReportSelected ? '' : ' not-allowed'),
                                            children: [
                                                e('img', {
                                                    src: window.static_url + 'img/email-icon.png',
                                                    width: 62,
                                                    height: 40
                                                }),
                                                e('p', {
                                                    children: translate('E-mail')
                                                })
                                            ],
                                            onClick: c.emailClicked
                                        }),
                                        e('div', {
                                            id: 'report-download',
                                            className: 'report-option' + (m.anyReportSelected && !platform.isIos ? '' : ' not-allowed'),
                                            children: [
                                                e('img', {
                                                    src: window.static_url + 'img/download-icon.png',
                                                    width: 40,
                                                    height: 40
                                                }),
                                                e('p', {
                                                    children: translate('Download')
                                                })
                                            ],
                                            onClick: c.downloadClicked
                                        }),
                                        e('div', {
                                            id: 'report-google-drive',
                                            className: 'report-option' + (this.props.isGoogleUser && m.anyReportSelected ? '' : ' not-allowed'),
                                            children: [
                                                e('img', {
                                                    src: window.static_url + 'img/google-drive-icon.png',
                                                    width: 62,
                                                    height: 40
                                                }),
                                                e('p', {
                                                    children: translate('Google Drive')
                                                })
                                            ],
                                            onClick: c.googleDriveClicked
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
