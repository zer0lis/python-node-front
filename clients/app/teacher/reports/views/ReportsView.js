import React from 'react';
import CheckIcon from 'LoggedInIcon';
import SearchIcon from 'SearchIcon';
import Input from 'Input';
import CloseX from 'CloseX';
import Select from 'Select';
import InfoIcon from 'InfoIcon';
import SortDownIcon from 'SortDownIcon';
import SortUpIcon from 'SortUpIcon';
import Loader from 'Loader';
import NoResultsIcon from 'NoResultsIcon';
import ArchiveIcon from 'ArchiveIcon';
import TrashIcon from 'TrashIcon';
import MoreButton from 'MoreButton';
import Pagination from 'Pagination';
import IconLabelButton from 'IconLabelButton';
import RestoreIcon from 'RestoreIcon';
import Constants from 'Constants';
import platform from 'Platform';
import utils from 'Utils';
import {translate} from 'translator';

let e = React.createElement,
    c = null,
    m = null;

export default class ReportsView extends React.Component {
    
    renderDeleteButton() {
        return e(IconLabelButton, {
            className: 'state-change-button',
            disabled: !this.props.anySelected,
            icon: TrashIcon,
            label: translate('DELETE'),
            onClick: c.deleteClicked
        });
    }
    
    render() {
        c = this.props.controller;
        m = this.props.model;
        
        let reportsChildren = [];
        
        if (m.fetchingReports) {
            reportsChildren = [
                e(Loader, {
                    className: 'reports-loader',
                    text: m.waitingText
                })
            ];
        } else if (this.props.reports.length === 0) {
            let noReportsFoundChildren = [
                e(NoResultsIcon, {
                    className: 'no-reports-found-icon'
                }),
                e('span', {
                    className: 'no-reports-found-large-text',
                    children: translate('No Reports Found')
                })
            ];
            
            if (m.lastSearchText.length > 0) {
                noReportsFoundChildren.push(
                    e('span', {
                        className: 'no-reports-found-small-text',
                        children: translate('Nothing found matching "{0}".').format(m.lastSearchText)
                    }),
                    e('span', {
                        className: 'start-over-text',
                        dangerouslySetInnerHTML: {__html: translate('Delete search term(s) to start over.')}
                    })
                );
            }
            
            reportsChildren = [
                e('div', {
                    className: 'no-reports-found-container',
                    children: noReportsFoundChildren
                })
            ];
        }
        
        let stateChangeButtons = [];
        
        if (m.state === 'archived') {
            stateChangeButtons.push(
                e(IconLabelButton, {
                    className: 'state-change-button',
                    disabled: !this.props.anySelected,
                    icon: ArchiveIcon,
                    label: translate('UNARCHIVE'),
                    onClick: c.unarchiveClicked
                }),
                this.renderDeleteButton()
            );
        } else if (m.state === 'trashed') {
            stateChangeButtons.push(
                e(IconLabelButton, {
                    className: 'state-change-button',
                    disabled: !this.props.anySelected,
                    icon: RestoreIcon,
                    label: translate('RESTORE'),
                    onClick: () => c.restoreClicked() // Zero arguments means restore all checked reports.
                }),
                e(IconLabelButton, {
                    className: 'state-change-button',
                    disabled: !this.props.anySelected,
                    icon: TrashIcon,
                    label: translate('DELETE FOREVER'),
                    onClick: () => c.purgeClicked() // Zero arguments means purge all checked reports.
                })
            );
            stateChangeButtons[platform.isPhone() ? 'unshift' : 'push'](
                e('div', {
                    className: 'auto-delete-container',
                    children: [
                        e(InfoIcon, {
                            className: 'auto-delete-icon',
                            color: '#309bff'
                        }),
                        e('span', {
                            className: 'auto-delete-text',
                            children: translate('Reports in the trash will automatically delete after 30 days.')
                        })
                    ]
                })
            );
        } else {
            stateChangeButtons.push(
                e(IconLabelButton, {
                    className: 'state-change-button',
                    disabled: !this.props.anySelected,
                    icon: ArchiveIcon,
                    label: translate('ARCHIVE'),
                    onClick: c.archiveClicked
                }),
                this.renderDeleteButton()
            );
        }
        
        if (this.props.reports.length > 0) {
            let SortIcon = m.sortDirection === 'asc' ? SortUpIcon : SortDownIcon;
            
            let headerChildren = [
                e('th', {
                    className: 'reports-all-column',
                    children: [
                        e('div', {
                            id: 'reports-all-container',
                            children: [
                                e('input', {
                                    className: 'vertical-align-middle',
                                    type: 'checkbox',
                                    id: 'reports-all-check',
                                    checked: m.allChecked,
                                    onChange: c.allChanged
                                }),
                                e('label', {
                                    className: 'vertical-align-middle reports-all-label',
                                    htmlFor: 'reports-all-check',
                                    children: translate('ALL')
                                })
                            ]
                        })
                    ]
                }),
                e('th', {
                    className: m.state === 'trashed' ? 'reports-name-trash-column' : 'reports-name-column',
                    children: [
                        e('span', {
                            className: 'reports-sort-header' + (m.sortColumn === 'name' ? ' reports-active-sort-header' : ''),
                            children: [
                                e('span', {
                                    className: 'reports-column-header-text',
                                    children: translate('NAME')
                                }),
                                e(SortIcon, {
                                    className: 'reports-sort-icon' + (m.sortColumn === 'name' ? ' reports-active-sort-icon' : '')
                                })
                            ],
                            title: translate('Sort by name'),
                            onClick: () => c.columnClicked('name')
                        })
                    ]
                }),
                e('th', {
                    className: m.state === 'trashed' ? 'reports-date-trash-column' : 'reports-date-column',
                    children: [
                        e('span', {
                            className: 'reports-sort-header' + (m.sortColumn === 'date' ? ' reports-active-sort-header' : ''),
                            children: [
                                e('span', {
                                    className: 'reports-column-header-text',
                                    children: translate('DATE')
                                }),
                                e(SortIcon, {
                                    className: 'reports-sort-icon' + (m.sortColumn === 'date' ? ' reports-active-sort-icon' : '')
                                })
                            ],
                            title: translate('Sort by date'),
                            onClick: () => c.columnClicked('date')
                        })
                    ]
                }),
                e('th', {
                    className: 'reports-room-column',
                    children: [
                        e('span', {
                            className: 'reports-sort-header' + (m.sortColumn === 'room' ? ' reports-active-sort-header' : ''),
                            children: [
                                e('span', {
                                    className: 'reports-column-header-text',
                                    children: translate('CLASSROOM')
                                }),
                                e(SortIcon, {
                                    className: 'reports-sort-icon' + (m.sortColumn === 'room' ? ' reports-active-sort-icon' : '')
                                })
                            ],
                            title: translate('Sort by classroom'),
                            onClick: () => c.columnClicked('room')
                        })
                    ]
                }),
                e('th', {
                    className: 'reports-type-column',
                    children: [
                        e('span', {
                            className: 'reports-sort-header' + (m.sortColumn === 'type' ? ' reports-active-sort-header' : ''),
                            children: [
                                e('span', {
                                    className: 'reports-column-header-text',
                                    children: translate('TYPE')
                                }),
                                e(SortIcon, {
                                    className: 'reports-sort-icon' + (m.sortColumn === 'type' ? ' reports-active-sort-icon' : '')
                                })
                            ],
                            title: translate('Sort by type'),
                            onClick: () => c.columnClicked('type')
                        })
                    ]
                })
            ];
            
            if (m.state === 'trashed') {
                if (platform.isMobile()) {
                    headerChildren.push(
                        e('th', {
                            className: 'reports-more-column',
                            children: ''
                        })
                    );
                } else {
                    headerChildren.push(
                        e('th', {
                            className: 'reports-restore-column',
                            children: translate('RESTORE')
                        }),
                        e('th', {
                            className: 'reports-purge-column',
                            children: translate('DELETE FOREVER')
                        })
                    );
                }
            }
            
            let tableChildren = [
                e('tr', {
                    children: headerChildren
                })
            ];
            
            for (let report of this.props.reports) {
                let nameChildren = [
                    e('span', {
                        className: 'link',
                        children: report.name,
                        title: translate('View Report'),
                        onClick: () => c.viewReportClicked(report.activityId, report.quizId, report.type)
                    })
                ];
                
                let nameSubChildren = [],
                    dateText = '{0}/{1}/{2}'.format(report.date.getMonth() + 1, report.date.getDate(), report.date.getFullYear() - 2000);
                
                if (m.state === 'trashed') {
                    if (platform.isMobile()) {
                        nameSubChildren.push(
                            e('span', {
                                className: 'date-sub-text',
                                children: dateText + (report.daysRemaining ? ' | ' : '')
                            })
                        );
                    }
                    
                    nameSubChildren.push(
                        e('span', {
                            className: report.daysRemaining > 3 ? 'days-remaining-blue' : 'days-remaining-red',
                            children: report.daysRemaining + ' ' + (report.daysRemaining > 1 ? translate('days') : translate('day'))
                        })
                    );
                } else {
                    nameSubChildren.push(
                        e('span', {
                            className: 'standard-sub-text',
                            children: report.standard
                        })
                    );
                }
                
                if (nameSubChildren.length > 0) {
                    nameChildren.push(
                        e('div', {
                            className: 'name-sub-text',
                            children: nameSubChildren
                        })
                    );
                }
                
                let hours = report.date.getHours(),
                    ampm = hours >= 12 ? 'PM' : 'AM';
                
                if (hours > 12) {
                    hours -= 12;
                }
                
                let minutes = report.date.getMinutes();
                
                if (minutes < 10) {
                    minutes = '0' + minutes;
                }
                
                let dateChildren = [
                    e('span', {
                        className: 'reports-date-text',
                        children: dateText
                    }),
                    e('span', {
                        className: 'reports-time-text',
                        children: '{0}:{1} {2}'.format(hours, minutes, ampm)
                    })
                ];
                
                let cellChildren = [
                    e('td', {
                        className: 'reports-checkbox',
                        children: [
                            e('input', {
                                className: 'vertical-align-middle',
                                type: 'checkbox',
                                checked: report.selected,
                                onChange: () => c.reportToggled(report.activityId)
                            })
                        ]
                    }),
                    e('td', {
                        className: m.state === 'trashed' ? 'reports-name-trash-column' : 'reports-name-column',
                        children: nameChildren
                    }),
                    e('td', {
                        className: m.state === 'trashed' ? 'reports-date-trash-column' : 'reports-date-column',
                        children: dateChildren
                    }),
                    e('td', {
                        className: 'reports-room-column',
                        children: report.room
                    }),
                    e('td', {
                        className: 'reports-type-column',
                        children: report.displayType
                    })
                ];
                
                if (m.state === 'trashed') {
                    if (platform.isMobile()) {
                        cellChildren.push(
                            e('td', {
                                className: 'reports-more-column',
                                children: [
                                    e(MoreButton, {
                                        className: 'reports-more-button',
                                        arrowClass: 'reports-more-menu-arrow',
                                        menuClass: 'reports-more-menu',
                                        menuItems: [
                                            e('div', {
                                                className: 'reports-more-menu-item',
                                                children: [
                                                    e(RestoreIcon, {
                                                        className: 'more-restore-icon'
                                                    }),
                                                    e('span', {
                                                        className: 'more-menu-text',
                                                        children: translate('RESTORE')
                                                    })
                                                ],
                                                onClick: () => c.restoreClicked(report.activityId)
                                            }),
                                            e('div', {
                                                className: 'reports-more-menu-item',
                                                children: [
                                                    e(TrashIcon, {
                                                        className: 'more-purge-icon',
                                                        color: '#8cb4d2'
                                                    }),
                                                    e('span', {
                                                        className: 'more-menu-text',
                                                        children: translate('DELETE FOREVER')
                                                    })
                                                ],
                                                onClick: () => c.purgeClicked(report.activityId)
                                            })
                                        ]
                                    })
                                ]
                            })
                        );
                    } else {
                        cellChildren.push(
                            e('td', {
                                className: 'reports-restore-column',
                                children: [
                                    e('div', {
                                        className: 'restore-report-icon-button',
                                        children: [
                                            e(RestoreIcon, {
                                                className: 'restore-report-icon'
                                            })
                                        ],
                                        title: translate('Restore Report'),
                                        onClick: () => c.restoreClicked(report.activityId)
                                    })
                                ]
                            }),
                            e('td', {
                                className: 'reports-purge-column',
                                children: [
                                    e('div', {
                                        className: 'purge-report-icon-button',
                                        children: [
                                            e(TrashIcon, {
                                                className: 'purge-report-icon',
                                                color: '#8cb4d2'
                                            })
                                        ],
                                        title: translate('Delete Report Forever'),
                                        onClick: () => c.purgeClicked(report.activityId)
                                    })
                                ]
                            })
                        );
                    }
                }
                
                tableChildren.push(
                    e('tr', {
                        children: cellChildren
                    })
                );
            }
            
            if (m.hasSearched && m.searchText.length > 0) {
                let resultsText = m.totalResults === 1 ? translate('{0} result for "{1}"') : translate('{0} results for "{1}"');
                
                reportsChildren = [
                    e('span', {
                        className: 'search-results-for-text',
                        children: resultsText.format(m.totalResults, m.searchText)
                    })
                ];
            }
            
            reportsChildren.push(
                e('div', {
                    className: 'state-change-buttons',
                    children: stateChangeButtons
                }),
                e('table', {
                    className: 'reports-table' + (m.totalResults > Constants.REPORTS_SEARCH_LIMIT ? ' reports-table-with-pagination' : ''),
                    children: [
                        e('tbody', {
                            children: tableChildren
                        })
                    ]
                })
            );
        }
        
        let displayState = utils.capitalize(m.state);
        
        if (displayState === 'Trashed') {
            displayState = translate('Trash');
        } else if (displayState === 'Deleted_rooms') {
            displayState = translate('From Deleted Classrooms');
        }
        
        if (m.state !== 'deleted_rooms' &&
            m.state !== 'active' &&
            m.state !== 'archived' &&
            m.state !== 'trashed') {
            displayState = displayState.toUpperCase();
        }
        
        displayState = utils.ellipsify(displayState, 16);
        
        let searchContainerChildren = [];
        
        if (m.showBanner && m.bannerText) {
            searchContainerChildren.push(
                e('div', {
                    className: 'reports-banner',
                    children: [
                        e(CheckIcon, {
                            color: '#ffffff',
                            svgClass: 'reports-banner-icon'
                        }),
                        e('span', {
                            className: 'reports-banner-text',
                            children: m.bannerText
                        }),
                        e('span', {
                            className: 'reports-banner-sub-text',
                            children: m.bannerSubText
                        })
                    ]
                })
            );
        }
        
        let searchInputPlaceholder = platform.isPhone() ? translate('Search') : translate('Search {0}').format(displayState);
        
        searchContainerChildren.push(
            e(SearchIcon, {
                className: 'reports-search-icon'
            }),
            e(Input, {
                id: 'reports-search-input',
                maxLength: 256,
                onChange: (event) => c.searchTextChanged(event.target.value),
                onKeyPress: (event) => {
                    if (event.key === 'Enter') {
                        if (platform.isMobile()) {
                            event.target.blur();
                        }
                        c.searchClicked();
                    }
                },
                placeholder: searchInputPlaceholder,
                type: 'text',
                value: m.searchText || ''
            })
        );
        
        if (m.searchText.length > 0) {
            searchContainerChildren.push(
                e('div', {
                    className: 'clear-search',
                    children: [
                        e(CloseX, {
                            className: 'clear-search-x',
                            color: Constants.CLEAR_INPUT_X
                        })
                    ],
                    onClick: c.clearSearchClicked
                })
            );
        }
        
        let roomsReports = [],
            deletedRoomsCount = 0,
            roomNames = m.roomNames;
        
        for (let room in roomNames) {
            if (roomNames.hasOwnProperty(room)) {
                let title = room;
                if (title === 'deleted_rooms') {
                    deletedRoomsCount = roomNames[room];
                    continue;
                } else {
                    title = title.toUpperCase();
                    title = utils.ellipsify(title, 20);
                }
                title += ' ({0})'.format(roomNames[room]);
                roomsReports.push({title: title, value: room, selected: room === m.state});
            }
        }
        
        roomsReports.sort(function(a, b) {
            if (a.title > b.title) {
                return 1;
            }
            if (a.title < b.title) {
                return -1;
            }
            return 0;
        });
        
        let roomsGroup = {
            children: roomsReports,
            selectable: false,
            title: translate('Classrooms')
        };
        
        let activeText       = translate('Active')   + (m.activeTotal   !== -1 ? (' ({0})'.format(m.activeTotal))   : ''),
            archivedText     = translate('Archived') + (m.archivedTotal !== -1 ? (' ({0})'.format(m.archivedTotal)) : ''),
            deletedRoomsText = translate('From Deleted Classrooms ({0})').format(deletedRoomsCount),
            trashedText      = translate('Trash')    + (m.trashedTotal  !== -1 ? (' ({0})'.format(m.trashedTotal))  : '');
        
        let allGroup = {
            children: [
                {title: activeText,       value: 'active',        selected: 'active'        === m.state},
                {title: archivedText,     value: 'archived',      selected: 'archived'      === m.state},
                {title: deletedRoomsText, value: 'deleted_rooms', selected: 'deleted_rooms' === m.state},
                {title: trashedText,      value: 'trashed',       selected: 'trashed'       === m.state}
            ],
            selectable: false,
            title: translate('All')
        };
        
        let filterOptions = [
            roomsGroup,
            allGroup
        ];
        
        searchContainerChildren.push(
            e('button', {
                className: 'button button-large button-secondary reports-search-button',
                children: translate('SEARCH'),
                onClick: c.searchClicked
            }),
            e('div', {
                className: 'reports-filter-container',
                children: [
                    e('span', {
                        className: 'filter-by-text',
                        children: translate('Filter by')
                    }),
                    e(Select, {
                        children: filterOptions,
                        disabled: m.fetchingReports,
                        onSelect: (name, value) => c.stateChanged(value)
                    })
                ]
            })
        );
        
        let needPagination = m.totalResults > Constants.REPORTS_SEARCH_LIMIT &&
                            !m.fetchingReports &&
                             this.props.reports.length > 0 &&
                             Math.ceil(m.totalResults / Constants.REPORTS_SEARCH_LIMIT) > 1;
        
        let allChildren = [
            e('div', {
                id: 'reports-inner-container',
                className: 'form' + (needPagination ? (' reports-with-pages' + '') : ''),
                children: [
                    e('div', {
                        children: [
                            e('span', {
                                className: 'reports-header-text',
                                children: translate('Reports')
                            })
                        ]
                    }),
                    e('div', {
                        className: 'reports-search-bar',
                        children: [
                            e('div', {
                                className: 'reports-search-container',
                                children: searchContainerChildren
                            })
                        ]
                    }),
                    reportsChildren
                ]
            })
        ];
        
        if (needPagination) {
            allChildren.push(
                e(Pagination, {
                    currentPage: m.page,
                    itemsPerPage: Constants.REPORTS_SEARCH_LIMIT,
                    nextClicked: c.nextClicked,
                    pageClicked: c.pageClicked,
                    previousClicked: c.previousClicked,
                    totalItems: m.totalResults
                })
            );
        }
        
        return e('div', {
            id: 'reports-outer-container',
            children: allChildren
        });
    }
    
}
