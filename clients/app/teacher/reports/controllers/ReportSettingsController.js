import React from 'react';
import ReportSettingsModel from 'ReportSettingsModel';
import view from 'ReportSettingsView';
import dots from 'Dots';
import popup from 'PopupController';
import modal from 'ModalController';
import platform from 'Platform';
import {account} from 'TeacherModel';
import {translate} from 'translator';

let router = null,
    model = null;

class ReportSettingsController {
    
    open(props) {
        router = props.router;
        
        model = new ReportSettingsModel();
        model.activityId = props.activityId;
        model.quizId = props.quizId;
        model.activityRunning = props.activityRunning;
        model.allowViewChart = props.allowViewChart;
        model.allowPdf = props.allowPdf;
        model.exitCallback = props.exitCallback;
        
        let popupContainer = document.createElement('div');
        popupContainer.id = 'reports-popup';
        document.body.appendChild(popupContainer);
        
        controller.renderView();
    }
    
    /**
     * This method is intended for internal use only. External users of this class
     * who want to show the report settings dialog should call show().
     */
    renderView() {
        modal.render({
            title: model.activityRunning ? translate('Select an option below to end the activity and save the reports.') : translate('Select an option below to save the reports.'),
            modalWidth: '600',
            content: React.createElement(
                view, {
                    controller: controller,
                    model: model,
                    isGoogleUser: account.googleUser
                }
            )
        });
    }
    
    cancelClicked() {
        dots.stop();
        modal.closeModal();
    }
    
    getReportsClicked() {
        if (!model.expanded) {
            model.expanded = true;
            controller.renderView();
        }
    }

    toggleReportsSettings() {
         if (model.expanded) {
            model.expanded = false;
            controller.renderView();
        }
    }
    
    viewChartClicked() {
        if (model.allowViewChart) {
            controller.cancelClicked();
            
            if (model.exitCallback) {
                model.exitCallback(`final-results/${model.activityId}/${model.quizId}/table`);
            } else {
                router.routeToFinalResultsTable({
                    activityId: model.activityId,
                    quizId: model.quizId
                });
            }
        }
    }
    
    toLaunchClicked() {
        if (model.activityRunning) {
            controller.cancelClicked();
            
            if (model.exitCallback) {
                model.exitCallback();
            } else {
                router.routeToLaunch();
            }
        }
    }
    
    excelClicked() {
        model.excelSelected = !model.excelSelected;
        controller.renderView();
    }
    
    studentPdfClicked() {
        if (model.allowPdf) {
            model.studentPdfSelected = !model.studentPdfSelected;
            controller.renderView();
        }
    }
    
    questionPdfClicked() {
        if (model.allowPdf) {
            model.questionPdfSelected = !model.questionPdfSelected;
            controller.renderView();
        }
    }
    
    emailClicked() {
        controller.sendReport('em');
    }
    
    sendReport(type) {
        if (model.anyReportSelected) {
            if (model.exitCallback) {
                model.exitCallback();
            }
            
            setTimeout(function() {
                model.sendReport(type, controller.reportSent);
                controller.cancelClicked();
            }, 500);
        }
    }
    
    reportSent(success, type) {
        if (success) {
            if (type === 'em') {
                popup.render({
                    title: translate('Sending'),
                    message: translate('You should receive a report email in the next few minutes.')
                });
            } else if (type === 'go') {
                popup.render({
                    title: translate('Added'),
                    message:translate('A report has been added to your Google Drive.')
                });
            }
        } else {
            popup.render({
                title: translate('Unknown Error'),
                message: translate('An error occurred. Please check your connection and try again.')
            });
        }
    }
    
    downloadClicked() {
        if (model.anyReportSelected && !platform.isIos) {
            
            if (model.exitCallback) {
                model.exitCallback('', controller.startDownload);
            } else {
                controller.startDownload();
            }
        }
    }
    
    startDownload() {
        let hiddenIFrameID = 'reportDownloader',
        iframe = document.getElementById(hiddenIFrameID);
        
        if (iframe === null) {
            iframe = document.createElement('iframe');
            iframe.id = hiddenIFrameID;
            iframe.style.display = 'none';
            document.body.appendChild(iframe);
        }
        
        iframe.src = `${window.backend_host}/quizzes/api/activity-report/?${model.getRequestParams('dl')}`;
        
        controller.cancelClicked();
    }
    
    googleDriveClicked() {
        if (account.googleUser) {
            controller.sendReport('go');
        }
    }
    
}

let controller = new ReportSettingsController();
module.exports = controller;
