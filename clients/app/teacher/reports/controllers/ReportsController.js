import Backbone from 'backbone';
import React from 'react';
import ReactDOM from 'react-dom';
import header from 'HeaderController';
import ReportsModel from 'ReportsModel';
import view from 'ReportsView';
import popup from 'PopupController';
import reportSettings from 'ReportSettingsController';
import Constants from 'Constants';
import {translate} from 'translator';

let router = null,
    model = null;

class ReportsController {
    
    doReports(teacherRouter) {
        router = teacherRouter;
        header.render('reports');
        model = new ReportsModel();
        model.fetchReports(1, controller.renderView);
        controller.renderView();
    }
    
    renderView() {
        if (Backbone.history.fragment === 'reports') {
            ReactDOM.render(
                React.createElement(
                    view, {
                        controller: controller,
                        model: model,
                        
                        // Pass these method calls as properties so
                        // they only execute once per render:
                        anySelected: model.anySelected(),
                        reports: model.getVisibleReports()
                    }
                ),
                document.getElementById('main-content')
            );
        }
    }
    
    searchTextChanged(searchText) {
        model.searchText = searchText;
        
        if (searchText.length === 0 && model.hasSearched) {
            model.hasSearched = false;
            model.lastSearchText = '';
            model.page = 1;
            model.resetReports();
            model.fetchReports(1, controller.renderView);
        }
        
        controller.renderView();
    }
    
    clearSearchClicked() {
        model.hasSearched = true;
        controller.searchTextChanged('');
        let searchInput = document.getElementById('reports-search-input');
        if (searchInput) {
            searchInput.focus();
        }
    }
    
    searchClicked() {
        if (model.searchText.length > 0) {
            model.hasSearched = true;
            model.page = 1;
            model.resetReports();
            model.fetchReports(1, controller.renderView);
            controller.renderView();
        }
    }
    
    stateChanged(state) {
        if (state !== model.state) {
            model.state = state;
            model.page = 1;
            model.resetReports();
            model.fetchReports(1, controller.renderView);
            controller.renderView();
        }
    }
    
    archiveClicked() {
        let selectedReports = model.getSelectedReports();
        if (selectedReports.length === 0) {
            return;
        }

        let totalReports = model.activeTotal;

        if (model.checkRoomTotal(model.state)) {
            totalReports = model.roomTotal;
        }

        // If there is a filter applied, set the max number to the filter's total results.
        if (model.totalResults) {
            totalReports = model.totalResults;
        }

        if (Math.ceil((totalReports - selectedReports.length) / Constants.REPORTS_SEARCH_LIMIT) < model.page) {
            // If we're archiving enough reports that we can't stay on the same page afterward, decrement the page number.
            model.page = model.page - 1 === 0 ? 1 : model.page - 1;
        }
        model.changeState(selectedReports, 'archived', controller.stateChangeCompleted);
        model.waitingText = translate('Archiving...');
        model.resetReports();
        controller.renderView();
        
        // Set up the banner for the next view render.
        model.bannerText = translate('Successfully Archived!');
        model.bannerSubText = '';
    }
    
    unarchiveClicked() {
        let selectedReports = model.getSelectedReports();
        if (selectedReports.length === 0) {
            return;
        }
        if (Math.ceil((model.archivedTotal - selectedReports.length) / Constants.REPORTS_SEARCH_LIMIT) < model.page) {
            // If we're unarchiving enough reports that we can't stay on the same page afterward, decrement the page number.
            model.page = model.page - 1 === 0 ? 1 : model.page - 1;
        }
        model.changeState(selectedReports, 'active', controller.stateChangeCompleted);
        model.waitingText = translate('Unarchiving...');
        model.resetReports();
        controller.renderView();
        
        // Set up the banner for the next view render.
        model.bannerText = translate(translate('Successfully Restored!'));
        model.bannerSubText = selectedReports.length > 1 ? translate('The reports are now active.') : translate('The report is now active.');
    }
    
    deleteClicked() {
        let selectedReports = model.getSelectedReports();
        if (selectedReports.length === 0) {
            return;
        }

        let totalReports = model.state === 'active' ? model.activeTotal : model.archivedTotal;

        if (model.checkRoomTotal(model.state)) {
            totalReports = model.roomTotal;
        }

        // If there is a filter applied, set the max number to the filter's total results.
        if (model.totalResults) {
            totalReports = model.totalResults;
        }

        if (Math.ceil((totalReports - selectedReports.length) / Constants.REPORTS_SEARCH_LIMIT) < model.page) {
            // If we're deleting enough reports that we can't stay on the same page afterward, decrement the page number.
            model.page = model.page - 1 === 0 ? 1 : model.page - 1;
        }
        model.changeState(selectedReports, 'trashed', controller.stateChangeCompleted);
        model.waitingText = translate('Deleting...');
        model.resetReports();
        controller.renderView();
        
        // Set up the banner for the next view render.
        model.bannerText = translate('Successfully Deleted!');
        model.bannerSubText = selectedReports.length > 1 ? translate('The reports are now in your trash.') : translate('The report is now in your trash.');
    }
    
    restoreClicked(activityId) {
        let selectedReports = activityId ? [model.getReportByActivityId(activityId)] : model.getSelectedReports();
        
        if (selectedReports.length === 0) {
            return;
        }
        
        if (Math.ceil((model.trashedTotal - selectedReports.length) / Constants.REPORTS_SEARCH_LIMIT) < model.page) {
            // If we're restoring enough reports that we can't stay on the same page afterward, decrement the page number.
            model.page = model.page - 1 === 0 ? 1 : model.page - 1;
        }
        
        model.changeState(selectedReports, 'active', controller.stateChangeCompleted);
        model.waitingText = translate('Restoring...');
        model.resetReports();
        controller.renderView();
        
        // Set up the banner for the next view render.
        model.bannerText = translate('Successfully Restored!');
        model.bannerSubText = selectedReports.length > 1 ? translate('The reports are now active.') : translate('The report is now active.');
    }
    
    purgeClicked(activityId) {
        let selectedReports = activityId ? [model.getReportByActivityId(activityId)] : model.getSelectedReports();
        
        if (selectedReports.length === 0) {
            return;
        }
        
        let message = translate('Are you sure you want to permanently delete this report?');
        
        if (selectedReports.length > 1) {
            message = translate('Are you sure you want to permanently delete these reports?');
        }
        
        popup.render({
            title: translate('Please Confirm'),
            message: message,
            buttonText: translate('Yes'),
            cancelText: translate('No'),
            buttonClicked: () => {
                controller.purgeConfirmed(selectedReports);
            }
        });
    }
    
    purgeConfirmed(selectedReports) {
        if (Math.ceil((model.trashedTotal - selectedReports.length) / Constants.REPORTS_SEARCH_LIMIT) < model.page) {
            // If we're purging enough reports that we can't stay on the same page afterward, decrement the page number.
            model.page = model.page - 1 === 0 ? 1 : model.page - 1;
        }
        model.purgeReports(selectedReports, controller.stateChangeCompleted);
        model.waitingText = translate('Deleting Forever...');
        model.resetReports();
        controller.renderView();
        
        // Set up the banner for the next view render.
        model.bannerText = translate('Successfully Deleted Forever.');
        model.bannerSubText = '';
    }
    
    stateChangeCompleted(success) {
        if (success && model.bannerText) {
            model.showBanner = true;
            controller.renderView();
            controller.startBannerCountdown();
        }
        
        model.fetchingReports = false;
        model.fetchReports(model.page, controller.renderView);
    }
    
    startBannerCountdown() {
        if (model.bannerTimeoutId) {
            window.clearTimeout(model.bannerTimeoutId);
            model.bannerTimeoutId = null;
        }
        
        model.bannerTimeoutId = window.setTimeout(function() {
            model.bannerText = '';
            model.showBanner = false;
            controller.renderView();
        }, Constants.BANNER_TIMEOUT * 1.5);
    }
    
    columnClicked(columnName) {
        if (columnName === model.sortColumn) {
            model.toggleSortDirection();
        }
        
        model.sortColumn = columnName;
        controller.renderView();
    }
    
    allChanged() {
        model.setAllChecked(!model.allChecked);
        controller.renderView();
    }
    
    reportToggled(activityId) {
        model.toggleSelected(activityId);
        controller.renderView();
    }
    
    viewReportClicked(activityId, quizId, type) {
        reportSettings.open({
            router: router,
            activityId: activityId,
            quizId: quizId,
            activityRunning: false,
            allowViewChart: type !== 'short_answer',
            allowPdf: type !== 'short_answer'
        });
    }
    
    previousClicked() {
        // The pagination component will not invoke this callback if the new page number would be less than 1.
        controller.pageClicked(model.page - 1);
    }
    
    nextClicked() {
        // The pagination component will not invoke this callback if the new page number would be greater than total number of pages.
        controller.pageClicked(model.page + 1);
    }
    
    pageClicked(newPage) {
        model.setAllChecked(false);
        model.page = newPage;
        
        if (!model.reports[model.page]) {
            model.fetchingReports = false;
            model.fetchReports(model.page, controller.renderView);
        }
        
        controller.renderView();
    }
    
}

let controller = new ReportsController();
module.exports = controller;
