import React from 'react';

let e = React.createElement;

export default class LaunchButton extends React.Component {
    
    render() {
        let circleChildren = this.props.content;
        
        if (this.props.icon) {
            circleChildren = e(this.props.icon, {
                svgClass: this.props.svgClass,
                pathClass: this.props.pathClass
            });
        }
        
        return e('div', {
            className: 'launch-button-container' + (this.props.className ? (' ' + this.props.className) : ''),
            children: [
                e('div', {
                    className: 'launch-button-circle',
                    children: circleChildren
                }),
                e('span', {
                    className: 'launch-button-label',
                    children: this.props.label
                })
            ],
            onClick: this.props.onClick
        });
    }
    
}
