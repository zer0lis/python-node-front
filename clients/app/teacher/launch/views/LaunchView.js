import React from 'react';
import LaunchButton from 'LaunchButton';
import LaunchQuizIcon from 'LaunchQuizIcon';
import LaunchSpaceRaceIcon from 'LaunchSpaceRaceIcon';
import LaunchExitTicketIcon from 'LaunchExitTicketIcon';
import {translate} from 'translator';

let e = React.createElement;

export default class LaunchView extends React.Component {
    
    render() {
        let c = this.props.controller;
        
        return e('div', {
            id: 'launch-container',
            children: [
                e('div', {
                    className: 'launch-button-row-1',
                    children: [
                        e(LaunchButton, {
                            icon: LaunchQuizIcon,
                            svgClass: 'launch-quiz-icon',
                            pathClass: 'launch-quiz-icon-path',
                            label: translate('Quiz'),
                            onClick: c.quizClicked
                        }),
                        e(LaunchButton, {
                            icon: LaunchSpaceRaceIcon,
                            svgClass: 'launch-space-race-icon',
                            pathClass: 'launch-space-race-icon-path',
                            label: translate('Spaceship Game'),
                            onClick: c.spaceRaceClicked
                        }),
                        e(LaunchButton, {
                            icon: LaunchExitTicketIcon,
                            svgClass: 'launch-exit-ticket-icon',
                            pathClass: 'launch-exit-ticket-icon-path',
                            label: translate('Final Survey'),
                            onClick: c.exitTicketClicked
                        })
                    ]
                }),
                e('div', {
                    className: 'launch-button-row-2',
                    children: [
                        e('div', {
                            className: 'quick-question-separator',
                            children: [
                                e('span', {
                                    className: 'quick-question-separator-text',
                                    children: translate('POLLS')
                                })
                            ]
                        }),
                        e(LaunchButton, {
                            className: 'mc-button',
                            content: 'MC',
                            label: translate('Multiple Choice'),
                            onClick: c.mcClicked
                        }),
                        e(LaunchButton, {
                            className: 'tf-button',
                            content: 'TF',
                            label: translate('True / False'),
                            onClick: c.tfClicked
                        }),
                        e(LaunchButton, {
                            className: 'sa-button',
                            content: 'SA',
                            label: translate('Short Answer'),
                            onClick: c.saClicked
                        })
                    ]
                })
            ]
        });
    }
    
}
