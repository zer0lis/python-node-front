import React from 'react';
import Input from 'Input';
import Toggle from 'Toggle';
import HelpTooltip from 'HelpTooltip';
import {translate} from 'translator';

let e = React.createElement;

export default class ShortAnswerView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            namesTooltip = null,
            oneAttemptTooltip = null;
        
        if (m.showNamesTooltip) {
            namesTooltip = {
                location: 'above',
                title: translate('Required'),
                text: translate('Student names must be enabled for classrooms with rosters.')
            }
        }

        if (m.showOneAttemptTooltip) {
            let title = translate('PRO Feature'),
                text = translate('This setting is only available in classrooms with rosters.');

            if (this.props.isPro && !this.props.hasRoster) {
                title = translate('Roster Required');
            }

            oneAttemptTooltip = {
                location: 'above',
                title: title,
                text: text
            }
        }

        return e('div', {
            id: 'sa-popup-bg',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'sa-popup-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('span', {
                                    className: 'popup-title',
                                    children: translate('Short Answer')
                                }),
                                e('a', {
                                    className: 'cancel-button',
                                    title: translate('Close'),
                                    children: [
                                        e('i', {
                                            className: 'close-popup',
                                            'data-icon': 'h'
                                        })
                                    ],
                                    onClick: c.cancelClicked
                                })
                            ]
                        }),
                        e('div', {
                            id: 'sa-popup-content',
                            className: 'form',
                            children: [
                                e(Input, {
                                    error: false,
                                    hideTooltip: true,
                                    id: 'saQuestion',
                                    label: translate('Optional Question'),
                                    maxLength: 65000,
                                    onChange: (event) => c.questionChanged(event.target.value),
                                    value: m.question
                                }),
                                e(Toggle, {
                                    id: 'sa-allow-unlimited-responses',
                                    label: translate('Allow unlimited responses'),
                                    checked: m.allowRepeats,
                                    onChange: c.allowRepeatsChanged
                                }),
                                e(Toggle, {
                                    id: 'sa-require-student-names',
                                    label: translate('Require student names'),
                                    checked: m.requireNames,
                                    disabled: this.props.hasRoster,
                                    onChange: c.requireNamesChanged,
                                    onClick: c.requireNamesClicked,
                                    tooltip: namesTooltip
                                }),
                                e(Toggle, {
                                    id: 'sa-one-attempt-activity',
                                    label: translate('One Attempt'),
                                    checked: m.oneAttempt,
                                    disabled: !this.props.hasRoster,
                                    onChange: c.oneAttemptChanged,
                                    onClick: c.oneAttemptClicked,
                                    tooltip: oneAttemptTooltip,
                                    children: e(HelpTooltip, {
                                        className: 'short-answer-help-tooltip',
                                        above: true,
                                        showProIcon: !this.props.isPro,
                                        title: this.props.isPro ? translate('One Attempt') : translate('Pro Feature'),
                                        text:  translate('Students can only take the quiz once. If they leave and rejoin the classroom, they will continue the quiz where they left off.')
                                    })
                                })
                            ]
                        }),
                        e('div', {
                            className: 'modal-footer',
                            children: [
                                e('button', {
                                    id: 'sa-start-button',
                                    className: 'action-button action-button-200',
                                    children: translate('START'),
                                    onClick: c.startClicked
                                }),
                                e('button', {
                                    className: 'secondary-action-button action-button-200',
                                    children: translate('CANCEL'),
                                    onClick: c.cancelClicked
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
