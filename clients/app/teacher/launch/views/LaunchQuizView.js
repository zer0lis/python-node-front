import React from 'react';
import _ from 'underscore';
import CloseX from 'CloseX';
import Loader from 'Loader';
import SearchIcon from 'SearchIcon';
import Input from 'Input';
import BackArrowIcon from 'BackArrowIcon';
import ListFolder from 'ListFolder';
import Select from 'Select';
import SortDownIcon from 'SortDownIcon';
import SortUpIcon from 'SortUpIcon';
import NoResultsIcon from 'NoResultsIcon';
import EmptyFolderIcon from 'EmptyFolderIcon';
import FeedbackIcon from 'FeedbackIcon';
import StudentNavIcon from 'StudentNavIcon';
import TeacherPacedIcon from 'TeacherPacedIcon';
import Toggle from 'Toggle';
import HelpTooltip from 'HelpTooltip';
import RadioButton from 'RadioButton';
import Tooltip from 'Tooltip';
import utils from 'Utils';
import platform from 'Platform';
import Constants from 'Constants';
import {translate} from 'translator';

const MAIN_HEADER = 49;
const STEP_HEADER = 60;
const FILTER_BAR  = 68;
const MAIN_FOOTER = 56;

let e = React.createElement,
    c = null,
    m = null;

export default class LaunchQuizView extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {searchFocused: false};
    }
    
    createAdditionalSettings() {
        let namesTooltip = null;
        
        if (m.showNamesTooltip) {
            namesTooltip = {
                location: 'above',
                title: translate('Required'),
                text: translate('Student names are required in classrooms with rosters.')
            };
        }
        
        let feedbackTooltip = null;
        
        if (m.showFeedbackTooltip) {
            feedbackTooltip = {
                location: 'above',
                title: translate('Disabled'),
                text: translate('Feedback is disabled because students can change answers before submitting.')
            };
        }
        
        let questionsTooltip = null;
        
        if (m.showQuestionsTooltip) {
            questionsTooltip = {
                location: 'above',
                title: translate('Disabled'),
                text: translate('Questions are delivered in the same order to all students.')
            };
        }
        
        let methodTooltip = null;
        
        if (m.showMethodTooltip) {
            methodTooltip = {
                className: 'delivery-method-tooltip',
                location: 'above',
                text: translate('Please choose a delivery method first.')
            };
            namesTooltip = methodTooltip;
        }

        let oneAttempTooltip = null;
        
        if (m.showOneAttemptTooltip) {
            let title = translate('PRO Feature');
            let text = translate('This setting is only available in classrooms with rosters.');

            if(this.props.isPro && !this.props.hasRoster) {
                title = translate('Roster Required');
            }

            oneAttempTooltip = {
                location: 'above',
                title: title,
                text: text
            }
        }

        let additionalSettings = '';
        
        if (platform.isPhone()) {
            additionalSettings = e('span', {
                className: 'space-race-additional-settings-header',
                children: translate('Additional Settings')
            });
        }

        return e('div', {
            className: 'start-quiz-settings-container' + (platform.isPhone() ? '' : ' large-view'),
            children: [
                additionalSettings,
                e(Toggle, {
                    checked: m.additionalSettings.requireNames || this.props.hasRoster,
                    disabled: this.props.hasRoster || !m.deliveryMethod,
                    id: 'start-quiz-require-names',
                    label: translate('Require Names'),
                    onChange: c.requireNamesChanged,
                    onClick: c.requireNamesClicked,
                    tooltip: namesTooltip
                }),
                e(Toggle, {
                    checked: m.additionalSettings.shuffleQuestions,
                    disabled: m.deliveryMethod === 'teacherPaced' || (m.isQuiz && m.deliveryMethod === ''),
                    id: 'start-quiz-shuffle-questions',
                    label: translate('Shuffle Questions'),
                    onChange: c.shuffleQuestionsChanged,
                    onClick: c.shuffleQuestionsClicked,
                    tooltip: questionsTooltip
                }),
                e(Toggle, {
                    checked: m.additionalSettings.shuffleAnswers,
                    disabled: m.isQuiz && m.deliveryMethod === '',
                    id: 'start-quiz-shuffle-answers',
                    label: translate('Shuffle Answers'),
                    onChange: c.shuffleAnswersChanged,
                    onClick: c.shuffleAnswersClicked
                }),
                e(Toggle, {
                    checked: m.additionalSettings.showFeedback,
                    disabled: m.deliveryMethod === 'navigation' || (m.isQuiz && m.deliveryMethod === ''),
                    id: 'start-quiz-show-feedback',
                    label: translate('Show Question Feedback'),
                    onChange: c.showFeedbackChanged,
                    onClick: c.showFeedbackClicked,
                    tooltip: feedbackTooltip
                }),
                e(Toggle, {
                    checked: m.additionalSettings.showScore,
                    disabled: m.isQuiz && m.deliveryMethod === '',
                    id: 'start-quiz-show-score',
                    label: translate('Show Final Score'),
                    onChange: c.showScoreChanged,
                    onClick: c.showScoreClicked
                }),
                e(Toggle, {
                    checked: m.additionalSettings.oneAttempt,
                    disabled: !this.props.hasRoster || (m.deliveryMethod === '' && m.isQuiz),
                    id: 'start-quiz-one-attempt',
                    label: translate('One Attempt'),
                    onChange: c.oneAttemptChanged,
                    onClick: c.oneAttemptClicked,
                    tooltip: oneAttempTooltip,
                    children: e(HelpTooltip, {
                        above: true,
                        showProIcon: !this.props.isPro,
                        className: 'start-quiz-help-tooltip',
                        title: this.props.isPro ? translate('One Attempt') : translate('Pro Feature'),
                        text: translate('Students can only take the quiz once. If they leave and rejoin the classroom, they will continue the quiz where they left off.')
                    })
                })
            ]
        });
    }
    
    createDeliveryMethod(method) {
        let methodText = translate('Instant Feedback');
        if (method === 'navigation') {
            methodText = translate('Open Navigation');
        } else if (method === 'teacherPaced') {
            methodText = translate('Teacher Paced');
        }
        
        let Icon = FeedbackIcon,
            svgClass = 'feedback-svg',
            pathClass = 'feedback-path';
        
        if (method === 'navigation') {
            Icon = StudentNavIcon;
            svgClass = 'navigation-svg';
            pathClass = 'navigation-path';
        } else if (method === 'teacherPaced') {
            Icon = TeacherPacedIcon;
            svgClass = 'teacher-paced-svg';
            pathClass = 'teacher-paced-path';
        }
        
        let tooltipTitle = translate('Instant Feedback');
        let tooltipText = translate('Students answer questions in order and cannot change answers. Instant feedback is provided after each question. You monitor progress in a table of live results.');
        
        if (method === 'navigation') {
            tooltipTitle = translate('Open Navigation');
            tooltipText = translate('Students may answer questions in any order and change answers before finishing. You monitor progress in a table of live results.');
        } else if (method === 'teacherPaced') {
            tooltipTitle = translate('Teacher Paced');
            tooltipText = translate('You control the flow of questions and monitor responses as they happen. You may skip and revisit questions.');
        }
        
        let selected = m.deliveryMethod === method;
        
        return e('div', {
            className: 'delivery-method-container',
            children: [
                e('div', {
                    className: 'delivery-method-button' + (selected ? ' delivery-method-button-selected' : ''),
                    children: [
                        e('div', {
                            className: 'delivery-method-circle' + (selected ? ' delivery-method-circle-selected' : ''),
                            children: [
                                e(Icon, {
                                    svgClass: svgClass,
                                    pathClass: pathClass + (selected ? ' delivery-method-icon-selected' : '')
                                })
                            ]
                        }),
                        e('span', {
                            className: 'delivery-method-text' + (selected ? ' delivery-method-text-selected' : ''),
                            children: methodText
                        })
                    ],
                    onClick: () => {
                        if (method === 'feedback') {
                            c.feedbackClicked();
                        } else if (method === 'navigation') {
                            c.navigationClicked();
                        } else {
                            c.teacherPacedClicked();
                        }
                    }
                }),
                e(HelpTooltip, {
                    above: true,
                    className: 'start-quiz-help-tooltip',
                    title: tooltipTitle,
                    text: tooltipText
                })
            ]
        });
    }
    
    getSpaceRaceIcon(icon) {
        return e('img', {
            className: 'start-quiz-space-race-icon',
            src: `${window.static_url}img/space-race/${icon}-small.png`
        });
    }
    
    render() {
        c = this.props.controller;
        m = this.props.model;
        
        let step = m.step;
        
        let modalContentChildren = [];
        
        if (m.waiting) {
            modalContentChildren.push(
                e(Loader, {
                    className: 'start-quiz-loader',
                    text: m.waitingText
                })
            );
        } else {
            let step1Children = [],
                step2Children = [];
            
            let step1HeaderChildren = [
                e('div', {
                    className: 'step-circle ' + (step === 1 ? 'active-step-circle' : 'inactive-step-circle'),
                    children: [
                        e('span', {
                            className: 'step-circle-text',
                            children: '1'
                        })
                    ]
                }),
                e('span', {
                    className: 'step-title-text ' + (step === 1 ? 'active-step-title-text' : 'inactive-step-title-text'),
                    children: platform.isPhone() ? translate('Quiz') : translate('Choose Quiz')
                })
            ];
            
            if (step === 1) {
                step1HeaderChildren.push(
                    e('span', {
                        className: 'step-x-of-x-text',
                        dangerouslySetInnerHTML: {__html: translate('Step {0} <i>of</i> 2').format('1')}
                    })
                );
            } else {
                step1HeaderChildren.push(
                    e('div', {
                        className: 'selected-quiz-change-container',
                        children: [
                            e('span', {
                                className: 'selected-quiz-name-text',
                                children: utils.ellipsify(m.selectedQuiz.name, platform.isPhone() ? 15 : 50)
                            }),
                            e('span', {
                                className: 'selected-quiz-change-text link',
                                children: translate('Change'),
                                onClick: c.previousClicked
                            })
                        ]
                    })
                );
            }
            
            step1Children.push(
                e('div', {
                    className: 'step-header',
                    children: step1HeaderChildren
                })
            );
            
            let step2HeaderChildren = [
                // Althouh the "Step 2 of 2" text appears on the right side of the view, we render it in the
                // HTML first to avoid this Firefox bug: https://bugzilla.mozilla.org/show_bug.cgi?id=488725
                e('span', {
                    className: 'step-x-of-x-text' + (step === 1 ? ' inactive-step-x-of-x-text' : ''),
                    dangerouslySetInnerHTML: {__html: translate('Step {0} <i>of</i> 2').format('2')}
                }),
                e('div', {
                    className: 'step-circle ' + (step === 2 ? 'active-step-circle' : 'inactive-step-circle'),
                    children: [
                        e('span', {
                            className: 'step-circle-text',
                            children: '2'
                        })
                    ]
                }),
                e('span', {
                    className: 'step-title-text ' + (step === 2 ? 'active-step-title-text' : 'inactive-step-title-text'),
                    children: m.step2Title
                })
            ];
            
            step2Children.push(
                e('div', {
                    className: 'step-header step-header-2',
                    children: step2HeaderChildren
                })
            );
            
            if (step === 1) {
                let filterChildren = [
                    e(SearchIcon, {
                        className: 'start-quiz-search-icon'
                    }),
                    e(Input, {
                        autoCapitalize: 'off',
                        id: 'start-quiz-search-input',
                        maxLength: 255,
                        onFocus: () => {
                            if (window.innerWidth < 768) {
                                let footer = document.getElementById('start-quiz-footer');
                                if (footer) {
                                    footer.style.position = 'absolute'; // Prevent the footer from covering the search input.
                                }
                                this.setState({searchFocused: true});
                            }
                        },
                        onBlur: () => {
                            if (window.innerWidth < 768) {
                                setTimeout(() => {
                                    this.setState({searchFocused: false});
                                    let footer = document.getElementById('start-quiz-footer');
                                    if (footer) {
                                        footer.style.position = 'fixed';
                                    }
                                }, 375);
                            }
                        },
                        onChange: (event) => c.searchTextChanged(event.target.value),
                        onKeyPress: (event) => {
                            if (event.key === 'Enter' && platform.isMobile()) {
                                event.target.blur();
                            }
                        },
                        placeholder: translate('Search Quizzes'),
                        type: 'text',
                        value: m.searchText || ''
                    })
                ];
                
                if (m.searchText.length > 0) {
                    filterChildren.push(
                        e('div', {
                            className: 'clear-search',
                            children: [
                                e(CloseX, {
                                    className: 'clear-search-x',
                                    color: Constants.CLEAR_INPUT_X
                                })
                            ],
                            onClick: c.clearSearchClicked
                        })
                    );
                }
                
                step1Children.push(
                    e('div', {
                        className: 'start-quiz-filter-bar',
                        children: filterChildren
                    })
                );
                
                let rootFolder = m.currentFolder.id === Constants.ROOT_FOLDER,
                    folderConextChildren = [];
                
                if (!rootFolder && m.searchText.length === 0) {
                    folderConextChildren.push(
                        e('button', {
                            className: 'launch-quiz-back-button',
                            children: [
                                e(BackArrowIcon, {
                                    className: 'launch-quiz-back-button-icon'
                                })
                            ],
                            onClick: c.backClicked
                        })
                    );
                }
                
                let labelClass = 'launch-quiz-current-folder-label';
                
                if (rootFolder || m.searchText.length > 0) {
                    labelClass += ' launch-quiz-current-folder-label-root';
                }
                
                folderConextChildren.push(
                    e('span', {
                        className: labelClass,
                        children: m.searchText.length > 0 ? (translate('Search Results') + ':') : m.currentFolder.name
                    })
                );
                
                let folderConextRowClass = 'launch-quiz-folder-context-row';
                
                if (rootFolder || m.searchText.length > 0) {
                    folderConextRowClass = 'launch-quiz-folder-context-row-root';
                }
                
                step1Children.push(
                    e('div', {
                        className: folderConextRowClass,
                        children: folderConextChildren
                    })
                );
                
                let NameSortIcon = m.sortDirection === 'asc' ? SortUpIcon : SortDownIcon;
                
                if (m.sortColumn !== 'name') {
                    NameSortIcon = SortUpIcon;
                }
                
                let DateSortIcon = m.sortDirection === 'asc' ? SortUpIcon : SortDownIcon;
                
                if (m.sortColumn !== 'date') {
                    DateSortIcon = SortDownIcon;
                }
                
                let headerChildren = [
                    e('th', {
                        className: 'start-quiz-radio-column',
                        children: ''
                    }),
                    e('th', {
                        className: 'start-quiz-name-column',
                        children: [
                            e('span', {
                                className: 'start-quiz-sort-header' + (m.sortColumn === 'name' ? ' start-quiz-active-sort-header' : ''),
                                children: [
                                    e('span', {
                                        className: 'start-quiz-column-header-text',
                                        children: translate('NAME')
                                    }),
                                    e(NameSortIcon, {
                                        className: 'start-quiz-sort-icon' + (m.sortColumn === 'name' ? ' start-quiz-active-sort-icon' : '')
                                    })
                                ],
                                title: translate('Sort by name'),
                                onClick: () => c.columnClicked('name')
                            })
                        ]
                    }),
                    e('th', {
                        className: 'start-quiz-date-column',
                        children: [
                            e('span', {
                                className: 'start-quiz-sort-header' + (m.sortColumn === 'date' ? ' start-quiz-active-sort-header' : ''),
                                children: [
                                    e('span', {
                                        className: 'start-quiz-column-header-text',
                                        children: translate('DATE')
                                    }),
                                    e(DateSortIcon, {
                                        className: 'start-quiz-sort-icon' + (m.sortColumn === 'date' ? ' start-quiz-active-sort-icon' : '')
                                    })
                                ],
                                title: translate('Sort by date'),
                                onClick: () => c.columnClicked('date')
                            })
                        ]
                    })
                ];
                
                let tableChildren = [
                    e('tr', {
                        children: headerChildren
                    })
                ];
                
                let foldersToDisplay = this.props.searchResultFolders;
                
                if (m.searchText.length === 0) {
                    foldersToDisplay = [];
                    
                    if (this.props.isPro) {
                        for (let folder of m.currentFolder.children) {
                            if (!folder.deleted) {
                                foldersToDisplay.push(folder);
                            }
                        }
    
                        if (m.sortColumn === 'name') {
                            foldersToDisplay = _.sortBy(foldersToDisplay, (folder) => {
                                return folder.name.toLowerCase();
                            });
                        } else {
                            foldersToDisplay = _.sortBy(foldersToDisplay, 'date');
                        }
    
                        if (m.sortDirection === 'desc') {
                            foldersToDisplay.reverse(); // Underscore sorts ascending by default, so reverse if needed.
                        }
                    }
                }
                
                for (let folder of foldersToDisplay) {
                    tableChildren.push(
                        e('tr', {
                            className: 'launch-quiz-folder-row',
                            children: [
                                e('td', {
                                    colSpan: 3,
                                    className: 'launch-quiz-list-folder-cell',
                                    children: [
                                        e(ListFolder, {
                                            className: 'launch-quiz-list-folder',
                                            hideButton: false,
                                            id: folder.id,
                                            name: folder.name,
                                            onClick: () => c.folderClicked(folder)
                                        })
                                    ]
                                })
                            ]
                        })
                    );
                }
                
                if (this.props.visibleQuizzes.length > 0) {
                    for (let quiz of this.props.visibleQuizzes) {
                        let selected = m.selectedQuiz && m.selectedQuiz.id === quiz.id;

                        let cellChildren = [
                            e('td', {
                                className: 'start-quiz-radio-column',
                                children: [
                                    e('div', {
                                        className: 'start-quiz-radio-outer' + (selected ? ' start-quiz-radio-outer-selected' : ''),
                                        children: [
                                            e('div', {
                                                className: 'start-quiz-radio-inner',
                                                style: {visibility: selected ? 'visible' : 'hidden'}
                                            })
                                        ]
                                    })
                                ]
                            })
                        ];

                        let nameChildren = [
                            e('span', {
                                className: 'start-quiz-name-text',
                                children: quiz.name
                            })
                        ];

                        if (quiz.standard && quiz.standard.length > 0) {
                            nameChildren.push(
                                e('div', {
                                    className: 'start-quiz-standard-text',
                                    children: quiz.standard
                                })
                            )
                        }

                        cellChildren.push(
                            e('td', {
                                className: 'start-quiz-name-column',
                                children: nameChildren
                            }),
                            e('td', {
                                className: 'start-quiz-date-column',
                                children: '{0}/{1}/{2}'.format(quiz.date.getMonth() + 1, quiz.date.getDate(), quiz.date.getFullYear() - 2000)
                            })
                        );

                        tableChildren.push(
                            e('tr', {
                                className: 'start-quiz-row' + (selected ? ' selected-start-quiz-row' : ''),
                                children: cellChildren,
                                onClick: () => c.quizClicked(quiz.id, quiz.soc, quiz.name)
                            })
                        );
                    }
                }
                
                let noChildFolders = m.currentFolder.children.length === 0,
                    noSearchResultFolders = m.searchText.length > 0 && this.props.searchResultFolders.length === 0,
                    noQuizzes = this.props.visibleQuizzes.length === 0;
                
                let allChildFoldersDeleted = true;
                
                for (let child of m.currentFolder.children) {
                    if (!child.deleted) {
                        allChildFoldersDeleted = false;
                        break;
                    }
                }
                
                if ((noChildFolders|| noSearchResultFolders || allChildFoldersDeleted ) && noQuizzes) {
                    let emptyFolder = (noChildFolders || allChildFoldersDeleted) && m.searchText.length === 0;
                    
                    let NoQuizzesIcon = emptyFolder ? EmptyFolderIcon : NoResultsIcon;
                    
                    let noQuizzesChildren = [
                        e(NoQuizzesIcon, {
                            className: 'no-quizzes-found-icon'
                        }),
                        e('span', {
                            className: 'no-quizzes-found-large-text' + (emptyFolder ? ' empty-folder-large-text' : ''),
                            children: emptyFolder ? translate('This folder is empty') : translate('Nothing found matching "{0}".').format(m.searchText)
                        })
                    ];
                    
                    if (m.searchText.length > 0) {
                        noQuizzesChildren.push(
                            e('span', {
                                className: 'no-quizzes-found-small-text',
                                children: translate('Delete search terms to start over.')
                            })
                        );
                    }
                    
                    let noQuizzesFoundContainerHeight = 160;
                    
                    if (window.innerWidth < 768 && !this.state.searchFocused) {
                        const NO_QUIZZES_MARGIN = 40;
                        noQuizzesFoundContainerHeight = window.innerHeight - MAIN_HEADER - STEP_HEADER - FILTER_BAR - NO_QUIZZES_MARGIN - STEP_HEADER - MAIN_FOOTER; // Make the modal fit in the view port.
                        noQuizzesFoundContainerHeight += 'px';
                    }
                    
                    step1Children.push(
                        e('div', {
                            className: 'no-quizzes-found-container' + (emptyFolder ? ' empty-folder-container' : ''),
                            children: noQuizzesChildren,
                            style: {height: noQuizzesFoundContainerHeight}
                        })
                    );
                } else {
                    let quizListHeight = 240;
                    
                    if (window.innerWidth < 768 && !this.state.searchFocused) {
                        quizListHeight = window.innerHeight - MAIN_HEADER - STEP_HEADER - FILTER_BAR - STEP_HEADER - MAIN_FOOTER; // Make the modal fit in the view port.
                    }
                    
                    step1Children.push(
                        e('div', {
                            className: 'start-quiz-table-container',
                            children: [
                                e('table', {
                                    className: 'start-quiz-table',
                                    children: [
                                        e('tbody', {
                                            children: tableChildren
                                        })
                                    ]
                                })
                            ],
                            style: {height: quizListHeight + 'px'}
                        })
                    );
                }
            } else {
                if (m.isQuiz) {
                    step2Children.push(
                        e('div', {
                            className: 'start-quiz-delivery-container',
                            children: [
                                this.createDeliveryMethod('feedback'),
                                this.createDeliveryMethod('navigation'),
                                this.createDeliveryMethod('teacherPaced')
                            ]
                        }),
                        this.createAdditionalSettings()
                    );
                } else {
                    let teamCountOptions = [];
                    for (let i = 2; i <= 20; i++) {
                        teamCountOptions.push({
                                title: i,
                                value: i,
                                selected: i === m.teamCount
                            }
                        )
                    }
                    
                    let iconOptions = [
                        {leftIcon: this.getSpaceRaceIcon('rocket'),    iconClass: 'space-race-icon', title: translate('Rocket'),    value: 'rocket',    selected: 'rocket'    === m.icon},
                        {leftIcon: this.getSpaceRaceIcon('bear'),      iconClass: 'space-race-icon', title: translate('Bear'),      value: 'bear',      selected: 'bear'      === m.icon},
                        {leftIcon: this.getSpaceRaceIcon('bee'),       iconClass: 'space-race-icon', title: translate('Bee'),       value: 'bee',       selected: 'bee'       === m.icon},
                        {leftIcon: this.getSpaceRaceIcon('bicycle'),   iconClass: 'space-race-icon', title: translate('Bicycle'),   value: 'bicycle',   selected: 'bicycle'   === m.icon},
                        {leftIcon: this.getSpaceRaceIcon('spaceship'), iconClass: 'space-race-icon', title: translate('Spaceship'), value: 'spaceship', selected: 'spaceship' === m.icon},
                        {leftIcon: this.getSpaceRaceIcon('unicorn'),   iconClass: 'space-race-icon', title: translate('Unicorn'),   value: 'unicorn',   selected: 'unicorn'   === m.icon}
                    ];
                    
                    let date = new Date();
                    if (date.getMonth() === 10) {
                        iconOptions.push(
                            {leftIcon: this.getSpaceRaceIcon('turkey'), iconClass: 'space-race-icon', title: translate('Seasonal'), value: 'turkey', selected: 'turkey' === m.icon}
                        );
                    } else if (date.getMonth() === 11) {
                        iconOptions.push(
                            {leftIcon: this.getSpaceRaceIcon('reindeer'), iconClass: 'space-race-icon', title: translate('Seasonal'), value: 'reindeer', selected: 'reindeer' === m.icon}
                        );
                    }
                    
                    let countdownOptions = [
                        {title: translate('None'), value: -1, selected: -1 === m.countdown}
                    ];
                    
                    for (let i = 1; i <= 15; i++) {
                        let minutes = i;
                        if (i === 11) minutes = 15;
                        if (i === 12) minutes = 20;
                        if (i === 13) minutes = 25;
                        if (i === 14) minutes = 30;
                        if (i === 15) minutes = 45;
                        
                        let title = translate('{0} minutes').format(minutes);
                        if (minutes === 1) {
                            title = translate(' 1 minute');
                        } else if (minutes < 10) {
                            title = ` ${title}`;
                        }
                        
                        let seconds = minutes * 60;
                        
                        countdownOptions.push(
                            {title: title, value: seconds, selected: seconds === m.countdown}
                        );
                    }
                    
                    let countdownChildren = [
                        e('p', {
                            className: 'space-race-select-label',
                            children: translate('Countdown')
                        }),
                        e(Select, {
                            children: countdownOptions,
                            disabled: !this.props.isPro,
                            onClickDisabled: () => {
                                if (!this.props.isPro) {
                                    c.disabledCountdownClicked();
                                }
                            },
                            onSelect: (name, value) => c.countdownChanged(value)
                        })
                    ];
                    
                    if (m.showCountdownTooltip) {
                        countdownChildren.push(
                            e(Tooltip, {
                                location: 'above',
                                title: translate('PRO Feature'),
                                text: translate('This setting is available to Socrative PRO users.')
                            })
                        );
                    }
                    
                    step2Children.push(
                        e('div', {
                            className: 'start-quiz-select-container',
                            children: [
                                e('div', {
                                    className: 'space-race-select-container teams-container',
                                    children: [
                                        e('p', {
                                            className: 'space-race-select-label',
                                            children: translate('Teams')
                                        }),
                                        e(Select, {
                                            children: teamCountOptions,
                                            error: m.teamCountError,
                                            label: translate('Number of Teams'),
                                            onSelect: (name, value) => c.teamCountChanged(value)
                                        })
                                    ]
                                }),
                                e('div', {
                                    className: 'radio-buttons-container',
                                    children: [
                                        e(RadioButton, {
                                            className: 'auto-assign-radio',
                                            label: translate('Auto-assign'),
                                            selected: m.teamAssignment === 'auto',
                                            onClick: () => c.teamAssignmentChanged('auto')
                                        }),
                                        e(RadioButton, {
                                            className: 'student-choice-radio',
                                            label: translate('Student Choice'),
                                            selected: m.teamAssignment === 'student',
                                            onClick: () => c.teamAssignmentChanged('student')
                                        })
                                    ]
                                }),
                                e('div', {
                                    className: 'space-race-select-container',
                                    children: [
                                        e('p', {
                                            className: 'space-race-select-label',
                                            children: translate('Icon')
                                        }),
                                        e(Select, {
                                            children: iconOptions,
                                            onSelect: (name, value) => c.iconChanged(value)
                                        })
                                    ]
                                }),
                                e('div', {
                                    className: 'space-race-select-container countdown-container',
                                    children: countdownChildren
                                })
                            ]
                        }),
                        this.createAdditionalSettings()
                    );
                }
            }
            
            modalContentChildren.push(
                e('div', {
                    className: 'step1',
                    children: step1Children
                }),
                e('div', {
                    className: 'step2',
                    children: step2Children
                })
            );
        }
        
        let modalChildren = [
            e('div', {
                className: 'modal-header',
                children: [
                    e('span', {
                        className: 'popup-title',
                        children: m.step1Title
                    }),
                    e('div', {
                        className: 'cancel-button',
                        children: [
                            e(CloseX, {
                                className: 'cancel-button-x',
                                color: '#cccccc'
                            })
                        ],
                        onClick: c.cancelClicked
                    })
                ]
            }),
            e('div', {
                id: 'start-quiz-modal-content',
                className: 'form',
                children: modalContentChildren
            })
        ];
        
        if (!m.waiting) {
            let footerChildren = [
                e('button', {
                    className: 'start-quiz-footer-button start-quiz-previous-button',
                    children: translate('PREVIOUS'),
                    disabled: step === 1,
                    onClick: c.previousClicked
                })
            ];
            
            if (step === 1) {
                footerChildren.push(
                    e('button', {
                        className: 'start-quiz-footer-button start-quiz-next-button',
                        children: translate('NEXT'),
                        disabled: !m.selectedQuiz,
                        onClick: c.nextClicked
                    })
                );
            } else {
                let disabled = false;
                
                if (m.isQuiz) {
                    disabled = !m.deliveryMethod;
                } else {
                    disabled = m.teamCount === 0;
                }
                
                footerChildren.push(
                    e('button', {
                        className: 'start-quiz-footer-button start-quiz-start-button',
                        children: translate('START'),
                        disabled: disabled,
                        key: 'launch-quiz-start',
                        onClick: c.startClicked
                    })
                );
            }
            
            modalChildren.push(
                e('div', {
                    id: 'start-quiz-footer',
                    className: 'start-quiz-footer form',
                    children: footerChildren
                })
            );
        }
        
        return e('div', {
            id: 'start-quiz-modal-background',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'start-quiz-modal-box',
                    className: 'modal-dialog',
                    children: modalChildren
                })
            ]
        });
    }
    
}
