import  React           from 'react';
import  ReactDOM        from 'react-dom';
import  _               from 'underscore';
import  model           from 'ShortAnswerModel';
import  view            from 'ShortAnswerView';
import  utils           from 'Utils';
import  room            from 'room';
import  user            from 'user';
import  activity        from 'activity';
import  dots from 'Dots';
import  platform        from 'Platform';

class ShortAnswerController {
    
    render(props) {
        model.reset(room.hasRoster());
        model.startCallback = props.startCallback;
        
        let saContainer = document.createElement('div');
        saContainer.id = 'sa-modal-container';
        document.body.appendChild(saContainer);
        
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model,
                    hasRoster: room.hasRoster(),
                    isPro: user.isPro()
                }
            ),
            document.getElementById('sa-modal-container'),
            () => {
                if (!model.pageBlurred) {
                    model.pageBlurred = true;
                    utils.blurPage();
                }
            }
        )
    }
    
    cancelClicked() {
        // Defer so the document click listener can fire first, if necessary.
        _.defer(() => {
            let saContainer = document.getElementById('sa-modal-container');
            ReactDOM.unmountComponentAtNode(saContainer);
            saContainer.parentNode.removeChild(saContainer);
            utils.unblurPage();
        });
    }

    questionChanged(question) {
        model.question = question;
        controller.renderView();
    }
    
    allowRepeatsChanged() {
        model.allowRepeats = !model.allowRepeats;
        controller.renderView();
    }
    
    requireNamesChanged() {
        model.requireNames = !model.requireNames;
        controller.renderView();
    }
    
    requireNamesClicked() {
        if (room.hasRoster()) {
            model.showNamesTooltip = true;
            controller.addClickListener();
            controller.renderView();
        }
    }

    oneAttemptChanged() {
        model.oneAttempt = !model.oneAttempt;
        controller.renderView();
    }

    oneAttemptClicked() {

        if (!room.hasRoster()) {
            model.showOneAttemptTooltip = true;
            controller.renderView();

            if (platform.isIos) {
                document.body.style.cursor = 'pointer'; // This makes any element respond to click events in iOS.
            }

            controller.addClickListener();
        }

    }
    
    addClickListener() {
        document.body.addEventListener('click', controller.documentClicked);
    }
    
    documentClicked() {
        if (platform.isIos) {
            document.body.style.cursor = 'default';
        }

        _.defer(() => {
            document.body.removeEventListener('click', controller.documentClicked);
            model.showNamesTooltip = false;
            model.showOneAttemptTooltip = false;
            controller.renderView();
        });
    }
    
    startClicked() {
        dots.start('sa-start-button');
        activity.startQuickQuestion({
            type: 'fr',
            allow_repeat_responses: model.allowRepeats,
            require_names: model.requireNames,
            one_attempt: model.oneAttempt,
            text: model.question,
            callback: controller.activityStarted
        });
    }
    
    activityStarted() {
        dots.stop();
        controller.cancelClicked();
        model.startCallback();
    }
    
}

let controller = new ShortAnswerController();
module.exports = controller;
