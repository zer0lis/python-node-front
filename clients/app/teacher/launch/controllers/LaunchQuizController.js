import React        from 'react';
import ReactDOM     from 'react-dom';
import _            from 'underscore';
import LaunchQuizModel from 'LaunchQuizModel';
import view         from 'LaunchQuizView';
import user         from 'user';
import teacher      from 'TeacherModel';
import room         from 'room';
import activity     from 'activity';
import utils        from 'Utils';
import platform     from 'Platform';
import store        from 'store';
import Constants    from 'Constants';
import {translate}  from 'translator';

let router = null,
    model = null;

class LaunchQuizController {
    
    open(props) {
        router = props.router;
        model = new LaunchQuizModel(room.hasRoster());
        model.activityType = props.activityType;
        
        if (props.activityType === Constants.QUIZ) {
            model.step1Title = translate('Launch Quiz');
            model.step2Title = platform.isPhone() ? translate('Delivery Method') : translate('Choose Delivery Method and Settings');
        } else {
            model.step1Title = translate('Launch Space Race');
            model.step2Title = platform.isPhone() ? translate('Settings') : translate('Choose Settings');
            model.pacing           = 'student';
            model.navigation       = false;
            model.requireNames     = true;
            model.shuffleQuestions = false;
            model.shuffleAnswers   = false;
            model.showFeedback     = true;
            model.showScore        = false;
        }
        
        let container = document.createElement('div');
        container.id = 'start-quiz-modal-container';
        document.body.appendChild(container);
        
        if (teacher.currentQuizFolderId === Constants.NO_FOLDER || teacher.currentQuizFolderId === Constants.TRASH) {
            teacher.currentQuizFolderId = Constants.ROOT_FOLDER;
        }
        
        if (teacher.shouldFetchQuizzes) {
            model.waiting = true;
            model.waitingText = translate('Loading Quizzes...');
            teacher.fetchQuizzes(controller.quizzesFetched);
        } else {
            model.currentFolder = teacher.quizFolderNav.getFolder(teacher.currentQuizFolderId);
        }
        
        controller.renderView();
    }
    
    quizzesFetched() {
        teacher.shouldFetchQuizzes = false;
        model.currentFolder = teacher.quizFolderNav.getFolder(teacher.currentQuizFolderId);
        model.waiting = false;
        controller.renderView();
    }
    
    renderView() {
        let container = document.getElementById('start-quiz-modal-container');
        if (container) {
            ReactDOM.render(
                React.createElement(
                    view, {
                        controller: controller,
                        model: model,
                        isPro: user.isPro(),
                        hasRoster: room.hasRoster(),
                        searchResultFolders: model.searchResultFolders, // Pass these getters as properties so
                        visibleQuizzes: model.visibleQuizzes            // they only execute once per render.
                    }
                ),
                container,
                () => {
                    if (!model.pageBlurred) {
                        model.pageBlurred = true;
                        utils.blurPage();
                    }
                }
            );
        }
    }
    
    cancelClicked() {
        let container = document.getElementById('start-quiz-modal-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
        utils.unblurPage();
    }
    
    searchTextChanged(searchText) {
        model.searchText = searchText;
        model.selectedQuiz = null;
        controller.renderView();
    }
    
    clearSearchClicked() {
        controller.searchTextChanged('');
        if (window.innerWidth >= 768) {
            let searchInput = document.getElementById('start-quiz-search-input');
            if (searchInput) {
                searchInput.focus();
            }
        }
    }
    
    backClicked() {
        model.currentFolder = teacher.quizFolderNav.getFolder(model.currentFolder.parentId);
        teacher.currentQuizFolderId = model.currentFolder.id;
        controller.searchTextChanged('');
    }
    
    folderClicked(folder) {
        model.currentFolder = folder;
        teacher.currentQuizFolderId = model.currentFolder.id;
        controller.searchTextChanged('');
    }
    
    columnClicked(column) {
        if (column === model.sortColumn) {
            model.toggleSortDirection();
        } else if (model.sortColumn !== 'name' && column === 'name') {
            model.sortDirection = 'asc';
        } else if (model.sortColumn !== 'date' && column === 'date') {
            model.sortDirection = 'desc';
        }
        model.sortColumn = column;
        controller.renderView();
    }
    
    quizClicked(id, soc, name) {
        model.selectedQuiz = {
            id: id,
            soc: soc,
            name: name
        };
        controller.renderView();
    }
    
    feedbackClicked() {
        model.showMethodTooltip = false;
        model.deliveryMethod  = 'feedback';
        model.pacing          = 'student';
        model.navigation      = false;
        model.requireNames    = true;
        model.shuffleQuestions = false;
        model.shuffleAnswers   = false;
        model.showFeedback    = true;
        model.showScore       = false;
        controller.renderView();
    }
    
    navigationClicked() {
        model.showMethodTooltip = false;
        model.deliveryMethod  = 'navigation';
        model.pacing          = 'student';
        model.navigation      = true;
        model.requireNames    = true;
        model.shuffleQuestions = false;
        model.shuffleAnswers   = false;
        model.showFeedback    = false;
        model.showScore       = false;
        controller.renderView();
    }
    
    teacherPacedClicked() {
        model.showMethodTooltip = false;
        model.deliveryMethod  = 'teacherPaced';
        model.pacing          = 'teacher';
        model.navigation      = false;
        model.requireNames    = true;
        model.shuffleQuestions = false;
        model.shuffleAnswers   = false;
        model.showFeedback    = false;
        model.showScore       = false;
        controller.renderView();
    }
    
    teamCountChanged (count) {
        model.teamCount = count;
        model.teamCountError = false;
        controller.renderView();
    }
    
    teamAssignmentChanged(assignment) {
        model.teamAssignment = assignment;
        controller.renderView();
    }
    
    iconChanged(icon) {
        model.icon = icon;
        controller.renderView();
    }
    
    countdownChanged(seconds) {
        model.countdown = seconds;
        controller.renderView();
    }
    
    disabledCountdownClicked() {
        if (!model.showCountdownTooltip) {
            model.showCountdownTooltip = true;
            controller.renderView();
            if (platform.isIos) {
                document.body.style.cursor = 'pointer'; // This makes any element respond to click events in iOS.
            }
            document.body.addEventListener('click', controller.bodyClicked);
        }
    }
    
    bodyClicked() {
        if (platform.isIos) {
            document.body.style.cursor = 'default';
        }
        _.defer(() => {
            document.body.removeEventListener('click', controller.bodyClicked);
            model.showMethodTooltip = false;
            model.showCountdownTooltip = false;
            model.showOneAttemptTooltip = false;
            controller.renderView();
        });
    }
    
    requireNamesChanged() {
        model.requireNames = !model.requireNames;
        controller.renderView();
    }
    
    requireNamesClicked() {
        if (model.isQuiz && model.deliveryMethod === '') {
            if (!model.showMethodTooltip) {
                model.showMethodTooltip = true;
            }
        } else if (room.hasRoster() && !model.showNamesTooltip) {
            model.showNamesTooltip = true;
        }
        
        if (model.showMethodTooltip || model.showNamesTooltip) {
            controller.renderView();
            if (platform.isIos) {
                document.body.style.cursor = 'pointer'; // This makes any element respond to click events in iOS.
            }
            document.body.addEventListener('click', controller.bodyClickedAfterNamesTooltip);
        }
    }
    
    bodyClickedAfterNamesTooltip() {
        if (platform.isIos) {
            document.body.style.cursor = 'default';
        }
        _.defer(() => {
            document.body.removeEventListener('click', controller.bodyClickedAfterNamesTooltip);
            model.showNamesTooltip = false;
            controller.renderView();
        });
    }
    
    shuffleQuestionsChanged() {
        model.shuffleQuestions = !model.shuffleQuestions;
        controller.renderView();
    }
    
    shuffleQuestionsClicked() {
        if (model.isQuiz && model.deliveryMethod === '' && !model.showMethodTooltip) {
            model.showMethodTooltip = true;
        } else if (model.pacing === 'teacher' && !model.showQuestionsTooltip) {
            model.showQuestionsTooltip = true;
        }
        
        if (model.showMethodTooltip || model.showQuestionsTooltip) {
            controller.renderView();
            if (platform.isIos) {
                document.body.style.cursor = 'pointer'; // This makes any element respond to click events in iOS.
            }
            document.body.addEventListener('click', controller.bodyClickedAfterQuestionsTooltip);
        }
    }
    
    bodyClickedAfterQuestionsTooltip() {
        if (platform.isIos) {
            document.body.style.cursor = 'default';
        }
        _.defer(() => {
            document.body.removeEventListener('click', controller.bodyClickedAfterQuestionsTooltip);
            model.showQuestionsTooltip = false;
            controller.renderView();
        });
    }
    
    shuffleAnswersChanged() {
        model.shuffleAnswers = !model.shuffleAnswers;
        controller.renderView();
    }
    
    shuffleAnswersClicked() {
        if (model.isQuiz && model.deliveryMethod === '' && !model.showMethodTooltip) {
            model.showMethodTooltip = true;
            controller.renderView();
            if (platform.isIos) {
                document.body.style.cursor = 'pointer'; // This makes any element respond to click events in iOS.
            }
            document.body.addEventListener('click', controller.bodyClicked);
        }
    }
    
    showFeedbackChanged() {
        model.showFeedback = !model.showFeedback;
        controller.renderView();
    }
    
    showFeedbackClicked() {
        if (model.isQuiz && model.deliveryMethod === '' && !model.showMethodTooltip) {
            model.showMethodTooltip = true;
        } else if (model.deliveryMethod === 'navigation' && !model.showFeedbackTooltip) {
            model.showFeedbackTooltip = true;
        }
        
        if (model.showMethodTooltip || model.showFeedbackTooltip) {
            controller.renderView();
            if (platform.isIos) {
                document.body.style.cursor = 'pointer'; // This makes any element respond to click events in iOS.
            }
            document.body.addEventListener('click', controller.bodyClickedAfterFeedbackTooltip);
        }
    }
    
    bodyClickedAfterFeedbackTooltip() {
        if (platform.isIos) {
            document.body.style.cursor = 'default';
        }
        _.defer(() => {
            document.body.removeEventListener('click', controller.bodyClickedAfterFeedbackTooltip);
            model.showFeedbackTooltip = false;
            controller.renderView();
        });
    }
    
    showScoreChanged() {
        model.showScore = !model.showScore;
        controller.renderView();
    }
    
    showScoreClicked() {
        if (model.isQuiz && model.deliveryMethod === '' && !model.showMethodTooltip) {
            model.showMethodTooltip = true;
            controller.renderView();
            if (platform.isIos) {
                document.body.style.cursor = 'pointer'; // This makes any element respond to click events in iOS.
            }
            document.body.addEventListener('click', controller.bodyClicked);
        }
    }

    oneAttemptChanged() {
        model.oneAttempt = !model.oneAttempt;
        controller.renderView();
    }

    oneAttemptClicked() {
        if (model.isQuiz && model.deliveryMethod === '' && !model.showMethodTooltip) {
            model.showMethodTooltip = true;
            model.showOneAttemptTooltip = false;
        }
        else if (!room.hasRoster() && !model.showOneAttemptTooltip) {
            model.showOneAttemptTooltip = true;
        }

        if (model.showOneAttemptTooltip || model.showMethodTooltip) {
            controller.renderView();
            if (platform.isIos) {
                document.body.style.cursor = 'pointer'; // This makes any element respond to click events in iOS.
            }
            document.body.addEventListener('click', controller.bodyClicked);
        }

    }

    previousClicked() {
        model.step -= 1;
        controller.renderView();
    }
    
    nextClicked() {
        model.step += 1;
        controller.renderView();
    }
    
    startClicked() {
        teacher.shouldFetchQuizzes = true;
        model.waiting = true;
        model.waitingText = translate('Starting Activity...');
        controller.renderView();
        let settings = {
            room_name:         room.getRoomName(),
            soc_num:           model.selectedQuiz.soc,
            pacing:            model.pacing,
            allow_student_nav: model.navigation,
            require_names:     model.requireNames,
            random_questions:  model.shuffleQuestions,
            random_answers:    model.shuffleAnswers,
            show_feedback:     model.showFeedback,
            show_results:      model.showScore,
            one_attempt:       model.oneAttempt
        };
        if (model.activityType === Constants.QUIZ) {
            activity.startQuiz({
                settings: settings,
                success: () => {
                    controller.cancelClicked();
                    router.routeToLiveResults();
                }
            });
        } else {
            if (model.teamCount === 0) {
                model.teamCountError = true;
                controller.renderView();
                return;
            }
            settings.team_count           = model.teamCount;
            settings.team_assignment_type = model.teamAssignment;
            settings.icon_type            = model.icon;
            settings.seconds              = model.countdown;
            activity.startSpaceRace({
                settings: settings,
                success: () => {
                    controller.cancelClicked();
                    controller.spaceRaceStarted();
                }
            });
        }
    }
    
    spaceRaceStarted() {
        store.remove('countdownStopped');
        activity.resetCountdown(model.countdown);
        router.routeToSpaceRace();
    }
    
}

let controller = new LaunchQuizController();
module.exports = controller;
