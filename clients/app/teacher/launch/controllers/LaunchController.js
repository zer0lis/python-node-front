import React    from 'react';
import ReactDOM from 'react-dom';
import view     from 'LaunchView';
import header   from 'HeaderController';
import user     from 'user';
import popup    from 'PopupController';
import activity from 'activity';
import launchQuizModal from 'LaunchQuizController';
import saPopup  from 'ShortAnswerController';
import {translate} from 'translator';

let router = null;

class LaunchController {
    
    doLaunch(teacherRouter) {
        router = teacherRouter;
        header.render('launch');
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller
                }
            ),
            document.getElementById('main-content'),
            controller.viewRendered
        );
    }
    
    viewRendered() {
        if (user.isFirstTime()) {
            user.unsetFirstTime();
            controller.showWelcomePopup();
        }
    }
    
    profilePopupClosed() {
        popup.closePopup();
        router.routeToProfileInfo();
    }
    
    showWelcomePopup() {
        let title = translate('Welcome to Socrative!');
        if (user.isFirstTimePro()) {
            title = translate('Welcome to Socrative PRO!');
            user.unsetFirstTimePro();
        }
        popup.render({
            title: title,
            message: translate('To learn more about what you can do with Socrative, select "HELP" from the menu near the top right.')
        });
    }
    
    quizClicked() {
        activity.checkBeforeStartingActivity(() => {
            launchQuizModal.open({
                activityType: activity.QUIZ,
                router: router
            });
        });
    }
    
    spaceRaceClicked() {
        activity.checkBeforeStartingActivity(() => {
            launchQuizModal.open({
                activityType: activity.SPACE_RACE,
                router: router
            });
        });
    }
    
    exitTicketClicked() {
        activity.startExitTicket({
            success: controller.activityStarted,
            error: controller.exitTicketError
        });
    }
    
    exitTicketError() {
        popup.render({
            title: translate('Unknown Error'),
            message: translate('An exit ticket could not be started. Please try again later.')
        });
    }
    
    mcClicked() {
        controller.startQuestion('mc');
    }
    
    tfClicked() {
        controller.startQuestion('tf');
    }
    
    saClicked() {
        activity.checkBeforeStartingActivity(controller.startSaConfirmed);
    }
    
    startSaConfirmed() {
        saPopup.render({
            startCallback: controller.activityStarted
        });
    }
    
    startQuestion(type) {
        activity.startQuickQuestion({
            type: type,
            require_names: false,
            callback: controller.activityStarted
        });
    }
    
    activityStarted() {
        router.routeToLiveResults();
    }
}

let controller = new LaunchController();
module.exports = controller;
