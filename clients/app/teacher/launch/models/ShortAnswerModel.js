'use strict';

class ShortAnswerModel {
    
    reset(requireNames) {

        this.startCallback          = null;
        this.question               = '';
        this.allowRepeats           = false;
        this.requireNames           = requireNames;
        this.showNamesTooltip       = false;
        this.pageBlurred            = false;
        this.oneAttempt             = requireNames; //  whether the activity can be taken only once or not
        this.showOneAttemptTooltip  = false;
    }
    
}

module.exports = new ShortAnswerModel();
