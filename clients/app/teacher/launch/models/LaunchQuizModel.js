'use strict';

let Constants = require('Constants'),
    teacher = require('TeacherModel');

import _ from 'underscore';
import user from 'user';

class LaunchQuizModel {
    
    constructor(rostered) {
        /*--------------*/
        /*    Client    */
        /*--------------*/
        this.activityType = ''; // One of the activity type constants.
        this.countdown = -1; // The countdown time, in seconds, for the space race activity.
        this.currentFolder = null; // The folder whose context is currently being displayed.
        this.deliveryMethod = ''; // One of the quiz delivery methods (feedback, navigation, etc.)
        this.icon = 'rocket'; // The icon used for the space race activity.
        this.navigation = ''; // Whether open navigation is allowed during the quiz.
        this.oneAttempt = rostered; // Whether students can only attempt the quiz once (default to true if the room is rostered, false otherwise).
        this.pacing = ''; // The pacing for the quiz (student or teacher paced).
        this.pageBlurred = false; // Whether the page has been blurred after opening this modal.
        this.requireNames = false; // Whether student names are required for this quiz.
        this.searchText = ''; // The text in the "Search Quizzes" input.
        this.selectedQuiz = null; // The quiz selected during step 1.
        this.showCountdownTooltip = false; // Whether to show the tooltip about the space race countdown being a pro feature.
        this.showFeedback = false; // Whether feedback will be shown to students after each question is answered.
        this.showFeedbackTooltip = false; // Whether to show the tooltip about feedback being disabled in open navigation quizzes.
        this.showMethodTooltip = false; // Whether to show the tooltip about selecting a delivery method before selecting settings.
        this.showNamesTooltip = false; // Whether to show the tooltip about student names being required in rostered rooms.
        this.showOneAttemptTooltip = false; // Whether to show the tooltip about one attempt being a pro feature available in rostered rooms.
        this.showQuestionsTooltip = false; // Whether to show the tooltip about question delivery in a teacher-paced quiz.
        this.showScore = false; // Whether students will be shown their final score after finishing the quiz.
        this.shuffleAnswers = false; // Whether answers should be shuffled during the quiz.
        this.shuffleQuestions = false; // Whether questions should be shuffled during the quiz.
        this._sortColumn = user.getQuizSortColumn() || 'date'; // The column by which quizzes and folders are sorted.
        this._sortDirection = user.getQuizSortDirection() || 'desc'; // The direction in which quizzes and folders are sorted.
        this.step = 1; // Which step the teacher is currently on. Step 1 is selecting a quiz, step 2 is selecting quiz settings.
        this.step1Title = ''; // The title of this modal on step 1.
        this.step2Title = ''; // The title of this modal on step 2.
        this.teamAssignment = 'auto'; // The team assignment type for the space race activity (auto or student).
        this.teamCount = 0; // The number of teams in the space race activity.
        this.teamCountError = false; // Whether to show an error state on the select element for the number of teams.
        this.waiting = false; // Whether the client is waiting to display the folders/quizzes (usually because a network request in progress).
        this.waitingText = ''; // The text to display instead of the folders/quizzes while the client is waiting.
    }
    
    get sortColumn() {
        return this._sortColumn;
    }
    
    set sortColumn(value) {
        this._sortColumn = value;
        user.setQuizSortColumn(value);
    }
    
    get sortDirection() {
        return this._sortDirection;
    }
    
    set sortDirection(value) {
        this._sortDirection = value;
        user.setQuizSortDirection(value);
    }
    
    toggleSortDirection() {
        this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    }
    
    get searchResultFolders() {
        let folders = [],
            searchText = this.searchText.toLowerCase();
        
        if (!user.isPro()) {
            return folders;
        }
        
        if (teacher.quizFolderNav && searchText.length > 0) {
            for (let folder of teacher.quizFolderNav.idFolderMap.values()) {
                if (folder.name.toLowerCase().indexOf(searchText) !== -1) {
                    if (!folder.deleted && !teacher.quizFolderNav.hasDeletedAncestor(folder)) {
                        folders.push(folder);
                    }
                }
            }
            
            if (this.sortColumn === 'name') {
                folders = _.sortBy(folders, (folder) => {
                    return folder.name.toLowerCase();
                });
            } else {
                folders = _.sortBy(folders, 'date');
            }
            
            if (this.sortDirection === 'desc') {
                folders.reverse(); // Underscore sorts ascending by default, so reverse if needed.
            }
        }
        
        return folders;
    }
    
    /**
     * Get an array of the quizzes that should be visible at any given
     * time during step 1, based on search text or the current folder.
     * @returns {array}
     */
    get visibleQuizzes() {
        let visible = [];
        
        for (let quiz of teacher.quizzes) {
            let searchText = this.searchText.toLowerCase();
            
            if (searchText.length > 0) {
                if (quiz.name.toLowerCase().indexOf(searchText) !== -1) {
                    let parent = teacher.quizFolderNav.getFolder(quiz.parent_id);
                    
                    if (!quiz.deleted && !parent.deleted && !teacher.quizFolderNav.hasDeletedAncestor(parent)) {
                        visible.push(quiz);
                    }
                }
            } else {
                if (!quiz.deleted && (quiz.parent_id === teacher.currentQuizFolderId || !user.isPro())) {
                    visible.push(quiz);
                }
            }
        }
        
        if (this.sortColumn === 'name') {
            visible = _.sortBy(visible, (quiz) => {
                return quiz.name.toLowerCase();
            });
        } else {
            visible = _.sortBy(visible, 'date');
        }
        
        if (this.sortDirection === 'desc') {
            visible.reverse(); // Underscore sorts ascending by default, so reverse if needed.
        }
        
        return visible;
    }
    
    get isQuiz() {
        return this.activityType === Constants.QUIZ;
    }
    
    get additionalSettings() {
        return {
            requireNames: this.requireNames,
            randomQuestions: this.randomQuestions,
            randomAnswers: this.randomAnswers,
            showFeedback: this.showFeedback,
            showScore: this.showScore,
            oneAttempt: this.oneAttempt
        };
    }
    
}

module.exports = LaunchQuizModel;
