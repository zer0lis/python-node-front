import React  from 'react';
import CloseX from 'CloseX';
import Input  from 'Input';
import {translate} from 'translator';

let e = React.createElement;

export default class ActivateLicenseView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        return e('div', {
            id: 'activate-license-modal-background',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'activate-license-modal-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('span', {
                                    className: 'popup-title',
                                    children: translate('Activate License')
                                }),
                                e('div', {
                                    className: 'cancel-button',
                                    children: [
                                        e(CloseX, {
                                            className: 'cancel-button-x',
                                            color: '#cccccc'
                                        })
                                    ],
                                    onClick: c.cancelClicked
                                })
                            ]
                        }),
                        e('div', {
                            id: 'activate-license-modal-content',
                            className: 'form',
                            children: [
                                e('div', {
                                    children: [
                                        e('span', {
                                            className: 'activate-license-info',
                                            dangerouslySetInnerHTML: {__html: translate('If you already have a license key, enter it here. Once your key is activated, you will have instant access to Socrative <strong>PRO</strong>.')}
                                        })
                                    ]
                                }),
                                e('div', {
                                    id: 'license-key',
                                    children: [
                                        e(Input, {
                                            id: 'license-key-id',
                                            autoFocus: true,
                                            label: translate('License Key'),
                                            maxLength: 29,
                                            value: m.key,
                                            status: m.errors.licenseKey,
                                            tooltipLocation: 'above',
                                            onChange: (event) => c.licenseKeyChanged(event.target.value)
                                        })
                                    ]
                                }),
                                e('div', {
                                    className: 'activate-license-buttons-container',
                                    children: [
                                        e('button', {
                                            id: 'activate-license-button-id',
                                            className: 'activate-license-button',
                                            disabled: !m.isValidKey(),
                                            children: translate('ACTIVATE'),
                                            onClick: c.activateClicked
                                        }),
                                        e('button', {
                                            className: 'activate-license-button activate-license-cancel-button',
                                            children: translate('CANCEL'),
                                            onClick: c.cancelClicked
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
