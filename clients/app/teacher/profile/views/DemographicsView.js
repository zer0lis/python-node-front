import React from 'react';
import ProfileTabs from 'ProfileTabs';
import Select from 'Select';
import Input from 'Input';
import Tooltip from 'Tooltip';
import countries from 'countries';
import states from 'states';
import Constants from 'Constants';
import {translate} from 'translator';

let e = React.createElement;

export default class DemographicsView extends React.Component {
    
    getFlag(language) {
        return e('img', {
            className: 'profile-language-icon',
            src: `${window.static_url}img/flags/${language}.png`
        });
    }
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        let countryOptions = [{
            selected: 'US' === m.country,
            title: 'USA',
            value: 'US'
        }];
        
        for (let country of countries) {
            countryOptions.push({
                selected: country === m.country,
                title: country,
                value: country
            });
        }
        
        let demoChildren = [
            e('div', {
                className: 'profile-select-container',
                children: [
                    e('p', {
                        className: 'demo-select-label',
                        children: translate('Country')
                    }),
                    e(Select, {
                        error: m.errors.country.error,
                        label: translate('Select Your Country'),
                        children: countryOptions,
                        onSelect: (name, value) => c.countryChanged(value),
                    })
                ]
            })
        ];
        
        let languageOptions = [
            {leftIcon: this.getFlag('en'),    iconClass: 'flag', title: 'English',             value: 'en',    selected: 'en'    === m.language},
            {leftIcon: this.getFlag('da'),    iconClass: 'flag', title: 'Dansk',               value: 'da',    selected: 'da'    === m.language},
            {leftIcon: this.getFlag('de'),    iconClass: 'flag', title: 'Deutsche',            value: 'de',    selected: 'de'    === m.language},
            {leftIcon: this.getFlag('en-gb'), iconClass: 'flag', title: 'English (UK)',        value: 'en-gb', selected: 'en-gb' === m.language},
            {leftIcon: this.getFlag('es'),    iconClass: 'flag', title: 'Español',             value: 'es',    selected: 'es'    === m.language},
            {leftIcon: this.getFlag('es-mx'), iconClass: 'flag', title: 'Español (México)',    value: 'es-mx', selected: 'es-mx' === m.language},
            {leftIcon: this.getFlag('fi'),    iconClass: 'flag', title: 'Suomalainen',         value: 'fi',    selected: 'fi'    === m.language},
            {leftIcon: this.getFlag('fr'),    iconClass: 'flag', title: 'Français',            value: 'fr',    selected: 'fr'    === m.language},
            {leftIcon: this.getFlag('fr-ca'), iconClass: 'flag', title: 'Français (Canadien)', value: 'fr-ca', selected: 'fr-ca' === m.language},
            {leftIcon: this.getFlag('ko'),    iconClass: 'flag', title: '한국어',                value: 'ko',    selected: 'ko'    === m.language},
            {leftIcon: this.getFlag('ms'),    iconClass: 'flag', title: 'Malay',               value: 'ms',    selected: 'ms'    === m.language},
            {leftIcon: this.getFlag('nl'),    iconClass: 'flag', title: 'Nederlands',          value: 'nl',    selected: 'nl'    === m.language},
            {leftIcon: this.getFlag('pt-br'), iconClass: 'flag', title: 'Português (Brasil)',  value: 'pt-br', selected: 'pt-br' === m.language},
            {leftIcon: this.getFlag('sv'),    iconClass: 'flag', title: 'Svenska',             value: 'sv',    selected: 'sv'    === m.language},
            {leftIcon: this.getFlag('th'),    iconClass: 'flag', title: 'ไทย',                 value: 'th',    selected: 'th'    === m.language},
            {leftIcon: this.getFlag('tr'),    iconClass: 'flag', title: 'Türk',                value: 'tr',    selected: 'tr'    === m.language},
            {leftIcon: this.getFlag('zh-cn'), iconClass: 'flag', title: '简体中文',             value: 'zh-cn', selected: 'zh-cn' === m.language}
        ];
        
        let languageChildren = [
            e('p', {
                className: 'demo-select-label',
                children: translate('Language')
            }),
            e(Select, {
                disabled: this.props.languageDisabled,
                error: m.errors.language.error,
                label: translate('Select Your Language'),
                onSelect: (name, value) => c.languageChanged(value),
                children: languageOptions,
                onClickDisabled: this.props.languageDisabled ? c.disabledLanguageClicked : function(){}
            })
        ];
        
        if (m.showLanguageTooltip) {
            languageChildren.push(
                e(Tooltip, {
                    location: 'above',
                    title: translate('Activity Running'),
                    text: translate('Language cannot be changed while an activity is running.')
                })
            );
        }
        
        demoChildren.push(
            e('div', {
                className: 'profile-select-container profile-margin-left profile-language-select',
                children: languageChildren
            })
        );
        
        if (m.demoVisible.usOrgType || m.demoVisible.intlOrgType) {
            let orgTitleK12  = m.demoVisible.usOrgType ? translate('K-12')      : translate('Primary/Secondary School'),
                orgTitleUSHE = m.demoVisible.usOrgType ? translate('Higher Ed') : translate('University');
            
            let orgTypeOptions = [
                {title: orgTitleK12,            value: Constants.ORGANIZATION_K12,       selected: Constants.ORGANIZATION_K12       === m.orgType},
                {title: orgTitleUSHE,           value: Constants.ORGANIZATION_HIGHER_ED, selected: Constants.ORGANIZATION_HIGHER_ED === m.orgType},
                {title: translate('Corporate'), value: Constants.ORGANIZATION_CORP,      selected: Constants.ORGANIZATION_CORP      === m.orgType},
                {title: translate('Other'),     value: Constants.ORGANIZATION_OTHER,     selected: Constants.ORGANIZATION_OTHER     === m.orgType}
            ];
            
            demoChildren.push(
                e('div', {
                    className: 'profile-select-container',
                    children: [
                        e('p', {
                            className: 'demo-select-label',
                            children: translate('Organization Type')
                        }),
                        e(Select, {
                            error: m.errors.orgType.error,
                            label: translate('Select Your Organization Type'),
                            children: orgTypeOptions,
                            onSelect: (name, value) => c.orgTypeChanged(value)
                        })
                    ]
                })
            );
        }
        
        if (m.demoVisible.zipCode) {
            demoChildren.push(
                e(Input, {
                    containerClass: 'profile-margin-left',
                    id: 'zipCode',
                    keyValue: 'zipCode',
                    label: translate('Zip Code'),
                    maxLength: 5,
                    status: m.errors.zipCode,
                    type: 'text',
                    value: m.zipCode,
                    onChange: (event) => c.zipCodeChanged(event.target.value)
                })
            )
        }
        
        let schoolOptions = [];
        
        if (m.demoVisible.schoolsByZip) {
            let matchingSchools = [],
                schoolsByZip = m.schoolsByZip[m.zipCode];
            
            if (schoolsByZip && schoolsByZip.schools) {
                for (let school of schoolsByZip.schools) {
                    matchingSchools.push({
                        selected: school.id === m.schoolId,
                        title: school.school_name,
                        value: school.id
                    });
                }
                
                let matchingSchoolsGroup = {
                    selectable: false,
                    title: translate('Matching Schools'),
                    children: matchingSchools
                };
                
                schoolOptions.push(matchingSchoolsGroup);
            }
            
            if (schoolsByZip && schoolsByZip.nearby_schools) {
                let nearbySchools = [];
                
                for (let school of schoolsByZip.nearby_schools) {
                    nearbySchools.push({
                        selected: school.id === m.schoolId,
                        title: school.school_name,
                        value: school.id
                    });
                }
                
                let nearbySchoolsGroup = {
                    selectable: false,
                    title: translate('Nearby Schools'),
                    children: nearbySchools
                };
                
                schoolOptions.push(nearbySchoolsGroup);
            }
            
            schoolOptions.push({
                selected: Constants.SCHOOL_NOT_LISTED === m.schoolId,
                title: translate('SCHOOL NOT LISTED'),
                value: Constants.SCHOOL_NOT_LISTED
            });
            
            demoChildren.push(
                e('div', {
                    className: 'profile-select-container',
                    children: [
                        e('p', {
                            className: 'demo-select-label',
                            children: translate('School')
                        }),
                        e(Select, {
                            disabled: m.loadingByZip,
                            error: !m.loadingByZip && m.errors.schoolId.error,
                            label: m.loadingByZip ? '' : translate('Select Your School'),
                            loading: m.loadingByZip,
                            children: schoolOptions,
                            onSelect: (name, value) => c.schoolByZipChanged(value),
                        })
                    ]
                })
            );
        } else if (m.demoVisible.schoolsByCountry) {
            if (m.schoolsByCountry[m.country]) {
                for (let school of m.schoolsByCountry[m.country]) {
                    schoolOptions.push({
                        selected: school.id === m.schoolId,
                        title: school.school_name,
                        value: `${school.id}_____${school.school_name}`
                    });
                }
            }
            
            schoolOptions.push({
                selected: Constants.SCHOOL_NOT_LISTED === m.schoolId,
                title: translate('SCHOOL NOT LISTED'),
                value: `${Constants.SCHOOL_NOT_LISTED}_____SCHOOL NOT LISTED`
            });
            
            demoChildren.push(
                e('div', {
                    className: 'profile-select-container profile-margin-left',
                    children: [
                        e('p', {
                            className: 'demo-select-label',
                            children: translate('School')
                        }),
                        e(Select, {
                            disabled: m.loadingByCountry,
                            error: !m.loadingByCountry && m.errors.schoolId.error,
                            label: m.loadingByCountry ? '' : translate('Select Your School'),
                            loading: m.loadingByCountry,
                            children: schoolOptions,
                            onSelect: (name, value) => c.schoolByCountryChanged(value),
                        })
                    ]
                })
            );
        }
        
        let stateOptions = [];
        
        if (m.demoVisible.state) {
            for (let state of states) {
                stateOptions.push({
                    selected: state.code === m.state,
                    title: state.name,
                    value: state.code
                });
            }
            
            demoChildren.push(
                e('div', {
                    className: 'profile-select-container profile-margin-left',
                    children: [
                        e('p', {
                            className: 'demo-select-label',
                            children: translate('State')
                        }),
                        e(Select, {
                            error: m.errors.state.error,
                            label: translate('Select Your State'),
                            children: stateOptions,
                            onSelect: (name, value) => c.stateChanged(value)
                        })
                    ]
                })
            );
        }
        
        let evenContainerClass = m.demoVisible.number % 2 === 0 ? '' : ' profile-margin-left';
        
        if (m.demoVisible.orgName) {
            demoChildren.push(
                e(Input, {
                    containerClass: evenContainerClass,
                    id: 'orgName',
                    keyValue: 'orgName',
                    label: translate('Organization Name'),
                    maxLength: 255,
                    status: m.errors.orgName,
                    type: 'text',
                    value: m.orgName,
                    onChange: (event) => c.orgNameChanged(event.target.value)
                })
            )
        }
        
        if (m.demoVisible.schoolName) {
            demoChildren.push(
                e(Input, {
                    containerClass: evenContainerClass,
                    id: 'schoolName',
                    keyValue: 'schoolName',
                    label: translate('School Name'),
                    maxLength: 255,
                    status: m.errors.schoolName,
                    type: 'text',
                    value: m.schoolName,
                    onChange: (event) => c.schoolNameChanged(event.target.value)
                })
            )
        }
        
        if (m.demoVisible.role) {
            let roleMargin = m.demoVisible.number % 2 !== 0 ? '' : ' profile-margin-left';
            
            demoChildren.push(
                e('div', {
                    className: 'profile-select-container' + roleMargin,
                    children: [
                        e('p', {
                            className: 'demo-select-label',
                            children: translate('Role')
                        }),
                        e(Select, {
                            error: m.errors.role.error,
                            label: translate('Select Your Role'),
                            children: [
                                {title: translate('Teacher'),       value: Constants.ROLE_TEACHER,       selected: Constants.ROLE_TEACHER       === m.role[0]},
                                {title: translate('Administrator'), value: Constants.ROLE_ADMINISTRATOR, selected: Constants.ROLE_ADMINISTRATOR === m.role[0]},
                                {title: translate('IT/Technology'), value: Constants.ROLE_IT_TECHNOLOGY, selected: Constants.ROLE_IT_TECHNOLOGY === m.role[0]},
                                {title: translate('Other'),         value: Constants.ROLE_OTHER,         selected: Constants.ROLE_OTHER         === m.role[0]}
                            ],
                            onSelect: (name, value) => c.roleChanged(value)
                        })
                    ]
                })
            );
        }
        
        demoChildren.push(
            e('div', {
                className: 'profile-button-container',
                children: [
                    e('button', {
                        id: 'demo-save-button',
                        className: 'action-button action-button-200',
                        children: translate('SAVE'),
                        onClick: c.saveClicked
                    })
                ]
            })
        );
        
        return e('div', {
            id: 'profile-container',
            children: [
                e(ProfileTabs, this.props),
                e('div', {
                    id: 'profile-form-container',
                    className: 'form',
                    children: demoChildren
                })
            ]
        })
    }
    
}
