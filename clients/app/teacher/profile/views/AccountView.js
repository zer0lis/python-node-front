import React from 'react';
import ProfileTabs from 'ProfileTabs';
import ProIcon from 'ProIcon';
import RenewProIcon from 'RenewProIcon';
import DownloadIcon from 'DownloadIcon';
import EmailIcon from 'EmailIcon';
import Toggle from 'Toggle';
import LicenseView from 'LicenseView';
import platform from 'Platform';
import uuid from 'uuid';
import Constants from 'Constants';
import {translate} from 'translator';

let e = React.createElement;

export default class AccountView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            l = m.licenseModel,
            licenseChildren = [],
            containerClass = 'form';
        
        if (this.props.isPro) {
            let renewalChildren = [
                e('div', {
                    className: 'account-icon-container',
                    children: e((c.inRenewalWindow() && !m.autoRenew) || this.props.inGracePeriod ? RenewProIcon : ProIcon)
                })
            ];
            
            let expiredHeader = '',
                daysLeftText = '';
            
            if (this.props.inGracePeriod) {
                let days = this.props.gracePeriodLeft === 1 ? translate('1 day') : translate('{0} days').format(this.props.gracePeriodLeft);
                expiredHeader = translate('Your license has expired!');
                daysLeftText = translate('You have {0} left in your renewal grace period.').format(days);
            }
            
            if (!(m.buyingLicense || l.licenseStep === Constants.COMPLETE_STEP)) {
                renewalChildren.push(
                    e('span', {
                        className: 'license-expired-header',
                        children: expiredHeader
                    }),
                    e('span', {
                        className: 'days-left-text',
                        children: daysLeftText
                    }),
                    e('div', {
                        className: 'account-summary-row first-account-summary-row',
                        children: [
                            e('span', {
                                className: 'account-summary-left-column',
                                children: translate('License Key')
                            }),
                            e('span', {
                                className: 'account-summary-right-column license-key-right-column',
                                children: m.licenseKey
                            })
                        ]
                    }),
                    e('div', {
                        className: 'account-summary-row',
                        children: [
                            e('span', {
                                className: 'account-summary-left-column',
                                children: translate('Expiration Date')
                            }),
                            e('span', {
                                className: 'account-summary-right-column',
                                children: this.props.expiration
                            })
                        ]
                    })
                );
                
                if (m.canAutoRenew && !this.props.inGracePeriod) {
                    renewalChildren.push(
                        e('div', {
                            className: 'account-summary-row',
                            children: [
                                e('span', {
                                    className: 'account-summary-left-column',
                                    children: translate('Auto Renew')
                                }),
                                e(Toggle, {
                                    className: 'auto-renew-toggle',
                                    checked: m.autoRenew,
                                    id: 'toggleAutoRenew',
                                    onChange: c.accountAutoRenewChanged
                                })
                            ]
                        })
                    );
                }
                
                renewalChildren.push(
                    e('button', {
                        className: 'action-button action-button-200',
                        children: m.isBuyer ? translate('RENEW LICENSE') : translate('PURCHASE LICENSE'),
                        id: 'renewLicenseButton',
                        onClick: c.renewLicenseClicked
                    })
                );
            }
            
            licenseChildren.push(
                e('div', {
                    className: 'renewal-container',
                    children: renewalChildren
                })
            );
        } else {
            licenseChildren.push(
                e('div', {
                    className: 'account-type-buttons-outer',
                    children: [
                        e('div', {
                            className: 'account-type-buttons-inner',
                            children: [
                                e('div', {
                                    className: 'account-type-button',
                                    children: [
                                        e(ProIcon),
                                        e('div', {
                                            className: 'account-type-button-text',
                                            key: uuid.v4(),
                                            children: translate('Upgrade your free account to Socrative PRO for multiple classrooms, rosters, and much more!')
                                        }),
                                        e('div', {
                                            className: 'learn-more-link' + (m.buyingLicense ? ' hidden' : ''),
                                            children: [
                                                e('span', {
                                                    className: 'link',
                                                    children: translate('Learn More'),
                                                    onClick: c.learnMoreClicked
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                }),
                e('button', {
                    className: 'action-button action-button-200' + (m.buyingLicense ? ' hidden' : ''),
                    children: translate('UPGRADE'),
                    id: 'profileUpgradeButton',
                    onClick: c.upgradeClicked
                })
            );
        }
        
        if (m.buyingLicense) {
            licenseChildren.push(
                e(LicenseView, this.props)
            );
        }

        let handleReceipt = [];
        
        if (!m.buyingLicense && m.isBuyer) {
            handleReceipt.push(
                e('div', {
                    className: platform.isIE() || platform.isEdge() ? 'fixed-width view-receipt-container' : 'view-receipt-container',
                    children: [
                        e('p', {
                            children: translate('Need a copy of your receipt?')
                        }),
                        e('div', {
                            className: platform.isMobile() ? 'receipt-buttons-container clearfix' : 'hover receipt-buttons-container clearfix',
                            children: [
                                e('span', {
                                    className: 'receipt-button email-button',
                                    onClick: c.emailReceiptClicked,
                                    children: [
                                        e(EmailIcon),
                                        e('span', {
                                            className: 'receipt-email-label',
                                            children: translate('EMAIL')
                                        })
                                    ]
                                }),
                                e('span', {
                                    className: platform.isIos ? 'receipt-button download-button disabled' : 'receipt-button download-button',
                                    onClick: c.downloadReceiptClicked,
                                    children: [
                                        e(DownloadIcon),
                                        e('span', {
                                            className: 'receipt-download-label',
                                            children: translate('DOWNLOAD')
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            );
        }

        return e('div', {
            id: 'profile-container',
            children: [
                e(ProfileTabs, this.props),
                e('div', {
                    id: 'profile-form-container',
                    className: containerClass,
                    children: [
                        licenseChildren,
                        handleReceipt
                    ]
                })
            ]
        })
    }
    
}
