import React from 'react';
import ProfileTabs from 'ProfileTabs';
import Input from 'Input';
import {translate} from 'translator';

let e = React.createElement;

export default class ProfileView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        let profileChildren = [
            e(Input, {
                id: 'firstName',
                label: translate('First Name'),
                maxLength: 255,
                status: m.errors.firstName,
                value: m.firstName,
                onChange: (event) => c.firstNameChanged(event.target.value)
            }),
            e(Input, {
                containerClass: 'profile-margin-left',
                id: 'lastName',
                label: translate('Last Name'),
                maxLength: 255,
                status: m.errors.lastName,
                value: m.lastName,
                onChange: (event) => c.lastNameChanged(event.target.value)
            }),
            e(Input, {
                id: 'email',
                label: translate('Email'),
                maxLength: 255,
                status: m.errors.email,
                type: 'email',
                value: m.email,
                onChange: (event) => c.emailChanged(event.target.value)
            })
        ];
        
        if (m.showChangePassword) {
            profileChildren.push(
                e('div', {
                    className: 'profile-change-password-link',
                    children: [
                        e('a', {
                            className: 'link',
                            href: '#profile/info',
                            children: translate('Change Password'),
                            onClick: c.changePasswordClicked
                        })
                    ]
                })
            );
        }
        
        if (this.props.passwordFieldsVisible) {
            let inputType = m.showPasswords ? 'text' : 'password';
            
            profileChildren.push(
                e(Input, {
                    autoComplete: 'off',
                    containerClass: 'old-password-input',
                    id: 'currentPassword',
                    label: translate('Current Password'),
                    maxLength: 255,
                    status: m.errors.currentPassword,
                    type: inputType,
                    value: m.currentPassword,
                    onChange: (event) => c.currentPasswordChanged(event.target.value)
                }),
                e(Input, {
                    id: 'newPassword',
                    label: translate('New Password'),
                    maxLength: 255,
                    status: m.errors.password,
                    type: inputType,
                    value: m.password,
                    onChange: (event) => c.newPasswordChanged(event.target.value)
                }),
                e(Input, {
                    containerClass: 'profile-margin-left',
                    id: 'newPassword2',
                    label: translate('Confirm New Password'),
                    maxLength: 255,
                    status: m.errors.password2,
                    type: inputType,
                    value: m.password2,
                    onChange: (event) => c.newPassword2Changed(event.target.value)
                }),
                e('div', {
                    id: 'show-passwords-container',
                    className: '',
                    children: [
                        e('input', {
                            id: 'show-passwords-checkbox',
                            checked: m.showPasswords,
                            onChange: c.showPasswordsChanged,
                            type: 'checkbox'
                        }),
                        e('label', {
                            className: 'show-passwords-label',
                            htmlFor: 'show-passwords-checkbox',
                            children: translate('Show Passwords')
                        })
                    ]
                })
            );
        }
        
        profileChildren.push(
            e('div', {
                className: 'profile-button-container',
                children: [
                    e('button', {
                        id: 'profile-save-button',
                        className: 'action-button action-button-200',
                        children: translate('SAVE'),
                        onClick: c.saveClicked
                    })
                ]
            })
        );
        
        return e('div', {
            id: 'profile-container',
            children: [
                e(ProfileTabs, this.props),
                e('div', {
                    id: 'profile-form-container',
                    className: 'form',
                    children: profileChildren
                })
            ]
        })
    }
    
}
