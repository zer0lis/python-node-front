import Backbone from 'backbone';
import React from 'react';
import ReactDOM from 'react-dom';
import header from 'HeaderController';
import teacher from 'TeacherModel';
import view from 'ProfileView';
import dots from 'Dots';
import user from 'user';
import Constants from 'Constants';
import {translate} from 'translator';

let router = null,
    model = teacher.account,
    licenseModel = model.licenseModel;

class ProfileController {
    
    doProfile(teacherRouter) {
        router = teacherRouter;
        header.render();
        
        if (router.isEnteringProfile()) {
            model.resetPasswordFields();
            model.restoreInitialValues();
            model.banner.message = '';
        }
        
        if (!model.initialEmail) {
            licenseModel.buyerId = user.getId();
            licenseModel.seats = 1;
            licenseModel.years = 1;
            licenseModel.licenseStep = Constants.LICENSE_STEP;
            model.buyingLicense = false;
            model.fetchAccount(() => licenseModel.fetchPrices(controller.fetchSchools));
        } else {
            controller.fetchSchools();
        }
    }
    
    fetchSchools() {
        if (!model.schoolsFetched) {
            if (model.shouldFetchByZip()) {
                model.fetchSchoolsByZip(controller.schoolsFetched);
            } else if (model.shouldFetchByCountry()) {
                model.fetchSchoolsByCountry(controller.schoolsFetched);
            } else {
                controller.schoolsFetched(); // When the org type is Corporate/Other, there are no schools to fetch.
            }
        } else {
            controller.schoolsFetched();
        }
        controller.renderView();
    }
    
    schoolsFetched() {
        model.schoolsFetched = true;
        model.validateProfile(controller.renderView);
        controller.renderView();
    }
    
    renderView() {
        if (Backbone.history.fragment === 'profile/info') {
            ReactDOM.render(
                React.createElement(
                    view, {
                        controller: controller,
                        model: model,
                        selectedTab: 'info',
                        router: router,
                        passwordFieldsVisible: !model.showChangePassword && !(model.mcUser || model.googleUser)
                    }
                ),
                document.getElementById('main-content')
            );
        }
    }

    firstNameChanged(firstName) {
        model.firstName = firstName;
        model.errors.firstName.error = false;
        controller.renderView();
    }
    
    lastNameChanged(lastName) {
        model.lastName = lastName;
        model.errors.lastName.error = false;
        controller.renderView();
    }
    
    emailChanged(email) {
        model.email = email.toLowerCase().trim();
        model.errors.email.error = false;
        model.emailVerified = email === model.initialEmail;
        controller.renderView();
    }
    
    changePasswordClicked() {
        model.showChangePassword = false;
        controller.renderView();
    }
    
    currentPasswordChanged(currentPassword) {
        model.currentPassword = currentPassword;
        model.errors.currentPassword.error = false;
        controller.renderView();
    }
    
    newPasswordChanged(newPassword) {
        model.password = newPassword;
        model.errors.password.error = false;
        controller.renderView();
    }
    
    newPassword2Changed(newPassword2) {
        model.password2 = newPassword2;
        model.errors.password2.error = false;
        controller.renderView();
    }
    
    showPasswordsChanged() {
        model.showPasswords = !model.showPasswords;
        controller.renderView();
    }
    
    saveClicked() {
        dots.start('profile-save-button');
        model.validateProfile(controller.saveValidationResult);
    }
    
    saveValidationResult() {
        if (model.errors.firstName.error ||
            model.errors.lastName.error ||
            model.errors.email.error ||
            model.errors.currentPassword.error ||
            model.errors.password.error ||
            model.errors.password2.error) {
            dots.stop();
            controller.renderView();
        } else {
            if (!model.validateDemographics()) {
                dots.stop();
                router.routeToProfileDemo();
            } else {
                model.saveAccount(controller.accountSaved);
            }
        }
    }
    
    accountSaved(result) {
        if (result.success) {
            user.setFirstName(model.firstName); // Update the user model for the header (SOC-1036).
            
            if (model.language !== user.getLanguage()) {
                user.setLanguage(model.language);
                router.routeToTeacherLogin();
            } else {
                dots.stop();
                model.showBanner(Constants.BANNER_TYPE_SUCCESS, translate('Profile updated!'), controller.renderView);
                header.render();
                controller.renderView();
            }
        } else {
            if (result.oldPasswordWrong) {
                dots.stop();
                controller.renderView();
            } else if (result.authFailure) {
                router.routeToTeacherLogin();
            }
        }
    }
    
}

let controller = new ProfileController();
module.exports = controller;
