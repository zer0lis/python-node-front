import Backbone from 'backbone';
import React from 'react';
import ReactDOM from 'react-dom';
import header from 'HeaderController';
import user from 'user';
import teacher from 'TeacherModel';
import view from 'AccountView';
import activateLicenseModal from 'ActivateLicenseController';
import goPro from 'GoProController';
import dots from 'Dots';
import Constants from 'Constants';
import popup from 'PopupController';
import utils from 'Utils';
import renewalNotice from 'RenewalController';
import bannerController from 'BannerController';
import {translate} from 'translator';

let router = null,
    model = teacher.account,
    licenseModel = model.licenseModel;

class AccountController {
    
    doAccount(teacherRouter) {
        router = teacherRouter;
        header.render();
        
        if (router.isEnteringProfile()) {
            model.resetPasswordFields();
            model.restoreInitialValues();
            model.banner.message = '';
        }
        
        if (!model.initialEmail) {
            licenseModel.buyerId = user.getId();
            licenseModel.seats = 1;
            licenseModel.years = 1;
            licenseModel.licenseStep = Constants.LICENSE_STEP;
            model.buyingLicense = false;
            model.fetchAccount(() => licenseModel.fetchPrices(controller.fetchSchools));
        } else {
            controller.fetchSchools();
        }
    }
    
    fetchSchools() {
        licenseModel.calculatePriceData();
        controller.setRenewalDate();
        if (!model.schoolsFetched) {
            if (model.shouldFetchByZip()) {
                model.fetchSchoolsByZip(controller.schoolsFetched);
            } else if (model.shouldFetchByCountry()) {
                model.fetchSchoolsByCountry(controller.schoolsFetched);
            } else {
                controller.schoolsFetched(); // When the org type is Corporate/Other, there are no schools to fetch.
            }
        } else {
            controller.schoolsFetched();
        }
        controller.renderView();
    }
    
    schoolsFetched() {
        model.schoolsFetched = true;
        controller.renderView();
    }
    
    renderView() {
        if (Backbone.history.fragment === 'profile/billing') {
            let today = new Date(),
                gracePeriod = model.gracePeriod;
            
            ReactDOM.render(
                React.createElement(
                    view, {
                        controller: controller,
                        model: model,
                        selectedTab: 'billing',
                        router: router,
                        isPro: user.isPro(),
                        expiration: utils.formatDate(model.expiration),
                        inGracePeriod: user.isPro() && model.expiration.getTime() <= today.getTime() && today.getTime() <= gracePeriod.getTime(),
                        gracePeriodLeft: user.isPro() ? Math.round((gracePeriod - today) / (1000 * 60 * 60 * 24)) : 0,
                        registration: false
                    }
                ),
                document.getElementById('main-content')
            )
        }
    }
    
    inRenewalWindow() {
        if (!user.isPro()) {
            return false;
        }
        
        let today = new Date();
        return model.renewalWindow.getTime() <= today.getTime() && today.getTime() <= model.gracePeriod.getTime();
    }
    
    /**
     * Change the auto renew setting for an existing license.
     */
    accountAutoRenewChanged() {
        let oldAutoRenewValue = model.autoRenew;
        model.autoRenew = !model.autoRenew;
        
        licenseModel.saveAutoRenew(model.autoRenew, (success) => {
            if (!success) {
                model.autoRenew = oldAutoRenewValue;
                controller.renderView();
                
                popup.render({
                    title: translate('Unknown Error'),
                    message: translate('There was a problem changing your auto renew setting. Please check your connection and try again.')
                });
            } else {
                renewalNotice.closeClicked();
                header.render();
            }
        });
        
        controller.renderView();
    }
    
    renewLicenseClicked() {
        model.banner.message = '';
        model.buyingLicense = true;
        controller.renderView();
    }
    
    learnMoreClicked() {
        goPro.render({hideGoProButton: true});
    }
    
    upgradeClicked() {
        model.banner.message = '';
        model.buyingLicense = true;
        controller.renderView();
    }
    
    enterLicenseKeyClicked() {
        activateLicenseModal.render({
            activateCallback: (licenseExpiration, licenseKey) => {
                if (licenseExpiration && licenseKey) {
                    user.setPro(true);
                    bannerController.getBannerInfo();
                    header.render();
                    model.showBanner(Constants.BANNER_TYPE_SUCCESS, translate('License Activated!'), controller.renderView);
                    model.setExpiration(new Date(licenseExpiration));
                    model.licenseKey = licenseKey;
                    model.buyingLicense = false;
                    renewalNotice.closeAfterRenewal();
                    controller.renderView();
                }
            }
        });
    }
    
    inputChanged(id, value) {
        if (id === 'licenseSeats') {
            licenseModel.seats = parseInt(value);
            if (isNaN(licenseModel.seats)) {
                licenseModel.seats = ''; // Allow an empty input so the user needn't select the current value first.
            }
            if (licenseModel.isValidSeats()) {
                controller.seatsBlurred();
            }
        } else if (id === 'couponName') {
            licenseModel.couponName = value;
            licenseModel.setError('couponName', false);
        } else if (id === 'nameOnCard') {
            licenseModel.nameOnCard = value;
            licenseModel.setError('nameOnCard', !licenseModel.isValidNameOnCard());
        } else if (id === 'cardNumber') {
            licenseModel.cardType = Stripe.card.cardType(value);
            licenseModel.cardNumber = value;
            licenseModel.setError('cardNumber', !licenseModel.isValidCardNumber());
        } else if (id === 'cvc') {
            licenseModel.cvc = value;
            licenseModel.setError('cvc', !licenseModel.isValidCvc());
        }
        controller.renderView();
    }
    
    seatsBlurred() {
        if (!licenseModel.seats || licenseModel.seats < 1) {
            licenseModel.seats = 1;
        } else if (licenseModel.seats > Constants.MAX_DISPLAY_SEATS) {
            licenseModel.seats = Constants.MAX_DISPLAY_SEATS;
        }
        
        licenseModel.calculatePriceData();
        controller.renderView();
    }
    
    otherPaymentOptionsClicked() {
        popup.render({
            title: translate('Payment Options'),
            message: translate('Would you like to request more information about payment options and bulk pricing?'),
            buttonText: translate('Yes'),
            cancelText: translate('No'),
            buttonClicked: () => {
                let role = utils.encodeForUrl(model.role[0] || ''),
                    firstName = utils.encodeForUrl(model.firstName || ''),
                    lastName = utils.encodeForUrl(model.lastName || ''),
                    zip = utils.encodeForUrl(model.zipCode || ''),
                    orgType = utils.encodeForUrl(model.orgType || ''),
                    orgName = utils.encodeForUrl(model.schoolName || model.orgName || ''),
                    email = utils.encodeForUrl(model.email || ''),
                    seats = utils.encodeForUrl(licenseModel.seats || ''),
                    years = utils.encodeForUrl(licenseModel.years || '');
                window.open(`${marketing_host}/pricing.html?role=${role}&firstName=${firstName}&lastName=${lastName}&zip=${zip}&orgType=${orgType}&orgName=${orgName}&email=${email}&seats=${seats}&years=${years}#bulk`);
            }
        });
    }
    
    yearsChanged(years) {
        licenseModel.years = years;
        controller.setRenewalDate();
        licenseModel.calculatePriceData();
        controller.renderView();
    }
    
    setRenewalDate() {
        let renewalDate = new Date();
        
        if (user.isPro()) {
            renewalDate.setTime(model.expiration.getTime());
        }
        
        renewalDate.setFullYear(renewalDate.getFullYear() + licenseModel.years);
        licenseModel.renewalDate = renewalDate;
    }
    
    applyCouponClicked() {
        if (!licenseModel.isValidCouponName()) {
            dots.stop();
            licenseModel.setError('couponName', true);
        } else {
            dots.start('applyCouponButton');
            licenseModel.fetchCoupon(controller.couponFetched);
        }
        
        controller.renderView();
    }
    
    couponFetched(success) {
        dots.stop();
        
        if (success) {
            licenseModel.calculatePriceData();
        }
        
        controller.renderView();
    }
    
    clearCouponClicked() {
        licenseModel.coupon = null;
        licenseModel.couponName = '';
        licenseModel.couponSuccess = false;
        licenseModel.calculatePriceData();
        controller.renderView();
    }
    
    applyNowChanged() {
        licenseModel.applyNow = !licenseModel.applyNow;
        controller.renderView();
    }
    
    /**
     * Change the auto renew setting during license purchase.
     */
    autoRenewChanged() {
        licenseModel.autoRenew = !licenseModel.autoRenew;
        controller.renderView();
    }
    
    reviewAndPayClicked() {
        if (licenseModel.isValidCouponName() && !model.couponSuccess) { // The user forgot to apply the coupon.
            dots.start('reviewAndPayButton');
            
            licenseModel.fetchCoupon((success) => {
                dots.stop();
                
                if (success) {
                    licenseModel.calculatePriceData();
                    licenseModel.licenseStep = Constants.PAYMENT_STEP;
                }
                
                controller.renderView();
            });
        } else {
            licenseModel.licenseStep = Constants.PAYMENT_STEP;
            controller.renderView();
        }
    }
    
    monthChanged(value) {
        licenseModel.month = value;
        licenseModel.setError('month', false);
        controller.renderView();
    }
    
    yearChanged(value) {
        licenseModel.year = value;
        licenseModel.setError('year', false);
        controller.renderView();
    }
    
    previousClicked() {
        licenseModel.licenseStep = Constants.LICENSE_STEP;
        controller.renderView();
    }
    
    purchaseClicked() {
        if (licenseModel.canPurchase()) {
            dots.start('licensePurchaseButton');
            licenseModel.makePurchase(controller.purchaseResult);
        } else {
            dots.stop();
        }
        
        controller.renderView();
    }
    
    purchaseResult(success, error) {
        if (!success) {
            dots.stop();
            
            if (error === Constants.PRICE_MISMATCH) {
                popup.render({
                    title: translate('Price Change'),
                    message: translate('Our prices recently changed. The total for your order has been updated.')
                });
            } else if (error === Constants.INTERNAL_SERVER_ERROR) {
                controller.showErrorBanner(translate('There was a problem completing your purchase or activating your license. Please contact Socrative Support.'));
            } else {
                controller.showErrorBanner(error);
            }
        } else {
            if (!user.isPro() && licenseModel.applyNow) {
                user.setPro(true);
                bannerController.getBannerInfo();
            }
            
            if (licenseModel.applyNow) {
                model.setExpiration(new Date(licenseModel.renewalDate.getTime()));
            }
            
            model.showBanner(Constants.BANNER_TYPE_SUCCESS, translate('Purchase Complete!'), controller.renderView);
            renewalNotice.closeAfterRenewal();
            licenseModel.licenseStep = Constants.COMPLETE_STEP;
        }
        
        controller.renderView();
    }
    
    showErrorBanner(message) {
        dots.stop();
        
        if (!message) {
            message = translate('Check your credit card information and try again.');
        }
        
        model.showBanner(Constants.BANNER_TYPE_ERROR, message);
        controller.renderView();
        
        if (window.innerWidth >= 768) {
            document.body.scrollTop = 0;
        }
    }
    
    closeBannerClicked() {
        model.banner.message = '';
        controller.renderView();
    }

    emailReceiptClicked() {
        model.getReceipt(controller.receiptSent);
    }

    receiptSent(success) {
        if (success) {
            model.showBanner(Constants.BANNER_TYPE_SUCCESS, translate('The receipt has been sent to {0}.').format(model.email), controller.renderView);
        } else {
            model.showBanner(Constants.BANNER_TYPE_ERROR, translate('An error occurred. Please check your connection and try again.'), controller.renderView);
        }
        
        header.render();
        controller.renderView();
    }

    downloadReceiptClicked() {
        controller.startDownload()
    }

    startDownload() {
        let hiddenIFrameID = 'receiptDownloader',
        iframe = document.getElementById(hiddenIFrameID);

        if (iframe === null) {
            iframe = document.createElement('iframe');
            iframe.id = hiddenIFrameID;
            iframe.style.display = 'none';
            document.body.appendChild(iframe);
        }
        
        iframe.src = window.backend_host + '/lecturers/api/payment/receipt/?action=download';
    }
    
}

let controller = new AccountController();
module.exports = controller;
