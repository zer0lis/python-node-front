import Backbone from 'backbone';
import React from 'react';
import ReactDOM from 'react-dom';
import header from 'HeaderController';
import teacher from 'TeacherModel';
import view from 'DemographicsView';
import dots from 'Dots';
import activity from 'activity';
import Constants from 'Constants';
import platform from 'Platform';
import user from 'user';
import {translate} from 'translator';

let router = null,
    model = teacher.account,
    licenseModel = model.licenseModel;

class DemographicsController {
    
    doDemo(teacherRouter) {
        router = teacherRouter;
        header.render();
        
        if (router.isEnteringProfile()) {
            model.resetPasswordFields();
            model.restoreInitialValues();
            model.banner.message = '';
        }
        
        if (!model.initialEmail) {
            licenseModel.buyerId = user.getId();
            licenseModel.seats = 1;
            licenseModel.years = 1;
            licenseModel.licenseStep = Constants.LICENSE_STEP;
            model.buyingLicense = false;
            model.fetchAccount(() => licenseModel.fetchPrices(controller.fetchSchools));
        } else {
            controller.fetchSchools();
        }
    }
    
    fetchSchools() {
        if (!model.schoolsFetched) {
            if (model.shouldFetchByZip()) {
                model.fetchSchoolsByZip(controller.schoolsFetched);
            } else if (model.shouldFetchByCountry()) {
                model.fetchSchoolsByCountry(controller.schoolsFetched);
            } else {
                controller.schoolsFetched(); // When the org type is Corporate/Other, there are no schools to fetch.
            }
        } else {
            controller.schoolsFetched();
        }
        controller.determineVisible();
    }
    
    schoolsFetched() {
        model.schoolsFetched = true;
        model.validateDemographics();
        activity.refresh(controller.activityRefreshed);
        controller.determineVisible();
    }
    
    activityRefreshed() {
        controller.determineVisible();
    }
    
    determineVisible() {
        let visible = {country: true, number: 2}; // Start at 2 because country and languge are always visible.
        
        if (model.country) {
            if (model.isCountryUS()) {
                visible.usOrgType = true;
                visible.number++;
                if (model.isK12()) {
                    visible.zipCode = true;
                    visible.number++;
                    if (model.isValidZipCode()) {
                        visible.schoolsByZip = true;
                        visible.number++;
                        if (model.isSchoolSelected()) {
                            visible.state = true;
                            visible.role = true;
                            visible.number += 2;
                        } else if (model.isSchoolNotListed()) {
                            visible.state = true;
                            visible.schoolName = true;
                            visible.role = true;
                            visible.number += 3;
                        }
                    }
                } else if (model.isHigherEd()) {
                    visible.state = true;
                    visible.number++;
                    if (model.state) {
                        visible.orgName = true;
                        visible.role = true;
                        visible.number += 2;
                    }
                } else if (model.orgType) {
                    visible.orgName = true;
                    visible.role = true;
                    visible.number += 2;
                }
            } else {
                visible.intlOrgType = true;
                visible.number++;
                if (model.isK12()) {
                    visible.schoolsByCountry = true;
                    visible.number++;
                    if (model.isSchoolSelected()) {
                        visible.role = true;
                        visible.number++;
                    } else if (model.isSchoolNotListed()) {
                        visible.schoolName = true;
                        visible.role = true;
                        visible.number += 2;
                    }
                } else if (model.orgType) {
                    visible.orgName = true;
                    visible.role = true;
                    visible.number += 2;
                }
            }
        }
        
        model.demoVisible = visible;
        dots.stop();
        controller.renderView();
    }
    
    renderView() {
        if (Backbone.history.fragment === 'profile/demo') {
            ReactDOM.render(
                React.createElement(
                    view, {
                        controller: controller,
                        model: model,
                        selectedTab: 'demo',
                        router: router,
                        languageDisabled: activity.isInProgress()
                    }
                ),
                document.getElementById('main-content')
            );
        }
    }
    
    countryChanged(country) {
        model.country = country;
        model.errors.country.error = false;
        
        // The country changed. Reset everything after it.
        model.orgType = '';
        model.orgName = '';
        model.zipCode = '';
        model.schoolId = null;
        model.state = '';
        model.schoolName = '';
        
        controller.determineVisible();
    }
    
    languageChanged(language) {
        model.language = language;
        model.errors.language.error = false;
        controller.determineVisible();
    }
    
    disabledLanguageClicked() {
        model.showLanguageTooltip = true;
        controller.determineVisible();
        controller.addClickListener();
    }
    
    addClickListener() {
        if (platform.isIos || platform.isIosApp) {
            document.addEventListener('touchend', controller.documentClicked);
        } else {
            document.addEventListener('click', controller.documentClicked);
        }
    }
    
    documentClicked() {
        document.removeEventListener('touchend', controller.documentClicked);
        document.removeEventListener('click', controller.documentClicked);
        model.showLanguageTooltip = false;
        controller.determineVisible();
    }
    
    orgTypeChanged(orgType) {
        model.orgType = orgType;
        model.errors.orgType.error = false;
        
        // The org type changed. Reset everything after it.
        model.orgName = '';
        model.zipCode = '';
        model.schoolId = null;
        model.state = '';
        model.schoolName = '';
        
        if (model.isCountryUS()) {
            controller.determineVisible();
        } else {
            if (model.isK12()) {
                model.loadingByCountry = true;
                controller.determineVisible();
                model.fetchSchoolsByCountry(controller.determineVisible);
            } else {
                controller.determineVisible();
            }
        }
    }
    
    schoolByCountryChanged(idAndName) {
        let parts = idAndName.split('_____'),
            id = parseInt(parts[0]),
            name = parts[1];
        
        model.schoolId = id;
        model.errors.schoolId.error = false;
        model.schoolName = id === Constants.SCHOOL_NOT_LISTED ? '' : name;
        
        controller.determineVisible();
    }
    
    schoolNameChanged(name) {
        model.schoolName = name;
        model.errors.schoolName.error = false;
        controller.determineVisible();
    }
    
    zipCodeChanged(zipCode) {
        if (zipCode === '' || /^\d+$/.test(zipCode)) {
            model.zipCode = zipCode;
            model.errors.zipCode.error = false;
            
            // The zip code changed. Reset everything after it.
            model.schoolId = null;
            model.state = '';
            model.schoolName = '';
            
            if (model.isValidZipCode()) {
                model.loadingByZip = true;
                model.fetchSchoolsByZip(controller.determineVisible);
            }
        } else {
            model.errors.zipCode.error = true;
            model.errors.zipCode.message = translate('Please enter a 5-digit zip code.');
        }
        
        controller.determineVisible();
    }
    
    schoolByZipChanged(id) {
        model.schoolId = id;
        model.errors.schoolId.error = false;
        
        // The domestic school changed. Reset everything after it.
        model.state = '';
        model.schoolName = '';
        
        if (model.isCountryUS() && model.isK12()) {
            model.setDataFromSchool();
        }
        
        controller.determineVisible();
    }
    
    stateChanged(state) {
        model.state = state;
        model.errors.state.error = false;
        controller.determineVisible();
    }
    
    roleChanged(role) {
        model.role = [role];
        model.errors.role.error = false;
        controller.determineVisible();
    }
    
    orgNameChanged(orgName) {
        model.orgName = orgName;
        model.errors.orgName.error = false;
        controller.determineVisible();
    }
    
    saveClicked() {
        dots.start('demo-save-button');
        model.validateProfile(controller.saveValidationResult);
    }
    
    saveValidationResult() {
        if (model.errors.firstName.error ||
            model.errors.lastName.error ||
            model.errors.email.error ||
            model.errors.currentPassword.error ||
            model.errors.password.error ||
            model.errors.password2.error) {
            dots.stop();
            router.routeToProfileInfo();
        } else {
            if (!model.validateDemographics()) {
                dots.stop();
                controller.determineVisible();
            } else {
                model.saveAccount(controller.accountSaved);
            }
        }
    }
    
    accountSaved(result) {
        if (result.success) {
            user.setFirstName(model.firstName); // Update the user model for the header (SOC-1036).
            
            if (model.language !== user.getLanguage()) {
                user.setLanguage(model.language);
                router.routeToTeacherLogin();
            } else {
                dots.stop();
                model.showBanner(Constants.BANNER_TYPE_SUCCESS, translate('Profile updated!'), controller.renderView);
                header.render();
                controller.determineVisible();
            }
        } else {
            if (result.oldPasswordWrong) {
                dots.stop();
                router.routeToProfileInfo();
            } else if (result.authFailure) {
                router.routeToTeacherLogin();
            }
        }
    }
    
}

let controller = new DemographicsController();
module.exports = controller;
