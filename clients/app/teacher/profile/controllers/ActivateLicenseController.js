import React        from 'react';
import ReactDOM     from 'react-dom';
import LicenseModel from 'LicenseModel';
import view         from 'ActivateLicenseView';
import utils        from 'Utils';
import dots      from 'Dots';

let model = null;

class ActivateLicenseController {

    render(props) {
        model = new LicenseModel();
        model.activateCallback = props.activateCallback;
        
        let container = document.createElement('div');
        container.id = 'activate-license-modal-container';
        document.body.appendChild(container);
        
        controller.renderView();
    }

    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('activate-license-modal-container'),
            () => {
                if (!model.pageBlurred) {
                    model.pageBlurred = true;
                    utils.blurPage();
                }
            }
        );
    }

    licenseKeyChanged(value) {
        model.key = value.trim();
        model.setError('licenseKey', false);
        controller.renderView();
    }

    activateClicked() {
        dots.start('activate-license-button-id');
        controller.renderView();
        model.activateLicense(controller.activateLicenseResult);
    }

    activateLicenseResult(success, licenseExpiration) {
        if (success) {
            controller.cancelClicked();
            model.activateCallback(licenseExpiration, model.key);
        } else {
            dots.stop();
            controller.renderView();
        }

    }

    cancelClicked() {
        let container = document.getElementById('activate-license-modal-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
        utils.unblurPage();
    }

}

let controller = new ActivateLicenseController();
module.exports = controller;
