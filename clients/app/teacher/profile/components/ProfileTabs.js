import React from 'react';
import SuccessIcon from 'SuccessIcon';
import AlertIcon from 'AlertIcon';
import CloseX from 'CloseX';
import Constants from 'Constants';
import platform from 'Platform';
import {translate} from 'translator';

let e = React.createElement;

export default class ProfileTabs extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            router = this.props.router;
        
        let tabChildren = [
            e('li', {
                className: this.props.selectedTab === 'info' ? 'selected' : '',
                children: translate('PROFILE'),
                onClick: router.routeToProfileInfo
            }),
            e('li', {
                className: this.props.selectedTab === 'demo' ? 'selected' : '',
                children: translate('DEMOGRAPHICS'),
                onClick: router.routeToProfileDemo
            })
        ];
        
        if (!platform.isIosApp) {
            tabChildren.push(
                e('li', {
                    className: this.props.selectedTab === 'billing' ? 'selected' : '',
                    children: translate('ACCOUNT'),
                    onClick: router.routeToProfileBilling
                })
            );
        }
        
        let allChildren = [];
        
        if (m.banner.message) {
            let bannerClass = m.banner.type === Constants.BANNER_TYPE_SUCCESS ? 'profile-success-banner' : 'profile-error-banner',
                Icon = m.banner.type === Constants.BANNER_TYPE_SUCCESS ? SuccessIcon : AlertIcon;
            
            let bannerChildren = [
                e(Icon, {
                    className: 'profile-banner-icon',
                    color: '#ffffff'
                }),
                e('span', {
                    className: 'profile-banner-text',
                    children: m.banner.message
                })
            ];
            
            if (m.banner.type === Constants.BANNER_TYPE_ERROR) {
                bannerChildren.push(
                    e('div', {
                        className: 'profile-banner-error-close',
                        children: [
                            e(CloseX, {
                                className: 'profile-banner-error-close-x'
                            })
                        ],
                        onClick: c.closeBannerClicked
                    })
                );
            }
            allChildren.push(
                e('div', {
                    className: bannerClass,
                    children: bannerChildren
                })
            );
        }
        
        allChildren.push(
            e('ul', {
                className: 'profile-nav',
                children: tabChildren
            })
        );
        
        return e('div', {
            className: 'profile-tabs',
            children: allChildren
        });
    }
    
}
