import Backbone from 'backbone';
import React from 'react';
import ReactDOM from 'react-dom';
import PasswordModel from 'PasswordModel';
import view from 'ResetPasswordView';
import dots from 'Dots';
import popup from 'PopupController';
import store from 'store';
import Constants from 'Constants';
import {translate} from 'translator';

let router = null,
    model = null;

class ResetPasswordController {
    
    doResetPassword(teacherLoginRouter, token) {
        router = teacherLoginRouter;
        
        model = new PasswordModel();
        model.token = token;
        model.creating = /create-password/.test(Backbone.history.fragment);
        
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('body')
        );
    }
    
    passwordChanged(id, value) {
        if (id === 'password') {
            model.password = value;
            model.setError('password', value.length === 0);
        } else {
            model.password2 = value;
            model.setError('password2', value.length === 0);
        }
        
        controller.renderView();
    }
    
    submitClicked() {
        if (!model.isValidPassword()) {
            model.setError('password', true, translate('Please use 8 or more characters.'));
        } else if (!model.isPasswordMatch()) {
            model.setError('password2', true, translate('The passwords must match.'));
        } else if (!model.isValidToken()) {
            popup.render({
                title: translate('Invalid'),
                message: translate('This password link is invalid. Please check the link and try again.')
            });
        } else {
            dots.start('submitNewPassword');
            model.submitNewPassword(controller.submitNewPasswordResult);
        }
        
        controller.renderView();
    }
    
    submitNewPasswordResult(error) {
        let title = translate('Success'),
            message = model.creating ? translate('Your password has been created.') : translate('Your password has been changed.'),
            buttonClicked = controller.routeAfterSuccess,
            cancelClicked = controller.routeAfterSuccess;
        
        if (error === Constants.INVALID_CHANGE_PASSWORD_TOKEN) {
            title = translate('Invalid');
            message = translate('This password link is invalid. Please check the link and try again.');
            buttonClicked = () => {};
            cancelClicked = () => {};
        } else if (error === Constants.TEMPORARY_TOKEN_EXPIRED) {
            title = translate('Expired');
            message = translate('This password link has expired. Please request a new password link.');
            buttonClicked = () => router.routeToForgotPassword();
            cancelClicked = () => router.routeToForgotPassword()
        } else if (error) {
            title = translate('Unknown Error');
            message = translate('An unknown error occurred. Please try again later.');
            buttonClicked = () => {};
            cancelClicked = () => {};
        }
        
        popup.render({
            title: title,
            message: message,
            buttonClicked: buttonClicked,
            cancelClicked: cancelClicked
        });
    }
    
    routeAfterSuccess() {
        if (store.get('resetPasswordFrom') === 'getPro') {
            router.routeToRegisterProAccount(store.get('proType') || 'k12');
            store.remove('resetPasswordFrom');
        } else {
            router.routeToTeacherLogin();
        }
    }
    
}

let controller = new ResetPasswordController();
module.exports = controller;
