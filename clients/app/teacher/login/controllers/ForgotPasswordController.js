import React from 'react';
import ReactDOM from 'react-dom';
import PasswordModel from 'PasswordModel';
import view from 'ForgotPasswordView';
import popup from 'PopupController';
import dots from 'Dots';
import {translate} from 'translator';

let router = null,
    model = null;

class ForgotPasswordController {
    
    doForgotPassword(teacherLoginRouter) {
        router = teacherLoginRouter;
        model = new PasswordModel();
        controller.renderView();
    }

    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('body')
        );
    }

    emailChanged(value) {
        model.email = value;
        model.setError('email', model.email === '');
        controller.renderView();
    }

    submitClicked() {
        if (model.isValidEmail()) {
            dots.start('resetPwdButton');
            model.getResetLink(controller.getResetLinkResult);
        } else {
            model.setError('email', true, translate('Please enter a valid email.'));
        }
        
        controller.renderView();
    }

    getResetLinkResult(success) {
        if (success) {
            popup.render({
                title: translate('Check Email'),
                message: translate('Please check your email for a password reset link.')
            });
        } else {
            dots.stop();
        }
        
        controller.renderView();
    }
    
}

let controller = new ForgotPasswordController();
module.exports = controller;
