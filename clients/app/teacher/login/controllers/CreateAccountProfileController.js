import React from 'react';
import ReactDOM from 'react-dom';
import AccountModel from 'AccountModel';
import teacher from 'TeacherModel';
import view from 'CreateAccountProfileView';
import dots from 'Dots';

let router = null,
    model = teacher.account;

class CreateAccountProfileController {
    
    doCreateAccountProfile(teacherLoginRouter) {
        router = teacherLoginRouter;
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('body')
        );
    }

    firstNameChanged(firstName) {
        model.firstName = firstName;
        model.errors.firstName.error = false;
        controller.renderView();
    }
    
    lastNameChanged(lastName) {
        model.lastName = lastName;
        model.errors.lastName.error = false;
        controller.renderView();
    }
    
    emailChanged(email) {
        model.email = email.toLowerCase().trim();
        model.errors.email.error = false;
        model.emailVerified = false;
        controller.renderView();
    }
    
    email2Changed(email2) {
        model.email2 = email2.toLowerCase().trim();
        model.errors.email2.error = false;
        controller.renderView();
    }
    
    passwordChanged(password) {
        model.password = password;
        model.errors.password.error = false;
        controller.renderView();
    }
    
    password2Changed(password2) {
        model.password2 = password2;
        model.errors.password2.error = false;
        controller.renderView();
    }
    
    nextClicked() {
        dots.start('createAccountProfileNextButton');
        model.validateCreateProfile(controller.profileValidationResult);
    }
    
    profileValidationResult() {
        dots.stop();
        
        if (model.errors.firstName.error ||
            model.errors.lastName.error ||
            model.errors.email.error ||
            model.errors.email2.error ||
            model.errors.password.error ||
            model.errors.password2.error) {
            controller.renderView();
        } else {
            router.routeToRegisterDemo();
        }
    }
    
    cancelClicked() {
        model = teacher.account = new AccountModel();
        router.routeToIndex();
    }
}

let controller = new CreateAccountProfileController();
module.exports = controller;
