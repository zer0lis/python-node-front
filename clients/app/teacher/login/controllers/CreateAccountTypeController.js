import React from 'react';
import ReactDOM from 'react-dom';
import teacher from 'TeacherModel';
import view from 'CreateAccountTypeView';
import activateLicenseModal from 'ActivateLicenseController';
import goPro from 'GoProController';
import dots from 'Dots';
import Constants from 'Constants';
import popup from 'PopupController';
import user from 'user';
import utils from 'Utils';
import {translate} from 'translator';

let router = null,
    model = teacher.account,
    licenseModel = model.licenseModel;

class CreateAccountTypeController {
    
    doAccountType(teacherLoginRouter) {
        router = teacherLoginRouter;
        
        if (!model.email) {
            router.routeToTeacherLaunch();
            return;
        }
        
        model.step3Status = Constants.STATUS_SELECTED;
        model.headerText = translate('Account Type');
        model.subHeaderText = translate('Please select an account type:');
        
        licenseModel.buyerId = user.getId();
        licenseModel.seats = licenseModel.seats || 1;
        licenseModel.years = licenseModel.years || 1;
        licenseModel.calculatePriceData();
        
        let renewalDate = new Date();
        renewalDate.setFullYear(renewalDate.getFullYear() + 1);
        licenseModel.renewalDate = renewalDate;
        
        if (user.wantsPro()) {
            user.setWantsPro(false);
            controller.proClicked();
        } else {
            controller.renderView();
        }
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model,
                    registration: true
                }
            ),
            document.getElementById('body')
        );
    }
    
    freeClicked() {
        model.step4Status = null;
        model.selectedAccountType = Constants.FREE_USER;
        controller.renderView();
    }
    
    finishClicked() {
        if (model.selectedAccountType === Constants.FREE_USER) {
            dots.start('register-finish-button');
            router.routeToTeacherLaunchAndReplace();
        }
    }
    
    proClicked() {
        model.step4Status = Constants.STATUS_IDLE;
        model.selectedAccountType = Constants.PRO_USER;
        controller.renderView();
    }
    
    enterLicenseKeyClicked() {
        activateLicenseModal.render({
            activateCallback: (success) => {
                if (success) {
                    user.setFirstTimePro(true);
                    router.routeToTeacherLaunchAndReplace();
                }
            }
        });
    }
    
    inputChanged(id, value) {
        if (id === 'licenseSeats') {
            licenseModel.seats = parseInt(value);
            if (isNaN(licenseModel.seats)) {
                licenseModel.seats = ''; // Allow an empty input so the user needn't select the current value first.
            }
            if (licenseModel.isValidSeats()) {
                controller.seatsBlurred();
            }
        } else if (id === 'couponName') {
            licenseModel.couponName = value;
            licenseModel.setError('couponName', false);
        } else if (id === 'nameOnCard') {
            licenseModel.nameOnCard = value;
            licenseModel.setError('nameOnCard', !licenseModel.isValidNameOnCard());
        } else if (id === 'cardNumber') {
            licenseModel.cardType = Stripe.card.cardType(value);
            licenseModel.cardNumber = value;
            licenseModel.setError('cardNumber', !licenseModel.isValidCardNumber());
        } else if (id === 'cvc') {
            licenseModel.cvc = value;
            licenseModel.setError('cvc', !licenseModel.isValidCvc());
        }
        controller.renderView();
    }
    
    seatsBlurred() {
        if (!licenseModel.seats || licenseModel.seats < 1) {
            licenseModel.seats = 1;
        } else if (licenseModel.seats > Constants.MAX_DISPLAY_SEATS) {
            licenseModel.seats = Constants.MAX_DISPLAY_SEATS;
        }
        
        licenseModel.calculatePriceData();
        controller.renderView();
    }
    
    otherPaymentOptionsClicked() {
        popup.render({
            title: translate('Payment Options'),
            message: translate('Would you like to request more information about payment options and bulk pricing?'),
            buttonText: translate('Yes'),
            cancelText: translate('No'),
            buttonClicked: () => {
                let role = utils.encodeForUrl(model.role[0] || ''),
                    firstName = utils.encodeForUrl(model.firstName || ''),
                    lastName = utils.encodeForUrl(model.lastName || ''),
                    zip = utils.encodeForUrl(model.zipCode || ''),
                    orgType = utils.encodeForUrl(model.orgType || ''),
                    orgName = utils.encodeForUrl(model.schoolName || model.orgName || ''),
                    email = utils.encodeForUrl(model.email || ''),
                    seats = utils.encodeForUrl(licenseModel.seats || ''),
                    years = utils.encodeForUrl(licenseModel.years || '');
                window.open(`${marketing_host}/pricing.html?role=${role}&firstName=${firstName}&lastName=${lastName}&zip=${zip}&orgType=${orgType}&orgName=${orgName}&email=${email}&seats=${seats}&years=${years}#bulk`);
            }
        });
    }
    
    yearsChanged(years) {
        licenseModel.years = years;
        let renewalDate = new Date();
        renewalDate.setFullYear(renewalDate.getFullYear() + years);
        licenseModel.renewalDate = renewalDate;
        licenseModel.calculatePriceData();
        controller.renderView();
    }
    
    applyCouponClicked() {
        if (!licenseModel.isValidCouponName()) {
            dots.stop();
            licenseModel.setError('couponName', true);
        } else {
            dots.start('applyCouponButton');
            licenseModel.fetchCoupon(controller.couponFetched);
        }
        controller.renderView();
    }
    
    couponFetched(success) {
        dots.stop();
        if (success) {
            licenseModel.calculatePriceData();
        }
        controller.renderView();
    }
    
    clearCouponClicked() {
        licenseModel.coupon = null;
        licenseModel.couponName = '';
        licenseModel.couponSuccess = false;
        licenseModel.calculatePriceData();
        controller.renderView();
    }
    
    applyNowChanged() {
        licenseModel.applyNow = !licenseModel.applyNow;
        controller.renderView();
    }
    
    autoRenewChanged() {
        licenseModel.autoRenew = !licenseModel.autoRenew;
        controller.renderView();
    }
    
    reviewAndPayClicked() {
        if (licenseModel.isValidCouponName() && !licenseModel.couponSuccess) { // The user forgot to apply the coupon.
            dots.start('reviewAndPayButton');
            licenseModel.fetchCoupon((success) => {
                dots.stop();
                if (success) {
                    licenseModel.calculatePriceData();
                    model.step3Status = Constants.STATUS_COMPLETED;
                    model.step4Status = Constants.STATUS_SELECTED;
                    model.headerText = translate('Review & Pay');
                    model.subHeaderText = '';
                    licenseModel.licenseStep = Constants.PAYMENT_STEP;
                }
                controller.renderView();
            });
        } else {
            model.step3Status = Constants.STATUS_COMPLETED;
            model.step4Status = Constants.STATUS_SELECTED;
            model.headerText = translate('Review & Pay');
            model.subHeaderText = '';
            licenseModel.licenseStep = Constants.PAYMENT_STEP;
            controller.renderView();
        }
    }
    
    monthChanged(value) {
        licenseModel.month = value;
        licenseModel.setError('month', false);
        controller.renderView();
    }
    
    yearChanged(value) {
        licenseModel.year = value;
        licenseModel.setError('year', false);
        controller.renderView();
    }
    
    previousClicked() {
        model.step3Status = Constants.STATUS_SELECTED;
        model.step4Status = Constants.STATUS_IDLE;
        model.headerText = translate('Account Type');
        model.subHeaderText = translate('Please select an account type:');
        licenseModel.licenseStep = Constants.LICENSE_STEP;
        controller.renderView();
    }
    
    purchaseClicked() {
        if (licenseModel.canPurchase()) {
            model.stripeError = '';
            dots.start('licensePurchaseButton');
            licenseModel.makePurchase(controller.purchaseResult);
        } else {
            dots.stop();
        }
        controller.renderView();
    }
    
    purchaseResult(success, error) {
        if (!success) {
            if (error === Constants.PRICE_MISMATCH) {
                popup.render({
                    title: translate('Price Change'),
                    message: translate('Our prices recently changed. The total for your order has been updated.')
                });
            } else if (error === Constants.INTERNAL_SERVER_ERROR) {
                controller.showErrorBanner(translate('There was a problem completing your purchase or activating your license. Please contact Socrative Support.'));
            } else {
                controller.showErrorBanner(error);
            }
        } else {
            licenseModel.licenseStep = Constants.COMPLETE_STEP;
            model.headerText = '';
            if (licenseModel.applyNow) {
                user.setFirstTimePro(true);
            }
        }
        controller.renderView();
    }
    
    myAccountClicked() {
        router.routeToTeacherLaunchAndReplace();
    }
    
    showErrorBanner(message) {
        dots.stop();
        
        if (!message) {
            message = translate('Check your credit card information and try again.');
        }
        
        model.stripeError = message;
        controller.renderView();
        
        if (window.innerWidth >= Constants.BREAKPOINT_TABLET) {
            document.body.scrollTop = 340;
        }
    }
    
    closeBannerClicked() {
        model.stripeError = '';
        controller.renderView();
    }

    learnMoreClicked() {
        goPro.render({hideGoProButton: true});
    }
    
}

let controller = new CreateAccountTypeController();
module.exports = controller;
