import Backbone from 'backbone';
import React from 'react';
import ReactDOM from 'react-dom';
import LoginModel from 'LoginModel';
import user from 'user';
import room from 'room';
import dots from 'Dots';
import view from 'LoginView';
import {translate} from 'translator';

let router = null,
    model = null;

class LoginController {
    
    doLogin(teacherLoginRouter) {
        router = teacherLoginRouter;
        model = new LoginModel();
        user.setModeSilent('teacher');
        
        if (Backbone.history.fragment === 'zendesk-sso') {
            model.signInZendesk(controller.zendeskSignInResult);
        } else {
            room.join({
                role: 'teacher',
                success: () => {
                    user.setWantsPro(Backbone.history.fragment === 'get-pro');
                    controller.routeToTeacher();
                },
                error: () => {
                    user.logout(() => {
                        user.setWantsPro(Backbone.history.fragment === 'get-pro');
                    });
                    controller.renderView();
                }
            });
        }
    }
    
    doLogout(teacherLoginRouter) {
        user.logout(function() {
            teacherLoginRouter.routeToIndex();
        });
    }
    
    zendeskSignInResult(url) {
        if (url) {
            window.location.href = url; // Redirect to a Zendesk URL provided by the back end.
        } else {
            dots.stop();
            model.setEmailError(translate('Your email or password is incorrect.'));
            controller.renderView();
        }
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.body // TODO .... When we migrate to ES6, render to a <div> in the body instead of directly to the body (React discourages rendering directly to the body).
        );
    }
    
    emailChanged(email) {
        model.email = email;
        model.errors.email.error = false;
        controller.renderView();
    }
    
    passwordChanged(password) {
        model.password = password;
        model.errors.email.error = false;
        model.errors.password.error = false;
        controller.renderView();
    }
    
    signInClicked() {
        if (model.email.length === 0) {
            model.errors.email.error = true;
            controller.renderView();
        } else if (!model.isValidEmail()) {
            model.setEmailError(translate('Please enter a valid email.'));
            controller.renderView();
        } else if (!model.isValidLegacyPassword()) {
            model.errors.password.error = true;
            controller.renderView();
        } else {
            dots.start('teacherLoginButton');
            if (Backbone.history.fragment === 'zendesk-sso') {
                model.signInZendesk(controller.zendeskSignInResult);
            } else {
                model.signIn(controller.signInResult);
            }
        }
    }
    
    signInResult(data) {
        if (data) {
            user.set(data);
            user.setMode('teacher');
            controller.routeToTeacher();
        } else {
            dots.stop();
            controller.renderView();
        }
    }
    
    routeToTeacher() {
        if (user.getSharedSocNumber()) {
            router.routeToImportQuiz(user.getSharedSocNumber());
        } else {
            router.routeToTeacherLaunch();
        }
    }
    
    googleSignInClicked() {
        if (Backbone.history.fragment === 'zendesk-sso') {
            router.routeToOAuthZendeskLogin();
        } else {
            router.routeToOAuthLogin();
        }
    }
    
}

let controller = new LoginController();
module.exports = controller;
