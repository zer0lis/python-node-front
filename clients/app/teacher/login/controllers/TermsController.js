import React from 'react';
import ReactDOM from 'react-dom';
import view from 'TermsView';
import utils from 'Utils';

class TermsController {
    
    open(agreeClicked) {
        let termsContainer = document.createElement('div');
        termsContainer.id = 'terms-modal-container';
        document.body.appendChild(termsContainer);
        
        ReactDOM.render(
            React.createElement(
                view, {
                    agreeClicked: () => {
                        controller.cancelClicked();
                        agreeClicked();
                    },
                    cancelClicked: controller.cancelClicked
                }
            ),
            termsContainer,
            () => {
                utils.blurPage();
            }
        );
    }
    
    cancelClicked() {
        let termsContainer = document.getElementById('terms-modal-container');
        ReactDOM.unmountComponentAtNode(termsContainer);
        termsContainer.parentNode.removeChild(termsContainer);
        utils.unblurPage();
    }
    
}

let controller = new TermsController();
module.exports = controller;
