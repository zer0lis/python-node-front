import React from 'react';
import ReactDOM from 'react-dom';
import teacher from 'TeacherModel';
import view from 'CreateAccountDemographicsView';
import termsModal from 'TermsController';
import user from 'user';
import room from 'room';
import platform from 'Platform';
import dots from 'Dots';
import Constants from 'Constants';
import {translate} from 'translator';

let router = null,
    model = teacher.account;

class CreateAccountDemographicsController {
    
    doCreateAccountDemographics(teacherLoginRouter) {
        router = teacherLoginRouter;
        
        if (!model.emailVerified) {
            router.routeToRegisterInfo();
        } else if (user.isFirstTime()) {
            // The user clicked the browser's back button at the account type step. Reset that view
            // and send them back to it so they can't try to create their account again (SOC-2021).
            model.step4Status = Constants.STATUS_IDLE;
            model.selectedAccountType = null;
            model.licenseModel.licenseStep = Constants.LICENSE_STEP;
            router.routeToRegisterAccount();
        } else {
            controller.determineVisible();
        }
    }
    
    determineVisible() {
        let visible = {country: true, number: 1};
        
        if (model.country) {
            if (model.isCountryUS()) {
                visible.usOrgType = true;
                visible.number++;
                if (model.isK12()) {
                    visible.zipCode = true;
                    visible.number++;
                    if (model.isValidZipCode()) {
                        visible.schoolsByZip = true;
                        visible.number++;
                        if (model.isSchoolSelected()) {
                            visible.state = true;
                            visible.role = true;
                            visible.number += 2;
                        } else if (model.isSchoolNotListed()) {
                            visible.state = true;
                            visible.schoolName = true;
                            visible.role = true;
                            visible.number += 3;
                        }
                    }
                } else if (model.isHigherEd()) {
                    visible.state = true;
                    visible.number++;
                    if (model.state) {
                        visible.orgName = true;
                        visible.role = true;
                        visible.number += 2;
                    }
                } else if (model.orgType) {
                    visible.orgName = true;
                    visible.role = true;
                    visible.number += 2;
                }
            } else {
                visible.intlOrgType = true;
                visible.number++;
                if (model.isK12()) {
                    visible.schoolsByCountry = true;
                    visible.number++;
                    if (model.isSchoolSelected()) {
                        visible.role = true;
                        visible.number++;
                    } else if (model.isSchoolNotListed()) {
                        visible.schoolName = true;
                        visible.role = true;
                        visible.number += 2;
                    }
                } else if (model.orgType) {
                    visible.orgName = true;
                    visible.role = true;
                    visible.number += 2;
                }
            }
        }
        
        model.demoVisible = visible;
        dots.stop();
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('body')
        );
    }
    
    countryChanged(country) {
        model.country = country;
        model.errors.country.error = false;
        
        // The country changed. Reset everything after it.
        model.orgType = '';
        model.orgName = '';
        model.zipCode = '';
        model.schoolId = null;
        model.state = '';
        model.schoolName = '';
        
        controller.determineVisible();
    }
    
    orgTypeChanged(orgType) {
        model.orgType = orgType;
        model.errors.orgType.error = false;
        
        // The org type changed. Reset everything after it.
        model.orgName = '';
        model.zipCode = '';
        model.schoolId = null;
        model.state = '';
        model.schoolName = '';
        
        if (model.isCountryUS()) {
            controller.determineVisible();
        } else {
            if (model.isK12()) {
                model.loadingByCountry = true;
                controller.determineVisible();
                model.fetchSchoolsByCountry(controller.determineVisible);
            } else {
                controller.determineVisible();
            }
        }
    }
    
    schoolByCountryChanged(idAndName) {
        let parts = idAndName.split('_____'),
            id = parseInt(parts[0]),
            name = parts[1];
        
        model.schoolId = id;
        model.errors.schoolId.error = false;
        model.schoolName = id === Constants.SCHOOL_NOT_LISTED ? '' : name;
        
        controller.determineVisible();
    }
    
    schoolNameChanged(name) {
        model.schoolName = name;
        model.errors.schoolName.error = false;
        controller.determineVisible();
    }
    
    zipCodeChanged(zipCode) {
        if (zipCode === '' || /^\d+$/.test(zipCode)) {
            model.zipCode = zipCode;
            model.errors.zipCode.error = false;
            
            // The zip code changed. Reset everything after it.
            model.schoolId = null;
            model.state = '';
            model.schoolName = '';
            
            if (model.isValidZipCode()) {
                model.loadingByZip = true;
                model.fetchSchoolsByZip(controller.determineVisible);
            }
        } else {
            model.errors.zipCode.error = true;
            model.errors.zipCode.message = translate('Please enter a 5-digit zip code.');
        }
        
        controller.determineVisible();
    }
    
    schoolByZipChanged(id) {
        model.schoolId = id;
        model.errors.schoolId.error = false;
        
        // The domestic school changed. Reset everything after it.
        model.state = '';
        model.schoolName = '';
        
        if (model.isCountryUS() && model.isK12()) {
            model.setDataFromSchool();
        }
        
        controller.determineVisible();
    }
    
    stateChanged(state) {
        model.state = state;
        model.errors.state.error = false;
        controller.determineVisible();
    }
    
    roleChanged(role) {
        model.role = [role];
        model.errors.role.error = false;
        controller.determineVisible();
    }
    
    orgNameChanged(orgName) {
        model.orgName = orgName;
        model.errors.orgName.error = false;
        controller.determineVisible();
    }
    
    termsLinkClicked() {
        termsModal.open(controller.agreeClicked);
    }
    
    agreeClicked() {
        model.termsChecked = true;
        model.errors.terms.error = false;
        controller.determineVisible();
    }
    
    termsChanged() {
        model.termsChecked = !model.termsChecked;
        model.errors.terms.error = !model.termsChecked;
        controller.determineVisible();
    }
    
    previousClicked() {
        router.routeToRegisterInfo();
    }
    
    createAccountClicked() {
        if (!model.validateDemographics(true)) {
            controller.determineVisible();
        } else {
            dots.start('createAccountButton');
            model.createAccount(controller.createAccountResult);
        }
    }
    
    createAccountResult(id) {
        if (id) {
            user.setMode('teacher');
            user.setFirstTime(true);
            user.setId(id);
            room.join({
                role: 'teacher',
                success: controller.roomJoinSuccess
            });
        }
    }
    
    roomJoinSuccess() {
        if (platform.isIosApp) {
            router.routeToTeacherLaunch();
        } else {
            model.licenseModel.fetchPrices(controller.pricesFetched);
        }
    }
    
    pricesFetched(success) {
        if (success) {
            dots.stop();
            router.routeToRegisterAccount();
        } else {
            router.routeToTeacherLaunch();
        }
    }
    
}

let controller = new CreateAccountDemographicsController();
module.exports = controller;
