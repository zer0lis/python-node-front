import Constants from 'Constants';
import model from 'GoogleLoginModel';
import React from 'react';
import ReactDOM from 'react-dom';
import user from 'user';
import view from 'GoogleLoginView';
import {translate} from 'translator';
import popup from 'PopupController';

let router = null;

class GoogleLoginController {
    
    doOAuthLogin(teacherLoginRouter, zendeskLogin) {
        router = teacherLoginRouter;
        model.zendeskLogin = zendeskLogin;
        model.submitForm = true;
        controller.renderView();
    }

    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    model: model
                }
            ),
            document.getElementById('body'),
            controller.viewRendered
        );
    }
    
    viewRendered() {
        if (model.submitForm) {
            model.submitForm = false;
            model.promptApproval = false;
            let form = document.getElementById('oauth_form');
            if (form) {
                let wantsPro = user.wantsPro();
                user.logout(() => {
                    if (wantsPro) {
                        user.setModeSilent('teacher');
                        user.setWantsPro(true);
                    }
                    form.submit();
                });
            }
        }
    }
    
    doLoginSuccess(teacherLoginRouter) {
        router = teacherLoginRouter;
        controller.routeToTeacher();
    }

    routeToTeacher() {
        if (user.getSharedSocNumber()) {
            router.routeToImportQuiz(user.getSharedSocNumber());
        } else {
            router.routeToTeacherLaunch();
        }
    }
    
    doOAuthError(teacherLoginRouter, errorCode) {
        router = teacherLoginRouter;
        user.logout();

        model.submitForm=false;
        controller.renderView();

        popup.render({
            title: translate("Google Error"),
            message: translate('There was a problem logging in with Google ({0}). Please try again or contact Socrative support.').format(decodeURIComponent(errorCode)),
            buttonClicked: () => {
                router.routeToTeacherLogin();
            },
            cancelClicked: () => {
                router.routeToTeacherLogin();
            }
        });
    }

    showCreateAccountPopup(teacherLoginRouter, email) {
        router = teacherLoginRouter;

        model.submitForm=false;
        controller.renderView();

        popup.render({
            title: translate("New Account"),
            message: translate('There is not yet a Socrative account for {0}. Would you like to create one now?').format(email),
            buttonText: translate('Create Account'),
            cancelText: translate('Cancel'),
            buttonClicked: () => {
                router.routeToRegisterInfo();
            },
            cancelClicked: () => {
                router.routeToTeacherLogin();
            }
        })
    }

    promptForApproval(teacherLoginRouter, state) {
        router = teacherLoginRouter;
        model.zendeskLogin = state === Constants.ZENDESK_SSO_STATE;
        model.promptApproval = true;
        model.submitForm = true;
        controller.renderView();
    }

}

let controller = new GoogleLoginController();
module.exports = controller;
