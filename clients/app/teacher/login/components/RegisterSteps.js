/*
    These are the supported statuses and their effects:
    
        'selected':  The step circle has a blue fill. The step number is white. There is no checkmark.
        'idle':      The step circle has a gray border and white fill. The step number is gray. There is no checkmark.
        'completed': The step circle has a gray fill. The step number is white. There is a checkmark.
        falsy:       The step is not displayed.
 */

import React from 'react';
import Check from 'RegisterStepCheck';
import {translate} from 'translator';

let e = React.createElement;

export default class RegisterSteps extends React.Component {
    
    render() {
        let step1Children = [1],
            step2Children = [2],
            step3Children = [3];
        
        if (this.props.step1Status === 'completed') {
            step1Children.push(e(Check));
        }
        
        if (this.props.step2Status === 'completed') {
            step2Children.push(e(Check));
        }
        
        if (this.props.step3Status === 'completed') {
            step3Children.push(e(Check));
        }
        
        let containerChildren = [
            e('div', {
                className: 'register-step',
                children: [
                    e('div', {
                        className: 'register-step-number-' + this.props.step1Status,
                        children: step1Children
                    })
                ]
            }),
            e('div', {
                className: 'register-step-divider-left'
            }),
            e('div', {
                className: 'register-step',
                children: [
                    e('div', {
                        className: 'register-step-number-' + this.props.step2Status,
                        children: step2Children
                    })
                ]
            })
        ];
        
        if (this.props.step3Status) {
            containerChildren.push(
                e('div', {
                    className: 'register-step-divider-right'
                }),
                e('div', {
                    className: 'register-step',
                    children: [
                        e('div', {
                            className: 'register-step-number-' + this.props.step3Status,
                            children: step3Children
                        })
                    ]
                })
            );
        }
        
        if (this.props.step4Status) {
            containerChildren.push(
                e('div', {
                    className: 'register-step-divider-right'
                }),
                e('div', {
                    className: 'register-step',
                    children: [
                        e('div', {
                            className: 'register-step-number-' + this.props.step4Status,
                            children: 4
                        })
                    ]
                })
            );
        }
        
        return e('div', {
            id: 'register-steps',
            children: [
                e('div', {
                    id: 'register-steps-header',
                    children: translate('NEW TEACHER ACCOUNT')
                }),
                e('div', {
                    id: 'register-steps-container',
                    children: containerChildren
                })
            ]
        });
    }
    
}
