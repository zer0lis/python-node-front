import React from 'react';
import LoginLogo from 'LoginLogo';
import {translate} from 'translator';

let e = React.createElement;

export default class RegisterHeader extends React.Component {
    
    render() {
        let children = [e(LoginLogo)];
        
        if (!this.props.onlyLogo) {
            children.push(
                e('div', {
                    children: [
                        e('div', {
                            className: 'teacher-info',
                            children: [
                                e('div', {
                                    children: [
                                        e('span', {
                                            className: 'teacher-info-row'
                                        },
                                        translate('Students do not need an account. ')),
                                        e('span', {
                                            className: 'teacher-info-row',
                                            children: [
                                                e('span', {}, translate("Join a teacher's classroom here: ")),
                                                e('a', {
                                                    href: '/login/student/'
                                                },
                                                translate('Student Login'))
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                })
            );
        }
        
        return e('div', {
            className: 'register-header' + (this.props.onlyLogo ? ' register-header-payment-complete' : ''),
            children: children
        })
    }
    
}
