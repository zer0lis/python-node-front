import React from 'react';

let e = React.createElement;

export default class RegisterStepCheck extends React.Component {
    
    render() {
        return e('div', {
            className: 'register-step-check-circle',
            children: [
                e('div', {
                    className: 'register-step-check',
                    dangerouslySetInnerHTML: {__html: '<svg width="12px" height="9px" viewBox="0 0 12 9"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"><g transform="translate(-444.000000, -256.000000)" stroke="#CCCCCC" stroke-width="2"><g transform="translate(394.000000, 251.000000)"><g transform="translate(51.000000, 6.000000)"><path d="M9.72826087,0.267558528 L3.47826087,6.42140468"></path><path d="M0.434782609,3.47826087 L3.47826087,6.42140468"></path></g></g></g></g></svg>'}
                })
            ]
        })
    }
    
}
