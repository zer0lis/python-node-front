import React from 'react';
import LoginLogo from 'LoginLogo';
import Input from 'Input';
import FooterView from 'FooterView';
import {translate} from 'translator';
import platform from 'Platform';

let e = React.createElement,
    c = null,
    m = null;

export default class LoginView extends React.Component {

    keyPressed(event) {
        if (event.key === 'Enter') {
            c.signInClicked();
        }
    }
    
    render() {
        c = this.props.controller;
        m = this.props.model;
        
        return e('div', {
            id: 'page-container',
            children: [
                e('div', {
                    id: 'main-content',
                    children: [
                        e(LoginLogo),
                        e('div', {
                            id: 'center-content',
                            className: 'vertical-align-middle',
                            children: [
                                e('div', {
                                    id: 'teacherLoginForm',
                                    className: 'form single-display',
                                    children: [
                                        e('div', {
                                            className: 'teacher-login-title',
                                            children: [
                                                e('h1', {
                                                    children: translate('Teacher Login')
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            children: [
                                                e(Input, {
                                                    autoFocus: true,
                                                    className: 'loginInput',
                                                    label: translate('Email'),
                                                    maxLength: 255,
                                                    onChange: (event) => c.emailChanged(event.target.value),
                                                    onKeyPress: (event) => this.keyPressed(event),
                                                    status: m.errors.email,
                                                    tooltipLocation: 'above',
                                                    type: 'email',
                                                    value: m.email
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            id: 'teacher-password',
                                            children: [
                                                e(Input, {
                                                    className: 'loginInput',
                                                    label: translate('Password'),
                                                    maxLength: 255,
                                                    onChange: (event) => c.passwordChanged(event.target.value),
                                                    onKeyPress: (event) => this.keyPressed(event),
                                                    status: m.errors.password,
                                                    type: 'password',
                                                    value: m.password
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            id: 'action-button-container',
                                            className: 'align-center',
                                            children: [
                                                e('button', {
                                                    id: 'teacherLoginButton',
                                                    className: 'button button-primary button-large action-button',
                                                    children: translate('SIGN IN'),
                                                    onClick: c.signInClicked
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            id: 'bottom-links',
                                            children: [
                                                e('a', {
                                                    href: '#forgot-password',
                                                    children: translate('Reset password')
                                                }),
                                                e('a', {
                                                    id: 'getAnAccount',
                                                    href: '#register/info',
                                                    children: translate('Create account')
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            className: 'google-sign-in' + (platform.isIosApp || platform.isAndroidApp ? ' hidden' : ''),
                                            children: [
                                                e('button', {
                                                    className: 'google-login-button teacher-login-google-button',
                                                    children: [
                                                        e('svg', {
                                                            className: 'google-login-icon',
                                                            width: '18px',
                                                            height: '18px',
                                                            viewBox: '0 0 18 18',
                                                            dangerouslySetInnerHTML: {__html: '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-519.000000, -574.000000)"><g transform="translate(474.000000, 169.000000)"><g transform="translate(34.000000, 392.000000)"><g transform="translate(11.000000, 13.000000)"><path d="M9,3.48 C10.69,3.48 11.83,4.21 12.48,4.82 L15.02,2.34 C13.46,0.89 11.43,0 9,0 C5.48,0 2.44,2.02 0.96,4.96 L3.87,7.22 C4.6,5.05 6.62,3.48 9,3.48 L9,3.48 Z" fill="#EA4335"></path><path d="M17.64,9.2 C17.64,8.46 17.58,7.92 17.45,7.36 L9,7.36 L9,10.7 L13.96,10.7 C13.86,11.53 13.32,12.78 12.12,13.62 L14.96,15.82 C16.66,14.25 17.64,11.94 17.64,9.2 L17.64,9.2 Z" fill="#4285F4"></path><path d="M3.88,10.78 C3.69,10.22 3.58,9.62 3.58,9 C3.58,8.38 3.69,7.78 3.87,7.22 L0.96,4.96 C0.35,6.18 0,7.55 0,9 C0,10.45 0.35,11.82 0.96,13.04 L3.88,10.78 L3.88,10.78 Z" fill="#FBBC05"></path><path d="M9,18 C11.43,18 13.47,17.2 14.96,15.82 L12.12,13.62 C11.36,14.15 10.34,14.52 9,14.52 C6.62,14.52 4.6,12.95 3.88,10.78 L0.97,13.04 C2.45,15.98 5.48,18 9,18 L9,18 Z" fill="#34A853"></path><polygon points="0 0 18 0 18 18 0 18"></polygon></g></g></g></g></g>'}
                                                        }),
                                                        translate('Sign in with Google')
                                                    ],
                                                    onClick: c.googleSignInClicked
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                }),
                e('footer', {
                    id: 'footer',
                    className: 'vertical-align',
                    children: e(FooterView)
                })
            ]
        });
    }

}
