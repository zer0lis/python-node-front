import React from 'react';
import Logo from 'LoginLogo';
import Input from 'Input';
import Footer from 'FooterView';
import {translate} from 'translator';

let e = React.createElement,
    c = null,
    m = null;

export default class ResetPasswordView extends React.Component {
    
    keyPressed(event) {
        if (event.key === 'Enter') {
            this.passwordInput.blur();
            this.password2Input.blur();
            c.submitClicked();
        }
    }
    
    render() {
        c = this.props.controller;
        m = this.props.model;
        
        return e('div', {
            id: 'page-container',
            children: [
                e('div', {
                    id: 'main-content',
                    children: [
                        e(Logo),
                        e('div', {
                            id: 'center-content',
                            className: 'vertical-align-middle',
                            children: [
                                e('div', {
                                    id: 'teacherLoginForm',
                                    className: 'form single-display',
                                    children: [
                                        e('div', {
                                            className: 'teacher-login-title',
                                            children: [
                                                e('h1', {
                                                    children: m.creating ? translate('Create Password') : translate('Reset Password')
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            id: 'teacher-password',
                                            children: [
                                                e(Input, {
                                                    autoFocus: true,
                                                    className: 'loginInput',
                                                    id: 'password',
                                                    label: translate('New Password'),
                                                    maxLength: 255,
                                                    onChange: (event) => c.passwordChanged(event.target.id, event.target.value),
                                                    onKeyPress: (event) => this.keyPressed(event),
                                                    refValue: (input) => {this.passwordInput = input},
                                                    status: m.errors.password,
                                                    tooltipLocation: 'above',
                                                    type: 'password',
                                                    value: m.password
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            id: 'teacher-password2',
                                            children: [
                                                e(Input, {
                                                    className: 'loginInput',
                                                    id: 'password2',
                                                    label: translate('Confirm New Password'),
                                                    maxLength: 255,
                                                    onChange: (event) => c.passwordChanged(event.target.id, event.target.value),
                                                    onKeyPress: (event) => this.keyPressed(event),
                                                    refValue: (input) => {this.password2Input = input},
                                                    status: m.errors.password2,
                                                    type: 'password',
                                                    value: m.password2
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            id: 'action-button-container',
                                            className: 'align-center',
                                            children: [
                                                e('button', {
                                                    id: 'submitNewPassword',
                                                    className: 'button button-large button-primary action-button',
                                                    children: translate('SUBMIT'),
                                                    onClick: c.submitClicked
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                }),
                e('footer', {
                    id: 'footer',
                    className: 'vertical-align',
                    children: e(Footer)
                })
            ]
        });
    }
    
}
