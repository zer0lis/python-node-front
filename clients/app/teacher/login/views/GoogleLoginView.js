import React from 'react';
import LoginLogo from 'LoginLogo';
import FooterView from 'FooterView';
import {translate} from 'translator';

let e = React.createElement;

export default class GoogleLoginView extends React.Component {
    
    render() {
        let m = this.props.model;
        
        let promptInput =  e('input', {
            type: 'hidden',
            name: 'approval_prompt',
            value: 'force'
        });
        
        return e('div', {
            id: 'page-container',
            children: [
                e('div', {
                    id: 'main-content',
                    className: 'vertical-align',
                    children: [
                        e(LoginLogo),
                        e('style', {
                            dangerouslySetInnerHTML: {__html:
                                'body, html {' +
                                '    width: 100%;' +
                                '    height: 100%;' +
                                '    overflow: hidden;' +
                                '    font-family: "Open Sans", sans-serif;' +
                                '    font-weight: 400;' +
                                '}'
                            }
                        }),
                        e('div', {
                            className: 'wait_container',
                            children: [
                                e('div', {
                                    style: {align: 'right', padding: '4em', 'margin-top': '50px'},
                                    children: [
                                        e('h1', {
                                            children: translate('Please wait...')
                                        })
                                    ]
                                }),
                                e('form', {
                                    id: 'oauth_form',
                                    name: 'oauth_form',
                                    method: 'post',
                                    action: 'https://accounts.google.com/o/oauth2/auth',
                                    children: [
                                        e('input', {
                                            type: 'hidden',
                                            name: 'response_type',
                                            value: 'code'
                                        }),
                                        e('input', {
                                            type: 'hidden',
                                            name: 'client_id',
                                            value: m.clientId
                                        }),
                                        e('input', {
                                            type: 'hidden',
                                            name: 'redirect_uri',
                                            value: m.redirectUri
                                        }),
                                        e('input', {
                                            type: 'hidden',
                                            name: 'scope',
                                            value: 'email profile https://www.googleapis.com/auth/drive'
                                        }),
                                        e('input', {
                                            type: 'hidden',
                                            name: 'state',
                                            value: m.zendeskLogin ? 'zendesk-sso' : 'oauthlogin'
                                        }),
                                        e('input', {
                                            type: 'hidden',
                                            name: 'access_type',
                                            value: 'offline'
                                        }),
                                        m.promptApproval ? promptInput : ''
                                    ]
                                })
                            ]
                        })
                    ]
                }),
                e('footer', {
                    id: 'footer',
                    className: 'vertical-align',
                    children: e(FooterView)
                })
            ]
        });
    }
    
}
