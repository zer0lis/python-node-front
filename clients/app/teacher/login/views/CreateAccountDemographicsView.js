import React from 'react';
import RegisterHeader from 'RegisterHeader';
import RegisterSteps from 'RegisterSteps';
import countries from 'countries';
import states from 'states';
import Select from 'Select';
import Input from 'Input';
import Tooltip from 'Tooltip';
import AlertIcon from 'AlertIcon';
import FooterView from 'FooterView';
import platform from 'Platform';
import Constants from 'Constants';
import {translate} from 'translator';

let e = React.createElement;

export default class CreateAccountDemographicsView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        let countryOptions = [{
            selected: 'US' === m.country,
            title: 'USA',
            value: 'US'
        }];
        
        for (let country of countries) {
            countryOptions.push({
                selected: country === m.country,
                title: country,
                value: country
            });
        }
        
        let demoChildren = [
            e('div', {
                className: 'register-select-container',
                children: [
                    e('p', {
                        className: 'demo-select-label',
                        children: translate('Country')
                    }),
                    e(Select, {
                        error: m.errors.country.error,
                        label: translate('Select Your Country'),
                        children: countryOptions,
                        onSelect: (name, value) => c.countryChanged(value)
                    })
                ]
            })
        ];
        
        if (m.demoVisible.usOrgType || m.demoVisible.intlOrgType) {
            let orgTitleK12  = m.demoVisible.usOrgType ? translate('K-12')      : translate('Primary/Secondary School'),
                orgTitleUSHE = m.demoVisible.usOrgType ? translate('Higher Ed') : translate('University');
            
            let orgTypeOptions = [
                {title: orgTitleK12,            value: Constants.ORGANIZATION_K12,       selected: Constants.ORGANIZATION_K12       === m.orgType},
                {title: orgTitleUSHE,           value: Constants.ORGANIZATION_HIGHER_ED, selected: Constants.ORGANIZATION_HIGHER_ED === m.orgType},
                {title: translate('Corporate'), value: Constants.ORGANIZATION_CORP,      selected: Constants.ORGANIZATION_CORP      === m.orgType},
                {title: translate('Other'),     value: Constants.ORGANIZATION_OTHER,     selected: Constants.ORGANIZATION_OTHER     === m.orgType}
            ];
            
            demoChildren.push(
                e('div', {
                    className: 'register-select-container register-margin-left',
                    children: [
                        e('p', {
                            className: 'demo-select-label',
                            children: translate('Organization Type')
                        }),
                        e(Select, {
                            error: m.errors.orgType.error,
                            label: translate('Select Your Organization Type'),
                            children: orgTypeOptions,
                            onSelect: (name, value) => c.orgTypeChanged(value)
                        })
                    ]
                })
            );
        }
        
        if (m.demoVisible.zipCode) {
            demoChildren.push(
                e(Input, {
                    id: 'zipCode',
                    keyValue: 'zipCode',
                    label: translate('Zip Code'),
                    maxLength: 5,
                    status: m.errors.zipCode,
                    type: 'text',
                    value: m.zipCode,
                    onChange: (event) => c.zipCodeChanged(event.target.value)
                })
            )
        }
        
        let schoolOptions = [];
        
        if (m.demoVisible.schoolsByZip) {
            let matchingSchools = [],
                schoolsByZip = m.schoolsByZip[m.zipCode];
            
            if (schoolsByZip && schoolsByZip.schools) {
                for (let school of schoolsByZip.schools) {
                    matchingSchools.push({
                        selected: school.id === m.schoolId,
                        title: school.school_name,
                        value: school.id
                    });
                }
                
                let matchingSchoolsGroup = {
                    selectable: false,
                    title: translate('Matching Schools'),
                    children: matchingSchools
                };
                
                schoolOptions.push(matchingSchoolsGroup);
            }
            
            if (schoolsByZip && schoolsByZip.nearby_schools) {
                let nearbySchools = [];
                
                for (let school of schoolsByZip.nearby_schools) {
                    nearbySchools.push({
                        selected: school.id === m.schoolId,
                        title: school.school_name,
                        value: school.id
                    });
                }
                
                let nearbySchoolsGroup = {
                    selectable: false,
                    title: translate('Nearby Schools'),
                    children: nearbySchools
                };
                
                schoolOptions.push(nearbySchoolsGroup);
            }
            
            schoolOptions.push({
                selected: Constants.SCHOOL_NOT_LISTED === m.schoolId,
                title: translate('SCHOOL NOT LISTED'),
                value: Constants.SCHOOL_NOT_LISTED
            });
            
            demoChildren.push(
                e('div', {
                    className: 'register-select-container register-margin-left',
                    children: [
                        e('p', {
                            className: 'demo-select-label',
                            children: translate('School')
                        }),
                        e(Select, {
                            disabled: m.loadingByZip,
                            error: !m.loadingByZip && m.errors.schoolId.error,
                            label: m.loadingByZip ? '' : translate('Select Your School'),
                            loading: m.loadingByZip,
                            children: schoolOptions,
                            onSelect: (name, value) => c.schoolByZipChanged(value)
                        })
                    ]
                })
            );
        } else if (m.demoVisible.schoolsByCountry) {
            if (m.schoolsByCountry[m.country]) {
                for (let school of m.schoolsByCountry[m.country]) {
                    schoolOptions.push({
                        selected: school.id === m.schoolId,
                        title: school.school_name,
                        value: `${school.id}_____${school.school_name}`
                    });
                }
            }
            
            schoolOptions.push({
                selected: Constants.SCHOOL_NOT_LISTED === m.schoolId,
                title: translate('SCHOOL NOT LISTED'),
                value: `${Constants.SCHOOL_NOT_LISTED}_____SCHOOL NOT LISTED`
            });
            
            demoChildren.push(
                e('div', {
                    className: 'register-select-container',
                    children: [
                        e('p', {
                            className: 'demo-select-label',
                            children: translate('School')
                        }),
                        e(Select, {
                            disabled: m.loadingByCountry,
                            error: !m.loadingByCountry && m.errors.schoolId.error,
                            label: m.loadingByCountry ? '' : translate('Select Your School'),
                            loading: m.loadingByCountry,
                            children: schoolOptions,
                            onSelect: (name, value) => c.schoolByCountryChanged(value)
                        })
                    ]
                })
            );
        }
        
        let stateOptions = [];
        
        if (m.demoVisible.state) {
            for (let state of states) {
                stateOptions.push({
                    selected: state.code === m.state,
                    title: state.name,
                    value: state.code
                });
            }
            
            demoChildren.push(
                e('div', {
                    className: 'register-select-container',
                    children: [
                        e('p', {
                            className: 'demo-select-label',
                            children: translate('State')
                        }),
                        e(Select, {
                            error: m.errors.state.error,
                            label: translate('Select Your State'),
                            children: stateOptions,
                            onSelect: (name, value) => c.stateChanged(value)
                        })
                    ]
                })
            );
        }
        
        let evenContainerClass = m.demoVisible.number % 2 === 0 ? '' : ' register-margin-left';
        
        if (m.demoVisible.orgName) {
            demoChildren.push(
                e(Input, {
                    containerClass: evenContainerClass,
                    id: 'orgName',
                    keyValue: 'orgName',
                    label: translate('Organization Name'),
                    maxLength: 255,
                    status: m.errors.orgName,
                    type: 'text',
                    value: m.orgName,
                    onChange: (event) => c.orgNameChanged(event.target.value)
                })
            )
        }
        
        if (m.demoVisible.schoolName) {
            demoChildren.push(
                e(Input, {
                    containerClass: evenContainerClass,
                    id: 'schoolName',
                    keyValue: 'schoolName',
                    label: translate('School Name'),
                    maxLength: 255,
                    status: m.errors.schoolName,
                    type: 'text',
                    value: m.schoolName,
                    onChange: (event) => c.schoolNameChanged(event.target.value)
                })
            )
        }
        
        if (m.demoVisible.role) {
            let roleMargin = m.demoVisible.number % 2 !== 0 ? '' : ' register-margin-left';
            
            demoChildren.push(
                e('div', {
                    className: 'register-select-container' + roleMargin,
                    children: [
                        e('p', {
                            className: 'demo-select-label',
                            children: translate('Role')
                        }),
                        e(Select, {
                            error: m.errors.role.error,
                            label: translate('Select Your Role'),
                            children: [
                                {title: translate('Teacher'),       value: Constants.ROLE_TEACHER,       selected: Constants.ROLE_TEACHER       === m.role[0]},
                                {title: translate('Administrator'), value: Constants.ROLE_ADMINISTRATOR, selected: Constants.ROLE_ADMINISTRATOR === m.role[0]},
                                {title: translate('IT/Technology'), value: Constants.ROLE_IT_TECHNOLOGY, selected: Constants.ROLE_IT_TECHNOLOGY === m.role[0]},
                                {title: translate('Other'),         value: Constants.ROLE_OTHER,         selected: Constants.ROLE_OTHER         === m.role[0]}
                            ],
                            onSelect: (name, value) => c.roleChanged(value)
                        })
                    ]
                })
            );
        }
        
        let termsLabelChildren = [
            translate('I agree to the '),
            e('span', {
                className: 'link',
                children: translate('terms'),
                onClick: (event) => {
                    event.preventDefault(); // Prevent the terms box from becoming checked/unchecked.
                    c.termsLinkClicked();
                }
            }),
            '.'
        ];
        
        if (m.errors.terms.error) {
            termsLabelChildren.push(
                e(AlertIcon, {
                    className: 'input-error-icon'
                })
            );
        }
        
        let termsModuleChildren = [
            e('div', {
                id: 'terms-container',
                children: [
                    e('input', {
                        id: 'terms',
                        className: 'vertical-align-middle',
                        checked: m.termsChecked,
                        type: 'checkbox',
                        onChange: c.termsChanged
                    }),
                    e('label', {
                        className: 'vertical-align-middle terms-label',
                        htmlFor: 'terms',
                        children: termsLabelChildren
                    })
                ]
            })
        ];
        
        if (m.errors.terms.error) {
            termsModuleChildren.push(
                e(Tooltip, {
                    location: 'below',
                    title: translate('Required'),
                    text: translate('Please accept the terms of service.')
                })
            );
        }
        
        return e('div', {
            id: 'page-container',
            children: [
                e('div', {
                    id: 'main-content',
                    children: [
                        e(RegisterHeader),
                        e(RegisterSteps, {
                            step1Status: Constants.STATUS_COMPLETED,
                            step2Status: Constants.STATUS_SELECTED,
                            step3Status: platform.isIosApp ? null : 'idle'
                        }),
                        e('div', {
                            id: 'center-content',
                            className: 'vertical-align-middle',
                            children: [
                                e('div', {
                                    id: 'register-container',
                                    className: 'form',
                                    children: [
                                        e('div', {
                                            className: 'register-form-container',
                                            children: [
                                                e('div', {
                                                    className: 'register-form-header',
                                                    children: translate('Demographics')
                                                }),
                                                demoChildren,
                                                e('div', {
                                                    className: 'terms-module-container',
                                                    children: termsModuleChildren
                                                }),
                                                e('div', {
                                                    className: 'register-buttons',
                                                    children: [
                                                        e('div', {
                                                            className: 'register-action-button',
                                                            children: [
                                                                e('button', {
                                                                    className: 'secondary-action-button action-button-200',
                                                                    children: translate('PREVIOUS'),
                                                                    onClick: c.previousClicked
                                                                })
                                                            ]
                                                        }),
                                                        e('div', {
                                                            className: 'register-action-button',
                                                            children: [
                                                                e('button', {
                                                                    id: 'createAccountButton',
                                                                    className: 'action-button action-button-200',
                                                                    children: translate('NEXT'),
                                                                    onClick: c.createAccountClicked
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                }),
                e('footer', {
                    id: 'footer',
                    className: 'vertical-align',
                    children: e(FooterView)
                })
            ]
        });
    }
    
}
