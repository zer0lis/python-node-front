import React from 'react';
import AlertIcon from 'AlertIcon';
import RegisterHeader from 'RegisterHeader';
import RegisterSteps from 'RegisterSteps';
import FreeIcon from 'FreeIcon';
import ProIcon from 'ProIcon';
import AccountTypeCheck from 'AccountTypeCheck';
import CloseX from 'CloseX';
import Footer from 'FooterView';
import Constants from 'Constants';
import LicenseView from 'LicenseView';
import {translate} from 'translator';

let e = React.createElement;

export default class CreateAccountTypeView extends React.Component {

    render() {
        let c = this.props.controller,
            m = this.props.model,
            l = m.licenseModel,
            canFinish = m.selectedAccountType !== null,
            showingForm = m.selectedAccountType === Constants.PRO_USER,
            accountChildren = [],
            subHeaderChildren = [];

        if (m.stripeError) {
            accountChildren.push(
                e('div', {
                    className: 'register-form-stripe-error',
                    children: [
                        e(AlertIcon, {
                            className: 'register-form-stripe-alert',
                            color: '#ffffff'
                        }),
                        e('span', {
                            className: 'register-form-stripe-text',
                            children: m.stripeError
                        }),
                        e('div', {
                            className: 'register-stripe-error-close',
                            children: [
                                e(CloseX, {
                                    className: 'register-stripe-error-close-x'
                                })
                            ],
                            onClick: c.closeBannerClicked
                        })
                    ]
                })
            );
        }

        if (m.subHeaderText) {
            subHeaderChildren.push(
                e('div', {
                    className: 'register-form-header-sub2',
                    children: m.subHeaderText
                })
            );
        }

        accountChildren.push(
            e('div', {
                className: 'register-form-sub-header-free',
                children: subHeaderChildren
            })
        );

        let freeColor = m.selectedAccountType === Constants.FREE_USER ? '#ffffff' : null,
            freeClass = m.selectedAccountType === Constants.FREE_USER ? 'account-type-button-selected' : 'account-type-button',
            freeTextClass = m.selectedAccountType === Constants.FREE_USER ? 'account-type-button-text-selected' : 'account-type-button-text',
            proColor = m.selectedAccountType === Constants.PRO_USER ? '#ffffff' : null,
            proClass = m.selectedAccountType === Constants.PRO_USER ? 'account-type-button-selected' : 'account-type-button',
            proTextClass = m.selectedAccountType === Constants.PRO_USER ? 'account-type-button-text-selected' : 'account-type-button-text';

        let freeChildren = [
            e(FreeIcon, {
                className: 'free-account-type-icon',
                color: freeColor
            }),
            e('div', {
                className: freeTextClass + ' account-type-free-text',
                children: translate('Socrative FREE')
            }),
            e('div', {
                className: freeTextClass,
                children: translate('All the standard awesome features.')
            })
        ];

        let proChildren = [
            e(ProIcon, {
                className: 'pro-account-type-icon',
                color: proColor
            }),
            e('div', {
                className: proTextClass,
                children: translate('Multiple classrooms, rosters, and much more!')
            })
        ];

        let accountTypeCheck = e(AccountTypeCheck);

        if (m.selectedAccountType === Constants.FREE_USER) {
            freeChildren.push(accountTypeCheck);
        } else if (m.selectedAccountType === Constants.PRO_USER) {
            proChildren.push(accountTypeCheck);
        }

        if (l.licenseStep === Constants.LICENSE_STEP) {
            accountChildren.push(
                e('div', {
                    className: 'account-type-buttons-outer',
                    children: [
                        e('div', {
                            className: 'account-type-buttons-inner',
                            children: [
                                e('div', {
                                    className: freeClass,
                                    children: freeChildren,
                                    onClick: c.freeClicked
                                }),
                                e('div', {
                                    className: proClass,
                                    children: proChildren,
                                    onClick: c.proClicked
                                })
                            ]
                        })
                    ]
                })
            );
        }

        if (showingForm) {
            accountChildren.push(
                e(LicenseView, this.props)
            );
        }

        let finishButtonClass = 'action-button-200';
        finishButtonClass += (canFinish ? ' action-button' : ' action-button-disabled');

        let mainContentChildren = [
            e(RegisterHeader, {
                onlyLogo: l.licenseStep === Constants.COMPLETE_STEP
            })
        ];
        
        if (l.licenseStep !== Constants.COMPLETE_STEP) {
            mainContentChildren.push(
                e(RegisterSteps, {
                    step1Status: 'completed',
                    step2Status: 'completed',
                    step3Status: m.step3Status,
                    step4Status: m.step4Status
                })
            );
        }
        
        mainContentChildren.push(
            e('div', {
                id: 'center-content',
                className: 'vertical-align-middle',
                children: [
                    e('div', {
                        id: 'register-container',
                        className: 'form',
                        children: [
                            e('div', {
                                className: 'register-form-container' + (l.licenseStep === Constants.COMPLETE_STEP ? ' register-form-container-payment-complete' : ''),
                                children: [
                                    e('div', {
                                        className: 'register-form-header',
                                        children: m.headerText
                                    }),
                                    accountChildren,
                                    e('div', {
                                        className: 'register-action-button register-finish-button' + (showingForm ? ' hidden' : ''),
                                        children: [
                                            e('button', {
                                                id: 'register-finish-button',
                                                className: finishButtonClass,
                                                children: translate('FINISH'),
                                                onClick: () => {
                                                    if (canFinish) {
                                                        c.finishClicked();
                                                    }
                                                }
                                            })
                                        ]
                                    })
                                ]
                            })
                        ]
                    })
                ]
            })
        );

        return e('div', {
            id: 'page-container',
            children: [
                e('div', {
                    id: 'main-content',
                    children: mainContentChildren
                }),
                e('footer', {
                    id: 'footer',
                    className: 'vertical-align',
                    children: [
                        e(Footer, {
                            learnMoreClicked: c.learnMoreClicked
                        })
                    ]
                })
            ]
        });
    }
}
