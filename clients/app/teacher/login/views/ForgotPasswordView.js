import React from 'react';
import Logo from 'LoginLogo';
import Input from 'Input';
import Footer from 'FooterView';
import {translate} from 'translator';

let e = React.createElement;

export default class ForgotPasswordView extends React.Component {

    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        return e('div', {
            id: 'page-container',
            children: [
                e('div', {
                    id: 'main-content',
                    children: [
                        e(Logo),
                        e('div', {
                            id: 'center-content',
                            className: 'vertical-align-middle',
                            children: [
                                e('div', {
                                    id: 'teacherLoginForm',
                                    className: 'form single-display',
                                    children: [
                                        e('div', {
                                            className: 'teacher-login-title',
                                            children: [
                                                e('h1', {
                                                    children: translate('Forgot Password')
                                                })
                                            ]
                                        }),
                                        e('span', {
                                            className: 'forgot-password-explanation',
                                            children: translate("Enter your email and we'll send you a password reset link.")
                                        }),
                                        e('div', {
                                            id: 'reset-email',
                                            children: [
                                                e(Input, {
                                                    autoFocus: true,
                                                    className: 'loginInput',
                                                    id: 'email',
                                                    label: translate('Email'),
                                                    maxLength: 255,
                                                    onChange: (event) => c.emailChanged(event.target.value),
                                                    onKeyPress: (event) => {
                                                        if (event.key === 'Enter') {
                                                            this.emailInput.blur();
                                                            c.submitClicked();
                                                        }
                                                    },
                                                    refValue: (input) => {this.emailInput = input},
                                                    status: m.errors.email,
                                                    tooltipLocation: 'above',
                                                    type: 'email',
                                                    value: m.email
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            id: 'action-button-container',
                                            className: 'align-center',
                                            children: [
                                                e('button', {
                                                    id: 'resetPwdButton',
                                                    className: 'button button-primary button-large action-button',
                                                    children: translate('SUBMIT'),
                                                    onClick: c.submitClicked
                                                })
                                            ]
                                        }),
                                        e('div', {
                                            id: 'bottom-links',
                                            className: 'align-center',
                                            children: [
                                                e('a', {
                                                    id: 'cancelButton',
                                                    children: translate('Back'),
                                                    href: '#'
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                }),
                e('footer', {
                    id: 'footer',
                    className: 'vertical-align',
                    children: e(Footer)
                })
            ]
        });
    }

}
