import React from 'react';
import RegisterHeader from 'RegisterHeader';
import RegisterSteps from 'RegisterSteps';
import Input from 'Input';
import FooterView from 'FooterView';
import platform from 'Platform';
import {translate} from 'translator';

let e = React.createElement;

export default class CreateAccountProfileView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        return e('div', {
            id: 'page-container',
            children: [
                e('div', {
                    id: 'main-content',
                    children: [
                        e(RegisterHeader),
                        e(RegisterSteps, {
                            step1Status:  m.step1Status,
                            step2Status:  m.step2Status,
                            step3Status:  platform.isIosApp ? null : m.step3Status
                        }),
                        e('div', {
                            id: 'center-content',
                            className: 'vertical-align-middle',
                            children: [
                                e('div', {
                                    id: 'register-container',
                                    className: 'form',
                                    children: [
                                        e('div', {
                                            className: 'register-form-container',
                                            children: [
                                                e('div', {
                                                    className: 'register-form-header',
                                                    children: translate('Profile')
                                                }),
                                                e(Input, {
                                                    id: 'firstName',
                                                    label: translate('First Name'),
                                                    maxLength: 255,
                                                    status: m.errors.firstName,
                                                    value: m.firstName,
                                                    onChange: (event) => c.firstNameChanged(event.target.value)
                                                }),
                                                e(Input, {
                                                    containerClass: 'register-margin-left',
                                                    id: 'lastName',
                                                    label: translate('Last Name'),
                                                    maxLength: 255,
                                                    status: m.errors.lastName,
                                                    value: m.lastName,
                                                    onChange: (event) => c.lastNameChanged(event.target.value)
                                                }),
                                                e(Input, {
                                                    id: 'email',
                                                    label: translate('Email'),
                                                    maxLength: 255,
                                                    status: m.errors.email,
                                                    tooltipLocation: 'above',
                                                    type: 'email',
                                                    value: m.email,
                                                    onChange: (event) => c.emailChanged(event.target.value)
                                                }),
                                                e(Input, {
                                                    containerClass: 'register-margin-left',
                                                    id: 'email2',
                                                    label: translate('Confirm Email'),
                                                    maxLength: 255,
                                                    status: m.errors.email2,
                                                    tooltipLocation: 'above',
                                                    type: 'email',
                                                    value: m.email2,
                                                    onChange: (event) => c.email2Changed(event.target.value)
                                                }),
                                                e(Input, {
                                                    id: 'password',
                                                    label: translate('Password'),
                                                    maxLength: 255,
                                                    status: m.errors.password,
                                                    tooltipLocation: 'above',
                                                    type: 'password',
                                                    value: m.password,
                                                    onChange: (event) => c.passwordChanged(event.target.value)
                                                }),
                                                e(Input, {
                                                    containerClass: 'register-margin-left',
                                                    id: 'password2',
                                                    label: translate('Confirm Password'),
                                                    maxLength: 255,
                                                    status: m.errors.password2,
                                                    tooltipLocation: 'above',
                                                    type: 'password',
                                                    value: m.password2,
                                                    onChange: (event) => c.password2Changed(event.target.value)
                                                }),
                                                e('div', {
                                                    className: 'register-buttons',
                                                    children: [
                                                        e('div', {
                                                            className: 'register-action-button',
                                                            children: [
                                                                e('button', {
                                                                    className: 'secondary-action-button action-button-200',
                                                                    children: translate('CANCEL'),
                                                                    onClick: c.cancelClicked
                                                                })
                                                            ]
                                                        }),
                                                        e('div', {
                                                            className: 'register-action-button',
                                                            children: [
                                                                e('button', {
                                                                    id: 'createAccountProfileNextButton',
                                                                    className: 'action-button action-button-200',
                                                                    children: translate('NEXT'),
                                                                    onClick: c.nextClicked
                                                                })
                                                            ]
                                                        })
                                                    ]
                                                })
                                            ]
                                        })
                                    ]
                                })
                            ]
                        })
                    ]
                }),
                e('footer', {
                    id: 'footer',
                    className: 'vertical-align',
                    children: e(FooterView)
                })
            ]
        });
    }
    
}
