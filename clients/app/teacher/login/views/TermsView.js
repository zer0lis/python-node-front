import React from 'react';
import TermsContent from 'TermsContent';
import {translate} from 'translator';

let e = React.createElement;

export default class TermsView extends React.Component {
    
    render() {
        return e('div', {
            id: 'terms-bg',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'terms-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('span', {
                                    className: 'popup-title',
                                    children: 'Socrative, Inc. Terms & Conditions of Service'
                                }),
                                e('a', {
                                    className: 'cancel-button',
                                    title: translate('Close'),
                                    children: [
                                        e('i', {
                                            className: 'close-popup',
                                            'data-icon': 'h'
                                        })
                                    ],
                                    onClick: this.props.cancelClicked
                                })
                            ]
                        }),
                        e('div', {
                            id: 'terms-content-container',
                            dangerouslySetInnerHTML: {__html: TermsContent}
                        }),
                        e('div', {
                            className: 'modal-footer',
                            children: [
                                e('button', {
                                    className: 'action-button action-button-200',
                                    children: translate('I AGREE'),
                                    onClick: this.props.agreeClicked
                                }),
                                e('button', {
                                    className: 'secondary-action-button action-button-200 terms-cancel-button',
                                    children: translate('CANCEL'),
                                    onClick: this.props.cancelClicked
                                })
                            ]
                        })
                    ]
                })
            ]
        });
    }
    
}
