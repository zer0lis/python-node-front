'use strict';

class GoogleLoginModel {
    
    constructor() {
        this.zendeskLogin = false;
        this.submitForm = false;
        this.promptApproval = false; // If true, force Google to show the popup to allow Socrative access.
    }
    
    get clientId() {
        if (window.location.hostname.toLowerCase() === 'a.socrative.com') {
            return '274119749266-iqcu10tk52lrqisfrluu7o411mvbek8n.apps.googleusercontent.com';
        }
        
        return '176718315647.apps.googleusercontent.com';
    }
    
    get redirectUri() {
        let hostname = window.location.hostname.toLowerCase();

        if (hostname === 'b.socrative.com') {
            return 'https://teacher.socrative.com/v3/login/google';
        } else if (hostname === 'rob.socrative.com') {
            return 'https://teacher-dev.socrative.com/v3/login/google';
        } else {
            return 'http://teacher-local.socrative.com:4000/v3/login/google';
        }
    }
    
}

module.exports = new GoogleLoginModel();
