import React from 'react';
import BaseHeaderView from 'BaseHeaderView';
import HeaderMenuButton from 'HeaderMenuButton';
import MobileHeaderMenu from 'MobileHeaderMenu';
import HeaderUserName from 'HeaderUserName';
import HeaderNav from 'HeaderNav';
import HeaderStudentCount from 'HeaderStudentCount';

let e = React.createElement;

export default class HeaderView extends React.Component {
    
    render() {
        let m = this.props.model;
        
        let children = [
            e(HeaderMenuButton, this.props),
            e(HeaderUserName, this.props),
            e(HeaderNav, this.props),
            e(HeaderStudentCount, this.props)
        ];
        
        if (m.mobileMenuVisible) {
            children.push(
                e(MobileHeaderMenu, this.props)
            );
        }
        
        
        return e(BaseHeaderView, Object.assign({children: children}, this.props));
    }
    
}
