import React from 'react';
import ReactDOM from 'react-dom';
import _ from 'underscore';
import Constants from 'Constants';
import messaging from 'Messaging';
import room from 'room';
import user from 'user';
import activity from 'activity';
import model from 'HeaderModel';
import view from 'HeaderView';
import renewalModel from 'RenewalModel';
import popup from 'PopupController';
import platform from 'Platform';
import {translate} from 'translator';

let router = null;

class HeaderController {
    
    initialize(teacherRouter) {
        router = teacherRouter;
        model.shouldRouteToLaunch = false;
        controller.roomJoinSuccess();
        messaging.addListener({
            message: Constants.PRESENCE,
            callback: controller.presenceChanged
        });
        messaging.addListener({
            message: Constants.HAND_RAISE,
            callback: controller.handRaiseChanged
        });
    }
    
    activityRefreshed() {
        controller.render(model.selectedTab);
    }
    
    render(tab) {
        model.selectedTab = tab;
        
        let roomsInMenu = 0,
            rooms = room.getRooms();
        
        if (rooms) {
            let defaultRoomName = null;
            
            for (let room of rooms) {
                if (room.in_menu) {
                    roomsInMenu++;
                }
                
                if (room.default) {
                    defaultRoomName = room.name;
                }
            }
            
            // The default room should always be in the rooms menu, unless the teacher
            // is currently in the default room and no other rooms are in the menu.
            if (roomsInMenu === 1 && defaultRoomName !== null && defaultRoomName.toLowerCase() === room.get('name').toLowerCase()) {
                roomsInMenu = 0;
            }
        }
        
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model,
                    logoClass: 'teacher-header-logo',
                    roomName: room.getRoomName(),
                    allowRoomsMenu: user.isPro() && roomsInMenu > 0,
                    rooms: rooms,
                    isPro: user.isPro(),
                    userName: user.getFirstName() || translate('Menu'),
                    userMenu: 'teacher',
                    renewalModel: renewalModel,
                    disableResults: !activity.isInProgress(),
                    hasRoster: room.hasRoster(),
                    rosterSize: room.get('student_count') || 0,
                    studentList: room.getStudents(),
                    enableHandraise: user.getHandRaiseStatus(),
                    handsRaised: room.getHandsRaised()
                }
            ),
            document.getElementById('header-container'),
            controller.afterRender
        );
    }
    
    afterRender() {
        if (model.mobileMenuVisible) {
            let mobileMenu = document.getElementById('mobile-teacher-header-menu');
            TweenLite.to(mobileMenu, 0.4375, {right: 0, ease: Quint.easeOut});
        }
        
        if (model.roomsMenuVisible) {
            let menu = document.getElementById('rooms-menu');
            
            if (menu) {
                let bounds = menu.getBoundingClientRect();
                if (bounds.left < 0) { // The menu is off screen to the left, so move it right with some padding.
                    menu.style.right = '-' + (-bounds.left + 32) + 'px';
                }
            }
        }
    }
    
    roomsMenuClicked() {
        if (user.isPro() && !model.roomsMenuVisible && !model.mobileMenuVisible) {
            _.defer(() => { // Defer so the document click listener finishes first.
                model.roomsMenuVisible = true;
                controller.getRoomsList();
                controller.addClickListener();
            });
        }
    }
    
    getRoomsList() {
        room.getRoomsList({
            success: controller.getRoomsSuccess,
            error: controller.getRoomsError
        });
    }
    
    getRoomsSuccess() {
        controller.render(model.selectedTab);
    }
    
    getRoomsError(response) {
        popup.render({
            title: translate('Unknown Error'),
            message: translate('Your classrooms are currently unavailable. Please try again later.')
        });
    }
    
    headerRoomClicked(roomName) {
        if (room.getRoomName().toUpperCase() !== roomName) {
            popup.render({
                title: translate('Please Confirm'),
                message: translate('Are you sure you want to change to the {0} classroom?').format(roomName),
                buttonText: translate('Yes'),
                cancelText: translate('No'),
                buttonClicked: () => {
                    controller.switchRoom(roomName);
                }
            });
        }
    }
    
    switchRoom(roomName) {
        model.nextRoom = roomName;
        model.shouldRouteToLaunch = true;
        let oldRoomName = room.getRoomName().toLowerCase();
        messaging.unsubscribe({
            channel: [`${oldRoomName}-teacher`, `${oldRoomName}-student`],
            callback: controller.joinNextRoom
        });
    }
    
    joinNextRoom() {
        activity.reset();
        room.join({
            role: 'teacher',
            roomname: model.nextRoom,
            success: controller.roomJoinSuccess,
            error: controller.roomJoinError
        });
    }
    
    roomJoinSuccess() {
        model.studentsInRoom = room.get('students_logged_in') || 0;
        room.set('students_logged_in', model.studentsInRoom);
        
        controller.getRoomsList();
        
        if (room.hasRoster()) {
            controller.getStudentList();
        }
        
        activity.refresh(controller.activityRefreshed);
        
        if (model.shouldRouteToLaunch) {
            router.routeToLaunch();
        }
    }

    roomJoinError(response) {
        popup.render({
            title: translate('Unknown Error'),
            message: translate('The classroom could not be changed at this time. Please try again later.')
        });
    }
    
    presenceChanged(data) {
        if (room.hasRoster()) {
            controller.getStudentsSuccess(); // The messaging module will have already fetched the student list.
            room.setHandsRaised();
        } else {
            if (data && data.occupancy) {
                model.studentsInRoom = data.occupancy - 1; // Subtract 1 to exclude the teacher from the count.
                room.set('students_logged_in', model.studentsInRoom);
                controller.render(model.selectedTab);
            }
        }
    }
    
    handRaiseChanged(data) {
        data = JSON.parse(data);
        room.updateStudentHandraise(data.id, data.state);
        controller.rerender();
    }
    
    usernameClicked() {
        if (!model.accountMenuVisible) {
            _.defer(controller.usernameClickedDeferred); // Defer so that the document click listener finishes first.
        }
    }
    
    usernameClickedDeferred() {
        model.accountMenuVisible = true;
        controller.render(model.selectedTab);
        controller.addClickListener();
    }
    
    addClickListener() {
        if (platform.isIos) {
            document.body.style.cursor = 'pointer'; // This makes any element respond to click events in iOS.
        }
        document.body.addEventListener('click', controller.documentClicked);
    }
    
    documentClicked(event) {
        // Defer removing the document listener so we don't prematurely close menus (i.e. so other click events can be handled first).
        _.defer(() => {
            controller.removeClickListener(event);
        });
    }
    
    removeClickListener(event) {
        if (event.target.id === 'show-student-ids-label' || event.target.id === 'show-student-ids') {
            return;
        }
        if (event.target.id === 'handraise' || event.target.id === 'handraise-label' || event.target.id.indexOf('handraise-student') !== -1) {
            return;
        }
        if (platform.isIos) {
            document.body.style.cursor = 'default';
        }
        document.body.removeEventListener('click', controller.documentClicked);
        model.accountMenuVisible = false;
        model.roomsMenuVisible = false;
        model.rosterMenuVisible = false;
        controller.render(model.selectedTab);
    }
    
    clearRoomClicked() {
        popup.render({
            title: translate('Please Confirm'),
            message: translate('Are you sure you want to clear all students from the {0} classroom?').format(room.getRoomName().toUpperCase()),
            buttonText: translate('Yes'),
            cancelText: translate('No'),
            buttonClicked: () => {
                controller.hideMobileMenus();
                user.clearRoom(controller.afterRoomCleared);
            }
        });
    }
    
    afterRoomCleared(error) {
        if (error) {
            popup.render({
                title: translate('Unknown Error'),
                message: translate('The room could not be cleared. Please try again later.')
            });
        } else if (user.isPro() && room.hasRoster()) {
            controller.getStudentList();
        }
    }
    
    refreshClicked() {
        popup.render({
            title: translate('Please Confirm'),
            message: translate('Are you sure you want to refresh?'),
            buttonText: translate('Yes'),
            cancelText: translate('No'),
            buttonClicked: () => {
                window.location.reload();
            }
        });
    }
    
    profileClicked() {
        controller.hideMobileMenus(router.routeToProfileInfo);
    }
    
    helpClicked() {
        let language = user.getLanguage();
        if (!language) {
            language = 'en';
        }
        language = language.toLowerCase();
        
        // Account for differences in our language abbreviations and those expected by the help center.
        if (language === 'en') {
            language = 'en-us';
        } else if (language === 'es-mx') {
            language = 'es';
        } else if (language === 'fr-ca') {
            language = 'fr';
        } else if (language === 'pt-br') {
            language = 'pt';
        } else if (language === 'zh-cn') {
            language = 'zh-CN';
        }
        
        language = encodeURIComponent(language).replace(/[!'()*]/g, (c) => {
            return '%' + c.charCodeAt(0).toString(16);
        });
        
        window.open(`https://help.socrative.com/hc/${language}/signin?return_to=https://help.socrative.com/hc/${language}&locale=1`).focus();
        controller.hideMobileMenus();
    }
    
    signOutClicked() {
        popup.render({
            title: translate('Please Confirm'),
            message: translate('Are you sure you want to sign out?'),
            buttonText: translate('Yes'),
            cancelText: translate('No'),
            buttonClicked: () => {
                user.logout(() => {
                    window.location.href = '/login/teacher/';
                });
            }
        });
    }

    renewNotifyClicked() {
        controller.hideMobileMenus(router.routeToProfileBilling);
    }

    logoClicked() {
        if (!model.mobileMenuVisible) {
            controller.hideMobileMenus();
            router.routeToLaunch();
        }
    }
    
    launchClicked() {
        controller.hideMobileMenus(router.routeToLaunch);
    }
    
    quizzesClicked() {
        controller.hideMobileMenus(router.routeToQuizzes);
    }
    
    roomsClicked() {
        controller.hideMobileMenus(router.routeToRooms);
    }
    
    resultsClicked() {
        if (activity.isInProgress()) {
            controller.hideMobileMenus(router.routeToLiveResults);
        }
    }
    
    reportsClicked() {
        controller.hideMobileMenus(router.routeToReports);
    }
    
    rosterMenuClicked() {
        if (!model.rosterMenuVisible) {
            _.defer(() => { // Defer so the document click listener finishes first.
                model.rosterMenuVisible = true;
                
                if (room.hasRoster()) {
                    controller.getStudentList();
                } else {
                    controller.render(model.selectedTab);
                }
                
                controller.addClickListener();
            });
        }
    }
    
    getStudentList() {
        room.getStudentList({
            success: controller.getStudentsSuccess,
            error: controller.getStudentsError
        });
    }
    
    getStudentsSuccess(students) {
        let count = 0;
        
        if (!students) {
            students = room.getStudents();
        }
        
        for (let student of students) {
            if (student.logged_in) {
                count++;
            }
        }
        
        model.studentsInRoom = count;
        room.set('students_logged_in', model.studentsInRoom);
        room.setHandsRaised();
        controller.render(model.selectedTab);
    }
    
    getStudentsError() {
        popup.render({
            title: translate('Unknown Error'),
            message: translate('This roster is currently unavailable. Please try again later.')
        });
    }
    
    showIdsClicked() {
        model.showingStudentIds = !model.showingStudentIds;
        controller.render(model.selectedTab);
    }

    toggleHandraiseStatus() {
        model.setHandraiseStatus(!user.getHandRaiseStatus(), controller.rerender); 
    }

    rerender() {
        controller.render(model.selectedTab);
    }

    toggleStudentHandraise(id) {
        model.toggleStudentHandraise(id, controller.rerender); 
    }
    
    mobileMenuClicked() {
        if (model.mobileMenuVisible) {
            controller.hideMobileMenus();
        } else {
            model.mobileMenuVisible = true;
            document.getElementById('page-container').className += ' mobile-menu-active';
            window.addEventListener('orientationchange', controller.orientationChanged);
            controller.render(model.selectedTab);
        }
    }
    
    mobileUserMenuClicked() {
        model.mobileUserMenuVisible = !model.mobileUserMenuVisible;
        model.mobileStudentMenuVisible = false;
        controller.render(model.selectedTab);
    }
    
    mobileStudentMenuClicked() {
        model.mobileStudentMenuVisible = !model.mobileStudentMenuVisible;
        model.mobileUserMenuVisible = false;
        controller.render(model.selectedTab);
    }

    /**
     * Hide the mobile header menu immediately, with no animation (see SOC-1093).
     */
    orientationChanged() {
        window.removeEventListener('orientationchange', controller.orientationChanged);
        model.mobileMenuVisible = false;
        model.mobileUserMenuVisible = false;
        model.mobileStudentMenuVisible = false;
        if (document.getElementById('mobile-teacher-header-menu')) {
            document.getElementById('mobile-teacher-header-menu').style.right = '-100%';
        }
        let pageContainer = document.getElementById('page-container'); 
        pageContainer.className = pageContainer.className.replace(' mobile-menu-active', '');
        controller.render(model.selectedTab);
    }
    
    hideMobileMenus(callback) {
        if (model.mobileMenuVisible) {
            model.mobileMenuVisible = false;
            model.mobileUserMenuVisible = false;
            model.mobileStudentMenuVisible = false;
            let mobileMenu = document.getElementById('mobile-teacher-header-menu');
            TweenLite.to(mobileMenu, 0.1375, {right: '-100%', ease: Cubic.easeOut, onComplete: () => {
                let pageContainer = document.getElementById('page-container');
                pageContainer.className = pageContainer.className.replace(' mobile-menu-active', '');
                controller.render(model.selectedTab);
                if (callback) {
                    callback();
                }
            }});
        } else if (callback) {
            callback();
        }
    }
    
}

let controller = new HeaderController();
module.exports = controller;
