let room = require('room'),
    user = require('user'),
    request = require('Request');

class HeaderModel {
    
    constructor() {
        this.accountMenuVisible = false; // Whether the account menu is visible (profile, help, sign out).
        this.mobileMenuVisible = false; // Whether the mobile navigation menu is visible.
        this.mobileStudentMenuVisible = false; // Whether the student menu has been expanded in the mobile navigation menu (student ids and hand raise).
        this.mobileUserMenuVisible = false; // Whether the user menu has been expanded in the mobile navigation menu (refresh, profile, help, sign out).
        this.nextRoom = ''; // The room to join after asynchronously unsubscribing from pubnub messaging in the current room.
        this.roomsMenuVisible = false; // Whether the rooms menu is visible.
        this.rosterMenuVisible = false; // Whether the roster menu is visible.
        this.selectedTab = ''; // The currently selected tab (bolded and underlined). One of the <NAME>_TAB constants, or an empty string if no tab is selected.
        this.shouldRouteToLaunch = false; // Whether the teacher should be routed to the launch tab after a room change.
        this.showingStudentIds = true; // Whether student ids are being shown on the roster menu.
        this.studentsInRoom = 0; // The number of students in the current room.
    }
    
    setHandraiseStatus(value, callback) {
        request.post({
            url: `${window.backend_host}/rooms/api/handraise/`,
            data: {
                room_name: room.get('name'),
                active: value
            },
            success: () => {
                user.setHandraiseStatus(value);
                room.clearAllHandraises();
                if (callback) {
                    callback();
                }
            },
            error: (response) => {
                console.error(response);
            }
        });
    }
    
    toggleStudentHandraise(id, callback) {
        let student = room.getStudents().find((student) => {
            return student.id === id;
        });
        
        let studentHandraise = !student.handraise;
        
        if (student.logged_in) {
            request.post({
                url: `${window.backend_host}/lecturers/api/handraise/`,
                data: {
                    room_name: room.get('name'),
                    active: studentHandraise,
                    id: id
                },
                success: () => {
                    room.updateStudentHandraise(id, studentHandraise, callback);
                },
                error: (response) => {
                    console.error(response);
                }
            });
        }
    }
    
}

module.exports = new HeaderModel();
