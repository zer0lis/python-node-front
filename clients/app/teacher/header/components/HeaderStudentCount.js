import React from 'react';
import StudentCountIcon from 'StudentCountIcon';
import HandRaisedIcon from 'HandRaisedIcon';
import DownChevronSmall from 'DownChevronSmall';
import HeaderRosterMenu from 'HeaderRosterMenu';

let e = React.createElement;

export default class HeaderStudentCount extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            countClass = 'student-count-number',
            countValue = m.studentsInRoom;
        
        if (!this.props.hasRoster && countValue < 100) {
            countClass += ' small-count';
        }
        
        if (this.props.hasRoster) {
            countValue += '/' + this.props.rosterSize;
        }

        let handsRaised = '',
            handsRaisedClass = 'hands-raised';
        
        if (this.props.handsRaised > 9) {
            handsRaisedClass += ' two-digits';
        }
        
        if (this.props.handsRaised > 99) {
            handsRaisedClass += ' three-digits';
        }

        if (this.props.enableHandraise && this.props.handsRaised > 0) {
            handsRaised = e('span', {
                className: handsRaisedClass,
                children: [
                    e(HandRaisedIcon, {
                        className: 'hands-raised-icon'
                    }),
                    this.props.handsRaised
                ]
            })
        }
        
        return e('div', {
            className: 'student-count-container',
            children: [
                e('span', {
                    className: countClass,
                    children: countValue
                }),
                handsRaised,
                e('div', {
                    className: 'student-count-icon',
                    children: [
                        e('div', {
                            children: e(StudentCountIcon, {color: '#ffffff'})
                        })
                    ]
                }),
                e(DownChevronSmall, {
                    className: 'student-count-chevron',
                    color: '#ffffff'
                }),
                e(HeaderRosterMenu, this.props)
            ],
            onClick: c.rosterMenuClicked
        })
    }
    
}
