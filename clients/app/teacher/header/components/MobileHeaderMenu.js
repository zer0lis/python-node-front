import React from 'react';
import TeacherIcon from 'TeacherIcon';
import ProBadgeIconSmall from 'ProBadgeIconSmall';
import UpChevronSmall from 'UpChevronSmall';
import DownChevronSmall from 'DownChevronSmall';
import StudentCountIcon from 'StudentCountIcon';
import LoggedInIcon from 'LoggedInIcon';
import LoggedOutIcon from 'LoggedOutIcon';
import SmallActivityStatus from 'SmallActivityStatus';
import HandraiseIconActive from 'HandraiseIconActive';
import HandraiseIconInactive from 'HandraiseIconInactive';
import HandRaisedIcon from 'HandRaisedIcon';
import RightChevronSmall from 'RightChevronSmall';
import RenewIconSmall from 'RenewIconSmall';
import utils from 'Utils';
import platform from 'Platform';
import Constants from 'Constants';
import {translate} from 'translator';

let e = React.createElement;

export default class MobileHeaderMenu extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            r = this.props.renewalModel,
            allChildren = [],
            userMenuHeaderChildren = [];
        
        if (this.props.isPro) {
            userMenuHeaderChildren.push(
                e(ProBadgeIconSmall, {
                    className: 'mobile-header-icon mobile-header-pro-badge-icon',
                    fill: r.showRenewal ? Constants.RENEWAL_COLOR : Constants.PRO_BADGE_COLOR,
                })
            );
        } else {
            userMenuHeaderChildren.push(
                e(TeacherIcon, {
                    className: 'mobile-header-icon',
                    color: '#8CB4D2'
                })
            );
        }
        
        userMenuHeaderChildren.push(
            e('span', {
                className: 'mobile-header-text',
                children: this.props.userName
            })
        );
        
        if (m.mobileUserMenuVisible) {
            userMenuHeaderChildren.push(
                e(UpChevronSmall, {
                    className: 'mobile-header-up-chevron',
                    color: '#cccccc'
                })
            );
        } else {
            userMenuHeaderChildren.push(
                e(DownChevronSmall, {
                    className: 'mobile-header-down-chevron',
                    color: '#cccccc'
                })
            );
        }
        
        let userMenuChildren = [
            e('li', {
                className: 'mobile-user-menu-header',
                children: userMenuHeaderChildren,
                onClick: c.mobileUserMenuClicked
            })
        ];
        
        if (m.mobileUserMenuVisible) {
            let message = r.type === Constants.PRO_ENDING ? translate('Expiring') : translate('Expired');
            
            if (r.showRenewal) {
                userMenuChildren.push(
                    e('li', {
                        className: 'mobile-user-menu-item renew-notify',
                        children: [
                            e(RenewIconSmall),
                            e('span', {
                                children: translate('Renew your account')
                            }),
                            e('span', {
                                className: 'date',
                                children: `${message} ${r.expirationDateFormatted}`
                            })
                        ],
                        onClick: c.renewNotifyClicked
                    })
                )
            }

            userMenuChildren.push(
                e('li', {
                    className: 'mobile-user-menu-item',
                    children: [
                        e('span', {
                            children: translate('Refresh')
                        })
                    ],
                    onClick: c.refreshClicked
                }),
                e('li', {
                    className: 'mobile-user-menu-item',
                    children: [
                        e('span', {
                            children: translate('Profile')
                        })
                    ],
                    onClick: c.profileClicked
                }),
                e('li', {
                    className: 'mobile-user-menu-item',
                    children: [
                        e('span', {
                            children: translate('Help')
                        })
                    ],
                    onClick: c.helpClicked
                }),
                e('li', {
                    className: 'mobile-user-menu-item',
                    children: [
                        e('span', {
                            children: translate('Sign Out')
                        })
                    ],
                    onClick: c.signOutClicked
                })
            );
        }
        
        let userMenu = [
            e('ul', {
                id: 'mobile-user-menu',
                children: userMenuChildren
            })
        ];
        
        let countClass = 'student-count-number';
        
        if (!this.props.hasRoster && m.studentsInRoom < 100) {
            countClass += ' small-count';
        }

        if (this.props.enableHandraise && this.props.handsRaised > 0) {
            countClass += ' active';
        }
        
        let countValue = m.studentsInRoom,
            iconClass = 'student-count-icon-no-roster';
        
        if (this.props.hasRoster) {
            countValue += '/' + this.props.rosterSize;
            iconClass = 'student-count-icon-with-roster'
        }

        let handsRaised = '',
            handsRaisedClass = 'hands-raised';
        
        if (this.props.handsRaised > 9) {
            handsRaisedClass += ' two-digits';
        }
        if (this.props.handsRaised > 99) {
            handsRaisedClass += ' three-digits';
        }
        if (this.props.enableHandraise && this.props.handsRaised > 0) {
            handsRaised = e('span', {
                className: handsRaisedClass,
                children: [
                    e(HandRaisedIcon, {
                        className: 'hands-raised-icon'
                    }),
                    this.props.handsRaised
                ]
            })
        }

        let studentMenuHeaderChildren = [
            e(StudentCountIcon, {
                className: 'mobile-go-pro-icon',
                color: '#8CB4D2'
            }),
            e('span', {
                className: 'mobile-header-text',
                style: {
                    display: this.props.handsRaised > 0  ? 'none' : 'inline-block'
                },
                children: translate('Students')
            }),
            e('span', {
                className: countClass,
                children: countValue
            }),
            handsRaised,
            e('span', {
                className: 'clear-room-link link',
                children: translate('Clear Classroom'),
                onClick: (event) => {
                    event.stopPropagation(); // Don't expand/collapse the student menu when Clear Room is tapped.
                    c.clearRoomClicked();
                }
            })
        ];
        
        if (m.mobileStudentMenuVisible) {
            studentMenuHeaderChildren.push(
                e(UpChevronSmall, {
                    className: 'mobile-header-up-chevron',
                    color: '#cccccc'
                })
            );
        } else {
            studentMenuHeaderChildren.push(
                e(DownChevronSmall, {
                    className: 'mobile-header-down-chevron',
                    color: '#cccccc'
                })
            );
        }
        
        let studentMenuChildren = [
            e('li', {
                className: 'mobile-student-menu-header',
                children: studentMenuHeaderChildren,
                onClick: c.mobileStudentMenuClicked
            })
        ];
        
        if (m.mobileStudentMenuVisible) {
            let MAX_NAME_LENGTH = 22,
                students = [];
            
            if (this.props.studentList) {
                studentMenuChildren.push(
                    e('div', {
                        className: 'clearfix actions-container',
                        children: [
                            e('div', {
                                className: 'form show-ids-row align-left',
                                children: [
                                    e('input', {
                                        type: 'checkbox',
                                        id: 'show-student-ids',
                                        onClick: c.showIdsClicked,
                                        checked: m.showingStudentIds
                                    }),
                                    e('label', {
                                        htmlFor: 'show-student-ids',
                                        children: translate('Student IDs')
                                    })
                                ]
                            }),
                            e('div', {
                                className: 'form handraise-row align-right',
                                children: [
                                    e('input', {
                                        type: 'checkbox',
                                        id: 'handraise',
                                        onClick: c.toggleHandraiseStatus,
                                        checked: this.props.enableHandraise
                                    }),
                                    e('label', {
                                        htmlFor: 'handraise',
                                        id: 'handraise-label',
                                        children: translate('Hand Raise'),
                                        onClick: c.toggleHandraiseStatus
                                    })
                                ]
                            })
                        ]
                    })
                );
                for (let i = 0; i < this.props.studentList.length; i++) {
                    let student = this.props.studentList[i];
                    
                    let icon = e(LoggedInIcon, {
                        svgClass: 'header-logged-in'
                    });
                    
                    if (!student.logged_in) {
                        icon = e(LoggedOutIcon, {
                            svgClass: 'header-logged-out-svg',
                            pathClass: 'header-logged-out-path'
                        });
                    }
                    
                    let rowChildren = [
                        icon,
                        e('span', {
                            className: m.showingStudentIds ? 'header-student-name-with-id' : 'header-student-name-without-id',
                            children: utils.ellipsify(student.last_name + ', ' + student.first_name, MAX_NAME_LENGTH)
                        })
                    ];
                    
                    if (m.showingStudentIds) {
                        rowChildren.push(
                            e('span', {
                                className: 'header-student-id',
                                children: 'ID: ' + student.student_id
                            })
                        );
                    }
                    
                    let hand = e(HandraiseIconInactive, {
                        className:  'handraise handraise-off'
                    });
                    
                    if (student.handraise) {
                        hand = e(HandraiseIconActive, {
                            className:  'handraise handraise-on'
                        });
                    }
                    if (this.props.enableHandraise) {
                        rowChildren.push(
                            e('span', {
                                className: 'student-handraise',
                                children: [
                                    hand,
                                    e('span', {
                                        className: 'student-handraise-button',
                                        id: 'handraise-student-' + student.id,
                                        'data-id': student.id,
                                        onClick: (event) => c.toggleStudentHandraise(Number(event.target.getAttribute('data-id')))
                                    })
                                ]
                            })
                        );
                    }
                    students.push(
                        e('div', {
                            className: 'header-roster-row',
                            children: [
                                e('span', {
                                    children: rowChildren
                                })
                            ]
                        })
                    )
                }
            }
            
            studentMenuChildren.push(
                e('div', {
                    id: 'scrollable-class-list',
                    children: students
                })
            );
        }
        
        let studentMenu = [
            e('ul', {
                id: 'mobile-student-menu',
                children: studentMenuChildren
            })
        ];
        
        let resultsChildren = [
            e('span', {
                id: 'mobile-results-tab-text',
                className: 'mobile-nav-text' + (m.selectedTab === 'results' ? ' mobile-nav-text-selected' : '') + (this.props.disableResults ? ' disabled' : ''),
                children: translate('results')
            })
        ];
        
        if (!this.props.disableResults && platform.isPhone()) { // Results are not disabled, so an activity is running.
            resultsChildren.push(
                e(SmallActivityStatus, {
                    id: 'mobile-results-tab-activity',
                    className: 'mobile-results-animated-wifi-container',
                    animated: true
                })
            );
        }
        
        resultsChildren.push(
            e(RightChevronSmall, {
                className: 'mobile-nav-chevron',
                color: '#cccccc'
            })
        );
        
        let mobileNavChildren = [
            e('li', {
                className: 'mobile-nav-item',
                children: [
                    e('span', {
                        className: 'mobile-nav-text' + (m.selectedTab === 'launch' ? ' mobile-nav-text-selected' : ''),
                        children: translate('launch')
                    }),
                    e(RightChevronSmall, {
                        className: 'mobile-nav-chevron',
                        color: '#cccccc'
                    })
                ],
                onClick: c.launchClicked
            }),
            e('li', {
                className: 'mobile-nav-item',
                children: [
                    e('span', {
                        className: 'mobile-nav-text' + (m.selectedTab === 'quizzes' ? ' mobile-nav-text-selected' : ''),
                        children: translate('quizzes')
                    }),
                    e(RightChevronSmall, {
                        className: 'mobile-nav-chevron',
                        color: '#cccccc'
                    })
                ],
                onClick: c.quizzesClicked
            }),
            e('li', {
                className: 'mobile-nav-item',
                children: [
                    e('span', {
                        className: 'mobile-nav-text' + (m.selectedTab === 'rooms' ? ' mobile-nav-text-selected' : ''),
                        children: translate('classrooms')
                    }),
                    e(RightChevronSmall, {
                        className: 'mobile-nav-chevron',
                        color: '#cccccc'
                    })
                ],
                onClick: c.roomsClicked
            }),
            e('li', {
                className: 'mobile-nav-item',
                children: [
                    e('span', {
                        className: 'mobile-nav-text' + (m.selectedTab === 'reports' ? ' mobile-nav-text-selected' : ''),
                        children: translate('reports')
                    }),
                    e(RightChevronSmall, {
                        className: 'mobile-nav-chevron',
                        color: '#cccccc'
                    })
                ],
                onClick: c.reportsClicked
            }),
            e('li', {
                id: 'results-mobile-nav-item',
                className: 'mobile-nav-item',
                children: resultsChildren,
                onClick: c.resultsClicked
            })
        ];
        
        let navMenu = [
            e('ul', {
                id: 'mobile-nav-menu',
                children: mobileNavChildren
            })
        ];
        
        allChildren.push(
            userMenu,
            studentMenu,
            navMenu
        );

        let marginTop = (67 + document.getElementById('header-container').offsetTop + 'px').toString()
        
        return e('div', {
            id: 'mobile-teacher-header-menu',
            style: {
                top: marginTop
            },
            children: allChildren
        });
    }
    
}
