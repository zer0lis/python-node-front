import React from 'react';
import ActivityStatus from 'ActivityStatus';
import utils from 'Utils';

let e = React.createElement;

export default class HeaderRoomsMenu extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            rooms = [];
        
        if (this.props.rooms) {
            for (let room of this.props.rooms) {
                let roomName = room.name.toUpperCase();
                
                if (!room.in_menu || roomName === this.props.roomName.toUpperCase()) {
                    continue;
                }
                
                let displayName = utils.ellipsify(roomName, 22);
                
                rooms.push(
                    e(ActivityStatus, {
                        className: room.active ? 'rooms-menu-animated-wifi-container' : 'rooms-menu-wifi-container',
                        animated: room.active
                    }),
                    e('li', {
                        children: displayName,
                        onClick: () => c.headerRoomClicked(roomName)
                    })
                );
            }
        }
        
        return e('div', {
            id: 'rooms-menu-container',
            children: [
                e('div', {
                    id: 'rooms-menu',
                    style: {display: m.roomsMenuVisible ? 'block' : 'none'},
                    children: [
                        e('ul', {
                            children: rooms
                        })
                    ]
                })
            ]
        });
    }
    
}
