import React from 'react';
import platform from 'Platform';
import Constants from 'Constants';
import RenewIconSmall from 'RenewIconSmall';
import {translate} from 'translator';

let e = React.createElement;

export default class HeaderMenu extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            r = this.props.renewalModel,
            children = [];
        
        if (r.showRenewal) {
            let message = r.type === Constants.PRO_ENDING ? translate('Expiring') : translate('Expired');
            
            children.push(
                e('li', {
                    className: 'renew-notify',
                    children: [
                        e(RenewIconSmall),
                        e('span', {
                            children: translate('Renew your account')
                        }),
                        e('span', {
                            className: 'date',
                            children: `${message} ${r.expirationDateFormatted}`
                        })
                    ],
                    onClick: c.renewNotifyClicked
                })
            )
        }
        
        if (platform.isMobile()) {
            children.push(
                e('li', {
                    children: translate('REFRESH'),
                    onClick: c.refreshClicked
                })
            );
        }
        
        children.push(
            e('li', {
                children: translate('PROFILE'),
                onClick: c.profileClicked
            }),
            e('li', {
                children: translate('HELP'),
                onClick: c.helpClicked
            }),
            e('li', {
                children: translate('SIGN OUT'),
                onClick: c.signOutClicked
            })
        );
        return e('div', {
            id: 'account-menu-container',
            children: [
                e('div', {
                    id: 'account-menu',
                    style: {display: m.accountMenuVisible ? 'block' : 'none'},
                    children: [
                        e('i', {
                            'data-icon': 'f'
                        }),
                        e('ul', {
                            children: children
                        })
                    ]
                })
            ]
        })
    }
    
}
