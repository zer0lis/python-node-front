import React from 'react';
import LoggedInIcon from 'LoggedInIcon';
import LoggedOutIcon from 'LoggedOutIcon';
import HandraiseIconActive from 'HandraiseIconActive';
import HandraiseIconInactive from 'HandraiseIconInactive';
import utils from 'Utils';
import {translate} from 'translator';

let e = React.createElement;

export default class HeaderRosterMenu extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            MAX_NAME_LENGTH = 22,
            students = [];
        
        if (this.props.studentList) {
            for (let i = 0; i < this.props.studentList.length; i++) {
                let student = this.props.studentList[i];
                
                let icon = e(LoggedInIcon, {
                    svgClass: 'header-logged-in'
                });
                
                if (!student.logged_in) {
                    icon = e(LoggedOutIcon, {
                        svgClass: 'header-logged-out-svg',
                        pathClass: 'header-logged-out-path'
                    });
                }
                
                let rowChildren = [
                    icon,
                    e('span', {
                        className: m.showingStudentIds ? 'header-student-name-with-id' : 'header-student-name-without-id',
                        children: utils.ellipsify(student.last_name + ', ' + student.first_name, MAX_NAME_LENGTH)
                    })
                ];
                
                if (m.showingStudentIds) {
                    rowChildren.push(
                        e('span', {
                            className: 'header-student-id',
                            children: 'ID: ' + student.student_id
                        })
                    );
                }
                
                let hand = e(HandraiseIconInactive, {
                    className:  'handraise handraise-off'
                });
                
                if (student.handraise) {
                    hand = e(HandraiseIconActive, {
                        className:  'handraise handraise-on'
                    });
                }

                if (this.props.enableHandraise) {
                    rowChildren.push(
                        e('span', {
                            className: 'student-handraise',
                            children: [
                                hand,
                                e('span', {
                                    className: 'student-handraise-button',
                                    id: 'handraise-student-' + student.id,
                                    'data-id': student.id,
                                    onClick: c.toggleStudentHandraise
                                })
                            ]
                        })
                    );
                }
                
                students.push(
                    e('div', {
                        className: 'header-roster-row',
                        children: [
                            e('span', {
                                children: rowChildren
                            })
                        ]
                    })
                );
            }
        }
        
        let rosterMenuChildren = [
            e('i', {
                'data-icon': 'f'
            })
        ];

        if (this.props.hasRoster) {
            rosterMenuChildren.push(
                e('div', {
                    className: 'roster-class-list-header',
                    children: [
                        e('span', {
                            className: 'class-list-text',
                            children: m.studentsInRoom + '/' + this.props.rosterSize
                        }),
                        e('span', {
                            id: 'clear-room',
                            className: 'clear-room-link float-right link',
                            children: translate('Clear Classroom'),
                            onClick: c.clearRoomClicked
                        })
                    ]
                }),
                e('div', {
                    className: 'form show-ids-row',
                    children: [
                        e('input', {
                            type: 'checkbox',
                            id: 'show-student-ids',
                            onClick: c.showIdsClicked,
                            checked: m.showingStudentIds
                        }),
                        e('label', {
                            htmlFor: 'show-student-ids',
                            id: 'show-student-ids-label',
                            children: translate('Show Student IDs'),
                            onClick: c.showIdsClicked
                        })
                    ]
                }),
                e('div', {
                    className: 'form handraise-row',
                    children: [
                        e('input', {
                            type: 'checkbox',
                            id: 'handraise',
                            onClick: c.toggleHandraiseStatus,
                            checked: this.props.enableHandraise
                        }),
                        e('label', {
                            htmlFor: 'handraise',
                            id: 'handraise-label',
                            children: translate('Enable Hand Raise'),
                            onClick: c.toggleHandraiseStatus
                        })
                    ]
                }),
                e('div', {
                    id: 'scrollable-class-list',
                    children: students
                })
            );
        } else {
            rosterMenuChildren.push(
                e('span', {
                    id: 'clear-room',
                    className: 'clear-room-no-roster',
                    children: translate('Clear Classroom'),
                    onClick: c.clearRoomClicked
                })
            );
        }
        
        return e('div', {
            id: 'roster-menu-container',
            children: [
                e('div', {
                    id: 'roster-menu',
                    className: this.props.hasRoster ? '' : 'roster-menu-no-roster',
                    style: {display: m.rosterMenuVisible ? 'block' : 'none'},
                    children: rosterMenuChildren,
                    onClickCapture: (event) => {
                        let id = event.target.id;
                        
                        if (id === 'clear-room') {
                            c.clearRoomClicked();
                        } else {
                            event.nativeEvent.stopImmediatePropagation(); // Prevent the document event listener from closing this menu.
                            
                            if (id === 'show-student-ids') {
                                c.showIdsClicked();
                            }
                            
                            if (id === 'handraise') {
                                c.toggleHandraiseStatus();
                            }
                            
                            if (id.indexOf('handraise-student-') !== -1) {
                                c.toggleStudentHandraise(Number(event.target.getAttribute('data-id')));
                            }
                        }
                        
                        event.stopPropagation(); // Prevent the React listener from requesting the student list again.
                    }
                })
            ]
        });
    }
    
}
