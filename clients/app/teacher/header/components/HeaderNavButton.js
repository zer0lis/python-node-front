import React from 'react';
import uuid from 'uuid';
import SmallActivityStatus from 'SmallActivityStatus';

let e = React.createElement;

export default class HeaderNavButton extends React.Component {
    
    render() {
        let displayClass = this.props.selected ? 'selected' : '';
        
        if (this.props.disabled) {
            displayClass = 'disabled';
        }
        
        let children = [
            e('span', {
                id: this.props.results ? 'results-tab-text' : uuid.v4(),
                children: this.props.label
            })
        ];
        
        if (this.props.results && !this.props.disabled) {
            displayClass += ' header-results';
            children.push(
                e(SmallActivityStatus, {
                    id: 'results-tab-activity',
                    className: 'results-animated-wifi-container',
                    animated: true
                })
            );
        }
        
        return e('li', {
            className: displayClass,
            children: children,
            onClick: this.props.onClick
        });
    }
    
}
