import React from 'react';
import HeaderNavButton from 'HeaderNavButton';
import Constants from 'Constants';
import {translate} from 'translator';

let e = React.createElement;

export default class HeaderNav extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        return e('div', {
            id: 'header-nav',
            children: [
                e('ul', {
                    children: [
                        e(HeaderNavButton, Object.assign({
                            label: translate('launch'),
                            selected: m.selectedTab === Constants.LAUNCH_TAB,
                            onClick: c.launchClicked},
                            this.props)
                        ),
                        e(HeaderNavButton, Object.assign({
                            label: translate('quizzes'),
                            selected: m.selectedTab === Constants.QUIZZES_TAB,
                            onClick: c.quizzesClicked},
                            this.props)
                        ),
                        e(HeaderNavButton, Object.assign({
                            label: translate('classrooms'),
                            selected: m.selectedTab === Constants.ROOMS_TAB,
                            onClick: c.roomsClicked},
                            this.props)
                        ),
                        e(HeaderNavButton, Object.assign({
                            label: translate('reports'),
                            selected: m.selectedTab === Constants.REPORTS_TAB,
                            onClick: c.reportsClicked},
                            this.props)
                        ),
                        e(HeaderNavButton, Object.assign({
                            label: translate('results'),
                            selected: m.selectedTab === Constants.RESULTS_TAB,
                            onClick: c.resultsClicked,
                            results: true,
                            disabled: this.props.disableResults},
                            this.props)
                        )
                    ]
                })
            ]
        });
    }
    
}
