import React from 'react';
import ReactDOM from 'react-dom';
import view from 'ModalView';
import utils from 'Utils';

class ModalController {
    
    render(props) {
        let modalContainer = document.getElementById('modal-container');
        
        if (modalContainer) {
            modalContainer.parentNode.removeChild(modalContainer); // If a modal is open, close it before opening another.
        }

        modalContainer = document.createElement('div');
        modalContainer.id = 'modal-container';
        document.body.appendChild(modalContainer);

        ReactDOM.render(
            React.createElement(
                view, {
                    title:          props.title,
                    content:        props.content,
                    modalClass:     props.modalClass ? props.modalClass : '',
                    maxModalHeight: props.maxModalHeight ? props.maxModalHeight : '',
                    onCloseClicked: props.onCloseClicked ? props.onCloseClicked : controller.onCloseClicked,
                    modalWidth:     props.modalWidth ? props.modalWidth : '700'
                }
            ),
            modalContainer,
            function() {
                utils.blurPage();
            }
        );
    }

    onCloseClicked() {
        controller.closeModal();
    }
    
    closeModal(event) {
        if (event) {
            event.stopPropagation(); // Stop duplicate click events that might fire from buttons and the background.
        }
        
        let modalContainer = document.getElementById('modal-container');
        
        if (modalContainer) {
            ReactDOM.unmountComponentAtNode(modalContainer);
            modalContainer.parentNode.removeChild(modalContainer);
        }
        
        utils.unblurPage();
    }
}

let controller = new ModalController();
module.exports = controller;
