import React from 'react';
import CloseX from 'CloseX';
import platform from 'Platform';
import {translate} from 'translator';

let e = React.createElement;

export default class ModalView extends React.Component {
    
    render() {
        return e('div', {
            id: 'modal-wrapper',
            style: {
                width: this.props.modalWidth ? this.props.modalWidth : 700,
                marginLeft: this.props.modalWidth ? - this.props.modalWidth/2 : - 700/2
            },
            children: [
                e('div', {
                    id: 'modal-holder',
                    className: this.props.modalClass ? this.props.modalClass : '',
                    children: [
                        e('div', {
                            className: platform.browser === 'IE' ? 'modal-header ie-browser' : 'modal-header',
                            children: [
                                e('h1', {
                                    className: 'modal-title',
                                    children: this.props.title
                                }),
                                e('a', {
                                    className: 'cancel-button',
                                    title: translate('Close'),
                                    children: [
                                        e(CloseX, {
                                            color: '#ccc'
                                        })
                                    ],
                                    onClick: (event) => {
                                        event.preventDefault();
                                        this.props.onCloseClicked();
                                    }
                                })
                            ]
                        }),
                        e('div', {
                            id: 'modal-content',
                            children: this.props.content
                        })
                    ]
                })
            ]
        });
    }
    
}
