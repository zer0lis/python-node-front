/**
 * NOTE: This module is used in both the teacher and student headers.
 */

import React from 'react';
import HeaderLogo from 'HeaderLogo';
import HeaderRoomName from 'HeaderRoomName';

let e = React.createElement;

export default class BaseHeaderView extends React.Component {
    
    render() {
        return e('div', {
            id: 'base-header',
            children: [
                e(HeaderLogo, this.props),
                e(HeaderRoomName, this.props),
                this.props.children
            ]
        });
    }
    
}
