import React from 'react';
import ReactDOM from 'react-dom';
import view from 'FooterView';
import goPro from 'GoProController';

class FooterController {
    
    render(teacherRouter) {
        ReactDOM.render(
            React.createElement(
                view, {
                    learnMoreClicked: () => goPro.render({router: teacherRouter})
                }
            ),
            document.getElementById('footer')
        );
    }
    
}

module.exports = new FooterController();
