import React from 'react';
import ProBadgeIconSmall from 'ProBadgeIconSmall';
import user from 'user';
import {translate} from 'translator';

let e = React.createElement;

export default class FooterView extends React.Component {

    render() {
        let footerHTML = [];

        footerHTML.push(
            e('div', {
                className: user.isTeacher() ? 'align-left' : '',
                children: [
                    e('span', {
                        className: 'socrative',
                        children: 'Socrative'
                    }),
                    ' Student Response by ',
                    e('span', {
                        className: 'mc',
                        children: 'MasteryConnect'
                    })
                ]
            })
        );
        
        if (user.isPro()) {
            footerHTML.push(
                e('div', {
                    className: 'align-right',
                    children: [
                        e('span', {
                            className: 'pro-badge-icon-container',
                            children: e(ProBadgeIconSmall, {
                                className: 'pro-badge-icon',
                                pathClass: 'pro-badge-icon-path'
                            })
                        }),
                        e('span', {
                            dangerouslySetInnerHTML: {__html: translate("You're <strong>PRO!</strong>")}
                        })
                    ]
                })
            )
        } else if (user.isTeacher()) {
            footerHTML.push(
                e('div', {
                    className: 'align-right',
                    children: [
                        e('span', {
                            className: 'pro-badge-icon-container',
                            children: e(ProBadgeIconSmall, {
                                className: 'pro-badge-icon',
                                pathClass: 'pro-badge-icon-path'
                            })
                        }),
                        e('span', {
                            dangerouslySetInnerHTML: {__html: translate("Get <strong>PRO!</strong>")}
                        }),
                        e('span', {
                            className: 'link',
                            onClick: () => this.props.learnMoreClicked(),
                            children: translate('Learn More')
                        })
                    ]
                })
            )
        }
        
        return e('div', {
            className: 'footer vertical-align-middle',
            children: [
                e('div', {
                    className: 'footer-content clearfix',
                    children: [
                        footerHTML
                    ]
                })
            ]
        });
    }

}
