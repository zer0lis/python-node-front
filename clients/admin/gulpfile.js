'use strict';

let gulp = require('gulp'),
    concat = require('gulp-concat'),
    less = require('gulp-less');

let config = {
	port: 3005,
	devBaseUrl: 'localhost:3005',
	paths: {
		html: './app/*.html',
		hbs: './app/*.hbs',
		js: './app/js/*.js',
		scripts: './app/js/scripts/*.js',
		images: './app/images/*',
		files: './app/files/*',
		partials: './app/partials/*',
		css: './app/resources/**/*.css',
		lessCSS: './app/css/less/**/*.less',
		dist: './dist',
		mainJs: './app/js/main.js'
	}
};

gulp.task('html', () => {
	gulp.src(config.paths.html)
		.pipe(gulp.dest(config.paths.dist));
});

gulp.task('hbs', () => {
	gulp.src(config.paths.hbs)
		.pipe(gulp.dest(config.paths.dist));
});

gulp.task('js', () => {
	return gulp.src(config.paths.js)
    .pipe(concat('main.js'))
    .pipe(gulp.dest('./dist/js'));
});

gulp.task('scripts', () => {
	return gulp.src(config.paths.scripts)
		.pipe(gulp.dest(config.paths.dist + '/js/scripts'));
});

gulp.task('less', () => {
	return gulp.src('./app/css/less/main.less')
		.pipe(less())
		.pipe(gulp.dest(config.paths.dist + '/css'));
});

gulp.task('css', () => {
	gulp.src(config.paths.css)
		.pipe(concat('bundle.css'))
		.pipe(gulp.dest(config.paths.dist + '/css'));
});

gulp.task('images', () => {
	gulp.src(config.paths.images)
		.pipe(gulp.dest(config.paths.dist + '/images'))
});

gulp.task('files', () => {
    gulp.src(config.paths.files)
        .pipe(gulp.dest(config.paths.dist + '/files'))
});

gulp.task('partials', () => {
	gulp.src(config.paths.partials)
		.pipe(gulp.dest(config.paths.dist + '/partials'))
});

gulp.task('watch', () => {
	gulp.watch(config.paths.html, ['html']);
	gulp.watch(config.paths.hbs, ['hbs']);
    gulp.watch(config.paths.js, ['js']);
    gulp.watch(config.paths.lessCSS, ['less']);
	gulp.watch(config.paths.partials, ['partials']);
    gulp.watch(config.paths.scripts, ['scripts']);
});

gulp.task('default', ['html', 'hbs', 'js', 'scripts', 'css', 'less', 'images', 'files', 'partials', 'watch']);

gulp.task('build', ['html', 'hbs', 'js', 'scripts', 'css', 'less', 'images', 'files', 'partials']);
