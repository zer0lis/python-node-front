$(function () {
    var email = $('#email');
    var notify = $('.notify');
    var validData = false;
    $('#get-deleted-quizzes').click(function() {
        if (email.val() === '') {
            notify.addClass('error');
            notify.html('Please complete email');
            validData = false;
            return;
        }

        if (email.val().indexOf('@') == -1 && email.val().indexOf('.') == -1) {
            notify.addClass('error');
            notify.html('Email format is not correct');
            return;
        }

        notify.removeClass('error');
        notify.text();
        validData = true;

        if (validData) {
            var data = {};
            data.email = email.val();

            $.ajax({
                url: '/deleted-quizzes',
                contentType: 'application/json',
                type: 'POST',
                data: JSON.stringify(data),

                success: function(response) {
                    let deletedQuizzes = [];
                    $.each(response, function(index, item) {
                        deletedQuizzes.push('<div class="default-listing deleted-quiz">Quiz Name: ' + item.quiz_name + '</br>' + 'SOCNumber: ' + item.soc_number + '<button class="undelete-quiz action-button" id="' + item.id + '">Undelete Quiz</button></div>')
                    });

                    if (deletedQuizzes.length === 0) {
                        deletedQuizzes.push('<div class="default-listing deleted-quiz"> There are no deleted quizzes for this account</div>')
                    }

                    notify.addClass('success');
                    notify.html(deletedQuizzes);
                },

                error: function(err) {
                    notify.addClass('error');
                    notify.text(err.responseText);
                }

            }) 
        }
    });

	$('.undelete-quiz-wrapper').on('click', '.undelete-quiz', function() {
		var quizId = $(this).attr('id');

        var data = {};
        data.id = quizId;
        data.email = email.val();
        notify.attr('class', 'notify');
        notify.html('');
        $.ajax({
            url: '/undelete/quiz',
            contentType: 'application/json',
            type: 'POST',
            data: JSON.stringify(data),

            success: function(response) {
                notify.addClass('success');
                notify.html('Quiz has been readded to the account');
            },

            error: function(err) {
                notify.addClass('error');
                notify.html(err.responseText);
            }
        });
    });
})