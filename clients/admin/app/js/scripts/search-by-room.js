$(function () {
    var room = $('#room');
    var notify = $('.notify');
    var validData = false;
    $('#search-by-room').click(function() {
        if (room.val().trim() === '') {
            notify.addClass('error');
            notify.text('Please enter a room name');
            validData = false;
            return;
        }

        notify.removeClass('error');
        notify.text();
        validData = true;

        if (validData) {
            var data = {};
            data.room = room.val();

            $.ajax({
                url: '/search/room',
                contentType: 'application/json',
                type: 'POST',
                data: JSON.stringify(data),

                success: function(response) {
                    notify.removeClass('error');
                    notify.addClass('success');
                    notify.html(
                        `User info: (date format MM-DD-YYYY)<br />
                         Email: ${response.email}<br />
                         Last Login: ${response.lastLogin}<br />
                         Register Date: ${response.registerDate}<br />
                         Created Date: ${response.createdDate}<br />
                         License Key: ${response.key}<br />
                         License Expiration Date: ${response.expirationDate}<br />
                         Buyer Email: ${response.buyerEmail}`
                    );
                },

                error: function(err) {
                    notify.removeClass('success');
                    notify.addClass('error');
                    notify.text(err.responseText);
                }

            }) 
        }
    });
});
