var addBannerNotify = $('.add-banner-section .notify');
var activeBannersNotify = $('.active-banner-section .notify');

$(function() {
    var audience = $('#audience');
    
    function toggleGoPro() {
        var free = audience.val() == 1,
            goPro = $('#action-popup');
        
        if (!free && goPro.prop('checked')) {
            $('#no-action').prop('checked', true);
        }
        
        goPro.prop('disabled', !free);
    }
    
    audience.on('change', toggleGoPro);
    toggleGoPro();
    
    $('#view-active-banner').click(function() {
        activeBannersNotify.attr('class', 'notify');
        $.ajax({
            url: '/current-banners',
            contentType: 'application/json',
            type: 'GET',
            success: function(rawBanners) {
                if (rawBanners.length === 0) {
                    activeBannersNotify.addClass('info');
                    activeBannersNotify.html('There is no active banner on this platform.');
                } else {
                    var currentBanners = [];
                    $.each(rawBanners, function(index, item) {
                        var audience = 'All',
                            action = 'None';
                        
                        if (item.audience == 1) {
                            audience = 'Free';
                        } else if (item.audience == 2) {
                            audience = 'Pro';
                        }
                        
                        if (item.action_type === 'goProPopup') {
                            action = 'Go PRO Modal'
                        } else if (item.action_type === 'externalResource') {
                            action = 'External URL (' + item.url + ')';
                        }
                        
                        currentBanners.push(
                            `<div class="default-listing active-banners">
                                 Title: ${item.title}</br>
                                 Content: ${item.content}</br>
                                 Audience: ${audience}</br>
                                 Action: ${action}</br>
                                 Force Reappear: ${!item.dismissible}</br>
                             </div>
                             <button class="stop-banner action-button" id="${item.id}">TURN BANNER OFF</button>`
                        );
                    });
                    activeBannersNotify.addClass('success');
                    activeBannersNotify.html(currentBanners);
                }
            },
            error: function(err) {
                activeBannersNotify.addClass('error');
                activeBannersNotify.html(err.responseText);
            }
        });
    });

    $('.active-banner-section').on('click', '.stop-banner', function() {
        activeBannersNotify.attr('class', 'notify');
        activeBannersNotify.html('');
        $.ajax({
            url: '/disable-banner',
            contentType: 'application/json',
            type: 'POST',
            data: JSON.stringify({id: $(this).attr('id')}),
            success: function(response) {
                activeBannersNotify.addClass('success');
                activeBannersNotify.html(response);
            },
            error: function(err) {
                activeBannersNotify.addClass('error');
                activeBannersNotify.html(err.responseText);
            }
        });
    });

    var title = $('#title'),
        content = $('#content'),
        action = $('.action-container'),
        url = $('#external-link'),
        deploy = $('#deploy-banner'),
        dismissible = $('#force-reappear');

    deploy.click(function() {
        var errorsHTML = '';
        var errorsList = [];
        
        if (title.val() === '') {
            errorsList.push('Please add a banner title')
        }

        if (content.val() === '') {
            errorsList.push('Please add a banner content')
        }

        if (errorsList.length === 0) {
            errorsHTML = '';
            addBannerNotify.removeClass('error');
            addBannerNotify.html(errorsHTML);

            var bannerData = {
                audience: audience.find(':selected').val(),
                title: title.val(),
                content: content.val(),
                action: $('.action-container input:checked').val(),
                dismissible: !dismissible.is(':checked')
            };

            if (url.val()) {
                bannerData.url = url.val();
            }
            
            $.ajax({
                url: '/activate-banner',
                contentType: 'application/json',
                data: JSON.stringify(bannerData),
                type: 'POST',
                success: function(response) {
                    addBannerNotify.addClass('success');
                    addBannerNotify.html(response);
                },
                error: function(err) {
                    addBannerNotify.addClass('error');
                    addBannerNotify.html(err.responseText);
                }
            })

        } else {
            $.each(errorsList, function( index, value ) {
                errorsHTML += '<p>' + value + '</p>';
            });
            addBannerNotify.addClass('error');
            addBannerNotify.html(errorsHTML);
        }
    });

});
