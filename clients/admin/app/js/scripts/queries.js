$(function() {
    var submit = $('#submit-query'),
        emails = $('#emails-to-send'),
        querys = $('#querys'),
        notify = $('.notify'),
        queryDescription = $('.description'),
        queryOutput = $('.query-output');

    $( "#start-date").datepicker({
        dateFormat: "yy-mm-dd",
        maxDate: "-1d"
    });
    $( "#end-date").datepicker({
        dateFormat: "yy-mm-dd",
        maxDate: "+0d",
    });

    var startDate = $( "#start-date");
    var endDate = $( "#end-date");
    var validData = false;

    checkQuery = function() {
        queryDescription.hide();
        queryOutput.hide();
        var query = querys.val();
        $(`.description.${query}`).show();

        if(query === 'logins') {
            queryOutput.show();
        }
    };

    checkQuery();

    querys.on('change', function() {
        checkQuery();
    });
    
    $(submit).click(function() {
        var errorsHTML = '';
        var errorsList = [];

        notify.removeClass('error');
        notify.removeClass('success');

        var emailsToSend = emails.val().split(',').map(function(item) {
          return item.trim();
        });

        if (emails.val() === '') {
            errorsList.push('Please add at least one email');
        } else {
            $.each(emailsToSend, function(index, email) {
                if (email.indexOf('@') == -1 || email.indexOf('.') == -1) {
                    errorsList.push('Email format is not correct for email -> ' + email);
                }
            })
        }

        if (startDate.val() === '') {
            errorsList.push('Please complete start date');
        }

        if (endDate.val() === '') {
            errorsList.push('Please complete end date');
        }

        if (endDate.val() <= startDate.val()) {
            errorsList.push('End date should be at least one day after start date');
        }

        if (errorsList.length === 0) {
            errorsHTML = '';
            notify.removeClass('error');
            notify.html(errorsHTML);
            
            var data = {};
            data.emailType = querys.val();
            data.emails = emailsToSend;
            data.startDate = startDate.val();
            data.endDate = endDate.val();
            data.orgType = $("input[name='org_type']:checked"). val();
            data.output = querys.val() === 'logins' ? $("input[name='output']:checked").val() : null;

            $.ajax({
                url: '/queries',
                contentType: 'application/json',
                type: 'POST',
                data: JSON.stringify(data),

                success: function(message) {
                    notify.addClass('success');
                    notify.text(message);
                },

                error: function(err) {
                    notify.addClass('error');
                    notify.text(err.responseText);
                }

            }) 
        } else {
            $.each(errorsList, function( index, value ) {
                errorsHTML += '<p>' + value + '</p>';
            });
            notify.addClass('error');
            notify.html(errorsHTML);
        }
    });
});
