$(function () {
    var email = $('#email');
    var notify = $('.notify');
    var validData = false;
    $('#downgrade-user').click(function() {
        if (email.val() === '') {
            notify.addClass('error');
            notify.text('Please complete email');
            validData = false;
            return;
        }

        if (email.val().indexOf('@') == -1 && email.val().indexOf('.') == -1) {
            notify.addClass('error');
            notify.text('Email format is not correct');
            return;
        }

        notify.removeClass('error');
        notify.text();
        validData = true;

        if (validData) {
            var data = {};
            data.email = email.val();

            $.ajax({
                url: '/user/downgrade',
                contentType: 'application/json',
                type: 'POST',
                data: JSON.stringify(data),

                success: function(message) {
                    notify.removeClass('error');
                    notify.addClass('success');
                    notify.text(message);
                },

                error: function(err) {
                    notify.removeClass('success');
                    notify.addClass('error');
                    notify.text(err.responseText);
                }

            }) 
        }
    });
})