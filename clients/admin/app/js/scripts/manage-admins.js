$(function() {
    let notify = $('.notify');
    
    $.ajax({
        url: '/admins-list',
        contentType: 'application/json',
        type: 'GET',

        success: function(response) {
            var list = [];

            for (var i = 0; i < response.length; i++) {
                list.push('<tr id="' + response[i].user_id + '"><td class="email">' + response[i].email + '</td><td>' + response[i].role + '</td><td><button class="danger delete">Delete</button></td></tr>')
            }

            $('#admins-list').append(list);
        },

        error: function() {
            // console.log('error');
        }
    });
    var adminForm = $('#add-admin-form');
    $('#add-admin').click(function() {
        $(this).hide();
        adminForm.show();
    });

    $('#cancel').click(function(){
        adminForm.hide();
        notify.hide();
        notify.removeClass('error');
        notify.text();
        $('#add-admin').show();
    });

    $('#submit-user').click(function(){
        var data = {};
        data.email = $('#email').val();
        data.role = $('#role').val();
        
        $.ajax({
            url: '/add-admin/',
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(data),

            success: function() {
                window.top.location.reload();
            },

            error: function(error) {
                notify.addClass('error');
                notify.text(error.responseText);
                notify.show();
            }
        });
    });

    $('#admins-list').on('click', '.delete', function(){
        $.ajax({
            url: '/remove-admin/' + $(this).parent().parent().attr('id'),
            type: 'DELETE',
            contentType: 'application/json',

            success: function() {
                window.top.location.reload();
            },

            error: function() {
                // console.log('error');
            }
        });
    })
});