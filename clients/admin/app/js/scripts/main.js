function toggleLoader(element, state) {
    if (state === 'show') {
        $(element).addClass('loading-state');
    } else {
        setTimeout(function() {
            $(element).removeClass('loading-state');
        }, 300)
    }
}

function hideNotify(wait) {
    var time = wait || 3000,
        notify = $('.notify');

    setTimeout(function() {
        notify.slideUp(300, function() {
            notify.removeClass('error success info').html('').attr('style', '');
        });
    }, time)
}
