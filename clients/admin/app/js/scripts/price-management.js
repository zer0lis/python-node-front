'use strict';

$(function () {
    var basePriceNotify = $('.base-price-container .notify');
    var bulkPriceNotify = $('.bulk-price-container .notify');
    var couponsNotify = $('.coupons-container .notify');
    let renewalPricesNotify = $('.renewal-prices-container .notify');

    function convertToDollars(amountInPennies) {
        var dollarPrice = amountInPennies / 100;
        if (/\.\d$/.test(dollarPrice)) {
            dollarPrice += '0';
        } else if (!/\./.test(dollarPrice)) {
            dollarPrice += '.00';
        }
        return dollarPrice;
    }
    
    $('#dialog').dialog({
        autoOpen: false,
        dialogClass: 'no-close',
        modal: true
    });

/*--------------*/
/*    Prices    */
/*--------------*/

    var base_price = $('#base-price'),
        min_seats = $('#min-seats'),
        update_base_price = $('#update-base-price'),
        tiersListingWrapper = $('.tiers-listing tbody');

    $.ajax({
        url: '/prices',
        contentType: 'application/json',
        type: 'GET',

        beforeSend: function() {
            toggleLoader('.base-price-container', 'show');
            toggleLoader('.bulk-price-container', 'show');
        },

        success: function(response) {
            var tiersList = [];

            if (response.prices) {
                var prices = response.prices,
                    basePrice = convertToDollars(prices[0].amount);
                    
                base_price.val(basePrice);
                min_seats.val(prices[0].seats);
                base_price.attr('data-price-id', prices[0].id);
                $('#current-value').html(basePrice);

                
                if (prices.length > 1) {
                    $.each(prices, function(index, price) {
                        if (price.seats > 1) {
                            let dollars = convertToDollars(price.amount);
                            tiersList.push(
                                `<tr data-price-id="${price.id}">
                                    <td><input type="text" value="${price.name}" class="tier-name"/></td>
                                    <td><input type="number" value="${price.seats}" class="tier-seats" /></td>
                                    <td class="price-amount clearfix">
                                        <input type="number" value="${dollars}" class="tier-amount" />
                                        <span class="current-value-wrapper">$${dollars}</span>
                                    </td>
                                    <td>
                                        <div class="actions-container">
                                            <button class="button btn-small primary update-tier">Update</button>
                                            <button class="button btn-small danger delete-tier">Delete</button>
                                        </div>
                                    </td>
                                </tr>`
                            );
                        }
                    });
                }
            }

            tiersListingWrapper.html(tiersList);
        },

        error: function() {
            basePriceNotify.addClass('error');
            basePriceNotify.html('There was an error retrieving prices.');
        },

        complete: function() {
            toggleLoader('.base-price-container', 'hide');
            toggleLoader('.bulk-price-container', 'hide');
        }
    });

    update_base_price.click(function() {
        var errors = false;
        var data = {};
        basePriceNotify.removeClass('error success');
        data.amount = base_price.val();
        data.seats = min_seats.val();
        data.name = 'Base Price';
        var id = base_price.attr('data-price-id');

        if (base_price.val().replace(/\D/g,'') === '') {
            errors = true;
            basePriceNotify.addClass('error');
            basePriceNotify.html('Please add an amount.');
        }

        if (base_price.val().replace(/\D/g,'') <= 0) {
            errors = true;
            basePriceNotify.addClass('error');
            basePriceNotify.html('Please add a positive value.');
        }

        if (!errors) {
            basePriceNotify.removeClass('error');
            basePriceNotify.html('');

            $.ajax({
                url: `/prices/${id}`,
                contentType: 'application/json',
                type: 'PUT',
                data: JSON.stringify(data),
                beforeSend: function() {
                    toggleLoader('.base-price-container', 'show');
                },

                success: function(response) {
                    basePriceNotify.removeClass('error').addClass('success');
                    basePriceNotify.html(response);
                    $('#current-value').html(data.amount);
                },

                error: function(err) {
                    basePriceNotify.removeClass('success').addClass('error');
                    basePriceNotify.html(err.responseText);
                },

                complete: function() {
                    toggleLoader('.base-price-container', 'hide');
                    hideNotify();
                }
            });
        }
    });

    let toggleTierForm = function(action) {
        let tierFormButton = $('#toggle-tier-form'),
            addTierForm = $('.add-tier-form');

        if (action === 'show') {
            tierFormButton.fadeOut(300, function() {
                addTierForm.fadeIn(300);
            });
        } else {
            addTierForm.fadeOut(300, function() {
                tierFormButton.fadeIn(300);
            });
        }
    };

    $('#toggle-tier-form').click(function() {
        toggleTierForm('show');
    });

    $('#cancel-tier').click(function() {
        toggleTierForm('hide');
        clearAddTierForm();
    });

    // add tier
    var addTierName = $('#add-tier-name'),
        addTierSeats = $('#add-tier-seats'),
        addTierAmount = $('#add-tier-amount');

    let clearAddTierForm = function() {
        addTierName.val('');
        addTierSeats.val('');
        addTierAmount.val('');
    };

    $('#add-tier').click(function() {
        var data = {};
        var errorsHTML = '';
        var errorsList = [];
        
        data.name = addTierName.val().trim();
        data.seats = addTierSeats.val();
        data.amount = addTierAmount.val();

        if (data.name === '') {
            errorsList.push('Please add tier name');
        }

        if (data.seats === '') {
            errorsList.push('Please add tier seat');
        } else if (data.seat < 1) {
            errorsList.push('Min seats must be at least 1');
        }

        if (data.amount === '') {
            errorsList.push('Please add tier amount');
        } else if (data.amount < 0) {
            errorsList.push('The amount must be positive');
        }

        if (errorsList.length === 0) {
            bulkPriceNotify.removeClass('error');
            bulkPriceNotify.html('');
            $.ajax({
                url: '/prices',
                contentType: 'application/json',
                type: 'POST',
                data: JSON.stringify(data),

                beforeSend: function() {
                    toggleLoader('.bulk-price-container', 'show');
                },

                success: function(response) {
                    clearAddTierForm();
                    toggleTierForm('hide');
                    bulkPriceNotify.removeClass('error').addClass('success');
                    bulkPriceNotify.html(response.message);
                    tiersListingWrapper.append(`<tr data-price-id="${response.id}">
                                <td><input type="text" value="${data.name}" class="tier-name"/></td>
                                <td><input type="number" value="${data.seats}" class="tier-seats" /></td>
                                <td class="price-amount clearfix">
                                    <input type="number" value="${data.amount}" class="tier-amount" />
                                    <span class="current-value-wrapper">$${data.amount}</span>
                                </td>
                                <td>
                                    <div class="actions-container">
                                        <button class="button btn-small primary update-tier">Update</button>
                                        <button class="button btn-small danger delete-tier">Delete</button>
                                    </div>
                                </td>
                            </tr>`)
                },

                error: function(err) {
                    bulkPriceNotify.removeClass('success').addClass('error');
                    bulkPriceNotify.html(err.responseJSON.message);
                },

                complete: function() {
                    toggleLoader('.bulk-price-container', 'hide');
                    hideNotify();
                }
            });
        } else {
            $.each(errorsList, function( index, value ) {
                errorsHTML += '<p>' + value + '</p>';
            });
            bulkPriceNotify.addClass('error');

            bulkPriceNotify.html(errorsHTML);
        }
    });

    $('.tiers-listing').on('click', '.update-tier', function() {
        let id = $(this).parents('tr').attr('data-price-id'),
            parentRow = $(`[data-price-id="${id}"]`);
        
        var data = {
            name: parentRow.find($('.tier-name')).val().trim(),
            seats: parentRow.find($('.tier-seats')).val(),
            amount: parentRow.find($('.tier-amount')).val()
        };
        
        $.ajax({
            url: `/prices/${id}`,
            contentType: 'application/json',
            type: 'PUT',
            data: JSON.stringify(data),

            success: function(response) {
                bulkPriceNotify.removeClass('error').addClass('success');
                bulkPriceNotify.html(response);
                parentRow.find('.current-value-wrapper').html(`$${data.amount}`);
            },

            error: function(error) {
                bulkPriceNotify.removeClass('success').addClass('error');
                bulkPriceNotify.html(error.responseText);
            },

            complete: function() {
                hideNotify();
            }
        });
    });

    $('.tiers-listing').on('click', '.delete-tier', function() {
        let id = $(this).parents('tr').attr('data-price-id');
    
        $('#message').text('Are you sure you want to delete this price?');
    
        $('#dialog').dialog(
            'option',
            'buttons',
            [
                {
                    text: 'Yes',
                    click: function() {
                        $.ajax({
                            url: `/prices/${id}`,
                            contentType: 'application/json',
                            type: 'DELETE',
                            success: function(response) {
                                $('.tiers-listing').find(`[data-price-id="${id}"]`).remove();
                            },
                            error: function(error) {
                                bulkPriceNotify.removeClass('success').addClass('error');
                                bulkPriceNotify.html(error.responseText);
                            }
                        });
                        $(this).dialog('close');
                    }
                },
                {
                    text: 'No',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            ]
        );
    
        $('#dialog').dialog('open');
        
    });

/*---------------*/
/*    Coupons    */
/*---------------*/

    $( "#add-coupon-expiration-date").datepicker({dateFormat: "mm/dd/yy"});

    var couponsListingWrapper = $('.coupons-listing tbody');
    
    $.ajax({
        url: '/coupons',
        contentType: 'application/json',
        type: 'GET',

        beforeSend: function() {
            toggleLoader('.coupons-container', 'show');
        },

        success: function(coupons) {
            var couponsList = [];
            if (coupons.length > 0) {
                $.each(coupons, function(index, item) {
                    let expirationDate = new Date(item.expiration_date);
    
                    // Make sure the coupon expiration date does not fall behind due to time zone.
                    if (expirationDate.getTimezoneOffset() > 0) {
                        expirationDate.setMinutes(expirationDate.getMinutes() + expirationDate.getTimezoneOffset());
                    }
                    
                    let month = expirationDate.getMonth() + 1,
                        date = expirationDate.getDate(),
                        year = expirationDate.getFullYear();
                    
                    if (month < 10) {
                        month = `0${month}`;
                    }
                    
                    if (date < 10) {
                        date = `0${date}`;
                    }
                    
                    couponsList.push(
                        `<tr data-coupon-id="${item.id}" data-coupon-name="${item.name}">
                            <td><input type="text" value="${item.name}" class="coupon-name"/></td>
                            <td><input type="number" value="${convertToDollars(item.amount)}" class="coupon-amount" /></td>
                            <td>
                                <span class="switch-container">
                                    <label class="switch">
                                        <input type="checkbox" class="coupon-bulk" ${item.allow_bulk ? 'checked' : ''} />
                                        <div></div>
                                    </label>
                                </span>
                            </td>
                            <td><input type="text" class="coupon-expiration-date" value="${month}/${date}/${year}" /></td>
                            <td>
                                <div class="actions-container">
                                    <button class="button btn-small primary update-coupon">Update</button>
                                    <button class="button btn-small danger delete-coupon">Delete</button>
                                </div>
                            </td>
                        </tr>`
                    );
                });
            }

            couponsListingWrapper.html(couponsList);
        },

        error: function() {
            couponsNotify.addClass('error');
            couponsNotify.html('There was an error retrieving coupons.');
        },

        complete: function() {
            toggleLoader('.coupons-container', 'hide');
            $( ".coupon-expiration-date").datepicker({
                dateFormat: 'mm/dd/yy'
            });
        }
    });

    let toggleCouponForm = function(action) {
        let couponFormButton = $('#toggle-coupon-form'),
            addCouponForm = $('.add-coupon-form');

        if (action === 'show') {
            couponFormButton.fadeOut(300, function() {
                addCouponForm.fadeIn(300);
            });
        } else {
            addCouponForm.fadeOut(300, function() {
                couponFormButton.fadeIn(300);
            });
        }
    };

    $('#toggle-coupon-form').click(function() {
        toggleCouponForm('show');
    });

    $('#cancel-coupon').click(function() {
        toggleCouponForm('hide');
        clearAddCouponForm();
    });

    // add coupon
    var addCouponName = $('#add-coupon-name'),
        addCouponAmount = $('#add-coupon-amount'),
        addCouponAllowBulk = $('#add-coupon-allow-bulk'),
        addCouponExpirationDate = $('#add-coupon-expiration-date');

    let clearAddCouponForm = function() {
        addCouponName.val('');
        addCouponAmount.val('');
        addCouponAllowBulk.attr('checked', false);
        addCouponExpirationDate.val('');
    };

    $('#add-coupon').click(function() {
        var errorsHTML = '';
        var errorsList = [];
        var data = {};
        
        data.name = addCouponName.val().trim();
        data.amount = addCouponAmount.val();
        data.allowBulk = addCouponAllowBulk.is(':checked');
        data.expirationDate = addCouponExpirationDate.val();

        if (data.name === '') {
            errorsList.push('Please add a coupon name.')
        }

        if (data.amount === '') {
            errorsList.push('Please add an amount.');
        } else if (data.amount < 0) {
            errorsList.push('The amount should be positive.');
        }

        if (data.expirationDate === '') {
            errorsList.push('Please add an expiration date');
        }

        if (errorsList.length === 0) {
            couponsNotify.removeClass('error');
            couponsNotify.html('');

            $.ajax({
                url: '/coupons',
                contentType: 'application/json',
                type: 'POST',
                data: JSON.stringify(data),

                success: function(response) {
                    clearAddCouponForm();
                    toggleCouponForm('hide');

                    couponsNotify.removeClass('error').addClass('success');
                    couponsNotify.html(response.message);

                    couponsListingWrapper.append(`<tr data-coupon-id="${response.id}" data-coupon-name="${data.name}">
                            <td><input type="text" value="${data.name}" class="coupon-name"/></td>
                            <td><input type="number" value="${data.amount}" class="coupon-amount" /></td>
                            <td>
                                <span class="switch-container">
                                    <label class="switch">
                                        <input type="checkbox" class="coupon-bulk" ${data.allowBulk ? 'checked' : ''} />
                                        <div></div>
                                    </label>
                                </span>
                            </td>
                            <td><input type="text" class="coupon-expiration-date" value="${data.expirationDate}" /></td>
                            <td>
                                <div class="actions-container">
                                    <button class="button btn-small primary update-coupon">Update</button>
                                    <button class="button btn-small danger delete-coupon">Delete</button>
                                </div>
                            </td>
                        </tr>`)
                },

                error: function(error) {
                    couponsNotify.removeClass('success').addClass('error');
                    couponsNotify.html(JSON.parse(error.responseText).message);
                },

                complete: function() {
                    hideNotify();
                }
            });
        } else {
            $.each(errorsList, function( index, value ) {
                errorsHTML += '<p>' + value + '</p>';
            });
            couponsNotify.addClass('error');

            couponsNotify.html(errorsHTML);
        }
    });

    $('.coupons-listing').on('click', '.update-coupon', function() {
        let id = $(this).parents('tr').attr('data-coupon-id'),
            parentRow = $(`[data-coupon-id="${id}"]`);
        
        var data = {
            initialName: parentRow.attr('data-coupon-name'),
            name: parentRow.find($('.coupon-name')).val().trim(),
            amount: parentRow.find($('.coupon-amount')).val(),
            allowBulk: parentRow.find($('.coupon-bulk')).is(':checked'),
            expirationDate: parentRow.find($('.coupon-expiration-date')).val()
        };
        
        $.ajax({
            url: `/coupons/${id}`,
            contentType: 'application/json',
            type: 'PUT',
            data: JSON.stringify(data),

            success: function(response) {
                couponsNotify.removeClass('error').addClass('success');
                couponsNotify.html(response);
                parentRow.attr('data-coupon-name', data.name);
            },

            error: function(error) {
                couponsNotify.removeClass('success').addClass('error');
                couponsNotify.html(error.responseText);
            },

            complete: function() {
                hideNotify();
            }
        });
    });

    $('.coupons-listing').on('click', '.delete-coupon', function() {
        let id = $(this).parents('tr').attr('data-coupon-id');
        
        $('#message').text('Are you sure you want to delete this coupon?');
        
        $('#dialog').dialog(
            'option',
            'buttons',
            [
                {
                    text: 'Yes',
                    click: function() {
                        $.ajax({
                            url: `/coupons/${id}`,
                            contentType: 'application/json',
                            type: 'DELETE',
                            success: function() {
                                $(`.coupons-listing`).find(`[data-coupon-id="${id}"]`).remove();
                            },
                            error: function(error) {
                                couponsNotify.removeClass('success').addClass('error');
                                couponsNotify.html(error.responseText);
                            }
                        });
                        $(this).dialog('close');
                    }
                },
                {
                    text: 'No',
                    click: function () {
                        $(this).dialog('close');
                    }
                }
            ]
        );
    
        $('#dialog').dialog('open');
    });
    
/*----------------------*/
/*    Renewal Prices    */
/*----------------------*/
    
    let renewalPricesTable = $('.renewal-prices-list tbody');
    
    let renewalPriceNames = {
        K12: 'K-12',
        USHE: 'Higher Ed',
        CORP: 'Corporate',
        OTHR: 'Other'
    };
    
    $.ajax({
        url: '/renewal-prices',
        contentType: 'application/json',
        type: 'GET',
        beforeSend: function() {
            toggleLoader('.renewal-prices-container', 'show');
        },
        success: function(renewalPrices) {
            let renewalPriceRows = [];
            
            for (let renewalPrice of renewalPrices) {
                let dollars = convertToDollars(renewalPrice.amount);
                
                renewalPriceRows.push(
                    `<tr data-renewal-price-id="${renewalPrice.id}">
                        <td><input type="text" value="${renewalPriceNames[renewalPrice.organizationType] || 'None'}" class="renewal-price-org-type" readonly/></td>
                        <td class="price-amount clearfix">
                            <input type="number" value="${dollars}" class="renewal-price-amount" />
                            <span class="current-value-wrapper">$${dollars}</span>
                        </td>
                        <td>
                            <div class="actions-container">
                                <button class="button btn-small primary update-renewal-price">Update</button>
                            </div>
                        </td>
                    </tr>`
                );
            }
            
            renewalPricesTable.html(renewalPriceRows);
        },
        error: function() {
            renewalPricesNotify.addClass('error');
            renewalPricesNotify.html('There was an error retrieving renewal prices.');
        },
        complete: function() {
            toggleLoader('.renewal-prices-container', 'hide');
        }
    });
    
    $('.renewal-prices-list').on('click', '.update-renewal-price', function() {
        let id = $(this).parents('tr').attr('data-renewal-price-id'),
            parentRow = $(`[data-renewal-price-id="${id}"]`);
        
        let data = {
            amount: parentRow.find($('.renewal-price-amount')).val()
        };
        
        $.ajax({
            url: `/renewal-prices/${id}`,
            contentType: 'application/json',
            type: 'PUT',
            data: JSON.stringify(data),
            success: function(response) {
                renewalPricesNotify.removeClass('error').addClass('success');
                renewalPricesNotify.html(response);
                parentRow.find('.current-value-wrapper').html(`$${data.amount}`);
            },
            error: function(error) {
                renewalPricesNotify.removeClass('success').addClass('error');
                renewalPricesNotify.html(error.responseText);
            },
            complete: function() {
                hideNotify();
            }
        });
    });
    
});
