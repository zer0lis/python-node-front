$(function () {
    var email_to_keep = $('#first-account');
    var email_to_merge = $('#second-account');
    var notify = $('.merge-response');
    var validData = false;
    $('#merge-accounts').click(function() {
        if (email_to_keep.val() === '' || email_to_merge.val() === '') {
            notify.addClass('error');
            notify.text('Please complete both fields');
            validData = false;
        } else if ((email_to_keep.val().indexOf('@') == -1 && email_to_keep.val().indexOf('.') == -1) || (email_to_merge.val().indexOf('@') == -1 && email_to_merge.val().indexOf('.') == -1) ) {
            notify.addClass('error');
            notify.text('Email format is not correct');
        } else {
            notify.removeClass('error');
            notify.text();
            validData = true;
        }

        if (validData) {
            var data = {};
            data.email_to_keep = email_to_keep.val();
            data.email_to_merge = email_to_merge.val();

            $.ajax({
                url: '/merge',
                contentType: 'application/json',
                type: 'POST',
                data: JSON.stringify(data),

                success: function(message) {
                    notify.removeClass('error');
                    notify.addClass('success');
                    notify.text(message);
                },

                error: function(err) {
                    notify.removeClass('success');
                    notify.addClass('error');
                    notify.text(err.responseText);
                }

            }) 
        }
    });
})