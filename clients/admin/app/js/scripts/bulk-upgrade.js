$(function () {
    var fileInput = $('#upgrade-users'),
        buyerFirstName = $('#buyerFirstName'),
        buyerLastName = $('#buyerLastName'),
        buyerEmail = $('#buyerEmail'),
        years = $('#years'),
        seats = $('#seats'),
        amount = $('#amount'),
        expiration = $('#expiration'),
        sendEmails = $('#sendEmails'),
        notify = $('.notify'),
        file = null;
    
    fileInput.on('change', function (event) {
        file = event.target.files[0];
    });
    
    $('#bulk-upgrade').click(function() {
        if (file === null ||
            !amount.val() ||
            !buyerFirstName.val() ||
            !buyerLastName.val() ||
            !buyerEmail.val() ||
            !seats.val()
            ) {
            notify.removeClass('success');
            notify.addClass('error');
            notify.text('Please fill in the required fields.');
            return;
        }
        
        var expirationDate = new Date();
        expirationDate.setFullYear(expirationDate.getFullYear() + parseInt(years.val()));
        
        var expirationString = expiration.val().trim();
        
        if (expirationString) {
            if (!/^\d{2}\/\d{2}\/\d{4}$/.test(expirationString)) {
                notify.removeClass('success');
                notify.addClass('error');
                notify.text('Invalid expiration date format!');
                return;
            } else {
                expirationDate = new Date(expirationString);
                if (expirationDate.getTime() < Date.now()) {
                    notify.removeClass('success');
                    notify.addClass('error');
                    notify.text('Expiration date is way too soon!');
                    return;
                }
            }
        }
        
        if (!/^\d+\.\d{2}$/.test(amount.val())) {
            notify.removeClass('success');
            notify.addClass('error');
            notify.text('Invalid price per seat. Please follow this format: 20.00');
            return;
        }
        
        notify.removeClass('error');
        notify.text();
        
        var data = new FormData();
        data.append('file', file);
        data.append('amount', amount.val());
        data.append('expiration', expirationDate);
        data.append('buyerFirstName', buyerFirstName.val());
        data.append('buyerLastName', buyerLastName.val());
        data.append('buyerEmail', buyerEmail.val());
        data.append('years', years.val());
        data.append('seats', seats.val());
        data.append('sendEmails', sendEmails.prop('checked'));
        
        $.ajax({
            url: '/bulk-upgrade/license',
            type: 'POST',
            processData: false,
            contentType: false,
            data: data,
            success: function(message) {
                notify.html('');
                notify.removeClass('error');
                notify.addClass('success');
                notify.text(message);
            },
            error: function(err) {
                notify.removeClass('success');
                notify.addClass('error');
                notify.text(err.responseText);
            }
        });
    });
});
