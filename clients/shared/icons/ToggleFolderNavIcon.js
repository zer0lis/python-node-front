import React from 'react';

export default class ToggleFolderNavIcon extends React.Component {
    
    render() {
        let className = 'toggle-folder-nav-icon-default';
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        return React.createElement('svg', {
            className: className,
            viewBox: '0 0 24 24',
            dangerouslySetInnerHTML: {__html: '<path d="M22,18.2v-3c0-0.6-0.4-1-1-1h-8V14v-2h5.7c0.9,0,1.7-0.7,1.7-1.5V3.8c0-0.8-0.8-1.5-1.7-1.5h-3.5L14.5,1c-0.4-0.6-1-1-1.7-1H6.8c-1.1,0-2,0.9-2,2v8.5c0,0.8,0.7,1.5,1.7,1.5H11v2v0.1H3c-0.6,0-1,0.4-1,1v3c-1.2,0.4-2,1.5-2,2.8c0,1.7,1.3,3,3,3s3-1.3,3-3c0-1.3-0.8-2.4-2-2.8v-2h7v2c-1.2,0.4-2,1.5-2,2.8c0,1.7,1.3,3,3,3s3-1.3,3-3c0-1.3-0.8-2.4-2-2.8v-2h7v2c-1.2,0.4-2,1.5-2,2.8c0,1.7,1.3,3,3,3s3-1.3,3-3C24,19.7,23.2,18.6,22,18.2z M3,22c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S3.6,22,3,22z M12,22c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S12.6,22,12,22z M6.8,10l0-8h5.9L14,4.3h4.4V10H6.8z M21,22c-0.6,0-1-0.4-1-1s0.4-1,1-1s1,0.4,1,1S21.6,22,21,22z"/>'}
        });
    }
    
}
