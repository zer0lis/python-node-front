import React from 'react';

export default class ReportsIcon extends React.Component {
    
    render() {
        let color = this.props.color || '#8cb4d2';
        
        return React.createElement('svg', {
            className: this.props.className,
            viewBox: '0 0 20.6 24',
            dangerouslySetInnerHTML: {__html: '<path fill="'+color+'" d="M17.1,2c0.8,0,1.4,0.6,1.4,1.4v17.1c0,0.8-0.6,1.4-1.4,1.4H3.4C2.6,22,2,21.4,2,20.6V3.4C2,2.6,2.6,2,3.4,2H17.1 M17.1,0H3.4C1.5,0,0,1.5,0,3.4v17.1C0,22.5,1.5,24,3.4,24h13.7c1.9,0,3.4-1.5,3.4-3.4V3.4C20.6,1.5,19,0,17.1,0L17.1,0zM7.1,18.4v-4.7c0-0.6-0.4-1-1-1s-1,0.4-1,1v4.7c0,0.6,0.4,1,1,1S7.1,19,7.1,18.4z M15.1,18.4v-8.1c0-0.6-0.4-1-1-1s-1,0.4-1,1v8.1c0,0.6,0.4,1,1,1S15.1,19,15.1,18.4z M11.1,18.4V6.8c0-0.6-0.4-1-1-1s-1,0.4-1,1v11.6c0,0.6,0.4,1,1,1S11.1,19,11.1,18.4z"/>'}
        });
    }
    
}
