import React from 'react';

export default class GenerateCodeIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: 'generate-code',
            width: '16px',
            height: '17px',
            viewBox: '0 0 16 17',
            dangerouslySetInnerHTML: {__html: '<path d="M15.4,0c-0.6,0-1,0.4-1,1v2.9C12.9,2,10.6,0.7,8,0.7c-4.4,0-8,3.6-8,8c0,4.4,3.6,8,8,8c2.6,0,4.9-1.3,6.4-3.2  h-2.8c-1,0.8-2.3,1.2-3.6,1.2c-3.3,0-6-2.7-6-6s2.7-6,6-6c1.7,0,3.3,0.7,4.4,1.9h-1.8c-0.6,0-1,0.4-1,1s0.4,1,1,1h4.8  c0.6,0,1-0.4,1-1V1C16.4,0.4,15.9,0,15.4,0z"/>'}
        });
    }
    
}
