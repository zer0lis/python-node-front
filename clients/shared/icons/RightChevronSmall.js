import React from 'react';

export default class RightChevronSmall extends React.Component {
    
    render() {
        let color = this.props.color;
        
        return React.createElement('svg', {
            className: this.props.className,
            width: '6px',
            height: '10px',
            viewBox: '0 0 6 10',
            dangerouslySetInnerHTML: {__html: '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"><g transform="translate(-299.000000, -295.000000)" stroke-width="2" stroke="'+color+'"><g transform="translate(0.000000, -1.000000)"><g transform="translate(56.000000, 278.000000)"><g ><polyline transform="translate(246.000000, 23.000000) scale(-1, -1) translate(-246.000000, -23.000000) " points="248 27 244 23 248 19"></polyline></g></g></g></g></g>'}
        });
    }
    
}
