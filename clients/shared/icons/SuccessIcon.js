import React from 'react';
import uuid from 'uuid';

export default class SuccessIcon extends React.Component {
    
    render() {
        let pathId1 = uuid.v4(),
            maskId1 = uuid.v4(),
            color = this.props.color;
        
        return React.createElement('svg', {
            className: this.props.className || '',
            width: '21px',
            height: '20px',
            viewBox: '0 0 21 20',
            dangerouslySetInnerHTML: {__html: '<defs><ellipse id="'+pathId1+'" cx="10.01" cy="10" rx="10.01" ry="10"></ellipse><mask id="'+maskId1+'" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="20.02" height="20" fill="white"><use xlink:href="#'+pathId1+'"></use></mask></defs><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-130.000000, -130.000000)" stroke="'+color+'"><g transform="translate(0.000000, 109.000000)"><g transform="translate(130.021000, 21.000000)"><use mask="url(#'+maskId1+')" stroke-width="4" xlink:href="#'+pathId1+'"></use><path d="M14.6813333,7.33333333 L8.67533333,13.8461538" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path><path d="M5.33866667,10 L8.67533333,13.8461538" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path></g></g></g></g>'}
        });
    }
    
}
