import React from 'react';

export default class CancelIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            width: '16px',
            height: '16px',
            viewBox: '0 0 16 16',
            dangerouslySetInnerHTML: {__html: '<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16px" height="16px" viewBox="0 0 16 16" version="1.1"> <!-- Generator: sketchtool 3.8.3 (29802) - http://www.bohemiancoding.com/sketch --> <title>7DE391B8-CBC3-4065-B841-CF367C7DC125</title> <desc>Created with sketchtool.</desc> <defs/> <g id="edit-room-name" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="27-edit-room-name-entered" transform="translate(-277.000000, -276.000000)"> <g id="active-edit" transform="translate(18.000000, 262.000000)"> <g id="save-cancel-active" transform="translate(135.000000, 0.000000)"> <g id="cancel" transform="translate(111.000000, 1.000000)"> <rect id="Rectangle-1742-Copy" x="0" y="0" width="40" height="40"/> <g id="Group-6" transform="translate(10.000000, 10.000000)"> <rect id="Rectangle-1742" x="0.666666667" y="0.666666667" width="20" height="20"/> <path d="M17.6458333,4.51282051 L4.58210831,17.3755652" id="Path-110" stroke="#CCCCCC" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/> <path d="M4.83333333,4.19230769 L17.8970584,17.0550523" id="Path-110-Copy" stroke="#CCCCCC" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/> </g> </g> </g> </g> </g> </g></svg>'}
        });
    }
    
}
