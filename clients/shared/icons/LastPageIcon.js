import React from 'react';

export default class LastPageIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.className,
            viewBox: '0 0 12 18',
            dangerouslySetInnerHTML: {__html: '<path style="fill: #6d9ec4" d="M9.7,8.3C9.9,8.5,10,8.7,10,9S9.9,9.5,9.7,9.7l-8,8c-0.4,0.4-1,0.4-1.4,0s-0.4-1,0-1.4L7.6,9L0.3,1.7c-0.4-0.4-0.4-1,0-1.4c0.4-0.4,1-0.4,1.4,0L9.7,8.3z M11,0c-0.6,0-1,0.4-1,1v8v8c0,0.6,0.4,1,1,1s1-0.4,1-1V1C12,0.4,11.6,0,11,0z"/>'}
        });
    }
    
}
