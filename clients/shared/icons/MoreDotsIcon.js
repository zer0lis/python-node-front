import React from 'react';

export default class MoreDotsIcon extends React.Component {
    
    render() {
        let color = this.props.color || '#8cb4d2';
        
        return React.createElement('svg', {
            className: 'more-dots-icon' + (this.props.className ? (' ' + this.props.className) : ''),
            viewBox: '0 0 24 6.9',
            dangerouslySetInnerHTML: {__html: '<path style="fill: '+color+'" d="M3.4,0C1.5,0,0,1.5,0,3.4s1.5,3.4,3.4,3.4s3.4-1.5,3.4-3.4S5.3,0,3.4,0z M3.4,4.9C2.6,4.9,2,4.2,2,3.4S2.6,2,3.4,2c0.8,0,1.4,0.6,1.4,1.4S4.2,4.9,3.4,4.9z M12,0c-1.9,0-3.4,1.5-3.4,3.4s1.5,3.4,3.4,3.4s3.4-1.5,3.4-3.4S13.9,0,12,0zM12,4.9c-0.8,0-1.4-0.6-1.4-1.4S11.2,2,12,2s1.4,0.6,1.4,1.4S12.8,4.9,12,4.9z M20.6,0c-1.9,0-3.4,1.5-3.4,3.4s1.5,3.4,3.4,3.4S24,5.3,24,3.4S22.5,0,20.6,0z M20.6,4.9c-0.8,0-1.4-0.6-1.4-1.4S19.8,2,20.6,2C21.4,2,22,2.6,22,3.4S21.4,4.9,20.6,4.9z"/>'}
        });
    }
    
}
