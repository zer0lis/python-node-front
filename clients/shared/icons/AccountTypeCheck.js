import React from 'react';

export default class AccountTypeCheck extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: 'account-type-check',
            viewBox: '0 0 36 36',
            dangerouslySetInnerHTML: {__html: '<defs><circle id="account-type-check-path-1" cx="12" cy="12" r="12"></circle><mask id="account-type-check-mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="-6" y="-6" width="36" height="36"><rect x="-6" y="-6" width="36" height="36" fill="white"></rect><use xlink:href="#account-type-check-path-1" fill="black"></use></mask><mask id="account-type-check-mask-3" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="-2" y="-2" width="28" height="28"><rect x="-2" y="-2" width="28" height="28" fill="white"></rect><use xlink:href="#account-type-check-path-1" fill="black"></use></mask></defs><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-563.000000, -459.000000)"><g transform="translate(569.000000, 465.000000)"><g><use fill="#DFEAF3" fill-rule="evenodd" xlink:href="#account-type-check-path-1"></use><use stroke="#DFEAF3" mask="url(#account-type-check-mask-2)" stroke-width="12" xlink:href="#account-type-check-path-1"></use><use stroke="#6D9EC4" mask="url(#account-type-check-mask-3)" stroke-width="4" xlink:href="#account-type-check-path-1"></use></g><g transform="translate(7.000000, 9.000000)" stroke="#6D9EC4" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9.72826087,0.267558528 L3.47826087,6.42140468"></path><path d="M0.434782609,3.47826087 L3.47826087,6.42140468"></path></g></g></g></g>'}
        });
    }
    
}
