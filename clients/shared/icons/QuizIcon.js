import React from 'react';

export default class QuizIcon extends React.Component {
    
    render() {
        let className = 'quiz-icon-default';
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        return React.createElement('svg', {
            className: className,
            viewBox: '0 0 17.1 20',
            dangerouslySetInnerHTML: {__html: '<path d="M13.3,0H3.9C1.7,0,0,1.7,0,3.9v12.3C0,18.3,1.7,20,3.9,20h9.4c2.1,0,3.9-1.7,3.9-3.9V3.9C17.1,1.7,15.4,0,13.3,0z M15.1,16.1c0,1-0.8,1.9-1.9,1.9H3.9c-1,0-1.9-0.8-1.9-1.9V3.9C2,2.8,2.8,2,3.9,2h9.4c1,0,1.9,0.8,1.9,1.9V16.1z M14.2,14.5c0,0.6-0.4,1-1,1h-9c-0.6,0-1-0.4-1-1s0.4-1,1-1h9C13.7,13.5,14.2,13.9,14.2,14.5z M14.2,5.5c0,0.6-0.4,1-1,1h-9c-0.6,0-1-0.4-1-1s0.4-1,1-1h9C13.7,4.5,14.2,4.9,14.2,5.5z M14.2,9.8c0,0.6-0.4,1-1,1h-9c-0.6,0-1-0.4-1-1s0.4-1,1-1h9C13.7,8.8,14.2,9.3,14.2,9.8z"/>'}
        });
    }
    
}
