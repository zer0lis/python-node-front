import React from 'react';

export default class SuccessIconLarge extends React.Component {
    
    render() {
        let color = this.props.color || '#8fc254';
        
        return React.createElement('svg', {
            className: this.props.className,
            viewBox: '0 0 100 100',
            dangerouslySetInnerHTML: {__html: `<path fill="${color}" d="M50,100C22.4,100,0,77.6,0,50S22.4,0,50,0s50,22.4,50,50S77.6,100,50,100z M50,3C24.1,3,3,24.1,3,50s21.1,47,47,47s47-21.1,47-47S75.9,3,50,3z M44,70.6c0.2-0.1,0.3-0.2,0.4-0.3c0,0,0,0,0.1-0.1l30-32.5c0.6-0.6,0.5-1.6-0.1-2.1c-0.6-0.6-1.6-0.5-2.1,0.1L43.4,67L27.8,49c-0.5-0.6-1.5-0.7-2.1-0.2c-0.6,0.5-0.7,1.5-0.2,2.1l16.7,19.2c0,0,0,0.1,0.1,0.1c0.1,0.1,0.3,0.2,0.4,0.3c0.2,0.1,0.4,0.1,0.6,0.2c0,0,0,0,0,0C43.5,70.7,43.8,70.7,44,70.6z"/>`}
        })
    }
    
}
