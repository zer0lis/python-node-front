import React from 'react';

export default class RestoreIcon extends React.Component {
    
    render() {
        let className = 'restore-icon-default';
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        return React.createElement('svg', {
            className: className,
            viewBox: '0 0 22 25',
            dangerouslySetInnerHTML: {__html: '<path d="M10.8,2.5c-3.2,0-6.1,1.3-8.2,3.6V1.5c0-0.6-0.4-1-1-1s-1,0.4-1,1v7c0,0.6,0.4,1,1,1h7c0.6,0,1-0.4,1-1s-0.4-1-1-1H4c1.7-1.9,4.2-3,6.8-3c5.1,0,9.2,4.1,9.2,9.2S15.9,23,10.8,23c-2.9,0-5.7-1.4-7.4-3.8l-1.6,1.2c2.1,2.9,5.5,4.6,9,4.6C17,25,22,20,22,13.8S17,2.5,10.8,2.5z"/>'}
        });
    }
    
}
