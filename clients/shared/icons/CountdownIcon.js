import React from 'react';

export default class CountdownIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.className,
            width: '22px',
            height: '24px',
            viewBox: '0 0 22 24',
            dangerouslySetInnerHTML: {__html: '<defs><path d="M12,24 C17.6806431,24 22.2857143,19.3949289 22.2857143,13.7142857 C22.2857143,8.03364257 17.6806431,3.42857143 12,3.42857143 C6.31935686,3.42857143 1.71428571,8.03364257 1.71428571,13.7142857 C1.71428571,19.3949289 6.31935686,24 12,24 Z" id="countdown-icon-path-1"></path><mask id="countdown-icon-mask-2" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="20.5714286" height="20.5714286" fill="white"><use xlink:href="#countdown-icon-path-1"></use></mask></defs><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-19.000000, -150.000000)"><g transform="translate(0.000000, 133.000000)"><g transform="translate(18.000000, 17.000000)"><rect x="0" y="0" width="24" height="24"></rect><use stroke="#FFFFFF" mask="url(#countdown-icon-mask-2)" stroke-width="4" xlink:href="#countdown-icon-path-1"></use><path d="M12,9.42857143 L12,13.7142857" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" transform="translate(12.000000, 11.571429) scale(1, -1) translate(-12.000000, -11.571429) "></path><path d="M12,1.37142857 L12,3.94285714" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" transform="translate(12.000000, 2.657143) scale(1, -1) translate(-12.000000, -2.657143) "></path><path d="M12,21.4285714 L12,22.2857143" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" transform="translate(12.000000, 21.857143) scale(1, -1) translate(-12.000000, -21.857143) "></path><path d="M12,5.14285714 L12,6" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round" transform="translate(12.000000, 5.571429) scale(1, -1) translate(-12.000000, -5.571429) "></path><path d="M13.7142857,1 L10.2857143,1" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round"></path><path d="M4.28571429,13 L3.42857143,13" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round"></path><path d="M20.5714286,13 L19.7142857,13" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round"></path></g></g></g></g>'}
        });
    }
    
}
