import React from 'react';

export default class MergeIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.className,
            viewBox: '0 0 23.43 14',
            dangerouslySetInnerHTML: {__html: '<path d="M23.2,6.36,18.91,1.22A1,1,0,0,0,17.37,2.5L20.29,6H14.43A7,7,0,0,1,9.85,2.75C8.94,1.4,8,0,4.43,0H1A1,1,0,0,0,1,2H4.43c2.51,0,3,.66,3.76,1.86A8.36,8.36,0,0,0,11.55,7a7.75,7.75,0,0,0-3.19,3.11C7.62,11.29,7.18,12,4.43,12H1a1,1,0,0,0,0,2H4.43C8,14,9,12.83,10.06,11.18,10.77,10,11.51,8.86,14.43,8h5.86l-2.92,3.5a1,1,0,0,0,1.54,1.28L23.2,7.64A1,1,0,0,0,23.2,6.36Z"/>'}
        });
    }
    
}
