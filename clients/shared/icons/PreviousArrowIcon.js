import React from 'react';

export default class PreviousArrowIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.className,
            viewBox: '0 0 10 18',
            dangerouslySetInnerHTML: {__html: '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-392.000000, -1687.000000)" stroke="#6D9EC4" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><g transform="translate(95.000000, 1454.000000)"><g transform="translate(0.000000, 107.000000)"><g transform="translate(298.000000, 127.000000)"><path d="M8.00000002,16 L1.76250978e-08,8 L8.00000008,0"></path></g></g></g></g></g>'}
        });
    }
    
}
