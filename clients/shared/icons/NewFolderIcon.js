import React from 'react';

export default class NewFolderIcon extends React.Component {
    
    render() {
        let className = 'new-folder-icon-default';
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        return React.createElement('svg', {
            className: className,
            viewBox: '0 0 24 23',
            dangerouslySetInnerHTML: {__html: '<path d="M7.7,18.2H2.3c-1.2,0-2.2-1-2.2-2.3V3.2c0-1.6,1.3-3,3-3h6.3c1.6,0,2.9,1.3,3,2.9c0.1,0.4,0.4,0.6,0.8,0.6h5.7C20,3.7,21,4.7,21,6l0,1.5l-2-0.8V6c0-0.2-0.1-0.3-0.2-0.3h-5.7c-1.3,0-2.5-0.9-2.8-2.3l0-0.2c0-0.5-0.5-1-1-1H3c-0.5,0-1,0.4-1,1v12.7c0,0.2,0.1,0.3,0.2,0.3h5.4V18.2z M24,16.2c0-3.9-3.1-7-7-7s-7,3.1-7,7s3.1,7,7,7S24,20.1,24,16.2z M22,16.2c0,2.8-2.2,5-5,5s-5-2.2-5-5s2.2-5,5-5S22,13.5,22,16.2z M18,18.2v-1.1l1,0c0,0,0,0,0,0c0.5,0,1-0.4,1-1c0-0.6-0.4-1-1-1l-1,0v-0.9c0-0.6-0.4-1-1-1s-1,0.4-1,1v0.9l-1,0c0,0,0,0,0,0c-0.5,0-1,0.4-1,1c0,0.6,0.4,1,1,1l1,0v1.1c0,0.6,0.4,1,1,1S18,18.8,18,18.2z M19,6.8 M21,6"/>'}
        });
    }
    
}
