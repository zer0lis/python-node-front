import React from 'react';

export default class LoggedOutIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.svgClass,
            viewBox: '0 0 24 24',
            dangerouslySetInnerHTML: {__html: '<path class="'+this.props.pathClass+'" d="M12,0C5.4,0,0,5.4,0,12c0,6.6,5.4,12,12,12c6.6,0,12-5.4,12-12C24,5.4,18.6,0,12,0z M21.9,12c0,2.3-0.8,4.5-2.2,6.2L6.2,3.9C7.9,2.8,9.9,2.1,12,2.1C17.5,2.1,21.9,6.5,21.9,12z M2.1,12c0-2.6,1-4.9,2.6-6.7l13.6,14.3c-1.7,1.4-3.9,2.3-6.3,2.3C6.5,21.9,2.1,17.5,2.1,12z"/>'}
        });
    }
    
}
