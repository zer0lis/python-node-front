import React from 'react';

export default class TrashIconLarge extends React.Component {
    
    render() {
        let className = 'trash-icon-large-default',
            color = this.props.color || '#cccccc';
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        return React.createElement('svg', {
            className: className,
            viewBox: '0 0 76 83',
            dangerouslySetInnerHTML: {__html: `<path fill="${color}" d="M38.5,66.4c-0.8,0-1.5-0.7-1.5-1.5v-35c0-0.8,0.7-1.5,1.5-1.5c0.8,0,1.5,0.7,1.5,1.5v35C40,65.7,39.3,66.4,38.5,66.4z M53,64.9v-35c0-0.8-0.7-1.5-1.5-1.5S50,29.1,50,29.9v35c0,0.8,0.7,1.5,1.5,1.5S53,65.7,53,64.9zM27,64.9v-35c0-0.8-0.7-1.5-1.5-1.5S24,29.1,24,29.9v35c0,0.8,0.7,1.5,1.5,1.5S27,65.7,27,64.9z M75.2,15.5c0,0.8-0.7,1.5-1.5,1.5h-6.6v56.6c0,4.7-3.6,8.5-8,8.5H16.9c-4.4,0-8-3.8-8-8.5V17H2.3c-0.8,0-1.5-0.7-1.5-1.5S1.5,14,2.3,14h19.8V8.5c0-4.3,3.6-7.8,7.9-7.8h15.9c4.4,0,7.9,3.5,7.9,7.8V14h19.8C74.5,14,75.2,14.7,75.2,15.5z M25.1,14h25.7V8.5c0-2.7-2.2-4.8-4.9-4.8H30.1c-2.7,0-4.9,2.2-4.9,4.8V14z M64.1,17H11.9v56.6c0,3,2.2,5.5,5,5.5h42.3c2.7,0,5-2.5,5-5.5V17z"/>`}
        });
    }
    
}
