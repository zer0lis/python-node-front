import React from 'react';
import uuid from 'uuid';

export default class TeacherIcon extends React.Component {
    
    render() {
        let pathId1 = uuid.v4(),
            pathId2 = uuid.v4(),
            maskId1 = uuid.v4(),
            maskId2 = uuid.v4(),
            color = this.props.color;
        
        return React.createElement('svg', {
            className: this.props.className,
            width: '28px',
            height: '28px',
            viewBox: '0 0 28 28',
            dangerouslySetInnerHTML: {__html: '<defs><path id="'+pathId1+'" d="M15,29 C22.7319865,29 29,22.7319865 29,15 C29,7.2680135 22.7319865,1 15,1 C7.2680135,1 1,7.2680135 1,15 C1,22.7319865 7.2680135,29 15,29 Z"></path><mask id="'+maskId1+'" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="0" y="0" width="28" height="28" fill="white"><use xlink:href="#'+pathId1+'"></use></mask><path id="'+pathId2+'" d="M16,16 L16,13.1954887 C16,12.7443586 15.8037924,12.4060162 15.4113712,12.1804511 L11.5585284,10.2631579 C11.5585284,10.2631579 9.89966583,10 9.89966555,9.41729323 C9.89966527,8.83458647 11.219619,8.27068139 11.6120401,7.33082707 C11.933112,6.50375526 12.0936455,5.73308628 12.0936455,5.01879699 C12.0936455,4.64285526 12.0222973,4.19173195 11.8795987,3.66541353 C11.6298761,2.57518252 11.1482756,1.69173271 10.4347826,1.01503759 C9.72128963,0.338342481 8.90078502,0 7.97324415,0 C7.08137793,0 6.27871037,0.328944079 5.56521739,0.986842105 C4.85172441,1.64474013 4.35228682,2.49999474 4.06688963,3.55263158 C3.95986569,4.00376165 3.90635452,4.49247857 3.90635452,5.01879699 C3.90635452,5.95865132 4.08472508,6.78571071 4.44147157,7.5 C4.86956736,8.36466598 6.07894874,8.93281081 6.10033445,9.47368421 C6.12172015,10.0145576 4.54849498,10.2067669 4.54849498,10.2067669 L0.535117057,12.1804511 C0.178370569,12.368422 0,12.7067645 0,13.1954887 L0,16"></path><mask id="'+maskId2+'" maskContentUnits="userSpaceOnUse" maskUnits="objectBoundingBox" x="-2" y="-2" width="20" height="20"><rect x="-2" y="-2" width="20" height="20" fill="white"></rect><use xlink:href="#'+pathId2+'" fill="black"></use></mask></defs><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-72.000000, -15.000000)"><g transform="translate(0.000000, -1.000000)"><g transform="translate(57.000000, 1.000000)"><g transform="translate(14.000000, 14.000000)"><rect x="0" y="0" width="28" height="28"></rect><use stroke="'+color+'" mask="url(#'+maskId1+')" stroke-width="4" xlink:href="#'+pathId1+'"></use><g stroke="'+color+'" transform="translate(7.000000, 8.000000)" stroke-linejoin="round"><g><use mask="url(#'+maskId2+')" stroke-width="4" xlink:href="#'+pathId2+'"></use><polyline stroke-width="2" stroke-linecap="round" points="3.76470588 10.4210526 8.00000022 15.0000002 12.2352941 10.4210526"></polyline></g></g></g></g></g></g></g>'}
        });
    }
    
}
