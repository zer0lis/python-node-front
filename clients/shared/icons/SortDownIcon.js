import React from 'react';

export default class SortDownIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.className,
            viewBox: '0 0 9.6 11.4',
            dangerouslySetInnerHTML: {__html: '<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M8.6,7.3l-3.8,3.1L1,7.3 M4.8,1v8.9"/>'}
        });
    }
    
}
