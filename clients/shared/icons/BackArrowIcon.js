import React from 'react';

export default class BackArrowIcon extends React.Component {
    
    render() {
        let className = 'back-arrow-icon-default';
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        return React.createElement('svg', {
            className: className,
            viewBox: '0 0 22 19.4',
            dangerouslySetInnerHTML: {__html: '<path d="M21,8.6H3.1l5.5-7C9,1.2,8.9,0.6,8.5,0.2C8-0.1,7.4-0.1,7.1,0.4L0.2,9.1c-0.3,0.4-0.3,0.9,0,1.2L7.1,19c0.2,0.3,0.5,0.4,0.8,0.4c0.2,0,0.4-0.1,0.6-0.2c0.4-0.3,0.5-1,0.2-1.4L3,10.6h18c0.6,0,1-0.4,1-1S21.5,8.6,21,8.6z"/>'}
        })
    }
    
}
