import React from 'react';

export default class OkIconCircle extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: 'ok-icon-circle',
            width: '24px',
            height: '24px',
            viewBox: '0 0 24 24',
            dangerouslySetInnerHTML: {__html: '<path d="M10,0C4.5,0,0,4.4,0,9.8s4.5,9.8,10,9.8s10-4.4,10-9.8S15.5,0,10,0z M10,17.6c-4.4,0-8-3.5-8-7.8S5.6,2,10,2s8,3.5,8,7.8  S14.4,17.6,10,17.6z M15.4,7.9l-6,6.4c-0.4,0.4-1.1,0.4-1.5,0l-3.3-3.8C4.2,10,4.3,9.4,4.7,9c0.4-0.4,1-0.3,1.4,0.1l2.6,2.9l5.2-5.6  c0.4-0.4,1-0.4,1.4,0C15.8,6.8,15.8,7.5,15.4,7.9z"/>'}
        });
    }
    
}
