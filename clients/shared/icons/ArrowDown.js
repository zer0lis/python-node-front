import React from 'react';

export default class ArrowDown extends React.Component {
    
    render() {
        let color = this.props.color || '#ffffff';
        
        return React.createElement('svg', {
            className: this.props.className,
            width: '10px',
            height: '6px',
            viewBox: '0 0 10 6',
            dangerouslySetInnerHTML: {__html: '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"> <g id="add-students-btn-blue" transform="translate(-374.000000, -18.000000)" stroke="'+color+'" stroke-width="2"> <g id="add-students" transform="translate(252.000000, 0.000000)"> <polyline id="Path-109-Copy" transform="translate(126.666667, 20.833333) scale(-1, -1) rotate(-270.000000) translate(-126.666667, -20.833333) " points="128.5 24.5 124.833333 20.8333333 128.5 17.1666667"/> </g> </g> </g>'}
        });
    }
    
}
