import React from 'react';

export default class SortUpIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.className,
            viewBox: '0 0 9.6 11.4',
            dangerouslySetInnerHTML: {__html: '<path fill="none" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" d="M1,4.1L4.8,1l3.8,3.1 M4.8,10.4V1.5"/>'}
        });
    }
    
}
