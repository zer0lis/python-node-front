import React from 'react';

export default class RightChevronThin extends React.Component {
    
    render() {
        let className = 'right-chevron-thin-default';
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        return React.createElement('svg', {
            className: className,
            viewBox: '0 0 6.3 12.3',
            dangerouslySetInnerHTML: {__html: '<path d="M1,12.3c-0.2,0-0.5-0.1-0.6-0.2c-0.4-0.4-0.5-1-0.1-1.4L4,6.1L0.2,1.6c-0.4-0.4-0.3-1.1,0.1-1.4c0.4-0.4,1.1-0.3,1.4,0.1l4.3,5.1c0.3,0.4,0.3,0.9,0,1.3l-4.3,5.1C1.6,12.2,1.3,12.3,1,12.3z"/>'}
        });
    }
    
}
