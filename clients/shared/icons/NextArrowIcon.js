import React from 'react';

export default class NextArrowIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.className,
            viewBox: '14 0 10 18',
            dangerouslySetInnerHTML: {__html: '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-392.000000, -1687.000000)" stroke="#6D9EC4" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><g transform="translate(95.000000, 1454.000000)"><g transform="translate(0.000000, 107.000000)"><g transform="translate(298.000000, 127.000000)"><path d="M22,16 L14,8 L22.0000001,0" transform="translate(18.000000, 8.000000) scale(-1, 1) translate(-18.000000, -8.000000)"></path></g></g></g></g></g>'}
        });
    }
    
}
