import React from 'react';

export default class AddIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.className,
            width: '24px',
            height: '25px',
            viewBox: '0 0 24 25',
            dangerouslySetInnerHTML: {__html: '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"> <g id="add-more" transform="translate(-4.000000, -7.000000)"> <g id="close-copy-8" transform="translate(15.707107, 19.707107) rotate(-315.000000) translate(-15.707107, -19.707107) translate(4.707107, 8.707107)"> <g id="Group-2" transform="translate(0.000000, 0.000000)"> <g id="Group-6" transform="translate(0.707107, 0.292893)"> <path d="M18.4852817,2.92893219 L2.92893219,18.485281" id="Path-110" stroke="#8CB4D2" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/> <path d="M2.29289322,2.93787601 L17.9693632,18.3731696" id="Path-110-Copy" stroke="#8CB4D2" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/> <rect id="Rectangle-1742" x="0.828427125" y="-1.63424829e-13" width="19.5857864" height="20.5857864"/> </g> </g> </g> </g> </g>'},
            onClick: (event) => {
                if (this.props.onClick) {
                    this.props.onClick(event);
                }
            }
        });
    }
    
}
