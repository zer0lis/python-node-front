import React from 'react';

export default class DownChevronLarge extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.className,
            width: '12px',
            height: '8px',
            viewBox: '0 0 12 8',
            dangerouslySetInnerHTML: {__html: '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"><g transform="translate(-198.000000, -30.000000)" stroke="#FFFFFF" stroke-width="2"><g><g><g><g transform="translate(116.000000, 23.000000)"><polyline transform="translate(88.000000, 10.556180) scale(-1, 1) rotate(-90.000000) translate(-88.000000, -10.556180) " points="90.5561798 15.5561798 85.4438202 10.5561798 90.5561798 5.55617979"></polyline></g></g></g></g></g></g>'}
        })
    }
    
}
