import React from 'react';
import uuid from 'uuid';
import Constants from 'Constants';

export default class ProBadgeIconSmall extends React.Component {

    render() {
        let pathId1 = uuid.v4(),
            maskId2 = uuid.v4(),
            pathId3 = uuid.v4(),
            pathClass = this.props.pathClass || '',
            fill = this.props.fill ? this.props.fill : Constants.PRO_BADGE_COLOR;
        
        return React.createElement('span', {
            className: this.props.className,
            dangerouslySetInnerHTML: {__html: '<svg width="26" height="25"><defs><path id="'+pathId1+'" d="M9.988.992L7.804 7.664H.737c-.32 0-.602.205-.7.506-.1.302.01.632.267.818l5.717 4.124-2.182 6.672c-.1.3.01.632.267.818.13.093.282.14.434.14.15 0 .303-.047.432-.14l5.717-4.123 5.717 4.122c.13.093.28.14.433.14.152 0 .304-.047.433-.14.26-.186.366-.517.267-.818l-2.182-6.672 5.716-4.124c.258-.186.367-.516.268-.818-.098-.3-.382-.506-.7-.506h-7.068L11.39.992c-.098-.3-.382-.506-.7-.506-.32 0-.603.205-.702.506z"/><mask id="'+maskId2+'" width="26" height="25" x="-1" y="-1"><path fill="#fff" d="M-1-.514h23.378v22.256H-1z"/><use xlink:href="#'+pathId1+'"/></mask><path id="'+pathId3+'" d="M8.873 9.022c-.46.37-.678.79-.678 1.29 0 .473.19.842.596 1.13.38.264.978.5 1.764.658.542.13.92.263 1.11.42.19.16.3.37.3.606 0 .264-.11.474-.353.632-.243.157-.57.236-.975.236-.244 0-.46-.026-.704-.08-.217-.05-.406-.156-.542-.262 0 0-.217-.79-.217-.815-.026-.026-.893-.026-.893 0v1.395c.325.21.677.368 1.057.473.406.105.84.157 1.328.157.76 0 1.356-.157 1.817-.5.46-.315.703-.762.703-1.315 0-.5-.19-.868-.596-1.157-.407-.29-1.003-.527-1.816-.685-.542-.105-.894-.236-1.084-.368-.19-.157-.27-.34-.27-.58 0-.235.108-.445.297-.63.19-.184.515-.262.922-.262.27 0 .515.026.732.104.217.078.407.184.542.315 0 0 .19.71.19.737.025.026.894.052.894 0 0-.053-.055-1.37-.082-1.342-.27-.21-.596-.368-.976-.5-.407-.13-.84-.185-1.328-.185-.705 0-1.274.185-1.735.527z"/></defs><g fill="none" fill-rule="evenodd" transform="translate(1 1)"><use fill="' + fill + '" xlink:href="#'+pathId1+'"/><use class="'+ pathClass +'" stroke-width="2" mask="url(#'+maskId2+')" xlink:href="#'+pathId1+'"/><use fill="#FFF" xlink:href="#'+pathId3+'"/></g></svg>'}
        });
    }
    
}
