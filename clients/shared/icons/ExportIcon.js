import React from 'react';

export default class ExportIcon extends React.Component {
    
    render() {
        let color = this.props.color || '#8cb4d2';
        
        return React.createElement('svg', {
            className: this.props.className,
            viewBox: '0 0 20 20.6',
            dangerouslySetInnerHTML: {__html: '<path fill="'+color+'" d="M20,9.8v8.5c0,1.3-1.1,2.3-2.5,2.3h-15c-1.4,0-2.5-1-2.5-2.3V9.8c0-1.3,1.1-2.3,2.5-2.3h5.2v2H2.5C2.2,9.5,2,9.7,2,9.8v8.5c0,0.1,0.2,0.3,0.5,0.3h15c0.3,0,0.5-0.2,0.5-0.3V9.8c0-0.1-0.2-0.3-0.5-0.3h-5.2v-2h5.2C18.9,7.5,20,8.5,20,9.8z M7.1,4.9c0.3,0,0.5-0.1,0.7-0.3L9,3.4v9.2c0,0.6,0.4,1,1,1s1-0.4,1-1V3.3l1.3,1.3c0.2,0.2,0.4,0.3,0.7,0.3c0.3,0,0.5-0.1,0.7-0.3c0.4-0.4,0.4-1,0-1.4l-2.9-2.9C10.6,0.1,10.3,0,10,0C9.8,0,9.5,0.1,9.3,0.3L6.4,3.2C6,3.6,6,4.2,6.4,4.6C6.6,4.8,6.8,4.9,7.1,4.9z"/>'}
        });
    }
    
}
