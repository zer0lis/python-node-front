/*
    Plus Icon - An SVG icon of a plus symbol (+)
    
        innerClass {string} Required class name for the inner <g> element that defines this icon's color. Must set the CSS "stroke" property.
        outerClass {string} Required class name for the outer <svg> element that defines this icon's width, height, and position.
 */

import React from 'react';

export default class PlusIcon extends React.Component {
    
    render() {
        let innerClass = this.props.innerClass;
        
        return React.createElement('svg', {
            className: this.props.outerClass,
            viewBox: '0 0 16 16',
            dangerouslySetInnerHTML: {__html: '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round" stroke-linejoin="round"><g class="'+innerClass+'" transform="translate(-991.000000, -182.000000)" stroke-width="2"><g transform="translate(966.000000, 168.000000)"><g transform="translate(33.000000, 22.000000) rotate(-315.000000) translate(-33.000000, -22.000000) translate(28.000000, 17.000000)"><path d="M8.984375,0.384615385 L4.42312853e-13,9.23076923"></path><path d="M4.42312853e-13,0.769230769 L8.984375,9.61538462"></path></g></g></g></g>'}
        });
    }
    
}
