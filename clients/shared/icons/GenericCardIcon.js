import React from 'react';

export default class GenericCardIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.className,
            viewBox: '0 0 28 20',
            dangerouslySetInnerHTML: {__html: '<path fill="#cccccc" d="M23,0H5C2.2,0,0,2.2,0,5v10c0,2.8,2.2,5,5,5h18c2.8,0,5-2.2,5-5V5C28,2.2,25.8,0,23,0z M5,2h18c1.5,0,2.7,1.1,3,2.5H2C2.3,3.1,3.5,2,5,2z M26,15c0,1.7-1.3,3-3,3H5c-1.7,0-3-1.3-3-3V6.5h24V15z M13,10.5H4v-2h9V10.5z M10,14.5H4v-2h6V14.5z M24,10.5h-4v-2h4V10.5z"/>'}
        });
    }
    
}
