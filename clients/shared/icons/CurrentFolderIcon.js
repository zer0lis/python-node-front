import React from 'react';

export default class CurrentFolderIcon extends React.Component {
    
    render() {
        let className = 'folder-icon-default';
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        return React.createElement('svg', {
            className: className,
            viewBox: '0 0 20 16.7',
            dangerouslySetInnerHTML: {__html: '<path d="M2.5,0h7.6l0,0c0.9,0,1.6,0.7,1.6,1.6v0.9l0,0c0,0.4,0.3,0.7,0.7,0.7h5.5C19,3.2,20,4.2,20,5.3v9.2c0,1.2-1,2.1-2.1,2.1H2.1C1,16.7,0,15.7,0,14.5v-12l0,0C0,1.1,1.1,0,2.5,0z"/>'}
        });
    }
    
}
