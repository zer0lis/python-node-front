import React from 'react';

export default class TrashIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className : this.props.className,
            viewBox: '0 0 21.3 23',
            dangerouslySetInnerHTML: {__html: '<path d="M9.6,17.6V8.7c0-0.6,0.4-1,1-1s1,0.4,1,1v8.8c0,0.6-0.4,1-1,1S9.6,18.1,9.6,17.6z M13.6,18.6c0.6,0,1-0.4,1-1V8.7c0-0.6-0.4-1-1-1s-1,0.4-1,1v8.8C12.6,18.1,13.1,18.6,13.6,18.6z M7.6,18.6c0.6,0,1-0.4,1-1V8.7c0-0.6-0.4-1-1-1s-1,0.4-1,1v8.8C6.6,18.1,7.1,18.6,7.6,18.6z M21.3,5c0,0.6-0.4,1-1,1h-0.8v13.9c0,1.7-1.4,3.1-3.2,3.1H4.9c-1.7,0-3.1-1.4-3.1-3.1V6H1C0.4,6,0,5.6,0,5s0.4-1,1-1h4.4V3.1c0-1.7,1.3-3.1,3-3.1v0h4.4c1.7,0,3.1,1.4,3.1,3.1V4h4.4C20.8,4,21.3,4.4,21.3,5z M7.4,4h6.6V3.1c0-0.6-0.5-1.1-1.1-1.1H8.5C7.9,2,7.4,2.5,7.4,3.1V4z M17.5,6H3.8v13.9c0,0.6,0.5,1.1,1.1,1.1h11.4c0.6,0,1.2-0.5,1.2-1.1V6z"/>'}
        });
    }
    
}
