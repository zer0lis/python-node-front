import React from 'react';

export default class FolderIcon extends React.Component {
    
    render() {
        let className = 'folder-icon-default';
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        return React.createElement('svg', {
            className: className,
            viewBox: '0 0 24 20',
            dangerouslySetInnerHTML: {__html: '<path d="M21.4,20H2.6C1.2,20,0,18.9,0,17.5V3c0-1.7,1.3-3,3-3h8c1.7,0,3,1.3,3,3c0,0.5,0.4,0.9,0.9,0.9h6.6C22.8,3.9,24,5,24,6.4v11.1C24,18.9,22.8,20,21.4,20z M3,2C2.5,2,2,2.4,2,3v14.5C2,17.8,2.2,18,2.6,18h18.9c0.3,0,0.6-0.3,0.6-0.5V6.4c0-0.3-0.3-0.5-0.6-0.5h-6.6C13.3,5.9,12,4.6,12,3c0-0.5-0.5-1-1-1H3z"/>'}
        })
    }
    
}
