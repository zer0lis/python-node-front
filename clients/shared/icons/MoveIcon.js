import React from 'react';

export default class MoveIcon extends React.Component {
    
    render() {
        let className = 'move-icon-default';
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        return React.createElement('svg', {
            className: className,
            viewBox: '0 0 24 22',
            dangerouslySetInnerHTML: {__html: '<path d="M20.7,15.9H13c-0.5,0-1,0.4-1,1c0,0.6,0.4,1,1,1h7.9c-0.1,0.1-0.2,0.3-0.4,0.4l-1.8,1.9c-0.4,0.4-0.4,1,0,1.4c0.4,0.4,1,0.4,1.4,0l1.8-1.9l1.8-1.9c0.3-0.4,0.4-0.9,0-1.3L22,14.4l-1.8-2.1c-0.4-0.4-1-0.5-1.4-0.1c-0.4,0.4-0.5,1-0.1,1.4M8.6,18H2.3C1,18,0,16.9,0,15.7V3c0-1.6,1.3-3,3-3h6.3c1.6,0,2.9,1.3,3,2.9c0.1,0.4,0.4,0.6,0.8,0.6h5.7c1.2,0,2.2,1,2.2,2.3l0,2.5l-2-0.8V5.7c0-0.2-0.1-0.3-0.2-0.3h-5.7c-1.3,0-2.5-0.9-2.8-2.3l0-0.2c0-0.5-0.5-1-1-1H3C2.5,2,2,2.4,2,3v12.7C2,15.8,2.1,16,2.3,16h6.4V18z"/>'}
        });
    }
    
}
