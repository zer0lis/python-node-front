import React from 'react';

export default class CheckIcon extends React.Component {
    
    render() {
        let color = this.props.color || '#8cb4d2';
        
        return React.createElement('svg', {
            className: this.props.className,
            viewBox: '0 0 24 17',
            dangerouslySetInnerHTML: {__html: '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-443.000000, -1688.000000)" stroke="'+color+'" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><g transform="translate(95.000000, 1454.000000)"><g transform="translate(0.000000, 107.000000)"><g transform="translate(348.000000, 128.000000)"><path d="M22.375,0.615384615 L8,14.7692308"></path><path d="M1,8.00000001 L8,14.7692308"></path></g></g></g></g></g>'}
        });
    }
    
}
