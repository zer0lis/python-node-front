import React from 'react';

export default class DownloadIcon extends React.Component {
    
    render() {
        let className = this.props.className || 'download-icon-default';
        
        return React.createElement('svg', {
            className : className,
            viewBox: '0 0 14 20',
            dangerouslySetInnerHTML: {__html: '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-107.000000, -9.000000)"><g transform="translate(0.000000, 7.000000)"><g transform="translate(102.000000, 0.000000)"><g><g transform="translate(12.000000, 12.000000) rotate(-270.000000) translate(-12.000000, -12.000000) "><rect x="-1.77635684e-15" y="0" width="24" height="24"></rect><g transform="translate(15.357143, 11.642857) scale(1, -1) translate(-15.357143, -11.642857) translate(12.857143, 6.142857)" stroke="#8CB4D2" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline transform="translate(2.142857, 5.142857) scale(-1, 1) translate(-2.142857, -5.142857) " points="4.28571435 10.2857143 6.61971189e-08 5.14285714 4.28571435 0"></polyline></g><path d="M3.42857143,12 L15.890061,12" stroke="#8CB4D2" stroke-width="2" stroke-linecap="round"></path><path d="M21,6.84822474 L21,17.1505076" stroke="#8CB4D2" stroke-width="2" stroke-linecap="round"></path></g></g></g></g></g></g>'}
        });
    }
    
}
