import React from 'react';

export default class RenewProIcon extends React.Component {
    
    render() {
        let color = this.props.color || '#cccccc',
            className = this.props.className || 'default-renew-pro-icon';
        
        return React.createElement('svg', {
            className: className,
            viewBox: '0 0 98.6 92.7',
            dangerouslySetInnerHTML: {__html: `<path fill="${color}" d="M46.3,0C20.8,0,0,20.8,0,46.3s20.8,46.3,46.3,46.3c8,0,15.6-2,22.1-5.6c-0.6-0.8-1.1-1.6-1.6-2.5c-6.1,3.3-13.1,5.2-20.5,5.2c-10,0-19.2-3.4-26.5-9.1l0-13.8c0-1.6,0.6-2.7,1.8-3.4l9.6-4.7l14,15.1c0.3,0.3,0.7,0.5,1.1,0.5s0.8-0.2,1.1-0.5l13.9-15.1l8.3,4.1c0.7-0.8,1.5-1.5,2.3-2.2l-12.8-6.4c-0.1-0.1-0.3-0.1-0.4-0.1l-0.2,0c-0.1,0-0.4-0.1-0.7-0.1c-0.4-0.1-0.8-0.2-1.1-0.3c0.2-0.2,0.5-0.4,0.7-0.7l0.1-0.1c1.8-1.6,2.8-2.9,3.4-4.4c1.2-3.1,1.8-6,1.8-8.8c0-1.5-0.3-3.2-0.8-5.2c-0.9-4.1-2.8-7.6-5.6-10.2c-2.9-2.8-6.4-4.2-10.2-4.2c-3.7,0-7.1,1.4-10,4.1c-2.8,2.6-4.7,5.9-5.8,10c-0.4,1.8-0.6,3.6-0.6,5.6c0,3.6,0.7,6.8,2.1,9.5c0.6,1.3,1.6,2.4,3.2,3.9c0.2,0.2,0.4,0.4,0.6,0.6c-0.2,0-0.3,0.1-0.5,0.1c-0.3,0.1-0.5,0.1-0.6,0.1l-0.3,0c-0.2,0-0.3,0.1-0.5,0.1l-13.6,6.7c-2.2,1.2-3.4,3.3-3.4,6l0,11.2C8.3,70.2,3,58.9,3,46.3C3,22.4,22.4,3,46.3,3s43.3,19.4,43.3,43.3c0,3.8-0.5,7.4-1.4,10.9c1,0.2,1.9,0.6,2.8,1c1-3.8,1.6-7.8,1.6-11.9C92.7,20.8,71.9,0,46.3,0z M34.9,57L34.9,57c0.2,0,0.4-0.1,0.8-0.1c0.6-0.1,1.2-0.3,1.8-0.4c0.8-0.2,1.4-0.5,1.8-0.8c0.3-0.2,0.6-0.6,0.6-1s0-0.8-0.3-1.1c-0.3-0.4-0.7-0.8-2.6-2.5c-1.3-1.2-2-2.1-2.5-3c-1.2-2.4-1.8-5.1-1.8-8.2c0-1.8,0.2-3.4,0.5-4.9c0.9-3.5,2.6-6.3,5-8.5c2.4-2.2,5-3.3,8-3.3c3.1,0,5.8,1.1,8.1,3.4s4,5.2,4.8,8.8c0.5,1.7,0.7,3.2,0.7,4.5c0,2.4-0.5,4.9-1.6,7.7c-0.4,1.1-1.2,2-2.7,3.3l-0.1,0.1c-2.4,2.1-2.5,2.3-2.7,2.8c-0.2,0.6-0.1,1.3,0.4,1.7c0.1,0.1,0.2,0.2,0.3,0.2c0.5,0.3,1.1,0.6,1.9,0.9c0.6,0.2,1.2,0.4,1.9,0.5c0.4,0.1,0.7,0.1,0.9,0.2l0.5,0.2L46.3,70.6L34.1,57.4L34.9,57z M97.3,59.4v9.3c0,0.7-0.6,1.3-1.3,1.3h-9.3c-0.7,0-1.3-0.6-1.3-1.3s0.6-1.3,1.3-1.3h6.1c-2.2-2.5-5.5-4-9-4c-6.8,0-12.3,5.5-12.3,12.3S76.9,88,83.6,88c4.4,0,8.3-2.4,10.5-5.9l2.1,1.7c-2.7,4.1-7.3,6.9-12.6,6.9c-8.2,0-15-6.7-15-15s6.7-15,15-15c4.3,0,8.2,1.9,11,4.8v-6.1c0-0.7,0.6-1.3,1.3-1.3S97.3,58.7,97.3,59.4z"/>`}
        });
    }
    
}
