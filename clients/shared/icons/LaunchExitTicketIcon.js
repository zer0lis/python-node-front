import React from 'react';

export default class LaunchExitTicketIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.svgClass,
            viewBox: '0 0 66 66',
            dangerouslySetInnerHTML: {__html: '<path class="'+this.props.pathClass+'" d="M36,63.6H8.1c-3.2,0-5.8-2.9-5.8-6.4V8.7c0-3.5,2.6-6.4,5.8-6.4H36c3.2,0,5.8,2.9,5.8,6.4v17c0,0.8-0.7,1.5-1.5,1.5s-1.5-0.7-1.5-1.5v-17c0-1.9-1.2-3.4-2.8-3.4H8.1c-1.5,0-2.8,1.5-2.8,3.4v48.5c0,1.9,1.2,3.4,2.8,3.4H36c1.5,0,2.8-1.5,2.8-3.4v-17c0-0.8,0.7-1.5,1.5-1.5s1.5,0.7,1.5,1.5v17C41.8,60.8,39.2,63.6,36,63.6z M29.2,31.5c-0.8,0-1.5,0.7-1.5,1.5s0.7,1.5,1.5,1.5h31.6l-7.3,7.3c-0.6,0.6-0.6,1.5,0,2.1c0.6,0.6,1.5,0.6,2.1,0l9.5-9.5c0.3-0.3,0.5-0.7,0.4-1.2c0-0.4-0.1-0.8-0.4-1.2l-9.5-9.5c-0.6-0.6-1.5-0.6-2.1,0c-0.6,0.6-0.6,1.5,0,2.1l6.8,6.8H29.2z"/>'}
        });
    }
    
}
