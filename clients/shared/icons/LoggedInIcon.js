import React from 'react';

export default class LoggedInIcon extends React.Component {
    
    render() {
        let color = this.props.color || '#8FC254';
        
        return React.createElement('svg', {
            className: this.props.svgClass,
            viewBox: '0 0 20 20',
            dangerouslySetInnerHTML: {__html: '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-941.000000, -271.000000)" fill="'+color+'"><g transform="translate(927.000000, 120.000000)"><g transform="translate(1.000000, 53.000000)"><g transform="translate(0.000000, 86.000000)"><g transform="translate(13.000000, 12.000000)"><g><g><g><g><g><g><g><path d="M10,18 L10,18 C14.418278,18 18,14.418278 18,10 C18,5.581722 14.418278,2 10,2 C5.581722,2 2,5.581722 2,10 C2,14.418278 5.581722,18 10,18 L10,18 L10,18 Z M10,20 L10,20 C4.4771525,20 0,15.5228475 0,10 C0,4.4771525 4.4771525,0 10,0 C15.5228475,0 20,4.4771525 20,10 C20,15.5228475 15.5228475,20 10,20 L10,20 L10,20 Z"></path><path d="M13.6967398,7.39017814 C14.1025707,7.0155854 14.735229,7.04090894 15.1098218,7.44673985 C15.4844145,7.85257075 15.459091,8.48522912 15.0532601,8.85982186 C15.0532601,8.85982186 9.90959493,13.494755 9.89057234,13.5156797 C9.51906391,13.9243359 8.88661547,13.9544502 8.47795922,13.5829417 L5.57732448,10.9459816 C5.16866823,10.5744731 5.13855397,9.94202469 5.5100624,9.53336844 C5.88157083,9.12471219 6.51401927,9.09459793 6.92267552,9.46610636 L9.20238635,11.5385861 L13.6967398,7.39017814 Z"></path></g></g></g></g></g></g></g></g></g></g></g></g></g>'}
        });
    }
    
}
