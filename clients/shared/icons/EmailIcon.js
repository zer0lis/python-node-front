import React from 'react';

export default class EmailIcon extends React.Component {
    
    render() {
        let className = this.props.className || 'email-icon-default';
        
        return React.createElement('svg', {
            className: className,
            viewBox: '0 0 27 20',
            dangerouslySetInnerHTML: {__html: '<path d="M22.6,0H2C0.9,0,0,0.9,0,2v13.5c0,1.1,0.9,2,2,2h20.6c1.1,0,2-0.9,2-2V2C24.7,0.9,23.8,0,22.6,0z M20.7,2 c-2.4,2.2-6.2,5.7-6.9,6.2c-0.7,0.5-1.2,0.5-1.9,0L4.3,2H20.7z M22.6,15.6H2c0,0,0,0,0,0V2.7l8.8,7c0.7,0.5,1.4,0.8,2.2,0.8 c0.7,0,1.5-0.3,2.2-0.8c0.9-0.7,5.7-5.1,7.6-6.8L22.6,15.6C22.7,15.6,22.7,15.6,22.6,15.6z"/>'}
        });
    }
    
}
