import React from 'react';

export default class FirstPageIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.className,
            viewBox: '0 0 12 18',
            dangerouslySetInnerHTML: {__html: '<path style="fill: #6d9ec4" d="M11.7,16.3c0.4,0.4,0.4,1,0,1.4c-0.4,0.4-1,0.4-1.4,0l-8-8C2.1,9.5,2,9.3,2,9s0.1-0.5,0.3-0.7l8-8c0.4-0.4,1-0.4,1.4,0c0.4,0.4,0.4,1,0,1.4L4.4,9L11.7,16.3z M1,0C0.4,0,0,0.4,0,1v16c0,0.6,0.4,1,1,1s1-0.4,1-1V9V1C2,0.4,1.6,0,1,0z"/>'}
        });
    }
    
}
