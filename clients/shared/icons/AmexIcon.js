import React from 'react';

export default class AmexIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.className,
            viewBox: '0 0 28 21',
            dangerouslySetInnerHTML: {__html: '<path fill="#00adef" d="M24.5,21h-21C1.6,21,0,19.4,0,17.5v-14C0,1.6,1.6,0,3.5,0h21C26.4,0,28,1.6,28,3.5v14C28,19.4,26.4,21,24.5,21zM3.5,1C2.1,1,1,2.1,1,3.5v14C1,18.9,2.1,20,3.5,20h21c1.4,0,2.5-1.1,2.5-2.5v-14C27,2.1,25.9,1,24.5,1H3.5z M6,10.8H5.3L6,9v0.1l0.8,1.7H6z M12.5,11.3L11,8H9v4.8L6.8,8H6H5.2L3,13h1.3l0.4-1.1H6h1.2L7.7,13h2.5V9.3l1.7,3.7h1.2l1.7-3.6V13H16V8h-2L12.5,11.3zM20,11.9h-2.7v-1h2.7V10h-2.7V9.1H20l1.4,1.3L20,11.9z M22.9,10.4l0.9-0.9L25,8h-1.5l-1.4,1.5L20.8,8H16v5h4.6l1.4-1.6l1.4,1.7H25l-1.3-1.5L22.9,10.4z"/>'}
        });
    }
    
}
