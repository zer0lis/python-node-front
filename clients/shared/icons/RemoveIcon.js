import React from 'react';

export default class RemoveIcon extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.className,
            width: '18px',
            height: '18px',
            viewBox: '0 0 18 18',
            dangerouslySetInnerHTML: {__html: '<path d="M17.6,16l-7.2-7.1l7.2-7.2c0.4-0.4,0.4-1,0-1.4c-0.4-0.4-1-0.4-1.4,0L9,7.5L1.7,0.3c-0.4-0.4-1-0.4-1.4,0 c-0.4,0.4-0.4,1,0,1.4L7.6,9l-7.2,7.2c-0.2,0.2-0.3,0.4-0.3,0.7c0,0.3,0.1,0.5,0.3,0.7c0.4,0.4,1,0.4,1.4,0L9,10.4l7.2,7.1 c0.2,0.2,0.4,0.3,0.7,0.3c0.3,0,0.5-0.1,0.7-0.3C18,17.1,18,16.4,17.6,16z"/>'},
            onClick: (event) => {
                if (this.props.onClick) {
                    this.props.onClick(event);
                }
            }
        })
    }
    
}
