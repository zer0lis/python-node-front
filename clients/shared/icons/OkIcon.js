import React from 'react';

export default class OkIcon extends React.Component {
    
    render() {
        let color = this.props.color || '#8cb4d2';
        
        return React.createElement('svg', {
            width: '20px',
            height: '14px',
            viewBox: '0 0 20 14',
            dangerouslySetInnerHTML: {__html: `<svg width="20px" height="14px" viewBox="0 0 20 14"><g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-234.000000, -276.000000)"><g transform="translate(18.000000, 262.000000)"><g transform="translate(135.000000, 0.000000)"><g transform="translate(71.000000, 1.000000)"><rect x="0" y="0" width="40" height="40"/><g transform="translate(10.000000, 10.000000)"><rect x="0" y="0" width="20" height="20"/><path d="M18.5263158,4.46153846 L7.63157895,15.0769231" stroke="${color}" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/><path d="M1.31085526,9.84911243 L7.11348684,15.316568" stroke="${color}" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></g></g></g></g></g></g></svg>`}
        });
    }
    
}
