import React from 'react';

export default class EmptyFolderIcon extends React.Component {
    
    render() {
        let className = 'empty-folder-icon-default';
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        return React.createElement('svg', {
            className: className,
            viewBox: '0 0 104 79',
            dangerouslySetInnerHTML: {__html: '<path d="M88.5,78.2H15.5c-5.5,0-10.2-4.1-10.9-9.6L0.8,38.5c-0.2-1.6,0.2-3.2,1.2-4.4c1-1.2,2.3-2,3.9-2.3H5.8V12.3C5.8,5.9,11,0.8,17.4,0.8h34.8c4.1,0,7.5,3.3,7.5,7.5v4.2c0,1.8,1.5,3.3,3.3,3.3h25.3c5.5,0,9.9,4.4,9.9,9.9v6.3c2.9,0.5,5,2.9,5,5.9c0,0.2,0,0.5,0,0.7l-3.8,30.1C98.7,74.1,94,78.2,88.5,78.2z M6.8,34.8c-0.1,0-0.2,0-0.4,0c-1.6,0.2-2.8,1.7-2.6,3.4l3.8,30.1c0.5,4,3.9,7,7.9,7h72.9c4,0,7.4-3,7.9-7l3.8-30.1c0-0.1,0-0.2,0-0.4c0-1.7-1.3-3-3-3H6.8zM8.8,31.8h86.3v-6.2c0-3.8-3.1-6.9-6.9-6.9H63c-3.5,0-6.3-2.8-6.3-6.3V8.2c0-2.5-2-4.5-4.5-4.5H17.4c-4.7,0-8.5,3.8-8.5,8.5V31.8z"/>'}
        });
    }
    
}
