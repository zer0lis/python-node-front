import React from 'react';
import CloseX from 'CloseX';
import {translate} from 'translator';

let e = React.createElement;

export default class PopupView extends React.Component {

    render() {
        let c = this.props.controller;
        
        let footerChildren = [
            e('button', {
                className: 'button button-primary button-large',
                children: this.props.buttonText ? this.props.buttonText : translate('OK'),
                onClick: c.buttonClicked
            })
        ];

        if (this.props.cancelText) {
            footerChildren.push(
                e('button', {
                    className: 'button button-primary-common button-large',
                    children: this.props.cancelText,
                    onClick: c.cancelClicked
                })
            );
        }
        
        return e('div', {
            id: 'pop-up-bg',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'pop-up-box',
                    className: 'modal-dialog',
                    children: [
                        e('div', {
                            className: 'modal-header',
                            children: [
                                e('span', {
                                    className: 'popup-title',
                                    children: this.props.title
                                }),
                                e('a', {
                                    className: 'cancel-button',
                                    style: {
                                        display: 'block'
                                    },
                                    title: translate('Close'),
                                    children: [
                                        e(CloseX, {
                                            color: '#ccc'
                                        })
                                    ],
                                    onClick: c.cancelClicked
                                })
                            ]
                        }),
                        e('div', {
                            id: 'pop-up-message',
                            children: this.props.message
                        }),
                        e('div', {
                            className: 'modal-footer',
                            children: footerChildren
                        })
                    ],
                    onClick: function (event) {
                        event.stopPropagation(); // Make sure clicks on the dialog do not bubble up to the background.
                    }
                })
            ],
            onClick: c.cancelClicked
        });
    }
    
}
