class PopupModel {
    
    constructor() {
        this.buttonClicked = null; // Optional callback executed when the primary button is clicked.
        this.cancelClicked = null; // Optional callback executed when the cancel button is clicked.
        this.focusElement = null; // Optional id of an input to focus after the popup is closed.
    }
    
}

module.exports = PopupModel;
