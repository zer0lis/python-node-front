/**
 * NOTE: This module is used in both the teacher and student headers.
 */

import React from 'react';
import SmallActivityStatus from 'SmallActivityStatus';
import HeaderDownArrow from 'DownChevronLarge';
import HeaderRoomsMenu from 'HeaderRoomsMenu';
import utils from 'Utils';

let e = React.createElement;

export default class HeaderRoomName extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            displayName = this.props.roomName;
        
        if (window.innerWidth < 375) {
            displayName = utils.ellipsify(displayName, 25);
        }
        
        let children = [
            e('span', {
                children: displayName
            })
        ];
        
        if (!this.props.disableResults && window.innerWidth < 768 && this.props.userMenu === 'teacher') { // Results are not disabled, so an activity is running.
            children.push(
                e(SmallActivityStatus, {
                    id: 'header-room-name-activity',
                    className: 'header-animated-wifi-container',
                    animated: true
                })
            );
        }
        
        if (this.props.allowRoomsMenu) {
            if (!m.mobileMenuVisible) {
                children.push(
                    e(HeaderDownArrow, {
                        className: 'header-down-chevron-large'
                    })
                );
            }
            
            if (m.roomsMenuVisible) {
                children.push(
                    e('i', {
                        'data-icon': 'f'
                    })
                );
            }
            
            children.push(
                e(HeaderRoomsMenu, this.props)
            );
        }
        
        return e('div', {
            id: 'header-room-name-container',
            className: this.props.roomName && this.props.roomName.length > 25 ? 'long-room-name' : '',
            children: [
                e('div', {
                    id: 'header-room-name',
                    className: !this.props.allowRoomsMenu ? 'no-menu' : '',
                    children: children,
                    onClick: (event) => {
                        if (c.roomsMenuClicked) {
                            c.roomsMenuClicked(event);
                        }
                    }
                })
            ]
        });
    }
    
}
