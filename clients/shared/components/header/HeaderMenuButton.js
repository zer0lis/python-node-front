/**
 * NOTE: This module is used in both the teacher and student headers.
 */

import React from 'react';
import CloseX from 'CloseX';
import HeaderMenuBars from 'HeaderMenuBars';

let e = React.createElement;

export default class HeaderMenuButton extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model,
            children = [];
        
        if (m.mobileMenuVisible) {
            children.push(
                e(CloseX, {
                    className: 'header-menu-close'
                })
            );
        } else {
            children.push(
                e(HeaderMenuBars, {
                    className: 'header-menu-bars'
                })
            );
            
            if (this.props.userMenu === 'student' && this.props.handraiseStatus && this.props.handraise) {
                children.push(e('span', {
                        className: 'hand-raised-mobile'
                    })
                );
            }
        }
        
        return e('div', {
            className: 'header-menu-button',
            children: children,
            onClick: (event) => {
                if (c.mobileMenuClicked) {
                    c.mobileMenuClicked(event);
                }
            }
        });
    }
    
}
