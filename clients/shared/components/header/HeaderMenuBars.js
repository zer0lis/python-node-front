import React from 'react';

export default class HeaderMenuBars extends React.Component {
    
    render() {
        return React.createElement('svg', {
            className: this.props.className,
            width: '26px',
            height: '16px',
            viewBox: '0 0 26 16',
            dangerouslySetInnerHTML: {__html: '<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g transform="translate(-281.000000, -23.000000)"><g><g><g transform="translate(280.000000, 22.000000)"><rect x="0" y="0" width="28" height="28"></rect><g transform="translate(2.000000, 1.000000)" stroke="#FFFFFF" stroke-width="2" stroke-linecap="round"><path d="M0,1 L24,1"></path><path d="M0,8 L24,8"></path><path d="M0,15 L24,15"></path></g></g></g></g></g></g>'},
            onClick: (event) => {
                if (this.props.onClick) {
                    this.props.onClick(event);
                }
            }
        });
    }
    
}
