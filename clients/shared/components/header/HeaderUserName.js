/**
 * NOTE: This module is used in both the teacher and student headers.
 */

import React from 'react';
import DownChevronSmall from 'DownChevronSmall';
import ProBadgeIconSmall from 'ProBadgeIconSmall';
import HeaderMenu from 'HeaderMenu';
import HandRaisedIcon from 'HandRaisedIcon';
import StudentHeaderMenu from 'StudentHeaderMenu';
import Constants from 'Constants';

let e = React.createElement;

export default class HeaderUserName extends React.Component {
    
    render() {
        let c = this.props.controller,
            r = this.props.renewalModel;
        
        let menuChildren = [
            e('span', {
                children: this.props.userName
            }),
            e(DownChevronSmall, {
                className: 'username-chevron',
                color: '#ffffff'
            })
        ];
        
        menuChildren.push(this.props.userMenu === 'teacher' ? e(HeaderMenu, this.props) : e(StudentHeaderMenu, this.props));
        
        if (this.props.handraiseStatus && this.props.handraise) {
            menuChildren.push(e('span', {
                className: 'hand-raised',
                children: e(HandRaisedIcon, {
                        className: 'hands-raised-icon'
                    })
            }))
        }

        let containerChildren = [];
        
        if (this.props.userMenu === 'teacher' && this.props.isPro) {
            containerChildren.push(
                e(ProBadgeIconSmall, {
                    className: 'pro-badge-icon',
                    fill: r.showRenewal ? Constants.RENEWAL_COLOR : Constants.PRO_BADGE_COLOR,
                    pathClass: 'pro-badge-icon-path'
                })
            );
        }
        
        containerChildren.push(
            e('div', {
                id: 'header-user-name',
                children: menuChildren,
                onClick: c.usernameClicked
            })
        );
        
        return e('div', {
            id: 'header-user-name-container',
            children: containerChildren
        });
    }
    
}
