/**
 * Socrative Pagination - A page transition component with the following features:
 * 
 *     1. Previous and next buttons for moving backward and forward, one page at a time.
 *     2. Text for the current range of items, as well as the total number of items.
 *     3. Up to 7 page numbers for jumping directly to specific pages.
 * 
 * When a page jump occurs, this component centers the target page number (if there are enough pages before and after).
 * Here's an example after jumping from page 1 to 7, when there are a total of 572 items (12 pages):
 * 
 *     [<] 301-350 of 572 [>]
 *        4 5 6 [7] 8 9 10
 * 
 * Properties:
 * 
 *     currentPage     {number}   The page number currently being displayed
 *     itemsPerPage    {number}   The number of items displayed per page
 *     nextClicked     {function} Callback invoked when the next button is clicked
 *     pageClicked     {function} Callback invoked when any page number is clicked
 *     previousClicked {function} Callback invoked when the previous button is clicked
 *     totalItems      {number}   The total number of items to be paginated
 */

import React from 'react';
import FirstPageIcon from 'FirstPageIcon';
import PreviousIcon from 'PreviousArrowIcon';
import NextIcon from 'NextArrowIcon';
import LastPageIcon from 'LastPageIcon';
import {translate} from 'translator';

const MAX_PAGES = 7;

let e = React.createElement;

export default class Pagination extends React.Component {
    
    canClickPrevious() {
        return this.props.currentPage > 1;
    }
    
    canClickNext() {
        return this.props.currentPage < this.getTotalPages();
    }
    
    firstClicked() {
        if (this.props.pageClicked && this.canClickPrevious()) {
            this.props.pageClicked(1);
        }
    }
    
    lastClicked() {
        if (this.props.pageClicked && this.canClickNext()) {
            this.props.pageClicked(this.getTotalPages());
        }
    }
    
    previousClicked() {
        if (this.props.previousClicked && this.canClickPrevious()) {
            this.props.previousClicked();
        }
    }
    
    nextClicked() {
        if (this.props.nextClicked && this.canClickNext()) {
            this.props.nextClicked();
        }
    }
    
    pageClicked(page) {
        if (this.props.pageClicked && page !== this.props.currentPage) {
            this.props.pageClicked(page);
        }
    }
    
    getLowPage() {
        return this.props.currentPage * this.props.itemsPerPage - (this.props.itemsPerPage - 1);
    }
    
    getHighPage() {
        return Math.min(this.props.currentPage * this.props.itemsPerPage, this.props.totalItems);
    }
    
    getTotalPages() {
        return Math.ceil(this.props.totalItems / this.props.itemsPerPage);
    }
    
    getPagesToDisplay() {
        let pagesToDisplay = [],
            i = 1;
        
        if (this.props.currentPage <= 4 || this.getTotalPages() <= MAX_PAGES) {
            for (; i <= Math.min(MAX_PAGES, this.getTotalPages()); i++) {
                pagesToDisplay.push(i);
            }
        } else {
            let pageCount = 0;
            
            for (i = this.props.currentPage + 3; pageCount < MAX_PAGES; i--) {
                if (i <= this.getTotalPages()) {
                    pagesToDisplay.push(i);
                    pageCount++;
                }
            }
            
            pagesToDisplay.reverse();
        }
        
        return pagesToDisplay;
    }
    
    render() {
        let pageNumbers = this.getPagesToDisplay(),
            pageNumbersToDisplay = [];
        
        for (let i = 0; i < pageNumbers.length; i++) {
            let pageNumber = pageNumbers[i];
            
            pageNumbersToDisplay.push(
                e('button', {
                    className: 'page-number-button' + (pageNumber === this.props.currentPage ? ' page-number-button-selected' : ''),
                    children: pageNumber,
                    key: 'page-number-' + pageNumber,
                    onClick: ((pageNumber) => {
                        return () => {
                            this.pageClicked(pageNumber);
                        }
                    })(pageNumber) // Capture the page number in the closure.
                })
            );
        }
        
        let progressText = '<b>{0}-{1}</b> <i>{2}</i> <b>{3}</b>'.format(this.getLowPage(), this.getHighPage(), translate('of'), this.props.totalItems);
        
        if (this.props.itemsPerPage === 1) {
            progressText = '<b>{0}</b> <i>{1}</i> <b>{2}</b>'.format(this.getLowPage(), translate('of'), this.props.totalItems);
        }
        
        return e('div', {
            className: 'pagination-container' + (this.props.className ? (' ' + this.props.className) : ''),
            children: [
                e('div', {
                    className: 'previous-next-container',
                    children: [
                        e('button', {
                            className: 'top-page-button previous-button' + (this.canClickPrevious() ? '' : ' disabled'),
                            children: [
                                e(FirstPageIcon, {
                                    className: 'first-last-page-arrow'
                                })
                            ],
                            onClick: () => this.firstClicked()
                        }),
                        e('button', {
                            className: 'top-page-button previous-button' + (this.canClickPrevious() ? '' : ' disabled'),
                            children: [
                                e(PreviousIcon, {
                                    className: 'next-previous-page-arrow'
                                })
                            ],
                            onClick: () => this.previousClicked()
                        }),
                        e('span', {
                            className: 'pagination-range',
                            dangerouslySetInnerHTML: {__html: progressText}
                        }),
                        e('button', {
                            className: 'top-page-button next-button' + (this.canClickNext() ? '' : ' disabled'),
                            children: [
                                e(NextIcon, {
                                    className: 'next-previous-page-arrow'
                                })
                            ],
                            onClick: () => this.nextClicked()
                        }),
                        e('button', {
                            className: 'top-page-button next-button' + (this.canClickNext() ? '' : ' disabled'),
                            children: [
                                e(LastPageIcon, {
                                    className: 'first-last-page-arrow'
                                })
                            ],
                            onClick: () => this.lastClicked()
                        })
                    ]
                }),
                e('div', {
                    className: 'page-numbers-container',
                    children: pageNumbersToDisplay
                })
            ]
        });
    }
    
}
