import React from 'react';
import _ from 'underscore';
import DownChevronSmall from 'DownChevronSmall';
import platform from 'Platform';

const languages = [
    {code: 'en',    language: 'English'},
    {code: 'da',    language: 'Dansk'},
    {code: 'de',    language: 'Deutsche'},
    {code: 'en-gb', language: 'English (UK)'},
    {code: 'es',    language: 'Español'},
    {code: 'es-mx', language: 'Español (México)'},
    {code: 'fi',    language: 'Suomalainen'},
    {code: 'fr',    language: 'Français'},
    {code: 'fr-ca', language: 'Français (Canadien)'},
    {code: 'ko',    language: '한국어'},
    {code: 'ms',    language: 'Malay'},
    {code: 'nl',    language: 'Nederlands'},
    {code: 'pt-br', language: 'Português (Brasil)'},
    {code: 'sv',    language: 'Svenska'},
    {code: 'th',    language: 'ไทย'},
    {code: 'tr',    language: 'Türk'},
    {code: 'zh-cn', language: '简体中文'}
];

let e = React.createElement;

export default class LanguageMenu extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            menuVisible: false,
            bodyClicked: this.bodyClicked.bind(this), // Make it possible to remove the body click listener.
            orientationChanged: this.orientationChanged.bind(this) // Make it possible to remove the window orientationchange listener.
        };
    }
    
    createMenuItem(code, language, selected) {
        let children = [
            e('img', {
                className: 'language-menu-flag',
                src: `${window.static_url}img/flags/${code}.png`
            }),
            e('span', {
                className: 'language-menu-text',
                children: language
            })
        ];
        
        if (selected) {
            children.push(
                e('i', {
                    className: 'language-menu-checkmark',
                    'data-icon': 'd'
                })
            );
        }
        
        let languageMenuItemClass = 'language-menu-item';
        
        if (selected) {
            languageMenuItemClass += ' language-menu-item-selected';
        }
        
        if (!platform.isIos) {
            languageMenuItemClass += ' language-menu-item-hover';
        }
        
        return e('div', {
            className: languageMenuItemClass,
            children: children,
            onClick: () => this.props.controller.languageChanged(code)
        });
    }
    
    bodyClicked() {
        let view = this;
        
        if (platform.isIos) {
            document.body.style.cursor = 'default';
        }
        
        _.defer(() => {
            document.body.removeEventListener('click', view.state.bodyClicked);
            window.removeEventListener('orientationchange', view.state.orientationChanged);
            view.setState({
                menuVisible: false
            });
        });
    }
    
    orientationChanged() {
        document.body.removeEventListener('click', this.state.bodyClicked);
        window.removeEventListener('orientationchange', this.state.orientationChanged);
        this.setState({
            menuVisible: false
        });
    }
    
    buttonClicked() {
        let visible = !this.state.menuVisible;
        
        if (visible) {
            if (platform.isIos) {
                document.body.style.cursor = 'pointer'; // Make the body respond to click events in iOS.
            }
            
            document.body.addEventListener('click', this.state.bodyClicked);
            window.addEventListener('orientationchange', this.state.orientationChanged);
        }
        
        this.setState({
            menuVisible: visible
        });
    }
    
    render() {
        let selectedLanguage = '';
        
        for (let language of languages) {
            if (language.code === this.props.languageCode) {
                selectedLanguage = language.language;
                break;
            }
        }
        
        let children = [
            e('img', {
                className: 'language-menu-flag-selected',
                src: `${window.static_url}img/flags/${this.props.languageCode}.png`
            }),
            e('span', {
                className: 'language-menu-text',
                children: selectedLanguage
            }),
            e(DownChevronSmall, {
                color: 'rgba(85,85,85,0.5)',
                className: 'language-menu-chevron'
            })
        ];
        
        if (this.state.menuVisible) {
            let menuItems = [];
            
            for (let language of languages) {
                menuItems.push(
                    this.createMenuItem(language.code, language.language, language.code === this.props.languageCode)
                );
            }
            
            let MIN_INNER_HEIGHT = 800;
            
            children.push(
                e('div', {
                    className: 'language-menu-arrow' + (platform.isPhone() || window.innerHeight < MIN_INNER_HEIGHT ? ' language-menu-arrow-above' : ' language-menu-arrow-below')
                })
            );
            
            children.push(
                e('div', {
                    className: 'language-menu' + (platform.isPhone() || window.innerHeight < MIN_INNER_HEIGHT ? ' language-menu-above' : ' language-menu-below'),
                    children: menuItems
                })
            );
        }
        
        return e('div', {
            className: 'language-menu-button join-room-language-menu',
            children: children,
            onClick: () => this.buttonClicked()
        });
    }
    
}
