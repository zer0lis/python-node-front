import React from 'react';

let e = React.createElement;

class DropDownOption extends React.Component {
    
    render() {
        return e('li', {
            className: 'button-dropdown-element',
            children: this.props.text || '',
            onClick: (event) => {
                if (this.props.onClick) {
                    this.props.onClick(event, this.props.data);
                }
            }
        });
    }
    
}

export default class DropdownList extends React.Component {
    
    render() {
        let children = [];
        
        for (let option of this.props.children) {
            children.push(
                e(DropDownOption, {
                    text: option.text,
                    data: option.data,
                    onClick: option.onClick
                })
            );
        }

        return e('ul', {
            className: 'button-dropdown-container',
            children: children
        });
    }
    
}
