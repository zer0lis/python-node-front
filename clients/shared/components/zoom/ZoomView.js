import React from 'react';
import ImageIcon from 'ImageIcon';
import CloseIcon from 'CloseIcon';

const DEFAULT_WIDTH = 300;
const BORDER = 1;
const MARGIN = 10;

let e = React.createElement;

export default class ZoomView extends React.Component {
    
    render() {
        let c = this.props.controller,
            m = this.props.model;
        
        let children = [
            e('div', {
                className: 'zoom-image-close-container',
                children: [
                    e(CloseIcon, {
                        className: 'zoom-image-close-x',
                        color: '#999999'
                    })
                ]
            })
        ];
        
        let image = m.image,
            containerStyle = {},
            imageStyle = {};
        
        if (m.imageLoaded) {
            if (image.width + BORDER + MARGIN > window.innerWidth || image.height + BORDER + MARGIN > window.innerHeight) {
                let scaledImageWidth = image.width,
                    scaledImageHeight = image.height;
                
                if (image.width > image.height) {
                    scaledImageWidth = window.innerWidth - (BORDER * 2) - (MARGIN * 2);
                    scaledImageHeight = scaledImageWidth / image.width * image.height;
                    containerStyle.width = window.innerWidth - (MARGIN * 2);
                } else {
                    scaledImageHeight = window.innerHeight - (BORDER * 2) - (MARGIN * 2);
                    scaledImageWidth = scaledImageHeight / image.height * image.width;
                    containerStyle.width = scaledImageWidth + (BORDER * 2);
                    containerStyle.height = window.innerHeight - (MARGIN * 2);
                }
                
                imageStyle.height = scaledImageHeight;
                imageStyle.width = scaledImageWidth;
            } else {
                containerStyle.width = image.width + (BORDER * 2);
                containerStyle.height = image.height + (BORDER * 2);
                imageStyle.width = image.width;
                imageStyle.height = image.height;
            }
            
            children.push(
                e('img', {
                    className: 'zoom-image',
                    src: image.src,
                    style: imageStyle
                })
            );
        } else {
            children.push(
                e('div', {
                    className: 'zoom-image-placeholder-container',
                    children: [
                        e(ImageIcon, {
                            className: 'zoom-image-placeholder'
                        })
                    ]
                })
            );
            
            containerStyle.width = `${DEFAULT_WIDTH + BORDER + BORDER}px`;
        }
        
        return e('div', {
            id: 'zoom-view-background',
            className: 'modal open',
            children: [
                e('div', {
                    id: 'zoom-view-box',
                    className: 'modal-dialog',
                    children: children,
                    style: containerStyle
                })
            ],
            onClick: c.closeClicked
        });
    }
    
}
