import React from 'react';
import ReactDOM from 'react-dom';
import ZoomModel from 'ZoomModel';
import view from 'ZoomView';

let model = null;

class ZoomController {
    
    open(imageUrl) {
        let container = document.createElement('div');
        container.id = 'zoom-view-container';
        document.body.appendChild(container);
        
        model = new ZoomModel();
        model.image = new Image();
        model.image.onload = controller.imageLoaded;
        model.image.src = imageUrl;
        
        controller.renderView();
    }
    
    imageLoaded() {
        model.imageLoaded = true;
        controller.renderView();
    }
    
    renderView() {
        ReactDOM.render(
            React.createElement(
                view, {
                    controller: controller,
                    model: model
                }
            ),
            document.getElementById('zoom-view-container')
        );
    }
    
    closeClicked() {
        let container = document.getElementById('zoom-view-container');
        ReactDOM.unmountComponentAtNode(container);
        container.parentNode.removeChild(container);
    }
    
}

let controller = new ZoomController();
module.exports = controller;
