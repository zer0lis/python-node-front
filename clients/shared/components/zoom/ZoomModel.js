'use strict';

class ZoomModel {
    
    constructor() {
        this.image = null; // The image to be displayed (an instance of the native Image class).
        this.imageLoaded = false; // Whether the image has been loaded.
    }
    
}

module.exports = ZoomModel;
