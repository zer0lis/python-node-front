/**
 * Socrative Folder Menu - The menu component used in crumb navigation of folders.
 * 
 * A folder menu is composed of the following visual elements:
 * 
 *     - A toggle button with two icons: A folder icon and a down chevron icon
 *     - A menu of clickable folder names, only visible after the toggle is clicked and the mouse is over any part of the folder menu component
 * 
 * Properties:
 * 
 *     className {string}   CSS class applied to the entire component (usually for positioning or sizing)
 *     folders   {array}    Array of one or more folder objects to be displayed when the toggle button is clicked
 *     onClick   {function} Function to execute when a folder in the menu is clicked
 */

import React from 'react';
import FolderIcon from 'FolderIcon';
import DownChevronIcon from 'RightChevronThin'; // The design calls for the right chevron SVG to be rotated 90 degrees (so we were only given one asset).

let e = React.createElement;

export default class FolderMenu extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            showingMenu: false
        };
    }
    
    render() {
        let children = [
            e('button', {
                className: 'folder-menu-button' + (this.state.showingMenu ? ' folder-menu-button-selected' : ''),
                children: [
                    e(FolderIcon, {
                        className: 'folder-menu-folder-icon'
                    }),
                    e(DownChevronIcon, {
                        className: 'folder-menu-chevron-icon'
                    })
                ],
                onClick: () => {
                    this.setState({
                        showingMenu: !this.state.showingMenu
                    });
                }
            })
        ];
        
        if (this.state.showingMenu) {
            let folders = [];
            
            for (let folder of this.props.folders) {
                folders.push(
                    e('span', {
                        className: 'folder-menu-item',
                        children: folder.name,
                        key: `folder-menu-item-${folder.id}`,
                        onClick: () => {
                            this.props.onClick(folder.id);
                            this.setState({
                                showingMenu: false
                            });
                        }
                    })
                );
            }
            
            children.push(
                e('div', {
                    className: 'folder-menu-items-container',
                    children: folders
                })
            );
        }
        
        return e('div', {
            className: 'folder-menu-default' + (this.props.className ? ` ${this.props.className}` : ''),
            children: children,
            onMouseLeave: () => {
                this.setState({
                    showingMenu: false
                });
            }
        });
    }
    
}
