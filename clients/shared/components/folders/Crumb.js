/**
 * Socrative Crumb - The label/link component used in crumb navigation.
 * 
 * A crumb is composed of the following visual elements:
 * 
 *     - A label for the crumb's name, which can be a clickable "link"
 *     - A title tooltip, displayed on hover when the label has been ellipsified
 *     - An optional right chevron "separator" icon
 * 
 * Properties:
 * 
 *     className {string}   CSS class applied to the entire component (usually for positioning or sizing)
 *     current   {boolean}  Flag indicating whether this is the current crumb. If this is true, the crumb will not look like a link or be clickable.
 *     id        {number}   The id associated with this crumb (used as the React key)
 *     label     {string}   String of text used for the label
 *     onClick   {function} Function to execute when this crumb is clicked and it is not the current crumb
 */

import React from 'react';
import TitleTooltip from 'TitleTooltip';
import RightChevronThin from 'RightChevronThin';

let e = React.createElement;

export default class Crumb extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            showingTooltip: false
        };
    }
    
    render() {
        let labelChildren = [
            e('span', {
                className: 'crumb-label' + (!this.props.current ? ' link' : ''),
                children: this.props.label,
                onMouseEnter: (event) => {
                    if (event.target.offsetWidth < event.target.scrollWidth && !this.state.showingTooltip) {
                        this.setState({
                            showingTooltip: true
                        });
                    }
                },
                onMouseLeave: () => {
                    if (this.state.showingTooltip) {
                        this.setState({
                            showingTooltip: false
                        });
                    }
                },
                onClick: () => {
                    if (!this.props.current) {
                        this.setState({
                            showingTooltip: false
                        });
                        this.props.onClick(this.props.id);
                    }
                }
            })
        ];
        
        if (this.state.showingTooltip) {
            labelChildren.push(
                React.createElement(
                    TitleTooltip, {
                        className: 'crumb-tooltip',
                        text: this.props.label
                    }
                )
            );
        }
        
        let children = [
            e('div', {
                className: 'crumb-label-container',
                children: labelChildren
            })
        ];
        
        if (!this.props.current) {
            children.push(
                React.createElement(
                    RightChevronThin, {
                        className: 'crumb-separator'
                    }
                )
            );
        }
        
        return e('div', {
            className: 'crumb-default' + (this.props.className ? ` ${this.props.className}` : ''),
            children: children,
            key: `crumb-${this.props.id}`
        });
    }
    
}
