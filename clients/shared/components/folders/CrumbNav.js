/**
 * Socrative CrumbNav - A component that provides crumb navigation for folders.
 * 
 * A crumb nav is composed of the following visual elements:
 * 
 *     - One or more crumbs
 *     - A folder menu toggle, only visible when the trail has 5 or more crumbs
 * 
 * Properties:
 * 
 *     className      {string}   CSS class applied to the entire component (usually for positioning or sizing)
 *     currentFolder  {object}   Object representing the current folder
 *     folderNavModel {object}   The instance of FolderNavModel on which this component is operating
 *     onClick        {function} Function to execute when a crumb is clicked
 */

import React from 'react';
import platform from 'Platform';
import FolderMenu from 'FolderMenu';
import Crumb from 'Crumb';
import Constants from 'Constants';

let e = React.createElement;

export default class CrumbNav extends React.Component {
    
    getMaxCrumbs() {
        return platform.isTabletPortrait() ? 3 : 4;
    }
    
    render() {
        let trail = [this.props.currentFolder],
            id = this.props.currentFolder.id;
        
        while (id !== Constants.ROOT_FOLDER) {
            let folder = this.props.folderNavModel.getFolder(id),
                parent = this.props.folderNavModel.getFolder(folder.parentId);
            
            trail.unshift(parent);
            id = parent.id;
        }
        
        let children = [],
            index = 0;
        
        if (trail.length > this.getMaxCrumbs()) {
            let menuCount = trail.length - this.getMaxCrumbs(),
                menuFolders = [];
            
            for (; index < menuCount; index++) {
                menuFolders.push(trail[index]);
            }
            
            children.push(
                e(FolderMenu, {
                    folders: menuFolders,
                    onClick: (id) => {
                        this.props.onClick(id);
                    }
                })
            );
        }
        
        for (; index < trail.length; index++) {
            let folder = trail[index];
            
            children.push(
                e(Crumb, {
                    className: trail.length > this.getMaxCrumbs() ? 'crumb-nav-folder-menu' : 'crumb-nav-no-folder-menu',
                    current: folder === this.props.currentFolder,
                    id: folder.id,
                    label: folder.name,
                    onClick: (id) => {
                        this.props.onClick(id);
                    }
                })
            );
        }
        
        return e('div', {
            className: 'crumb-nav-default' + (this.props.className ? ` ${this.props.className}` : ''),
            children: children
        });
    }
    
}
