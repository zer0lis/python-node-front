/**
 * Socrative Nav Folder - The folder component used in navigation sidebars.
 * 
 * A nav folder is composed of the following visual elements:
 * 
 *     - An expander button with a chevron icon that faces right when the folder is collapsed, and down when the folder is expanded
 *     - A folder icon
 *     - The folder's name
 *     - A tooltip, displayed on hover when the name has been ellipsified
 * 
 * Properties:
 * 
 *     className       {string}   The CSS class applied to the entire component (usually for positioning)
 *     current         {boolean}  Whether this folder is the currently selected folder in the navigation sidebar
 *     expanded        {boolean}  Whether this folder has been expanded in the navigation sidebar
 *     id              {number}   Required folder id, used for the React key property
 *     level           {number}   The level of indent for this component (0 is the root level)
 *     name            {string}   The name of the folder
 *     onClick         {function} Function to execute when the component is clicked
 *     onDoubleClick   {function} Function to execute when the component is double-clicked
 *     onExpanderClick {function} Function to execute when the expander button is clicked
 */

import React from 'react';
import FolderIcon from 'FolderIcon';
import CurrentFolderIcon from 'CurrentFolderIcon';
import RightChevron from 'RightChevronSmall';
import DownChevron from 'DownChevronSmall';
import TitleTooltip from 'TitleTooltip';

const NAV_SIDEBAR_WIDTH = 240;    // Width of the navigation sidebar in pixels (from the .quizzes-folder-nav-container selector in quizzes.less).
const FOLDER_INDENT = 30;         // Number of padding-left pixels for each folder level.
const EXPANDER_BUTTON_WIDTH = 36; // Width of the expander button in pixels (from the .nav-folder-expander-button selector in nav-folder.less).
const EXPANDER_BUTTON_RIGHT_MARGIN = 8; // Width of the expander button's right margin in pixels (from the .nav-folder-expander-button selector in nav-folder.less).
const FOLDER_ICON_WIDTH = 21;     // Width of the folder icon in pixels (from the .nav-folder-icon selector in nav-folder.less).
const NAME_LEFT_MARGIN = 8;       // Number of margin-left pixels for each folder name (from the .nav-folder-name selector in nav-folder.less).
const RIGHT_BORDER = 1;           // Number of border-right pixels for the navigation sidebar (from .quizzes-short-divider in quizzes.less).
const MAX_VISIBLE_NAME_LEVEL = 5; // The indent level at which the name is still partially visible, so hovering to see the title tooltip still works.

let e = React.createElement;

export default class NavFolder extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            showingTooltip: false
        };
    }
    
    render() {
        let Chevron = this.props.expanded ? DownChevron : RightChevron,
            Folder = this.props.current  ? CurrentFolderIcon : FolderIcon,
            className = 'nav-folder';
        
        if (this.props.current) {
            className += ' nav-folder-current';
        }
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        let navFolderChildren = [
            e(Folder, {
                className: 'nav-folder-icon' + (this.props.current ? ' folder-icon-current' : '')
            })
        ];
        
        if (this.state.showingTooltip) {
            navFolderChildren.push(
                e(TitleTooltip, {
                    className: 'nav-folder-name-tooltip',
                    text: this.props.name
                })
            );
        }
        
        return e('div', {
            className: className,
            key: this.props.id ? `nav-folder-${this.props.id}` : 'root-folder',
            style: {
                paddingLeft: (this.props.level * FOLDER_INDENT) + 'px'
            },
            children: [
                e('button', {
                    className: 'nav-folder-expander-button',
                    children: [
                        e(Chevron, {
                            className: this.props.expanded ? 'nav-folder-expander-button-expanded' : '',
                            color: this.props.current ? '#8cb4d2' : '#cccccc'
                        })
                    ],
                    onClick: (event) => {
                        event.stopPropagation();
                        this.props.onExpanderClick();
                    }
                }),
                e('div', {
                    className: 'nav-folder-container',
                    children: navFolderChildren
                }),
                e('span', {
                    className: 'nav-folder-name' + (this.props.current ? ' nav-folder-name-current' : ''),
                    children: this.props.level <= MAX_VISIBLE_NAME_LEVEL ? this.props.name : '', // Hide the name at level 6 and above because there's no good way to handle overflow.
                    style: {
                        width: `${NAV_SIDEBAR_WIDTH - ((this.props.level * FOLDER_INDENT) + EXPANDER_BUTTON_WIDTH + EXPANDER_BUTTON_RIGHT_MARGIN + FOLDER_ICON_WIDTH + NAME_LEFT_MARGIN + RIGHT_BORDER)}px`
                    },
                    onMouseEnter: (event) => {
                        if (event.target.offsetWidth < event.target.scrollWidth && !this.state.showingTooltip) {
                            this.setState({
                                showingTooltip: true
                            });
                        }
                    },
                    onMouseLeave: () => {
                        if (this.state.showingTooltip) {
                            this.setState({
                                showingTooltip: false
                            });
                        }
                    }
                })
            ],
            onClick: () => {
                this.props.onClick();
            },
            onDoubleClick: () => {
                this.props.onDoubleClick();
            }
        });
    }
    
}
