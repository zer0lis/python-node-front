/**
 * Socrative List Folder - The component used in folder lists for simple navigation and optional selection.
 * 
 * A list folder is composed of the following visual elements:
 * 
 *     - A folder icon
 *     - The folder's name
 *     - A "go" button with a right chevron icon. Clicking this button takes you into the folder.
 * 
 * Properties:
 * 
 *     className       {string}   Optional CSS class applied to the entire component (usually for positioning)
 *     hideButton      {boolean}  Optional flag indicating whether the "go" button should be visible
 *     id              {number}   Required folder id, used for the React key property
 *     name            {string}   Required folder name
 *     onClick         {function} Required function that will be executed when either the "go" button is clicked, or when the component is clicked and onSelect is falsy
 *     onSelect        {function} Optional function that will be executed when the component is clicked. This makes the folder selectable.
 *     selectable      {boolean}  Optional flag indicating whether this folder is selectable
 *     selected        {boolean}  Optional flag indicating whether this is the selected folder in the list
 *     selectError     {string}   Optional message to display in a banner when selectable is false and the user attempts to select this component
 */

import React from 'react';
import FolderIcon from 'FolderIcon';
import RightChevron from 'RightChevron';
import AlertIcon from 'AlertIcon';
import Constants from 'Constants';

let e = React.createElement;

export default class ListFolder extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            showBanner: false
        };
    }
    
    componentWillReceiveProps(nextProps) {
        this.setState({
            showBanner: this.state.showBanner && nextProps.id === this.props.id
        });
    }
    
    // Since the banner is cleared with setTimeout(), use a "mounted" property to avoid
    // accessing this.state or calling setState() after the component has unmounted. As
    // strange as it may seem, this is actually what the React documentation recommends:
    // https://facebook.github.io/react/blog/2015/12/16/ismounted-antipattern.html
    componentDidMount() {
        this.mounted = true;
    }
    
    componentWillUnmount() {
        this.mounted = false;
    }
    
    render() {
        let className = 'list-folder-container-default';
        
        if (this.props.selected) {
            className += ' list-folder-container-default-selected';
        }
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        let children = [
            React.createElement(
                FolderIcon, {
                    className: 'list-folder-icon'
                }
            ),
            e('span', {
                className: 'list-folder-name',
                children: this.props.name
            })
        ];
        
        if (!this.props.hideButton) {
            children.push(
                e('button', {
                    className: 'list-folder-go-button',
                    children: [
                        React.createElement(
                            RightChevron, {
                                className: 'list-folder-go-button-icon',
                                color: '#bbbbbb'
                            }
                        )
                    ],
                    onClick: (event) => {
                        event.stopPropagation();
                        this.props.onClick(event);
                    }
                })
            );
        }
        
        if (this.state.showBanner) {
            children.push(
                e('div', {
                    className: 'list-folder-banner',
                    children: [
                        React.createElement(
                            AlertIcon, {
                                color: '#ffffff',
                                className: 'list-folder-banner-icon'
                            }
                        ),
                        e('span', {
                            className: 'list-folder-banner-text',
                            children: this.props.selectError
                        })
                    ]
                    
                })
            );
        }
        
        return e('div', {
            className: className,
            children: children,
            key: `list-folder-${this.props.id}`,
            onClick: (event) => {
                if (this.props.onSelect) {
                    if (this.props.selectable === false) {
                        if (this.props.selectError) {
                            this.setState({
                                showBanner: true
                            });
                            setTimeout(() => {
                                if (this.mounted) {
                                    this.setState({
                                        showBanner: false
                                    });
                                }
                            }, Constants.BANNER_TIMEOUT * 1.125); // Remove the banner after 2.25 seconds (its CSS animation time).
                        }
                        return;
                    }
                    this.props.onSelect(event);
                } else {
                    this.props.onClick(event);
                }
            },
            onDoubleClick: (event) => {
                if (this.props.selectable === false || !this.props.onSelect) {
                    return; // The folder is not selectable, so the onClick() handler will have executed already.
                }
                this.props.onClick(event);
            }
        });
    }
    
}
