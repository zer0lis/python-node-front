/**
 * Socrative Nav Trash - The trash component used in navigation sidebars.
 * 
 * A nav trash is composed of the following visual elements:
 * 
 *     - A trash icon
 *     - The translated label "Trash"
 * 
 * Properties:
 * 
 *     className {string}   The CSS class applied to the entire component (usually for positioning)
 *     current   {boolean}  Whether the trash is the currently selected item in the navigation sidebar
 *     onClick   {function} Function to execute when the component is clicked
 */

import React from 'react';
import TrashIcon from 'TrashIcon';
import {translate} from 'translator';

let e = React.createElement;

export default class NavTrash extends React.Component {
    
    render() {
        let className = 'nav-trash';
        
        if (this.props.current) {
            className += ' nav-trash-current';
        }
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        return e('div', {
            className: className,
            children: [
                e(TrashIcon, {
                    className: 'nav-trash-icon' + (this.props.current ? ' nav-trash-icon-current' : '')
                }),
                e('span', {
                    className: 'nav-trash-name' + (this.props.current ? ' nav-trash-name-current' : ''),
                    children: translate('Trash')
                })
            ],
            onClick: (event) => {
                this.props.onClick(event);
            }
        });
    }
    
}
