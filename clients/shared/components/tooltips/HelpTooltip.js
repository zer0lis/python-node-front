import React from 'react';
import _ from 'underscore';
import InfoIcon from 'InfoIcon';
import ProBadgeIcon from 'ProBadgeIcon';
import platform from 'Platform';

let e = React.createElement;

export default class HelpTooltip extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            bodyClicked: this.bodyClicked.bind(this), // Make it possible to remove the body click listener.
            showTooltip: false
        };
    }
    
    helpClicked() {
        let showTooltip = !this.state.showTooltip;
        
        if (showTooltip) {
            document.body.addEventListener('click', this.state.bodyClicked);
            if (platform.isIos) {
                document.body.style.cursor = 'pointer'; // This makes any element respond to click events in iOS.
            }
        }
        
        this.setState({
            showTooltip: showTooltip
        });
    }
    
    bodyClicked() {
        if (platform.isIos) {
            document.body.style.cursor = 'default';
        }
        
        let view = this;
        
        _.defer(function() {
            document.body.removeEventListener('click', view.state.bodyClicked);
            view.setState({
                showTooltip: false
            });
        });
    }
    
    render() {
        let children = [
            e('div', {
                className: 'help-tooltip-button',
                children: [
                    e(InfoIcon, {
                        className: 'help-icon',
                        color: '#6d9ec4'
                    })
                ],
                onClick: () => {
                    this.helpClicked();
                }
            })
        ];

        let proIcon = '';
        
        if (this.props.showProIcon) {
            proIcon = e(ProBadgeIcon, {
                className: 'pro-badge-icon',
                color: '#f89e1b',
                size: '24'
            })
        }
        
        if (this.state.showTooltip) {
            children.push(
                e('div', {
                    className: 'help-tooltip ' + (this.props.above ? 'help-tooltip-above' : 'help-tooltip-below'),
                    children: [
                        proIcon,
                        e('span', {
                            className: 'help-title',
                            children: this.props.title
                        }),
                        e('span', {
                            className: 'help-text',
                            children: this.props.text
                        })
                    ]
                }),
                e('div', {
                    className: 'help-tooltip-arrow ' + (this.props.above ? 'help-tooltip-arrow-above' : 'help-tooltip-arrow-below')
                })
            );
        }
        
        return e('div', {
            className: this.props.className + ' help-tooltip-container',
            children: children
        });
    }
    
}
