import React from 'react';
import AlertIcon from 'AlertIcon';

let e = React.createElement;

export default class Tooltip extends React.Component {

    render() {
        let className = this.props.className ? (' ' + this.props.className) : '';
        
        if (this.props.title) {
            className += ' tooltip-title';
        }
        
        let location = this.props.location || 'below',
            title = null;
        
        if (this.props.title) {
            title = e('p', {
                children: [
                    e(AlertIcon, {
                        className: 'input-error-icon'
                    }),
                    e('span', {
                        className: 'tooltip-title-text',
                        children: this.props.title
                    })
                ]
            });
        }

        return e('div', {
            className: 'tooltip-container-' + location + className,
            children: [
                e('div', {
                    className: 'tooltip',
                    children: [
                        e('div', {
                            className: 'triangle-left-' + location
                        }),
                        e('div', {
                            className: 'triangle-right-' + location
                        }),
                        title,
                        e('span', {
                            className: 'tooltip-text',
                            children: this.props.text
                        })
                    ]
                })
            ]
        });
    }
    
}
