/**
 * Socrative Anchor - A custom component that wraps the standard HTML <a> element and extends its capabilities.
 * 
 * An anchor is composed of:
 * 
 *     - A label for the text of the anchor
 *     - An error tooltip, displayed on click when the anchor is disabled
 * 
 * Properties:
 * 
 *     className    {string}   CSS class applied to the entire component (usually for positioning or sizing)
 *     disabled     {boolean}  Whether the anchor is disabled
 *     href         {string}   Optional value for the HTML href attribute
 *     keyValue     {string}   The unique value associated with this anchor, used as the React key
 *     label        {string}   String of text used for the label
 *     onClick      {function} Optional callback invoked when the anchor is clicked and not disabled
 *     tooltipTitle {string}   The title of the tooltip displayed on click when the anchor is disabled
 *     tooltipText  {string}   The text of the tooltip displayed on click when the anchor is disabled
 */

import React from 'react';
import Tooltip from 'Tooltip';

let e = React.createElement;

export default class Anchor extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            showingTooltip: false
        };
    }
    
    render() {
        let anchorProps = {
            className: 'link' + (this.props.disabled ? ' anchor-default-disabled' : ''),
            children: this.props.label,
            onClick: (event) => {
                if (this.props.disabled) {
                    event.preventDefault();
                    event.stopPropagation();
                    this.setState({
                        showingTooltip: !this.state.showingTooltip
                    });
                } else if (this.props.onClick) {
                    this.props.onClick(event);
                }
            }
        };
        
        if (this.props.href) {
            anchorProps.href = this.props.href;
        }
        
        let children = [
            e('a', anchorProps)
        ];
        
        if (this.state.showingTooltip) {
            children.push(
                e(Tooltip, {
                    className: 'anchor-tooltip',
                    location: 'above',
                    title: this.props.tooltipTitle,
                    text: this.props.tooltipText
                })
            );
        }
        
        return e('div', {
            className: 'anchor-default' + (this.props.className ? ` ${this.props.className}` : ''),
            children: children,
            onMouseLeave: () => {
                if (this.state.showingTooltip) {
                    this.setState({
                        showingTooltip: false
                    });
                }
            }
        });
    }
    
}
