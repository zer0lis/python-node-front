/**
 * Socrative Activity Image - The component used to display images in activities. This component is used
 *                            in both the student quiz view and the teacher question drilldown view.
 * 
 * An activity image is composed of the following visual elements:
 * 
 *     - A placeholder icon displayed while the image is loading
 *     - An image
 *     - A magnifying glass icon below the image
 *     - A label below the image whose text is "zoom"
 * 
 * Properties:
 * 
 *     className {string} CSS class applied to the entire component (usually for positioning or sizing)
 *     imageUrl  {string} URL of the image to be displayed
 */

import React from 'react';
import ImageIcon from 'ImageIcon';
import SearchIcon from 'SearchIcon';
import zoomModal from 'ZoomController';
import {translate} from 'translator';

let e = React.createElement;

export default class ActivityImage extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            imageLoaded: false
        };
    }
    
    render() {
        let children = [
            e('img', {
                className: 'activity-image' + (this.state.imageLoaded ? '' : ' hidden'),
                src: this.props.imageUrl,
                onLoad: () => {
                    this.setState({
                        imageLoaded: true
                    });
                }
            })
        ];
        
        if (!this.state.imageLoaded) {
            children.push(
                e('div', {
                    className: 'activity-image-placeholder-container',
                    children: [
                        e(ImageIcon, {
                            className: 'activity-image-placeholder'
                        })
                    ]
                })
            );
        } else {
            children.push(
                e('div', {
                    className: 'activity-image-zoom-icon-container',
                    children: [
                        e(SearchIcon, {
                            className: 'activity-image-zoom-icon'
                        })
                    ]
                }),
                e('span', {
                    className: 'activity-image-zoom-label',
                    children: translate('zoom')
                })
            );
        }
        
        return e('div', {
            className: 'activity-image-container' + (this.props.className ? ` ${this.props.className}` : ''),
            children: children,
            onClick: () => {
                if (this.state.imageLoaded) {
                    zoomModal.open(this.props.imageUrl);
                }
            }
        });
    }
    
}
