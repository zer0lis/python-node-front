import React from 'react';

let e = React.createElement;

export default class RadioButton extends React.Component {
    
    render() {
        let selected = this.props.selected;
        
        return e('div', {
            className: 'radio-button' + (this.props.className ? (' ' + this.props.className) : ''),
            children: [
                e('div', {
                    className: 'radio-button-outer' + (selected ? ' radio-button-outer-selected' : ''),
                    children: [
                        e('div', {
                            className: 'radio-button-inner',
                            style: {visibility: selected ? 'visible' : 'hidden'}
                        })
                    ]
                }),
                e('span', {
                    className: 'radio-button-label',
                    children: this.props.label
                })
            ],
            onClick: (event) => {
                this.props.onClick(event);
            }
        });
    }
    
}
