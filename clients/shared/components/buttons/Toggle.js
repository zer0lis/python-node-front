import React from 'react';
import Tooltip from 'Tooltip';

let e = React.createElement;

export default class Toggle extends React.Component {
    
    onClick() {
        if (this.props.onClick) {
            this.props.onClick();
        }
    }
    
    render() {
        let toggleLabelClass = 'toggle-label';
        
        if (this.props.disabled) {
            toggleLabelClass += ' toggle-label-disabled';
        }
        
        let toggleChildren = [
            e('div', {
                className: (this.props.alignment) ? 'toggle-slider ' + this.props.alignment : 'toggle-slider align-left',
                children: [
                    e('input', {
                        id: this.props.id,
                        className: 'toggle-slider-checkbox',
                        type: 'checkbox',
                        disabled: this.props.disabled,
                        checked: this.props.checked,
                        onChange: this.props.onChange
                    }),
                    e('label', {
                        className: 'toggle-slider-label',
                        htmlFor: this.props.id,
                        children: [
                            e('div', {
                                className: 'toggle-slider-inner'
                            }),
                            e('div', {
                                className: 'toggle-slider-switch'
                            })
                        ]
                    })
                ],
                onClick: () => this.onClick()
            })
        ];
        
        if (this.props.label) {
            toggleChildren.push(
                e('label', {
                    htmlFor: this.props.id,
                    className: toggleLabelClass,
                    children: this.props.label,
                    onClick: () => this.onClick()
                })
            );
        }
        
        if (this.props.children) {
            toggleChildren.push(this.props.children)
        }
        
        if (this.props.tooltip) {
            toggleChildren.push(
                e(Tooltip, this.props.tooltip)
            );
        }
        
        return e('div', {
            className: 'settings-cnt clearfix' + (this.props.className ? ` ${this.props.className}` : ''),
            children: toggleChildren
        });
    }

}
