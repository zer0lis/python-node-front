/*
    More Button
    
    This component provides a "MORE" button with a 3-dot icon. When clicked, this 
    component opens a small menu over the "MORE" button with one or more items.
    
    Properties:
    
    arrowClass {string}   Optional CSS class to append to the menu arrow.
    className  {string}   Optional CSS class to append to the outer container of this component.
    menuClass  {string}   Optional CSS class to append to the menu.
    menuItems  {array}    Array of renderable components that will appear inside the menu when opened.
    onClick    {function} Optional callback that overrides the default click behavior of showing/hiding the menu.
    
    Note: This explains why a separate "mounted" variable is used to track whether this component is mounted:
          https://facebook.github.io/react/blog/2015/12/16/ismounted-antipattern.html
 */

import React from 'react';
import _ from 'underscore';
import platform from 'Platform';
import MoreDots from 'MoreDotsIcon';
import {translate} from 'translator';

let mounted = false,
    e = React.createElement;

export default class MoreButton extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            bodyClicked: this.bodyClicked.bind(this), // Make it possible to remove the body click listener.
            showMenu: false
        };
    }
    
    componentWillMount() {
        this.setState({
            showMenu: false
        });
        mounted = true;
    }
    
    componentWillUnmount() {
        this.setState({
            showMenu: false
        });
        mounted = false;
    }
    
    onClick(event) {
        if (this.props.onClick) {
            this.props.onClick(event);
        } else {
            if (!this.state.showMenu) {
                document.body.addEventListener('click', this.state.bodyClicked);
                if (platform.isIos) {
                    document.body.style.cursor = 'pointer'; // This makes any element respond to click events in iOS.
                }
            }
            this.setState({
                showMenu: !this.state.showMenu
            });
        }
    }
    
    bodyClicked() {
        if (platform.isIos) {
            document.body.style.cursor = 'default';
        }
        
        _.defer(() => {
            document.body.removeEventListener('click', this.state.bodyClicked);
            if (mounted) {
                this.setState({
                    showMenu: false
                });
            }
        });
    }
    
    render() {
        let children = [
            e(MoreDots, {
                className: 'more-dots-icon'
            }),
            e('span', {
                className: 'more-button-text',
                children: translate('MORE')
            })
        ];
        
        if (this.state.showMenu) {
            let menuChildren = [];
            
            for (let i = 0; i < this.props.menuItems.length; i++) {
                menuChildren.push(
                    this.props.menuItems[i]
                );
            }
            
            menuChildren.push(
                e('i', {
                    className: 'more-button-menu-arrow' + (this.props.arrowClass ? (' ' + this.props.arrowClass) : ''),
                    'data-icon': 'f'
                })
            );
            
            children.push(
                e('div', {
                    className: 'more-button-menu' + (this.props.menuClass ? (' ' + this.props.menuClass) : ''),
                    children: menuChildren
                })
            );
        }
        
        return e('div', {
            className: 'more-button-container' + (this.props.className ? (' ' + this.props.className) : ''),
            children: children,
            onClick: (event) => this.onClick(event)
        });
    }
    
}
