import React from 'react';
import EditIcon from 'EditIcon';

let e = React.createElement;

export default class EditButton extends React.Component {
    
    render() {
        let className = 'edit-button-default';
        
        if (this.props.className) {
            className += ` ${this.props.className}`;
        }
        
        return e('button', {
            className: className,
            children: [
                e(EditIcon, {
                    className: 'edit-button-icon'
                })
            ],
            disabled: this.props.disabled,
            onClick: (event) => {
                this.props.onClick(event);
            }
        });
    }
    
}
