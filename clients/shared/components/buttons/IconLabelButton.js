/**
 * Socrative Icon Label Button
 * 
 * This is a button component with a blue icon (#8cb4d2) to the left of its label. If the button is enabled, it 
 * displays a light gray background (#eeeeee) with square corners on hover. If the button is disabled, the icon 
 * and label are gray (#cccccc and rgba(85, 85, 85, 0.5), respectively). The default CSS for this component is
 * defined in icon-label-button.less.
 * 
 * Properties:
 * 
 *     className    {string}   The CSS class applied to the entire component (usually for positioning)
 *     disabled     {boolean}  Whether or not the button appears disabled
 *     icon         {object}   React class of an SVG used for the button's icon
 *     iconClass    {string}   Optional CSS class applied only to the icon (usually for positioning)
 *     label        {string}   The text used for the button's label
 *     onClick      {function} Callback invoked when the button is clicked
 *     title        {string}   Optional string to be displayed in a title tooltip on hover
 *     tooltipTitle {string}   The title of the tooltip displayed on click when the button is disabled
 *     tooltipText  {string}   The text of the tooltip displayed on click when the button is disabled
 * 
 * This component is used in the following locations:
 *     - Quizzes (FOLDERS, DELETE, MERGE, MOVE, CREATE FOLDER)
 *     - Reports (ARCHIVE, UNARCHIVE, DELETE, RESTORE, DELETE FOREVER)
 *     - Final Results (REPORTS, EXPORT SCORES)
 */

import React from 'react';
import TitleTooltip from 'TitleTooltip';
import Tooltip from 'Tooltip';

let e = React.createElement;

export default class IconTextButton extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            showingTitle: false,
            showingTooltip: false
        };
    }
    
    render() {
        let buttonClass = 'icon-label-button';
        
        if (this.props.className) {
            buttonClass += ` ${this.props.className}`;
        }
        
        if (this.props.disabled) {
            buttonClass += ' icon-label-button-disabled';
        }
        
        let children = [
            e(this.props.icon, {
                className: 'icon-label-button-icon' + (this.props.iconClass ? (' ' + this.props.iconClass) : '')
            })
        ];
        
        if (this.props.label) {
            children.push(
                e('span', {
                    className: 'icon-label-button-label',
                    children: this.props.label
                })
            );
        }
        
        if (this.state.showingTooltip) {
            children.push(
                e(Tooltip, {
                    className: 'icon-label-tooltip',
                    location: 'above',
                    title: this.props.tooltipTitle,
                    text: this.props.tooltipText
                })
            );
        } else if (this.state.showingTitle) {
            children.push(
                e(TitleTooltip, {
                    className: 'icon-label-button-title-tooltip',
                    text: this.props.title
                })
            );
        }
        
        return e('button', {
            className: buttonClass,
            children: children,
            onClick: (event) => {
                let newState = {
                    showingTitle: false
                };
                
                if (this.props.disabled) {
                    if (this.props.tooltipText) {
                        newState.showingTooltip = !this.state.showingTooltip;
                    }
                }
                
                this.setState(newState);
                
                if (!this.props.disabled) {
                    this.props.onClick(event);
                }
            },
            onMouseEnter: () => {
                if (this.props.title && !this.state.showingTooltip && !this.state.showingTitle) {
                    this.setState({
                        showingTitle: true
                    });
                }
            },
            onMouseLeave: () => {
                if (this.state.showingTitle || this.state.showingTooltip) {
                    this.setState({
                        showingTitle: false,
                        showingTooltip: false
                    });
                }
            }
        });
    }
    
}
