/**
 * Socrative Menu Button - A custom button component that displays a menu on click.
 * 
 * A menu button is composed of:
 * 
 *     - A button element styled for primary actions in Socrative (orange and pill-shaped by default)
 *     - One or more menu items displayed below the button on click
 * 
 * Properties:
 * 
 *     buttonClass {string}  Optional CSS class applied to the button (overrides the orange, pill-shape style)
 *     className   {string}  CSS class applied to the entire component (usually for positioning or sizing)
 *     icon        {object}  Optional SVG icon, displayed to the left of the button's label
 *     label       {string}  The label for the button
 *     loading     {boolean} Whether the button should display the three-dot loader and be disabled
 *     menuItems   {array}   An array of menu item objects. Each object should have the following properties:
 *                               label    {string}   Label for this menu item
 *                               keyValue {string}   Unique value associated with this menu item, used as its React key
 *                               onClick  {function} Callback function executed when this menu item is clicked
 */

import React from 'react';

let e = React.createElement;

export default class MenuButton extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            showingMenu: false
        };
    }
    
    render() {
        let allChildren = [],
            buttonChildren = [];
        
        if (this.props.icon) {
            buttonChildren.push(this.props.icon);
        }
        
        buttonChildren.push(this.props.label);
        
        allChildren.push(
            e('button', {
                children: buttonChildren,
                className: this.props.buttonClass || 'button button-primary button-small pill',
                disabled: this.props.loading,
                onClick: () => {
                    if (!this.props.loading) {
                        this.setState({
                            showingMenu: !this.state.showingMenu
                        });
                    }
                }
            })
        );
        
        if (this.state.showingMenu) {
            let menuItems = [];
            
            for (let menuItem of this.props.menuItems) {
                menuItems.push(
                    e('span', {
                        className: 'menu-button-menu-item',
                        children: menuItem.label,
                        key: `menu-button-menu-item-${menuItem.keyValue}`,
                        onClick: (event) => {
                            menuItem.onClick(event);
                            this.setState({
                                showingMenu: false
                            });
                        }
                    })
                );
            }
            
            allChildren.push(
                e('div', {
                    className: 'menu-button-menu-items-container',
                    children: menuItems
                })
            );
        }
        
        return e('div', {
            className: 'button-menu-default' + (this.props.className ? ` ${this.props.className}` : ''),
            children: allChildren,
            onMouseLeave: () => {
                this.setState({
                    showingMenu: false
                });
            }
        });
    }
    
}
