/**
 * Socrative Activity Status - The wifi symbol used to indicate whether an activity is running in a room.
 * 
 * Properties:
 * 
 *     className {string}  CSS class applied to the entire component (usually for positioning or sizing)
 *     animated  {boolean} Whether the symbol should be animated (an activity is running)
 */

import React from 'react';

let e = React.createElement;

export default class ActivityStatus extends React.Component {
    
    render() {
        return e('div', {
            className: this.props.className,
            children: [
                e('div', {
                    className: this.props.animated ? 'animated-wifi-symbol' : 'wifi-symbol',
                    children: [
                        e('div', {
                            className: (this.props.animated ? 'animated-wifi-circle' : 'wifi-circle') + ' first'
                        }),
                        e('div', {
                            className: (this.props.animated ? 'animated-wifi-circle' : 'wifi-circle') + ' second'
                        }),
                        e('div', {
                            className: (this.props.animated ? 'animated-wifi-circle' : 'wifi-circle') + ' third'
                        }),
                        e('div', {
                            className: (this.props.animated ? 'animated-wifi-circle' : 'wifi-circle') + ' fourth'
                        })
                    ]
                })
            ]
        })
    }
    
}
