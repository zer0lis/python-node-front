import React from 'react';
import {translate} from 'translator';

let e = React.createElement;

export default class Loader extends React.Component {
    
    render() {
        let text = this.props.text || translate('loading...');
        
        return e('div', {
            className: 'loader-container' + (this.props.className ? (' ' + this.props.className) : ''),
            children: [
                e('span', {
                    className: 'loading-text',
                    children: text
                }),
                e('div', {className: 'dot1'}),
                e('div', {className: 'dot2'}),
                e('div', {className: 'dot3'})
            ]
        });
    }
    
}
