import React from 'react';

let e = React.createElement;

export default class SmallActivityStatus extends React.Component {
    
    render() {
        return e('div', {
            id: this.props.id,
            className: this.props.className,
            children: [
                e('div', {
                    className: this.props.animated ? 'animated-wifi-symbol' : 'wifi-symbol',
                    children: [
                        e('div', {
                            className: (this.props.animated ? 'animated-wifi-circle' : 'wifi-circle') + ' first'
                        }),
                        e('div', {
                            className: (this.props.animated ? 'animated-wifi-circle' : 'wifi-circle') + ' second'
                        })
                    ]
                })
            ]
        });
    }
    
}
