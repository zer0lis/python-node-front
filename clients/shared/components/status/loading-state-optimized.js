// Introduce the SOCRATIVE namespace to avoid global conflicts.
var SOCRATIVE = SOCRATIVE || {};

SOCRATIVE.initTemplateLoader = {
    start: function() {
        document.getElementById('body').insertAdjacentHTML('beforeend',
            '<div id="loader">'+
                '<div class="spinner">'+
                    '<div class="bounce1"></div>'+
                    '<div class="bounce2"></div>'+
                    '<div class="bounce3"></div>'+
                '</div>'+
                '<div id="loader-message">Loading.</div>'+
            '</div>');

        var i = 0;
        var loaderMessage = document.getElementById('loader-message');
        var changeMessage = function() {
            i++;
            if (i > 9)  loaderMessage.innerHTML = "Still working on it.";
            if (i > 16) loaderMessage.innerHTML = "We're making progress.";
            if (i > 24) loaderMessage.innerHTML = "Did you know? Socrative supports teachers in over 150 countries!";
            if (i > 34) loaderMessage.innerHTML = "Did you know? Socrative users have created over 6 million quizzes!";
        };
        this.intervalId = setInterval(changeMessage, 1000);
    },

    stop: function() {
        var loader = document.getElementById('loader');
        if (loader != null)
            loader.parentNode.removeChild(loader);
        clearInterval(this.intervalId);
    }
};

// Show the loading messages if the app is taking more than 3 seconds to load.
SOCRATIVE.checkTime = {
    start: function() {
        var ct = this;
        var checkTimer = function() {
            SOCRATIVE.initTemplateLoader.start();
            ct.reset();
        };
        this.intervalId = setInterval(checkTimer, 3000);
    },
    reset: function() {
        clearInterval(this.intervalId);
    }
};

SOCRATIVE.checkTime.start();
