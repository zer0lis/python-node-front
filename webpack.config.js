let ExtractTextPlugin = require('extract-text-webpack-plugin'),
    path = require('path'),
    webpack = require('webpack');

module.exports = {
    devtool: 'source-map',
    entry: {
        studentLogin: './clients/app/entry/studentLogin',
        teacherLogin: './clients/app/entry/teacherLogin',
        student: './clients/app/entry/student',
        teacher: './clients/app/entry/teacher'
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader'
            },
            {
                test:/\.less$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'less-loader']
                })
            },
            {
                test: /\.(png|gif|woff|woff2|eot|ttf|svg)$/,
                loader: 'file-loader'
            },
            {
                test: /backbone\.js$/,
                loader: 'imports-loader?define=>false'
            }
        ]
    },
    output: {
        chunkFilename: '[id].bundle.min.js',
        filename: '[name].bundle.min.js',
        path: path.resolve(__dirname, 'clients/app/assets/files/js'),
        publicPath: process.env.STATIC_URL_PATH ? process.env.STATIC_URL_PATH : '/files/js/'
        // For the value of publicPath, the push tool sets the STATIC_URL_PATH environment 
        // variable appropriately for dev or prod. For local development, the Node/Express 
        // server references assets from the /clients/app/assets directory.
    },
    plugins: [
        new ExtractTextPlugin({
            filename: '../css/[name].min.css' // Locally, this is appended to /clients/app/assets/files/js/
        }),
        new webpack.DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify('production')
        }),
        new webpack.IgnorePlugin(/^jquery$/),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'common'
        })
    ],
    resolve: {
        extensions: ['.js', '.less'],
        modules: [
            path.join(__dirname, 'clients/app/routers'),
            path.join(__dirname, 'clients/app/shared/footer'),
            path.join(__dirname, 'clients/app/shared/header'),
            path.join(__dirname, 'clients/app/shared/modal'),
            path.join(__dirname, 'clients/app/student/components'),
            path.join(__dirname, 'clients/app/student/header'),
            path.join(__dirname, 'clients/app/student/header/components'),
            path.join(__dirname, 'clients/app/student/login'),
            path.join(__dirname, 'clients/app/student/quiz/controllers'),
            path.join(__dirname, 'clients/app/student/quiz/models'),
            path.join(__dirname, 'clients/app/student/quiz/views'),
            path.join(__dirname, 'clients/app/teacher/banner'),
            path.join(__dirname, 'clients/app/teacher/edit-quiz'),
            path.join(__dirname, 'clients/app/teacher/edit-quiz/components'),
            path.join(__dirname, 'clients/app/teacher/header'),
            path.join(__dirname, 'clients/app/teacher/header/components'),
            path.join(__dirname, 'clients/app/teacher/launch/components'),
            path.join(__dirname, 'clients/app/teacher/launch/controllers'),
            path.join(__dirname, 'clients/app/teacher/launch/models'),
            path.join(__dirname, 'clients/app/teacher/launch/views'),
            path.join(__dirname, 'clients/app/teacher/license/views'),
            path.join(__dirname, 'clients/app/teacher/login/components'),
            path.join(__dirname, 'clients/app/teacher/login/controllers'),
            path.join(__dirname, 'clients/app/teacher/login/models'),
            path.join(__dirname, 'clients/app/teacher/login/views'),
            path.join(__dirname, 'clients/app/teacher/profile/components'),
            path.join(__dirname, 'clients/app/teacher/profile/controllers'),
            path.join(__dirname, 'clients/app/teacher/profile/models'),
            path.join(__dirname, 'clients/app/teacher/profile/views'),
            path.join(__dirname, 'clients/app/teacher/quizzes/controllers'),
            path.join(__dirname, 'clients/app/teacher/quizzes/models'),
            path.join(__dirname, 'clients/app/teacher/quizzes/views'),
            path.join(__dirname, 'clients/app/teacher/renewal'),
            path.join(__dirname, 'clients/app/teacher/reports/controllers'),
            path.join(__dirname, 'clients/app/teacher/reports/models'),
            path.join(__dirname, 'clients/app/teacher/reports/views'),
            path.join(__dirname, 'clients/app/teacher/results/controllers'),
            path.join(__dirname, 'clients/app/teacher/results/models'),
            path.join(__dirname, 'clients/app/teacher/results/views'),
            path.join(__dirname, 'clients/app/teacher/rooms/components'),
            path.join(__dirname, 'clients/app/teacher/rooms/controllers'),
            path.join(__dirname, 'clients/app/teacher/rooms/models'),
            path.join(__dirname, 'clients/app/teacher/rooms/views'),
            path.join(__dirname, 'clients/app/teacher/shared/controllers'),
            path.join(__dirname, 'clients/app/teacher/shared/views'),
            path.join(__dirname, 'clients/shared/components'),
            path.join(__dirname, 'clients/shared/components/activity'),
            path.join(__dirname, 'clients/shared/components/buttons'),
            path.join(__dirname, 'clients/shared/components/elements'),
            path.join(__dirname, 'clients/shared/components/folders'),
            path.join(__dirname, 'clients/shared/components/header'),
            path.join(__dirname, 'clients/shared/components/logos'),
            path.join(__dirname, 'clients/shared/components/menus'),
            path.join(__dirname, 'clients/shared/components/popup'),
            path.join(__dirname, 'clients/shared/components/status'),
            path.join(__dirname, 'clients/shared/components/tooltips'),
            path.join(__dirname, 'clients/shared/components/zoom'),
            path.join(__dirname, 'clients/shared/icons'),
            path.join(__dirname, 'shared'),
            path.join(__dirname, 'shared/models'),
            path.join(__dirname, 'shared/translations'),
            path.join(__dirname, 'node_modules')
        ]
    }
};
