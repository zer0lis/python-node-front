'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in default folders last updated:', error);
    }
};

exports.up = async (db) => {
    try {
        await db.changeColumn(
            'folders',
            'last_updated',
            {
                type: 'timestamp with time zone',
                notNull: true,
                defaultValue: new String('now()')
            }
        );
    } catch (error) {
        logError(error);
    }
};

exports.down = async (db) => {
    try {
        await db.changeColumn(
            'folders',
            'last_updated',
            {
                type: 'timestamp with time zone'
            }
        );
    } catch (error) {
        logError(error);
    }
};

exports._meta = {
    "version": 1
};
