'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in drop password and authtoken from admins migration:', error);
    }
};

exports.up = (db) => {
    return db.removeColumn('admins', 'password', logError).then(() => {
        db.removeColumn('admins', 'auth_token', logError);
    }, logError);
};

exports.down = (db) => {
    return null;
};

exports._meta = {
    "version": 1
};
