'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in prices migration:', error);
    }
};

exports.up = (db) => {
    return db.createTable('prices', {
        id: {
            type: 'integer',
            primaryKey: true,
            notNull: true,
            autoIncrement: true
        },
        amount: {
            type: 'integer',
            notNull: true
        },
        name: {
            type: 'character varying',
            length: 255,
            notNull: true,
            unique: true
        },
        seats: {
            type: 'integer',
            notNull: true,
            unique: true
        }
    }).then(() => {
        db.runSql('INSERT INTO prices (amount, name, seats) VALUES(?, ?, ?)', [6999, 'Base Price', 1], logError);
    }, logError);
};

exports.down = (db) => {
    return db.dropTable('prices');
};

exports._meta = {
    "version": 1
};
