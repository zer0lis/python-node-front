'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in drop allowed and removed from activations migration:', error);
    }
};

exports.up = (db) => {
    return db.removeColumn('license_activations', 'allowed', logError).then(() => {
        db.removeColumn('license_activations', 'removed', logError);
    }, logError);
};

exports.down = (db) => {
    return db.addColumn('license_activations', 'allowed', {type: 'boolean', notNull: true, defaultValue: true}, logError).then(() => {
        db.addColumn('license_activations', 'removed', {type: 'boolean', notNull: true, defaultValue: false}, logError).then(() => {
            db.addIndex('license_activations', 'license_activations_allowed', ['allowed'], true, logError);
            db.addIndex('license_activations', 'license_activations_removed', ['removed'], false, logError);
        }, logError);
    }, logError);
};

exports._meta = {
    "version": 1
};
