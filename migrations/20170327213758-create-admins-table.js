'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in create admins migration:', error);
    }
};

exports.up = (db) => {
    return db.createTable('admins', {
        id: {
            type: 'integer',
            primaryKey: true,
            notNull: true,
            autoIncrement: true
        },
        email: {
            type: 'character varying',
            length: 128,
            notNull: true,
            unique: true
        },
        password: {
            type: 'character varying',
            length: 128,
            notNull: true
        },
        auth_token: {
            type: 'character varying',
            length: 20,
            notNull: true,
            unique: true
        },
        last_name: {
            type: 'character varying',
            length: 32,
            notNull: true
        },
        first_name: {
            type: 'character varying',
            length: 32,
            notNull: true
        },
        valid: {
            type: 'boolean',
            notNull: true
        },
        last_login: {
            type: 'timestamp with time zone'
        },
        created_by_id: {
            type: 'integer',
            notNull: true
        },
        register_date: {
            type: 'timestamp with time zone',
            notNull: true
        }
    }).then(() => {
        db.runSql('CREATE INDEX admins_email_like ON admins (email varchar_pattern_ops)', [], logError);
        db.runSql('CREATE INDEX admins_auth_token_like ON admins (auth_token varchar_pattern_ops)', [], logError);
        db.runSql('ALTER SEQUENCE admins_id_seq RESTART WITH 50', [], logError); // The sequence name will change, so make sure we don't have duplicate IDs.
    }, logError);
};

exports.down = (db) => {
    return db.dropTable('admins');
};

exports._meta = {
    "version": 1
};
