'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in drop old stripe charge response column migration:', error);
    }
};

exports.up = async (db) => {
    try {
        await db.removeColumn('licenses', 'stripe_charge_response', logError);
    } catch (error) {
        logError(error);
    }
};

exports.down = (db) => {
    return null;
};

exports._meta = {
    "version": 1
};
