'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in rename coupon column migration:', error);
    }
};

exports.up = (db) => {
    return db.renameColumn('coupons', 'coupon', 'name').then(() => {
        db.runSql('ALTER TABLE coupons RENAME CONSTRAINT coupons_coupon_key TO coupons_name_key', [], logError);
        db.runSql('ALTER INDEX coupons_coupon_like RENAME TO coupons_name_like', [], logError);
        db.runSql('DROP INDEX coupons_coupon').then(() => {
            db.runSql('CREATE INDEX coupons_name ON coupons (LOWER(name))', [], logError);
        });
    }, logError);
};

exports.down = (db) => {
    return db.renameColumn('coupons', 'name', 'coupon').then(() => {
        db.runSql('ALTER TABLE coupons RENAME CONSTRAINT coupons_name_key TO coupons_coupon_key', [], logError);
        db.runSql('ALTER INDEX coupons_name_like RENAME TO coupons_coupon_like', [], logError);
        db.runSql('DROP INDEX coupons_name').then(() => {
            db.addIndex('coupons', 'coupons_coupon', ['coupon'], false, logError);
        });
    }, logError);
};

exports._meta = {
    "version": 1
};
