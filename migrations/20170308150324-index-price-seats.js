'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in index price seats migration:', error);
    }
};

exports.up = (db) => {
    return db.addIndex('prices', 'prices_seats', ['seats'], false, logError);
};

exports.down = (db) => {
    return db.removeIndex('prices', 'prices_seats', logError);
};

exports._meta = {
    "version": 1
};
