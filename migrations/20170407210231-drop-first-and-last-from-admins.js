'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in drop first and last from admins migration:', error);
    }
};

exports.up = (db) => {
    return db.removeColumn('admins', 'first_name', logError).then(() => {
        db.removeColumn('admins', 'last_name', logError);
    }, logError);
};

exports.down = (db) => {
    return null;
};

exports._meta = {
    "version": 1
};
