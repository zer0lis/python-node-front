'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in license_activations migration:', error);
    }
};

exports.up = function(db) {
    return db.createTable('license_activations', {
        id: {
            type: 'integer',
            primaryKey: true,
            notNull: true,
            autoIncrement: true
        },
        license_id: {
            type: 'integer',
            notNull: true
        },
        user_id: {
            type: 'integer',
            notNull: true
        },
        activation_date: {
            type: 'timestamp with time zone',
            notNull: true
        },
        allowed: {
            type: 'boolean',
            notNull: true
        },
        removed: {
            type: 'boolean',
            notNull: true
        }
    }).then(() => {
        db.addIndex('license_activations', 'license_activations_license_id', ['license_id'], false, logError);
        db.addIndex('license_activations', 'license_activations_user_id', ['user_id'], false, logError);
        db.addIndex('license_activations', 'license_activations_allowed', ['allowed'], false, logError);
        db.addIndex('license_activations', 'license_activations_removed', ['removed'], false, logError);
        db.runSql(
            `ALTER TABLE license_activations
             ADD CONSTRAINT license_id_refs_licenses_id
             FOREIGN KEY (license_id)
             REFERENCES licenses (id)
             DEFERRABLE INITIALLY DEFERRED`, [], logError
        );
        db.runSql(
            `ALTER TABLE license_activations
             ADD CONSTRAINT user_id_refs_socrative_users_socrativeuser_id
             FOREIGN KEY (user_id)
             REFERENCES socrative_users_socrativeuser (id)
             DEFERRABLE INITIALLY DEFERRED`, [], logError
        );
    }, logError);
};

exports.down = function(db) {
    return db.dropTable('license_activations');
};

exports._meta = {
    "version": 1
};
