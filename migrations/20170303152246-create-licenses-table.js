'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in licenses migration:', error);
    }
};

exports.up = (db) => {
    return db.createTable('licenses', {
        id: {
            type: 'integer',
            primaryKey: true,
            notNull: true,
            autoIncrement: true
        },
        buyer_id: {
            type: 'integer',
            notNull: true
        },
        key: {
            type: 'character',
            length: 29,
            notNull: true,
            unique: true
        },
        expiration_date: {
            type: 'timestamp with time zone',
            notNull: true
        },
        coupon_id: {
            type: 'integer',
            notNull: true
        },
        auto_renew: {
            type: 'boolean',
            notNull: true
        },
        years: {
            type: 'integer',
            notNull: true
        },
        price_per_seat: {
            type: 'integer',
            notNull: true
        },
        customer_id: {
            type: 'character varying',
            length: 255
        },
        stripe_charge_response: {
            type: 'text',
            notNull: true
        }
    }).then(() => {
        db.addIndex('licenses', 'licenses_buyer_id', ['buyer_id'], false, logError);
        db.addIndex('licenses', 'licenses_key', ['key'], false, logError);
        db.addIndex('licenses', 'licenses_coupon_id', ['coupon_id'], false, logError);
        db.runSql('CREATE INDEX licenses_key_like ON licenses (key bpchar_pattern_ops)', [], logError);
        db.runSql(
            `ALTER TABLE licenses
             ADD CONSTRAINT buyer_id_refs_socrative_users_socrativeuser_id
             FOREIGN KEY (buyer_id)
             REFERENCES socrative_users_socrativeuser (id)
             DEFERRABLE INITIALLY DEFERRED`, [], logError
        );
        db.runSql(
            `ALTER TABLE licenses
             ADD CONSTRAINT coupon_id_refs_coupons_id
             FOREIGN KEY (coupon_id)
             REFERENCES coupons (id)
             DEFERRABLE INITIALLY DEFERRED`, [], logError
        );
    }, logError);
};

exports.down = (db) => {
    return db.dropTable('licenses');
};

exports._meta = {
    "version": 1
};
