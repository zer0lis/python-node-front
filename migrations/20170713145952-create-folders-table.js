'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in create folders migration:', error);
    }
};

exports.up = async (db) => {
    try {
        await db.createTable('folders', {
            id: {
                type: 'integer',
                primaryKey: true,
                notNull: true,
                autoIncrement: true
            },
            name: {
                type: 'character varying',
                length: 255,
                notNull: true
            },
            parent_id: {
                type: 'integer'
            },
            deleted: {
                type: 'boolean',
                notNull: true,
                defaultValue: false
            },
            hidden: {
                type: 'boolean',
                notNull: true,
                defaultValue: false
            },
            type: {
                type: 'integer',
                notNull: true
            },
            user_id: {
                type: 'integer',
                notNull: true
            },
            created_date: {
                type: 'timestamp with time zone',
                notNull: true,
                defaultValue: new String('now()')
            },
            last_updated: {
                type: 'timestamp with time zone'
            },
            hidden_date: {
                type: 'timestamp with time zone'
            }
        });
        
        await db.addIndex('folders', 'folders_type', ['type'], false, logError);
        await db.addIndex('folders', 'folders_hidden', ['hidden'], false, logError);
        
        await db.runSql(
            `ALTER TABLE folders
             ADD CONSTRAINT parent_id_refs_folders_id
             FOREIGN KEY (parent_id)
             REFERENCES folders (id)
             DEFERRABLE INITIALLY DEFERRED`, [], logError
        );
        
        await db.runSql(
            `ALTER TABLE folders
             ADD CONSTRAINT user_id_refs_socrative_users_socrativeuser_id
             FOREIGN KEY (user_id)
             REFERENCES socrative_users_socrativeuser (id)
             DEFERRABLE INITIALLY DEFERRED`, [], logError
        );
    } catch (error) {
        logError(error);
    }
};

exports.down = (db) => {
    return db.dropTable('folders');
};

exports._meta = {
    "version": 1
};
