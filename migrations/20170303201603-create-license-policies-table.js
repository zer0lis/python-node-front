'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in license_policies migration:', error);
    }
};

exports.up = function(db) {
    return db.createTable('license_policies', {
        id: {
            type: 'integer',
            primaryKey: true,
            notNull: true,
            autoIncrement: true
        },
        license_id: {
            type: 'integer',
            notNull: true
        },
        type: {
            type: 'integer',
            notNull: true
        },
        pattern: {
            type: 'character varying',
            length: 255,
            notNull: true
        }
    }).then(() => {
        db.addIndex('license_policies', 'license_policies_license_id', ['license_id'], false, logError);
        db.runSql(
            `ALTER TABLE license_policies
             ADD CONSTRAINT license_id_refs_licenses_id
             FOREIGN KEY (license_id)
             REFERENCES licenses (id)
             DEFERRABLE INITIALLY DEFERRED`, [], logError
        );
    }, logError);
};

exports.down = function(db) {
    return db.dropTable('license_policies');
};

exports._meta = {
    "version": 1
};
