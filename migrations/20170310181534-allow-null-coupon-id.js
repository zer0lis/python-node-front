'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in allow null coupon id migration:', error);
    }
};

exports.up = (db) => {
    return db.changeColumn('licenses', 'coupon_id', {
        type: 'integer',
        foreignKey: {
            name: 'coupon_id_refs_coupons_id',
            table: 'coupons',
            mapping: {
                coupon_id: 'id'
            },
            rules: {}
        }
    }, logError);
};

exports.down = (db) => {
    return db.changeColumn('licenses', 'coupon_id', {
        type: 'integer',
        notNull: true,
        foreignKey: {
            name: 'coupon_id_refs_coupons_id',
            table: 'coupons',
            mapping: {
                coupon_id: 'id'
            },
            rules: {}
        }
    }, logError);
};

exports._meta = {
    "version": 1
};
