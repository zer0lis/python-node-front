'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in create renewal prices migration:', error);
    }
};

exports.up = async (db) => {
    try {
        await db.createTable('renewal_prices', {
            id: {
                type: 'integer',
                primaryKey: true,
                notNull: true,
                autoIncrement: true
            },
            organization_type: {
                type: 'character varying',
                length: 4
            },
            amount: {
                type: 'integer',
                notNull: true
            }
        });
        
        await db.runSql(
            `INSERT INTO renewal_prices (organization_type, amount)
             VALUES (?, ?), (?, ?), (?, ?), (?, ?), (?, ?)`,
            ['K12', 3499, 'USHE', 5499, 'CORP', 5499, 'OTHR', 5499, null, 5499],
            logError
        );
    } catch (error) {
        logError(error);
    }
};

exports.down = (db) => {
    return db.dropTable('renewal_prices');
};

exports._meta = {
    "version": 1
};
