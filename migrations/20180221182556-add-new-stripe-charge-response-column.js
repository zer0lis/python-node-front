'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in add new stripe charge response column migration:', error);
    }
};

exports.up = async (db) => {
    try {
        await db.addColumn(
            'license_transactions',
            'stripe_charge_response',
            {
                type: 'text'
            },
            logError
        );
    } catch (error) {
        logError(error);
    }
};

exports.down = async (db) => {
    try {
        await db.removeColumn('license_transactions', 'stripe_charge_response', logError);
    } catch (error) {
        logError(error);
    }
};

exports._meta = {
    "version": 1
};
