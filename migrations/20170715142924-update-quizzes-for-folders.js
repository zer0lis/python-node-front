'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in update quizzes for folders migration:', error);
    }
};

exports.up = async (db) => {
    try {
        await db.addColumn(
            'quizzes_quiz',
            'parent_id',
            {
                type: 'integer'
            },
            logError
        );
    
        await db.addColumn(
            'quizzes_quiz',
            'deleted',
            {
                type: 'boolean',
                notNull: true,
                defaultValue: false
            },
            logError
        );
    
        await db.addColumn(
            'quizzes_quiz',
            'hidden_date',
            {
                type: 'timestamp with time zone'
            },
            logError
        );
    
        await db.runSql(
            `ALTER TABLE quizzes_quiz
             ADD CONSTRAINT quizzes_parent_id_refs_folders_id
             FOREIGN KEY (parent_id)
             REFERENCES folders (id)
             DEFERRABLE INITIALLY DEFERRED`, [], logError
        );
    } catch (error) {
        logError(error);
    }
};

exports.down = async (db) => {
    try {
        await db.removeColumn('quizzes_quiz', 'parent_id', logError);
        await db.removeColumn('quizzes_quiz', 'deleted', logError);
        await db.removeColumn('quizzes_quiz', 'hidden_date', logError);
    } catch (error) {
        logError(error);
    }
};

exports._meta = {
    "version": 1
};
