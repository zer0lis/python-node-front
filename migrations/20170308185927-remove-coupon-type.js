'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in remove coupon type migration:', error);
    }
};

exports.up = (db) => {
    return db.removeColumn('coupons', 'type', logError);
};

exports.down = (db) => {
    return db.addColumn('coupons', 'type', {type: 'integer', notNull: true}, logError);
};

exports._meta = {
    "version": 1
};
