'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in license_transactions migration:', error);
    }
};

exports.up = function(db) {
    return db.createTable('license_transactions', {
        id: {
            type: 'integer',
            primaryKey: true,
            notNull: true,
            autoIncrement: true
        },
        license_id: {
            type: 'integer',
            notNull: true
        },
        seats: {
            type: 'integer',
            notNull: true
        },
        price: {
            type: 'integer',
            notNull: true
        },
        purchase_date: {
            type: 'timestamp with time zone',
            notNull: true
        }
    }).then(() => {
        db.addIndex('license_transactions', 'license_transactions_license_id', ['license_id'], false, logError);
        db.runSql(
            `ALTER TABLE license_transactions
             ADD CONSTRAINT license_id_refs_licenses_id
             FOREIGN KEY (license_id)
             REFERENCES licenses (id)
             DEFERRABLE INITIALLY DEFERRED`, [], logError
        );
    }, logError);
};

exports.down = function(db) {
    return db.dropTable('license_transactions');
};

exports._meta = {
    "version": 1
};
