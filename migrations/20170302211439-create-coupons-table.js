'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in coupons migration:', error);
    }
};

exports.up = (db) => {
    return db.createTable('coupons', {
        id: {
            type: 'integer',
            primaryKey: true,
            notNull: true,
            autoIncrement: true
        },
        coupon: {
            type: 'character varying',
            length: 64,
            notNull: true,
            unique: true
        },
        type: {
            type: 'integer',
            notNull: true
        },
        amount: {
            type: 'integer',
            notNull: true
        },
        allow_bulk: {
            type: 'boolean',
            notNull: true
        },
        expiration_date: {
            type: 'timestamp with time zone',
            notNull: true
        }
    }).then(() => {
        db.addIndex('coupons', 'coupons_coupon', ['coupon'], false, logError);
        db.runSql('CREATE INDEX coupons_coupon_like ON coupons (coupon varchar_pattern_ops)', [], logError);
    }, logError);
};

exports.down = (db) => {
    return db.dropTable('coupons');
};

exports._meta = {
    "version": 1
};
