'use strict';

let logError = (error) => {
    if (error) {
        console.log('Error in create admin roles migration:', error);
    }
};

exports.up = (db) => {
    return db.createTable('admin_roles', {
        id: {
            type: 'integer',
            primaryKey: true,
            notNull: true,
            autoIncrement: true
        },
        level: {
            type: 'integer',
            notNull: true
        },
        user_id: {
            type: 'integer',
            notNull: true
        }
    }).then(() => {
        db.addIndex('admin_roles', 'admin_roles_user_id', ['user_id'], false, logError);
        db.runSql('ALTER SEQUENCE admin_roles_id_seq RESTART WITH 50', [], logError); // The sequence name will change, so make sure we don't have duplicate IDs.
        db.runSql(
            `ALTER TABLE admin_roles
             ADD CONSTRAINT user_id_refs_admins_id
             FOREIGN KEY (user_id)
             REFERENCES admins (id)
             DEFERRABLE INITIALLY DEFERRED`, [], logError
        );
    }, logError);
};

exports.down = (db) => {
    return db.dropTable('admin_roles');
};

exports._meta = {
    "version": 1
};
