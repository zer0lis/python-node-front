'use strict';

let Constants = require('../shared/Constants');

class Validations {
    
    /**
     * Check whether an uploaded file matches any of the known Excel mime types.
     * @param {object} file The uploaded Excel file.
     * @returns {boolean}
     */
    isValidExcelFile(file) {
        return [
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
            'application/vnd.ms-excel',
            'application/octet-stream'
        ].indexOf(file.mimetype) !== -1;
    }
    
    isValidEmail(email) {
        return /@.*?\./.test(email) && email.length <= Constants.MAX_STRING_LENGTH_255;
    }
    
    isValidParentId(parentId) {
        return parentId === null || (/^\d+$/.test(parentId) && parentId > 0);
    }
    
    /**
     * Check whether a new password is valid. This cannot be used for login validation because early
     * versions of Socrative did not enforce password length. This can, however, be used for account
     * creation and password reset.
     * @param password The password to validate.
     * @returns {boolean}
     */
    isValidPassword(password) {
        return password && password.length >= 8;
    }
    
}

module.exports = new Validations();
