module.exports = Object.freeze({
    
    /*-------------------------*/
    /*    Account Constants    */
    /*-------------------------*/
    MAX_STRING_LENGTH_128: 128,
    MAX_STRING_LENGTH_255: 255,
    MIN_PASSWORD_LENGTH: 8,
    ORGANIZATION_CORP: 'CORP',
    ORGANIZATION_HIGHER_ED: 'USHE',
    ORGANIZATION_K12: 'K12',
    ORGANIZATION_OTHER: 'OTHR',
    ROLE_ADMINISTRATOR: 'administrator',
    ROLE_IT_TECHNOLOGY: 'it-technology',
    ROLE_OTHER: 'other',
    ROLE_TEACHER: 'teacher',
    SCHOOL_NOT_LISTED: -1,
    STATUS_COMPLETED: 'completed',
    STATUS_IDLE: 'idle',
    STATUS_SELECTED: 'selected',
    
    
    /*----------------------*/
    /*    Activity Types    */
    /*----------------------*/
    QUIZ:       'QZ',
    SPACE_RACE: 'SR',
    NO_REPORT:  'NR', // The type code for QQ-TF and QQ-MC activities, because we don't provide reports for them.
    QQ_SA:      '1Q',
    VOTE:       '1V',
    
    
    /*--------------------*/
    /*    Admin Levels    */
    /*--------------------*/
    EDITOR_USER: 64,
    ADMIN_USER: 128,
    SUPREME_USER: 256,
    
    
    /*------------------------*/
    /*    Banner Constants    */
    /*------------------------*/
    BANNER_ACTION_GO_PRO: 'goProPopup',
    BANNER_ACTION_EXTERNAL_URL: 'externalResource',
    BANNER_ACTION_NONE: 'none',
    BANNER_AUDIENCE_ALL: 0,
    BANNER_AUDIENCE_FREE: 1,
    BANNER_AUDIENCE_PRO: 2,
    BANNER_REMIND_LATER: 64,
    BANNER_SHOW: 1,
    BANNER_STATUS_ACTIVE: 0,
    BANNER_STATUS_HIDDEN: 64,
    BANNER_TIMEOUT: 2000,
    BANNER_TYPE_ERROR: 1,
    BANNER_TYPE_SUCCESS: 2,
    
    
    /*------------------------*/
    /*    Cache Namespaces    */
    /*------------------------*/
    FOLDERS_NAMESPACE: 'folders',
    USER_NAMESPACE: 'user',
    
    
    /*----------------------*/
    /*    Cache Timeouts    */
    /*----------------------*/
    TEACHER_CACHE_TIMEOUT: 1209600,
    
    
    /*--------------*/
    /*    Colors    */
    /*--------------*/
    CLEAR_INPUT_X: '#cbcbcb',
    RENEWAL_COLOR: '#44a5ff',
    PRO_BADGE_COLOR: '#F89E1B',
    
    
    /*--------------------*/
    /*    Cookie Names    */
    /*--------------------*/
    AUTH_COOKIE_NAME: 'ua',
    LANGUAGE_COOKIE_NAME: 'socrative_lang',
    
    
    /*-------------------*/
    /*    Email Types    */
    /*-------------------*/
    ACCOUNTING_EMAIL: 'accounting',
    FORGOT_PASSWORD: 'forgot_password',
    LAST_LOGINS: 'logins',
    MULTI_SEATS_RECEIPT: 'multi_seats_receipt',
    PRO_EXPIRING: 'pro_expiring',
    RECEIPT: 'receipt',
    REGISTRATION_EMAIL: 'registration',
    UPGRADE_PRO_EMAIL: 'upgrade_pro',
    
    
    /*-----------------------------*/
    /*    Environment Hostnames    */
    /*-----------------------------*/
    PRODUCTION_HOSTNAME: 'b.socrative.com',
    UPLOADS_HOSTNAME: 'https://uploads.socrative.com/',


    /*-------------------*/
    /*    Error Codes    */
    /*-------------------*/
    UNKNOWN_ERROR: -1,
    INVALID_EMAIL: 5,
    INVALID_CHANGE_PASSWORD_TOKEN: 14,
    INVALID_FILE_TYPE: 26,
    INVALID_QUESTION_TYPE: 27,
    INVALID_PASSWORD: 40,
    MISSING_LICENSE_DATA: 100,
    INVALID_COUPON: 101,
    EXPIRED_COUPON: 102,
    STRIPE_ERROR: 103,
    PRICE_MISMATCH: 104,
    TOO_MANY_LICENSE_KEY_COLLISIONS: 105,
    INVALID_LICENSE_KEY: 106,
    EXPIRED_LICENSE_KEY: 107,
    USERS_ALREADY_LICENSED: 108,
    ACTIVATION_BLOCKED_BY_POLICY: 109,
    NOT_ENOUGH_SEATS: 110,
    NOT_UNIQUE_NAME: 111,
    NOT_UNIQUE_SEATS: 112,
    BASE_PRICE_SEATS: 113,
    DATABASE_ERROR: 114,
    INVALID_ID: 115,
    COUPON_IN_USE: 116,
    INVALID_FOLDER_DATA: 117,
    INVALID_QUIZ_DATA: 118,
    TOO_MANY_EXCEL_ROWS: 119,
    QUIZ_NAME_TOO_LONG: 120,
    QUESTION_TEXT_MISSING: 121,
    QUIZ_ANSWER_TOO_LONG: 122,
    NOT_ENOUGH_ANSWERS: 123,
    QUIZ_QUESTION_TOO_LONG: 124,
    QUESTIONS_MISSING: 125,
    FOLDER_CANNOT_BE_PURGED: 126,
    FOLDER_NOT_FOUND: 127,
    INVALID_UPDATE_FOLDERS_DATA: 128,
    INVALID_MERGE_QUIZ_DATA: 129,
    TOO_MANY_QUESTIONS_TO_BE_MERGED: 130,
    INVALID_ACCOUNT_DATA: 131,
    QUESTION_TYPE_MISSING: 201,
    AUTH_TOKEN_MISSING: 203,
    EMAIL_MISSING: 204,
    PASSWORD_MISSING: 220,
    QUIZ_NAME_MISSING: 245,
    QUIZ_NOT_FOUND: 400,
    USER_NOT_FOUND: 402,
    STUDENT_NOT_FOUND: 412,
    PERMISSION_DENIED: 600,
    ROOM_ALREADY_EXISTS: 804,
    LOGIN_FAILED: 805,
    ACCOUNT_SUSPENDED: 806,
    FILE_UPLOAD_ERROR: 809,
    INTERNAL_SERVER_ERROR: 809,
    TEMPORARY_TOKEN_EXPIRED: 814,
    ACTIVITY_RUNNING: 819,
    STRING_IS_TOO_LONG: 821,
    FILE_CORRUPT: 837,
    ROSTER_CONTENT_NOT_AVAILABLE: 837,
    TOO_MANY_QUESTIONS: 839,
    FILE_TOO_LARGE: 840,
    ROSTER_FILE_NOT_IMPORTABLE: 841,
    ROOM_ALREADY_TAKEN: 844, // This and ROOM_ALREADY_EXISTS (804) are effective duplicates of each other.
    STUDENT_ALREADY_LOGGED_IN: 845,
    EMPTY_ROSTER_FILE: 846,
    TOO_MANY_STUDENTS: 848,
    IP_BLOCKED_FOR_ONE_DAY: 851,
    ACCOUNT_BLOCKED_FOR_FIVE_MINUTES: 852,
    ACCOUNT_BLOCKED_FOR_ONE_HOUR: 853,
    ACCOUNT_BLOCKED_FOR_ONE_DAY: 854,
    FEATURE_NOT_AVAILABLE: 857,
    
    
    /*----------------------*/
    /*    Error Messages    */
    /*----------------------*/
    ACTIVATION_BLOCKED_BY_POLICY_MESSAGE: 'All activations blocked by policy. Please notify a Socrative devloper immediately!',
    FILE_UPLOAD_PROBLEM_MESSAGE: 'File upload problem',
    INTERNAL_SERVER_ERROR_MESSAGE: 'Internal server error',
    INVALID_ADMIN_EMAIL_MESSAGE: 'Admin emails must be in the masteryconnect.com domain',
    INVALID_ADMIN_ID_MESSAGE: 'Invalid admin id',
    INVALID_AMOUNT_MESSAGE: 'Invalid amount',
    INVALID_BANNER_ACTION_MESSAGE: 'Invalid banner action',
    INVALID_BANNER_AUDIENCE_MESSAGE: 'Invalid banner audience',
    INVALID_BANNER_CONTENT_MESSAGE: 'Invalid banner content',
    INVALID_BANNER_ENVIRONMENT_MESSAGE: 'Invalid banner environment',
    INVALID_BANNER_ID_MESSAGE: 'Invalid banner id',
    INVALID_BANNER_TITLE_MESSAGE: 'Invalid banner title',
    INVALID_BANNER_URL_MESSAGE: 'Invalid banner URL',
    INVALID_BUYER_MESSAGE: 'Invalid buyer',
    INVALID_EMAIL_ADDRESS_MESSAGE: 'Invalid email address',
    INVALID_EXPIRATION_DATE_MESSAGE: 'Invalid expiration date',
    INVALID_QUIZ_ID_MESSAGE: 'Invalid quiz id',
    INVALID_ROLE_MESSAGE: 'Invalid role',
    INVALID_ROOM_MESSAGE: 'Invalid room name',
    INVALID_SEATS_MESSAGE: 'Invalid number of seats',
    INVALID_SEND_EMAILS_MESSAGE: 'Invalid send emails value',
    INVALID_EMAILS_MESSAGE: 'At least one email from the list is not valid',
    INVALID_YEARS_MESSAGE: 'Invalid number of years',
    PERMISION_DENIED_MESSAGE: 'Permission denied',
    ROOM_NOT_IN_USE_MESSAGE: 'Room not in use',
    TOO_MANY_ROOMS_MESSAGE: 'Too many rooms to merge',
    USERS_ALREADY_PRO_MESSAGE: 'Users already PRO',
    USER_NOT_FOUND_MESSAGE: 'User not found',
    INVALID_COUPON_ALLOW_BULK: 'Invalid allow bulk',
    INVALID_COUPON_ID: 'Invalid coupon id',
    INVALID_PRICE_ID: 'Invalid price id',
    INVALID_COUPON_NAME: 'Invalid coupon name',
    INVALID_PRICE_COMPONENT_NAME: 'Invalid name',
    INVALID_PRICE_COMPONENT_SEATS: 'invalid seats',
    INVALID_PRICE_ID_MESSAGE: 'Invalid price id',
    INVALID_COUPON_EXPIRATION_DATE: 'Invalid coupon expiration date',
    INVALID_START_DATE: 'Invalid start date',
    INVALID_END_DATE: 'Invalid end date',

    
    /*------------------------*/
    /*    Folder Constants    */
    /*------------------------*/
    NO_FOLDER: 0,
    ROOT_FOLDER: null,
    TRASH: -1,
    QUIZ_FOLDER: 1,
    REPORT_FOLDER: 2,
    MAX_FOLDER_TREE_DEPTH: 6,
    
    
    /*------------------------*/
    /*   HTTP Status Codes    */
    /*------------------------*/
    HTTP_200_OK: 200,
    HTTP_201_CREATED: 201,
    HTTP_204_NO_CONTENT: 204,
    HTTP_302_REDIRECT: 302,
    HTTP_400_BAD_REQUEST: 400,
    HTTP_403_FORBIDDEN: 403,
    HTTP_404_NOT_FOUND: 404,
    HTTP_500_INTERNAL_SERVER_ERROR: 500,
    
    
    /*--------------------------*/
    /*    License Expiration    */
    /*--------------------------*/
    GRACE_PERIOD_DAYS: 30,
    RENEWAL_NOTICE_DAYS: 30,
    
    
    /*------------------------*/
    /*    License Policies    */
    /*------------------------*/
    ALLOW_POLICY: 1,
    BLOCK_POLICY: 2,
    
    
    /*---------------------*/
    /*    License Seats    */
    /*---------------------*/
    BULK_SEATS_THRESHOLD: 25,
    MAX_DISPLAY_SEATS: 999,
    
    
    /*---------------------*/
    /*    License Steps    */
    /*---------------------*/
    LICENSE_STEP: 1,
    PAYMENT_STEP: 2,
    COMPLETE_STEP: 3,
    
    
    /*-----------------------*/
    /*    Messaging Types    */
    /*-----------------------*/
    ACTIVITY_CHANGE: 'localActivity',
    HAND_RAISE: 'handraise',
    LOG_STUDENT_OUT: 'student',
    PRESENCE: 'presence',
    STUDENT_FINISHED: 'studentFinished',
    STUDENT_NAME_SET: 'studentNameSet',
    STUDENT_RESPONSE: 'studentResponse',
    STUDENT_STARTED_QUIZ: 'studentStartedQuiz',
    
    
    /*------------------*/
    /*    Move Tasks    */
    /*------------------*/
    DELETE_TASK: 1,
    MOVE_TASK: 2,
    RESTORE_TASK: 3,


    /*--------------------------*/
    /*    Password Constants    */
    /*--------------------------*/
    PASSWORD_ALGORITHM: 'pbkdf2',
    PASSWORD_DIGEST: 'sha256',
    PASSWORD_ITERATIONS: 12000,
    PASSWORD_KEY_LENGTH: 32,
    PASSWORD_TOKEN_LENGTH: 32,
    
    
    /*------------------------*/
    /*    Pubnub Constants    */
    /*------------------------*/
    MAX_PUBNUB_MESSAGE_LENGTH: 4096,
    
    
    /*----------------------*/
    /*    Quiz Constants    */
    /*----------------------*/
    EXCEL_MULTIPLE_CHOICE_QUESTION_TYPE: 'Multiple choice',
    EXCEL_OPEN_ENDED_QUESTION_TYPE: 'Open-ended',
    EXCEL_QUIZ_FIRST_QUESTION_ROW: 7,
    EXCEL_QUIZ_MAX_QUESTIONS: 100,
    EXCEL_QUIZ_NAME_CELL: 'B3',
    EXCEL_TYPE: 2,
    FREE_RESPONSE_QUESTION: 'FR',
    LEGACY_ID_LIMIT: 5000000,
    MAX_ANSWER_LENGTH: 10240,
    MAX_QUESTION_LENGTH: 32767, // This is the actual Microsoft limit for characters in a single cell (https://goo.gl/oaYqpD).
    MAX_QUIZ_NAME_LENGTH: 128,
    MULTIPLE_CHOICE_QUESTION: 'MC',
    SOC_TYPE: 1,
    MAX_NUMBER_OF_QUESTIONS: 1000,

    
    /*------------------------------*/
    /*    Registration Constants    */
    /*------------------------------*/
    REGISTRATION_STEP_COMPLETED: 'completed',
    REGISTRATION_STEP_IDLE: 'idle',
    REGISTRATION_STEP_SELECTED: 'selected',
    
    
    /*-------------------------*/
    /*    Renewal Constants    */
    /*-------------------------*/
    PRO_ENDING: 0,
    PRO_ENDED: 1,
    
    
    /*-------------------------*/
    /*    Reports Constants    */
    /*-------------------------*/
    REPORTS_SEARCH_LIMIT: 50,
    
    
    /*----------------------*/
    /*    Room Constants    */
    /*----------------------*/
    DEFAULT_ROOM: 1,
    IN_MENU: 2,
    LAST_USED: 8,
    MAX_PRO_ROOMS: 10,
    MAX_ROOM_NAME_LENGTH: 32,
    ARCHIVED_ROOM: 128,
    EXPIRED_ROOM: 256,
    NO_ROOM_STATUS_CHANGE: 0b111111111,
    
    
    /*-------------------*/
    /*    Roster File    */
    /*-------------------*/
    // Import and Download Column Order
    LAST_FIRST_ID: 1,
    FIRST_LAST_ID: 2,
    
    // Import and Download Type
    CSV: 1,
    XLSX: 2,
    
    
    /*--------------------*/
    /*    Score Export    */
    /*--------------------*/
    // Next Line
    ENTER: 1,
    ENTER_TWICE: 2,
    TAB: 3,
    TAB_TWICE: 4,
    DOWN_ARROW: 5,
    
    // Speed
    FAST: 50,
    MEDIUM: 150,
    SLOW: 300,
    VERY_SLOW: 500,
    
    // Export Method
    NUMERIC: 1,
    PERCENTAGE: 2,
    
    
    /*--------------------------*/
    /*    Screen Breakpoints    */
    /*--------------------------*/
    BREAKPOINT_PHONE_SMALL: 320,
    BREAKPOINT_PHONE_SMALL_LANDSCAPE: 568,
    BREAKPOINT_PHONE_MEDIUM: 375,
    BREAKPOINT_PHONE_LARGE:  414,
    BREAKPOINT_TABLET: 768,
    BREAKPOINT_DESKTOP_SMALL: 960,
    BREAKPOINT_DESKTOP_LARGE: 1220,
    
    
    /*--------------------------------*/
    /*    Stripe Credit Card Types    */
    /*--------------------------------*/
    AMERICAN_EXPRESS: 'American Express',
    DISCOVER: 'Discover',
    MASTERCARD: 'MasterCard',
    VISA: 'Visa',
    
    
    /*-------------------------*/
    /*    Student Constants    */
    /*-------------------------*/
    MAX_STUDENT_ID_LENGTH: 32,
    MAX_STUDENT_NAME_LENGTH: 64,
    
    
    /*---------------------------*/
    /*    Teacher Header Tabs    */
    /*---------------------------*/
    LAUNCH_TAB: 'launch',
    QUIZZES_TAB: 'quizzes',
    ROOMS_TAB: 'rooms',
    REPORTS_TAB: 'reports',
    RESULTS_TAB: 'results',
    
    
    /*-----------------------*/
    /*    User Type Codes    */
    /*-----------------------*/
    FREE_USER: 0,
    PRO_USER: 1,
    
    
    /*--------------------*/
    /*    Worker Tasks    */
    /*--------------------*/
    SEND_EMAIL_TASK: 'email',


    /*--------------------*/
    /*    Partner Enum    */
    /*--------------------*/
    GOOGLE_PARTNER: 'GO',
    MASTERY_CONNECT_PARTNER: 'MC',
    
    /*------------------------*/
    /*    google login errors */
    /*------------------------*/
    BAD_ACCESS_TOKEN: 'bad_access_token',
    GOOGLE_LOGIN_ERROR: 'google_login_error',
    ZENDESK_SSO_STATE: 'zendesk-sso'
});
