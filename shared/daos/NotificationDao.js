'use strict';

let BaseDao = require('./BaseDao'),
    pgFormat = require('pg-format');

class NotificationDao extends BaseDao {

    async resetNotifications(userId, notificationTypes) {
        let result = await this.executeQuery(pgFormat(
            `UPDATE notifications
             SET show=TRUE
             WHERE user_id=%L
             AND "type" IN (%L)
             AND is_hidden is FALSE`,
            userId, notificationTypes
        ));

        return result && result.command === 'UPDATE';
    }
}

module.exports = new NotificationDao();