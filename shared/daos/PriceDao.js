'use strict';

let BaseDao = require('./BaseDao'),
    Constants = require('../Constants'),
    logger = require('../Logger');

class PriceDao extends BaseDao {

    async selectPrices() {
        let result = await this.executeReadQuery(
            `SELECT *
             FROM prices
             ORDER BY seats ASC`
        );
        
        return result ? result.rows : [];
    }

    async insertPrice(price) {
        let result = null,
            client = await this.getClient();

        try {    
            result = await client.query(
                `INSERT INTO prices (amount, name, seats)
                 VALUES ($1, $2, $3) 
                 RETURNING id`, 
                 [price.amount, price.name, price.seats]
            );
        } catch (error) {
            if (error.constraint === 'prices_name_key') {
                return {error: Constants.NOT_UNIQUE_NAME};
            }

            if (error.constraint === 'prices_seats_key') {
                return {error: Constants.NOT_UNIQUE_SEATS};
            }
        } finally {
            client.release();
        }
        
        return result ? result.rows[0] : null;
    }

    async updatePrice(price) {
        let client = await this.getClient();
        
        try {
            await client.query('BEGIN');

            let result = await client.query(
                `UPDATE prices 
                 SET amount = $1, name = $2, seats = (
                     SELECT coalesce (max(seats), $3)
                     FROM prices
                     WHERE id = $4
                     AND seats = 1
                 )
                 WHERE id = $4 
                 RETURNING seats`, 
                 [price.amount, price.name, price.seats, price.id]
            );

            await client.query('COMMIT');

            if (result.rows[0].seats === price.seats) {
                return {error: false};
            } else {
                return {error: Constants.BASE_PRICE_SEATS};
            }
        } catch (error) {
            logger.error('An error occurred during the price update query:', error);
            try {
                await client.query('ROLLBACK');
            } catch (rollbackError) {
                logger.error('An error occurred rolling back price update query:', rollbackError);
            }

            if (error.constraint === 'prices_name_key') {
                return {error: Constants.NOT_UNIQUE_NAME};
            }

            if (error.constraint === 'prices_seats_key') {
                return {error: Constants.NOT_UNIQUE_SEATS};
            }
            
            return {error: Constants.DATABASE_ERROR};
        } finally {
            client.release();
        }
    }

    async deletePrice(id) {
        let result = await this.executeQuery(
            `DELETE
             FROM prices
             WHERE id = $1`,
             [id]
        );
        
        if (!result) {
            return {error: Constants.DATABASE_ERROR};
        } else if (result.rowCount === 0) {
            return {error: Constants.INVALID_ID};
        }
        
        return {error: false};
    }
    
    async selectRenewalPrices() {
        let result = await this.executeReadQuery(
            `SELECT *
             FROM renewal_prices
             ORDER BY id ASC`,
             [],
             true
        );
        
        return result ? result.rows : [];
    }
    
    async updateRenewalPrice(renewalPriceModel) {
        let result = await this.executeQuery(
            `UPDATE renewal_prices
             SET amount = $1
             WHERE id = $2
             RETURNING organization_type`,
             [renewalPriceModel.amount, renewalPriceModel.id],
             true
        );
        
        // The organization type can be NULL, so we return an empty string if there was an error.
        return result && result.rowCount === 1 ? result.rows[0].organizationType : '';
    }

}

module.exports = new PriceDao();
