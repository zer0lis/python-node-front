'use strict';

let BaseDao = require('./BaseDao'),
    Constants = require('../Constants');

class RoomDao extends BaseDao {
    
    async updateLastUsedRoom(teacherId, status) {
        let result = await this.executeQuery(
            `UPDATE rooms_room
             SET status = status & $1
             WHERE created_by_id = $2
             AND status & $3 = $4
             RETURNING name_lower`,
            [status, teacherId, Constants.LAST_USED, Constants.LAST_USED]
        );
        
        return result ? result.rows[0].name_lower : null;
    }
    
}

module.exports = new RoomDao();
