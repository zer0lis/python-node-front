'use strict';

let BaseDao = require('./BaseDao');

class AccountDao extends BaseDao {
    
    async isAvailable(email) {
        let result = await this.executeReadQuery(
            `SELECT id
             FROM socrative_users_socrativeuser
             WHERE email = $1`,
             [email]
        );
        
        return result && result.rowCount === 0;
    }
    
    async insertAccount(accountModel) {
        let {
            authToken,
            country,
            description,
            districtName,
            email,
            firstName,
            language,
            lastName,
            orgType,
            password,
            role,
            schoolId,
            schoolName,
            state,
            university,
            zipCode
        } = accountModel;
        
        let result = await this.executeQuery(
            `INSERT INTO socrative_users_socrativeuser
                (auth_token, country, description, district_name, email, first_name,
                 language, last_name, organization_type, password, user_role,
                 school_id, school_name, state, university, zip_code,
                 last_login, is_active, date_joined, register_date, display_name, tz_offset)
             VALUES
                 ($1, $2, $3, $4, $5, $6,
                  $7, $8, $9, $10, $11,
                  $12, $13, $14, $15, $16,
                  now(), TRUE, now(), CURRENT_DATE, '', 0)
             RETURNING id`,
             [authToken, country, description, districtName, email, firstName,
              language, lastName, orgType, password, role,
              schoolId, schoolName, state, university, zipCode]
        );
        
        return result ? result.rows[0].id : null;
    }
    
    async updateAccount(accountModel) {
        let {
            country,
            description,
            districtName,
            email,
            firstName,
            id,
            language,
            lastName,
            orgType,
            password,
            role,
            schoolId,
            schoolName,
            state,
            university,
            zipCode
        } = accountModel;
        
        let query = `UPDATE socrative_users_socrativeuser
                     SET first_name = $1, last_name = $2, email = $3, country = $4, language = $5,
                         organization_type = $6, zip_code = $7, school_name = $8, school_id = $9, state = $10,
                         university = $11, description = $12, user_role = $13, district_name = $14`;
        
        let values = [
            firstName, lastName, email, country, language,
            orgType, zipCode, schoolName, schoolId, state,
            university, description, role, districtName
        ];
        
        if (password) {
            values.push(password);
            query += `, password = $15`;
        }
        
        values.push(id);
        query += ` WHERE id = $${values.length}`;
        
        await this.executeQuery(query, values);
    }
    
    async selectPassword(id) {
        let result = await this.executeQuery(
            `SELECT password
             FROM socrative_users_socrativeuser
             WHERE id = $1`,
            [id]
        );
        
        return result && result.rows[0] ? result.rows[0].password : null;
    }
    
}

module.exports = new AccountDao();
