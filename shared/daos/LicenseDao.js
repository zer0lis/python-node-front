'use strict';

let BaseDao = require('./BaseDao'),
    pgFormat = require('pg-format'),
    logger = require('../Logger');

class LicenseDao extends BaseDao {
    
    async insertLicense(props) {
        let client = await this.getTxClient();
        if (!client) {
            return false;
        }
        
        try {
            let {
                buyerId,
                key,
                expirationDate,
                couponId,
                autoRenew,
                years,
                pricePerSeat,
                customerId,
                stripeResponse,
                seats,
                totalPrice,
                purchaseDate
            } = props;
            
            let licenseValues = [buyerId, key, expirationDate, couponId, autoRenew, years, pricePerSeat, customerId];
            
            let result = await client.query(
                `INSERT INTO licenses
                     (buyer_id, key, expiration_date, coupon_id, auto_renew, years, price_per_seat, customer_id)
                 VALUES
                     ($1, $2, $3, $4, $5, $6, $7, $8)
                 RETURNING id`,
                 licenseValues
            );
            
            let licenseId = result.rows[0].id;
            
            let transactionValues = [licenseId, seats, totalPrice, purchaseDate, stripeResponse];
            
            await client.query(
                `INSERT INTO license_transactions
                     (license_id, seats, price, purchase_date, stripe_charge_response)
                 VALUES
                     ($1, $2, $3, $4, $5)`,
                 transactionValues
            );
    
            await client.query('COMMIT');
            
            return licenseId;
        } catch (error) {
            logger.error('Error inserting license:', error);
            try {
                await client.query('ROLLBACK');
            } catch (rollBackError) {
                logger.error('Error rolling back insert license:', rollBackError);
            }
        } finally {
            client.release();
        }
    }
    
    async loadLicenseByKey(key) {
        let result = await this.executeQuery(
            `SELECT *
             FROM licenses
             WHERE key = $1`,
             [key]
        );
        if (result && result.rows[0]) {
            result.rows[0].expiration_date = new Date(result.rows[0].expiration_date);
            return result.rows[0];
        }
        return null;
    }
    
    async hasStripeCharge(userId, licenseKey) {
        let query = `SELECT l.id
                     FROM licenses AS l
                     JOIN license_transactions AS t
                     ON t.license_id = l.id
                     WHERE l.buyer_id = $1
                     AND t.stripe_charge_response IS NOT NULL`,
            params = [userId];
        
        if (licenseKey) {
            query += ' AND l.key = $2';
            params.push(licenseKey);
        }
        
        query += ' LIMIT 1';
        
        let result = await this.executeReadQuery(query, params);
        
        return result && result.rowCount === 1;
    }
    
    async selectAutoRenew(userId, licenseKey) {
        let result = await this.executeReadQuery(
            `SELECT auto_renew
             FROM licenses
             WHERE buyer_id = $1
             AND key = $2
             AND customer_id IS NOT NULL`,
            [userId, licenseKey]
        );
        
        let autoRenew = {
            allowed: false,
            value: false
        };
        
        if (result && result.rowCount > 0) {
            autoRenew.allowed = true;
            autoRenew.value = result.rows[0].auto_renew;
        }
        
        return autoRenew;
    }
    
    async updateAutoRenew(licenseModel, userId) {
        let result = await this.executeQuery(
            `UPDATE licenses
             SET auto_renew = $1
             WHERE key = $2
             AND buyer_id = $3
             AND customer_id IS NOT NULL
             RETURNING *`,
             [licenseModel.autoRenew, licenseModel.key, userId]
        );
        
        return result && result.rowCount === 1 ? result.rows[0] : null;
    }
    
    async selectExpiringLicenses(days) {
        let targetDay = `'${days} ${days === 1 ? 'day' : 'days'}'`,
            nextDay = `'${++days} ${days === 1 ? 'day' : 'days'}'`;
        
        let result = await this.executeReadQuery(pgFormat(
            `SELECT l.id, l.key, l.expiration_date, l.years, l.customer_id,
                    u.email, u.language, u.organization_type, u.first_name, u.last_name, u.description, u.university, u.school_name,
                    sum(t.seats)::integer AS seats
             FROM licenses AS l
             JOIN license_transactions AS t
             ON t.license_id = l.id
             JOIN socrative_users_socrativeuser AS u
             ON u.id = l.buyer_id
             WHERE l.auto_renew = TRUE
             AND l.customer_id IS NOT NULL
             AND l.expiration_date >= CURRENT_DATE + INTERVAL %L
             AND l.expiration_date <  CURRENT_DATE + INTERVAL %L
             GROUP BY l.id, l.key, l.expiration_date, l.years, l.customer_id,
                      u.email, u.language, u.organization_type, u.first_name, u.last_name, u.description, u.university, u.school_name`,
             targetDay,
             nextDay
        ), [], true);
        
        return result ? result.rows : null;
    }
    
    async selectNewestLicenseKey(userId, txClient) {
        let key = null;
        
        try {
            let result = await txClient.query(
                `SELECT l.key
                 FROM licenses l
                 JOIN license_activations a
                 ON a.license_id = l.id
                 AND a.user_id = $1
                 WHERE l.expiration_date + INTERVAL '30 days' >= now()
                 ORDER BY l.expiration_date DESC
                 LIMIT 1`,
                [userId]
            );
            
            if (result && result.rows[0]) {
                key = result.rows[0].key;
            }
        } catch (error) {
            logger.error('Error selecting newest license:', error);
        }
        
        return key;
    }
    
    async insertLicenseTransaction(licenseTransaction, txClient) {
        let {
            licenseId,
            seats,
            price,
            purchaseDate,
            stripeResponse
        } = licenseTransaction;
        
        try {
            let result = await txClient.query(
                `INSERT INTO license_transactions (license_id, seats, price, purchase_date, stripe_charge_response)
                 VALUES ($1, $2, $3, $4, $5)`,
                [licenseId, seats, price, purchaseDate, stripeResponse]
            );
            
            return result && result.rowCount === 1;
        } catch (error) {
            logger.error('Error inserting license transaction:', error);
            this.rollbackAndRelease(txClient);
        }
    }
    
    async updateLicenseExpirationDate(expirationDate, id, txClient) {
        try {
            let result = await txClient.query(
                `UPDATE licenses
                 SET expiration_date = $1
                 WHERE id = $2`,
                [expirationDate, id]
            );
            
            return result && result.rowCount === 1;
        } catch (error) {
            logger.error('Error updating license expiration date:', error);
            this.rollbackAndRelease(txClient);
        }
    }
    
}

module.exports = new LicenseDao();
