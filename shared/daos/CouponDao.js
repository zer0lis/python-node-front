'use strict';

let BaseDao = require('./BaseDao'),
    Constants = require('../Constants');

class CouponDao extends BaseDao {

	async loadCoupons() {
        let result = await this.executeQuery(
            `SELECT * 
             FROM coupons
             ORDER BY expiration_date ASC`
        );
        return result ? result.rows : null;
    }

    async selectCouponByName(name) {
        let result = await this.executeReadQuery(
            `SELECT *
             FROM coupons
             WHERE LOWER(name) = LOWER($1)`,
             [name]
        );
        
        if (result && result.rows[0]) {
            result.rows[0].expiration_date = new Date(result.rows[0].expiration_date);
            return result.rows[0];
        }
        
        return null;
    }

    async insertCoupon(coupon) {
        let result = null,
            client = await this.getClient();

        try {
            result = await client.query(
                `INSERT INTO coupons (name, amount, allow_bulk, expiration_date) 
                 VALUES ($1, $2, $3, $4) 
                 RETURNING id`, 
                 [coupon.name, coupon.amount, coupon.allowBulk, coupon.expirationDate]
            );
        } catch (error) {
            if (error.constraint === 'coupons_name_key') {
                return {error: Constants.NOT_UNIQUE_NAME};
            }
        } finally {
            client.release();
        }
    
        return result ? result.rows[0] : null;
    }

    async updateCoupon(coupon) {
        let result = null,
            client = await this.getClient();

        try {    
            result = await client.query(
                `UPDATE coupons 
                 SET name = $1, amount = $2, allow_bulk = $3, expiration_date = $4 
                 WHERE id = $5`, 
                 [coupon.name, coupon.amount, coupon.allowBulk, coupon.expirationDate, coupon.id]
            );
        } catch (error) {
            if (error.constraint === 'coupons_name_key') {
                return {error: Constants.NOT_UNIQUE_NAME};
            } else {
                return result;
            }
        } finally {
            client.release();
        }
    
        return result && result.command === 'UPDATE' && result.rowCount == 1;
    }

    async deleteCoupon(id) {
        let result = null,
            client = await this.getClient();

        try {
            result = await client.query(
                `DELETE
                 FROM coupons
                 WHERE id = $1
                 RETURNING name`,
                 [id]
            );
        } catch (error) {
            if (error.constraint === 'coupon_id_refs_coupons_id') {
                return {error: Constants.COUPON_IN_USE};
            } else {
                return {error: Constants.DATABASE_ERROR};
            }
        } finally {
            client.release();
        }
	    
        if (result.rowCount === 0) {
            return {error: Constants.INVALID_ID};
        }
        
        return {name: result.rows[0].name};
    }

}

module.exports = new CouponDao();
