'use strict';

let BaseDao = require('./BaseDao'),
    Constants = require('../Constants'),
    pgFormat = require('pg-format'),
    translate = require('../translations/translator').translate,
    logger = require('../Logger');

class QuizDao extends BaseDao {
    
    async selectQuizList(teacherId) {
        let quizzesResult = await this.executeReadQuery(
            `SELECT id, name, parent_id, soc_number AS soc, last_updated AS "date", sharable, deleted
             FROM quizzes_quiz
             WHERE created_by_id = $1
             AND last_updated IS NOT NULL
             AND is_hidden IS FALSE`,
             [teacherId]
        );
        
        if (!quizzesResult) {
            logger.error(`Error getting quiz list for teacher id ${teacherId}`);
            return null;
        }
        
        if (quizzesResult.rows.length === 0) {
            return []; // The teacher simply has no quizzes at the moment.
        }
        
        let quizzes = quizzesResult.rows,
            quizIds = [];
        
        for (let quiz of quizzes) {
            quizIds.push(quiz.id);
        }
    
        let standardsResult = await this.executeReadQuery(pgFormat(
            `SELECT quiz_id, name
             FROM quizzes_standard
             WHERE quiz_id IN (%L)`,
             quizIds
        ));
    
        if (!standardsResult) {
            logger.error(`Error getting standards for teacher id ${teacherId}`);
            return null;
        }
        
        let standardNameMap = new Map();
        
        for (let row of standardsResult.rows) {
            standardNameMap.set(row.quiz_id, row.name);
        }
        
        for (let quiz of quizzes) {
            quiz.standard = standardNameMap.get(quiz.id) || '';
        }
        
        return quizzes;
    }
    
    async insertNewQuiz(teacherId, name, parentId) {
        let result = await this.executeQuery(
            `INSERT INTO quizzes_quiz (created_by_id, name, parent_id, created_date, soc_number, is_hidden, sharable)
             VALUES ($1, $2, $3, now(), (SELECT currval('quizzes_quiz_id_seq') + ${Constants.LEGACY_ID_LIMIT}), FALSE, TRUE)
             RETURNING id, soc_number`,
             [teacherId, name, parentId]
        );
        
        if (!result || result.rowCount !== 1) {
            logger.error(`Failed to insert new quiz in database`);
            return null;
        }
        
        let quizId = result.rows[0].id,
            actualSocNumber = result.rows[0].soc_number,
            desiredSocNumber = String(quizId + Constants.LEGACY_ID_LIMIT);
        
        if (actualSocNumber !== desiredSocNumber) {
            result = await this.executeQuery(
                `UPDATE quizzes_quiz
                 SET soc_number = $1
                 WHERE id = $2`,
                 [desiredSocNumber, quizId]
            );
            
            if (!result || result.rowCount !== 1) {
                logger.error(`Failed to update SOC number to ${desiredSocNumber} in database`);
                return null;
            }
        }
        
        return desiredSocNumber;
    }
    
    async selectQuizBySoc(socNumber, forImport = false, isHidden = false) {
        let query = `SELECT *
                     FROM quizzes_quiz
                     WHERE soc_number = $1 `;
        
        if (forImport) {
            query += ' AND sharable = TRUE ';
        }
        
        query += `AND is_hidden = $2
                  ORDER BY last_updated DESC
                  LIMIT 1`;
        
        let result = await this.executeQuery(query, [socNumber, isHidden]);
        
        return result ? result.rows[0] : null;
    }
    
    async cloneQuiz(cloneQuizModel) {
        let client = await this.getTxClient();
        
        if (!client) {
            return null;
        }
        
        let {
            addStandards,
            hideCloning,
            hideOldQuiz,
            name,
            parentId,
            quiz,
            socNumber,
            userId
        } = cloneQuizModel;
        
        if (!hideCloning) {
            quiz.name = `${quiz.name.substr(0, 240)} (${translate('copy')})`;
        } else {
            quiz.is_hidden = false;
        }
        
        if (name) {
            quiz.name = name;
        }
        
        if (!hideOldQuiz) {
            quiz.is_hidden = false;
        }
        
        quiz.created_by_id = userId;
        quiz.last_updated = new Date();
        
        let insertQuery = `INSERT INTO quizzes_quiz (name, is_hidden, created_by_id, created_date, last_updated, sharable, parent_id, soc_number)
                           VALUES ($1, $2, $3, $4, $5, $6, $7,`;
        
        let insertArgs = [quiz.name, quiz.is_hidden, quiz.created_by_id, quiz.created_date, quiz.last_updated, quiz.sharable, parentId];
        
        if (!hideCloning || !hideOldQuiz) {
            insertQuery += ` (SELECT currval('quizzes_quiz_id_seq'))) RETURNING *`;
        } else {
            insertQuery += ' $8) RETURNING *';
            insertArgs.push(socNumber);
        }
        
        try {
            let insertQuizResult = await client.query(insertQuery, insertArgs);
            
            let oldQuizId = quiz.id;
            
            quiz = insertQuizResult.rows[0];
            
            let actualSocNumber = quiz.soc_number,
                desiredSocNumber = String(quiz.id + Constants.LEGACY_ID_LIMIT);
            
            if ((!hideCloning || !hideOldQuiz) && actualSocNumber !== desiredSocNumber) {
                await client.query(
                    `UPDATE quizzes_quiz
                     SET soc_number = $1
                     WHERE id = $2`,
                     [desiredSocNumber, quiz.id]
                );
                
                quiz.soc_number = desiredSocNumber;
            }
            
            let questionIdsResult = await client.query(
                `SELECT question_id
                 FROM quizzes_question
                 WHERE quiz_id = $1`,
                 [oldQuizId]
            );
            
            if (questionIdsResult && questionIdsResult.rows.length > 0) {
                let oldQuestionIds = [],
                    newQuestionIds = [];
                
                // Clone relevant rows in quizzes_question.
                for (let questionId of questionIdsResult.rows) {
                    let insertQuestionResult = await client.query(
                        `INSERT INTO quizzes_question (type, created_by_id, "order", question_text, explanation, created_date, grading_weight, quiz_id, legacy_id)
                         SELECT type, $1, "order", question_text, explanation, created_date, grading_weight, $2, legacy_id
                         FROM quizzes_question
                         WHERE question_id = $3
                         RETURNING question_id`,
                         [quiz.created_by_id, quiz.id, questionId.question_id]
                    );
                    
                    oldQuestionIds.push(questionId.question_id);
                    newQuestionIds.push(insertQuestionResult.rows[0].question_id);
                }
    
                // Clone relevant rows in quizzes_question_resources and common_mediaresource.
                let questionIdMap = new Map();
                
                for (let i = 0; i < oldQuestionIds.length; i++) {
                    questionIdMap.set(oldQuestionIds[i], newQuestionIds[i]);
                }
                
                let oldQuestionResourceQuery = pgFormat(
                    `SELECT mediaresource_id, question_id
                     FROM quizzes_question_resources
                     WHERE question_id in (%L)`,
                     oldQuestionIds
                );
                
                let oldQuestionResourceResult = await client.query(oldQuestionResourceQuery);
                
                if (oldQuestionResourceResult && oldQuestionResourceResult.rows.length > 0) {
                    let oldQuestionResourceMap = new Map();
                    
                    for (let oldQuestionResource of oldQuestionResourceResult.rows) {
                        oldQuestionResourceMap.set(oldQuestionResource.mediaresource_id, oldQuestionResource.question_id);
                    }
                    
                    let resourceQuery = pgFormat(
                        `SELECT *
                         FROM common_mediaresource
                         WHERE id IN (%L)`,
                         Array.from(oldQuestionResourceMap.keys())
                    );
                    
                    let resourceResult = await client.query(resourceQuery);
                    
                    if (resourceResult && resourceResult.rows.length > 0) {
                        let oldResourceIds = [],
                            newResourceIds = [];
                        
                        for (let resource of resourceResult.rows) {
                            let newResourceResult = await client.query(
                                `INSERT INTO common_mediaresource (owner_id, name, type, url)
                                 VALUES ($1, $2, $3, $4)
                                 RETURNING id`,
                                 [quiz.created_by_id, resource.name, resource.type, resource.url]
                            );
                            
                            oldResourceIds.push(resource.id);
                            newResourceIds.push(newResourceResult.rows[0].id);
                        }
                        
                        let resourceIdMap = new Map();
                        
                        for (let i = 0; i < oldResourceIds.length; i++) {
                            resourceIdMap.set(oldResourceIds[i], newResourceIds[i]);
                        }
                        
                        let newQuestionResources = [];
                        
                        for (let oldResourceId of resourceIdMap.keys()) {
                            newQuestionResources.push([
                                resourceIdMap.get(oldResourceId),
                                questionIdMap.get(oldQuestionResourceMap.get(oldResourceId))
                            ]);
                        }
                        
                        await client.query(pgFormat(
                            `INSERT INTO quizzes_question_resources (mediaresource_id, question_id)
                             VALUES %L`,
                             newQuestionResources
                        ));
                    }
                }
                
                // Clone relevant rows in quizzes_answer.
                let answersQuery = pgFormat(
                    `SELECT created_by_id, text, is_correct, "order", question_id, legacy_id
                     FROM quizzes_answer
                     WHERE question_id IN (%L)`,
                     oldQuestionIds
                );
                
                let answersResult = await client.query(answersQuery);
                
                if (answersResult && answersResult.rows.length > 0) {
                    let newAnswers = [];
                    
                    for (let answer of answersResult.rows) {
                        newAnswers.push([
                            quiz.created_by_id,
                            answer.text,
                            answer.is_correct,
                            answer.order,
                            questionIdMap.get(answer.question_id),
                            answer.legacy_id
                        ]);
                    }
                    
                    await client.query(pgFormat(
                        `INSERT INTO quizzes_answer (created_by_id, text, is_correct, "order", question_id, legacy_id)
                         VALUES %L`,
                         newAnswers
                    ));
                }
                
                // Clone relevant rows in quizzes_standard.
                if (addStandards) {
                    let standardQuery = await client.query(
                        `INSERT INTO quizzes_standard (quiz_id, subject_id, core_id, grade_id, standard_id, name, description)
                         SELECT $1, subject_id, core_id, grade_id, standard_id, name, description
                         FROM quizzes_standard
                         WHERE quiz_id = $2
                         RETURNING name`,
                         [quiz.id, oldQuizId]
                    );

                    if (standardQuery.rowCount > 0) {
                        quiz.standard = standardQuery.rows[0].name;
                    }
                }
                
                if (hideCloning && hideOldQuiz) {
                    await client.query(
                        `UPDATE quizzes_quiz
                         SET is_hidden = TRUE
                         WHERE id = $1`,
                         [oldQuizId]
                    );
                }
                
                await client.query('COMMIT');
                
                return quiz;
            } else {
                logger.error(`Will not clone quiz with id ${oldQuizId} because it has no questions.`);
                
                await client.query('ROLLBACK');
            }
        } catch (error) {
            logger.error('Error cloning quiz:', error);
            try {
                await client.query('ROLLBACK');
            } catch (rollBackError) {
                logger.error('Error rolling back clone quiz:', rollBackError);
            }
            return null;
        } finally {
            client.release();
        }
    }

    async getSharable(socNumbers, userId) {
        let result = await this.executeQuery(
            `SELECT id, sharable, soc_number
             FROM quizzes_quiz
             WHERE 
                soc_number = ANY($1) AND
                is_hidden is FALSE AND
                created_by_id = $2`,
            [socNumbers, userId]
        );

        if (!result) {
            return null;
        }

        let mapping = new Map();
        for(let row of result.rows) {
            mapping.set(row.soc_number, row);
        }

        return mapping;
    }

    async getQuestionIds(quizIds) {
        let result = await this.executeQuery(
            `SELECT question_id, quiz_id, "order"
             FROM quizzes_question
             WHERE 
                quiz_id = ANY($1)
             ORDER BY quiz_id,"order" ASC`,
            [quizIds]
        );

        if (!result) {
            return null;
        }

        let mapping = new Map();
        for(let row of result.rows) {
            if (mapping.has(row.quiz_id)) {
                mapping.get(row.quiz_id).push(row.question_id);
            }
            else {
                mapping.set(row.quiz_id, [row.question_id])
            }
        }

        return mapping;
    }

    async mergeQuizzes(quizModel, questionOrderMapping) {
        let errors = null,
            client = await this.getTxClient(),
            errorResp = {error: true};

        if (!client) {
            logger.error(`Couldn't get a client connection to start a transaction`);
            return errorResp;
        }

        try {
            // Store the relationship between the new order and the old question ids.
            let orderMapping = new Map();
            for (let [questionId, order] of questionOrderMapping) {
                orderMapping.set(order, questionId);
            }

            // Insert the new quiz.
            let queryResult = await client.query(
                `INSERT INTO quizzes_quiz (name, is_hidden, created_by_id, created_date, last_updated, sharable, parent_id, soc_number)
                 VALUES ($1, $2, $3, now(), now(), $4, $5, (SELECT currval('quizzes_quiz_id_seq') + $6))
                 RETURNING id, soc_number, created_date`,
                 [quizModel.name, quizModel.isHidden, quizModel.createdById, quizModel.sharable, quizModel.parentId, Constants.LEGACY_ID_LIMIT]
            );

            if (!queryResult || queryResult.rowCount !== 1 || queryResult.command !== 'INSERT') {
                logger.error('Inserting merged quiz failed.');
                errors = true;
                return errorResp;
            }

            quizModel.id = queryResult.rows[0].id;
            quizModel.soc = queryResult.rows[0].soc_number; // The client expects this property to be named "soc".
            quizModel.date = queryResult.rows[0].created_date; // The client expects this property to be named "date".

            let values = [];
            for (let item of questionOrderMapping) {
                values.push(item);
            }

            let questionIds = [...questionOrderMapping.keys()];
            
            // Insert questions from both quizzes.
            let insertQuery = pgFormat(
                `INSERT INTO quizzes_question ("type", created_by_id, "order", question_text, explanation, created_date, grading_weight, quiz_id)
                 SELECT "type", %L, (
                     SELECT new_order
                     FROM (VALUES %L) AS tbl(old_question_id, new_order)
                     WHERE old_question_id = question_id::text
                 )::integer, question_text, explanation, created_date, grading_weight, %L
                 FROM quizzes_question
                 WHERE question_id IN (%L)
                 RETURNING question_id, "order"`,
                 quizModel.createdById, values, quizModel.id, questionIds
            );

            queryResult = await client.query(insertQuery);

            if (!queryResult || queryResult.rowCount !== questionOrderMapping.size || queryResult.command !== 'INSERT') {
                logger.error('Error inserting new questions for the merged quiz');
                errors = true;
                return errorResp;
            }

            let questionsMapping = new Map(),
                oldQuestionMapping = new Map();

            for (let row of queryResult.rows) {
                questionsMapping.set(row.question_id, orderMapping.get(row.order));
                oldQuestionMapping.set(orderMapping.get(row.order), row.question_id);
            }

            let selectQuery = pgFormat(
                `SELECT question_id
                 FROM quizzes_question_resources
                 WHERE question_id IN (%L)`,
                 [...oldQuestionMapping.keys()]
            );

            queryResult = await client.query(selectQuery);

            if (!queryResult || queryResult.command !== 'SELECT') {
                logger.error('Error getting the questions with images');
                errors = true;
                return errorResp;
            }

            if (queryResult.rowCount > 0) {

                let questionsWithImgs = [];
                for (let row of queryResult.rows) {
                    questionsWithImgs.push(row.question_id);
                }
                
                // Insert media resources from both quizzes.
                insertQuery = pgFormat(
                    `INSERT INTO common_mediaresource (owner_id, "name", "type", url, data) 
                     SELECT owner_id, "name", "type", url, data
                     FROM common_mediaresource AS cmr
                     INNER JOIN quizzes_question_resources AS qqr
                     ON qqr.mediaresource_id = cmr.id
                     WHERE qqr.question_id IN (%L)
                     RETURNING id`,
                     questionsWithImgs
                );

                queryResult = await client.query(insertQuery);

                if (!queryResult || queryResult.command !== 'INSERT' || queryResult.rowCount !== questionsWithImgs.length) {
                    logger.error('Error inserting media for the new questions');
                    errors = true;
                    return errorResp;
                }

                let newQuestionsWithImgs = [];
                for (let questionId of questionsWithImgs) {
                    if (oldQuestionMapping.has(questionId)) {
                        newQuestionsWithImgs.push(oldQuestionMapping.get(questionId));
                    }
                }

                let media = [],
                    i = 0;
                
                for (let row of queryResult.rows) {
                    media.push([row.id, newQuestionsWithImgs[i]]);
                    i += 1;
                }

                insertQuery = pgFormat(
                    `INSERT INTO quizzes_question_resources (mediaresource_id, question_id)
                     VALUES %L`,
                     media
                );

                queryResult = await client.query(insertQuery);

                if (!queryResult || queryResult.rowCount !== media.length || queryResult.command !== 'INSERT') {
                    logger.error('Error inserting new media ids into the quizzes_question_resources table');
                    errors = true;
                    return errorResp;
                }
            }

            // Insert answers from both quizzes.
            insertQuery = pgFormat(
                `WITH questions AS (
                     SELECT new_question_id::integer, old_question_id::integer
                     FROM (VALUES %L) AS tbl(new_question_id, old_question_id)
                 )
                 INSERT INTO quizzes_answer (created_by_id, text, is_correct, "order", question_id)
                 SELECT %L, text, is_correct, "order", (
                     SELECT questions.new_question_id
                     FROM questions
                     WHERE questions.old_question_id = question_id
                 )
                 FROM quizzes_answer
                 WHERE question_id IN (select old_question_id from questions)`,
                 [...questionsMapping.entries()], quizModel.createdById
            );

            queryResult = await client.query(insertQuery);

            if (!queryResult || queryResult.command !== 'INSERT') {
                logger.error('Error inserting answers for the merged questions');
                errors = true;
                return errorResp;
            }

            return {quiz: quizModel};
        } catch (transactionError) {
            errors = true;
            logger.error(transactionError.toString());
        } finally {
            try {
                if (errors) {
                    client.query("ROLLBACK");
                }  else {
                    client.query("COMMIT");
                }
            } catch (databaseError) {
                logger.error(databaseError.toString())
            }
            client.release();
        }
    }
    
    async insertExcelQuiz(quizModel, questions) {
        let client = await this.getTxClient();
        
        if (!client) {
            return null;
        }
        
        try {
            let result = await client.query(
                `INSERT INTO quizzes_quiz (name, created_by_id, parent_id, is_hidden, sharable, created_date, last_updated, soc_number)
                 VALUES ($1, $2, $3, FALSE, TRUE, now(), now(), (SELECT currval('quizzes_quiz_id_seq')))
                 RETURNING *`,
                 [quizModel.name, quizModel.createdById, quizModel.parentId]
            );
    
            quizModel.id = result.rows[0].id;
            quizModel.date = result.rows[0].last_updated;
            
            let actualSocNumber = result.rows[0].soc_number,
                desiredSocNumber = String(quizModel.id + Constants.LEGACY_ID_LIMIT);
            
            if (actualSocNumber !== desiredSocNumber) {
                result = await client.query(
                    `UPDATE quizzes_quiz
                     SET soc_number = $1
                     WHERE id = $2`,
                     [desiredSocNumber, quizModel.id]
                );
    
                actualSocNumber = desiredSocNumber;
            }
    
            quizModel.soc = actualSocNumber;
    
            for (let question of questions) {
                question.quizId = quizModel.id;
                
                let questionId = await this.insertQuestion(question, client);
                
                for (let answer of question.answers) {
                    answer.questionId = questionId;
                    await this.insertAnswer(answer, client);
                }
            }
    
            await client.query('COMMIT');
            
            return quizModel;
        } catch (error) {
            logger.error('Error inserting Excel quiz:', error);
            try {
                await client.query('ROLLBACK');
            } catch (rollBackError) {
                logger.error('Error rolling back Excel quiz insert:', rollBackError);
            }
            return null;
        } finally {
            client.release();
        }
    }
    
    /**
     * Insert a question into the quizzes_question table.
     * @param {QuestionModel} questionModel The question to insert.
     * @param {pg.Client} client Optional client connection (e.g. when the insert is part of a transaction).
     * @returns {number|null} The ID of the inserted question, or null.
     */
    async insertQuestion(questionModel, client) {
        let {
            type,
            createdById,
            order,
            questionText,
            explanation,
            createdDate,
            gradingWeight,
            quizId
        } = questionModel;
        
        let query = `INSERT INTO quizzes_question (type, created_by_id, "order", question_text, explanation, created_date, grading_weight, quiz_id)
                     VALUES ($1, $2, $3, $4, $5, $6, $7, $8)
                     RETURNING question_id`;
        
        let params = [type, createdById, order, questionText, explanation, createdDate, gradingWeight, quizId];
        
        let result = client ? await client.query(query, params) : await this.executeQuery(query, params);
        
        return result ? result.rows[0].question_id : null;
    }
    
    /**
     * Insert an answer into the quizzes_answer table.
     * @param {AnswerModel} answerModel The answer to insert.
     * @param {pg.Client} client Optional client connection (e.g. when the insert is part of a transaction).
     * @returns {AnswerModel|null} The inserted answer with its new ID, or null.
     */
    async insertAnswer(answerModel, client) {
        let {
            createdById,
            text,
            isCorrect,
            order,
            questionId
        } = answerModel;
        
        let query = `INSERT INTO quizzes_answer (created_by_id, text, is_correct, "order", question_id)
                     VALUES ($1, $2, $3, $4, $5)
                     RETURNING id`;
        
        let params = [createdById, text, isCorrect, order, questionId];
        
        let result = client ? await client.query(query, params) : await this.executeQuery(query, params);
        
        if (!result || result.rowCount !== 1) {
            logger.error(`Failed to insert answer into database`);
            return null;
        }
        
        answerModel.id = result.rows[0].id;
        return answerModel;
    }
    
    async selectActiveQuizzesById(quizIds, teacherId) {
        let result = await this.executeQuery(pgFormat(
            `SELECT *
             FROM quizzes_quiz
             WHERE is_hidden IS FALSE
             AND id IN (%L)
             AND created_by_id = %L`,
             quizIds, teacherId
        ));
        
        return result ? result.rows : null;
    }
    
    async selectByParentId(parentIds, teacherId) {
        let result = await this.executeQuery(pgFormat(
            `SELECT *
             FROM quizzes_quiz
             WHERE is_hidden IS FALSE
             AND parent_id IN (%L)
             AND created_by_id = %L`,
             parentIds, teacherId
        ));
    
        return result ? result.rows : null;
    }
    
    async updateParentId(quizIds, parentId, client) {
        let updateQuery = pgFormat(
            `UPDATE quizzes_quiz
             SET parent_id = %L
             WHERE id IN (%L)`,
             parentId, quizIds
        );

        let result = client ? await client.query(updateQuery) : await this.executeQuery(updateQuery);
    
        return result ? result.command === 'UPDATE' : null;
    }
    
    async updateDeleted(quizIds, deleted, client) {
        let updateQuery = pgFormat(
            `UPDATE quizzes_quiz
             SET deleted = %L
             WHERE id IN (%L)`,
             deleted, quizIds
        );
        
        let result = client ? await client.query(updateQuery): await this.executeQuery(updateQuery);
        
        return result ? result.command === 'UPDATE' : null;
    }
    
    async updateHidden(folderIds, quizIds) {
        let client = await this.getTxClient();
    
        if (!client) {
            return false;
        }
        
        try {
            if (folderIds.length > 0) {
                let result = await client.query(pgFormat(
                    `UPDATE folders
                     SET hidden = TRUE, last_updated = now(), hidden_date = now()
                     WHERE id IN (%L)`,
                     folderIds
                ));
                
                if (!result || result.command !== 'UPDATE') {
                    await client.query('ROLLBACK');
                    return false;
                }
            }
            
            if (quizIds.length > 0) {
                let result = await client.query(pgFormat(
                    `UPDATE quizzes_quiz
                     SET is_hidden = TRUE, last_updated = now(), hidden_date = now()
                     WHERE id IN (%L)`,
                     quizIds
                ));
    
                if (!result || result.command !== 'UPDATE') {
                    await client.query('ROLLBACK');
                    return false;
                }
            }
            
            await client.query('COMMIT');
            
            return true;
        } catch (error) {
            logger.error('Error updating hidden for quizzes:', error);
            
            try {
                await client.query('ROLLBACK');
            } catch (rollBackError) {
                logger.error('Error rolling back quiz hiding update:', rollBackError);
            }
            
            return false;
        } finally {
            client.release();
        }
    }
    
}

module.exports = new QuizDao();
