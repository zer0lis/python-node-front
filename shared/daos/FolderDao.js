'use strict';

let BaseDao = require('./BaseDao'),
    pgFormat = require('pg-format'),
    Constants = require('../Constants'),
    logger = require('../Logger'),
    utils = require('../Utils');

class FoldersDao extends BaseDao {
    
    async selectFolders(userId, type) {
        let result = await this.executeReadQuery(
            `SELECT *
             FROM folders
             WHERE user_id = $1
             AND type = $2
             AND hidden IS FALSE`,
             [userId, type],
             true
        );
    
        return result ? result.rows : null;
    }
    
    async insertFolder(folder, userId) {
        let result = await this.executeQuery(
            `INSERT INTO folders (name, parent_id, type, user_id)
             VALUES ($1, $2, $3, $4)
             RETURNING id`,
             [folder.name, folder.parentId, folder.type, userId]
        );
    
        return result ? result.rows[0] : null;
    }
    
    async purgeFolders(ids) {
        let result = await this.executeQuery(
            `UPDATE folders 
             SET hidden = $1, hidden_date = now()
             WHERE id = ANY ($2)`,
             [true, ids]
        );

        return result && result.command === 'UPDATE';
    }
    
    async renameFolder(folder, userId) {
        let result = await this.executeQuery(
            `UPDATE folders
             SET name = $1, last_updated = now()
             WHERE id = $2
             AND user_id = $3
             RETURNING *`, // Return every column so the model can be put back into the ordered set in the cache.
             [folder.name, folder.id, userId],
             true
        );
        
        return result ? result.rows[0] : null;
    }
    
    async updateParentId(moveModel, client) {
        let result = await client.query(pgFormat(
            `UPDATE folders
             SET parent_id = %L, last_updated = now()
             WHERE id IN (%L)
             AND type = %L
             AND user_id = %L
             RETURNING *`, // Return every column so the model can be put back into the ordered set in the cache.
             moveModel.parentId, moveModel.folderIds, moveModel.type, moveModel.userId
        ));
        
        if (result && result.rows) {
            for (let row of result.rows) {
                utils.propsToCamelCase(row);
            }
        }
        
        return result ? result.rows : null;
    }
    
    async updateDeleted(moveModel, client) {
        let result = await client.query(pgFormat(
            `UPDATE folders
             SET deleted = %L, last_updated = now()
             WHERE id IN (%L)
             AND type = %L
             AND user_id = %L
             RETURNING *`, // Return every column so the model can be put back into the ordered set in the cache.
             moveModel.deleted, moveModel.folderIds, moveModel.type, moveModel.userId
        ));
    
        if (result && result.rows) {
            for (let row of result.rows) {
                utils.propsToCamelCase(row);
            }
        }
    
        return result ? result.rows : null;
    }
    
}

module.exports = new FoldersDao();
