'use strict';

let BaseDao = require('./BaseDao'),
    Constants = require('../Constants'),
    pgFormat = require('pg-format'),
    logger = require('../Logger');

class ActivationDao extends BaseDao {
    
    async insertActivations(licenseId, candidateUsers) {
        let result = {
            error: false,
            blockedUsers: []
        };
        
        let client = await this.getTxClient();
        
        if (!client) {
            result.error = true;
            return result;
        }
        
        try {
            // To prevent concurrency issues, acquire a write lock on the parent license row.
            await client.query(
                `SELECT *
                 FROM licenses
                 WHERE id = $1
                 FOR UPDATE`,
                 [licenseId]
            );
            
            let candidateUserIds = [];
            
            for (let candidate of candidateUsers) {
                candidateUserIds.push(candidate.id);
            }
    
            let duplicatesQuery = pgFormat(
                `SELECT *
                 FROM license_activations
                 WHERE license_id = %L
                 AND user_id IN (%L)`,
                 licenseId,
                 candidateUserIds
            );
            
            let duplicatesResult = await client.query(duplicatesQuery);
            
            let uniqueUsers = [];
            
            for (let candidate of candidateUsers) {
                let unique = true;
                
                for (let duplicate of duplicatesResult.rows) {
                    if (candidate.id === duplicate.user_id) {
                        unique = false;
                        break;
                    }
                }
                
                if (unique) {
                    uniqueUsers.push(candidate);
                }
            }
            
            if (!uniqueUsers.length) {
                await client.query('ROLLBACK');
                result.error = {userError: Constants.USERS_ALREADY_LICENSED};
                return result;
            }
            
            let policiesResult = await client.query(
                `SELECT *
                 FROM license_policies
                 WHERE license_id = $1`,
                 [licenseId]
            );
            
            let allowPolicies = [],
                blockPolicies = [],
                allowedUserIds = [],
                blockedUsers = [];
            
            for (let policy of policiesResult.rows) {
                if (policy.type === Constants.ALLOW_POLICY) {
                    allowPolicies.push(policy.pattern);
                } else {
                    blockPolicies.push(policy.pattern);
                }
            }
            
            for (let user of uniqueUsers) {
                let allowed = allowPolicies.length === 0;
                
                for (let allowPolicy of allowPolicies) {
                    if (user.email.indexOf(allowPolicy) !== -1) {
                        allowed = true;
                        break;
                    }
                }
                
                if (allowed) {
                    for (let blockPolicy of blockPolicies) {
                        if (user.email.indexOf(blockPolicy) !== -1) {
                            allowed = false;
                            break;
                        }
                    }
                }
                
                if (allowed) {
                    allowedUserIds.push(user.id);
                } else {
                    blockedUsers.push(user.email);
                }
            }
            
            if (!allowedUserIds.length) {
                await client.query('ROLLBACK');
                result.error = {userError: Constants.ACTIVATION_BLOCKED_BY_POLICY};
                return result;
            }
            
            let seatsResult = await client.query(
                `SELECT (
                     (SELECT sum(seats) FROM license_transactions WHERE license_id = $1) - (SELECT count(*) FROM license_activations WHERE license_id = $1)
                 ) AS seats_left`,
                 [licenseId]
            );
            
            if (seatsResult.rows[0].seats_left < allowedUserIds.length) {
                await client.query('ROLLBACK');
                result.error = {userError: Constants.NOT_ENOUGH_SEATS};
                return result;
            }
            
            await client.query(pgFormat(
                `DELETE FROM license_activations
                 WHERE user_id IN (%L)`,
                 allowedUserIds
            ));
            
            let activations = [],
                activationDate = new Date();
            
            for (let userId of allowedUserIds) {
                activations.push([licenseId, userId, activationDate]);
            }
            
            let insertQuery = pgFormat(
                `INSERT INTO license_activations
                     (license_id, user_id, activation_date)
                 VALUES
                     %L`,
                 activations
            );
            
            await client.query(insertQuery);
            
            await client.query('COMMIT');
            
            result.blockedUsers = blockedUsers;
            
            return result;
        } catch (error) {
            logger.error('Error inserting license activation:', error);
            
            try {
                await client.query('ROLLBACK');
            } catch (rollBackError) {
                logger.error('Error rolling back insert license activation:', rollBackError);
            }
            
            return result;
        } finally {
            client.release();
        }
    }
    
    async selectUserIdsByActivation(licenseId, txClient) {
        let userIds = [];
        
        try {
            let result = await txClient.query(
                `SELECT user_id
                 FROM license_activations
                 WHERE license_id = $1`,
                 [licenseId]
            );
            
            if (result) {
                userIds = result.rows;
            }
        } catch (error) {
            logger.error('Error selecting user ids by activation:', error);
        }
        
        return userIds;
    }
    
}

module.exports = new ActivationDao();
