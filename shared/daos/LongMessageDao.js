'use strict';

let BaseDao = require('./BaseDao');

class LongMessageDao extends BaseDao {
    
    async insertLongMessage(message) {
        let result = await this.executeQuery(
            `INSERT INTO socrative_tornado_longmessage (message)
             VALUES ($1)
             RETURNING id`,
            [message]
        );
        
        return result ? result.rows[0].id : null;
    }
    
}

module.exports = new LongMessageDao();
