'use strict';

let BaseDao = require('./BaseDao'),
    pgFormat = require('pg-format'),
    logger = require('../Logger'),
    Constants = require('../Constants');

class TeacherDao extends BaseDao {
    
    async loadTeacherByEmail(email) {
        let result = await this.executeQuery(
            `SELECT *
             FROM socrative_users_socrativeuser
             WHERE email = $1`,
             [email]
        );
    
        let teacher = null;
    
        if (result && result.rows[0]) {
            teacher = result.rows[0];
            await this.setProStatus(teacher);
        }
    
        return teacher;
    }

    async loadTeacherByAuthToken(authToken) {
        let result = await this.executeReadQuery(
            `SELECT *
             FROM socrative_users_socrativeuser
             WHERE auth_token = $1`,
            [authToken]
        );
        
        let teacher = null;
    
        if (result && result.rows[0]) {
            teacher = result.rows[0];
            await this.setProStatus(teacher);
        }
    
        return teacher;
    }
    
    async loadTeachersByEmails(emails) {
        let query = pgFormat(
            `SELECT id, auth_token, email
             FROM socrative_users_socrativeuser
             WHERE email in (%L)`,
             emails
        );
        let result = await this.executeQuery(query);
        return result ? result.rows : null;
    }
    
    async loadTeacherById(id) {
        let result = await this.executeQuery(
            `SELECT *
             FROM socrative_users_socrativeuser
             WHERE id = $1`,
             [id]
        );
    
        let teacher = null;
    
        if (result && result.rows[0]) {
            teacher = result.rows[0];
            await this.setProStatus(teacher);
        }
    
        return teacher;
    }
    
    async setProStatus(teacher) {
        let result = await this.executeReadQuery(
            `SELECT l.expiration_date, l.key
             FROM licenses l
             JOIN license_activations a
             ON a.license_id = l.id
             AND a.user_id = $1
             WHERE l.expiration_date + INTERVAL '30 days' >= now()
             ORDER BY l.expiration_date DESC
             LIMIT 1`,
            [teacher.id]
        );
        
        if (result && result.rows[0]) {
            teacher.level = Constants.PRO_USER;
            teacher.proExpiration = result.rows[0].expiration_date;
            teacher.licenseKey = result.rows[0].key;
        } else {
            teacher.level = Constants.FREE_USER;
            teacher.proExpiration = null;
            teacher.licenseKey = null;
        }
    }
    
    async updateLastLogin(date, authToken, id) {
        let result = await this.executeQuery(
            `UPDATE socrative_users_socrativeuser
             SET last_login = $1, auth_token = $2
             WHERE id = $3`,
             [date, authToken, id]
        );
        
        return result ? (result.command === 'UPDATE' && result.rowCount === 1) : null;
    }
    
    async updateBannerStatus(newStatus, oldStatus, id) {
        let result = await this.executeQuery(
            `UPDATE socrative_users_usersysmsg
             SET status = $1
             WHERE status = $2
             AND user_id = $3
             AND id = (
                 SELECT max(id)
                 FROM socrative_users_usersysmsg
                 WHERE user_id = $3
             )`,
             [newStatus, oldStatus, id]
        );
    }

    async updatePassword(password, id) {
        let result = await this.executeQuery(
            `UPDATE socrative_users_socrativeuser
             SET password = $1
             WHERE id = $2`,
             [password, id]
        );
        
        return result ? (result.command === 'UPDATE' && result.rowCount === 1) : null;
    }
    
}

module.exports = new TeacherDao();
