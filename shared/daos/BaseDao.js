'use strict';

let pg = require('pg'),
    config = require('config').get('config'),
    logger = require('../Logger'),
    utils = require('../Utils'),
    dbReadWritePool = new pg.Pool({
        database: config.db.name,
        host: config.db.host,
        idleTimeoutMillis: config.db.idleTimeoutMillis,
        min: config.db.min,
        max: config.db.max,
        password: config.db.password,
        port: config.db.port,
        ssl: false,
        user: config.db.username
    });

dbReadWritePool.on('error', (error) => {
    logger.error('An error occurred with the write database connection pool. The database might be down:', error);
});

let dbReadPool = dbReadWritePool;

if (process.env.NODE_ENV === 'production') {
    dbReadPool = new pg.Pool({
        database: config.db.name,
        host: config.db.replica,
        idleTimeoutMillis: config.db.idleTimeoutMillis,
        min: config.db.min,
        max: config.db.max,
        password: config.db.password,
        port: config.db.port,
        ssl: false,
        user: config.db.username
    });
    
    dbReadPool.on('error', (error) => {
        logger.error('An error occurred with the read database connection pool. The database might be down:', error);
    });
}

class BaseDao {
    
    async getClient() {
        let client = null;
        
        try {
            client = await dbReadWritePool.connect();
        } catch (error) {
            logger.error('An error occurred getting a client from the read/write database connection pool:', error);
        }
        
        return client;
    }
    
    async executeQuery(query, params, camelCase) {
        try {
            let result = await dbReadWritePool.query(query, params);
            
            if (camelCase && result && result.rows) {
                for (let row of result.rows) {
                    utils.propsToCamelCase(row);
                }
            }
            
            return result;
        } catch (error) {
            logger.error(`Error executing query (${query}) with params (${params}):`, error);
        }
    }
    
    async executeReadQuery(query, params, camelCase) {
        try {
            let result = await dbReadPool.query(query, params);
            
            if (camelCase && result && result.rows) {
                for (let row of result.rows) {
                    utils.propsToCamelCase(row);
                }
            }
            
            return result;
        } catch (error) {
            logger.error(`Error executing read-only query (${query}) with params (${params}):`, error);
        }
    }
    
    async getTxClient() {
        let client = null;
        
        try {
            client = await dbReadWritePool.connect();
        } catch (error) {
            logger.error('An error occurred getting a transaction client from the read/write database connection pool:', error);
            return null;
        }
        
        try {
            await client.query('BEGIN');
            return client;
        } catch (error) {
            logger.error('An error occurred attempting to start a transaction:', error);
            client.release();
        }
    }
    
    async commitAndRelease(txClient) {
        try {
            await txClient.query('COMMIT');
        } catch (commitError) {
            logger.error('Error committing transaction:', commitError);
        }
        
        txClient.release();
    }
    
    async rollbackAndRelease(txClient) {
        try {
            await txClient.query('ROLLBACK');
        } catch (rollbackError) {
            logger.error('Error rolling back transaction:', rollbackError);
        }
        
        txClient.release();
    }
    
}

module.exports = BaseDao;
