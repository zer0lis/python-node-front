'use strict';

let BaseDao = require('./BaseDao'),
    Constants = require('../Constants'),
    pgFormat = require('pg-format'),
    pg = require('pg'),
    config = require('config').get('config'),
    logger = require('../Logger');

class AdminDao extends BaseDao {
    
    async loadAdminByEmail(email) {
        let result = await this.executeQuery('SELECT a.*, r.level FROM admins a JOIN admin_roles r ON r.user_id = a.id WHERE a.email = $1', [email]);
        return result ? result.rows[0] : null;
    }
    
    async updateLastLogin(id) {
        await this.executeQuery(
            `UPDATE admins
             SET last_login = $1
             WHERE id = $2`,
             [new Date(), id]
        );
    }
    
    async deleteTeacher(email, authToken, id) {
        let result = await this.executeQuery(
            `UPDATE socrative_users_socrativeuser
             SET email = $1, is_active = FALSE, auth_token = $2
             WHERE id = $3`,
             [email, authToken, id]
        );
        return result ? (result.command === 'UPDATE' && result.rowCount == 1) : null;
    }
    
    async loadTeacherByRoom(room) {
        let result = {
            error: false
        };

        let teacherData = await this.executeQuery(
            `SELECT u.id, u.email, u.last_login, u.register_date
             FROM socrative_users_socrativeuser u
             JOIN rooms_room r ON r.created_by_id = u.id
             WHERE r.name_lower = $1`,
            [room]

        );
        
        if (teacherData.rows.length === 0) {
            result.error = Constants.ROOM_NOT_IN_USE_MESSAGE;
            return result;
        } else {
            result.email = teacherData.rows[0].email;
            result.lastLogin = teacherData.rows[0].last_login;
            result.registerDate = teacherData.rows[0].register_date;

            let id = teacherData.rows[0].id;

            let ssoData = await this.executeQuery(
                `SELECT data
                 FROM common_partner 
                 WHERE type='MC' AND user_id = $1
                 LIMIT 1`,
                 [id]
            );

            if (ssoData.rows.length > 0) {
                result.createdDate = JSON.parse(ssoData.rows[0].data);
            }
        
            let licenseData = await this.executeQuery(
                `SELECT l.key, l.expiration_date, u.email
                 FROM licenses l
                 JOIN license_activations a
                 ON a.license_id = l.id
                 AND a.user_id = $1
                 JOIN socrative_users_socrativeuser u
                 ON u.id = l.buyer_id
                 WHERE l.expiration_date + INTERVAL '30 days' >= now()
                 ORDER BY l.expiration_date DESC
                 LIMIT 1`,
                 [id]
                );

            if (licenseData.rows.length > 0) {
                let queryResult = licenseData.rows[0];
                result.key = queryResult.key;
                result.expirationDate = queryResult.expiration_date;
                result.buyerEmail = queryResult.email;
            }
        }
        
        return result;
    }
    
    async mergeAccounts(idToKeep, idToMerge) {
        let client = await this.getClient();
        
        if (!client) {
            return {message: Constants.INTERNAL_SERVER_ERROR_MESSAGE};
        }
        
        try {
            await client.query('BEGIN');
    
            // Make sure the total rooms, minus one discarded default room, will not be more than the max allowed.
            let roomCountQuery = pgFormat(
                `SELECT count(id)
                 FROM rooms_room
                 WHERE created_by_id IN (%L)
                 AND status < ${Constants.ARCHIVED_ROOM}`,
                 [idToKeep, idToMerge]
            );
            
            let countResult = await client.query(roomCountQuery);
            
            if (countResult.rows[0].count - 1 > Constants.MAX_PRO_ROOMS) {
                await client.query('ROLLBACK');
                return {message: Constants.TOO_MANY_ROOMS_MESSAGE};
            }
            
            // Determine whether either account is pro based on level.
            let userType = Constants.FREE_USER;
            
            let roleQuery = pgFormat(
                `SELECT expiration_date, level
                 FROM socrative_users_teacherrole
                 WHERE is_hidden IS FALSE
                 AND user_id IN (%L)`,
                 [idToKeep, idToMerge]
            );
            
            let roleResult = await client.query(roleQuery);
            
            for (let role of roleResult.rows) {
                let expirationDate = new Date(role.expiration_date);
                if (role.level === Constants.PRO_USER && expirationDate.getTime() > Date.now()) {
                    userType = Constants.PRO_USER;
                    break;
                }
            }
    
            // Determine whether either account is pro based on license.
            if (userType === Constants.FREE_USER) {
                let licenseQuery = pgFormat(
                    `SELECT l.expiration_date
                     FROM licenses l
                     JOIN license_activations a
                     ON a.license_id = l.id
                     AND a.user_id IN (%L)
                     WHERE l.expiration_date + INTERVAL '30 days' >= now()
                     ORDER BY l.expiration_date DESC
                     LIMIT 1`,
                     [idToKeep, idToMerge]
                );
                
                let licenseResult = await client.query(licenseQuery);
                
                if (licenseResult.rows && licenseResult.rows.length > 0) {
                    userType = Constants.PRO_USER;
                }
            }
            
            // Determine old room names that will need to be removed from the cache.
            let roomListResult = await client.query(
                `SELECT name_lower
                 FROM rooms_room
                 WHERE created_by_id = $1`,
                 [idToMerge]
            );
            
            // Perform the merge.
            await client.query(
                `UPDATE common_mediaresource
                 SET owner_id = $1
                 WHERE owner_id = $2`,
                 [idToKeep, idToMerge]
            );
            
            await client.query(
                `UPDATE common_partner
                 SET user_id = $1
                 WHERE user_id = $2`,
                 [idToKeep, idToMerge]
            );
            
            await client.query(
                `UPDATE licenses
                 SET buyer_id = $1
                 WHERE buyer_id = $2`,
                 [idToKeep, idToMerge]
            );
    
            await client.query(
                `UPDATE license_activations
                 SET user_id = $1
                 WHERE user_id = $2`,
                 [idToKeep, idToMerge]
            );
            
            await client.query(
                `UPDATE quizzes_answer
                 SET created_by_id = $1
                 WHERE created_by_id = $2`,
                 [idToKeep, idToMerge]
            );
            
            await client.query(
                `UPDATE quizzes_question
                 SET created_by_id = $1
                 WHERE created_by_id = $2`,
                 [idToKeep, idToMerge]
            );
            
            await client.query(
                `UPDATE quizzes_quiz
                 SET created_by_id = $1
                 WHERE created_by_id = $2`,
                 [idToKeep, idToMerge]
            );
            
            await client.query(
                `UPDATE folders
                 SET user_id = $1
                 WHERE user_id = $2`,
                [idToKeep, idToMerge]
            );
            
            await client.query(
                `UPDATE socrative_users_temptoken
                 SET created_by_id = $1
                 WHERE created_by_id = $2`,
                 [idToKeep, idToMerge]
            );
            
            if (userType === Constants.FREE_USER) {
                await client.query(
                    `UPDATE rooms_room
                     SET created_by_id = $1
                     WHERE created_by_id = $2
                     AND status = $3`,
                     [idToKeep, idToMerge, Constants.EXPIRED_ROOM]
                );
            } else {
                await client.query(
                    `UPDATE rooms_room
                     SET status = status | $1
                     WHERE created_by_id = $2
                     AND status < $3
                     AND status & $4 = 0`,
                     [Constants.IN_MENU, idToMerge, Constants.ARCHIVED_ROOM, Constants.DEFAULT_ROOM]
                );
    
                await client.query(
                    `UPDATE rooms_room
                     SET created_by_id = $1
                     WHERE created_by_id = $2
                     AND status & $3 = 0`,
                     [idToKeep, idToMerge, Constants.DEFAULT_ROOM]
                );
    
                await client.query(
                    `DELETE FROM rooms_rosterfilesettings
                     WHERE teacher_id = $1`,
                     [idToMerge]
                );
                
                await client.query(
                    `UPDATE socrative_users_teacherrole
                     SET user_id = $1
                     WHERE user_id = $2`,
                     [idToKeep, idToMerge]
                );
    
                await client.query(
                    `UPDATE socrative_users_teacherrole
                     SET is_hidden = TRUE
                     WHERE user_id = $1
                     AND is_hidden IS NOT TRUE`,
                     [idToKeep]
                );
    
                await client.query(
                    `UPDATE socrative_users_teacherrole
                     SET is_hidden = FALSE
                     WHERE user_id = $1
                     AND id = (
                         SELECT id
                         FROM socrative_users_teacherrole
                         WHERE user_id = $1
                         ORDER BY expiration_date DESC LIMIT 1
                     )`,
                     [idToKeep]
                );
            }
            
            let defaultRoomToKeepResult = await client.query(
                `SELECT name_lower
                 FROM rooms_room
                 WHERE created_by_id = $1
                 AND status & $2 = $2`,
                 [idToKeep, Constants.DEFAULT_ROOM]
            );
            
            let defaultRoomToKeep = defaultRoomToKeepResult.rows[0].name_lower;

            let defaultRoomToMergeResult = await client.query(
                `SELECT name_lower
                 FROM rooms_room
                 WHERE created_by_id = $1
                 AND status & $2 = $2`,
                 [idToMerge, Constants.DEFAULT_ROOM]
            );
            
            let defaultRoomToMerge = defaultRoomToMergeResult.rows[0].name_lower;

            await client.query(
                `UPDATE common_activityinstance
                 SET room_name = $1, started_by_id = $2
                 WHERE started_by_id = $3
                 AND room_name = $4`,
                 [defaultRoomToKeep, idToKeep, idToMerge, defaultRoomToMerge]
            );
            
            await client.query(
                `UPDATE common_activityinstance
                 SET started_by_id = $1
                 WHERE started_by_id = $2`,
                 [idToKeep, idToMerge]
            );
            
            await client.query(
                `DELETE FROM rooms_room
                 WHERE created_by_id = $1`,
                 [idToMerge]
            );
            
            await client.query(
                `UPDATE rooms_roomhistory
                 SET user_id = $1
                 WHERE user_id = $2`,
                 [idToKeep, idToMerge]
            );

            await client.query(
                `DELETE FROM socrative_users_usersysmsg
                 WHERE user_id = $1`,
                 [idToMerge]
            );
            
            await client.query(
                `DELETE FROM socrative_users_scoreexportsettings
                 WHERE teacher_id = $1`,
                 [idToMerge]
            );

            await client.query(
                `DELETE FROM notifications 
                 WHERE user_id in ($1, $2)`,
                [idToMerge, idToKeep]
            );
            
            await client.query(
                `DELETE FROM socrative_users_socrativeuser
                 WHERE id = $1`,
                 [idToMerge]
            );
            
            await client.query('COMMIT');
            
            return {mergedRooms: roomListResult.rows.map(room => room.name_lower)};
        } catch (error) {
            logger.error('An error occurred during the merge account queries:', error);
            
            try {
                await client.query('ROLLBACK');
            } catch (rollbackError) {
                logger.error('An error occurred rolling back the merge account queries:', rollbackError);
            }
            
            return {message: error};
        } finally {
            client.release();
        }
    }
    
    async loadDeletedQuizzes(id) {
        let result = await this.executeQuery(
            `SELECT DISTINCT ON (soc_number) soc_number, id
             FROM quizzes_quiz
             WHERE created_by_id = $1
             AND "name" != 'Exit Ticket Quiz'
             AND "name" != ''
             AND "name" NOT LIKE 'Quick Question %'
             AND "name" NOT LIKE 'Vote %'
             AND soc_number NOT IN (
                 SELECT soc_number
                 FROM quizzes_quiz
                 WHERE created_by_id = $1
                 AND is_hidden IS NOT TRUE
             )
             ORDER BY soc_number, id DESC`,
             [id]
        );
        
        let quizIds = [];
        
        for (let row of result.rows) {
            quizIds.push(row.id);
        }

        let quizzes = [];
        if (quizIds.length !== 0) {
            let quizzesQuery = pgFormat('SELECT * FROM quizzes_quiz WHERE id IN (%L) ORDER BY last_updated DESC', quizIds);
            let quizzesResult = await this.executeQuery(quizzesQuery);

            for (let quiz of quizzesResult.rows) {
                quizzes.push({
                    id: quiz.id,
                    quiz_name: quiz.name,
                    soc_number: quiz.soc_number
                });
            }
        }
        
        return quizzes;
    }
    
    async restoreQuiz(teacherId, quizId) {
        let result = await this.executeQuery(
            `UPDATE quizzes_quiz
             SET is_hidden = FALSE
             WHERE created_by_id = $1
             AND id = $2
             AND is_hidden IS NOT FALSE`,
             [teacherId, quizId]
        );
        return result ? (result.command === 'UPDATE' && result.rowCount == 1) : null;
    }
    
    async insertBulkUsers(usersIterator) {
        let users = [],
            today = new Date();
        
        for (let user of usersIterator) {
            users.push([user.password, today, user.email, true, today, today, user.displayName, user.authToken, user.first, user.last, 'en']);
        }
    
        let createUsersQuery = pgFormat(
            `INSERT INTO socrative_users_socrativeuser
                 (password, last_login, email, is_active, date_joined, register_date, display_name, auth_token, first_name, last_name, language)
             VALUES
                 %L
             RETURNING id, email`,
             users
        );
        
        let result = await this.executeQuery(createUsersQuery);
        return result ? result.rows : null;
    }
    
    async loadFreeUsersLicense(userIds) {
        let freeUsersQuery = pgFormat(
            `SELECT id, email
             FROM socrative_users_socrativeuser
             WHERE id IN (%1$L)
             AND id NOT IN (
                 SELECT u.id
                 FROM socrative_users_socrativeuser u
                 JOIN license_activations a
                 ON a.user_id = u.id
                 JOIN licenses l
                 ON l.id = a.license_id
                 WHERE u.id IN (%1$L)
                 AND l.expiration_date > now()
             )`,
             userIds
        );
    
        let result = await this.executeQuery(freeUsersQuery);
        return result ? result.rows : null;
    }
    
    async loadFreeUsers(userIds) {
        let freeUsersQuery = pgFormat(
            `SELECT id, email
             FROM socrative_users_socrativeuser
             WHERE id IN (%1$L)
             AND id NOT IN (
                 SELECT u.id
                 FROM socrative_users_socrativeuser u
                 JOIN socrative_users_teacherrole r ON r.user_id = u.id
                 WHERE r.is_hidden IS FALSE
                 AND u.id IN (%1$L)
             )`,
             userIds
        );
    
        let result = await this.executeQuery(freeUsersQuery);
        return result ? result.rows : null;
    }
    
    async upgradeToPro(userIds, expirationDate, amount) {
        let isHiddenQuery = pgFormat(
            `UPDATE socrative_users_teacherrole
             SET is_hidden = TRUE
             WHERE is_hidden IS FALSE
             AND user_id IN (%L)`,
             userIds
        );
        
        let result = await this.executeQuery(isHiddenQuery);
        if (!result || result.command !== 'UPDATE') {
            return false;
        }
        
        let roles = [];
        for (let id of userIds) {
            roles.push([1, expirationDate, id, amount, 'USD', 'manual', false]);
        }
        
        let createRolesQuery = pgFormat(
            `INSERT INTO socrative_users_teacherrole
                 (level, expiration_date, user_id, amount, currency, stripe_customer_id, is_hidden)
             VALUES
                 %L`,
             roles
        );
    
        result = await this.executeQuery(createRolesQuery);
        return result && result.command === 'INSERT' && result.rowCount == roles.length;
    }
    
    async downgradeToFree(id) {
        let result = await this.executeQuery(
            `UPDATE socrative_users_teacherrole
             SET is_hidden = TRUE
             WHERE is_hidden IS NOT TRUE
             AND user_id = $1`,
             [id]
        );
        
        if (!result || result.command !== 'UPDATE') {
            return false;
        }
    
        result = await this.executeQuery(
            `DELETE
             FROM license_activations
             WHERE user_id = $1`,
             [id]
        );
        
        return result && result.command === 'DELETE';
    }
    
    async loadBanners() {
        let result = await this.executeQuery(
            `SELECT *
             FROM socrative_users_systemmessage
             WHERE status = $1
             ORDER BY id DESC`,
             [Constants.BANNER_STATUS_ACTIVE]
        );
        
        return result ? result.rows : null;
    }
    
    async disableBanner(model) {
        let result = await this.executeQuery(
            `UPDATE socrative_users_systemmessage
             SET status = $1
             WHERE id = $2`,
             [Constants.BANNER_STATUS_HIDDEN, model.id]
        );
        
        return result && result.command === 'UPDATE' && result.rowCount == 1;
    }
    
    async hideBanner(model) {
        let hideBannerQuery = `UPDATE socrative_users_systemmessage
                               SET status = $1
                               WHERE status != $1`;
        
        let params = [Constants.BANNER_STATUS_HIDDEN];
        
        if (model.audience !== Constants.BANNER_AUDIENCE_ALL) {
            hideBannerQuery += ' AND audience = $2';
            params.push(model.audience);
        }
        
        let result = await this.executeQuery(hideBannerQuery, params);
        
        return result && result.command === 'UPDATE';
    }
    
    async insertBanner(model) {
        let result = await this.executeQuery(
            `INSERT INTO socrative_users_systemmessage
                 (content, title, url, action_type, dismissible, status, audience)
             VALUES
                 ($1, $2, $3, $4, $5, $6, $7)`,
             [model.content, model.title, model.url, model.action, model.dismissible, Constants.BANNER_STATUS_ACTIVE, model.audience]
        );
        
        return result && result.command === 'INSERT' && result.rowCount == 1;
    }
    
    async loadAdmins() {
        let result = await this.executeQuery(
            `SELECT *
             FROM admins a
             JOIN admin_roles r
             ON r.user_id = a.id`
        );
        return result ? result.rows : null;
    }
    
    async insertAdmin(email, level, createdById) {
        let client = await this.getClient();
        
        if (!client) {
            return false;
        }
        
        try {
            await client.query('BEGIN');
            
            let result = await client.query(
                `INSERT INTO admins
                     (email, valid, created_by_id, register_date)
                 VALUES
                     ($1, $2, $3, $4)
                 RETURNING id`,
                 [email, true, createdById, new Date()]
            );
            
            await client.query(
                `INSERT INTO admin_roles
                     (level, user_id)
                 VALUES
                     ($1, $2)`,
                 [level, result.rows[0].id]
            );
    
            await client.query('COMMIT');
            
            return true;
        } catch (error) {
            logger.error('An error occurred during the insert admin queries:', error);
            try {
                await client.query('ROLLBACK');
            } catch (rollbackError) {
                logger.error('An error occurred rolling back the insert admin queries:', rollbackError);
            }
        } finally {
            client.release();
        }
    }
    
    async deleteAdmin(id) {
        let client = await this.getClient();
        
        if (!client) {
            return false;
        }
        
        try {
            await client.query('BEGIN');
            
            await client.query(
                `DELETE FROM admin_roles
                 WHERE user_id = $1`,
                 [id]
            );
    
            await client.query(
                `DELETE FROM admins
                 WHERE id = $1`,
                [id]
            );
    
            await client.query('COMMIT');
    
            return true;
        } catch (error) {
            logger.error('An error occurred during the delete admin queries:', error);
            try {
                await client.query('ROLLBACK');
            } catch (rollbackError) {
                logger.error('An error occurred rolling back the delete admin queries:', rollbackError);
            }
        } finally {
            client.release();
        }
    }
    
}

module.exports = new AdminDao();
