'use strict';

let BaseDao = require('./BaseDao');

class PartnerDao extends BaseDao {
    
    async getPartner(userId, partnerType) {
        let result = await this.executeQuery(
            `SELECT *
             FROM common_partner
             WHERE user_id = $1 AND type = $2
             LIMIT 1`,
            [userId, partnerType]
        );

        return result && result.rowCount === 0 ? null : result.rows[0];
    }
    
    async isPartnerType(userId, type) {
        let result = await this.executeReadQuery(
            `SELECT id
             FROM common_partner
             WHERE user_id = $1
             AND type = $2`,
            [userId, type]
        );
        
        return result && result.rowCount > 0;
    }
    
    async insertPartner(partnerModel) {
        let result = await this.executeQuery(
            `INSERT
             INTO common_partner
             (user_id, type, data)
             VALUES ($1, $2, $3)`,
            [partnerModel.user_id, partnerModel.type, partnerModel.data]
        );

        return result;
    }
    
    async updateData(id, data) {
        let result = await this.executeQuery(
            `UPDATE common_partner
             SET data = $1
             WHERE id = $2`,
            [data, id]
        );

        return result;
    }
    
}

module.exports = new PartnerDao();
