/**
 * Socrative Messaging
 * 
 * A pubnub wrapper that executes listener callbacks when messages are received or presence changes.
 * 
 * The following object is an example of how the listeners are represented:
 * 
 * {
 *     studentResponse: [ // message type
 *         callback1,
 *         callback2,
 *         ...
 *     ],
 *     presence: [ // message type
 *         callback3,
 *         callback4,
 *         ...
 *     ],
 *     ...
 * }
 * 
 */

let request   = require('Request'),
    Constants = require('Constants'),
    room      = require('room');

let listeners = {};

/**
 * Private function called every time a message is received from pubnub. Executes all callbacks listening for the message.
 * @param {object} message The message received from pubnub
 */
let messageReceived = (message) => {
    let callbacks = listeners[message.key];
    
    if (callbacks) {
        for (let callback of callbacks) {
            callback(message.data);
        }
    }
};

/**
 * Private function called every time the presence changes. Executes all callbacks listening for presence.
 * @param {object} data The presence data received from pubnub
 */
let presenceChanged = (data) => {
    let callbacks = listeners[Constants.PRESENCE];
    
    if (callbacks) {
        if (room.hasRoster()) {
            room.getStudentList({
                success: () => {
                    for (let callback of callbacks) {
                        callback();
                    }
                }
            });
        } else {
            for (let callback of callbacks) {
                callback(data);
            }
        }
    }
};

class Messaging {
    
    /**
     * Start listening to a channel for messages and/or presence.
     * @param {object} props An object with the following properties:
     *     {string} channel The name of the channel (required)
     *     {boolean} messages If true, subscribe to messages
     *     {boolean} presence If true, subscribe to presence
     */
    subscribe(props) {
        let {channel, messages, presence} = props;
        
        let listenerOptions = {};
        
        let subscribeOptions = {
            channels: [channel]
        };
        
        if (messages) {
            listenerOptions.message = (data) => {
                let message = JSON.parse(data.message);
                
                if (message.long_message_id) { // The message was too long to send via pubnub, so get it from the back end.
                    request.get({
                        url: `${window.backend_host}/socrative-tornado/api/long-message/${message.long_message_id}`,
                        success: (response) => {
                            message = response.message ? JSON.parse(response.message) : response;
                            messageReceived(message);
                        }
                    });
                } else {
                    messageReceived(message);
                }
            };
        }
        
        if (presence) {
            listenerOptions.presence = (data) => presenceChanged(data);
            subscribeOptions.withPresence = true;
        }
        
        pubnub.addListener(listenerOptions);
        pubnub.subscribe(subscribeOptions);
    }
    
    /**
     * Stop listening to a channel (both messages and presence).
     * @param {object} props An object with the following properties:
     *     {string|array} channel The channel name(s) (required)
     *     {function} callback Function to execute after unsubscribing (optional)
     */
    unsubscribe(props) {
        let {channel, callback} = props;
        
        pubnub.unsubscribe({
            channels: Array.isArray(channel) ? channel : [channel]
        });
        
        if (callback) {
            callback();
        }
    }
    
    /**
     * Register a callback that will be executed each time a specific message is received from pubnub.
     * @param {object} props An object with the following properties (required):
     *     {string} message The message to listen for (use Constants.PRESENCE for presence)
     *     {function} callback A function to be executed each time the given message is received
     */
    addListener(props) {
        let {message, callback} = props;
        
        if (!listeners[message]) {
            listeners[message] = [callback];
        } else {
            listeners[message].push(callback);
        }
    }
    
    /**
     * Remove a function from the pool of callbacks.
     * @param {object} props An object with the following properties (required):
     *     {string} message The message to stop listening for (use Constants.PRESENCE for presence)
     *     {function} callback The function to remove from the pool of callbacks
     */
    removeListener(props) {
        let {message, callback} = props;
        let callbacks = listeners[message];
        
        if (callbacks) {
            for (let i = 0; i < callbacks.length; i++) {
                if (callbacks[i] === callback) {
                    callbacks.splice(i, 1);
                    return;
                }
            }
        }
    }
    
}

module.exports = new Messaging();
