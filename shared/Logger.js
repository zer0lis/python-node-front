'use strict';

let winston = require('winston'),
    config = require('config').get('config');

let nodeEnv = process.env.NODE_ENV,
    nodeAppInstance = process.env.NODE_APP_INSTANCE,
    transports = [];

let timestamp = () => {
    let now = new Date(),
        month = now.getMonth() + 1 < 10 ? `0${now.getMonth() + 1}` : now.getMonth() + 1,
        date = now.getDate() < 10 ? `0${now.getDate()}` : now.getDate(),
        year = now.getFullYear(),
        hours = now.getHours() < 10 ? `0${now.getHours()}` : now.getHours(),
        minutes = now.getMinutes() < 10 ? `0${now.getMinutes()}` : now.getMinutes(),
        seconds = now.getSeconds() < 10 ? `0${now.getSeconds()}` : now.getSeconds(),
        milliseconds = now.getMilliseconds();
    
    if (milliseconds < 10) {
        milliseconds = `00${milliseconds}`;
    } else if (milliseconds < 100) {
        milliseconds = `0${milliseconds}`;
    }
    
    return `${month}-${date}-${year} ${hours}:${minutes}:${seconds}.${milliseconds}`;
};

let formatter = (options) => {
    let hasMeta = options.meta && Object.keys(options.meta).length > 0,
        meta = '';
    
    if (hasMeta) {
        try {
            // Try to stringify the entire meta. If that's not possible because the meta
            // has a circular structure, just try to get message and stack properties.
            meta = JSON.stringify(options.meta);
        } catch (error) {
            meta = JSON.stringify(options.meta, ['message', 'stack']);
        }
    }
    
    return `${options.timestamp()} [${options.level.toUpperCase()}] ${options.message}` + (hasMeta ? ` ${meta}` : '');
};

if (nodeEnv === 'localhost') {
    transports.push(new winston.transports.Console({
        timestamp: timestamp,
        formatter: formatter
    }));
} else {
    transports.push(new winston.transports.File({
        filename: '/var/log/nodejs/socrative.log',
        json: false,
        timestamp: timestamp,
        formatter: formatter
    }));
}

module.exports = new winston.Logger({
    level: nodeEnv === 'production' && nodeAppInstance !== 'admin' ? 'error' : 'debug',
    transports: transports
});
