'use strict';

let Constants = require('../Constants');

class AdminBannerModel {
    
    constructor() {
        this.action = ''; // The action the user can take with the banner (one of the BANNER_ACTION constants).
        this.audience = -1; // The audience that will see the banner (one of the BANNER_AUDIENCE constants).
        this.content = ''; // The main message delivered by the banner.
        this.dismissible = false; //Whether to force the banner to reappear at the next login
        this.id = 0; // The id of the banner in the database.
        this.title = ''; // The bold text title that appears in the top left of the banner.
        this.url = null; // The url for the banner, when the action is BANNER_ACTION_EXTERNAL_URL.
    }
    
    /*-------------------*/
    /*    Validations    */
    /*-------------------*/

    isValidAction() {
        return this.action === Constants.BANNER_ACTION_GO_PRO ||
               this.action === Constants.BANNER_ACTION_EXTERNAL_URL ||
               this.action === Constants.BANNER_ACTION_NONE;
    }
    
    isValidAudience() {
        return this.audience == Constants.BANNER_AUDIENCE_ALL ||
               this.audience == Constants.BANNER_AUDIENCE_FREE ||
               this.audience == Constants.BANNER_AUDIENCE_PRO;
    }

    isValidContent() {
        return this.content && this.content.length > 0;
    }

    isValidDismissible() {
        return this.dismissible === true || this.dismissible === false;
    }

    isValidId() {
        return this.id > 0;
    }
    
    isValidTitle() {
        return this.title && this.title.length > 0;
    }
    
    isValidUrl() {
        return this.url && this.url.length > 0;
    }
    
}

module.exports = AdminBannerModel;
