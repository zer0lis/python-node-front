'use strict';

let request = require('../Request'),
    Constants = require('../Constants'),
    validations = require('../Validations'),
    translate = require('../translations/translator').translate;

class LoginModel {
    
    constructor() {
        /*--------------*/
        /*    Client    */
        /*--------------*/
        this.errors = { // Collection of input errors.
            email: {error: false, message: ''},
            password: {error: false, message: ''}
        };
        
        /*--------------*/
        /*    Shared    */
        /*--------------*/
        this.email = ''; // The email entered by the teacher.
        this.password = ''; // The password entered by the teacher.
    }
    
    /*-------------------*/
    /*    Validations    */
    /*-------------------*/
    
    isValidEmail() {
        return validations.isValidEmail(this.email);
    }
    
    isValidLegacyPassword() {
        return this.password && this.password.length >= 1;
    }
    
    /*---------------*/
    /*    Helpers    */
    /*---------------*/
    
    setEmailError(message) {
        this.errors.email.error = true;
        this.errors.email.message = message;
    }
    
    /*-----------------------*/
    /*    Client Requests    */
    /*-----------------------*/
    
    signInZendesk(callback) {
        let auth = {};
        
        if (this.isValidEmail() && this.isValidLegacyPassword()) {
            auth.email = this.email;
            auth.password = this.password;
        }
        
        request.post({
            url: `${window.backend_host}/users/api/zendesk/sso/`,
            data: auth,
            success: (response) => {
                callback(response.redirect_url);
            },
            error: () => {
                callback();
            }
        });
    }
    
    signIn(callback) {
        request.post({
            url: `${window.backend_host}/users/login/`,
            data: {
                email: this.email,
                password: this.password
            },
            success: (data) => {
                callback(data);
            },
            error: (response) => {
                if (response.error) {
                    let code = response.error.code;
                    
                    if (code === Constants.INVALID_EMAIL ||
                        code === Constants.USER_NOT_FOUND ||
                        code === Constants.EMAIL_MISSING ||
                        code === Constants.PASSWORD_MISSING ||
                        code === Constants.LOGIN_FAILED ||
                        code === Constants.STRING_IS_TOO_LONG) {
                        this.setEmailError(translate('Your email or password is incorrect.'));
                    } else if (code === Constants.ACCOUNT_SUSPENDED) {
                        this.setEmailError(translate('Your account is suspended. Please contact support for details.'));
                    } else if (code === Constants.ACCOUNT_BLOCKED_FOR_FIVE_MINUTES) {
                        this.setEmailError(translate('Your account has been blocked for 5 minutes. Please contact support for details.'));
                    } else if (code === Constants.ACCOUNT_BLOCKED_FOR_ONE_HOUR) {
                        this.setEmailError(translate('Your account has been blocked for 1 hour. Please contact support for details.'));
                    } else if (code === Constants.ACCOUNT_BLOCKED_FOR_ONE_DAY) {
                        this.setEmailError(translate('Your account has been blocked for 1 day. Please contact support for details.'));
                    } else if (code === Constants.IP_BLOCKED_FOR_ONE_DAY) {
                        this.setEmailError(translate('Your IP address has been blocked. Please contact support for details.'));
                    }
                }
                
                callback();
            }
        });
    }
    
}

module.exports = LoginModel;
