'use strict';

let Constants = require('../Constants'),
    validations = require('../Validations'),
    request = require('../Request'),
    translate = require('../translations/translator').translate;

class FolderModel {
    
    constructor() {
        /*--------------*/
        /*    Client    */
        /*--------------*/
        this.callback = null; // Callback executed by a modal on close to pass data back to the controller that opened the modal.
        this.pageBlurred = false; // Whether the page has been blurred after opening a modal.
        
        /*--------------*/
        /*    Shared    */
        /*--------------*/
        this.createdDate = new Date(); // Date when this folder was created.
        this.deleted = false; // Whether the folder has been moved to the trash.
        this.hidden = false; // Whether the folder has been purged from the trash.
        this.hiddenDate = null; // Date when this folder was purged from the trash.
        this.id = 0; // The id of the folder.
        this.lastUpdated = new Date(); // Date when this folder was last updated.
        this.name = translate('Untitled Folder'); // The name of the folder.
        this.parentId = 0; // The id of the folder in which this folder resides.
        this.type = 0; // The type of the folder (one of the Constants.<TYPE>_FOLDER values).
        this.userId = 0; // The id of the user who owns this folder.
    }
    
    getCacheable() {
        return {
            createdDate: this.createdDate,
            deleted: this.deleted,
            hidden: this.hidden,
            hiddenDate: this.hiddenDate,
            id: this.id,
            lastUpdated: this.lastUpdated,
            name: this.name,
            parentId: this.parentId,
            type: this.type,
            userId: this.userId
        };
    }
    
    /*-------------------*/
    /*    Validations    */
    /*-------------------*/
    
    isValidDeleted() {
        return this.deleted === true || this.deleted === false;
    }
    
    isValidHidden() {
        return this.hidden === true || this.hidden === false;
    }
    
    isValidId() {
        return /^\d+$/.test(this.id) && this.id > 0;
    }
    
    isValidName() {
        return this.name && this.name.trim().length > 0 && this.name.length <= 255;
    }
    
    isValidParentId() {
        return validations.isValidParentId(this.parentId);
    }
    
    isValidType() {
        return this.type === Constants.QUIZ_FOLDER ||
               this.type === Constants.REPORT_FOLDER;
    }
    
    isValidUserId() {
        return /^\d+$/.test(this.userId) && this.userId > 0;
    }
    
    /*-----------------------*/
    /*    Client Requests    */
    /*-----------------------*/
    
    createFolder(callback) {
        request.post({
            url: `${window.teacher_service}/v3/folders/`,
            data: {
                name: this.name,
                parentId: this.parentId || Constants.ROOT_FOLDER,
                type: this.type
            },
            success: (data) => {
                callback(data.id, data.name);
            },
            error: () => {
                callback();
            }
        });
    }
    
    renameFolder(callback) {
        request.put({
            url: `${window.teacher_service}/v3/folders/rename/${this.id}`,
            data: {
                name: this.name
            },
            success: () => {
                callback(this.id, this.name);
            },
            error: () => {
                callback();
            }
        });
    }
    
}

module.exports = FolderModel;
