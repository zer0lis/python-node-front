var Backbone  = require('backbone'),
    _         = require('underscore'),
    messaging = require('Messaging'),
    Constants = require('Constants'),
    request   = require('Request'),
    room      = require('room'),
    user      = require('user'),
    store     = require('store'),
    Quiz      = require('quiz-model'),
    popup     = require('PopupController'),
    translate = require('translator').translate;

var Activity = Backbone.Model.extend({
    
    /*-----------------------------*/
    /*    Activity Data Methods    */
    /*-----------------------------*/
    
    getId: function() {
        return activity.get('id');
    },
    
    getQuizId: function() {
        return activity.get('quiz').id;
    },
    
    setFinalResultQuizId: function(id) {
        activity.set('finalResultQuizId', id);
    },
    
    getFinalResultQuizId: function() {
        return activity.get('finalResultQuizId');
    },
    
    hasFinalResultQuizId: function() {
        return activity.has('finalResultQuizId');
    },
    
    setFinalResultActivityId: function(id) {
        activity.set('finalResultActivityId', id);
    },
    
    getFinalResultActivityId: function() {
        return activity.get('finalResultActivityId');
    },
    
    getCurrentQuestionNumber: function() {
        // The 'activity_state' property stores the index of the current question, with one very important
        // exception: If the current question is the first question, 'activity_state' will be -1 (not 0).
        var questionIndex = activity.get('activity_state');
        return questionIndex == -1 ? 1 : questionIndex + 1;
    },
    
    getResponses: function(isLive) {
        let responseType = isLive ? 'responses' : 'final_result_responses';
        let responses = [];
        if (activity.has(responseType)) {
            for (let response of activity.get(responseType)) {
                if (!response.hidden) {
                    responses.push(response);
                }
            }
        }
        return responses;
    },
    
    setResponseNames: function(isLive, responsesByUser) {
        var names = activity.get(isLive ? 'student_names' : 'final_result_student_names');
        _.each(responsesByUser, (response) => {
            response.student_name = names[response.user_uuid];
        });
    },
    
    
    /*---------------------------------------*/
    /*    Activity Type Codes and Methods    */
    /*---------------------------------------*/
    
    QUIZ:       'QZ',
    SPACE_RACE: 'SR',
    NO_REPORT:  'NR', // The type code for QQ-TF and QQ-MC activities, because we don't provide reports for them.
    QQ_SA:      '1Q',
    VOTE:       '1V',
    
    isQuickQuestion: function() {
        var type = activity.get('activity_type');
        return type === activity.QQ_SA || type === activity.NO_REPORT;
    },
    
    isQuickQuestionShortAnswer: function() {
        var quiz = activity.get('quiz');
        var typeIsFR = quiz && quiz.questions[0].get('type') === 'FR';
        return activity.isQuickQuestion() && typeIsFR;
    },
    
    isVote: function() {
        return activity.get('activity_type') === activity.VOTE;
    },
    
    isQuiz: function() {
        return activity.get('activity_type') === activity.QUIZ;
    },
    
    isSpaceRace: function() {
        return activity.get('activity_type') === activity.SPACE_RACE;
    },
    
    isNonSpaceRace: function() {
        return activity.has('activity_type') && (activity.isQuiz() || activity.isQuickQuestion() || activity.isVote());
    },
    
    getDisplayType: function(type, name) {
        if (type === activity.SPACE_RACE) {
            return translate('Space Race')
        } else if (type === activity.QUIZ) {
            return name === 'Exit Ticket Quiz' ? translate('Final Survey') : translate('Quiz')
        } else if (type === activity.QQ_SA) {
            return translate('Short Answer')
        }
    },
    
    
    /*---------------------------------*/
    /*    Activity Settings Methods    */
    /*---------------------------------*/
    
    isTeacherPaced: function() {
        return activity.has('settings') && activity.get('settings').pacing === 'teacher';
    },
    
    needsStudentName: function() {
        return activity.has('settings') && activity.get('settings').require_names;
    },
    
    
    /*---------------------------------*/
    /*    Activity Lifetime Methods    */
    /*---------------------------------*/
    
    prepareForNewActivity: function() {
        activity.stopCountdown();
        activity.clear({silent: true});
    },
    
    isInProgress: function() {
        return activity.has('activity_id');
    },
    
    isQuizRunning: function() {
        return activity.has('id') && activity.has('quiz');
    },
    
    endCurrentActivity: function(callback, keepData) {
        let data = new FormData();
        data.append('room_name', room.get('name'));
        
        request.post({
            url: `${window.backend_host}/rooms/api/end-current-activity/`,
            data: data,
            success: () => {
                if (!keepData) {
                    activity.clear();
                }
                if (callback) {
                    callback();
                }
            },
            error: (response) => {
                console.error(response);
            }
        });
    },
    
    
    /*----------------------------------*/
    /*    Activity Countdown Methods    */
    /*----------------------------------*/
    
    setCountdown: function(seconds) {
        activity.set({countdown: seconds}, {silent: true});
    },
    
    getCountdown: function() {
        return activity.get('countdown');
    },
    
    hasCountdown: function() {
        return activity.has('countdown');
    },
    
    setCountdownStarted: function(started) {
        activity.set({countdownStarted: started}, {silent: true});
    },
    
    isCountdownStarted: function() {
        return activity.has('countdownStarted') && 
               activity.get('countdownStarted');
    },
    
    shouldStartCountdown: function() {
        return activity.hasCountdown() &&
               activity.getCountdown() !== -1 &&
              !activity.isCountdownStarted();
    },
    
    startCountdown: function() {
        activity.setFinishedOnRefresh(false);
        activity.setCountdownStarted(true);
        activity.setCountdownId(window.setInterval(activity.updateCountdown, 1000));
    },
    
    addCountdownCallback: function(callback) {
        var countdownCallbacks = activity.getCountdownCallbacks();
        if (countdownCallbacks) {
            for (var i = 0; i < countdownCallbacks.length; i++) {
                if (callback === countdownCallbacks[i]) {
                    return;
                }
            }
        } else {
            countdownCallbacks = [];
            activity.setCountdownCallbacks(countdownCallbacks);
        }
        countdownCallbacks.push(callback);
    },
    
    setCountdownCallbacks: function(value) {
        this.set('countdownCallbacks', value);
    },
    
    getCountdownCallbacks: function() {
        return this.get('countdownCallbacks');
    },
    
    setCountdownId: function(id) {
        activity.set({countdownId: id}, {silent: true});
    },
    
    getCountdownId: function() {
        return activity.get('countdownId');
    },
    
    updateCountdown: function() {
        activity.setCountdown(activity.getCountdown() - 1);
        if (activity.getCountdown() === 0) {
            activity.stopCountdown();
            activity.endCurrentActivity(null, true);
        }
        var countdownCallbacks = activity.getCountdownCallbacks();
        if (countdownCallbacks) {
            for (var i = 0; i < countdownCallbacks.length; i++) {
                var callback = countdownCallbacks[i];
                callback();
            }
        }
    },
    
    stopCountdown: function() {
        window.clearInterval(activity.getCountdownId());
    },
    
    shouldShowCountdown: function() {
        return activity.hasCountdown() &&
               activity.getCountdown() != -1;
    },
    
    getCountdownMinutes: function() {
        return Math.floor(activity.getCountdown() / 60);
    },
    
    getCountdownSeconds: function() {
        return Math.floor(activity.getCountdown() % 60);
    },
    
    getCountdownDisplay: function() {
        var minutes = activity.getCountdownMinutes();
        var seconds = activity.getCountdownSeconds();
        if (seconds > -1 && seconds < 10) {
            seconds = '0' + seconds;
        }
        return minutes + ':' + seconds;
    },

    /**
     * Stop the countdown and reset its properties.
     * @param {number} seconds The optional number of seconds for the countdown. Defaults to -1 (disabled).
     */
    resetCountdown: function(seconds) {
        activity.stopCountdown();
        activity.setCountdown(seconds || -1);
        activity.setCountdownStarted(false);
    },
    
    isCountdownDone: function() {
        return activity.hasCountdown() &&
               activity.getCountdown() === 0;
    },
    
    hasCountdownAfterRefresh: function() {
        var settings = activity.get('settings');
        return settings && settings.seconds && settings.seconds > -1;
    },
    
    getCountdownAfterRefresh: function() {
        return activity.get('settings').seconds - Math.floor(Date.now() / 1000 - activity.get('start_time'));
    },
    
    setFinishedOnRefresh: function(value) {
        this.set('finishedOnRefresh', value);
    },
    
    getFinishedOnRefresh: function() {
        return this.get('finishedOnRefresh');
    },
    
    handleCountdownAfterRefresh: function() {
        if (activity.hasCountdownAfterRefresh()) {
            var timeLeft = activity.getCountdownAfterRefresh();
            if (timeLeft > 0) {
                if (store.get('countdownStopped') !== true) {
                    activity.resetCountdown(timeLeft);
                    activity.startCountdown();
                }
            } else {
                activity.setCountdown(0);
                activity.setCountdownStarted(true);
                activity.setFinishedOnRefresh(true);
                activity.endCurrentActivity(null, true);
            }
        }
    }
    
});

var activity = new Activity(); // TODO .... Move this to the bottom (right above the export) when all methods are defined in Backbone.Model.extend().

activity.checkBeforeStartingActivity = function(callback, options) {
    if (activity.isInProgress()) {
        if (!activity.isQuickQuestion() || activity.isQuickQuestionShortAnswer()) {
            popup.render({
                title: translate('Please Confirm'),
                message: translate('Would you like to stop the current activity and start a new one?'),
                buttonText: translate('Yes'),
                cancelText: translate('No'),
                buttonClicked: () => {
                    if (callback) {
                        callback(options);
                    }
                },
                cancelClicked: () => {
                    if (options && options.error) {
                        options.error();
                    }
                }
            });
            return;
        }
    }
    if (callback) {
        callback(options);
    }
};

activity.validateCachedQuiz = function() {
    if (!activity.has('quiz')) {
        return false;
    }

    var quiz = activity.get('quiz');

    if (!(_.isObject(quiz) &&
          _.isFunction(quiz.has) &&
          _.isFunction(quiz.get)) ||
          quiz.get('id') !== activity.get('activity_id') ||
          !_.isArray(quiz.questions)) {
        activity.unset('quiz');
        return false;
    }

    return true;
};

activity.getCurrentQuiz = function(callback) {
    if (!this.has('activity_id')) {
        callback(null);
        return;
    }
    if (activity.validateCachedQuiz()) {
        callback(this.get('quiz'));
    } else {
        var currentQuiz = new Quiz({ id: this.get('activity_id') });
        currentQuiz.fetch({
            success: function(){
                currentQuiz.initialize();
                activity.set('quiz', currentQuiz);
                callback(currentQuiz);
            }
        });
    }
};

activity.loadFinalResult = function(activityId, quizId, callback) {
    if (activity.getFinalResultActivityId() == activityId &&
        activity.getFinalResultQuizId() == quizId) {
        callback(true);
        return; // If we already have the final result data, don't load it again (SOC-957).
    }
    request.get({
        url: `${window.backend_host}/activities/api/report/${activityId}`,
        success: (data) => {
            let studentNames = {};
            if (data.student_names.length > 0 && data.student_names[0].student_id) {
                activity.set('final_result_rostered', true);
                _.each(data.student_names, (student) => {
                    studentNames[student.student_id] = student.name;
                });
            } else {
                activity.set('final_result_rostered', false);
                _.each(data.student_names, (student) => {
                    studentNames[student.user_uuid] = student.name;
                });
            }
            
            activity.set('final_result_students', data.student_names);
            activity.set('final_result_student_names', studentNames);
            activity.set('final_result_responses', data.responses);
            activity.set('final_result_activity_type', data.activity_type);
            activity.set('final_result_date', new Date(data.start_time * 1000).toDateString());
            activity.setFinalResultActivityId(activityId);
            activity.setFinalResultQuizId(quizId);
            
            let finalResult = new Quiz({id: quizId});
            finalResult.fetch({
                success: () => {
                    finalResult.initialize();
                    activity.set('final_result', finalResult);
                    callback(true);
                },
                error: (model, response, options) => {
                    console.error('Error fetching quiz for final results:');
                    console.error(model);
                    console.error(response);
                    console.error(options);
                    callback();
                }
            });
        },
        error: (response) => {
            console.error(response);
            callback();
        }
    });
};

activity.setStudentName = function(name, complete) {
    if (user.isTeacher()) {
        throw 'Exception: only students should be setting their name';
    }
    request.post({
        url: `${window.backend_host}/students/api/set-name/`,
        data: {
            activity_instance_id: activity.get('id'),
            student_name: name
        },
        success: () => {
            if (_.isFunction(complete)) {
                complete();
            }
        },
        error: (response) => {
            console.error(response);
        }
    });
};

activity.setSettings = function(data, options) {
    if (_.isArray(data.settings)) {
        var settings = data.settings;
        var new_settings = {};
        for (var i = settings.length - 1; i >= 0; i--) {
            var key = settings[i].key;
            if (_.contains(['pacing', 'team_assignment_type', 'team_count', 'icon_type'], key)) {
                new_settings[key] = settings[i].value;
            } else if (key === 'seconds') {
                var seconds = parseInt(settings[i].value);
                new_settings[key] = isNaN(seconds) ? -1 : seconds;
            } else {
                new_settings[key] = settings[i].value.toLowerCase() === 'true';
            }
        }
        data.settings = new_settings;
    }

    activity.set(data, options);
};

activity.reset = function() {
    activity.__hasRefreshed = false;
    activity.__refreshing = false;
    activity.__refreshingCallbacks = [];
    activity.clear();
};

activity.__hasRefreshed = false;
activity.__refreshing = false;
activity.__refreshingCallbacks = [];

activity.refresh = function(success) {
    // If we've already refreshed this session, just return the refreshments.
    // Any changes that happen to the activity should come in from pubnub,
    // so we shouldn't have to hit the DB again to have up-to-date data.
    if (this.__hasRefreshed) {
        _.defer(() => {
            success();
        });
        return;
    }

    this.__refreshingCallbacks.push(success);

    if (!this.__refreshing) {
        var that = this;
        this.__refreshing = true;

        this.__refresh(
            // success
            function(result) {
                that.__refreshing = false;
                that.__hasRefreshed = true;

                _.each(that.__refreshingCallbacks, function(callback) {
                    callback();
                });
                
                that.__refreshingCallbacks.length = 0;
            }
        );
    }
};

activity.__refresh = function(success) {
    var runRefresh = function() {
        var runSuccessCallback = function() {
            if (!_.isUndefined(success)) {
                success();
            }
        };

        if (!activity.has('started_time')) {
            var roomName = room.get('name');
            if (_.isUndefined(roomName)) {
                throw "InvalidOperationException: room_name needs to be set before loading the local activity.";
            }
            
            request.get({
                url: `${window.backend_host}/rooms/api/current-activity/${roomName}`,
                success: (data) => {
                    if (!data) {
                        runSuccessCallback(); // There is no activity.
                        return;
                    }
                    
                    var studentNames = {};
                    for (var i in data.student_names) {
                        var studentNameInfo = data.student_names[i];
                        studentNames[studentNameInfo.user_uuid] = studentNameInfo.name;
                    }
                    data.student_names = studentNames;
                    
                    activity.setSettings(data);
                    
                    if (user.isTeacher()) {
                        activity.handleCountdownAfterRefresh();
                    }
                    
                    if (activity.has('quiz')) {
                        if (_.isUndefined(activity.get('quiz').cid)) {
                            activity.set('quiz', new Quiz(activity.get('quiz')));
                        }
                        
                        runSuccessCallback();
                    } else {
                        runSuccessCallback();
                    }
                },
                error: (response) => {
                    console.error(response);
                }
            });
        } else {
            runSuccessCallback();
        }
    };
    
    runRefresh();
};

/**
 * Load the quiz and responses associated with the current activity. When loading is successful, the quiz
 * and responses will be available at {@code activity.get('quiz')} and {@code activity.get('responses')}.
 * @param {function} successCallback The function to execute when the quiz and responses have loaded.
 * @param {function} errorCallback The function to execute if a loading error occurs.
 */
activity.loadQuizAndResponses = function(successCallback, errorCallback) {
    request.get({
        url: `${window.backend_host}/quizzes/api/quiz/${activity.get('activity_id')}?room_name=${room.get('name')}`,
        success: (data) => {
            activity.set({quiz: data}, {silent: true});
            request.get({
                url: `${window.backend_host}/students/api/responses/${activity.get('id')}/`,
                success: (data) => {
                    activity.set({responses: data}, {silent: true});
                    if (successCallback) {
                        successCallback();
                    }
                },
                error: (response) => {
                    console.error('Error getting student responses:', response);
                    if (errorCallback) {
                        errorCallback();
                    }
                }
            });
        },
        error: (response) => {
            if (response.error && response.error.code === Constants.STUDENT_NOT_FOUND) {
                user.logout(() => {
                    window.location.href = '/login/student/';
                });
            }
            console.error('Error getting student quiz:', response);
        }
    });
};

activity.handleStudentResponse = function(data) {
    data = JSON.parse(data);
    
    if (data.activity_instance !== activity.get('id')) {
        return;
    }
    
    activity.updateStudentName({
        user_uuid: data.user_uuid,
        student_name: data.student_name
    }, true);
    
    let responses = _.clone(activity.get('responses')) || []; // Clone the responses to force a Backbone change event.
    let existingResponse = _.findWhere(responses, {id: data.id});
    
    if (!existingResponse || activity.get('settings').allow_repeat_responses) {
        responses.push(data);
    } else {
        responses[responses.indexOf(existingResponse)] = data;
    }
    
    activity.set('responses', responses);
};

activity.updateStudentName = function(studentNameData, silent) {
    var studentNames = {};

    if (activity.has('student_names')) {
        _.extend(studentNames, activity.get('student_names'));
    }

    studentNames[studentNameData.user_uuid] = studentNameData.student_name;
    activity.set('student_names', studentNames, {silent: silent});
};

if (user.isStudent()) {
    messaging.addListener({
        message: Constants.ACTIVITY_CHANGE,
        callback: (data) => {
            user.unset('team');
            if (data === 'activity_ended') {
                user.unset('user_input_name');
                activity.clear();
            } else {
                activity.setSettings(data);
            }
        }
    });
}

if (user.isTeacher()) {
    messaging.addListener({
        message: Constants.STUDENT_RESPONSE,
        callback: activity.handleStudentResponse
    });
    messaging.addListener({
        message: Constants.STUDENT_NAME_SET,
        callback: activity.updateStudentName
    });
}

activity.hideResponse = function(responseId, callback) {
    if (activity.has('responses')) {
        let existingResponse = _.findWhere(activity.get('responses'), {
            id: responseId
        });

        if (existingResponse) {
            let data = new FormData();
            data.append('response_id', responseId);
            data.append('room_name', room.get('name'));
            
            request.post({
                url: `${window.backend_host}/students/api/hide-response/`,
                data: data,
                success: () => {
                    existingResponse.hidden = true;
                    callback();
                },
                error: (response) => {
                    console.error(response);
                }
            });
        }
    }
};

activity.startQuiz = function(options) {
    request.post({
        url: `${window.backend_host}/lecturers/api/start-quiz/`,
        data: options.settings,
        success: (data) => {
            activity.prepareForNewActivity();
            activity.setSettings(data, {silent: true});
            activity.set({pacing: options.pacing}, {silent: true});
            activity.set({hideStudentResponses: false, hideStudentName: false}, {silent: true});
            options.success();
        },
        error: (response) => {
            console.error(response);
        }
    });
};

activity.startQuickQuestion = function(options) {
    if (options.type.toLowerCase() === 'fr') {
        activity.startQuickQuestionConfirmed(options);
    } else {
        this.checkBeforeStartingActivity(activity.startQuickQuestionConfirmed, options);
    }
};

activity.startQuickQuestionConfirmed = function(options) {
    request.post({
        url: `${window.backend_host}/lecturers/api/start-quick-question/`,
        data: {
            room_name: room.get('name'),
            question_type: options.type,
            question_text: options.text,
            allow_repeat_responses: options.allow_repeat_responses,
            require_names: options.require_names,
            one_attempt: options.one_attempt
        },
        success: (data) => {
            activity.prepareForNewActivity();
            activity.setSettings(data, {silent: true});
            activity.set({pacing: 'teacher'}, {silent: true});
            if (options.callback) {
                options.callback();
            }
        },
        error: (response) => {
            console.error(response);
        }
    });
};

activity.convertCurrentQuickQuestionToSurvey = function(callback) {
    request.post({
        url: `${window.backend_host}/lecturers/api/convert-quick-question-to-vote/`,
        data: {
            room_name: room.get('name'),
            require_names: false
        },
        success: (data) => {
            activity.setSettings(data, {silent: true});
            activity.set({pacing: 'teacher'}, {silent: true});
            if (_.isFunction(callback)) {
                callback();
            }
        },
        error: (response) => {
            console.error(response);
        }
    });
};

activity.startSpaceRace = function(options) {
    request.post({
        url: `${window.backend_host}/lecturers/api/start-space-race/`,
        data: options.settings,
        success: (data) => {
            activity.prepareForNewActivity();
            activity.setSettings(data, {silent: true});
            options.success();
        },
        error: (response) => {
            if (_.isFunction(options.error)) {
                options.error();
            }
            console.error(response);
        }
    });
};

activity.startExitTicket = function(options) {
    this.checkBeforeStartingActivity(function(options) {
        request.post({
            url: `${window.backend_host}/lecturers/api/start-exit-ticket/`,
            data: {
                room_name: room.get('name')
            },
            success: (data) => {
                activity.prepareForNewActivity();
                activity.setSettings(data, {silent: true});
                activity.set({hideStudentResponses: false, hideStudentName: false}, {silent: true});
                if (_.isObject(options) && _.isFunction(options.success)) {
                    options.success();  
                }
            },
            error: (response) => {
                console.error(response);
                if (options.error) {
                    options.error(response);
                }
            }
        });
    }, options);

};

module.exports = activity;