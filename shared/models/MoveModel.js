'use strict';

let validations = require('../Validations'),
    Constants = require('../Constants'),
    request = require('../Request');

class MoveModel {
    
    constructor() {
        /*--------------*/
        /*    Client    */
        /*--------------*/
        this.bannerIsError = false; // Whether the banner is depicting error (with a red background) or success (with a green background).
        this.bannerText = ''; // Text displayed at the top of the move modal on success/error events.
        this.bannerTimeoutId = 0; // The id of the timer that counts how long the banner is displayed. Pass this to window.clearTimeout() to stop the timer.
        this.callback = null; // Callback executed by the move modal on close to pass data back to the controller that opened the modal.
        this.creatingFolder = false; // Whether the move modal is creating a new folder for the teacher (network request in progress).
        this.currentFolder = null; // The folder whose context is currently being displayed.
        this.destinationFolder = null; // The folder into which the items will be moved.
        this.folderNavModel = null; // An instance of FolderNavModel that represents the folders as a tree data structure.
        this.moving = false; // Whether the move operation is underway (network request in progress).
        this.newFolderName = ''; // The name of a new folder being created in the move modal.
        this.newFolderNameFocused = false; // Whether the input for the new folder name has been focused.
        this.pageBlurred = false; // Whether the page has been blurred after opening the move modal.
        this.selectedFolder = null; // The folder in the folder list that is currently selected.
        this.startingFolder = null; // The folder in whose context the move modal was originally opened.
        this.typingFolderName = false; // Whether the teacher is currently typing a name for a new folder in the move modal.
        
        /*--------------*/
        /*    Shared    */
        /*--------------*/
        this.deleted = false; // Whether the items are being deleted or restored.
        this.folderIds = []; // The ids of the folders being moved.
        this.quizIds = []; // The ids of the quizzes being moved.
        this.parentId = -1; // The id of the folder into which the quizzes and/or quiz folders are being moved (null means the root folder).
        this.task = null; // One of the move tasks in Constants.js.
        this.type = 0; // The type of the destination folder (one of the <TYPE>_FOLDER constants).
        this.userId = 0; // The id of the teacher performing the operation.
    }
    
    /*-------------------*/
    /*    Validations    */
    /*-------------------*/
    
    isValidParentId() {
        return validations.isValidParentId(this.parentId);
    }
    
    isValidIds() {
        return this.folderIds &&
               this.quizIds &&
               this.folderIds.length + this.quizIds.length > 0 &&
               !this.folderIds.includes(this.parentId);
    }
    
    isValidTask() {
        return this.task === Constants.DELETE_TASK ||
               this.task === Constants.MOVE_TASK ||
               this.task === Constants.RESTORE_TASK;
    }
    
    /*-----------------------*/
    /*    Client Requests    */
    /*-----------------------*/
    
    moveQuizzes(callback) {
        request.put({
            url: `${window.teacher_service}/v3/quizzes/move/`,
            data: {
                folderIds: this.folderIds,
                quizIds: this.quizIds,
                parentId: this.destinationFolder.id,
                task: Constants.MOVE_TASK
            },
            success: () => {
                callback(true);
            },
            error: () => {
                callback();
            }
        });
    }
    
}

module.exports = MoveModel;
