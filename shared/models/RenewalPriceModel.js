'use strict';

class RenewalPriceModel {
    
    constructor() {
        this.amount = 0; // The amount of the renewal price (stored as pennies in the database).
        this.id = 0; // The id of the renewal price.
        this.organizationType = null; // The organization type. One of the ORGANIZATION_<TYPE> constants or null.
    }
    
    /*-------------------*/
    /*    Validations    */
    /*-------------------*/
    
    isValidAmount() {
        return /^\d{1,3}\.\d{2}$/.test(this.amount);
    }
    
    isValidId() {
        return this.id > 0;
    }
    
    isValidOrganizationType() {
        return this.organizationType === Constants.ORGANIZATION_CORP ||
               this.organizationType === Constants.ORGANIZATION_HIGHER_ED ||
               this.organizationType === Constants.ORGANIZATION_K12 ||
               this.organizationType === Constants.ORGANIZATION_OTHER ||
               this.organizationType === null;
    }
    
}

module.exports = RenewalPriceModel;
