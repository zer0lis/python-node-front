'use strict';

let validations = require('../Validations'),
    Constants = require('../Constants'),
    teacher = require('./TeacherModel'),
    request   = require('../Request'),
    translate = require('../translations/translator').translate;

class ImportQuizModel {
    
    constructor() {
        /*--------------*/
        /*    Client    */
        /*--------------*/
        this.excelErrors = ''; // The error(s) displayed if excel file upload fails.
        this.fileName = ''; // The name of the excel file selected for upload.
        this.importingByExcel = false; // Whether the client is attempting to import an Excel quiz (network request in progress).
        this.importingBySoc = false; // Whether the client is attempting to import a quiz by SOC number (network request in progress).
        this.importResultCallback = () => {}; // Callback function executed after the import attempt is finished.
        this.pageBlurred = false; // Whether the page has been blurred after opening the import quiz modal.
        this.progress = 0; // The progress of the excel file upload (displayed as a percentage).
        this.showProgress = false; // Whether the progress bar for file upload should be visible.
        this.socNumberStatus = {}; // The status object for displaying an error icon and message in the SOC number input.
        
        /*---------------*/
        /*    Service    */
        /*---------------*/
        this.file = null; // The Excel file received by the /v3/quizzes/import?type=Constants.EXCEL_TYPE API.
        
        /*--------------*/
        /*    Shared    */
        /*--------------*/
        let parentId = teacher.currentQuizFolderId;
        
        if (parentId === Constants.NO_FOLDER || parentId === Constants.TRASH) {
            parentId = Constants.ROOT_FOLDER;
        }
        
        this.parentId = parentId; // The id of the folder into which the quiz is being imported.
        this.socNumber = ''; // The SOC number of the quiz being imported.
    }
    
    /*-------------------*/
    /*    Validations    */
    /*-------------------*/
    
    isValidFile() {
        return validations.isValidExcelFile(this.file);
    }
    
    isValidParentId() {
        return validations.isValidParentId(this.parentId);
    }
    
    isValidSocNumber() {
        return this.socNumber && this.socNumber.length > 0;
    }
    
    /*-----------------------*/
    /*    Client Requests    */
    /*-----------------------*/
    
    importBySoc(callback) {
        request.post({
            url: `${window.teacher_service}/v3/quizzes/import?type=${Constants.SOC_TYPE}`,
            data: {
                parentId: this.parentId,
                socNumber: this.socNumber.replace(/[A-Za-z$-]/g, '')
            },
            success: (quiz) => {
                callback(quiz);
            },
            error: () => {
                callback();
            }
        });
    }
    
    importByExcel(file, progressCallback, doneCallback) {
        let data = new FormData();
        data.append('file', file);
        data.append('parentId', this.parentId);
        
        request.post({
            url: `${window.teacher_service}/v3/quizzes/import?type=${Constants.EXCEL_TYPE}`,
            data: data,
            progress: (event) => {
                if (event.lengthComputable) {
                    progressCallback(event.loaded / event.total * 100);
                }
            },
            success: (quiz) => {
                doneCallback(false, quiz);
            },
            error: (response) => {
                doneCallback(response.errors || {});
            }
        });
    }
    
}

module.exports = ImportQuizModel;
