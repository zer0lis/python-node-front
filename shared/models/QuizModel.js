'use strict';

let Constants = require('../Constants'),
    teacher = require('./TeacherModel'),
    validations = require('../Validations'),
    request = require('../Request');

class QuizModel {
    
    constructor() {
        /*--------------*/
        /*    Client    */
        /*--------------*/
        this.callback = null; // Callback function used by modals (e.g. Copy Quiz) to pass results back to the caller.
        
        /*--------------*/
        /*    Shared    */
        /*--------------*/
        this.createdById = 0; // The id of the user who created this quiz.
        this.createdDate =  null; // Date when this quiz was created.
        this.id = 0; // The id of this quiz.
        this.isHidden = false; // Whether or not this quiz has been purged.
        this.lastUpdated = null; // Date when this quiz was last updated.
        this.name = ''; // The name of this quiz specified by the teacher.
        this.parentId = 0; // The id of the folder in which this quiz resides.
        this.socNumber = ''; // The SOC number of this quiz.
        this.sharable = true; // Whether or not this quiz can be imported by other teachers.
    }
    
    /*-------------------*/
    /*    Validations    */
    /*-------------------*/
    
    isValidParentId() {
        return validations.isValidParentId(this.parentId);
    }

    isValidSocNumber() {
        return this.socNumber && this.socNumber.length > 0;
    }

    isValidName() {
        return this.name && 
               this.name.length > 0 &&
               this.name.length <= 255;
    }
    
    /*-----------------------*/
    /*    Client Requests    */
    /*-----------------------*/
    
    copyQuiz(callback) {
        let parentId = teacher.currentQuizFolderId;
        
        if (parentId === Constants.NO_FOLDER || parentId === Constants.TRASH) {
            parentId = Constants.ROOT_FOLDER;
        }
        
        request.post({
            url: `${window.teacher_service}/v3/quizzes/copy/`,
            data: {
                name: this.name,
                parentId: parentId,
                socNumber: this.socNumber
            },
            success: (quiz) => {
                quiz.date = new Date(quiz.date);
                callback(quiz);
            },
            error: () => {
                callback();
            }
        });
    }
    
}

module.exports = QuizModel;
