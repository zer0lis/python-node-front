'use strict';

let Constants = require('../Constants');

class UpdateFoldersModel {

    constructor() {
        /*--------------*/
        /*    Client    */
        /*--------------*/


        /*--------------*/
        /*    Shared    */
        /*--------------*/
        this.deleted = false; // Whether the folder will be moved to trash or not.
        this.folderIds = 0; // The ids of the folders to update.
        this.parentId = 0; // The id of the folder in which these folder will reside.
        this.type = 0; //type of the folders.
        this.userId = 0; // The id of the user who owns these folders.
    }

    isValidDeleted() {
        return this.deleted === true || this.deleted === false;
    }

    isValidFolderIds() {

        for(let folderId of this.folderIds)
            if (folderId <= 0)
                return false;

        return true;
    }

    isValidParentId() {
        return this.parentId === null || this.parentId  || (this.folderIds.indexOf(this.parentId) >= 0); // the parentId can't be in the folderIds
    }

    isValidType() {
        return this.type === Constants.QUIZ_FOLDER ||
            this.type === Constants.REPORT_FOLDER;
    }

    isValidUserId() {
        return this.userId > 0;
    }
}

module.exports = UpdateFoldersModel;