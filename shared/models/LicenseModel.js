'use strict';

let request   = require('../Request'),
    Constants = require('../Constants'),
    utils = require('../Utils'),
    translate = require('../translations/translator').translate;

class LicenseModel {
    
    constructor() {
        /*--------------*/
        /*    Client    */
        /*--------------*/
        this.activateCallback = null; // Callback to execute when the license activation modal is closed.
        this.activating = false; // Whether the client is currently activating a license.
        this.cardType = ''; // The type of credit card icon displayed during license purchase (see the types in Constants.js).
        this.couponSuccess = false; // Whether the client has successfully looked up a valid coupon by name.
        this.errors = { // Collection of license input errors. Set errors/messages with the setError() helper method.
            couponName: {error: false, message: ''},
            nameOnCard: {error: false, message: ''},
            cardNumber: {error: false, message: ''},
            month: {error: false, message: ''},
            year: {error: false, message: ''},
            cvc: {error: false, message: ''},
            licenseKey: {error: false, message: ''}
        };
        this.fetchingCoupon = false; // Whether the client is currently looking up a coupon by name.
        this.licenseStep = Constants.LICENSE_STEP; // Which license step the client is rendering at the moment.
        this.makingPurchase = false; // Whether the client is currently making a purchase.
        this.nameOnCard = ''; // The name on the credit card entered by the buyer.
        this.pageBlurred = false; // Whether the page has been blurred after opening the license activation modal.
        this.pricePerSeatFull = 0; // The price per seat before the coupon discount.
        this.renewalDate = new Date(); // The date displayed to the buyer when the license will need to be renewed.
        
        /*--------------*/
        /*    Shared    */
        /*--------------*/
        this.applyNow = true; // Whether one seat of the license should be activated at purchase time.
        this.autoRenew = true; // Whether the license should be renewed automatically.
        this.buyerId = 0; // The ID of the Socrative user buying the license (from the "socrative_users_socrativeuser" table in the database).
        this.cardNumber = ''; // The credit card number entered by the buyer.
        this.coupon = null; // An object representing a coupon from the "coupons" table in the database.
        this.couponName = ''; // The name of the coupon entered by the buyer.
        this.cvc = ''; // The card verification code entered by the buyer.
        this.expectedTotalPrice = -1; // The total price the buyer can expect to pay (considering seats, years, coupon, and prices).
        this.id = 0; // The ID of a license (from the "licenses" table in the database).
        this.key = ''; // The key of a license (from the "licenses" table in the database).
        this.month = ''; // The credit card expiration month entered by the buyer.
        this.pricePerSeat = 0; // The price per seat the buyer can expect to pay (considering seats, years, coupon, and prices).
        this.prices = []; // The prices from the "prices" table in the database.
        this.seats = 0; // The number of seats entered by the buyer.
        this.tokenId = null; // The Stripe token ID returned by calling Stripe.card.createToken() during purchase.
        this.userId = 0; // The ID of the Socrative user activating the license (from the "socrative_users_socrativeuser" table in the database).
        this.users = []; // Bulk users to be upgraded to PRO via Socrative Admin.
        this.year = 0; // The credit card expiration year entered by the buyer.
        this.years = 0; // The number of years for a license entered by the buyer.
    }
    
    /*---------------*/
    /*    Getters    */
    /*---------------*/
    
    get couponSavings() {
        let savings = 0;
        
        if (this.coupon) {
            savings = `-$${this.coupon.amount / 100}`;
            if (/\.\d$/.test(savings)) {
                savings += '0';
            } else if (!/\./.test(savings)) {
                savings += '.00';
            }
        }
        
        return savings;
    }
    
    get pricePerSeatFormatted() {
        return utils.formatPrice(this.pricePerSeat);
    }
    
    get pricePerSeatFullFormatted() {
        return utils.formatPrice(this.pricePerSeatFull);
    }
    
    get renewalDateFormatted() {
        return utils.formatDate(this.renewalDate);
    }
    
    get totalPriceFormatted() {
        return utils.formatPrice(this.expectedTotalPrice);
    }
    
    /*-------------------*/
    /*    Validations    */
    /*-------------------*/
    
    isValidApplyNow() {
        return this.applyNow === false || this.applyNow === true;
    }
    
    isValidAutoRenew() {
        return this.autoRenew === false || this.autoRenew === true;
    }
    
    isValidBuyerId() {
        return this.buyerId > 0;
    }
    
    isValidCardNumber() {
        return this.cardNumber && this.cardNumber.length > 0;
    }
    
    isValidCouponName() {
        return this.couponName && this.couponName.length > 0;
    }
    
    isValidCvc() {
        return this.cvc && this.cvc.length > 0;
    }
    
    isValidExpectedTotalPrice() {
        return this.expectedTotalPrice !== -1;
    }
    
    isValidKey() {
        return /^[0-9A-F]{25}$/i.test(this.key) ||
               /^([0-9A-F]{5}-){4}[0-9A-F]{5}$/i.test(this.key);
    }
    
    isValidNameOnCard() {
        return this.nameOnCard && this.nameOnCard.length > 0;
    }
    
    isValidSeats() {
        return /^\d+$/.test(this.seats) && this.seats > 0;
    }
    
    isValidTokenId() {
        return this.tokenId;
    }
    
    isValidUserId() {
        return this.userId > 0;
    }
    
    isValidUsers() {
        if (!Array.isArray(this.users) || this.users.length === 0) {
            return false;
        }
        
        for (let user of this.users) {
            if (!user.id) {
                return false;
            }
        }
        
        return true;
    }
    
    isValidYears() {
        return this.years > 0 && this.years <= 3;
    }
    
    /*----------------------*/
    /*    Helper Methods    */
    /*----------------------*/
    
    calculatePriceData() {
        for (let price of this.prices) {
            if (price.seats <= this.seats) {
                this.pricePerSeat = price.amount;
                this.pricePerSeatFull = price.amount;
            } else {
                break;
            }
        }
        if (this.coupon) {
            if (!this.coupon.allow_bulk) {
                this.pricePerSeat = this.prices[0].amount;
                this.pricePerSeatFull = this.prices[0].amount;
            }
            this.pricePerSeat -= this.coupon.amount;
        }
        this.expectedTotalPrice = this.pricePerSeat * this.seats * this.years;
    }
    
    canPurchase() {
        let validCardNumber = window.location.hostname.toLowerCase() === Constants.PRODUCTION_HOSTNAME ? Stripe.card.validateCardNumber(this.cardNumber) : this.isValidCardNumber();
        
        this.setError('nameOnCard', !this.isValidNameOnCard());
        this.setError('cardNumber', !validCardNumber);
        this.setError('month', !Stripe.card.validateExpiry(this.month, this.year));
        this.setError('year', !Stripe.card.validateExpiry(this.month, this.year));
        this.setError('cvc', !Stripe.card.validateCVC(this.cvc));
        
        return this.isValidNameOnCard() &&
               validCardNumber &&
               Stripe.card.validateExpiry(this.month, this.year) &&
               Stripe.card.validateCVC(this.cvc);
    }
    
    setError(input, error, message) {
        this.errors[input].error = error;
        this.errors[input].message = message || '';
    }
    
    /*-----------------------*/
    /*    Client Requests    */
    /*-----------------------*/
    
    fetchPrices(callback) {
        request.get({
            url: `${teacher_service}/v3/prices`,
            success: (response) => {
                this.prices = response.prices;
                callback(true);
            },
            error: () => {
                callback();
            }
        });
    }
    
    fetchCoupon(callback) {
        if (!this.fetchingCoupon) {
            this.fetchingCoupon = true;
            this.couponName = this.couponName.trim();
            request.get({
                url: `${teacher_service}/v3/coupons/name/${this.couponName}`,
                success: (coupon) => {
                    this.coupon = coupon;
                    this.couponSuccess = true;
                    callback(true);
                },
                error: (response) => {
                    this.couponSuccess = false;
                    if (response.error && response.error === Constants.INVALID_COUPON) {
                        this.setError('couponName', true, translate('This coupon is not valid.'));
                    } else {
                        this.setError('couponName', true, translate('There was a problem applying the coupon. Please check your connection and try again.'));
                    }
                    callback();
                },
                complete: () => {
                    this.fetchingCoupon = false;
                }
            });
        }
    }
    
    makePurchase(callback) {
        if (!this.makingPurchase) {
            this.makingPurchase = true;
            Stripe.setPublishableKey(window.stripe_key);
            Stripe.card.createToken({
                number: this.cardNumber,
                exp_month: this.month,
                exp_year: this.year,
                cvc: this.cvc
            }, (stripeStatus, stripeResponse) => {
                if (stripeResponse && stripeResponse.error) {
                    this.makingPurchase = false;
                    callback(false, stripeResponse.error.message);
                } else {
                    request.post({
                        url: `${teacher_service}/v3/licenses/`,
                        data: {
                            applyNow: this.applyNow,
                            autoRenew: this.autoRenew,
                            buyerId: this.buyerId,
                            couponName: this.couponSuccess ? this.couponName : '',
                            expectedTotalPrice: this.expectedTotalPrice,
                            seats: this.seats,
                            tokenId: stripeResponse.id,
                            years: this.years
                        },
                        success: (data) => {
                            this.key = data.key;
                            this.id = data.id;
                            callback(true);
                        },
                        error: (response) => {
                            if (response.error === Constants.PRICE_MISMATCH) {
                                this.prices = response.prices;
                                this.calculatePriceData();
                            } else if (response.error === Constants.STRIPE_ERROR) {
                                response.error = response.message;
                            }
                            this.makingPurchase = false;
                            callback(false, response.error);
                        }
                    })
                }
            });
        }
    }

    activateLicense(callback) {
        if (this.activating) {
            return;
        }

        this.activating = true;

        request.post({
            url: `${teacher_service}/v3/activations/user`,
            data: {key: this.key},
            success: (response) => {
                this.activating = false;
                callback(true, response.licenseExpiration);
            },
            error: (response) => {
                this.activating = false;

                let message = '';

                switch (response.error) {
                    case Constants.INVALID_LICENSE_KEY:
                        message = translate('This license key is invalid.');
                        break;
                    case Constants.EXPIRED_LICENSE_KEY:
                        message = translate('This license key has expired.');
                        break;
                    case Constants.NOT_ENOUGH_SEATS:
                        message = translate('This license key has no seats left.');
                        break;
                    default:
                        message = translate('An unknown error occurred. Please try again later.');
                }

                this.setError('licenseKey', true, message);

                callback(false);
            }
        });
    }
    
    saveAutoRenew(autoRenew, callback) {
        request.put({
            url: `${window.teacher_service}/v3/licenses/auto-renew`,
            data: {
                autoRenew: autoRenew
            },
            success: () => callback(true),
            error: () => callback()
        });
    }
    
}

module.exports = LicenseModel;
