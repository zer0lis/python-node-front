let translate = require('translator').translate;

module.exports = [
    {
        cssStyle: "Blue-team",
        name: translate("Blue"),
        color: "#276baa",
        secondaryColor: "#337abc"
    },
    {
        cssStyle: "Magenta-team",
        name: translate("Magenta"),
        color: "#ca2692",
        secondaryColor: "#d84585"
    },
    {
        cssStyle: "Lime-team",
        name: translate("Lime"),
        color: "#a2d35d",
        secondaryColor: "#caed6a"
    },
    {
        cssStyle: "Peach-team",
        name: translate("Peach"),
        color: "#fcbc89",
        secondaryColor: "#fecca5"
    },
    {
        cssStyle: "Violet-team",
        name: translate("Violet"),
        color: "#7d3990",
        secondaryColor: "#9148a5"
    },
    {
        cssStyle: "Teal-team",
        name: translate("Teal"),
        color: "#258a8c",
        secondaryColor: "#2d9698"
    },
    {
        cssStyle: "Indigo-team",
        name: translate("Indigo"),
        color: "#5f82c2",
        secondaryColor: "#7092d1"
    },
    {
        cssStyle: "Orange-team",
        name: translate("Orange"),
        color: "#d76d2c",
        secondaryColor: "#e57a39"
    },
    {
        cssStyle: "Red-team",
        name: translate("Red"),
        color: "#b83b24",
        secondaryColor: "#c54730"
    },
    {
        cssStyle: "Silver-team",
        name: translate("Silver"),
        color: "#5c646e",
        secondaryColor: "#798390"
    },
    {
        cssStyle: "Green-team",
        name: translate("Green"),
        color: "#4fa45e",
        secondaryColor: "#5cb86c"
    },
    {
        cssStyle: "Rose-team",
        name: translate("Rose"),
        color: "#ec7c90",
        secondaryColor: "#f893a5"
    },
    {
        cssStyle: "Gold-team",
        name: translate("Gold"),
        color: "#ba8324",
        secondaryColor: "#d59a35"
    },
    {
        cssStyle: "Maroon-team",
        name: translate("Maroon"),
        color: "#9d3444",
        secondaryColor: "#ad3a4c"
    },
    {
        cssStyle: "Yellow-team",
        name: translate("Yellow"),
        color: "#fbdb45",
        secondaryColor: "#fbdf5d"
    },
    {
        cssStyle: "Tan-team",
        name: translate("Tan"),
        color: "#D2B48C",
        secondaryColor: "#c39e6c"
    },
    {
        cssStyle: "Hot Pink-team",
        name: translate("Hot Pink"),
        color: "#e602e7",
        secondaryColor: "#b700b8"
    },
    {
        cssStyle: "Sienna-team",
        name: translate("Sienna"),
        color: "#e18942",
        secondaryColor: "#EAAA76"
    },
    {
        cssStyle: "Dimgray-team",
        name: translate("Dimgray"),
        color: "#c3c3c3",
        secondaryColor: "#A6A6A6"
    },
    {
        cssStyle: "Crimson-team",
        name: translate("Crimson"),
        color: "#990000",
        secondaryColor: "#800000"
    }
];
