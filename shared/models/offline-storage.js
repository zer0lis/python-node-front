var Backbone     = require('backbone'),
    _            = require('underscore'),
    popup        = require('PopupController'),
    translate    = require('translator').translate,
    localStorage = window.localStorage,
    offlineStorage = {};

offlineStorage.saveToLocalStorage = function(model) {

    if( model.has('id') && _.isString(model.name) ) {

        localStorage.setItem( 
            model.name + '-' + String( model.get('id') ), 
            JSON.stringify( model.toJSON() ) 
        );
    }
};

/**
 * function used to wrap around the Backbone save function
 * it uses localStorage as a fallback save option
 * 
 * @param   {object} model      the backbone instance
 * @param   {object} attributes the attributes sent to the save function
 * @param   {object} options    the options sent to the BB save function
 * 
 * @returns the normal save object (I think it's a jqXHR)
 */
offlineStorage.save = function(model, attributes, options) {
    // if this is undefined then the error callbacks will not be called
    if( !attributes || typeof attributes === 'undefined' ) {
        attributes = null;
    }

    if( !options || typeof options === 'undefined' ) {
        options = {};
    }
    
    var successFunction = typeof options.success === 'function' ? options.success : function(){};
    var errorFunction   = typeof options.error   === 'function' ? options.error   : function(){};


    options.success = function(data, request) {
        successFunction(data, request);
    };

    // the number of attempts that we tried to save the model
    var attempts = 0;
    options.error = function(data, request) {


        if( request.status === 400 ) {
            // do some validation
        }

        if( request.status === 500 ) {
            popup.render({
                title: translate('Unknown Error'),
                message: translate('Your latest changes were not saved. Please check your connection and try again.')
            });
            return;
        }

        // wait 5 seconds and try again
        setTimeout(function() {
                
            if( attempts <= 2 ) {
                attempts++;
                Backbone.Model.prototype.save.call(model, attributes, options);					
            } else {
                popup.render({
                    title: translate('Please Refresh'),
                    message: translate('Your latest changes might not have been saved. Please refresh the page.')
                });
            }
            
        }, 5000);

        errorFunction(data, request);
    };
    
    return Backbone.Model.prototype.save.call(model, attributes, options);
};

module.exports = offlineStorage;
