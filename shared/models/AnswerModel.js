'use strict';

class AnswerModel {
    
    constructor() {
        this.createdById = 0; // The ID of the teacher who created this answer.
        this.isCorrect = false; // Whether or not this answer is correct.
        this.order = 0; // The order in which this answer appears in the question (i.e. the answer number).
        this.questionId = 0; // The ID of the question to which this answer belongs.
        this.text = ''; // The text of this answer.
    }
    
}

module.exports = AnswerModel;
