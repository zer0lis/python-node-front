'use strict';

class CouponModel {
    
    constructor() {
        this.allowBulk = false; // Whether or not allow bulk was checked.
        this.amount = 0; // The amount that will be discounted per seat.
        this.expirationDate = ''; // When the coupon will no longer be valid.
        this.id = 0; // The id of the coupon.
        this._name = ''; // The friendly name of the coupon.
        this._initialName = ''; // The name of the coupon before edit
    }
    
    /*-----------------------*/
    /*    Getters/Setters    */
    /*-----------------------*/
    
    get name() {return this._name}
    set name(value) {this._name = value ? value.trim().toUpperCase() : value}

    get initialName() {return this._initialName}
    set initialName(value) {this._initialName = value ? value.trim().toUpperCase() : value}
    
    /*---------------*/
    /*    Helpers    */
    /*---------------*/
    
    getCacheable() {
        return {
            allow_bulk: this.allowBulk,
            amount: this.amount,
            expiration_date: this.expirationDate,
            id: this.id,
            name: this.name,
            initialName: this.initialName
        };
    }
    
    /*-------------------*/
    /*    Validations    */
    /*-------------------*/
    
    isValidAllowBulk() {
        return this.allowBulk === true || this.allowBulk === false;
    }
    
    isValidAmount() {
        return /^\d{1,3}\.\d{2}$/.test(this.amount);
    }
    
    isValidExpirationDate() {
        return /^\d{2}\/\d{2}\/\d{4}$/.test(this.expirationDate);
    }
    
    isValidId() {
        return this.id > 0;
    }
    
    isValidName() {
        return  this.name &&
                this.name.length > 0;
    }

    isValidInitialName() {
        return  this.initialName &&
                this.initialName.length > 0;
    }
    
}

module.exports = CouponModel;
