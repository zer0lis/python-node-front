'use strict';

class QuestionModel {
    
    constructor() {
        this.answers = []; // Array of AnswerModel objects associated with this question.
        this.createdById = 0; // The ID of the teacher who created this question.
        this.createdDate = null; // The date this question was created.
        this.explanation = ''; // The optional explanation for this question.
        this.gradingWeight = 1.0; // How much this question is worth.
        this.order = 0; // The order in which this question appears in the quiz (i.e. the question number).
        this.questionText = ''; // The text of the question entered by the teacher.
        this.quizId = 0; // The ID of the quiz to which this question belongs.
        this.type = ''; // One of the <TYPE>_QUESTION values in Constants.js.
    }
    
}

module.exports = QuestionModel;
