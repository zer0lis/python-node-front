'use strict';

class CloneQuizModel {
    
    constructor() {
        this.addStandards = false; // Whether the standards should be cloned along with the quiz.
        
        this.hideCloning = false; /*  If hideCloning is false:
                                          - The string " (copy)" will be appended to the name of the cloned quiz.
                                          - The SOC number of the cloned quiz will be the current value of the quizzes_quiz_id_seq sequence.
                                          - The SOC number of the cloned quiz might be updated to its id + Constants.LEGACY_ID_LIMIT (if the
                                            quizzes_quiz_id_seq sequence did not produce the desired value).
                                      
                                      If hideCloning is true:
                                          - The is_hidden property of the cloned quiz will be false.
                                          - The soc_number of the cloned quiz will be set to this.socNumber (see below).
                                          - The is_hidden property of the original quiz will be updated to false, if this.hideOldQuiz is also true (see below).  */
        
        this.hideOldQuiz = true; /*  If hideOldQuiz is false:
                                         - The is_hidden property of the cloned quiz will be false.
                                         - The SOC number of the cloned quiz will be the current value of the quizzes_quiz_id_seq sequence.
                                         - The SOC number of the cloned quiz might be updated to its id + Constants.LEGACY_ID_LIMIT (if the
                                           quizzes_quiz_id_seq sequence did not produce the desired value).
                                         
                                     If hideOldQuiz is true:
                                         - The is_hidden property of the original quiz will be updated to false, if this.hideCloning is also true (see above).  */
        
        this.name = ''; // The name for the cloned quiz.
        this.parentId = null; // The id of the quiz's parent folder (null means "Quizzes").
        this.quiz = null; // The quiz to be cloned.
        this.socNumber = ''; // The desired SOC number for the cloned quiz.
        this.userId = 0; // The id of the user who will own the cloned quiz.
    }
    
}

module.exports = CloneQuizModel;
