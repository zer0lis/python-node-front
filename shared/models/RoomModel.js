'use strict';

let Constants = require('../Constants'),
    request = require('../Request');

class RoomModel {
    
    constructor() {
        /*--------------*/
        /*    Client    */
        /*--------------*/
        this.callback = null; // Callback executed by a modal on close to pass data back to the controller that opened the modal.
        this.nameStatus = {error: false, message: ''}; // The status of the room name input.
        this.originalName = ''; // The name of the room when the rename room modal was first opened.
        this.pageBlurred = false; // Whether the page has been blurred after opening a modal.
        
        /*--------------*/
        /*    Shared    */
        /*--------------*/
        this.id = 0; // The id of the room.
        this.name = ''; // The name of the room.
    }
    
    /*-------------------*/
    /*    Validations    */
    /*-------------------*/
    
    isValidName() {
        let pattern = new RegExp(`^[a-zA-Z0-9]{1,${Constants.MAX_ROOM_NAME_LENGTH}}$`);
        return pattern.test(this.name);
    }
    
    /*-----------------------*/
    /*    Client Requests    */
    /*-----------------------*/
    
    addRoom(callback) {
        request.post({
            url: `${window.backend_host}/rooms/api/room/`,
            data: {
                room_name: this.name
            },
            success: (room) => {
                callback(null, room);
            },
            error: (response) => {
                let error = response && response.error ? response.error : {};
                callback(error);
            }
        });
    }
    
    renameRoom(callback) {
        request.put({
            url: `${window.backend_host}/rooms/api/room/${this.originalName}`,
            data: {
                new_room_name: this.name
            },
            success: () => {
                callback(null, this.id, this.name);
            },
            error: (response) => {
                let error = response && response.error ? response.error : {};
                callback(error);
            }
        });
    }
    
}

module.exports = RoomModel;
