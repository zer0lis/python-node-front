/**
 * This module represents the room to which the user is currently listening (via pubnub).
 * If the user is a:
 * <ul>
 *     <li>free teacher    - this represents the one and only (default) room
 *     <li>premium teacher - this represents the room currently selected in the header
 *     <li>student         - this represents the currently joined room
 * </ul>
 */

var Backbone  = require('backbone'),
    _         = require('underscore'),
    request   = require('Request'),
    bannerModel = require('BannerModel'),
    renewalModel = require('RenewalModel');

var Room = Backbone.Model.extend({
    
    initialize: function() {
        this.set('joining', true);
        this.set('joinCallbacks', []);
        this.set('name', '');
        this.set('studentCount', 0);
    },
    
    setRoomName: function(value) {
        room.set('name', value);
    },
    
    getRoomName: function() {
        return room.get('name');
    },
    
    getRooms: function() {
        return room.get('rooms');
    },
    
    hasRoster: function() {
        return room.get('rostered');
    },
    
    getRosterSize: function(roomName) {
        var rosterSize = 0;
        if (room.get('rooms')) {
            var rooms = room.get('rooms');
            for (var i = 0; i < rooms.length; i++) {
                var currentRoom = rooms[i];
                if (currentRoom.name === roomName) {
                    rosterSize = currentRoom.student_count;
                    break;
                }
            }
        }
        return rosterSize;
    },
    
    getStudents: function() {
        return room.get('students');
    },
    
    join: function(options) {
        var requestData = {
            role: options.role // The role option must be set to either 'student' or 'teacher'.
        };
        
        if (options.roomname) {
            requestData.name = options.roomname || '';
        }

        requestData.tz_offset = new Date().getTimezoneOffset();
        
        if (options.auth_token) {
            requestData.auth_token = options.auth_token; // This is necessary for MC SSO.
        }
        
        request.post({
            url: `${window.backend_host}/rooms/api/join/`,
            data: requestData,
            success: (data) => {
                room.clear().set(data);
                
                let user = require('user');
                user.setDataFromRoom(data);

                if (data.message) {
                    bannerModel.setData(data.message);
                }

                if (data.notifications) {
                    renewalModel.setData(data.notifications);
                }
                
                let roomName = data.name.toLowerCase();
                
                let messaging = require('Messaging');
                
                if (!options.studentLogin) {
                    messaging.subscribe({
                        channel: `${roomName}-${user.getMode()}`,
                        messages: true
                    });
                }
                
                if (options.role === 'teacher') {
                    messaging.subscribe({
                        channel: `${roomName}-student`,
                        presence: true
                    });
                }
                
                if (_.isFunction(options.success)) {
                    options.success();
                }
                _.each(room.get('joinCallbacks'), function(callback) {
                    callback();
                });

            },
            error: (response) => {
                if (_.isFunction(options.error)) {
                    options.error(response);
                }
            },
            complete: () => {
                room.set('joining', false);
                room.set('joinCallbacks', []);
            }
        });
    },
    
    /**
     * Get the teacher's list of rooms (premium teachers may have more than one).
     * @param {Object} options Object containing success and error callbacks
     */
    getRoomsList: function(options) {
        request.get({
            url: `${window.backend_host}/rooms/api/list/?in_menu=${options.inMenu || false}`,
            success: function(data) {
                if ('rooms' in data) {
                    let rooms = _.sortBy(data.rooms, 'name');
                    room.set('rooms', rooms);
                    if (options.success) {
                        options.success(data);
                    }
                } else {
                    if (options.error) {
                        options.error(data);
                    }
                }
            },
            error: function(xhr) {
                if (options.error) {
                    options.error(xhr);
                }
            }
        });
    },
    
    getStudentList: function(options) {
        if (!options.roomName) {
            options.roomName = room.getRoomName();
        }
        
        request.get({
            url: `${window.backend_host}/rooms/api/${options.roomName}/students/`,
            success: (students) => {
                /**
                 * students is an array of student objects:
                 *     {
                 *         first_name: 'Alice',
                 *         handraise: false,
                 *         id: 22,
                 *         last_name: 'Smith',
                 *         logged_in: true,
                 *         student_id: '44',
                 *         sync_id: null,
                 *         user_uuid: ''
                 *     }
                 */
                
                // Only store the student list on this model if it's for the current room.
                if (options.roomName === room.getRoomName()) {
                    room.set('students', _.sortBy(students, (student) => {
                        return student.last_name + ', ' + student.first_name;
                    }));
                }
                
                options.success(students);
            },
            error: (data) => {
                if (options.error) {
                    options.error(data);
                }
            }
        });
    },

    updateStudentHandraise: function(id, value, callback) {
        if (id) {
            var list = room.get('students');
            var student = _.find(list, function (item) {
                return item.id === id;
            });
            if (_.isBoolean(value)) {
                student.handraise = value;
            } else {
                if (value === 'off') {
                    student.handraise = false;
                } else {
                    student.handraise = true;
                }
            }
        }
        
        room.setHandsRaised();
        
        if (callback) {
            callback()
        }
    },

    clearAllHandraises: function() {
        var list = room.get('students');
        _.each(list, function(item) {
            item.handraise = false;
        });

        room.setHandsRaised('clearAll');
    },

    setHandsRaised: function(value) {
        if (room) {
            var list = room.get('students');
            var i = 0;
            if (value === "clearAll") {
                this.set('handsRaised', i);
                return;
            }

            _.each(list, function(item) {
                if (item.handraise) {
                    i++;
                }
            });

            this.set('handsRaised', i);
        }
    },

    getHandsRaised: function() {
        return this.get('handsRaised');
    }
    
});

var room = new Room();
module.exports = room;
