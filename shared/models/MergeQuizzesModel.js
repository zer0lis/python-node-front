'use strict';

let translate = require('../translations/translator').translate,
    teacher = require('./TeacherModel'),
    Constants = require('../Constants'),
    request = require('../Request'),
    validations = require('../Validations');

class MergeQuizzesModel {
    
    constructor() {
        /*-------------*/
        /*   Client    */
        /*-------------*/
        this.mergeCallback = null; // Callback function executed after the merge is finished.
        this.quizzes = []; // The two quizzes passed in when the merge quizzes modal is opened.
        this.selectedQuiz = null; // The quiz selected by the teacher to come first in the merged quiz.
        
        /*--------------*/
        /*    Shared    */
        /*--------------*/
        this.name = translate('Merged Quiz'); // The name of the merged quiz.
        this.quizSoc1 = ''; // The SOC number of the quiz whose questions will come first in the merged quiz.
        this.quizSoc2 = ''; // The SOC number of the quiz whose questions will come second in the merged quiz.
        
        let parentId = teacher.currentQuizFolderId;
        
        if (parentId === Constants.NO_FOLDER || parentId === Constants.TRASH) {
            parentId = Constants.ROOT_FOLDER;
        }
        
        this.parentId = parentId; // The id of the merged quiz's parent folder.
    }
    
    /*-------------------*/
    /*    Validations    */
    /*-------------------*/
    
    isValidQuizSoc1() {
        return this.quizSoc1 && parseInt(this.quizSoc1) > 0;
    }
    
    isValidQuizSoc2() {
        return this.quizSoc2 && parseInt(this.quizSoc2) > 0;
    }
    
    isValidName() {
        return  this.name &&
                this.name.length > 0;
    }
    
    isValidParentId() {
        return validations.isValidParentId(this.parentId);
    }
    
    /*-----------------------*/
    /*    Client Requests    */
    /*-----------------------*/
    
    mergeQuizzes(callback) {
        request.post({
            url: `${teacher_service}/v3/quizzes/merge/`,
            data: {
                name: this.name,
                quizSoc1: this.selectedQuiz,
                quizSoc2: this.quizzes[0].soc === this.selectedQuiz ? this.quizzes[1].soc : this.quizzes[0].soc,
                parentId: this.parentId
            },
            success: (quiz) => {
                quiz.date = new Date(quiz.date);
                callback(quiz);
            },
            error: () => {
                callback();
            }
        });
    }

}

module.exports = MergeQuizzesModel;
