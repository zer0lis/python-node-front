'use strict';

let Constants = require('../Constants'),
    validations = require('../Validations');

class BaseAdminModel {
    
    constructor() {
        this.reset();
    }
    
    reset() {
        this._email = '';
        this._room = '';
        this._emailToKeep = '';
        this._emailToMerge = '';
        this._id = 0;
        this._role = 0;
    }
    
    
    /*-----------------------*/
    /*    Getters/Setters    */
    /*-----------------------*/
    
    get email() {return this._email}
    set email(value) {this._email = value}

    get room() {return this._room}
    set room(value) {this._room = value}

    get emailToKeep() {return this._emailToKeep}
    set emailToKeep(value) {this._emailToKeep = value}

    get emailToMerge() {return this._emailToMerge}
    set emailToMerge(value) {this._emailToMerge = value}
    
    get id() {return this._id}
    set id(value) {this._id = value}
    
    get role() {return this._role}
    set role(value) {this._role = value}
    
    
    /*-------------------*/
    /*    Validations    */
    /*-------------------*/
    
    isValidEmail() {
        return validations.isValidEmail(this.email);
    }

    isValidEmails(emails) {
        if (emails.length < 1) {
            return false;
        }

        for (let email of emails) {
            if (!validations.isValidEmail(email)) {
                return false;
            }
        }

        return true;
    }
    
    isValidRoom() {
        return this.room && this.room.length > 0;
    }
    
    isValidMergeRequest() {
        return /@.*?\./.test(this.emailToKeep) && /@.*?\./.test(this.emailToMerge);
    }
    
    isValidId() {
        return this.id !== 0;
    }
    
    isValidAdminEmail() {
        return this.email && /@masteryconnect\.com$/.test(this.email.toLowerCase());
    }
    
    isValidRole() {
        return this.role == Constants.EDITOR_USER ||
               this.role == Constants.ADMIN_USER ||
               this.role == Constants.SUPREME_USER;
    }

    isValidDate(date) {
        return date !== '';
    }
    
}

module.exports = BaseAdminModel;
