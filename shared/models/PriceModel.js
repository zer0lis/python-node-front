'use strict';

class PriceModel {
    
    constructor() {
        this.amount = 0; // The amount of the price tier (stored as pennies in the database).
        this.id = 0; // The id of the price.
        this._name = ''; // The friendly name of the price (only visible in Socrative Admin).
        this.seats = 0; // The minimun number of seats needed to qualify for this price tier.
    }
    
    /*-----------------------*/
    /*    Getters/Setters    */
    /*-----------------------*/
    
    get name() {return this._name}
    set name(value) {this._name = value ? value.trim() : value}
    
    /*---------------*/
    /*    Helpers    */
    /*---------------*/
    
    getCacheable() {
        return {
            amount: this.amount,
            id: this.id,
            name: this.name,
            seats: this.seats
        };
    }
    
    /*-------------------*/
    /*    Validations    */
    /*-------------------*/
    
    isValidAmount() {
        return /^\d{1,3}\.\d{2}$/.test(this.amount);
    }
    
    isValidId() {
        return this.id > 0;
    }
    
    isValidName() {
        return  this.name &&
                this.name.length > 0;
    }

    isValidSeats() {
        return  this.seats &&
                this.seats > 0;
    }
    
}

module.exports = PriceModel;
