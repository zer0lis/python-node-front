'use strict';

class UpgradeUserModel {
    
    constructor() {
        this.buyerFirstName = '';
        this.buyerLastName = '';
        this.buyerEmail = '';
        this.years = 0;
        this.seats = 0;
        this.amount = 0;
        this.expiration = null;
        this.sendEmails = true;
    }
    
    /*-------------------*/
    /*    Validations    */
    /*-------------------*/
    
    isValidBuyerFirstName() {
        return this.buyerFirstName && this.buyerFirstName.length > 0;
    }
    
    isValidBuyerLastName() {
        return this.buyerLastName && this.buyerLastName.length > 0;
    }
    
    isValidBuyerEmail() {
        return /@.*?\./.test(this.buyerEmail);
    }
    
    isValidYears() {
        return this.years >= 1 && this.years <= 3;
    }
    
    isValidSeats() {
        return this.seats > 0;
    }
    
    isValidAmount() {
        return /^\d+\.\d{2}$/.test(this.amount);
    }
    
    isValidExpiration() {
        return this.expiration !== null && !isNaN(this.expiration);
    }
    
    isValidSendEmails() {
        return this.sendEmails === true || this.sendEmails === false;
    }
    
}

module.exports = UpgradeUserModel;
