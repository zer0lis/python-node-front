
var Backbone        = require('backbone'),
    _               = require('underscore'),
    
    Question        = require('LegacyQuestionModel'),
    
    offlineStorage  = require('offline-storage'),
    localActivity   = require('activity');

var localUser       = require('user');


var Quiz = Backbone.Model.extend({
    urlRoot: function() {

        return window.backend_host + "/quizzes/api/quiz/";
    },
    
    initialize: function () {
        var that = this;
        if( this.has("questions") ) {
            
            this.set('isTeacher', localUser.isTeacher());

			this.questions = [];
            _.each(this.get("questions"), function(q) {
                var newQ = new Question(q);
                newQ.set('quiz_id', that.get("id"));
                
                that.questions.push( newQ );
            });

            if( !this.get('isTeacher') && localActivity.get("settings").random_questions ) {
                this.questions = _.shuffle(this.questions);
            } else {
                this.questions = _.sortBy(this.questions, function(q){
                    return q.get("order");
                });    
            }
            
            this.unset("questions");
        }

        // if the quiz has a standard and the standard is complete
        if( this.has("standard") ) {

            this.standard = {
                "subject": this.get("standard")["subject_id"],
                "core": this.get("standard")["core_id"],
                "class": this.get("standard")["grade_id"],
                standard: {
                    id: this.get("standard")["standard_id"],
                    name: this.get("standard")["name"],
                    short_description: this.get("standard")["description"],
                }
            };

            this.unset("standard");
        } else {
            this.standard = null;
        }
    },

    /**
     * method used to get the name of the standard
     * used in live results and in the view chart (final results)
     * if the quiz doesn't have a standard it returns an empty string
     * @return {String}     the name of the standard
     */
    getStandardName: function() {

        var standardName = '';

        if( this.standard && this.standard.standard && this.standard.standard.name ) {
            return this.standard.standard.name;
        }

        return standardName;
    },
    
    toJSON: function (includeChildren) {
        var json = Backbone.Model.prototype.toJSON.call(this);
        
		if( !includeChildren && !_.isUndefined(json.questions) ) {
            delete json.questions;
		} else if( includeChildren ) {
            json.questions = [];
			_.each(this.questions, function(q){
				json.questions.push(q.toJSON(includeChildren));
			});
		}

        if( this.standard && this.standard.complete ) {
            
            // recreate the standard object to send it to the server
            json.standard = {
                subject_id:     parseInt(this.standard["subject"]),
                core_id:        parseInt(this.standard["core"]),
                grade_id:       parseInt(this.standard["class"]),
                standard_id:    parseInt(this.standard.standard["id"]),

                name:         String(this.standard.standard["name"]),
                description:  String(this.standard.standard["short_description"]),
            }
        } else {
            delete json.standard;
        }
    
        return json;
    },


    save: function(attributes, options) {
        this.unset("questions");

        if( this.has("dirty") && this.get("dirty") ) {
            this.unset("dirty");

            //console.log("saved quiz " + this.get("soc_number"));
            return offlineStorage.save(this, attributes, options);
        } else {
            return false;
        }
    }
});

module.exports = Quiz;

