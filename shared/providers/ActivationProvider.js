'use strict';

let Constants = require('../Constants'),
    logger = require('../Logger'),
    LicenseModel = require('../models/LicenseModel'),
    teacherDao = require('../daos/TeacherDao'),
    teacherCache = require('../caches/TeacherCache'),
    licenseCache = require('../caches/LicenseCache'),
    workerCache = require('../caches/WorkerCache'),
    config = require('config').get('config'),
    activationDao = require('../daos/ActivationDao');

class ActivationProvider {
    
    async activateLicenseForUser(request) {
        let teacher = request.teacher;
        
        let model = new LicenseModel();
        model.userId = teacher.id;
        model.key = request.body.key;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.MISSING_LICENSE_DATA}
        };
        
        if (!model.isValidUserId() ||
            !model.isValidKey()) {
            return result;
        }
        
        model.key = model.key.toUpperCase();
        
        if (model.key.length === 25) {
            model.key = model.key.match(/.{5}/g).join('-');
        }
        
        let license = await licenseCache.getLicense(model.key);
        
        if (!license) {
            result.data = {error: Constants.INVALID_LICENSE_KEY};
            return result;
        } else if (license.expiration_date.getTime() < Date.now()) {
            result.data = {error: Constants.EXPIRED_LICENSE_KEY};
            return result;
        }
        
        let {error, blockedUsers} = await activationDao.insertActivations(license.id, [teacher]);
        
        if (error) {
            logger.error('Error inserting activation for user:', error);
            if (error.userError) {
                result.data = {error: error.userError};
            } else {
                result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                result.data = {error: Constants.INTERNAL_SERVER_ERROR};
            }
        } else {
            await teacherCache.removeTeacher(teacher.auth_token);
    
            let tasks = [
                JSON.stringify({
                    email_address: teacher.email,
                    email_type: Constants.UPGRADE_PRO_EMAIL
                })
            ];
            await workerCache.addWorkerTasks(`${Constants.SEND_EMAIL_TASK}${config.platform}`, tasks);
            
            result.status = Constants.HTTP_200_OK;
            result.data = {
                blockedUsers: blockedUsers,
                licenseExpiration: license.expiration_date
            };
        }
        
        return result;
    }
    
    async activateLicenseForAdmin(request) {
        let model = new LicenseModel();
        model.userId = request.body.userId;
        model.users = request.body.users;
        model.key = request.body.key;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.MISSING_LICENSE_DATA}
        };
        
        if (!model.isValidUserId() ||
            !model.isValidUsers() ||
            !model.isValidKey()) {
            return result;
        }
        
        let admin = await teacherDao.loadTeacherById(model.userId);
        if (!admin) {
            result.data = {error: Constants.USER_NOT_FOUND};
            return result;
        }
        
        let license = await licenseCache.getLicense(model.key);
        if (!license) {
            result.data = {error: Constants.INVALID_LICENSE_KEY};
            return result;
        } else if (license.expiration_date.getTime() < Date.now()) {
            result.data = {error: Constants.EXPIRED_LICENSE_KEY};
            return result;
        }
        
        let {error, blockedUsers} = await activationDao.insertActivations(license.id, model.users);
        
        if (error) {
            logger.error('Error inserting activation(s) for admin:', error);
            if (error.userError) {
                result.data = {error: error.userError};
            } else {
                result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                result.data = {error: Constants.INTERNAL_SERVER_ERROR};
            }
        } else {
            result.status = Constants.HTTP_200_OK;
            result.data = {blockedUsers: blockedUsers};
        }
        
        return result;
    }
    
}

module.exports = new ActivationProvider();
