'use strict';

let Constants = require('../Constants'),
    priceCache = require('../caches/PriceCache'),
    priceDao = require('../daos/PriceDao'),
    PriceModel = require('../models/PriceModel'),
    RenewalPriceModel = require('../models/RenewalPriceModel');

class PriceProvider {
    
    async getPrices() {
        let prices = await priceCache.getPrices();
        return {
            status: prices.length ? Constants.HTTP_200_OK : Constants.HTTP_500_INTERNAL_SERVER_ERROR,
            data:   prices.length ? {prices: prices}      : {error: Constants.INTERNAL_SERVER_ERROR}
        };
    }

    async addPrice(request) {
        let model = new PriceModel();
        model.name = request.body.name;
        model.seats = parseInt(request.body.seats);
        model.amount = request.body.amount;

        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {message: Constants.INVALID_PRICE_COMPONENT_NAME}
        };

        if (!model.isValidName()) {
            return result;
        }
    
        if (!model.isValidSeats()) {
            result.data.message = Constants.INVALID_PRICE_COMPONENT_SEATS;
            return result;
        }

        if (!model.isValidAmount()) {
            result.data.message = Constants.INVALID_AMOUNT_MESSAGE;
            return result;
        }
    
        model.amount = parseInt(model.amount.replace('.', ''));
        
        let price = await priceDao.insertPrice(model);
        
        if (!price) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.data.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }
        
        if (price.error) {
            if (price.error === Constants.NOT_UNIQUE_NAME) {
                result.data.message = 'Price name must be unique';
            } else if (price.error === Constants.NOT_UNIQUE_SEATS) {
                result.data.message = 'Price seats must be unique';
            }
            return result;
        }

        model.id = price.id;
        priceCache.addPrices([model.getCacheable()]);
        
        result.status = Constants.HTTP_200_OK;
        result.data.id = price.id;
        result.data.message = 'Price added successfully!';
        return result;
    }

    async updatePrice(request) {
        let model = new PriceModel();
        model.name = request.body.name;
        model.seats = parseInt(request.body.seats);
        model.amount = request.body.amount;
        model.id = request.params.id;

        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: 'Price not updated.'
        };

        if (!model.isValidName()) {
            result.message = Constants.INVALID_PRICE_COMPONENT_NAME;
            return result;
        }

        if (!model.isValidSeats()) {
            result.message = Constants.INVALID_PRICE_COMPONENT_SEATS;
            return result;
        }

        if (!model.isValidAmount()) {
            result.message = Constants.INVALID_AMOUNT_MESSAGE;
            return result;
        }
        
        model.amount = parseInt(model.amount.replace('.', ''));
        
        if (!model.isValidId()) {
            result.message = Constants.INVALID_PRICE_ID;
            return result;
        }
        
        let queryResult = await priceDao.updatePrice(model);
    
        if (queryResult.error) {
            if (queryResult.error === Constants.NOT_UNIQUE_NAME) {
                result.message = 'Price name must be unique';
            } else if (queryResult.error === Constants.NOT_UNIQUE_SEATS) {
                result.message = 'Price seats must be unique';
            } else if (queryResult.error === Constants.BASE_PRICE_SEATS) {
                result.message = `Base Price seats can't pe updated`;
            } else {
                result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                result.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            }
            return result;
        }
        
        priceCache.updatePrice(model.getCacheable());

        result.status = Constants.HTTP_200_OK;
        result.message = 'Price updated successfully.';
        return result;
    }

    async deletePrice(request) {
        let model = new PriceModel();
        model.id = request.params.id;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_PRICE_ID_MESSAGE
        };
        
        if (!model.isValidId()) {
            return result;
        }
        
        let queryResult = await priceDao.deletePrice(model.id);
        
        if (queryResult.error === Constants.DATABASE_ERROR) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }
        
        priceCache.removePrice(model.id);
        
        result.status = Constants.HTTP_200_OK;
        result.message = 'Delete success.';
        return result;
    }
    
    async getRenewalPrices() {
        let renewalPrices = await priceCache.getRenewalPrices();
        
        return {
            status: renewalPrices.length ? Constants.HTTP_200_OK : Constants.HTTP_500_INTERNAL_SERVER_ERROR,
            data:   renewalPrices.length ? renewalPrices         : {error: Constants.INTERNAL_SERVER_ERROR}
        };
    }
    
    async changeRenewalPrice(request) {
        let model = new RenewalPriceModel();
        model.amount = request.body.amount;
        model.id = request.params.id;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: 'Invalid renewal price id.'
        };
        
        if (!model.isValidId()) {
            return result;
        }
        
        if (!model.isValidAmount()) {
            result.message = Constants.INVALID_AMOUNT_MESSAGE;
            return result;
        }
        
        model.amount = parseInt(model.amount.replace('.', ''));
        
        model.organizationType = await priceDao.updateRenewalPrice(model);
        
        if (model.organizationType === '') {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }
        
        priceCache.updateRenewalPrice(model);
        
        result.status = Constants.HTTP_200_OK;
        result.message = 'Renewal price updated successfully.';
        return result;
    }
    
}

module.exports = new PriceProvider();
