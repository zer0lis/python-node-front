'use strict';

let crypto = require('crypto'),
    config = require('config').get('config'),
    logger = require('../Logger');

const ALGORITHM = 'des-cbc',
    INITIALIZATION_VECTOR = '\x8a\xb8\xb1\xf7\xe1\x8b\x5a\x5b',
    PKCS5_1_BYTE  = '\x01',
    PKCS5_2_BYTES = '\x02\x02',
    PKCS5_3_BYTES = '\x03\x03\x03',
    PKCS5_4_BYTES = '\x04\x04\x04\x04',
    PKCS5_5_BYTES = '\x05\x05\x05\x05\x05',
    PKCS5_6_BYTES = '\x06\x06\x06\x06\x06\x06',
    PKCS5_7_BYTES = '\x07\x07\x07\x07\x07\x07\x07',
    PKCS5_8_BYTES = '\x08\x08\x08\x08\x08\x08\x08\x08',
    TOKEN           = "TT_",
    DOWNLOAD        = "DL_",
    SHARE           = "SOC_",
    SIGNUP_PREMIUM  = "SP_",
    USER_AUTH       = config.userAuthPrefix,
    STUDENT_AUTH    = config.studentAuthPrefix,
    PREFIXES = [TOKEN, DOWNLOAD, SHARE, USER_AUTH, STUDENT_AUTH, SIGNUP_PREMIUM];

let KEY_DICT                = {};
    KEY_DICT[TOKEN]         = "\xd2\xd4\xe1\xef\x0b\x6b\x5b\x8c";
    KEY_DICT[DOWNLOAD]      = "\xa4\xc0\x01\x3d\x8a\xe2\x1f\x4c";
    KEY_DICT[SHARE]         = "\xe1\xd0\x43\xf1\x29\x8e\x4f\xbc";
    KEY_DICT[USER_AUTH]     = "\xff\xf7\xc7\xae\xff\x55\x74\x24";
    KEY_DICT[STUDENT_AUTH]  = "\xb8\x3b\x45\x62\xce\xc8\xee\x88";
    KEY_DICT[SIGNUP_PREMIUM] = "\x2c\x07\x79\x21\xa4\x67\x55\x7a";

class CryptoProvider {

    padPkcs5(message) {
        let remainder = message.length % 8;

        switch (remainder) {
            case 0:
                message += PKCS5_8_BYTES;
                break;
            case 1:
                message += PKCS5_7_BYTES;
                break;
            case 2:
                message += PKCS5_6_BYTES;
                break;
            case 3:
                message += PKCS5_5_BYTES;
                break;
            case 4:
                message += PKCS5_4_BYTES;
                break;
            case 5:
                message += PKCS5_3_BYTES;
                break;
            case 6:
                message += PKCS5_2_BYTES;
                break;
            case 7:
                message += PKCS5_1_BYTE;
                break;
        }

        return message;
    }
    
    encryptUserAuthToken(token) {
        return this.encryptDesCbc(USER_AUTH, token);
    }
    
    encryptDesCbc(prefix, message) {
        if (PREFIXES.indexOf(prefix) === -1) {
            logger.error(`${prefix} is not a supported prefix.`);
            return null;
        }

        let paddedMessage = this.padPkcs5(message);
        let encrypter = crypto.createCipheriv(
            ALGORITHM,
            Buffer.from(KEY_DICT[prefix], 'binary'),
            Buffer.from(INITIALIZATION_VECTOR, 'binary')
        );

        let encryptedMessage = encrypter.update(paddedMessage, 'utf8', 'base64');
        encryptedMessage = encryptedMessage.replace(/\+/g, '-').replace(/\//g, '_');

        return prefix + encryptedMessage;
    }

    decryptDesCbc(prefix, encryptedText) {
        // do not use it directly. use decrypt() instead

        let decrypter = crypto.createDecipheriv(
            ALGORITHM,
            Buffer.from(KEY_DICT[prefix], 'binary'),
            Buffer.from(INITIALIZATION_VECTOR, 'binary')
        );

        decrypter = decrypter.setAutoPadding(true);

        encryptedText = encryptedText.replace(/\-/g, '+').replace(/_/g, '/');

        let message = decrypter.update(encryptedText, 'base64', 'utf8');

        message += decrypter.final('utf8');

        return message;
    }

    decrypt(encryptedText) {
        let index = encryptedText.indexOf('_');
        if (index === -1) {
            logger.error('Invalid encrypted value. No prefix available.');
            return null;
        }

        let prefix = encryptedText.slice(0, index+1);
        if (PREFIXES.indexOf(prefix) === -1) {
            logger.error(`${prefix} is not a valid prefix.`);
            return null;
        }

        return this.decryptDesCbc(prefix, encryptedText.slice(index+1))
    }

}

module.exports = new CryptoProvider();
