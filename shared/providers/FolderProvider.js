'use strict';

let Constants = require('../Constants'),
    translate = require('../translations/translator').translate,
    FolderModel = require('../models/FolderModel'),
    folderDao = require('../daos/FolderDao'),
    folderCache = require('../caches/FolderCache');

class FolderProvider {
    
    async createFolder(request) {
        let model = new FolderModel();
        model.name = request.body.name;
        model.parentId = request.body.parentId;
        model.type = request.body.type;
    
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.INVALID_FOLDER_DATA}
        };
        
        if (!model.isValidParentId() ||
            !model.isValidType()) {
            return result;
        }
    
        model.name = model.name.trim();
        if (!model.isValidName()) {
            model.name = translate('Untitled Folder');
        }
    
        let teacher = request.teacher;
        
        if (teacher.level !== Constants.PRO_USER) {
            result.data.error = Constants.FEATURE_NOT_AVAILABLE;
            return result;
        }
        
        let queryResult = await folderDao.insertFolder(model, teacher.id);
        
        if (!queryResult) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.data.error = Constants.INTERNAL_SERVER_ERROR;
            return result;
        }
    
        model.id = queryResult.id;
        model.userId = teacher.id;
        
        await folderCache.addFolders(teacher.id, model.type, [model.getCacheable()]);
        
        result.status = Constants.HTTP_200_OK;
        result.data = {
            id: queryResult.id,
            name: model.name
        };
        return result;
    }

    async renameFolder(request) {
        let model = new FolderModel();
        model.id = parseInt(request.params.id);
        model.name = request.body.name;
        model.userId = request.teacher.id;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.INVALID_FOLDER_DATA}
        };
        
        if (!model.isValidId() ||
            !model.isValidName()) {
            return result;
        }
        
        model.name = model.name.trim();
        
        let teacher = request.teacher;
        
        if (teacher.level !== Constants.PRO_USER) {
            result.data.error = Constants.FEATURE_NOT_AVAILABLE;
            return result;
        }
        
        let folder = await folderDao.renameFolder(model, teacher.id);
        
        if (!folder) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.data.error = Constants.INTERNAL_SERVER_ERROR;
            return result;
        }
        
        await folderCache.removeFolder(teacher.id, folder.type, model.id);
        
        await folderCache.addFolders(teacher.id, folder.type, [folder]);
        
        result.status = Constants.HTTP_200_OK;
        result.data = {};
        return result;
    }

}

module.exports = new FolderProvider();
