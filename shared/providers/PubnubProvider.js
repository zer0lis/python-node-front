'use strict';

let config = require('config').get('config'),
    Constants = require('../Constants'),
    PubNub = require('pubnub'),
    longMessageDao = require('../daos/LongMessageDao');

let pubnub = new PubNub({
    publishKey: config.pubnub.publishKey,
    secretKey: config.pubnub.secretKey,
    ssl: config.pubnub.ssl,
    subscribeKey: config.pubnub.subscribeKey,
});

class PubnubProvider {
    
    async sendMessage(channel, key, data) {
        let message = {
            key,
            data
        };
        
        // This is how Pubnub recommends determining message length. They don't explain why the magic
        // number 100 is added (https://support.pubnub.com/support/discussions/topics/14000006322).
        if (encodeURIComponent(channel + JSON.stringify(message)).length + 100 >= Constants.MAX_PUBNUB_MESSAGE_LENGTH) {
            message = {
                long_message_id: await longMessageDao.insertLongMessage(message)
            };
        }
        
        pubnub.publish({
            channel: channel,
            message: JSON.stringify(message)
        });
    }
    
}

module.exports = new PubnubProvider();
