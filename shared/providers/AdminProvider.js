'use strict';

let Constants = require('../Constants'),
    BaseAdminModel = require('../models/BaseAdminModel'),
    teacherDao = require('../daos/TeacherDao'),
    adminDao = require('../daos/AdminDao'),
    teacherCache = require('../caches/TeacherCache'),
    roomCache = require('../caches/RoomCache'),
    config = require('config').get('config'),
    UpgradeUserModel = require('../models/UpgradeUserModel'),
    passwordProvider = require('./PasswordProvider'),
    uuid = require('uuid'),
    workerCache = require('../caches/WorkerCache'),
    AdminBannerModel = require('../models/AdminBannerModel'),
    request = require('request'),
    licenseProvider = require('./LicenseProvider'),
    licenseDao = require('../daos/LicenseDao'),
    activationProvider = require('./ActivationProvider');

class AdminProvider {

    generateMenu(page, request) {
        let level = request.user.level;
        
        let menuList = [
            {
                url: '/dashboard',
                title: 'Dashboard',
                active: page === 'default' ? 'current' : ''
            }
        ];
        
        if (level && level >= Constants.SUPREME_USER) {
            menuList.push(
                {
                    url: '/queries',
                    title: 'Queries',
                    active: page === 'queries' ? 'current' : ''
                },
                {
                    url: '/accounting',
                    title: 'Accounting',
                    active: page === 'accounting' ? 'current' : ''
                }
            );
        }

        menuList.push(
            {
                url: '/delete-account',
                title: 'Delete Account',
                active: page === 'delete-account' ? 'current' : ''
            },
            {
                url: '/search-by-room',
                title: 'Search by Room',
                active: page === 'search-by-room' ? 'current' : ''
            },
            {
                url: '/login-to-account',
                title: 'Login to Teacher Account',
                active: page === 'login-to-account' ? 'current' : ''
            },
            {
                url: '/merge-accounts',
                title: 'Merge Accounts',
                active: page === 'merge-accounts' ? 'current' : ''
            },
            {
                url: '/reset-password',
                title: 'Reset Password',
                active: page === 'reset-password' ? 'current' : ''
            },
            {
                url: '/receipt-management',
                title: 'Receipt Management',
                active: page === 'receipt-management' ? 'current' : ''
            },
            {
                url: '/undelete-quiz',
                title: 'Undelete Quiz',
                active: page === 'undelete-quiz' ? 'current' : ''
            }
        );
        
        if (level && level >= Constants.ADMIN_USER) {
            menuList.push(
                {
                    url: '/upgrade-to-pro',
                    title: 'Upgrade to Pro',
                    active: page === 'pro-upgrade' ? 'current' : ''
                },
                {
                    url: '/downgrade-user',
                    title: 'Downgrade User',
                    active: page === 'downgrade-user' ? 'current' : ''
                }
            );
        }
        
        if (level && level >= Constants.SUPREME_USER) {
            menuList.push(
                {
                    url: '/manage-banners',
                    title: 'Manage Banners',
                    active: page === 'manage-banners' ? 'current' : ''
                },
                {
                    url: '/manage-admins',
                    title: 'Manage Admins',
                    active: page === 'manage-admins' ? 'current' : ''
                },
                {
                    url: '/manage-prices',
                    title: 'Price Management',
                    active: page === 'manage-prices' ? 'current' : ''
                }
            )
        }
    
        return {
            email: request.user.email,
            menuList: menuList
        };
    }

    async sendReports(request) {
        let model = new BaseAdminModel(),
            emails = request.body.emails;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_EMAIL_ADDRESS_MESSAGE
        };
        
        if (!model.isValidEmails(emails)){
            result.message = Constants.INVALID_EMAILS_MESSAGE;
            return result;
        }
        
        if (!model.isValidDate(request.body.startDate)) {
            result.message = Constants.INVALID_START_DATE;
            return result;
        }
        
        if (!model.isValidDate(request.body.endDate)) {
            result.message = Constants.INVALID_END_DATE;
            return result;
        }
        
        let task = {
            emails: emails,
            email_type: Constants.ACCOUNTING_EMAIL,
            start_date: request.body.startDate,
            end_date: request.body.endDate
        };
        
        await workerCache.addWorkerTasks(`${Constants.SEND_EMAIL_TASK}${config.platform}`, [JSON.stringify(task)]);
        
        result.status = Constants.HTTP_200_OK;
        result.message = 'Report was sent!';
        return result;
    }

    async runQuery(request) {
        let model = new BaseAdminModel(),
            emails = request.body.emails;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_EMAIL_ADDRESS_MESSAGE
        };
        
        if (!model.isValidEmails(emails)){
            result.message = Constants.INVALID_EMAILS_MESSAGE;
            return result;
        }
        
        if (!model.isValidDate(request.body.startDate)) {
            result.message = Constants.INVALID_START_DATE;
            return result;
        }
        
        if (!model.isValidDate(request.body.endDate)) {
            result.message = Constants.INVALID_END_DATE;
            return result;
        }
        
        let task = {
            emails: emails,
            email_type: request.body.emailType,
            start_date: request.body.startDate,
            end_date: request.body.endDate,
            org_type: request.body.orgType,
            output: request.body.output
        };
        
        await workerCache.addWorkerTasks(`${Constants.SEND_EMAIL_TASK}${config.platform}`, [JSON.stringify(task)]);

        result.status = Constants.HTTP_200_OK;
        result.message = 'Success. You will get the query result by email.';
        return result;
    }
    
    async deleteUser(request) {
        let model = new BaseAdminModel();
        model.email = request.body.email;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_EMAIL_ADDRESS_MESSAGE
        };
        
        if (!model.isValidEmail()) {
            return result;
        }

        let teacher = await teacherDao.loadTeacherByEmail(model.email.toLowerCase());
        if (!teacher) {
            result.message = Constants.USER_NOT_FOUND_MESSAGE;
            return result;
        }
        
        let success = await adminDao.deleteTeacher(`${teacher.email}-${uuid.v4()}`, uuid.v4().replace(/-/g, '').substring(0, 20), teacher.id);
        if (!success) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }
        
        await teacherCache.removeTeacher(teacher.auth_token);

        let deleteMessages = [
            'Success! You excel at pushing buttons to delete users.',
            'Nice work! That user is ancient history.',
            'Deleted! Tell that user to pack his bags.'
        ];
        
        result.status = Constants.HTTP_200_OK;
        result.message = deleteMessages[Math.floor(Math.random() * deleteMessages.length)];
        return result;
    }
    
    async getRoomOwner(request) {
        let model = new BaseAdminModel();
        model.room = request.body.room;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_ROOM_MESSAGE
        };
        
        if (!model.isValidRoom()) {
            return result;
        }
        
        let teacher = await adminDao.loadTeacherByRoom(model.room.toLowerCase());

        if (teacher.error) {
            result.message = teacher.error;
            return result;
        }
        
        result.status = Constants.HTTP_200_OK;
        result.message = {
            email: teacher.email,
            lastLogin: this.getDate(teacher.lastLogin),
            registerDate: this.getDate(teacher.registerDate),
            createdDate: teacher.createdDate ? this.parseTimestamp(teacher.createdDate.created_date) : 'Not an MC user',
            key: teacher.key ? teacher.key : 'Free User',
            expirationDate: teacher.expirationDate ? this.getDate(teacher.expirationDate) : 'Free user',
            buyerEmail: teacher.buyerEmail ? teacher.buyerEmail : 'Free user'
        };
        return result;
    }

    getDate(date) {
        return `${date.getMonth() + 1}-${date.getDate()}-${date.getFullYear()}`; // MM-DD-YYYY
    }

    parseTimestamp(timestamp) {
        return this.getDate(new Date(timestamp * 1000));
    }
    
    async getTokenLoginUrl(request) {
        let model = new BaseAdminModel();
        model.email = request.body.email;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_EMAIL_ADDRESS_MESSAGE
        };
        
        if (!model.isValidEmail()) {
            return result;
        }
        
        let teacher = await teacherDao.loadTeacherByEmail(model.email.toLowerCase());
        if (!teacher) {
            result.message = Constants.USER_NOT_FOUND_MESSAGE;
            return result;
        }
        
        result.status = Constants.HTTP_200_OK;
        result.message = `${request.protocol}://${config.app.hostname}/teacher/#teacher-login/${teacher.auth_token}`;
        return result;
    }
    
    async mergeAccounts(request) {
        let model = new BaseAdminModel();
        model.emailToKeep = request.body.email_to_keep;
        model.emailToMerge = request.body.email_to_merge;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_EMAIL_ADDRESS_MESSAGE
        };
        
        if (!model.isValidMergeRequest()) {
            return result;
        }
        
        let userToKeep = await teacherDao.loadTeacherByEmail(model.emailToKeep.toLowerCase());
        
        if (!userToKeep) {
            result.message = `${Constants.USER_NOT_FOUND_MESSAGE}: ${model.emailToKeep}`;
            return result;
        }
        
        let userToMerge = await teacherDao.loadTeacherByEmail(model.emailToMerge.toLowerCase());
        
        if (!userToMerge) {
            result.message = `${Constants.USER_NOT_FOUND_MESSAGE}: ${model.emailToMerge}`;
            return result;
        }
        
        result = await adminDao.mergeAccounts(userToKeep.id, userToMerge.id);
        
        if (result.mergedRooms) {
            for (let room of result.mergedRooms) {
                await roomCache.deleteRoom(room);
            }
            
            await roomCache.deleteRoomList(userToMerge.id);
            
            await roomCache.deleteRoomList(userToKeep.id);
            
            await teacherCache.removeTeacher(userToKeep.auth_token);
            
            let mergeMessages = [
                'Success! Your ability to merge users is unparalleled.',
                'Harmony has been achieved. Those two accounts are now one.',
                'Merged! You make merging look easy.'
            ];

            result.status = Constants.HTTP_200_OK;
            result.message = mergeMessages[Math.floor(Math.random() * mergeMessages.length)];
        } else {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
        }
        
        return result;
    }
    
    sendReceipt(request) {
        let model = new BaseAdminModel();
        model.email = request.body.email;
    
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_EMAIL_ADDRESS_MESSAGE
        };
    
        if (!model.isValidEmail()) {
            return result;
        }
        
        let options = {
            url: `${config.payment.url}/receipt/resend/`,
            method: 'POST',
            json: {
                "api_key": config.payment.apiKey,
                "email": model.email
            }
        };
        
        return new Promise((resolve, reject) => {
            request.post(options, (error, response) => {
                if (response.statusCode === Constants.HTTP_200_OK) {
                    let receiptMessages = [
                        'Success! The receipt should be on its way.',
                        'Done! Sending receipts is your special talent.',
                        'Sent! That was easy.'
                    ];
                    result.status = Constants.HTTP_200_OK;
                    result.message = receiptMessages[Math.floor(Math.random() * receiptMessages.length)];
                    resolve(result);
                } else {
                    result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                    result.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
                    reject(result);
                }
            });
        });
    }
    
    async getDeletedQuizzes(request) {
        let model = new BaseAdminModel();
        model.email = request.body.email;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_EMAIL_ADDRESS_MESSAGE
        };
        
        if (!model.isValidEmail()) {
            return result;
        }
        
        let teacher = await teacherDao.loadTeacherByEmail(model.email.toLowerCase());
        if (!teacher) {
            result.message = Constants.USER_NOT_FOUND_MESSAGE;
            return result;
        }
        
        result.status = Constants.HTTP_200_OK;
        result.message = await adminDao.loadDeletedQuizzes(teacher.id);
        return result;
    }
    
    async restoreQuiz(request) {
        let model = new BaseAdminModel();
        model.email = request.body.email;
        model.id = request.body.id;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_EMAIL_ADDRESS_MESSAGE
        };
        
        if (!model.isValidEmail()) {
            return result;
        }
        
        if (!model.isValidId()) {
            result.message = Constants.INVALID_QUIZ_ID_MESSAGE;
            return result;
        }
        
        let teacher = await teacherDao.loadTeacherByEmail(model.email.toLowerCase());
        if (!teacher) {
            result.message = Constants.USER_NOT_FOUND_MESSAGE;
            return result;
        }
        
        let success = await adminDao.restoreQuiz(teacher.id, model.id);
        if (!success) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }
    
        result.status = Constants.HTTP_200_OK;
        result.message = '';
        return result;
    }
    
    getBulkUsersFromFile(rawUsers) {
        let formatting = [],
            duplicates = [];
        
        let data = {
            result: {
                status: Constants.HTTP_400_BAD_REQUEST,
                message: ''
            },
            users: new Map()
        };
        
        for (let i = 1; i < rawUsers.length; i++) {
            let rawUser = rawUsers[i].split(',');
            if (rawUser.length < 3) {
                formatting.push(i + 1);
            } else {
                let first = rawUser[0].trim(),
                    last = rawUser[1].trim(),
                    email = rawUser[2].trim().toLowerCase();
                if (data.users.has(email)) {
                    duplicates.push(i + 1);
                } else {
                    data.users.set(email, {
                        first: first,
                        last: last,
                        email: email,
                        displayName: `${first} ${last}`
                    });
                }
            }
        }
    
        if (formatting.length > 0) {
            data.result.message = `The CSV file is not formatted correclty on ${formatting.length > 1 ? 'lines' : 'line'} ${formatting.join(', ')}. `;
        }
    
        if (duplicates.length > 0) {
            data.result.message += `The CSV file has ${duplicates.length > 1 ? 'duplicates on lines' : 'a duplicate on line'}  ${duplicates.join(', ')}.`;
        }
        
        return data;
    }
    
    /**
     * Create users from a map of user objects. Users in the map who already exist will simply be skipped.
     * 
     * @param {Map} users The users to create. Each user must be an object with the following properties:
     *     email - email address of the user
     *     displayName - concatenation of first and last names, separated by a space
     *     first - first name
     *     last - last name
     * @returns {Object} An object containing the existing users, the inserted users, and a
     *                   boolean indicating whether there was an error during the insertion.
     */
    async createBulkUsers(users) {
        let existingUsers = await teacherDao.loadTeachersByEmails(Array.from(users.keys()));
        if (existingUsers) {
            for (let user of existingUsers) {
                users.delete(user.email.toLowerCase());
            }
        }
        
        let insertedUsers = [];
        if (users.size > 0) {
            for (let user of users.values()) {
                let algorithm = Constants.PASSWORD_ALGORITHM,
                    digest = Constants.PASSWORD_DIGEST,
                    iterations = Constants.PASSWORD_ITERATIONS,
                    salt = passwordProvider.makeSalt(),
                    hash = uuid.v4().replace(/-/g, '');
                // Create an unusable hashed password and random auth token.
                user.password = `${algorithm}_${digest}$${iterations}$${salt}$${hash}`;
                user.authToken = uuid.v4().replace(/-/g, '').substring(0, 20);
            }
            insertedUsers = await adminDao.insertBulkUsers(users.values());
        }
        
        return {
            existingUsers: existingUsers,
            insertedUsers: insertedUsers,
            insertionError: insertedUsers === null
        };
    }
    
    async upgradeToProLicense(request) {
        let {result, users} = this.getBulkUsersFromFile(request.file.buffer.toString().split(/(?:\r\n|\r|\n)/g));

        if (result.message) {
            return result;
        }
    
        if (users.size === 0) {
            result.message = Constants.FILE_UPLOAD_PROBLEM_MESSAGE;
            return result;
        }
    
        let model = new UpgradeUserModel();
        model.buyerFirstName = request.body.buyerFirstName;
        model.buyerLastName = request.body.buyerLastName;
        model.buyerEmail = request.body.buyerEmail;
        model.years = request.body.years;
        model.seats = request.body.seats;
        model.amount = request.body.amount;
        model.expiration = new Date(request.body.expiration);
        model.sendEmails = request.body.sendEmails;
        
        // The request is multipart/form-data because it contains
        // a file, so we have to convert strings back to booleans:
        if (request.body.sendEmails === 'true') {
            model.sendEmails = true;
        } else if (request.body.sendEmails === 'false') {
            model.sendEmails = false;
        }
        
        if (!model.isValidBuyerFirstName() ||
            !model.isValidBuyerLastName() ||
            !model.isValidBuyerEmail()) {
            result.message = Constants.INVALID_BUYER_MESSAGE;
            return result;
        }
    
        model.buyerEmail = model.buyerEmail.toLowerCase();
    
        if (!model.isValidYears()) {
            result.message = Constants.INVALID_YEARS_MESSAGE;
            return result;
        }
        
        if (!model.isValidSeats()) {
            result.message = Constants.INVALID_SEATS_MESSAGE;
            return result;
        }
        
        if (!model.isValidAmount()) {
            result.message = Constants.INVALID_AMOUNT_MESSAGE;
            return result;
        }
    
        model.amount = parseInt(model.amount.replace('.', ''));
    
        if (!model.isValidExpiration()) {
            result.message = Constants.INVALID_EXPIRATION_DATE_MESSAGE;
            return result;
        }
    
        model.expiration.setHours(0); // Set the hour to midnight to avoid time zone issues.
        
        if (!model.isValidSendEmails()) {
            result.message = Constants.INVALID_SEND_EMAILS_MESSAGE;
            return result;
        }
        
        let licenseBuyer = users.has(model.buyerEmail);
        
        if (!licenseBuyer) {
            // We're not activating the license for the buyer, but he 
            // still needs an account if he doesn't already have one.
            users.set(model.buyerEmail, {
                first: model.buyerFirstName,
                last: model.buyerLastName,
                email: model.buyerEmail,
                displayName: `${model.buyerFirstName} ${model.buyerLastName}`
            });
        }
    
        let {existingUsers, insertedUsers, insertionError} = await this.createBulkUsers(users);
    
        if (insertionError) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }
        
        let buyerId = 0,
            authTokens = [],
            usersToUpgrade = [],
            usersToNotify = [];
        
        if (existingUsers) {
            for (let user of existingUsers) {
                if (user.email === model.buyerEmail) {
                    buyerId = user.id;
                    if (!licenseBuyer) {
                        continue;
                    }
                }
                authTokens.push(user.auth_token);
                usersToUpgrade.push(user);
                usersToNotify.push(user.email);
            }
        }
        
        for (let user of insertedUsers) {
            if (user.email === model.buyerEmail) {
                buyerId = user.id;
                if (!licenseBuyer) {
                    continue;
                }
            }
            usersToUpgrade.push(user);
            usersToNotify.push(user.email);
        }
        
        if (usersToUpgrade.length === 0) {
            result.status = Constants.HTTP_400_BAD_REQUEST;
            result.message = Constants.USERS_ALREADY_PRO_MESSAGE;
            return result;
        } else if (usersToUpgrade.length > model.seats) {
            result.status = Constants.HTTP_400_BAD_REQUEST;
            result.message = Constants.INVALID_SEATS_MESSAGE;
            return result;
        }
        
        let key = licenseProvider.createLicenseKey();
        
        let insertProps = {
            buyerId: buyerId,
            key: key,
            expirationDate: model.expiration,
            couponId: null,
            autoRenew: false,
            years: model.years,
            pricePerSeat: model.amount,
            customerId: null,
            stripeResponse: null,
            seats: model.seats,
            totalPrice: model.years * model.amount * model.seats,
            purchaseDate: new Date()
        };
    
        let licenseId = await licenseDao.insertLicense(insertProps);
        
        if (!licenseId) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }
        
        let activationRequest = {
            body: {
                userId: buyerId,
                users: usersToUpgrade,
                key: key
            }
        };
    
        let activationResult = await activationProvider.activateLicenseForAdmin(activationRequest);
        
        if (activationResult.data.error) {
            result.status = activationResult.status;
            if (activationResult.data.error == Constants.USERS_ALREADY_LICENSED) {
                result.message = Constants.USERS_ALREADY_PRO_MESSAGE;
            } else if (activationResult.data.error == Constants.ACTIVATION_BLOCKED_BY_POLICY) {
                // In this case a license will have been created, but not activated. As
                // such, a Socrative developer will need to delete the bogus license.
                result.message = Constants.ACTIVATION_BLOCKED_BY_POLICY_MESSAGE;
            } else if (activationResult.data.error == Constants.NOT_ENOUGH_SEATS) {
                result.message = Constants.INVALID_SEATS_MESSAGE;
            } else {
                result.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            }
            return result;
        }
    
        if (authTokens.length > 0) {
            await teacherCache.removeTeacherMulti(authTokens);
        }
    
        if (model.sendEmails) {
            let emails = [];
            if (activationResult.blockedUsers) {
                for (let email of usersToNotify) {
                    if (!activationResult.blockedUsers.contains(email)) {
                        emails.push(email);
                    }
                }
            } else {
                emails = usersToNotify;
            }
        
            let tasks = [];
            for (let email of emails) {
                let task = {
                    email_address: email,
                    email_type: Constants.UPGRADE_PRO_EMAIL
                };
                
                for (let user of existingUsers) {
                    if (user.email === email) {
                        task.hide_password = true; // Hide the "CREATE PASSWORD" button in the "Welcome to Socrative PRO" email.
                        break;
                    }
                }
                
                tasks.push(JSON.stringify(task));
            }
        
            if (tasks.length > 0) {
                await workerCache.addWorkerTasks(`${Constants.SEND_EMAIL_TASK}${config.platform}`, tasks);
            }
        }
    
        let upgradeMessages = [
            'Success! Nobody upgrades users like you do.',
            'PRO! Peace has been restored to the galaxy.',
            'Upgraded! ...and you get PRO, and you get PRO, and you get PRO...'
        ];
    
        let message = upgradeMessages[Math.floor(Math.random() * upgradeMessages.length)] + ` The license key is: ${key}.`;
    
        if (activationResult.blockedUsers && activationResult.blockedUsers.length > 0) {
            message += ` NOTE: License policy blocked the following from upgrading to PRO: ${blockedUsers.join(', ')}`;
        }
    
        result.status = Constants.HTTP_200_OK;
        result.message = message;
        return result;
    }
    
    async downgradeToFree(request) {
        let model = new BaseAdminModel();
        model.email = request.body.email;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_EMAIL_ADDRESS_MESSAGE
        };
        
        if (!model.isValidEmail()) {
            return result;
        }
        
        let teacher = await teacherDao.loadTeacherByEmail(model.email.toLowerCase());
        if (!teacher) {
            result.message = Constants.USER_NOT_FOUND_MESSAGE;
            return result;
        }
        
        let success = await adminDao.downgradeToFree(teacher.id);
        if (!success) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }
        
        await teacherCache.removeTeacher(teacher.auth_token);
    
        let downgradeMessages = [
            'Success! Pity the poor fool who just got downgraded.',
            'Done! No PRO for you!',
            'Downgraded! The user has gone back to the land of the free.'
        ];
        
        result.status = Constants.HTTP_200_OK;
        result.message = downgradeMessages[Math.floor(Math.random() * downgradeMessages.length)];
        return result;
    }
    
    async getBanners() {
        let result = {
            status: Constants.HTTP_500_INTERNAL_SERVER_ERROR,
            data: {error: Constants.INTERNAL_SERVER_ERROR_MESSAGE}
        };
    
        let banners = await adminDao.loadBanners();
        if (!banners) {
            return result;
        }
        
        result.status = Constants.HTTP_200_OK;
        result.data = banners;
        return result;
    }
    
    async disableBanner(request) {
        let model = new AdminBannerModel();
        model.id = request.body.id;
    
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_BANNER_ID_MESSAGE
        };
    
        if (!model.isValidId()) {
            result.message = Constants.INVALID_BANNER_ID_MESSAGE;
            return result;
        }
        
        let success = adminDao.disableBanner(model);
        if (!success) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            request.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }
    
        let disableBannerMessages = [
            'Success! That banner is history.',
            'Done! Erased from existence...',
            'Disabled! Your ability to turn banners off is astounding.'
        ];
    
        result.status = Constants.HTTP_200_OK;
        result.message = disableBannerMessages[Math.floor(Math.random() * disableBannerMessages.length)];
        return result;
    }
    
    async deployBanner(request) {
        let model = new AdminBannerModel();
        model.audience = request.body.audience;
        model.title = request.body.title;
        model.content = request.body.content;
        model.action = request.body.action;
        model.dismissible = request.body.dismissible;
        
        if (request.body.url) {
            model.url = request.body.url;
        }
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_BANNER_AUDIENCE_MESSAGE
        };
        
        if (!model.isValidAudience()) {
            result.message = Constants.INVALID_BANNER_AUDIENCE_MESSAGE;
            return result;
        }
        
        if (!model.isValidTitle()) {
            result.message = Constants.INVALID_BANNER_TITLE_MESSAGE;
            return result;
        }
        
        if (!model.isValidContent()) {
            result.message = Constants.INVALID_BANNER_CONTENT_MESSAGE;
            return result;
        }
        
        if (!model.isValidAction() || (model.audience != Constants.BANNER_AUDIENCE_FREE && model.action === Constants.BANNER_ACTION_GO_PRO)) {
            result.message = Constants.INVALID_BANNER_ACTION_MESSAGE;
            return result;
        }
        
        if (model.action === Constants.BANNER_ACTION_EXTERNAL_URL && !model.isValidUrl()) {
            result.message = Constants.INVALID_BANNER_URL_MESSAGE;
            return result;
        }

        if (!model.isValidDismissible()) {
            return result;
        }
        
        let hidden = await adminDao.hideBanner(model);
        if (!hidden) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            request.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }
        
        let success = await adminDao.insertBanner(model);
        if (!success) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            request.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }
    
        let deployBannerMessages = [
            'Success! The new banner is now waving proudly in the wind.',
            'Done! You deploy banners like a boss.',
            'Deployed! [Bob Ross]: "Let\'s add a happy little banner...yeah, right there."'
        ];
    
        result.status = Constants.HTTP_200_OK;
        result.message = deployBannerMessages[Math.floor(Math.random() * deployBannerMessages.length)];
        return result;
    }
    
    async getAdminList(request) {
        let result = {
            status: Constants.HTTP_500_INTERNAL_SERVER_ERROR,
            data: []
        };
        
        let admins = await adminDao.loadAdmins();
        if (!admins) {
            return result;
        }
        
        for (let admin of admins) {
            let role = 'Editor';
            if (admin.level == Constants.ADMIN_USER) {
                role = 'Admin';
            } else if (admin.level == Constants.SUPREME_USER) {
                role = 'Supreme';
            }
            result.data.push({
                email: admin.email,
                role: role,
                user_id: admin.user_id
            });
        }
        
        result.status = Constants.HTTP_200_OK;
        return result;
    }
    
    async addAdmin(request) {
        let model = new BaseAdminModel();
        model.email = request.body.email;
        model.role = request.body.role;
    
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_EMAIL_ADDRESS_MESSAGE
        };
        
        if (!model.isValidEmail()) {
            return result;
        }
        
        if (!model.isValidAdminEmail()) {
            result.message = Constants.INVALID_ADMIN_EMAIL_MESSAGE;
            return result;
        }
        
        if (!model.isValidRole()) {
            result.message = Constants.INVALID_ROLE_MESSAGE;
            return result;
        }
        
        let success = await adminDao.insertAdmin(model.email, model.role, request.user.id);
        if (!success) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }
    
        result.status = Constants.HTTP_200_OK;
        result.message = '';
        return result;
    }
    
    async deleteAdmin(request, id) {
        let model = new BaseAdminModel();
        model.id = request.params.id;
    
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_ADMIN_ID_MESSAGE
        };
        
        if (!model.isValidId()) {
            return result;
        }
    
        let success = await adminDao.deleteAdmin(model.id);
        if (!success) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }
    
        result.status = Constants.HTTP_200_OK;
        result.message = '';
        return result;
    }
    
}

module.exports = new AdminProvider();
