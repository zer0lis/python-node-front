'use strict';

let pug = require('pug'),
    SES = require('aws-sdk/clients/ses'),
    licenseDao = require('../daos/LicenseDao'),
    activationDao = require('../daos/ActivationDao'),
    priceCache = require('../caches/PriceCache'),
    config = require('config').get('config'),
    stripe = require('stripe')(config.stripe.apiKey),
    logger = require('../Logger'),
    translator = require('../translations/translator'),
    Constants = require('../Constants'),
    utils = require('../Utils');

let renderRenewalNotificationEmail = pug.compileFile('./shared/templates/emails/renewal-notification.pug'),
    renderRenewalErrorEmail = pug.compileFile('./shared/templates/emails/renewal-error.pug'),
    renderProReceiptEmail = pug.compileFile('./shared/templates/emails/pro-receipt.pug'),
    ses = new SES();

class RenewalProvider {
    
    async sendNotifications() {
        let licenses = [];
        
        if (process.env.NODE_ENV !== 'production') {
            return; // The dev database is not running when this cron job kicks off in the middle of the night.
        } else {
            licenses = await licenseDao.selectExpiringLicenses(30);
            
            if (licenses === null) {
                logger.error('Renewal notifications will not be sent (could not get expiring licenses).');
                return;
            }
        }
        
        for (let license of licenses) {
            let txClient = await activationDao.getTxClient();
            
            if (!txClient) {
                logger.error('Renewal notifications will not be sent (could not get transaction client).');
                return;
            }
            
            let userIds = await activationDao.selectUserIdsByActivation(license.id, txClient);
            
            if (userIds.length === 0) {
                logger.info(`License ${license.key} has no activations. Renewal notification will not be sent.`);
                activationDao.commitAndRelease(txClient);
                continue;
            }
            
            // If the license has multiple seats, we don't try to detect whether
            // one or more users may have renewed manually. We proceed to notify
            // the buyer about auto renewal and expect him to manage the license
            // with the license manager.
            
            if (license.seats === 1 && userIds.length === 1) {
                // If the license only has one seat, check whether the user has
                // a newer license. If so, we know he already renewed manually.
                let newestKey = await licenseDao.selectNewestLicenseKey(userIds[0].user_id, txClient);
                
                if (newestKey === null) {
                    logger.error(`Error checking for newer license. Renewal notification will not be sent for license ${license.key}.`);
                    activationDao.commitAndRelease(txClient);
                    continue;
                }
                
                if (newestKey !== license.key) {
                    logger.info(`User id ${userIds[0].user_id} has newer license ${newestKey}. Renewal notification will not be sent for old license ${license.key}.`);
                    activationDao.commitAndRelease(txClient);
                    continue;
                }
            }
            
            activationDao.commitAndRelease(txClient);
            
            translator.loadServiceTranslations(license.language);
            
            let translate = translator.translate,
                seats = license.seats === 1 ? translate('1 seat') : translate('{0} seats').format(license.seats),
                date = utils.formatDate(license.expirationDate),
                years = license.years === 1 ? translate('1 year') : translate('{0} years').format(license.years);
                
            let body = {
                renewalSeatsAndDate: translate('Your Socrative PRO license (key: {0}) is scheduled to renew automatically with {1} on {2} for {3}.').format(license.key, seats, date, years),
                renewalProcess: translate('At that time we will attempt to process a payment to renew your license at the lowest price available.'),
                renewalConfirmation: translate('A confirmation and receipt will be emailed to you when this process is completed successfully.'),
                profileActions: translate('Before that date, you may perform the following actions on the Account tab of your profile'),
                disableRenew: translate('Disable automatic renewal'),
                manualRenew: translate('Renew manually to change the seats or years of your license')
            };
            
            let emailParams = {
                Destination: {ToAddresses: [license.email]},
                Message: {
                    Subject: {
                        Data: translate('Socrative Pro License Renewal'),
                        Charset: 'UTF-8'
                    },
                    Body: {
                        Html: {
                            Data: renderRenewalNotificationEmail(body),
                            Charset: 'UTF-8'
                        },
                        Text: {
                            Data: `${body.renewalSeatsAndDate} ${body.renewalProcess} ${body.renewalConfirmation} ${body.profileActions}:\n${body.disableRenew}\n${body.manualRenew}`,
                            Charset: 'UTF-8'
                        }
                    }
                }
            };
            
            try {
                let messageId = await this.sendEmail(emailParams);
                logger.info(`Renewal notification sent to ${license.email} (message id: ${messageId}).`);
            } catch (error) {
                logger.error(`Error sending renewal notification to ${license.email}:`, error);
            }
        }
    }
    
    async performRenewals() {
        let licenses = [];
        
        if (process.env.NODE_ENV !== 'production') {
            return; // The dev database is not running when this cron job kicks off in the middle of the night.
        } else {
            licenses = await licenseDao.selectExpiringLicenses(1);
            
            if (licenses === null) {
                logger.error('Renewals will not occur (could not get expiring licenses).');
                return;
            }
        }
        
        let renewalPrices = [];
        
        for (let license of licenses) {
            let txClient = await activationDao.getTxClient();
            
            if (!txClient) {
                logger.error('Renewals will not occur (could not get transaction client).');
                return;
            }
            
            let userIds = await activationDao.selectUserIdsByActivation(license.id, txClient);
            
            if (userIds.length === 0) {
                logger.info(`License ${license.key} has no activations. Renewal will not occur.`);
                activationDao.commitAndRelease(txClient);
                continue;
            }
            
            // If the license has multiple seats, we don't try to detect whether
            // one or more users may have renewed manually. We proceed with auto
            // renew, expecting buyers to manage their licenses with the license
            // manager.
            
            if (license.seats === 1 && userIds.length === 1) {
                // If the license only has one seat, check whether the user has
                // a newer license. If so, we know he already renewed manually.
                let newestKey = await licenseDao.selectNewestLicenseKey(userIds[0].user_id, txClient);
                
                if (newestKey === null) {
                    logger.error(`Error checking for newer license. Renewal will not occur for license ${license.key}.`);
                    activationDao.commitAndRelease(txClient);
                    continue;
                }
                
                if (newestKey !== license.key) {
                    logger.info(`User id ${userIds[0].user_id} has newer license ${newestKey}. Renewal will not occur for old license ${license.key}.`);
                    activationDao.commitAndRelease(txClient);
                    continue;
                }
            }
            
            translator.loadServiceTranslations(license.language);
            
            let translate = translator.translate;
            
            let errorBody = {
                renewalProblem: translate('We encountered the following problem attempting to renew your Socrative PRO license (key: {0})').format(license.key),
                errorMessage: translate('Your renewal price could be not be determined because your account has no organization type.'),
                manuallyRenew: translate('You can set your organization type and manually renew your Socrative PRO license in your profile.')
            };
            
            let errorEmailParams = {
                Destination: {ToAddresses: [license.email]},
                Message: {
                    Subject: {
                        Data: translate('Socrative Pro License Renewal'),
                        Charset: 'UTF-8'
                    },
                    Body: {
                        Html: {
                            Data: renderRenewalErrorEmail(errorBody),
                            Charset: 'UTF-8'
                        },
                        Text: {
                            Data: `${errorBody.renewalProblem}:\n${errorBody.errorMessage}\n${errorBody.manuallyRenew}`,
                            Charset: 'UTF-8'
                        }
                    }
                }
            };
            
            if (renewalPrices.length === 0) {
                renewalPrices = await priceCache.getRenewalPrices();
            }
            
            let renewalPrice = renewalPrices.find(price => price.organizationType === license.organizationType);
            
            if (!renewalPrice) {
                logger.error(`Error determining renewal price for license ${license.key}. Renewal will not occur.`);
                
                try {
                    let messageId = await this.sendEmail(errorEmailParams);
                    logger.info(`Renewal error message sent to ${license.email} (message id: ${messageId}).`);
                } catch (error) {
                    logger.error(`Error sending renewal error message to ${license.email}:`, error);
                }
                
                activationDao.commitAndRelease(txClient);
                continue;
            }
            
            let renewalAmountInPennies = renewalPrice.amount * license.seats * license.years;
            
            let chargeData = {
                amount: renewalAmountInPennies,
                currency: 'USD',
                customer: license.customerId,
                description: `Automatic renewal for ${license.email}`,
                metadata: {
                    key: license.key,
                    orgType: license.organizationType,
                    pricePerSeat: renewalPrice.amount,
                    seats: license.seats,
                    years: license.years
                },
                statement_descriptor: 'Socrative Pro Renewal'
            };
            
            let chargeResponse = null;
            
            try {
                chargeResponse = await stripe.charges.create(chargeData);
            } catch (error) {
                logger.error(`Stripe charge error (${error.type}) for renewal of license ${license.key}: ${error.message}`);
                
                activationDao.commitAndRelease(txClient);
                
                errorBody.errorMessage = error.message;
                errorBody.manuallyRenew = translate('You can manually renew your Socrative PRO license on the Account tab of your profile.');
                
                errorEmailParams.Message.Body.Html.Data = renderRenewalErrorEmail(errorBody);
                errorEmailParams.Message.Body.Text.Data = `${errorBody.renewalProblem}:\n${errorBody.errorMessage}\n${errorBody.manuallyRenew}`;
                
                try {
                    let messageId = await this.sendEmail(errorEmailParams);
                    logger.info(`Renewal error message sent to ${license.email} (message id: ${messageId}).`);
                } catch (error) {
                    logger.error(`Error sending renewal error message to ${license.email}:`, error);
                }
                
                continue;
            }
            
            let transaction = {
                licenseId: license.id,
                seats: 0,
                price: renewalAmountInPennies,
                purchaseDate: new Date(),
                stripeResponse: chargeResponse
            };
            
            let success = await licenseDao.insertLicenseTransaction(transaction, txClient);
            
            if (!success) {
                continue; // The error has already been logged, and the transaction has already been rolled back.
            }
            
            license.expirationDate.setFullYear(license.expirationDate.getFullYear() + license.years);
            
            success = await licenseDao.updateLicenseExpirationDate(license.expirationDate, license.id, txClient);
            
            if (!success) {
                continue; // The error has already been logged, and the transaction has already been rolled back.
            }
            
            activationDao.commitAndRelease(txClient);
            
            let org = '';
            
            if (license.organizationType === Constants.ORGANIZATION_CORP || license.organizationType === Constants.ORGANIZATION_OTHER) {
                org = license.description;
            } else if (license.organizationType === Constants.ORGANIZATION_HIGHER_ED) {
                org = license.university;
            } else if (license.organizationType === Constants.ORGANIZATION_K12) {
                org = license.schoolName;
            }
            
            let body = {
                thankYou: translate('Thank you for going PRO!'),
                explanation: translate('Your Socrative PRO license has been renewed per the settings in your account.'),
                nameHeading: translate('Name'),
                name: `${license.lastName}, ${license.firstName}`,
                orgHeading: translate('Organization'),
                org: org,
                emailHeading: translate('Email'),
                email: license.email,
                amountHeading: translate('Amount'),
                amount: utils.formatPrice(renewalAmountInPennies),
                orderNumberHeading: translate('Order Number'),
                orderNumber: license.id,
                paymentMethodHeading: translate('Payment Method'),
                paymentMethod: `${chargeResponse.source.brand} (**** ${chargeResponse.source.last4})`,
                licenseKeyHeading: translate('License Key'),
                licenseKey: license.key,
                subscriptionLengthHeading: translate('Subscription Length'),
                subscriptionLength: license.years === 1 ? translate('1 year') : translate('{0} years').format(license.years),
                expirationDateHeading: translate('Expiration Date'),
                expirationDate: utils.formatDate(license.expirationDate),
                waStateSalesLabel: translate('Washington State Sales'),
                waStateSalesContent: translate('MasteryConnect is not required to, and does not, collect Washington sales or use tax. Under Washington law, purchases are not tax-exempt merely because a seller is not required to collect Washington’s tax. Washington law requires Washington purchasers to review untaxed purchases and, if any tax is owed, file a Washington use tax return and pay any tax due. Visit https://dor.wa.gov/consumerusetax for more information.')
            };
            
            let emailParams = {
                Destination: {ToAddresses: [license.email]},
                Message: {
                    Subject: {
                        Data: translate('Socrative Pro License Renewal'),
                        Charset: 'UTF-8'
                    },
                    Body: {
                        Html: {
                            Data: renderProReceiptEmail(body),
                            Charset: 'UTF-8'
                        },
                        Text: {
                            Data: `${body.thankYou} ${body.explanation}\n
${body.nameHeading}: ${body.name}
${body.orgHeading}: ${body.org}
${body.emailHeading}: ${body.email}
${body.amountHeading}: ${body.amount}
${body.orderNumberHeading}: ${body.orderNumber}
${body.paymentMethodHeading}: ${body.paymentMethod}
${body.licenseKeyHeading}: ${body.licenseKey}
${body.subscriptionLengthHeading}: ${body.subscriptionLength}
${body.expirationDateHeading}: ${body.expirationDate}
${body.waStateSalesLabel}
${body.waStateSalesContent}`,
                            Charset: 'UTF-8'
                        }
                    }
                }
            };
            
            try {
                let messageId = await this.sendEmail(emailParams);
                logger.info(`Renewal receipt sent to ${license.email} (message id: ${messageId}).`);
            } catch (error) {
                logger.error(`Error sending renewal receipt to ${license.email}:`, error);
            }
        }
    }
    
    sendEmail(emailParams) {
        emailParams.Source = 'Socrative <noreply@socrative.com>';
        emailParams.ReturnPath = 'bounce@socrative.com';
        
        return new Promise((resolve, reject) => {
            ses.sendEmail(emailParams, (error, response) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(response.MessageId);
                }
            });
        });
    }
    
}

module.exports = new RenewalProvider();
