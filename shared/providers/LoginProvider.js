'use strict';

let Constants = require('../Constants'),
    LoginModel = require('../models/LoginModel'),
    teacherDao = require('../daos/TeacherDao'),
    partnerDao = require('../daos/PartnerDao'),
    notificationDao = require('../daos/NotificationDao'),
    rateLimiter = require('../caches/RateLimitCache'),
    teacherCache = require('../caches/TeacherCache'),
    cryptoProvider = require('../providers/CryptoProvider'),
    crypto = require('crypto'),
    uuid = require('uuid'),
    translator = require('../translations/translator'),
    logger = require('../Logger'),
    config = require('config').config,
    asyncRequest = require('request-promise-native');

class LoginProvider {
    
    async logInEmailTeacher(request) {
        let model = new LoginModel();
        model.email = request.body.email;
        model.password = request.body.password;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.LOGIN_FAILED}
        };
        
        if (!model.isValidEmail() || !model.isValidLegacyPassword()) {
            await rateLimiter.incrementLoginFailures(request.ip, model.email);
            return result;
        }
        
        let blocked = await rateLimiter.shouldBlock(request.ip, model.email);
        if (blocked) {
            result.data.error = blocked;
            return result;
        }
        
        let teacher = await teacherDao.loadTeacherByEmail(model.email);
        if (!teacher) {
            await rateLimiter.incrementLoginFailures(request.ip, model.email);
            return result;
        } else if (!teacher.is_active) {
            await rateLimiter.incrementLoginFailures(request.ip, model.email);
            result.data.error = Constants.ACCOUNT_SUSPENDED;
            return result;
        } else {
            try {
                let validPassword = await this.verifyPassword(model.password, teacher.password);
                if (!validPassword) {
                    await rateLimiter.incrementLoginFailures(request.ip, model.email);
                    return result;
                }
            } catch (error) {
                logger.error('Error verifying password during login:', error);
                result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                result.data.error = Constants.INTERNAL_SERVER_ERROR;
                return result;
            }
            
            teacherCache.removeTeacher(teacher.auth_token);
            
            teacher.auth_token = this.generateAuthToken();
            
            let updated = await teacherDao.updateLastLogin(new Date(), teacher.auth_token, teacher.id);
            if (!updated) {
                result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                result.data.error = Constants.INTERNAL_SERVER_ERROR;
                return result;
            }
            
            // await teacherCache.addTeacher(teacher); // Uncomment when the back end is migrated to Node.js.
            
            await teacherDao.updateBannerStatus(Constants.BANNER_REMIND_LATER, Constants.BANNER_SHOW, teacher.id);
    
            result.status = Constants.HTTP_200_OK;
            result.data = {
                email: teacher.email,
                id: teacher.id,
                language: teacher.language,
                token: cryptoProvider.encryptUserAuthToken(teacher.auth_token)
            };
            return result;
        }
    }
    
    verifyPassword(password, encoded) {
        return new Promise((resolve, reject) => {
            let [, iterations, salt, hash] = encoded.split('$');
            crypto.pbkdf2(password, salt, parseInt(iterations), 32, 'sha256', (error, derived) => {
                if (!error) {
                    resolve(hash === derived.toString('base64').trim());
                } else {
                    reject(error);
                }
            });
        });
    }
    
    generateAuthToken(length = 20) {
        return uuid.v4().replace(/\-/g, '').substr(0, length);
    }
    
    async logInGoogleTeacher(request) {
        let code = request.query.code,
            error = request.query.error,
            state = request.query.state;

        if (error || !code) {
            return {url: `${request.protocol}://${config.app.hostname}/login/teacher/#google-error/${error}`}
        }

        let postData = {
            code : code,
            client_id: config.google.oAuthClientId,
            client_secret: config.google.oAuthClientSecret,
            redirect_uri: config.google.oAuthRedirectURI,
            grant_type: "authorization_code"
        };

        let resp = await asyncRequest({
            uri: config.google.oAuthRefreshTokenURI,
            method: "POST",
            form: postData,
            resolveWithFullResponse: true,
            simple: false // to avoid unhandled promise rejection in case of error http codes
        });

        if (!resp || resp.statusCode >= 400) {
            logger.error(resp);
            return {url: `${request.protocol}://${config.app.hostname}/login/teacher/#google-error/${Constants.GOOGLE_LOGIN_ERROR}`}
        }

        let jsonBody = null;

        try {
            jsonBody = JSON.parse(resp.body);
        } catch (error) {
            logger.error(error);
            return {url: `${request.protocol}://${config.app.hostname}/login/teacher/#google-error/${Constants.GOOGLE_LOGIN_ERROR}`}
        }

        let access_token = jsonBody.access_token,
            expires_in = jsonBody.expires_in,
            refresh_token = jsonBody.refresh_token || null;

        // make the next call to get the email of the user
        resp = await asyncRequest({
            uri: config.google.emailApiURL,
            method: 'GET',
            headers: {
                'Authorization': `Bearer ${access_token}`
            },
            resolveWithFullResponse: true,
            simple: false
        });

        if (resp.statusCode !== 200) {
            logger.error(resp);
            return {url: `${request.protocol}://${config.app.hostname}/login/teacher/#google-error/${Constants.BAD_ACCESS_TOKEN}`};
        }

        let email = '';
        try {
            email = JSON.parse(resp.body).emails[0].value.trim().toLowerCase();
        } catch (error) {
            logger.error(error);
            return {url: `${request.protocol}://${config.app.hostname}/login/teacher/#google-error/${Constants.GOOGLE_LOGIN_ERROR}`}
        }

        let teacher = await teacherDao.loadTeacherByEmail(email);

        if (!teacher) {
            logger.warn(`Google Login: There is no account for email ${email}`);
            return {url: `${request.protocol}://${config.app.hostname}/login/teacher/#google-create/${email}`};
        }


        let partner = await partnerDao.getPartner(teacher.id, Constants.GOOGLE_PARTNER);
        if (!partner) {

            if (!refresh_token) {
                return {url: `${request.protocol}://${config.app.hostname}/login/teacher/#google-approval/${state}`};
            }

            let partnerModel = {
                user_id: teacher.id,
                type: Constants.GOOGLE_PARTNER,
                data: JSON.stringify({
                    access_token: access_token,
                    expires: new Date(new Date().getTime() + expires_in*1000),
                    refresh_token: refresh_token
                })
            };

            await partnerDao.insertPartner(partnerModel);
        }
        else {
            //update the partner data json with the new data
            let googleData = {};
            try {
                googleData = JSON.parse(partner.data);
            } catch (error) {
                logger.error(error)
            }

            if (!refresh_token && !googleData.refresh_token) {
                return {url: `${request.protocol}://${config.app.hostname}/login/teacher/#google-approval/${state}`};
            }

            googleData.access_token = access_token;
            googleData.expires = new Date(new Date().getTime() + expires_in*1000).getTime()/1000; // the workers expect timestamp
            if (refresh_token) {
                googleData.refresh_token = refresh_token;
            }

            await partnerDao.updateData(partner.id, JSON.stringify(googleData));
        }

        teacherCache.removeTeacher(teacher.auth_token);

        teacher.auth_token = this.generateAuthToken();

        await teacherDao.updateLastLogin(new Date(), teacher.auth_token, teacher.id);
        if (teacher.level === Constants.PRO_USER) {
            await notificationDao.resetNotifications(teacher.id, [Constants.PRO_ENDING, Constants.PRO_ENDED])
        }

        let redirect_url = '';

        if (state !== Constants.ZENDESK_SSO_STATE) {
            await teacherDao.updateBannerStatus(Constants.BANNER_REMIND_LATER, Constants.BANNER_SHOW, teacher.id);
            redirect_url = `${request.protocol}://${config.app.hostname}/login/teacher/#google-login-success/${teacher.email}`;
        }
        else {
            redirect_url = `${request.protocol}://${config.app.hostname}/login/teacher/#${Constants.ZENDESK_SSO_STATE}`
        }

        return {
            url: redirect_url,
            token: cryptoProvider.encryptUserAuthToken(teacher.auth_token)
        };

    }
    
    /**
     * Verify a teacher by his or her auth token cookie. This is intended
     * as an Express middleware function for controlling access to routes.
     * @param {object}   request  The HTTP request object from Express
     * @param {object}   response The HTTP response object from Express
     * @param {function} next     The next middleware function to execute
     */
    async verifyTeacher(request, response, next) {
        let reject = () => {
            response.clearCookie(Constants.AUTH_COOKIE_NAME);
            response.status(Constants.HTTP_400_BAD_REQUEST);
            response.send({error: Constants.AUTH_TOKEN_MISSING});
        };
        
        let encryptedAuthToken = request.cookies[Constants.AUTH_COOKIE_NAME];
        
        if (!encryptedAuthToken) {
            reject();
            return;
        }
        
        let authToken = cryptoProvider.decrypt(encryptedAuthToken);
        
        if (!authToken){
            reject();
            return;
        }
        
        let teacher = await teacherCache.getTeacher(authToken);
        
        if (!teacher) {
            reject();
            return;
        }
    
        translator.loadServiceTranslations(teacher.language);
        
        request.teacher = teacher;
        
        next();
    }
    
}

module.exports = new LoginProvider();
