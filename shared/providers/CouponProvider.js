'use strict';

let Constants = require('../Constants'),
    CouponModel = require('../models/CouponModel'),
    couponDao = require('../daos/CouponDao'),
    couponCache = require('../caches/CouponCache');

class CouponProvider {
    
    async getCouponByName(couponName) {
        let model = new CouponModel();
        model.name = couponName;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.INVALID_COUPON}
        };
        
        if (!model.isValidName()) {
            return result;
        }
        
        let coupon = await couponCache.getCoupon(model.name);
        
        if (coupon && coupon.expiration_date.getTime() > Date.now()) {
            result.status = Constants.HTTP_200_OK;
            result.data = coupon;
        }
        
        return result;
    }

    async getCoupons(request) {
        let result = {
            status: Constants.HTTP_500_INTERNAL_SERVER_ERROR,
            data: []
        };
        
        let coupons = await couponDao.loadCoupons();
        if (!coupons) {
            return result;
        }

        result.status = Constants.HTTP_200_OK;
        result.data = coupons;
        return result;
    }

    async insertCoupon(request) {
        let model = new CouponModel();
        model.allowBulk = request.body.allowBulk;
        model.amount = request.body.amount;
        model.expirationDate = request.body.expirationDate;
        model.name = request.body.name;

        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {}
        };
        
        if (!model.isValidAllowBulk()) {
            result.data.message = Constants.INVALID_COUPON_ALLOW_BULK;
            return result;
        }
        
        if (!model.isValidName()) {
            result.data.message = Constants.INVALID_COUPON_NAME;
            return result;
        }

        if (!model.isValidExpirationDate()) {
            result.data.message = Constants.INVALID_COUPON_EXPIRATION_DATE;
            return result;
        }

        if (!model.isValidAmount()) {
            result.data.message = Constants.INVALID_AMOUNT_MESSAGE;
            return result;
        }
    
        model.amount = parseInt(model.amount.replace('.', ''));
        
        let coupon = await couponDao.insertCoupon(model);
        if (!coupon) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.data.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }

        if (coupon.error && coupon.error === Constants.NOT_UNIQUE_NAME) {
            result.data.message = 'Coupon name must be unique';
            return result;
        }
        
        model.id = coupon.id;
        await couponCache.addCoupon(model.getCacheable());
    
        result.status = Constants.HTTP_200_OK;
        result.data.message = 'Coupon added successfully.';
        result.data.id = coupon.id;
        return result;
    }

    async updateCoupon(request) {
        let model = new CouponModel();
        model.allowBulk = request.body.allowBulk;
        model.amount = request.body.amount;
        model.expirationDate = request.body.expirationDate;
        model.id = request.params.id;
        model.name = request.body.name;
        model.initialName = request.body.initialName;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST
        };
    
        if (!model.isValidAllowBulk()) {
            result.message = Constants.INVALID_COUPON_ALLOW_BULK;
            return result;
        }
    
        if (!model.isValidId()) {
            result.message = Constants.INVALID_COUPON_ID;
            return result;
        }
        
        if (!model.isValidName()) {
            result.message = Constants.INVALID_PRICE_COMPONENT_NAME;
            return result;
        }

        if (!model.isValidInitialName()) {
            return result;
        }

        if (!model.isValidExpirationDate()) {
            result.message = Constants.INVALID_COUPON_EXPIRATION_DATE;
            return result;
        }

        if (!model.isValidAmount()) {
            result.message = Constants.INVALID_AMOUNT_MESSAGE;
            return result;
        }
    
        model.amount = parseInt(model.amount.replace('.', ''));
        
        let coupon = await couponDao.updateCoupon(model);
        if (!coupon) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            return result;
        }

        if (coupon.error && coupon.error === Constants.NOT_UNIQUE_NAME) {
            result.message = 'Coupon name must be unique';
            return result;
        }
        
        await couponCache.updateCoupon(model.getCacheable());
        
        result.status = Constants.HTTP_200_OK;
        result.message = 'Coupon updated successfully.';
        return result;
    }

    async deleteCoupon(request) {
        let model = new CouponModel();
        model.id = request.params.id;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            message: Constants.INVALID_COUPON_ID
        };
    
        if (!model.isValidId()) {
            return result;
        }

        let queryResult = await couponDao.deleteCoupon(model.id);
        if (queryResult.error) {
            if (queryResult.error == Constants.DATABASE_ERROR) {
                result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                result.message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
            } else if (queryResult.error == Constants.COUPON_IN_USE) {
                result.message = 'This coupon has been used by one or more teachers. It cannot be deleted.';
            }
            return result;
        } else {
            await couponCache.deleteCoupon(queryResult.name);
        }
    
        result.status = Constants.HTTP_200_OK;
        result.message = 'Delete success.';
        return result;
    }
    
}

module.exports = new CouponProvider();
