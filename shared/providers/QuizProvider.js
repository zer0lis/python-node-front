'use strict';

let Constants = require('../Constants'),
    logger = require('../Logger'),
    translate = require('../translations/translator').translate,
    QuizModel = require('../models/QuizModel'),
    ImportQuizModel = require('../models/ImportQuizModel'),
    CloneQuizModel = require('../models/CloneQuizModel'),
    MergeQuizzesModel = require('../models/MergeQuizzesModel'),
    MoveModel = require('../models/MoveModel'),
    FolderNavModel = require('../models/FolderNavModel'),
    folderCache = require('../caches/FolderCache'),
    folderDao = require('../daos/FolderDao'),
    quizCache = require('../caches/QuizCache'),
    quizDao = require('../daos/QuizDao'),
    QuestionModel = require('../models/QuestionModel'),
    AnswerModel = require('../models/AnswerModel'),
    utils = require('../Utils'),
    excelTaskId = 0,
    excelTasks = {},
    excelParser = require('child_process').fork(`${__dirname}/../../tools/scripts/excel-quiz-reader.js`, [], {execArgv: []});

excelParser.on('message', (message) => {
    excelTasks[message.id](message);
});

class QuizProvider {
    
    async getQuizzes(request) {
        let quizzes = await quizDao.selectQuizList(request.teacher.id);
    
        let folders = await folderCache.getFolders(request.teacher.id, Constants.QUIZ_FOLDER);
        
        if (folders) {
            for (let folder of folders) { // Only send folder properties the client cares about.
                delete folder.createdDate;
                delete folder.hidden;
                delete folder.hiddenDate;
                delete folder.type;
                delete folder.userId;
            }
        }
        
        let result = {
            status: Constants.HTTP_200_OK,
            data: {
                folders: folders,
                quizzes: quizzes
            }
        };
        
        if (!quizzes && !folders) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.data = {error: Constants.INTERNAL_SERVER_ERROR}
        }
        
        return result;
    }
    
    async createQuiz(request) {
        let model = new QuizModel();
        model.parentId = request.body.parentId;
    
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.INVALID_FOLDER_DATA}
        };
    
        if (!model.isValidParentId()) {
            logger.error(`Will not create quiz with invalid parentId (${model.parentId})`);
            return result;
        }
    
        let teacher = request.teacher;
    
        if (teacher.level === Constants.FREE_USER && model.parentId !== null) {
            logger.error(`Free user (id ${teacher.id}) tried to create quiz with non-null parentId (${model.parentId})`);
            result.data.error = Constants.FEATURE_NOT_AVAILABLE;
            return result;
        }
        
        let socNumber = await quizDao.insertNewQuiz(teacher.id, translate('Untitled Quiz'), model.parentId);
        
        result.status = socNumber ? Constants.HTTP_200_OK   : Constants.HTTP_500_INTERNAL_SERVER_ERROR;
        result.data =   socNumber ? {soc_number: socNumber} : {error: Constants.INTERNAL_SERVER_ERROR};
        return result;
    }
    
    async importQuizBySoc(request) {
        let model = new ImportQuizModel();
        model.parentId = request.body.parentId;
        model.socNumber = request.body.socNumber;
    
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.INVALID_QUIZ_DATA}
        };
    
        if (!model.isValidParentId() ||
            !model.isValidSocNumber()) {
            logger.error(`Will not import quiz with invalid parentId (${model.parentId}) or socNumber (${model.socNumber})`);
            return result;
        }
        
        let quiz = await quizDao.selectQuizBySoc(model.socNumber, true);
        
        if (!quiz) {
            result.data.error = Constants.QUIZ_NOT_FOUND;
            return result;
        }
    
        let cloneQuizModel = new CloneQuizModel();
        cloneQuizModel.addStandards = true;
        cloneQuizModel.parentId = model.parentId;
        cloneQuizModel.quiz = quiz;
        cloneQuizModel.userId = request.teacher.id;
        
        quiz = await quizDao.cloneQuiz(cloneQuizModel);
        
        if (!quiz) {
            return result;
        }
        
        // Only send quiz properties the client cares about.
        quiz.date = quiz.last_updated;
        quiz.soc = quiz.soc_number;
        
        delete quiz.created_by_id;
        delete quiz.created_date;
        delete quiz.is_hidden;
        delete quiz.hidden_date;
        delete quiz.last_updated;
        delete quiz.legacy_id;
        delete quiz.soc_number;
    
        result.status = quiz ? Constants.HTTP_201_CREATED : Constants.HTTP_500_INTERNAL_SERVER_ERROR;
        result.data   = quiz ? quiz                       : {error: Constants.INTERNAL_SERVER_ERROR};
        return result;
    }
    
    async copyQuiz(request) {
        let model = new QuizModel();
        model.name = request.body.name;
        model.parentId = request.body.parentId;
        model.socNumber = request.body.socNumber;
    
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.INVALID_QUIZ_DATA}
        };
    
        if (!model.isValidParentId() ||
            !model.isValidSocNumber()) {
            logger.error(`Will not copy quiz with invalid parentId (${model.parentId}) or socNumber (${model.socNumber})`);
            return result;
        }
    
        let quiz = await quizDao.selectQuizBySoc(model.socNumber);
        
        if (!quiz) {
            result.data.error = Constants.QUIZ_NOT_FOUND;
            return result;
        }
        
        if (!model.name || model.name.trim().length === 0) {
            model.name = `${quiz.name} (${translate('copy')})`;
        } else {
            model.name = model.name.trim();
        }
        
        let cloneQuizModel = new CloneQuizModel();
        cloneQuizModel.addStandards = true;
        cloneQuizModel.parentId = model.parentId;
        cloneQuizModel.quiz = quiz;
        cloneQuizModel.userId = request.teacher.id;
        cloneQuizModel.name = model.name;
        
        let copiedQuiz = await quizDao.cloneQuiz(cloneQuizModel);
        
        if (copiedQuiz) {
            let newQuiz = {
                date: copiedQuiz.last_updated,
                soc: copiedQuiz.soc_number,
                name: copiedQuiz.name,
                parent_id: copiedQuiz.parent_id, // Unfortunately all other quiz APIs return snake_case properties.
                id: copiedQuiz.id,
                sharable: copiedQuiz.sharable,
                standard: copiedQuiz.standard || ''
            };
            
            result.status = Constants.HTTP_201_CREATED;
            result.data = newQuiz;
        } else {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.data = {error: Constants.INTERNAL_SERVER_ERROR};
        }
        
        return result;
    }
    
    async mergeQuizzes(request) {
        let model = new MergeQuizzesModel();
        model.quizSoc1 = request.body.quizSoc1;
        model.quizSoc2 = request.body.quizSoc2;
        model.parentId = request.body.parentId;

        if (request.body.name) {
            model.name = request.body.name;
        }

        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.INVALID_MERGE_QUIZ_DATA}
        };

        if (!model.isValidParentId() ||
            !model.isValidName() ||
            !model.isValidQuizSoc1() ||
            !model.isValidQuizSoc2() ||
            model.quizSoc1 === model.quizSoc2) {
            return result;
        }

        let teacher = request.teacher;

        let sharableMapping = await quizDao.getSharable([
                model.quizSoc1,
                model.quizSoc2
            ],
            teacher.id);

        if (!sharableMapping || sharableMapping.size !== 2) {
            result.data.error = Constants.QUIZ_NOT_FOUND;
            return result;
        }

        let quizIds = [sharableMapping.get(model.quizSoc1).id, sharableMapping.get(model.quizSoc2).id];
        let questionsMapping = await quizDao.getQuestionIds(quizIds);

        if (!questionsMapping || questionsMapping.size !== 2) { //both quizzes need to be valid ie: have questions
            result.data.error = Constants.QUIZ_NOT_FOUND;
            return result;
        }


        let quizModel = new QuizModel();
        quizModel.name = model.name.trim();
        quizModel.isHidden = false;
        quizModel.createdById = teacher.id;
        quizModel.sharable = sharableMapping.get(model.quizSoc1).sharable || sharableMapping.get(model.quizSoc2).sharable;
        quizModel.parentId = model.parentId;

        let order = 1; // recreate the order in the new quiz, first will come the questions from quizSoc1
        let orderMapping = new Map();
        for(let quizId of quizIds) {
            for (let questionId of questionsMapping.get(quizId)) {
                orderMapping.set(questionId, order);
                order += 1;
            }
        }

        if (orderMapping.size > Constants.MAX_NUMBER_OF_QUESTIONS) {
            result.data.error = Constants.TOO_MANY_QUESTIONS_TO_BE_MERGED;
            return result;
        }

        result = await quizDao.mergeQuizzes(quizModel, orderMapping);
        
        let quiz = result.quiz;
        
        if (quiz) {
            quiz.parent_id = quiz.parentId; // Unfortunately all other quiz APIs return snake_case properties.
            quiz.standard = '';
            
            // Only send quiz properties the client cares about.
            delete quiz.createdById;
            delete quiz.isHidden;
            delete quiz.lastUpdated;
            delete quiz.parentId;
        }

        result.status = result.error ? Constants.HTTP_500_INTERNAL_SERVER_ERROR : Constants.HTTP_200_OK;
        result.data   = result.error ? {error: Constants.INTERNAL_SERVER_ERROR} : quiz;
        return result;
    }
    
    addExcelTask(filename, teacherId, callback) {
        let id = excelTaskId++;
        excelTasks[id] = callback;
        excelParser.send({id: id, filename: filename, teacherId: teacherId});
    }
    
    importQuizByExcel(request, callback) {
        let model = new ImportQuizModel();
        model.file = request.file;
        model.parentId = request.body.parentId;
        
        let errors = {};
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {errors: errors}
        };
        
        if (!model.isValidFile()) {
            errors[Constants.INVALID_FILE_TYPE] = true;
            callback(result);
            return;
        }
        
        if (!model.isValidParentId()) {
            errors[Constants.INVALID_FOLDER_DATA] = true;
            callback(result);
            return;
        }
        
        let teacher = request.teacher;
        
        this.addExcelTask(model.file.path, teacher.id, async(response) => {
            if (response.error) {
                logger.error('Error reading Excel quiz:', response.error);
                result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                errors[Constants.INTERNAL_SERVER_ERROR] = true;
                callback(result);
                return;
            }
            
            let numQuestions = response.numQuestions,
                worksheet = response.worksheet;
            
            if (numQuestions === 0 || numQuestions > Constants.EXCEL_QUIZ_MAX_QUESTIONS) {
                errors[numQuestions === 0 ? Constants.QUESTIONS_MISSING : Constants.TOO_MANY_QUESTIONS] = true;
                callback(result);
                return;
            }
            
            let quizNameCell = worksheet[Constants.EXCEL_QUIZ_NAME_CELL];
            
            if (!quizNameCell || quizNameCell.v.length > Constants.MAX_QUIZ_NAME_LENGTH) {
                errors[!quizNameCell ? Constants.QUIZ_NAME_MISSING : Constants.QUIZ_NAME_TOO_LONG] = Constants.EXCEL_QUIZ_NAME_CELL;
                result.data.cell = Constants.EXCEL_QUIZ_NAME_CELL;
                callback(result);
                return;
            } else if (quizNameCell.v === '' || quizNameCell.v === 'Enter the name of the quiz here') {
                quizNameCell.v = translate('Untitled Quiz');
            }
            
            let quiz = new QuizModel();
            quiz.createdById = teacher.id;
            quiz.name = utils.escapeHtml(quizNameCell.v);
            quiz.parentId = model.parentId;
            
            let userError = false,
                questions = [],
                questionOrder = 1,
                lastRow = numQuestions + Constants.EXCEL_QUIZ_FIRST_QUESTION_ROW - 1;
                // The first 6 rows have no questions, so the last question row is: numQuestions + 7 - 1.
            
            for (let row = Constants.EXCEL_QUIZ_FIRST_QUESTION_ROW; row <= lastRow; row++) {
                let questionTypeCell = `A${row}`,
                    questionTypeCellObject = worksheet[questionTypeCell];
                
                let questionTextCell = `B${row}`,
                    questionTextCellObject = worksheet[questionTextCell];
                
                if (!questionTypeCellObject || questionTypeCellObject.v === '') {
                    if (!errors[Constants.QUESTION_TYPE_MISSING]) {
                        errors[Constants.QUESTION_TYPE_MISSING] = [];
                    }
                    errors[Constants.QUESTION_TYPE_MISSING].push(questionTypeCell);
                    userError = true;
                }
                
                if (!questionTextCellObject || questionTextCellObject.v === '') {
                    if (!errors[Constants.QUESTION_TEXT_MISSING]) {
                        errors[Constants.QUESTION_TEXT_MISSING] = [];
                    }
                    errors[Constants.QUESTION_TEXT_MISSING].push(questionTextCell);
                    userError = true;
                }
                
                if (!questionTypeCellObject) {
                    continue;
                }
                
                let question = new QuestionModel();
                question.createdById = teacher.id;
                question.createdDate = new Date();
                question.questionText = utils.escapeHtml(questionTextCellObject ? questionTextCellObject.v + '' : '');
                question.order = questionOrder;
                
                if (question.questionText.length > Constants.MAX_QUESTION_LENGTH) {
                    if (!errors[Constants.QUIZ_QUESTION_TOO_LONG]) {
                        errors[Constants.QUIZ_QUESTION_TOO_LONG] = [];
                    }
                    errors[Constants.QUIZ_QUESTION_TOO_LONG].push(questionTextCell);
                    userError = true;
                }
                
                if (questionTypeCellObject.v === Constants.EXCEL_OPEN_ENDED_QUESTION_TYPE) {
                    question.type = Constants.FREE_RESPONSE_QUESTION;
                } else if (questionTypeCellObject.v === Constants.EXCEL_MULTIPLE_CHOICE_QUESTION_TYPE) {
                    question.type = Constants.MULTIPLE_CHOICE_QUESTION;
                    
                    let correctAnswers = new Map(),
                        answers = [],
                        answerOrder = 1;
                    
                    for (let column of ['H', 'I', 'J', 'K', 'L']) {
                        let correctAnswerCell = worksheet[`${column}${row}`];
                        if (correctAnswerCell && ['A', 'B', 'C', 'D', 'E'].indexOf(correctAnswerCell.v != -1)) {
                            correctAnswers.set(correctAnswerCell.v, true);
                        }
                    }
                    
                    for (let column of ['C', 'D', 'E', 'F', 'G']) {
                        let answerCell = worksheet[`${column}${row}`];
                        if (!answerCell || answerCell.v === '') {
                            continue;
                        }
                        
                        let answer = new AnswerModel();
                        answer.createdById = teacher.id;
                        answer.isCorrect = correctAnswers.has(String.fromCharCode(column.charCodeAt(0) - 2));
                        answer.order = answerOrder;
                        answer.text = utils.escapeHtml(answerCell.v + '');
                        
                        if (answerCell.t === 'b') {
                            // If the cell type is boolean, preserve case by getting
                            // the formatted text instead of the value (see SOC-647).
                            answer.text = utils.escapeHtml(answerCell.w + '');
                        }
                        
                        if (answer.text.length > Constants.MAX_ANSWER_LENGTH) {
                            if (!errors[Constants.QUIZ_ANSWER_TOO_LONG]) {
                                errors[Constants.QUIZ_ANSWER_TOO_LONG] = [];
                            }
                            errors[Constants.QUIZ_ANSWER_TOO_LONG].push(`${column}${row}`);
                            userError = true;
                        }
                        
                        answers.push(answer);
                        answerOrder++;
                    }
                    
                    if (answers.length < 2) {
                        if (!errors[Constants.NOT_ENOUGH_ANSWERS]) {
                            errors[Constants.NOT_ENOUGH_ANSWERS] = [];
                        }
                        errors[Constants.NOT_ENOUGH_ANSWERS].push(row);
                        userError = true;
                    }
                    
                    question.answers = answers;
                } else {
                    if (!errors[Constants.INVALID_QUESTION_TYPE]) {
                        errors[Constants.INVALID_QUESTION_TYPE] = [];
                    }
                    errors[Constants.INVALID_QUESTION_TYPE].push(questionTypeCell);
                    userError = true;
                }
                
                questions.push(question);
                questionOrder++;
            }
    
            if (!userError) {
                quiz = await quizDao.insertExcelQuiz(quiz, questions);
            }
            
            if (userError || !quiz) {
                result.status = userError ? Constants.HTTP_400_BAD_REQUEST : Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                errors[Constants.INTERNAL_SERVER_ERROR] = true;
            } else {
                // Only send quiz properties the client cares about.
                quiz.parent_id = quiz.parentId;
                
                delete quiz.createdById;
                delete quiz.createdDate;
                delete quiz.isHidden;
                delete quiz.lastUpdated;
                delete quiz.parentId;
                delete quiz.socNumber;
                
                result.status = Constants.HTTP_201_CREATED;
                result.data = quiz;
            }
            
            callback(result);
        });
    }
    
    async moveQuizzes(request) {
        let model = new MoveModel();
        model.folderIds = request.body.folderIds;
        model.quizIds = request.body.quizIds;
        model.parentId = request.body.parentId;
        model.task = request.body.task;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.INVALID_QUIZ_DATA}
        };
        
        if (!model.isValidIds() ||
            !model.isValidParentId() ||
            !model.isValidTask()) {
            return result;
        }
    
        let teacher = request.teacher;
    
        if (model.task === Constants.MOVE_TASK && teacher.level === Constants.FREE_USER) {
            result.data.error = Constants.FEATURE_NOT_AVAILABLE; // Free users can only delete/restore.
            return result;
        }
        
        let cachedFolders = await folderCache.getFolders(teacher.id, Constants.QUIZ_FOLDER);
    
        if (!cachedFolders) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.data.error = Constants.INTERNAL_SERVER_ERROR;
            return result;
        }
    
        let idFolderMap = new Map();
        
        for (let folder of cachedFolders) {
            idFolderMap.set(folder.id, folder);
        }
        
        if (model.task === Constants.MOVE_TASK && model.parentId !== null) {
            let destinationFolder = idFolderMap.get(model.parentId);
            
            if (!destinationFolder || destinationFolder.deleted || destinationFolder.hidden) {
                result.data.error = Constants.INVALID_FOLDER_DATA;
                return result;
            }
        }
        
        if (model.quizIds.length > 0) {
            let quizzes = await quizDao.selectActiveQuizzesById(model.quizIds, teacher.id);
            
            if (!quizzes) {
                result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                result.data.error = Constants.INTERNAL_SERVER_ERROR;
                return result;
            }
            
            if (quizzes.length !== model.quizIds.length) {
                result.data.error = Constants.INVALID_QUIZ_DATA;
                return result;
            }
        }
    
        if (model.folderIds.length > 0) {
            for (let folderId of model.folderIds) {
                if (!idFolderMap.get(folderId)) {
                    result.data.error = Constants.FOLDER_NOT_FOUND;
                    return result;
                }
            }
        }
        
        result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
        result.data.error = Constants.INTERNAL_SERVER_ERROR;
        
        let success = true,
            updatedFolders = [],
            txClient = await quizDao.getTxClient();
        
        if (!txClient) {
            return result;
        }
        
        try {
            if (model.folderIds.length > 0) {
                model.deleted = model.task === Constants.DELETE_TASK;
                model.type = Constants.QUIZ_FOLDER;
                model.userId = teacher.id;
                
                if (model.task === Constants.MOVE_TASK) {
                    updatedFolders = await folderDao.updateParentId(model, txClient);
                } else {
                    updatedFolders = await folderDao.updateDeleted(model, txClient);
                }
                
                if (!updatedFolders) {
                    success = false;
                    return result;
                }
            }
            
            if (model.quizIds.length > 0) {
                if (model.task === Constants.MOVE_TASK) {
                    success = await quizDao.updateParentId(model.quizIds, model.parentId, txClient);
                } else {
                    success = await quizDao.updateDeleted(model.quizIds, model.task === Constants.DELETE_TASK, txClient);
                }
                
                if (!success) {
                    return result;
                }
                
                await quizCache.deleteQuizzes(model.quizIds);
            }
            
            // Only update the folder cache after we know the SQL transaction didn't fail on quizzes.
            if (updatedFolders.length > 0) {
                for (let folder of updatedFolders) {
                    idFolderMap.set(folder.id, folder);
                }
                
                await folderCache.purgeFolders(teacher.id, Constants.QUIZ_FOLDER);
                await folderCache.addFolders(teacher.id, Constants.QUIZ_FOLDER, idFolderMap.values());
            }
            
            result.status = success ? Constants.HTTP_200_OK : Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.data   = success ? {}                    : {error: Constants.INTERNAL_SERVER_ERROR};
            return result;
        } catch (error) {
            logger.error('Error moving quizzes and/or quiz folders:', error);
            success = false;
        } finally {
            if (success) {
                await txClient.query('COMMIT');
            } else {
                await txClient.query('ROLLBACK');
            }
            txClient.release();
        }
    }
    
    async purgeQuizzes(request) {
        let model = new MoveModel();
        model.folderIds = request.body.folderIds;
        model.quizIds = request.body.quizIds;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.INVALID_QUIZ_DATA}
        };
        
        if (!model.isValidIds()) {
            return result;
        }
        
        let teacher = request.teacher,
            purgingIdFolderMap = new Map(),
            purgingIdQuizMap = new Map();
        
        if (model.folderIds.length > 0) {
            let cachedFolders = await folderCache.getFolders(teacher.id, Constants.QUIZ_FOLDER);
            
            if (!cachedFolders) {
                result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                result.data.error = Constants.INTERNAL_SERVER_ERROR;
                return result;
            }
            
            let existingIdFolderMap = new Map(),
                folderNav = new FolderNavModel('Quizzes', cachedFolders);
            
            for (let folder of cachedFolders) {
                existingIdFolderMap.set(folder.id, folder);
            }
            
            for (let folderId of model.folderIds) {
                if (!existingIdFolderMap.get(folderId)) {
                    result.data.error = Constants.FOLDER_NOT_FOUND;
                    return result;
                }
                
                if (!purgingIdFolderMap.has(folderId)) {
                    folderNav.depthFirstPreOrder(folderNav.idFolderMap.get(folderId), 0, (folder) => {
                        purgingIdFolderMap.set(folder.id, true);
                    });
                }
            }
            
            let childQuizzes = await quizDao.selectByParentId(Array.from(purgingIdFolderMap.keys()), teacher.id);
            
            if (!childQuizzes) {
                result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                result.data.error = Constants.INTERNAL_SERVER_ERROR;
                return result;
            }
            
            for (let quiz of childQuizzes) {
                purgingIdQuizMap.set(quiz.id, true);
            }
        }
        
        if (model.quizIds.length > 0) {
            let quizzes = await quizDao.selectActiveQuizzesById(model.quizIds, teacher.id);
            
            if (!quizzes) {
                result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                result.data.error = Constants.INTERNAL_SERVER_ERROR;
                return result;
            }
            
            if (quizzes.length !== model.quizIds.length) {
                result.data.error = Constants.INVALID_QUIZ_DATA;
                return result;
            }
            
            for (let quiz of quizzes) {
                purgingIdQuizMap.set(quiz.id, true);
            }
        }
        
        let success = await quizDao.updateHidden(Array.from(purgingIdFolderMap.keys()), Array.from(purgingIdQuizMap.keys()));
        
        if (success) {
            for (let id of purgingIdFolderMap.keys()) {
                await folderCache.removeFolder(teacher.id, Constants.QUIZ_FOLDER, id);
            }
    
            if (purgingIdQuizMap.size > 0) {
                await quizCache.deleteQuizzes(purgingIdQuizMap.keys());
            }
        }
        
        result.status = success ? Constants.HTTP_200_OK : Constants.HTTP_500_INTERNAL_SERVER_ERROR;
        result.data   = success ? {}                    : {error: Constants.INTERNAL_SERVER_ERROR};
        return result;
    }
    
}

module.exports = new QuizProvider();
