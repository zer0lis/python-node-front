'use strict';

let Constants = require('../Constants'),
    logger = require('../Logger'),
    PasswordModel = require('../models/PasswordModel'),
    teacherDao = require('../daos/TeacherDao'),
    passwordCache = require('../caches/PasswordCache'),
    workerCache = require('../caches/WorkerCache'),
    cryptoProvider = require('./CryptoProvider'),
    crypto = require('crypto'),
    uuid = require('uuid'),
    config = require('config').get('config');

class RoomProvider {
//     """
//     service for clear room api
//         :param request:
// :param dataDict
//     """
//
//     user_uuid = dataDict.get("auth_token")
//     room_name = dataDict.get("room_name")
//
//     if not isStringNotEmpty(user_uuid):
//     return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST
//
//     if not isStringNotEmpty(room_name):
//     return BaseError.ROOM_NAME_MISSING, status.HTTP_400_BAD_REQUEST
//
//     try:
//         self.daoRegistry.roomDao.checkRoomPermission(user_uuid, room_name.lower())
//
//     room = self.serviceRegistry.roomService.validateRoom(room_name.lower())
//     if room.status < RoomModel.ARCHIVED and room.status & RoomModel.ROSTERED == RoomModel.ROSTERED:
//         self.daoRegistry.studentDao.logoutStudents(room.id)
//
//     publish.send_model(room_name.lower() + '-student', key="student", data="room_cleared")
//     if room.status & RoomModel.DEFAULT == RoomModel.DEFAULT:
//         self.roomFullLimiter.invalidateData(room.name_lower)
//
//     return {}, status.HTTP_200_OK
//
// except BaseDao.NotFound:
// return BaseError.AUTH_TOKEN_MISSING, status.HTTP_400_BAD_REQUEST
//
// except Exception as e:
// logger.error(exceptionStack(e))
// return BaseError.INTERNAL_SERVER_ERROR, status.HTTP_500_INTERNAL_SERVER_ERROR

    async clearRoom(request) {
        let failCounter = 'socrative.views.rooms.clear_room.fail';
        console.log(request);
        return 'test';
    }

    async createPasswordToken(request, sendEmail = true) {
        let model = new PasswordModel();
        model.email = request.body.email;

        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.INVALID_EMAIL}
        };

        if (!model.isValidEmail()) {
            return result;
        }

        let teacher = await teacherDao.loadTeacherByEmail(model.email);

        if (!teacher) {
            result.data.error = Constants.USER_NOT_FOUND;
            return result;
        } else {
            let token = uuid.v4().replace(/-/g, ''),
                success = await passwordCache.addToken(token, teacher.id);

            if (!success) {
                result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                result.data.error = Constants.INTERNAL_SERVER_ERROR;
                return result;
            }

            let hostname = config.app.hostname,
                url = `${/(a.socrative.com|localhost)/.test(hostname) ? 'http' : 'https'}://${hostname}/login/teacher/#reset-password/${token}`;

            if (sendEmail) {
                let tasks = [
                    JSON.stringify({
                        email_type: Constants.FORGOT_PASSWORD,
                        first_name: teacher.first_name,
                        to: teacher.email,
                        url: url,
                        user_id: teacher.id
                    })
                ];

                await workerCache.addWorkerTasks(`${Constants.SEND_EMAIL_TASK}${config.platform}`, tasks);
            }

            result.status = Constants.HTTP_200_OK;
            result.data = sendEmail ? {} : {url: url};
            return result;
        }
    }

    async changePassword(request) {
        let model = new PasswordModel();
        model.password = request.body.password;
        model.token = request.body.token;

        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.INVALID_PASSWORD}
        };

        if (!model.isValidPassword()) {
            return result;
        }

        if (!model.isValidToken()) {
            result.data.error = Constants.INVALID_CHANGE_PASSWORD_TOKEN;
            return result;
        }

        let teacherId = await passwordCache.getTeacherId(model.token);

        if (!teacherId) {
            result.data.error = Constants.TEMPORARY_TOKEN_EXPIRED;
            return result;
        }

        let hashedPassword = null;

        try {
            hashedPassword = await this.hashPassword(model.password);
        } catch (error) {
            logger.error('Error hashing password during password change:', error);
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.data.error = Constants.INTERNAL_SERVER_ERROR;
            return result;
        }

        let success = await teacherDao.updatePassword(hashedPassword, teacherId);

        if (success) {
            result.status = Constants.HTTP_200_OK;
            result.data = {};
        } else {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.data.error = Constants.INTERNAL_SERVER_ERROR;
        }

        return result;
    }

    hashPassword(password) {
        return new Promise((resolve, reject) => {
            let salt = this.makeSalt(),
                iterations = Constants.PASSWORD_ITERATIONS,
                keyLength = Constants.PASSWORD_KEY_LENGTH,
                digest = Constants.PASSWORD_DIGEST,
                algorithm = Constants.PASSWORD_ALGORITHM;

            crypto.pbkdf2(password, salt, iterations, keyLength, digest, (error, derived) => {
                if (error) {
                    reject(error);
                } else {
                    resolve(`${algorithm}_${digest}$${iterations}$${salt}$${derived.toString('base64')}`);
                }
            });
        });
    }

    makeSalt() {
        let result = '',
            possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

        for (let i = 0; i < 12; i++) {
            result += possible.charAt(Math.floor(Math.random() * possible.length));
        }

        return result;
    }

}

module.exports = new RoomProvider();
