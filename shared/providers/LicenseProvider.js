'use strict';

let Constants = require('../Constants'),
    logger = require('../Logger'),
    LicenseModel = require('../models/LicenseModel'),
    couponCache = require('../caches/CouponCache'),
    priceCache = require('../caches/PriceCache'),
    teacherDao = require('../daos/TeacherDao'),
    licenseDao = require('../daos/LicenseDao'),
    licenseCache = require('../caches/LicenseCache'),
    config = require('config').get('config'),
    stripe = require('stripe')(config.stripe.apiKey),
    uuid = require('uuid'),
    workerCache = require('../caches/WorkerCache'),
    activationDao = require('../daos/ActivationDao'),
    teacherCache = require('../caches/TeacherCache');

class LicenseProvider {
    
    async createLicense(request) {
        let model = new LicenseModel();
        model.applyNow = request.body.applyNow;
        model.autoRenew = request.body.autoRenew;
        model.buyerId = request.body.buyerId;
        model.couponName = request.body.couponName;
        model.expectedTotalPrice = request.body.expectedTotalPrice;
        model.seats = request.body.seats;
        model.tokenId = request.body.tokenId;
        model.years = request.body.years;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.MISSING_LICENSE_DATA}
        };
        
        if (!model.isValidApplyNow() ||
            !model.isValidAutoRenew() ||
            !model.isValidBuyerId() ||
            !model.isValidExpectedTotalPrice() ||
            !model.isValidSeats() ||
            !model.isValidTokenId() ||
            !model.isValidYears()) {
            return result;
        }
        
        let couponError = null;
        
        if (model.couponName) {
            model.coupon = await couponCache.getCoupon(model.couponName);
            if (!model.coupon) {
                couponError = Constants.INVALID_COUPON;
            } else if (model.coupon.expiration_date.getTime() < Date.now()) {
                couponError = Constants.EXPIRED_COUPON;
            }
        }
    
        if (couponError) {
            result.data.error = couponError;
            return result;
        }
        
        let clientExpectedTotalPrice = model.expectedTotalPrice; // Remeber the price expected by the client so the service can calculate and compare.
        model.prices = await priceCache.getPrices();
        model.calculatePriceData();
        
        if (clientExpectedTotalPrice !== model.expectedTotalPrice) {
            result.data = {
                error: Constants.PRICE_MISMATCH,
                prices: model.prices
            };
            return result;
        }
        
        let teacher = request.teacher;
        
        let chargeData = {
            amount: model.expectedTotalPrice,
            currency: 'USD',
            description: `Charge for ${teacher.email}`,
            metadata: {
                applyNow: model.applyNow,
                autoRenew: model.autoRenew,
                buyerId: model.buyerId,
                couponName: model.couponName || 'n/a',
                seats: model.seats,
                years: model.years,
                pricePerSeat: model.pricePerSeatFormatted
            },
            statement_descriptor: 'Socrative Pro License'
        };
        
        let customer = null;
        
        if (model.autoRenew) {
            try {
                customer = await stripe.customers.create({
                    description: `Customer for ${teacher.email}`,
                    email: teacher.email,
                    source: model.tokenId
                });
                chargeData.customer = customer.id;
            } catch (error) {
                return this.createStripeError(error);
            }
        } else {
            chargeData.source = model.tokenId;
        }
        
        let chargeResponse = null;
        try {
            chargeResponse = await stripe.charges.create(chargeData);
        } catch (error) {
            return this.createStripeError(error);
        }
        
        let expirationDate = teacher.level === Constants.PRO_USER ? teacher.proExpiration : new Date(),
            purchaseDate = new Date();
        
        expirationDate.setFullYear(expirationDate.getFullYear() + model.years);
        expirationDate.setHours(0); // Set the hour to midnight to avoid time zone issues.
        
        let key = this.createLicenseKey();
        
        let licenseProps = {
            buyerId: model.buyerId,
            key: key,
            expirationDate: expirationDate,
            couponId: model.coupon ? model.coupon.id : null,
            autoRenew: model.autoRenew,
            years: model.years,
            pricePerSeat: model.pricePerSeat,
            customerId: customer ? customer.id : null,
            stripeResponse: chargeResponse,
            seats: model.seats,
            totalPrice: model.expectedTotalPrice,
            purchaseDate: purchaseDate
        };
        
        let licenseId = await licenseDao.insertLicense(licenseProps);
        
        if (!licenseId) {
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.data.error = Constants.INTERNAL_SERVER_ERROR;
            return result;
        }

        if (model.applyNow) {
            let {error} = await activationDao.insertActivations(licenseId, [teacher]);
            if (error) {
                logger.error('Error activating license after purchase:', error);
                result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                result.data.error = Constants.INTERNAL_SERVER_ERROR;
                return result;
            }
            
            await teacherCache.removeTeacher(teacher.auth_token);
            
            let tasks = [
                JSON.stringify({
                    email_address: teacher.email,
                    email_type: Constants.UPGRADE_PRO_EMAIL
                })
            ];

            await workerCache.addWorkerTasks(`${Constants.SEND_EMAIL_TASK}${config.platform}`, tasks);
        }

        let receiptTasks = [
            JSON.stringify({
                email_address: teacher.email.toLowerCase(),
                email_type: Constants.RECEIPT,
                user_id: teacher.id
            })
        ];

        if (model.seats > 1) {
            receiptTasks.push(JSON.stringify({
                email_address: teacher.email,
                email_type: Constants.MULTI_SEATS_RECEIPT
            }));
        }

        await workerCache.addWorkerTasks(`${Constants.SEND_EMAIL_TASK}${config.platform}`, receiptTasks);

        result.status = Constants.HTTP_200_OK;
        result.data = {key: key, id: licenseId};
        return result;
    }
    
    createStripeError(error) {
        return {
            status: Constants.HTTP_500_INTERNAL_SERVER_ERROR,
            data:{
                error: Constants.STRIPE_ERROR,
                message: error.message,
                type: error.type
            }
        };
    }
    
    createLicenseKey() {
        let licenseKey = uuid.v4();
        licenseKey = licenseKey.slice(0, 14) + licenseKey.slice(22);
        let chars = licenseKey.replace(/-/g, '').split('');
        for (let i = 5; i <= 23; i += 6) {
            chars.splice(i, 0, '-');
        }
        return chars.join('').toUpperCase();
    }
    
    async changeAutoRenew(request) {
        let teacher = request.teacher;
        
        let model = new LicenseModel();
        model.autoRenew = request.body.autoRenew;
        model.key = teacher.licenseKey;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.MISSING_LICENSE_DATA}
        };
        
        if (!model.isValidAutoRenew() ||
            !model.isValidKey()) {
            return result;
        }
        
        let license = await licenseDao.updateAutoRenew(model, teacher.id);
        
        if (license) {
            await licenseCache.updateLicense(license);
            
            result.status = Constants.HTTP_200_OK;
            result.data = {};
        } else {
            logger.error(`Error updating auto_renew for user ${teacher.id} with license key ${model.key}`);
        }
        
        return result;
    }
    
}

module.exports = new LicenseProvider();
