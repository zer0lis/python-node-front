'use strict';

let Constants = require('../Constants'),
    utils = require('../Utils'),
    config = require('config').get('config'),
    AccountModel = require('../models/AccountModel'),
    CloneQuizModel = require('../models/CloneQuizModel'),
    accountDao = require('../daos/AccountDao'),
    licenseDao = require('../daos/LicenseDao'),
    partnerDao = require('../daos/PartnerDao'),
    quizDao = require('../daos/QuizDao'),
    roomDao = require('../daos/RoomDao'),
    passwordProvider = require('./PasswordProvider'),
    loginProvider = require('./LoginProvider'),
    cryptoProvider = require('./CryptoProvider'),
    pubnubProvider = require('./PubnubProvider'),
    teacherCache = require('../caches/TeacherCache'),
    workerCache = require('../caches/WorkerCache');

class AccountProvider {
    
    async checkAvailable(email) {
        let model = new AccountModel();
        model.email = email;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.INVALID_EMAIL}
        };
        
        if (!model.isValidEmail()) {
            return result;
        }
        
        let available = await accountDao.isAvailable(model.email);
        
        if (available) {
            result.status = Constants.HTTP_200_OK;
            result.data = {};
        }
        
        return result;
    }
    
    async createAccount(request) {
        let model = new AccountModel();
        model.country = request.body.country;
        model.districtName = request.body.districtName;
        model.email = request.body.email;
        model.firstName = request.body.firstName;
        model.language = request.cookies[Constants.LANGUAGE_COOKIE_NAME];
        model.lastName = request.body.lastName;
        model.password = request.body.password;
        model.orgName = request.body.orgName;
        model.orgType = request.body.orgType;
        model.role = request.body.role;
        model.schoolId = request.body.schoolId;
        model.schoolName = request.body.schoolName;
        model.state = request.body.state;
        model.zipCode = request.body.zipCode;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.INVALID_ACCOUNT_DATA}
        };
        
        if (!model.isValidCountry() ||
            !model.isValidDistrictName() ||
            !model.isValidEmail() ||
            !model.isValidFirstName() ||
            !model.isValidLastName() ||
            !model.isValidPassword() ||
            !model.isValidOrgName() ||
            !model.isValidOrgType() ||
            !model.isValidRole() ||
            !model.isValidSchoolId() ||
            !model.isValidSchoolName() ||
            !model.isValidState() ||
            !model.isValidZipCode()) {
            return result;
        }
        
        if (!model.isValidLanguage()) {
            model.language = 'en';
        }
        
        if (model.orgType === Constants.ORGANIZATION_HIGHER_ED) {
            model.university = model.orgName;
        }
        
        if (model.orgType === Constants.ORGANIZATION_CORP ||
            model.orgType === Constants.ORGANIZATION_OTHER) {
            model.description = model.orgName;
        }
        
        try {
            model.password = await passwordProvider.hashPassword(model.password);
        } catch (error) {
            logger.error('Error hashing password during account creation:', error);
            result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
            result.data.error = Constants.INTERNAL_SERVER_ERROR;
            return result;
        }
        
        model.authToken = loginProvider.generateAuthToken();
        model.role = JSON.stringify(model.role);
        model.zipCode = utils.convertEmtpyStringToNull(model.zipCode);
        model.university = utils.convertEmtpyStringToNull(model.university);
        model.description = utils.convertEmtpyStringToNull(model.description);
        model.schoolName = utils.convertEmtpyStringToNull(model.schoolName);
        model.state = utils.convertEmtpyStringToNull(model.state);
        model.districtName = utils.convertEmtpyStringToNull(model.districtName);
        
        let accountId = await accountDao.insertAccount(model);
        
        if (!accountId) {
            return result;
        }
        
        let initialQuiz = await quizDao.selectQuizBySoc(config.app.initialQuizSocNumber, false, true);
        
        if (initialQuiz) {
            let cloneQuizModel = new CloneQuizModel();
            cloneQuizModel.hideOldQuiz = false;
            cloneQuizModel.name = initialQuiz.name;
            cloneQuizModel.parentId = null;
            cloneQuizModel.quiz = initialQuiz;
            cloneQuizModel.userId = accountId;
            
            await quizDao.cloneQuiz(cloneQuizModel);
        }
        
        let task = [
            JSON.stringify({
                email_address: model.email,
                email_type: Constants.REGISTRATION_EMAIL,
                user_id: accountId
            })
        ];

        await workerCache.addWorkerTasks(`${Constants.SEND_EMAIL_TASK}${config.platform}`, task);
        
        result.data = {id: accountId};
        result.language = model.language;
        result.status = Constants.HTTP_201_CREATED;
        result.token = cryptoProvider.encryptUserAuthToken(model.authToken);
        
        return result;
    }
    
    async getAccount(request) {
        let teacher = request.teacher;
        
        let model = new AccountModel();
        
        if (teacher.level === Constants.PRO_USER) {
            let autoRenew = await licenseDao.selectAutoRenew(teacher.id, teacher.licenseKey);
            model.autoRenew = autoRenew.value;
            model.canAutoRenew = autoRenew.allowed;
            model.licenseKey = teacher.licenseKey;
        }

        model.isBuyer = await licenseDao.hasStripeCharge(teacher.id);
        
        model.country = teacher.country;
        model.language = teacher.language;
        model.districtName = teacher.district_name;
        model.email = teacher.email;
        model.expiration = teacher.proExpiration;
        model.firstName = teacher.first_name;
        model.lastName = teacher.last_name;
        model.mcUser = await partnerDao.isPartnerType(teacher.id, Constants.MASTERY_CONNECT_PARTNER);
        
        if (teacher.organization_type === Constants.ORGANIZATION_CORP ||
            teacher.organization_type === Constants.ORGANIZATION_OTHER) {
            model.orgName = teacher.description;
        } else if (teacher.organization_type === Constants.ORGANIZATION_HIGHER_ED) {
            model.orgName = teacher.university;
        }
        
        model.orgType = teacher.organization_type;
        
        try {
            model.role = JSON.parse(teacher.user_role);
        } catch(error) {
            model.role = [];
        }
        
        model.schoolId = teacher.school_id;
        model.schoolName = teacher.school_name;
        model.state = teacher.state;
        model.zipCode = teacher.zip_code;
        
        // Only send account properties the client cares about.
        delete model.authToken;
        delete model.banner;
        delete model.buyingLicense;
        delete model.currentPassword;
        delete model.demoVisible;
        delete model.description;
        delete model.email2;
        delete model.emailVerified;
        delete model.errors;
        delete model.googleUser;
        delete model.gracePeriod;
        delete model.headerText;
        delete model.id;
        delete model.initialCountry;
        delete model.initialDistrictName;
        delete model.initialEmail;
        delete model.initialFirstName;
        delete model.initialLanguage;
        delete model.initialLastName;
        delete model.initialOrgName;
        delete model.initialOrgType;
        delete model.initialRole;
        delete model.initialSchoolId;
        delete model.initialSchoolName;
        delete model.initialState;
        delete model.initialUniversity;
        delete model.initialZipCode;
        delete model.licenseModel;
        delete model.loadingByCountry;
        delete model.loadingByZip;
        delete model.password;
        delete model.password2;
        delete model.renewalWindow;
        delete model.schoolsByCountry;
        delete model.schoolsByZip;
        delete model.schoolsFetched;
        delete model.selectedAccountType;
        delete model.showChangePassword;
        delete model.showLanguageTooltip;
        delete model.showPasswords;
        delete model.step1Status;
        delete model.step2Status;
        delete model.step3Status;
        delete model.step4Status;
        delete model.stripeError;
        delete model.subHeaderText;
        delete model.termsChecked;
        delete model.university;
        
        return {
            status: Constants.HTTP_200_OK,
            data: model
        };
    }
    
    async updateAccount(request) {
        let teacher = request.teacher;
        
        let model = new AccountModel();
        model.firstName = request.body.firstName;
        model.lastName = request.body.lastName;
        model.email = request.body.email;
        model.currentPassword = request.body.currentPassword;
        model.password = request.body.password;
        model.country = request.body.country;
        model.language = request.body.language;
        model.orgType = request.body.orgType;
        model.schoolId = request.body.schoolId;
        model.state = request.body.state;
        model.zipCode = request.body.zipCode;
        model.orgName = request.body.orgName;
        model.schoolName = request.body.schoolName;
        model.role = request.body.role;
        model.districtName = request.body.districtName;
        
        let result = {
            status: Constants.HTTP_400_BAD_REQUEST,
            data: {error: Constants.INVALID_PASSWORD}
        };
        
        if (model.isValidCurrentPassword()) {
            if (!teacher.password) { // If the teacher was found in the cache, his password will be null.
                teacher.password = await accountDao.selectPassword(teacher.id);
            }
            
            if (!teacher.password) {
                logger.error('Unknown error looking up teacher password during account update.');
                result.status = Constants.HTTP_500_INTERNAL_SERVER_ERROR;
                result.data.error = Constants.INTERNAL_SERVER_ERROR;
                return result;
            }
            
            let isCorrectPassword = await loginProvider.verifyPassword(model.currentPassword, teacher.password);
            
            if (isCorrectPassword) {
                if (model.isValidPassword()) {
                    model.password = await passwordProvider.hashPassword(model.password);
                } else {
                    return result;
                }
            } else {
                return result;
            }
        } else if (model.currentPassword !== '' && model.password !== '') {
            return result;
        }
        
        result.data.error = Constants.INVALID_ACCOUNT_DATA;
        
        if (!model.isValidFirstName() ||
            !model.isValidLastName() ||
            !model.isValidEmail() ||
            !model.isValidCountry() ||
            !model.isValidLanguage() ||
            !model.isValidOrgType() ||
            !model.isValidRole() ||
            !model.isValidDistrictName()) {
            return result;
        }
        
        if (model.orgType === Constants.ORGANIZATION_K12) {
            if (!model.isValidSchoolName()) {
                return result;
            }
            
            if (model.isCountryUS() &&
                (!model.isValidZipCode() ||
                 !model.isValidSchoolId() ||
                 !model.isValidState())) {
                return result;
            }
        } else if (model.orgType === Constants.ORGANIZATION_HIGHER_ED) {
            if (!model.isValidOrgName()) {
                return result;
            }
            
            if (model.isCountryUS() && !model.isValidState()) {
                return result;
            }
            
            model.university = model.orgName;
        } else {
            if (!model.isValidOrgName()) {
                return result;
            }
            
            model.description = model.orgName;
        }
        
        model.role = JSON.stringify(model.role);
        
        if (!model.role) {
            model.role = [];
        }
        
        model.id = teacher.id;
        
        await accountDao.updateAccount(model);
        
        await teacherCache.removeTeacher(teacher.auth_token);
        
        if (model.language !== teacher.language) {
            result.language = model.language;
            let roomName = await roomDao.updateLastUsedRoom(teacher.id, Constants.NO_ROOM_STATUS_CHANGE);
            pubnubProvider.sendMessage(`${roomName}-student`, 'student', 'room_cleared');
        }
        
        result.status = Constants.HTTP_200_OK;
        result.data = {};
        return result;
    }
    
    async sendReceiptEmail(request) {
        let teacher = request.teacher;
        
        let task = [
            JSON.stringify({
                email_address: teacher.email.toLowerCase(),
                email_type: Constants.RECEIPT,
                user_id: teacher.id
            })
        ];

        await workerCache.addWorkerTasks(`${Constants.SEND_EMAIL_TASK}${config.platform}`, task);
        
        return {
            status: Constants.HTTP_200_OK,
            data: {}
        }
    }
    
}

module.exports = new AccountProvider();
