module.exports = {
    "Final Survey": "Exit Ticket",
    "Final Survey Quiz": "Exit Ticket Quiz",
    "POLLS": "QUICK QUESTION",
    "Polls": "Quick Question",
    "Spaceship Game": "Space Race",
    "Spaceship Game complete!": "Space Race complete!",
    "Spaceship Game countdown timer": "Space Race countdown timer",
    "Spaceship Game time remaining": "Space Race time remaining",
    "Please enter a new classroom name (or URL) to continue.": "Please enter a new room name (or URL) to continue.",
    "Invalid Classroom": "Invalid Room",
    "Please check the classroom name and try again.": "Please check the room name and try again.",
    "Classroom Name": "Room Name",
    "Change Classroom": "Change Room",
    "This classroom requires a student ID. Please enter your student ID to continue.": "This room requires a student ID. Please enter your student ID to continue.",
    "classrooms": "rooms",
    "Classrooms": "Rooms",
    "CLASSROOM": "ROOM",
    "Clear Classroom": "Clear Room",
    "Your classrooms are currently unavailable. Please try again later.": "Your rooms are currently unavailable. Please try again later.",
    "Are you sure you want to change to the {0} classroom?": "Are you sure you want to change to the {0} room?",
    "The classroom could not be changed at this time. Please try again later.": "The room could not be changed at this time. Please try again later.",
    "Are you sure you want to clear all students from the {0} classroom?": "Are you sure you want to clear all students from the {0} room?",
    "Student names are required in classrooms with rosters.": "Student names are required in rooms with rosters.",
    "This setting is only available in classrooms with rosters.": "This setting is only available in rooms with rosters.",
    "Students can only take the quiz once. If they leave and rejoin the classroom, they will continue the quiz where they left off.": "Students can only take the quiz once. If they leave and rejoin the room, they will continue the quiz where they left off.",
    "Student names must be enabled for classrooms with rosters.": "Student names must be enabled for rooms with rosters.",
    "Join a teacher's classroom here: ": "Join a teacher's room here: ",
    "Multiple classrooms, rosters, and much more!": "Multiple rooms, rosters, and much more!",
    "Upgrade your free account to Socrative PRO for multiple classrooms, rosters, and much more!": "Upgrade your free account to Socrative PRO for multiple rooms, rosters, and much more!",
    "Sort by classroom": "Sort by room",
    "From Deleted Classrooms": "From Deleted Rooms",
    "From Deleted Classrooms ({0})": "From Deleted Rooms ({0})",
    "Changing this roster will log all students out of the classroom.": "Changing this roster will log all students out of the room.",
    "An activity is currently running in that classroom.": "An activity is currently running in that room.",
    "Are you sure you want to delete this classroom? (Reports and quizzes will not be deleted.)": "Are you sure you want to delete this room? (Reports and quizzes will not be deleted.)",
    "Share Classroom": "Share Room",
    "Classroom added!": "Room added!",
    "Classroom deleted!": "Room deleted!",
    "Only letters (A-Z) and numbers are allowed in a classroom name.": "Only letters (A-Z) and numbers are allowed in a room name.",
    "Please enter a classroom name.": "Please enter a room name.",
    "Spaces are not allowed in classroom names.": "Spaces are not allowed in room names.",
    "Classroom names must be 32 or fewer characters.": "Room names must be 32 or fewer characters.",
    "Classroom name changed!": "Room name changed!",
    "That classroom name is not available.": "That room name is not available.",
    "Importing a roster will clear all students from the {0} classroom.": "Importing a roster will clear all students from the {0} room.",
    "Up to 10 private or public classrooms": "Up to 10 private or public rooms",
    "Restricted classroom access via student IDs": "Restricted room access via student IDs",
    "This classroom already contains a roster.": "This room already contains a roster.",
    "SHARE CLASSROOM": "SHARE ROOM",
    "The default classroom cannot be deleted.": "The default room cannot be deleted.",
    "A classroom cannot be deleted while it has an activity running.": "A room cannot be deleted while it has an activity running.",
    "Default Classroom": "Default Room",
    "A classroom cannot be changed while it has an activity running.": "A room cannot be changed while it has an activity running.",
    "Rosters are not allowed in the default classroom.": "Rosters are not allowed in the default room.",
    "Classroom Limit": "Room Limit",
    "You have reached the maximum number of classrooms.": "You have reached the maximum number of rooms.",
    "Add Classroom": "Add Room",
    "Rename Classroom": "Rename Room",
    "Go Pro to deliver multiple activities at the same time in up to 10 classrooms, and much more!": "Go Pro to deliver multiple activities at the same time in up to 10 rooms, and much more!",
    "Share this URL with students to give them direct access to this classroom.": "Share this URL with students to give them direct access to this room.",
    "Copy classroom URL for \"{0}\"": "Copy room URL for \"{0}\""




};
