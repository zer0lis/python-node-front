'use strict';

let translations = require('./en-mappings');

class Translator {
    
    /**
     * Load a translation file on demand for the browser.
     * @param {string}   language The user's preferred language (null defaults to 'en').
     * @param {function} callback Function to execute after the appropriate translation file has been loaded.
     */
    loadClientTranslations(language, callback) {
        let Cookies = require('js-cookie');
        
        if (!language) {
            language = Cookies.get('socrative_lang');
        }
        
        if (!language) {
            language = window.navigator.language || 'en';
            language = language.toLowerCase();
        }
        
        let languages = ['da', 'de', 'en', 'en-gb', 'es', 'es-mx', 'fi', 'fr', 'fr-ca', 'ko', 'ms', 'nl', 'pt-br', 'sv', 'th', 'tr', 'zh-cn'];
        if (languages.indexOf(language) == -1) {
            language = 'en';
        }
        
        Cookies.set('socrative_lang', language, {path: '/'});
        
        if (language !== 'en') {
            if (language === 'da') {
                require.ensure(['da-translations'], (require) => {
                    translations = require('da-translations');
                    callback();
                });
            } else if (language === 'de') {
                require.ensure(['de-translations'], (require) => {
                    translations = require('de-translations');
                    callback();
                });
            } else if (language === 'es' || language === 'es-mx') {
                require.ensure(['es-translations'], (require) => {
                    translations = require('es-translations');
                    callback();
                });
            } else if (language === 'fi') {
                require.ensure(['fi-translations'], (require) => {
                    translations = require('fi-translations');
                    callback();
                });
            } else if (language === 'fr' || language === 'fr-ca') {
                require.ensure(['fr-translations'], (require) => {
                    translations = require('fr-translations');
                    callback();
                });
            } else if (language === 'ko') {
                require.ensure(['ko-translations'], (require) => {
                    translations = require('ko-translations');
                    callback();
                });
            } else if (language === 'ms') {
                require.ensure(['ms-translations'], (require) => {
                    translations = require('ms-translations');
                    callback();
                });
            } else if (language === 'nl') {
                require.ensure(['nl-translations'], (require) => {
                    translations = require('nl-translations');
                    callback();
                });
            } else if (language === 'pt-br') {
                require.ensure(['pt-translations'], (require) => {
                    translations = require('pt-translations');
                    callback();
                });
            } else if (language === 'sv') {
                require.ensure(['sv-translations'], (require) => {
                    translations = require('sv-translations');
                    callback();
                });
            } else if (language === 'th') {
                require.ensure(['th-translations'], (require) => {
                    translations = require('th-translations');
                    callback();
                });
            } else if (language === 'tr') {
                require.ensure(['tr-translations'], (require) => {
                    translations = require('tr-translations');
                    callback();
                });
            } else if (language === 'zh-cn') {
                require.ensure(['zh-CN-translations'], (require) => {
                    translations = require('zh-CN-translations');
                    callback();
                });
            } else {
                translations = require('en-mappings');
                callback();
            }
        } else {
            translations = require('en-mappings');
            callback();
        }
    }

    /**
     * Load a translation file on demand for a Socrative service.
     * @param {string} language The user's preferred language (null defaults to 'en').
     */
    loadServiceTranslations(language) {
        if (!language) {
            language = 'en';
        }
        
        if (language === 'es-mx') {
            language = 'es';
        }
        
        if (language === 'fr-ca') {
            language = 'fr';
        }
        
        if      (language === 'da')    {translations = require('./da-translations')}
        else if (language === 'de')    {translations = require('./de-translations')}
        else if (language === 'en')    {translations = require('./en-mappings')}
        else if (language === 'es')    {translations = require('./es-translations')}
        else if (language === 'fi')    {translations = require('./fi-translations')}
        else if (language === 'fr')    {translations = require('./fr-translations')}
        else if (language === 'ko')    {translations = require('./ko-translations')}
        else if (language === 'ms')    {translations = require('./ms-translations')}
        else if (language === 'nl')    {translations = require('./nl-translations')}
        else if (language === 'pt-br') {translations = require('./pt-translations')}
        else if (language === 'sv')    {translations = require('./sv-translations')}
        else if (language === 'th')    {translations = require('./th-translations')}
        else if (language === 'tr')    {translations = require('./tr-translations')}
        else if (language === 'zh-cn') {translations = require('./zh-CN-translations')}
    }
    
    translate(text) {
        return translations[text] || text;
    }
    
}

module.exports = new Translator();
