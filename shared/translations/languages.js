module.exports = [
    'da',
    'de',
    'en',
    'en-gb',
    'es',
    'es-mx',
    'fi',
    'fr',
    'fr-ca',
    'ko',
    'ms',
    'nl',
    'pt-br',
    'sv',
    'th',
    'tr',
    'zh-cn'
];
