'use strict';

let BaseCache = require('./BaseCache'),
    couponDao = require('../daos/CouponDao'),
    logger = require('../Logger');

const COUPON_NAMESPACE = 'coupon';

class CouponCache extends BaseCache {

    async addCoupon(coupon) {
        let key = `${COUPON_NAMESPACE}:${coupon.name.toLowerCase()}`;
        try {
            await this.writeClient.setAsync(key, JSON.stringify(coupon));
        } catch (error) {
            logger.error('Error adding coupon to cache:', error);
        }
    }
    
    async getCoupon(name) {
        if (name) {
            name = name.toLowerCase();
        }
        
        let key = `${COUPON_NAMESPACE}:${name}`,
            coupon = null;
        
        try {
            coupon = await this.readClient.getAsync(key);
            
            if (coupon) {
                coupon = JSON.parse(coupon);
            }
        } catch (error) {
            logger.error('Error getting coupon from cache:', error);
        }
        
        if (!coupon) {
            coupon = await couponDao.selectCouponByName(name);
            
            if (coupon) {
                await this.addCoupon(coupon);
            }
        }
        
        if (coupon) {
            coupon.expiration_date = new Date(coupon.expiration_date);
        }

        return coupon;
    }
    
    async updateCoupon(coupon) {
        await this.deleteCoupon(coupon.initialName);
        await this.addCoupon(coupon);
    }
    
    async deleteCoupon(name) {
        let key = `${COUPON_NAMESPACE}:${name.toLowerCase()}`;
        try {
            await this.writeClient.delAsync(key);
        } catch (error) {
            logger.error('Error deleting coupon from cache:', error);
        }
    }

}

module.exports = new CouponCache();
