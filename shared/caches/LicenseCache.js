'use strict';

let BaseCache = require('./BaseCache'),
    licenseDao = require('../daos/LicenseDao'),
    logger = require('../Logger');

const LICENSE_NAMESPACE = 'license';

class LicenseCache extends BaseCache {

    async addLicense(license) {
        let key = `${LICENSE_NAMESPACE}:${license.key.toUpperCase()}`;
        
        try {
            await this.writeClient.setAsync(key, JSON.stringify(license));
        } catch (error) {
            logger.error('Error adding license to cache:', error);
        }
    }
    
    async getLicense(key) {
        if (key) {
            key = key.toUpperCase();
        }
        
        let redisKey = `${LICENSE_NAMESPACE}:${key}`,
            license = null;
        
        try {
            license = await this.readClient.getAsync(redisKey);
            
            if (license) {
                license = JSON.parse(license);
                license.expiration_date = new Date(license.expiration_date);
            }
        } catch (error) {
            logger.error('Error getting license from cache:', error);
        }
        
        if (!license) {
            license = await licenseDao.loadLicenseByKey(key);
            
            if (license) {
                await this.addLicense(license);
            }
        }
        
        return license;
    }
    
    async deleteLicense(key) {
        let redisKey = `${LICENSE_NAMESPACE}:${key.toUpperCase()}`;
        
        try {
            await this.writeClient.delAsync(redisKey);
        } catch (error) {
            logger.error('Error deleting license from cache:', error);
        }
    }
    
    async updateLicense(license) {
        await this.deleteLicense(license.key);
        await this.addLicense(license);
    }
    
}

module.exports = new LicenseCache();
