'use strict';

let BaseCache = require('./BaseCache'),
    priceDao = require('../daos/PriceDao'),
    logger = require('../Logger');

const PRICES_NAMESPACE = 'prices',
      RENEWAL_PRICES_NAMESPACE = 'renewal_prices';

class PriceCache extends BaseCache {
    
    /**
     * Add one or more prices to the cache.
     * @param {array} prices The prices to add to the cache. (To add one price, wrap it in an array.)
     */
    async addPrices(prices) {
        let args = [PRICES_NAMESPACE];
        
        for (let price of prices) {
            args.push(
                price.seats,
                JSON.stringify(price)
            );
        }
        
        try {
            await this.writeClient.zaddAsync(args);
        } catch (error) {
            logger.error('Error adding prices to cache:', error);
        }
    }
    
    async getPrices() {
        let prices = [];
        
        try {
            prices = await this.readClient.zrangeAsync([PRICES_NAMESPACE, 0, -1]);
            
            for (let i = 0; i < prices.length; i++) {
                prices[i] = JSON.parse(prices[i]);
            }
        } catch (error) {
            logger.error('Error getting prices from cache:', error);
        }
        
        if (prices.length === 0) {
            prices = await priceDao.selectPrices();
            
            if (prices.length > 0) {
                await this.addPrices(prices);
            }
        }
        
        return prices;
    }

    async removePrice(priceId) {
        let prices = await this.getPrices(),
            seats = prices.filter(price => price.id === priceId)[0].seats;

        try {
            await this.writeClient.zremrangebyscoreAsync([PRICES_NAMESPACE, seats, seats]);
        } catch (error) {
            logger.error('Error removing price from cache:', error);
        }
    }
    
    async updatePrice(price) {
        await this.removePrice(price.id);
        await this.addPrices([price]);
    }
    
    /**
     * Add one or more renewal prices to the cache.
     * @param {array} renewalPrices The renewal prices to add to the cache. (To add one price, wrap it in an array.)
     */
    async addRenewalPrices(renewalPrices) {
        let args = [RENEWAL_PRICES_NAMESPACE];
        
        for (let renewalPrice of renewalPrices) {
            args.push(
                renewalPrice.id,
                JSON.stringify(renewalPrice)
            );
        }
        
        try {
            await this.writeClient.zaddAsync(args);
        } catch (error) {
            logger.error('Error adding renewal prices to cache:', error);
        }
    }
    
    async getRenewalPrices() {
        let renewalPrices = [];
        
        try {
            renewalPrices = await this.readClient.zrangeAsync([RENEWAL_PRICES_NAMESPACE, 0, -1]);
            
            for (let i = 0; i < renewalPrices.length; i++) {
                renewalPrices[i] = JSON.parse(renewalPrices[i]);
            }
        } catch (error) {
            logger.error('Error getting renewal prices from cache:', error);
        }
        
        if (renewalPrices.length === 0) {
            renewalPrices = await priceDao.selectRenewalPrices();
            
            if (renewalPrices.length > 0) {
                await this.addRenewalPrices(renewalPrices);
            }
        }
        
        return renewalPrices;
    }
    
    async removeRenewalPrice(id) {
        try {
            await this.writeClient.zremrangebyscoreAsync([RENEWAL_PRICES_NAMESPACE, id, id]);
        } catch (error) {
            logger.error('Error removing renewal price from cache:', error);
        }
    }
    
    async updateRenewalPrice(renewalPrice) {
        await this.removeRenewalPrice(renewalPrice.id);
        await this.addRenewalPrices([renewalPrice]);
    }
    
}

module.exports = new PriceCache();
