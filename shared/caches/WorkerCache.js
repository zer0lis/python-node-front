'use strict';

let BaseCache = require('./BaseCache'),
    logger = require('../Logger');

const WORKER_NAMESPACE = 'worker';

class WorkerCache extends BaseCache {
    
    async addWorkerTasks(key, tasks) {
        // TODO .... In some future day, update the key to use the "worker:" namespace, and move everything into db 0.
        try {
            await this.writeClient.selectAsync(this.WORKER_DB);
            await this.writeClient.rpushAsync(key, tasks);
            await this.writeClient.selectAsync(0);
        } catch (error) {
            logger.error('Error adding tasks to worker queue:', error);
        }
    }
    
}

module.exports = new WorkerCache();
