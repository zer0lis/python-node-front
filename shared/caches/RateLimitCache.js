'use strict';

let BaseCache = require('./BaseCache'),
    Constants = require('../Constants'),
    logger = require('../Logger');

const LIMIT_IP_NAMESPACE = 'limit:ip';
const LIMIT_IP_EMAIL_NAMESPACE = 'limit:ip:email';
const MAX_EMAILS_FROM_SAME_IP = 10000;
const FAILED_LOGIN_CACHE_TIME = 300;

class RateLimitCache extends BaseCache {
    
    async shouldBlock(ip, email) {
        try {
            let readClient = this.readClient,
                writeClient = this.writeClient,
                ipKey = `${LIMIT_IP_NAMESPACE}:${ip}`,
                ipEmailKey = `${LIMIT_IP_EMAIL_NAMESPACE}:${ip}_${email}`;
        
            if (await readClient.scardAsync(ipKey) > MAX_EMAILS_FROM_SAME_IP) {
                return Constants.IP_BLOCKED_FOR_ONE_DAY;
            }
            
            let failures = await readClient.getAsync(ipEmailKey);
            if (failures > 10) {
                await writeClient.saddAsync(ipKey, email);
                await writeClient.expireAsync(ipKey, 86400);
                
                let error = Constants.ACCOUNT_BLOCKED_FOR_FIVE_MINUTES,
                    seconds = 300;
                
                if (failures > 20 && failures <= 30) {
                    error = Constants.ACCOUNT_BLOCKED_FOR_ONE_HOUR;
                    seconds = 3600;
                } else if (failures > 30) {
                    error = Constants.ACCOUNT_BLOCKED_FOR_ONE_DAY;
                    seconds = 86400;
                }
                
                await writeClient.incrAsync(ipEmailKey);
                await writeClient.expireAsync(ipEmailKey, seconds);
                return error;
            }
        } catch (error) {
            logger.error('Error with ip/email blocking in cache:', error);
            return false;
        }
    }
    
    async incrementLoginFailures(ip, email) {
        let ipEmailKey = `${LIMIT_IP_EMAIL_NAMESPACE}:${ip}_${email}`;
        try {
            await this.writeClient.incrAsync(ipEmailKey);
            await this.writeClient.expireAsync(ipEmailKey, FAILED_LOGIN_CACHE_TIME);
        } catch (error) {
            logger.error('Error incrementing login failures in cache:', error);
        }
    }
    
}

module.exports = new RateLimitCache();
