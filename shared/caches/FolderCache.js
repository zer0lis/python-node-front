'use strict';

let BaseCache = require('./BaseCache'),
    Constants = require('../Constants'),
    logger = require('../Logger'),
    folderDao = require('../daos/FolderDao');

class FolderCache extends BaseCache {
    
    /**
     * Add one or more folders to the cache.
     * @param {number} userId The id of the user who owns this folder.
     * @param {number} type The folder type (one of the Constants.<TYPE>_FOLDER values).
     * @param {array}  folders The folders to add to the cache. (To add one folder, wrap it in an array.)
     */
    async addFolders(userId, type, folders) {
        let args = [`${Constants.FOLDERS_NAMESPACE}:${userId}:${type}`];
        
        for (let folder of folders) {
            args.push(
                folder.id,
                JSON.stringify(folder)
            );
        }
        
        try {
            await this.writeClient.zaddAsync(args);
        } catch (error) {
            logger.error('Error adding folders to cache:', error);
        }
    }
    
    async getFolders(userId, type) {
        let folders = [];
        
        try {
            folders = await this.readClient.zrangeAsync([`${Constants.FOLDERS_NAMESPACE}:${userId}:${type}`, 0, -1]);
            for (let i = 0; i < folders.length; i++) {
                folders[i] = JSON.parse(folders[i]);
            }
        } catch(error) {
            logger.error('Error getting folders from cache:', error);
        }
        
        if (folders.length === 0) {
            folders = await folderDao.selectFolders(userId, type);
            if (folders && folders.length > 0) {
                await this.addFolders(userId, type, folders);
            }
        }
        
        return folders;
    }

    async removeFolder(userId, type, id) {
        try {
            await this.writeClient.zremrangebyscoreAsync(`${Constants.FOLDERS_NAMESPACE}:${userId}:${type}`, id, id);
        } catch (error) {
            logger.error('Error removing folders from cache:', error);
        }
    }  

    async purgeFolders(userId, type) {
        try {
            await this.writeClient.delAsync(`${Constants.FOLDERS_NAMESPACE}:${userId}:${type}`);
        } catch (error) {
            logger.error('Error removing folders from cache:', error);
        }
    } 
}

module.exports = new FolderCache();
