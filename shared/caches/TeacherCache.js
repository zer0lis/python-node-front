'use strict';

let BaseCache = require('./BaseCache'),
    teacherDao = require('../daos/TeacherDao'),
    Constants = require('../Constants'),
    logger = require('../Logger');

class TeacherCache extends BaseCache {
    
    // NOTE: This implemention of addTeacher() is incomplete. The remaining issue
    //       to be resolved is the case discrepancy (snake vs. camel) between the
    //       Python and JavaScript property names. This is thornier than it might
    //       seem, so for now let us be content to allow the activity service to
    //       be the only one that adds users to the cache.
    async addTeacher(teacher) {
        let key = `${Constants.USER_NAMESPACE}:${teacher.auth_token}`;
        
        teacher.password = null;
        
        if (teacher.proExpiration instanceof Date) {
            teacher.proExpiration = teacher.proExpiration.getTime() / 1000;
        }
        
        try {
            await this.writeClient.selectAsync(this.USER_DB);
            await this.writeClient.setexAsync(key, Constants.TEACHER_CACHE_TIMEOUT, JSON.stringify(teacher));
            await this.writeClient.selectAsync(0);
        } catch (error) {
            logger.error('Error adding teacher to cache:', error);
        }
    }
    
    async removeTeacher(authToken) {
        let key = `${Constants.USER_NAMESPACE}:${authToken}`;
        
        try {
            await this.writeClient.selectAsync(this.USER_DB);
            await this.writeClient.delAsync(key);
            await this.writeClient.selectAsync(0);
        } catch (error) {
            logger.error('Error removing teacher from cache:', error);
        }
    }
    
    async removeTeacherMulti(authTokens) {
        let keys = [];
        
        for (let authToken of authTokens) {
            keys.push(`${Constants.USER_NAMESPACE}:${authToken}`);
        }
        
        try {
            await this.writeClient.selectAsync(this.USER_DB);
            await this.writeClient.delAsync(keys);
            await this.writeClient.selectAsync(0);
        } catch (error) {
            logger.error('Error removing teachers from cache:', error);
        }
    }

    async getTeacher(authToken) {
        let key = `${Constants.USER_NAMESPACE}:${authToken}`;
        
        try {
            await this.readClient.selectAsync(this.USER_DB);
            let teacher = await this.readClient.getAsync(key);
            await this.readClient.selectAsync(0);
            
            if (!teacher) {
                teacher = await teacherDao.loadTeacherByAuthToken(authToken);
                
                if (!teacher) {
                    return null;
                }
                
                // await this.addTeacher(teacher); // Uncomment when the back end is migrated to Node.js.
                
                return teacher;
            }
            
            teacher = JSON.parse(teacher);
            
            // The Python-based activity service caches timestamps in seconds;
            // however, the JavaScript Date constructor needs milliseconds.
            if (teacher.proExpiration) {
                teacher.proExpiration = new Date(teacher.proExpiration * 1000);
            }
            
            return teacher;
        } catch (error) {
            logger.error('Error getting teacher from cache:', error);
        }
    }

}

module.exports = new TeacherCache();
