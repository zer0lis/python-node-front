'use strict';

let redis = require('redis'),
    bluebird = require('bluebird'),
    logger = require('../Logger'),
    config = require('config').get('config'),
    hosts = config.cache.hosts;

bluebird.promisifyAll(redis.RedisClient.prototype);

let read = createClient(hosts.read);
let write = hosts.write === hosts.read ? read : createClient(hosts.write);

function createClient(host) {
    let client = redis.createClient({
        host: host,
        port: config.cache.port,
        retry_strategy: handleRetry
    });
    client.on('error', handleError);
    return client;
}

function handleRetry(settings) {
    if (settings.error && settings.error.code === 'ECONNREFUSED') {
        logger.error('Connection to redis refused:', settings.error);
    } else if (settings.total_retry_time > 1000 * 60 * 60) {
        logger.error('Failed to reconnect to redis after attempting for an hour.');
    } else if (settings.times_connected > 10) {
        logger.error('Failed to reconnect to redis after 10 attempts.');
    } else {
        return settings.attempt * 100;
    }
}

function handleError(error) {
    logger.error('Error encountered by redis client:', error);
    if (error && error.indexOf('READONLY') != -1 || error && error.message && error.message.indexOf('READONLY') != -1) {
        if (read.server_info.role && read.server_info.role.toLowerCase() === 'master') {
            logger.info('The replica has become the master.');
            let temp = write;
            write = read;
            read = temp;
        }
    }
}

class BaseCache {
    
    // Database indexes used in the old caching model. All future caching should use namespaced keys in database 0.
    get WORKER_DB() {
        return 1;
    }
    
    get USER_DB() {
        return 1;
    }
    
    get EMAIL_RATE_LIMIT_DB() {
        return 2;
    }

    get IP_RATE_LIMIT_DB() {
        return 3;
    }
    
    get FULL_ROOM_DB() {
        return 4;
    }
    
    get readClient() {
        return read;
    }
    
    get writeClient() {
        return write;
    }
    
}

module.exports = BaseCache;
