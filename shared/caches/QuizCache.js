'use strict';

let BaseCache = require('./BaseCache'),
    Constants = require('../Constants'),
    logger = require('../Logger');

class QuizCache extends BaseCache {
    
    /**
     * Delete one or more quizzes from the cache.
     * @param {array} quizIds The ids of the quizzes to be deleted. (To delete one quiz, wrap it in an array.)
     */
    async deleteQuizzes(quizIds) {
        /*
            TODO .... In a future day, quizzes should be cached in database 0, and using namespaces:
            
            quiz:teacher:<quizId>
            quiz:student:<quizId>
         */
        
        let keys = [];
        
        for (let id of quizIds) {
            keys.push(
                `quiz_${id}_t`,
                `quiz_${id}_s`
            );
        }
        
        try {
            await this.writeClient.selectAsync(this.USER_DB);
            await this.writeClient.delAsync(keys);
            await this.writeClient.selectAsync(0);
        } catch (error) {
            logger.error('Error deleting quizzes from cache:', error);
        }
    }
    
}

module.exports = new QuizCache();
