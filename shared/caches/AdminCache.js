'use strict';

let BaseCache = require('./BaseCache'),
    adminDao = require('../daos/AdminDao'),
    logger = require('../Logger');

const ADMIN_NAMESPACE = 'admin';

class AdminCache extends BaseCache {

    async addAdmin(admin) {
        let key = `${ADMIN_NAMESPACE}:${admin.email.toLowerCase()}`;
        try {
            await this.writeClient.setAsync(key, JSON.stringify(admin));
        } catch (error) {
            logger.error('Error adding admin to cache:', error);
        }
    }
    
    async getAdminByEmail(email) {
        if (email) {
            email = email.toLowerCase();
        }
        let key = `${ADMIN_NAMESPACE}:${email}`;
        let admin = null;
        try {
            admin = await this.readClient.getAsync(key);
            if (admin) {
                admin = JSON.parse(admin);
            }
        } catch (error) {
            logger.error('Error getting admin from cache:', error);
        }
        if (!admin) {
            admin = await adminDao.loadAdminByEmail(email);
            if (admin) {
                await this.addAdmin(admin);
            }
        }
        if (admin) {
            if (admin.last_login) {
                admin.last_login = new Date(admin.last_login);
            }
            if (admin.register_date) {
                admin.register_date = new Date(admin.register_date);
            }
        }
        return admin;
    }

    async removeAdmin(email) {
        let key = `${ADMIN_NAMESPACE}:${email.toLowerCase()}`;
        try {
            await this.writeClient.delAsync(key);
        } catch (error) {
            logger.error('Error removing admin from cache:', error);
        }
    }
    
}

module.exports = new AdminCache();
