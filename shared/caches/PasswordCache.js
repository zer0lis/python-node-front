'use strict';

let BaseCache = require('./BaseCache'),
    logger = require('../Logger');

const PASSWORD_TOKEN_NAMESPACE = 'password_token';

class PasswordCache extends BaseCache {
    
    async addToken(token, teacherId) {
        // Password tokens must be set in database 1 because the email worker uses
        // database 1, and it creates a password token for each upgrade to PRO (in
        // case the user's account was generated for him and he needs to create a
        // password later).
        
        let key = `${PASSWORD_TOKEN_NAMESPACE}:${token}`;
        
        try {
            await this.writeClient.selectAsync(this.WORKER_DB);
            await this.writeClient.setAsync(key, teacherId, 'EX', 60 * 60 * 2); // Set the token to expire in 2 hours.
            await this.writeClient.selectAsync(0);
            return true;
        } catch (error) {
            logger.error('Error adding password token to cache:', error);
        }
    }
    
    async getTeacherId(token) {
        let key = `${PASSWORD_TOKEN_NAMESPACE}:${token}`,
            teacherId = null;
        
        try {
            await this.readClient.selectAsync(this.WORKER_DB);
            teacherId = await this.readClient.getAsync(key);
            await this.readClient.selectAsync(0);
            
            await this.writeClient.selectAsync(this.WORKER_DB);
            await this.writeClient.delAsync(key);
            await this.writeClient.selectAsync(0);
        } catch (error) {
            logger.error(`Error getting or deleting password token (${token}) from cache:`, error);
        }
        
        return teacherId;
    }
    
}

module.exports = new PasswordCache();
