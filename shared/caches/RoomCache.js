'use strict';

let BaseCache = require('./BaseCache'),
    logger = require('../Logger');

class RoomCache extends BaseCache {
    
    async deleteRoom(room) {
        let roomKey = `rot_${room}`;
        
        try {
            await this.writeClient.selectAsync(this.USER_DB);
            await this.writeClient.delAsync(roomKey);
            await this.writeClient.selectAsync(0);
        } catch (error) {
            logger.error('Error removing room from cache:', error);
        }
    }
    
    async deleteRoomList(userId) {
        let roomList = `rl_${userId}`;
        
        try {
            await this.writeClient.selectAsync(this.USER_DB);
            await this.writeClient.delAsync(roomList);
            await this.writeClient.selectAsync(0);
        } catch (error) {
            logger.error('Error removing room list from cache:', error);
        }
    }
    
}

module.exports = new RoomCache();
