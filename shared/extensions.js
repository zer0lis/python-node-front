/*
    Extensions to built-in JavaScript objects.
 */

// http://stackoverflow.com/questions/610406/javascript-equivalent-to-printf-string-format/4673436#4673436
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) { 
            return typeof args[number] != 'undefined' ? args[number] : match;
        });
    };
}

// http://stackoverflow.com/questions/11409895/whats-the-most-elegant-way-to-cap-a-number-to-a-segment
if (!Number.prototype.clamp) {
    Number.prototype.clamp = function(min, max) {
        return Math.min(Math.max(this, min), max);
    };
}
