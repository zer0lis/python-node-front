let uaParser = require('ua-parser-js'),
    Constants = require('./Constants');

class Platform {
    
    constructor() {
        let parser = new uaParser(),
            device = parser.getDevice();
        
        // OS
        this.OS = parser.getOS().name;
        
        // Browser
        this.browser = parser.getBrowser().name;
        this.browserVersion = parser.getBrowser().version;
        this.isMobileBrowser = device.type === 'mobile' || device.type === 'tablet';
        
        // Tablet
        this.isTablet = device.type === 'tablet';
        
        // iOS (could be browser or app)
        this.isIos = this.isIosApp = /iOS APP/.test(parser.getUA());
        if (device.model === 'iPhone' || device.model === 'iPad' || device.model === 'iPod') {
            this.isIos = true;
        }
        
        this.isAndroidApp = /Android APP/.test(parser.getUA());
    }
    
    isMobile() {
        return this.isMobileBrowser ||
               this.isAndroidApp ||
               this.isIosApp;
    }
    
    isPhone() {
        return this.isMobile() && !this.isTablet;
    }
    
    isSmallPhone() {
        return this.isPhone() && window.innerWidth < 375;
    }
    
    isIE() {
        return this.browser === 'IE';
    }
    
    isEdge() {
        return this.browser === 'Edge';
    }
    
    isFirefox() {
        return this.browser === 'Firefox';
    }
    
    isChrome() {
        return this.browser === 'Chrome';
    }
    
    isProduction() {
        return window.location.hostname.toLowerCase() === Constants.PRODUCTION_HOSTNAME;
    }
    
    isTabletPortrait() {
        return this.isTablet || screen.width === 768;
    }
    
}

module.exports = new Platform();
