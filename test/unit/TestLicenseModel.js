'use strict';

let LicenseModel = require('../../shared/models/LicenseModel'),
    model = new LicenseModel(),
    sinon = require('sinon'),
    expect = require('chai').expect;

describe('LicenseModel', () => {
    
    describe('calculatePriceData', () => {
        
        before(() => {
            model.prices = [
                {amount: 5999, seats: 1},
                {amount: 5999, seats: 2},
                {amount: 5699, seats: 10},
                {amount: 5399, seats: 50},
                {amount: 5249, seats: 100},
                {amount: 5049, seats: 150},
                {amount: 4949, seats: 200},
                {amount: 4799, seats: 250},
                {amount: 4300, seats: 300}
            ];
        });
        
        describe('when the defaults are unchanged', () => {
            
            before(() => {
                model.seats = 1;
                model.years = 1;
            });
            
            it('should calculate the price based on 1 seat, 1 year, and no coupon', () => {
                model.calculatePriceData();
                expect(model.pricePerSeat).to.equal(5999);
                expect(model.expectedTotalPrice).to.equal(5999);
            });
            
        });
        
        describe('when there is more than one seat but no coupon', () => {
            
            before(() => {
                model.seats = 10;
                model.years = 1;
            });
            
            it('should calculate the bulk price', () => {
                model.calculatePriceData();
                expect(model.pricePerSeat).to.equal(5699);
                expect(model.expectedTotalPrice).to.equal(5699 * 10);
            });
            
        });
        
        describe('when there is more than one seat and year but no coupon', () => {
            
            before(() => {
                model.seats = 50;
                model.years = 2;
            });
            
            it('should calculate the bulk price', () => {
                model.calculatePriceData();
                expect(model.pricePerSeat).to.equal(5399);
                expect(model.expectedTotalPrice).to.equal(5399 * 50 * 2);
            });
            
        });
        
        describe('when the defaults are unchanged but there is a coupon', () => {
            
            before(() => {
                model.seats = 1;
                model.years = 1;
                model.coupon = {amount: 2000}
            });
            
            it('should calculate the price with the coupon discount', () => {
                model.calculatePriceData();
                expect(model.pricePerSeat).to.equal(3999);
                expect(model.expectedTotalPrice).to.equal(3999);
            });
            
        });
        
        describe('when it is a bulk purchase and the coupon allows bulk', () => {
            
            before(() => {
                model.seats = 100;
                model.years = 3;
                model.coupon = {amount: 2000, allow_bulk: true}
            });
            
            it('should calculate the price with the coupon and bulk discounts', () => {
                model.calculatePriceData();
                expect(model.pricePerSeat).to.equal(3249);
                expect(model.expectedTotalPrice).to.equal(3249 * 100 * 3);
            });
            
        });
        
    });
    
});
