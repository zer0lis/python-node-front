'use strict';

let router = require('express').Router(),
    loginProvider = require('../../../shared/providers/LoginProvider'),
    provider = require('../../../shared/providers/LicenseProvider');

/**
 * Charge a user for Socrative PRO and create a license key.
 * @param {boolean} autoRenew
 * @param {number}  buyerId
 * @param {string}  couponName
 * @param {number}  expectedTotalPrice
 * @param {number}  seats
 * @param {object}  tokenId
 * @param {number}  years
 */
router.post('/', loginProvider.verifyTeacher, async (request, response) => {
    let result = await provider.createLicense(request);
    response.status(result.status);
    response.send(result.data);
});

router.put('/auto-renew', loginProvider.verifyTeacher, async (request, response) => {
    let result = await provider.changeAutoRenew(request);
    response.status(result.status);
    response.send(result.data);
});

module.exports = router;
