'use strict';

let router = require('express').Router(),
    provider = require('../../../shared/providers/FolderProvider'),
    loginProvider = require('../../../shared/providers/LoginProvider');

router.post('/', loginProvider.verifyTeacher, async (request, response) => {
    let result = await provider.createFolder(request);
    response.status(result.status);
    response.send(result.data);
});

router.put('/rename/:id', loginProvider.verifyTeacher, async (request, response) => {
    let result = await provider.renameFolder(request);
    response.status(result.status);
    response.send(result.data);
});

module.exports = router;
