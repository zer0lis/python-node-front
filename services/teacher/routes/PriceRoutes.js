'use strict';

let router = require('express').Router(),
    provider = require('../../../shared/providers/PriceProvider');

/**
 * Return an array of price objects.
 */
router.get('/', async (request, response) => {
    let result = await provider.getPrices();
    response.status(result.status);
    response.send(result.data);
});

module.exports = router;
