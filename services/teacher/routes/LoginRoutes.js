'use strict';

let router = require('express').Router(),
    provider = require('../../../shared/providers/LoginProvider'),
    Constants = require('../../../shared/Constants'),
    config = require('config').get('config');

let setCookie = (response, name, value, maxAge) => {
    let options = {
        domain: config.cookie.domain,
        httpOnly: true,
        maxAge: maxAge,
        path: '/',
        secure: config.platform !== 'LOCAL'
    };
    response.cookie(name, value, options);
};

/**
 * Log a teacher in via email/password.
 * @param {string} email
 * @param {string} password
 */
// router.post('/email', async (request, response) => {
//     let result = await provider.logInEmailTeacher(request);
//     if (result.data.language) {
//         response.cookie(Constants.LANGUAGE_COOKIE_NAME, result.data.language, {maxAge: 1000 * 60 * 60 * 24 * 365 * 8}); // 8 years
//     }
//     if (result.data.token) {
//         setCookie(response, Constants.AUTH_COOKIE_NAME, result.data.token, 1000 * 60 * 60 * 24 * 30); // 30 days
//     }
//     response.status(result.status);
//     response.send(result.data);
// });

/**
 * Redirect URI after Google login.
 */
router.get('/google', async (request, response) => {
    let result = await provider.logInGoogleTeacher(request, response);

    if (result.token) {
        setCookie(response, Constants.AUTH_COOKIE_NAME, result.token, 1000 * 60 * 60 * 24 * 30); // 30 days
    }
    
    response.redirect(result.url);
});

module.exports = router;
