'use strict';

let router = require('express').Router(),
    config = require('config').get('config'),
    provider = require('../../../shared/providers/RoomProvider'),
    loginProvider = require('../../../shared/providers/LoginProvider'),
    Constants = require('../../../shared/Constants');

router.post('/rooms/api/clear', loginProvider.verifyTeacher, async (request, response) => {
    let result = await provider.clearRoom(request);
    response.status(result.status);
    response.send(result.data);
});

module.exports = router;
