'use strict';

let router = require('express').Router(),
    multer = require('multer'),
    Constants = require('../../../shared/Constants'),
    provider = require('../../../shared/providers/QuizProvider'),
    loginProvider = require('../../../shared/providers/LoginProvider');

router.get('/', loginProvider.verifyTeacher, async (request, response) => {
    let result = await provider.getQuizzes(request);
    response.status(result.status);
    response.send(result.data);
});

router.post('/', loginProvider.verifyTeacher, async (request, response) => {
    let result = await provider.createQuiz(request);
    response.status(result.status);
    response.send(result.data);
});

let storage = multer.diskStorage({
    destination: '/tmp/excel-quiz-uploads'
});

let excelUpload = multer({
    limits: {
        fileSize: 1048576 // Limit size of Excel file to 1 MB.
    },
    storage: storage
}).single('file');

let isValidImportRequest = (request) => {
    return request.teacher.level === Constants.PRO_USER || request.body.parentId === null;
};

router.post('/import', loginProvider.verifyTeacher, async (request, response) => {
    let result = {
        status: Constants.HTTP_400_BAD_REQUEST,
        data: {error: Constants.FEATURE_NOT_AVAILABLE}
    };
    
    let importType = parseInt(request.query.type);
    
    if (importType === Constants.SOC_TYPE) {
        if (!isValidImportRequest(request)) {
            response.status(result.status);
            response.send(result.data);
            return;
        }
        
        result = await provider.importQuizBySoc(request);
        response.status(result.status);
        response.send(result.data);
    } else if (importType === Constants.EXCEL_TYPE) {
        excelUpload(request, response, (error) => {
            if (error) {
                result.data.errors = {};
                result.data.errors[Constants.FILE_TOO_LARGE] = true;
                response.status(result.status);
                response.send(result.data);
            } else {
                // The request is multipart/form-data, so we must convert parentId from a string back to its original value:
                request.body.parentId = request.body.parentId === 'null' ? null : parseInt(request.body.parentId);
                
                if (!isValidImportRequest(request)) {
                    result.data.errors = {};
                    result.data.errors[Constants.FEATURE_NOT_AVAILABLE] = true;
                    response.status(result.status);
                    response.send(result.data);
                    return;
                }
                
                provider.importQuizByExcel(request, (result) => {
                    response.status(result.status);
                    response.send(result.data);
                });
            }
        });
    }
});

router.post('/merge', loginProvider.verifyTeacher, async (request, response) => {
    let result = {
        status: Constants.HTTP_400_BAD_REQUEST,
        data: {error: Constants.FEATURE_NOT_AVAILABLE}
    };

    if (request.teacher.level === Constants.PRO_USER) {
        result = await provider.mergeQuizzes(request);
    }

    response.status(result.status);
    response.send(result.data);
});

router.post('/copy', loginProvider.verifyTeacher, async (request, response) => {
    let result = await provider.copyQuiz(request);
    response.status(result.status);
    response.send(result.data);
});

router.put('/move', loginProvider.verifyTeacher, async (request, response) => {
    let result = await provider.moveQuizzes(request);
    response.status(result.status);
    response.send(result.data);
});

router.put('/purge', loginProvider.verifyTeacher, async (request, response) => {
    let result = await provider.purgeQuizzes(request);
    response.status(result.status);
    response.send(result.data);
});

module.exports = router;
