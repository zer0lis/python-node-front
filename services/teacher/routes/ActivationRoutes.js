'use strict';

let router = require('express').Router(),
    provider = require('../../../shared/providers/ActivationProvider'),
    loginProvider = require('../../../shared/providers/LoginProvider'),
    Constants = require('../../../shared/Constants');

/**
 * Activate one seat of a license for an individual user.
 * @param {number} userId
 * @param {string} key
 */
router.post('/user', loginProvider.verifyTeacher, async (request, response) => {
    let result = await provider.activateLicenseForUser(request);
    response.status(result.status);
    response.send(result.data);
});

module.exports = router;
