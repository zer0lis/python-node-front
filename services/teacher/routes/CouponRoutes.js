'use strict';

let router = require('express').Router(),
    provider = require('../../../shared/providers/CouponProvider');

/**
 * Find a coupon by name and return it.
 */
router.get('/name/:name', async (request, response) => {
    let result = await provider.getCouponByName(request.params.name);
    response.status(result.status);
    response.send(result.data);
});

module.exports = router;
