'use strict';

let router = require('express').Router(),
    config = require('config').get('config'),
    provider = require('../../../shared/providers/AccountProvider'),
    loginProvider = require('../../../shared/providers/LoginProvider'),
    Constants = require('../../../shared/Constants');

router.get('/check/:email', async (request, response) => {
    let result = await provider.checkAvailable(request.params.email);
    response.status(result.status);
    response.send(result.data);
});

router.post('/', async (request, response) => {
    let result = await provider.createAccount(request);
    
    if (result.language) {
        response.cookie(Constants.LANGUAGE_COOKIE_NAME, result.language, {maxAge: 1000 * 60 * 60 * 24 * 365 * 8}); // 8 years
    }
    
    if (result.token) {
        let options = {
            domain: config.cookie.domain,
            httpOnly: true,
            maxAge: 1000 * 60 * 60 * 24 * 30, // 30 days
            path: '/',
            secure: config.platform !== 'LOCAL'
        };
        
        response.cookie(Constants.AUTH_COOKIE_NAME, result.token, options);
    }
    
    response.status(result.status);
    response.send(result.data);
});

router.get('/', loginProvider.verifyTeacher, async (request, response) => {
    let result = await provider.getAccount(request);
    response.status(result.status);
    response.send(result.data);
});

router.put('/', loginProvider.verifyTeacher, async (request, response) => {
    let result = await provider.updateAccount(request);
    
    if (result.language) {
        response.cookie(Constants.LANGUAGE_COOKIE_NAME, result.language, {maxAge: 1000 * 60 * 60 * 24 * 365 * 8}); // 8 years
    }
    
    response.status(result.status);
    response.send(result.data);
});

router.get('/receipt/email', loginProvider.verifyTeacher, async (request, response) => {
    let result = await provider.sendReceiptEmail(request);
    response.status(result.status);
    response.send(result.data);
});

module.exports = router;
