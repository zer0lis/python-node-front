'use strict';

let router = require('express').Router(),
    provider = require('../../../shared/providers/PasswordProvider');

/**
 * Begin the teacher password reset process (email a token link).
 * @param {string} email
 */
router.post('/forgot', async (request, response) => {
    let result = await provider.createPasswordToken(request);
    response.status(result.status);
    response.send(result.data);
});

/**
 * Reset a teacher password.
 * @param {string} token
 * @param {string} password
 */
router.post('/reset', async (request, response) => {
    let result = await provider.changePassword(request);
    response.status(result.status);
    response.send(result.data);
});

module.exports = router;
