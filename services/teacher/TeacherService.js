'use strict';

let express = require('express'),
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
    config = require('config').get('config');

let service = express();
service.disable('x-powered-by');
service.set('trust proxy', config.trustProxy);
service.use(bodyParser.json());
service.use(bodyParser.urlencoded({extended: false}));
service.use(cookieParser());
service.use((request, response, next) => {
    response.header('Access-Control-Allow-Origin', config.cors.allowedHost);
    response.header('Access-Control-Allow-Headers', config.cors.allowedHeaders);
    response.header('Access-Control-Allow-Methods', config.cors.allowedMethods);
    response.header('Access-Control-Allow-Credentials', true);
    next();
});

/*--------------------*/
/*    Login Routes    */
/*--------------------*/
service.use('/v3/login', require('./routes/LoginRoutes'));

/*----------------------*/
/*    Account Routes    */
/*----------------------*/
service.use('/v3/accounts', require('./routes/AccountRoutes'));

/*---------------------*/
/*    Folder Routes    */
/*---------------------*/
service.use('/v3/folders', require('./routes/FolderRoutes'));

/*----------------------*/
/*    License Routes    */
/*----------------------*/
service.use('/v3/activations', require('./routes/ActivationRoutes'));
service.use('/v3/coupons', require('./routes/CouponRoutes'));
service.use('/v3/licenses', require('./routes/LicenseRoutes'));
service.use('/v3/prices', require('./routes/PriceRoutes'));

/*-----------------------*/
/*    Password Routes    */
/*-----------------------*/
service.use('/v3/passwords', require('./routes/PasswordRoutes'));

/*-------------------*/
/*    Quiz Routes    */
/*-------------------*/
service.use('/v3/quizzes', require('./routes/QuizRoutes'));

// Default error handlers
service.use((request, response, next) => {
    let error = new Error(`Not Found: ${request.method} ${request.originalUrl}`);
    error.status = 404;
    next(error);
});

service.use((error, request, response) => {
    response.status(error.status || 500);
    response.send({
        message: error.message,
        error: process.env.NODE_ENV === 'production' ? {} : error
    });
});

module.exports = service;
