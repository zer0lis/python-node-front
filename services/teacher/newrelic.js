'use strict';

/**
 * New Relic agent configuration.
 *
 * See node_modules/newrelic/lib/config.default.js in the agent distribution for a
 * more complete description of configuration variables and their potential values.
 */
exports.config = {
    app_name: [`socrative teacher ${process.env.NODE_ENV}`],
    license_key: '997554b0a8b96ef173634db69092b299725c74b3',
    logging: {
        level: 'info'
    }
};
