'use strict';

let gulp = require('gulp'),
    nodemon = require('gulp-nodemon');

gulp.task('default', () => {
    nodemon({
        script: 'start',
        ext: 'js',
        execMap: {
            js: 'node --harmony'
        },
        env: {
            PORT: 3005
        },
        ignore: ['./node_modules/**']
    }).on('restart', () => {
        console.log('Restarting');
    });
});
