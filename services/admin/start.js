'use strict';

process.env.NODE_CONFIG_DIR = './shared/config';

let service = require('./AdminService'),
    logger = require('../../shared/Logger'),
    http = require('http'),
    port = process.env.PORT || 3005;

service.set('port', port);

let server = http.createServer(service);
server.listen(port);
server.on('listening', onListening);
server.on('error', onError);

function onListening() {
    logger.debug(`Listening on ${server.address().port}`);
}

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }
    
    switch (error.code) {
        case 'EACCES':
            logger.error(`Port ${port} requires elevated privileges`);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            logger.error(`Port ${port} is already in use`);
            process.exit(1);
            break;
        default:
            throw error;
    }
}
