'use strict';

let express = require('express'),
    passport = require('passport'),
    session = require('cookie-session'),
    bodyParser = require('body-parser'),
    path = require('path'),
    handlebars = require('express-handlebars'),
    GoogleStrategy = require('passport-google-oauth').OAuth2Strategy,
    cookieParser = require('cookie-parser'),
    config = require('config').get('config'),
    adminCache = require('../../shared/caches/AdminCache'),
    adminDao = require('../../shared/daos/AdminDao');

require('../../shared/extensions');

let service = express();
service.disable('x-powered-by');
service.set('trust proxy', config.trustProxy);
service.use(bodyParser.json());
service.use(bodyParser.urlencoded({extended: false}));
service.use(cookieParser());
service.use(session({
    secret: 'bob is your uncle',
    maxAge: 30 * 24 * 60 * 60 * 1000 // 30 days
}));

let homeDir = path.join(__dirname, '../../clients/admin/dist'),
    partialsDir = path.join(__dirname, '../../clients/admin/dist/partials');

service.use(express.static(homeDir));
service.set('views', homeDir);
service.engine('.hbs', handlebars({
    extname: '.hbs',
    partialsDir: partialsDir
}));
service.set('view engine', '.hbs');
service.use(passport.initialize());
service.use(passport.session());

passport.serializeUser((admin, done) => {
    done(null, admin.email);
});

passport.deserializeUser(async (email, done) => {
    let admin = await adminCache.getAdminByEmail(email);
    let error = admin ? null : 'Error getting admin for deserialization';
    done(error, admin);
});

let googleConfig = {
    clientID: config.adminService.auth.client_id,
    clientSecret: config.adminService.auth.client_secret,
    callbackURL: config.adminService.auth.callback_url
};

passport.use(new GoogleStrategy(googleConfig, async (accessToken, refreshToken, profile, done) => {
    let admin = await adminCache.getAdminByEmail(profile.emails[0].value);
    let error = admin ? null : 'Error getting admin for strategy';
    if (error) {
        return done(null, false);
    }
    await adminDao.updateLastLogin(admin.id);
    return done(null, admin);
}));

service.use('/', require('./routes/AdminRoutes'));

// Default error handlers
service.use((request, response, next) => {
    let error = new Error(`Not Found: ${request.method} ${request.originalUrl}`);
    error.status = 404;
    next(error);
});

service.use((error, request, response, next) => {
    response.status(error.status || 500);
    response.send({
        message: error.message,
        error: process.env.NODE_ENV === 'production' ? {} : error
    });
});

module.exports = service;
