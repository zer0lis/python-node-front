'use strict';

let router = require('express').Router(),
    passport = require('passport'),
    provider = require('../../../shared/providers/AdminProvider'),
    adminCache = require('../../../shared/caches/AdminCache'),
    passwordProvider = require('../../../shared/providers/PasswordProvider'),
    priceProvider = require('../../../shared/providers/PriceProvider'),
    couponProvider = require('../../../shared/providers/CouponProvider'),
    renewalProvider = require('../../../shared/providers/RenewalProvider'),
    Constants = require('../../../shared/Constants'),
    multer = require('multer');

function checkAuth(request, response, next) {
    if (request.isAuthenticated()) {
        return next();
    } else {
        response.redirect('/login');
    }
}

function checkAdmin(request, response, next) {
    if (request.user && request.user.level >= Constants.ADMIN_USER) {
        return checkAuth(request, response, next);
    } else {
        response.redirect('/dashboard');
    }
}

function checkSupreme(request, response, next) {
    if (request.user && request.user.level === Constants.SUPREME_USER) {
        return checkAuth(request, response, next);
    } else {
        response.redirect('/dashboard');
    }
}

/*-------------------*/
/*    View Routes    */
/*-------------------*/

router.get('/login', (request, response) => {
    response.render('login');
});

router.get('/auth/google', passport.authenticate('google', {scope: ['profile', 'email']}));

router.get('/googlelogincallback', passport.authenticate('google', {}), (request, response) => {
    response.redirect('/dashboard');
});

router.get('/logout', async (request, response) => {
    await adminCache.removeAdmin(request.user.email);
    request.logout();
    response.redirect('/login');
});

router.get('/', checkAuth, (request, response) => {
    response.redirect('/dashboard');
});

router.get('/dashboard', checkAuth, (request, response) => {
    response.render('index', provider.generateMenu('default', request));
});

router.get('/queries', checkAuth, (request, response) => {
    response.render('queries', provider.generateMenu('queries', request));
});

router.get('/accounting', checkAuth, (request, response) => {
    response.render('accounting', provider.generateMenu('accounting', request));
});

router.get('/delete-account', checkAuth, (request, response) => {
    response.render('delete-account', provider.generateMenu('delete-account', request));
});

router.get('/search-by-room', checkAuth, (request, response) => {
    response.render('search-by-room', provider.generateMenu('search-by-room', request));
});

router.get('/login-to-account', checkAuth, (request, response) => {
    response.render('login-to-account', provider.generateMenu('login-to-account', request));
});

router.get('/merge-accounts', checkAuth, (request, response) => {
    response.render('merge-accounts', provider.generateMenu('merge-accounts', request));
});

router.get('/reset-password', checkAuth, (request, response) => {
    response.render('reset-password', provider.generateMenu('reset-password', request));
});

router.get('/receipt-management', checkAuth, (request, response) => {
    response.render('receipt', provider.generateMenu('receipt-management', request));
});

router.get('/undelete-quiz', checkAuth, (request, response) => {
    response.render('undelete-quiz', provider.generateMenu('undelete-quiz', request));
});

router.get('/upgrade-to-pro', checkAdmin, (request, response) => {
    response.render('bulk-upgrade', provider.generateMenu('pro-upgrade', request));
});

router.get('/downgrade-user', checkAdmin, (request, response) => {
    response.render('downgrade-user', provider.generateMenu('downgrade-user', request));
});

router.get('/manage-banners', checkSupreme, (request, response) => {
    response.render('manage-banners', provider.generateMenu('manage-banners', request));
});

router.get('/manage-admins', checkSupreme, (request, response) => {
    response.render('manage-admins', provider.generateMenu('manage-admins', request));
});

router.get('/manage-prices', checkSupreme, (request, response) => {
    response.render('price-management', provider.generateMenu('manage-prices', request));
});


/*------------------*/
/*    API Routes    */
/*------------------*/

router.post('/accounting-reports', checkAuth, async (request, response) => {
    let result = await provider.sendReports(request);
    response.status(result.status);
    response.send(result.message);
});

router.post('/queries', checkAuth, async (request, response) => {
    let result = await provider.runQuery(request);
    response.status(result.status);
    response.send(result.message);
});

router.post('/user/delete', checkAuth, async (request, response) => {
    let result = await provider.deleteUser(request);
    response.status(result.status);
    response.send(result.message);
});

router.post('/search/room', checkAuth, async (request, response) => {
    let result = await provider.getRoomOwner(request);
    response.status(result.status);
    response.send(result.message);
});

router.post('/login/teacher-account', checkAuth, async (request, response) => {
    let result = await provider.getTokenLoginUrl(request);
    response.status(result.status);
    response.send(result.message);
});

router.post('/merge', checkAuth, async (request, response) => {
    let result = await provider.mergeAccounts(request);
    response.status(result.status);
    response.send(result.message);
});

router.post('/reset/password', checkAuth, async (request, response) => {
    let result = await passwordProvider.createPasswordToken(request, false);
    
    response.status(result.status);
    
    let message = Constants.USER_NOT_FOUND_MESSAGE;
    
    if (result.data.error) {
        if (result.status === Constants.HTTP_500_INTERNAL_SERVER_ERROR) {
            message = Constants.INTERNAL_SERVER_ERROR_MESSAGE;
        }
    } else {
        message = result.data.url;
    }
    
    response.send(message);
});

router.post('/receipt/send', checkAuth, async (request, response) => {
    let result = null;
    try {
        result = await provider.sendReceipt(request);
    } catch (error) {
        result = error;
    }
    response.status(result.status);
    response.send(result.message);
});

router.post('/deleted-quizzes', checkAuth, async (request, response) => {
    let result = await provider.getDeletedQuizzes(request);
    response.status(result.status);
    response.send(result.message);
});

router.post('/undelete/quiz', checkAuth, async (request, response) => {
    let result = await provider.restoreQuiz(request);
    response.status(result.status);
    response.send(result.message);
});

let bulkUpload = multer({limits: {fileSize: 1000000}}).single('file');

router.post('/bulk-upgrade/license', checkAdmin, (request, response) => {
    bulkUpload(request, response, async (error) => {
        if (error) {
            response.status(Constants.HTTP_400_BAD_REQUEST);
            response.send(Constants.FILE_UPLOAD_PROBLEM_MESSAGE);
        } else {
            let result = await provider.upgradeToProLicense(request);
            response.status(result.status);
            response.send(result.message);
        }
    });
});

router.post('/user/downgrade', checkAdmin, async (request, response) => {
    let result = await provider.downgradeToFree(request);
    response.status(result.status);
    response.send(result.message);
});

router.get('/current-banners', checkSupreme, async (request, response) => {
    let result = await provider.getBanners(request);
    response.status(result.status);
    response.send(result.data);
});

router.post('/disable-banner', checkSupreme, async (request, response) => {
    let result = await provider.disableBanner(request);
    response.status(result.status);
    response.send(result.message);
});

router.post('/activate-banner', checkSupreme, async (request, response) => {
    let result = await provider.deployBanner(request);
    response.status(result.status);
    response.send(result.message);
});

router.get('/admins-list', checkSupreme, async (request, response) => {
    let result = await provider.getAdminList(request);
    response.status(result.status);
    response.send(result.data);
});

router.post('/add-admin', checkSupreme, async (request, response) => {
    let result = await provider.addAdmin(request);
    response.status(result.status);
    response.send(result.message);
});

router.delete('/remove-admin/:id', checkSupreme, async (request, response) => {
    let result = await provider.deleteAdmin(request);
    response.status(result.status);
    response.send(result.message);
});

/*-------------------------*/
/*    Prices API Routes    */
/*-------------------------*/

router.get('/prices', checkSupreme, async (request, response) => {
    let result = await priceProvider.getPrices();
    response.status(result.status);
    response.send(result.data);
});

router.post('/prices', checkSupreme, async (request, response) => {
    let result = await priceProvider.addPrice(request);
    response.status(result.status);
    response.send(result.data);
});

router.put('/prices/:id', checkSupreme, async (request, response) => {
    let result = await priceProvider.updatePrice(request);
    response.status(result.status);
    response.send(result.message);
});

router.delete('/prices/:id', checkSupreme, async (request, response) => {
    let result = await priceProvider.deletePrice(request);
    response.status(result.status);
    response.send(result.message);
});

router.get('/coupons', checkSupreme, async (request, response) => {
    let result = await couponProvider.getCoupons(request);
    response.status(result.status);
    response.send(result.data);
});

router.post('/coupons', checkSupreme, async (request, response) => {
    let result = await couponProvider.insertCoupon(request);
    response.status(result.status);
    response.send(result.data);
});

router.put('/coupons/:id', checkSupreme, async (request, response) => {
    let result = await couponProvider.updateCoupon(request);
    response.status(result.status);
    response.send(result.message);
});

router.delete('/coupons/:id', checkSupreme, async (request, response) => {
    let result = await couponProvider.deleteCoupon(request);
    response.status(result.status);
    response.send(result.message);
});

/*-----------------------------*/
/*    Renewal Prices Routes    */
/*-----------------------------*/

router.get('/renewal-prices', checkSupreme, async (request, response) => {
    let result = await priceProvider.getRenewalPrices();
    response.status(result.status);
    response.send(result.data);
});

router.put('/renewal-prices/:id', checkSupreme, async (request, response) => {
    let result = await priceProvider.changeRenewalPrice(request);
    response.status(result.status);
    response.send(result.message);
});

/*-------------------*/
/*    Cron Routes    */
/*-------------------*/

const RENEWAL_API_TOKEN = '05BA4C3C127A4CDE86B7DC4570C9BF17';

router.post('/renewals/notify', (request, response) => {
    let status = Constants.HTTP_200_OK;
    
    if (request.body.token !== RENEWAL_API_TOKEN) {
        status = Constants.HTTP_403_FORBIDDEN;
    } else {
        // We don't await the result because it will be long running
        // and the cron job doesn't try to handle a response anyway.
        renewalProvider.sendNotifications();
    }
    
    response.status(status);
    response.send({});
});

router.post('/renewals/renew', (request, response) => {
    let status = Constants.HTTP_200_OK;
    
    if (request.body.token !== RENEWAL_API_TOKEN) {
        status = Constants.HTTP_403_FORBIDDEN;
    } else {
        // We don't await the result because it will be long running
        // and the cron job doesn't try to handle a response anyway.
        renewalProvider.performRenewals();
    }
    
    response.status(status);
    response.send({});
});

module.exports = router;
