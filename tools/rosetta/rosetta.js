let CommandLineArgs = require('command-line-args'),
    CommandLineUsage = require('command-line-usage'),
    rosetta = require('./core/rosetta.js');

let optionDefinitions = [
    {name: 'back',      alias: 'b', type: Boolean, description: 'Perform an action for the back end'},
    {name: 'front',     alias: 'f', type: Boolean, description: 'Perform an action for the front end'},
    {name: 'help',      alias: 'h', type: Boolean, description: 'Show this message and exit'},
    {name: 'prune',     alias: 'p', type: Boolean, description: 'Remove unused translations (requires --front or --back)'},
    {name: 'translate', alias: 't', type: Boolean, description: 'Get new translations (requires --front or --back)'}
];

let options = CommandLineArgs(optionDefinitions);

if (options.help) {
    console.log(CommandLineUsage([
        {
            header: 'Socrative Rosetta',
            content: 'A tool for translating strings in the Socrative code base.'
        },
        {
            header: 'Options',
            optionList: optionDefinitions
        }
    ]));
    process.exit(0);
}

rosetta.execute(options);
