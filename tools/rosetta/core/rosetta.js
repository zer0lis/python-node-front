let fs = require('fs'),
    request = require('request');

class Rosetta {
    
    execute(options) {
        rosetta.options = options;
        
        rosetta.languages = ['da', 'de', 'es', 'fi', 'fr', 'ko', 'ms', 'nl', 'pt', 'sv', 'th', 'tr', 'zh-CN'];
        
        rosetta.directories = [
            '../../clients/app/entry/',
            '../../clients/app/routers/',
            '../../clients/app/shared/',
            '../../clients/app/student/',
            '../../clients/app/teacher/',
            '../../clients/shared/',
            '../../shared/'
        ];
        
        rosetta.codebase = {};
        rosetta.translated = {};
        rosetta.untranslated = {};
        
        if (!rosetta.isSet('front') && !rosetta.isSet('back')) {
            rosetta.exitWithError('Please specify --front or --back');
        }
        
        if (rosetta.isSet('translate') || rosetta.isSet('prune')) {
            rosetta.findUntranslated();
        } else {
            rosetta.exitWithError('Please specify an action (either --translate or --prune)');
        }
    }
    
    /**
     * Return true if an option was passed on the command line, false otherwise.
     * @param {string} name The name of the command line option.
     * @returns {boolean}
     */
    isSet(name) {
        return rosetta.options.hasOwnProperty(name);
    }
    
    /**
     * Print an error to the console and exit.
     * @param {string} message The error message to print to the console.
     */
    exitWithError(message) {
        console.error('\n' + message + '\n');
        process.exit(1);
    }

    /**
     * Begin the process of translating/pruning by finding all untranslated strings.
     */
    findUntranslated() {
        rosetta.getExistingTranslations();
        
        if (rosetta.isSet('front')) {
            for (let directory of rosetta.directories) {
                rosetta.readDirectory(directory);
            }
        } else if (rosetta.isSet('back')) {
            rosetta.readEnglishFile();
        }
        
        if (rosetta.isSet('prune')) {
            rosetta.writePrunedTranslations();
        } else {
            rosetta.translate();
        }
    }

    /**
     * Read the existing translation files for each language into memory.
     */
    getExistingTranslations() {
        for (let language of rosetta.languages) {
            rosetta.translated[language] = rosetta.readTranslationFile(language);
        }
    }

    /**
     * Read one language's translation file into memory.
     * @param {string} language The language file to read
     * @returns {object} An object with English keys and translated values
     */
    readTranslationFile(language) {
        let file = `../../shared/translations/${language}-translations.js`;
        
        if (rosetta.isSet('back')) {
            file = `./backend/${language}-translations.js`;
        }
        
        let translations = {};
        
        try {
            fs.statSync(file);
        } catch (e) {
            return translations;
        }
        
        let input = fs.readFileSync(file, {encoding: 'utf8'}),
            lines = input.split('\n');
        
        for (let i = 0; i < lines.length; i++) {
            let line = lines[i].trim(),
                regex = /^"(.*?)"\:\s+"(.*?)",*\s*$/g,
                results = regex.exec(line);
            
            if (results && results.length === 3) {
                translations[results[1]] = results[2];
            }
        }
        
        return translations;
    }

    /**
     * Read all the JavaScript files in a directory and check for untranslated strings.
     * @param {string} dir The directory to read
     */
    readDirectory(dir) {
        let contents = fs.readdirSync(dir);
        
        for (let i = 0; i < contents.length; i++) {
            let item = dir + '/' + contents[i];
            
            if (fs.lstatSync(item).isDirectory()) {
                rosetta.readDirectory(item);
            } else if (fs.lstatSync(item).isFile() && !rosetta.shouldIgnoreFile(item)) {
                let strings = rosetta.readJavaScriptFile(item);
                rosetta.addToUntranslated(strings);
            }
        }
    }
    
    /**
     * Check whether a file should be ignored. Ignored files will not be parsed for translation strings.
     * @param {string} file The name of the file
     * @returns {boolean} true if the file should be ignored, false otherwise
     */
    shouldIgnoreFile(file) {
        if (/-translations\.js$/.test(file)) {
            return true;
        } else if (!/\.js$/.test(file)) {
            return true;
        }
        
        return false;
    }

    /**
     * Read a JavaScript file and parse it for translate() calls.
     * @param {string} file The name of the file
     * @returns {Array} An array of all the translate() strings in the file
     */
    readJavaScriptFile(file) {
        let input = fs.readFileSync(file, {encoding: 'utf8'}),
            lines = input.split('\n'),
            strings = [];
        
        for (let i = 0; i < lines.length; i++) {
            let line = lines[i].trim(),
                regex = /translate\(['"](.*?)['"]\)/g,
                results;
            
            while ((results = regex.exec(line)) !== null) {
                if (results && results.length === 2) {
                    let string = results[1].replace(/"/g, '\\"');
                    strings.push(string);
                }
            }
        }
        
        return strings;
    }
    
    readEnglishFile() {
        let input = fs.readFileSync('./backend/_english.txt', {encoding: 'utf8'}),
            lines = input.split('\n'),
            strings = [];
        
        for (let i = 0; i < lines.length; i++) {
            let line = lines[i].trim(),
                string = line.replace(/"/g, '\\"');
            
            strings.push(string);
        }
        
        rosetta.addToUntranslated(strings);
    }
    
    addToUntranslated(strings) {
        for (let string of strings) {
            for (let language of rosetta.languages) {
                if (rosetta.isSet('prune')) {
                    if (!rosetta.codebase[language]) {
                        rosetta.codebase[language] = {};
                    }
                    rosetta.codebase[language][string] = true;
                } else if (!rosetta.translated[language][string] || rosetta.translated[language][string].length === 0) {
                    if (!rosetta.untranslated[language]) {
                        rosetta.untranslated[language] = {};
                    }
                    rosetta.untranslated[language][string] = '';
                }
            }
        }
    }
    
    translate() {
        rosetta.createArrays();
        
        if (!rosetta.targets) {
            rosetta.exitWithError('Nothing to translate...');
        }
        
        rosetta.getBatch();
    }
    
    createArrays() {
        /*
            This is what the arrays will look like: 
            
                rosetta.targets = ['es, 'fr', ...]
                
                rosetta.terms = [
                    [{u: 'First Name', t: ''}, {u: 'Last Name', t:''}, ...], // Corresponds to 'es' in the target array
                    [{u: 'First Name', t: ''}, {u: 'Last Name', t:''}, ...], // Corresponds to 'fr' in the targets array
                ]
         */
        for (let language of rosetta.languages) {
            if (rosetta.untranslated[language]) {
                if (!rosetta.targets) {
                    rosetta.targets = [];
                }
                
                rosetta.targets.push(language);
                
                if (!rosetta.terms) {
                    rosetta.terms  = [];
                }
                
                rosetta.terms.push([]);
                
                let targetIndex = rosetta.targets.length - 1;
                
                for (let key in rosetta.untranslated[language]) {
                    if (rosetta.untranslated[language].hasOwnProperty(key)) {
                        rosetta.terms[targetIndex].push({u: key, t: ''});
                    }
                }
            }
        }
        
        rosetta.targetIndex = 0;
        rosetta.termStartIndex = 0;
        rosetta.termEndIndex = 0;
    }

    /**
     * Get a batch of terms translated. The Google Translate API requires that the request URL contain
     * less than 2,000 characters. (https://cloud.google.com/translate/v2/translating-text-with-rest)
     */
    getBatch() {
        let targetIndex = rosetta.targetIndex,
            targets = rosetta.targets,
            terms = rosetta.terms,
            target = targets[targetIndex],
            url = 'https://www.googleapis.com/language/translate/v2?format=text&source=en&prettyprint=false&target='+target+'&key='+process.env.GOOGLE_TRANSLATE_API_KEY;
        
        while (rosetta.termEndIndex < terms[targetIndex].length && String(url + rosetta.encodeBeforeGoogle(terms[targetIndex][rosetta.termEndIndex].u)).length < 2000) {
            url += rosetta.encodeBeforeGoogle(terms[targetIndex][rosetta.termEndIndex].u);
            rosetta.termEndIndex++;
        }
        
        request(url, rosetta.batchDone);
    }
    
    encodeBeforeGoogle(str) {
        return '&q=' + encodeURIComponent(str).replace(/[!'()*]/g, function(c) {return '%' + c.charCodeAt(0).toString(16);});
    }
    
    batchDone(error, response, body) {
        let targetIndex = rosetta.targetIndex,
            targets = rosetta.targets,
            terms = rosetta.terms;
        
        try {
            let result = JSON.parse(body);
            
            if (error || response.statusCode !== 200) {
                let message = result && result.error && result.error.message ? result.error.message : 'Unknown Error.';
                rosetta.exitWithError('Request Error: ' + message);
            } else {
                if (result.data && result.data.translations) {
                    for (let i = 0; i < result.data.translations.length; i++) {
                        let u = terms[targetIndex][rosetta.termStartIndex].u;
                        terms[targetIndex][rosetta.termStartIndex].t = result.data.translations[i].translatedText;
                        rosetta.termStartIndex++;
                    }
                    
                    rosetta.termStartIndex++;
                    rosetta.termEndIndex++;
                    
                    if (rosetta.termEndIndex < terms[targetIndex].length) {
                        rosetta.getBatch();
                    } else {
                        rosetta.targetIndex++;
                        
                        if (rosetta.targetIndex < targets.length) {
                            rosetta.termStartIndex = 0;
                            rosetta.termEndIndex = 0;
                            rosetta.getBatch();
                        } else {
                            rosetta.mergeNewTranslations();
                            rosetta.writeNewTranslations();
                        }
                    }
                }
            }
        } catch(error) {
            rosetta.exitWithError('Error processing batch:', error);
        }
    }
    
    mergeNewTranslations() {
        for (let i = 0; i < rosetta.languages.length; i++) {
            let language = rosetta.languages[i],
                newTranslations = {};
            
            for (let j = 0; j < rosetta.terms[i].length; j++) {
                let term = rosetta.terms[i][j];
                newTranslations[term.u] = term.t;
            }
            
            Object.assign(rosetta.translated[language], newTranslations);
        }
    }
    
    writeNewTranslations() {
        for (let language of rosetta.languages) {
            let keys = [];
            
            for (let key in rosetta.translated[language]) {
                if (rosetta.translated[language].hasOwnProperty(key)) {
                    keys.push(key);
                }
            }
            
            if (keys.length > 0) {
                keys.sort();
                
                let data = rosetta.isSet('front') ? 'module.exports = {' : 'SOCRATIVE.translations = {';
                
                for (let j = 0; j < keys.length; j++) {
                    let english = keys[j],
                        translation = rosetta.translated[language][english];
                    
                    data += '\n    "' + english + '": "' + translation + '"';
                    
                    if (j < keys.length - 1) {
                        data += ',';
                    }
                }
                
                data += '\n};\n';
                
                let file = `../../shared/translations/${language}-translations.js`;
                
                if (rosetta.isSet('back')) {
                    file = `./backend/${language}-translations.js`;
                }
                
                fs.writeFileSync(file, data);
            }
        }
    }
    
    writePrunedTranslations() {
        for (let language of rosetta.languages) {
            let keys = [];
            
            for (let key in rosetta.codebase[language]) {
                if (rosetta.codebase[language].hasOwnProperty(key)) {
                    keys.push(key);
                }
            }
            
            keys.sort();
            
            let data = rosetta.isSet('front') ? 'module.exports = {' : 'SOCRATIVE.translations = {';
            
            for (let j = 0; j < keys.length; j++) {
                let english = keys[j];
                let translation = rosetta.translated[language][english];
                data += '\n    "' + english + '": "' + translation + '"';
                if (j < keys.length - 1) {
                    data += ',';
                }
            }
            
            data += '\n};\n';
            
            let file = `../../shared/translations/${language}-translations.js`;
            
            if (rosetta.isSet('back')) {
                file = `./backend/${language}-translations.js`;
            }
            
            fs.writeFileSync(file, data);
        }
    }
    
}

let rosetta = new Rosetta();
module.exports = rosetta;
