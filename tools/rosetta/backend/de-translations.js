SOCRATIVE.translations = {
    "": "",
    "(Vote) {0}": "(Abstimmung) {0}",
    ", click": ", klicken",
    ", click the orange": ", klicke auf die Orange",
    ", go to": ", gehe zu",
    ", then complete registration": ", dann die Registrierung abschließen",
    "1 year": "1 Jahr",
    "150 student capacity per room": "150 Schülerkapazität pro Zimmer",
    "175 West 200 South, Suite 1000": "175 West 200 Süd, Suite 1000",
    "<b>Amount</b>": "<b> Betrag </ b>",
    "<b>Email</b>": "<b> E-Mail </ b>",
    "<b>Expires": "<b> Läuft ab",
    "<b>License Key</b>": "<b> Lizenzschlüssel </ b>",
    "<b>Name</b>": "<b> Name </ b>",
    "<b>Order Number</b>": "<b> Bestellnummer </ b>",
    "<b>Organization</b>": "<b> Organisation </ b>",
    "<b>Payment Method</b>": "<b> Zahlungsmethode </ b>",
    "<b>Socrative, Inc</b>": "<b> Socrative, Inc </ b>",
    "<b>Subscription Length</b>": "<b> Abonnementlänge </ b>",
    "<b>Thank you for going PRO!</b>": "<B> Danke, dass Sie PRO gehen! </ B>",
    "Account tab": "Konto-Tab",
    "Activate": "aktivieren Sie",
    "Additional Resources": "Zusätzliche Ressourcen",
    "Articles, step by steps</br> & screenshots": "Artikel, Schritt für Schritt </ br> & Screenshots",
    "Blue": "Blau",
    "COMMON HELP ARTICLES": "GEMEINSAME HILFEARTIKEL",
    "CREATE PASSWORD": "PASSWORT ERSTELLEN",
    "Class Scoring": "Klasse Scoring",
    "Click the link for": "Klicken Sie auf den Link für",
    "Column calculations only reflect present students": "Spaltenberechnungen spiegeln nur die anwesenden Schüler wider",
    "Copyright &copy; 2016 MasteryConnect, Inc. All Rights Reserved": "Copyright & copy; 2016 MasteryConnect, Inc. Alle Rechte vorbehalten",
    "Copyright &copy; 2017 MasteryConnect, Inc. All Rights Reserved": "Urheberrecht & Kopie; 2017 MasteryConnect, Inc. Alle Rechte vorbehalten",
    "Create Account": "Benutzerkonto anlegen",
    "Crimson": "karmesinrot",
    "Dimgray": "schwachgrau",
    "EXISTING USERS": "EXISTIERENDE BENUTZER",
    "Enter License Key": "Lizenzschlüssel eingeben",
    "Enter the": "Geben Sie die ein",
    "Expires": "Läuft ab",
    "Folders": "Ordner",
    "From the": "Von dem",
    "Get started by creating a password.": "Beginnen Sie mit dem Erstellen eines Kennworts.",
    "Getting Started with Socrative PRO": "Erste Schritte mit Socrative PRO",
    "Go here": "Gehe hier hin",
    "Gold": "Gold",
    "Green": "Grün",
    "Have a nice day": "Einen schönen Tag noch",
    "Have a nice day,": "Einen schönen Tag noch,",
    "Help Center": "Hilfezentrum",
    "Here is a receipt for your records.": "Hier ist eine Quittung für Ihre Unterlagen.",
    "Here’s the report you requested.": "Hier ist der Bericht, den Sie angefordert.",
    "Hi %s,": "Seine,",
    "Hot Pink": "Hot Pink",
    "How to Retrieve a Receipt": "So erhalten Sie einen Beleg",
    "How well did you understand today's material?": "Wie gut verstehen Sie heute Material?",
    "If you have more questions about password reset, please visit our %s Help Center %s": "Wenn Sie weitere Fragen zu Passwort-Reset haben, besuchen Sie bitte unsere% s Hilfe% s",
    "Indigo": "Indigo",
    "KEEP ME IN THE LOOP": "HALTEN SIE MICH IM LOOP",
    "LEARN MORE": "ERFAHREN SIE MEHR",
    "LICENSE INFORMATION": "LIZENZINFORMATIONEN",
    "Least Correct Answers:": "Am wenigsten Richtige Antworten:",
    "License Key": "Lizenzschlüssel",
    "Lime": "Limette",
    "Login": "Anmeldung",
    "Magenta": "Magenta",
    "Make the most of": "Das meiste aus ... machen",
    "Maroon": "Kastanienbraun",
    "MasteryConnect is not required to, and does not, collect Washington             sales or use tax. Under Washington law, purchases are not tax-exempt merely because a seller is not required             to collect Washington's tax. Washington law requires Washington purchasers to review untaxed purchases and,             if any tax is owed, file a Washington use tax return and pay any tax due. Visit https://dor.wa.gov/consumerusetax             for more information.": "",
    "More Help Articles...": "Weitere Hilfeartikel ...",
    "Most Correct Answers:": "Die meisten Richtige Antworten:",
    "Multiple Rooms": "Mehrere Räume",
    "NEW USERS": "NEUE NUTZER",
    "New premium features are now available.": "Neue Premium-Funktionen sind jetzt verfügbar.",
    "New to Socrative?": "Neu bei Socrative?",
    "No": "Nein",
    "Not at all": "Ganz und gar nicht",
    "Not very well": "Nicht sehr gut",
    "Number of correct answers": "Anzahl der richtigen Antworten",
    "Oh snap, there are too many rows in your excel: %d, try to have under 1000": "Oh snap, es gibt zu viele Zeilen in Excel:% d, versuchen Sie unter 1000 zu haben",
    "On step 3 of registration,": "In Schritt 3 der Registrierung",
    "One Attempt": "Ein Versuch",
    "Orange": "Orange",
    "Our Blog": "Unser Blog",
    "Page %d of %d": "Seite% d von% d",
    "Peach": "Pfirsich",
    "Please answer the teacher's question.": "Bitte beantworten Sie die Frage des Lehrers.",
    "Presence": "Gegenwart",
    "Pretty well": "Ziemlich gut",
    "Privacy Policy": "Datenschutz-Bestimmungen",
    "Purchaser Name": "Name des Käufers",
    "Question type or question text missing at row %d": "Fragetyp oder Fragetext in Zeile% d fehlt",
    "Quiz names cannot exceed 128 characters. Please rename your quiz.": "Quiz-Namen können nicht mehr als 128 Zeichen nicht überschreiten. Bitte benennen Sie die Quiz.",
    "Red": "Rot",
    "Renew": "Erneuern",
    "Report a Problem": "Ein Problem melden",
    "Report a problem": "Ein Problem melden",
    "Room:": "Zimmer:",
    "Rose": "Rose",
    "Rosters": "Dienstpläne",
    "SUPPORT": "UNTERSTÜTZEN",
    "Salt Lake City, Utah 84101": "Salt Lake City, Utah 84101",
    "Score:": "Ergebnis:",
    "Seats (Teacher Accounts)": "Sitze (Lehrerkonten)",
    "Sharable links for easy student login": "Freibare Links für die einfache Anmeldung von Studenten",
    "Short Answer Activity": "Kurze Antwort Aktivität",
    "Sienna": "Sienaerde",
    "Silver": "Silber",
    "Socrative <b>PRO</b> Receipt": "Socrative <b> PRO </ b> Empfang",
    "Socrative PRO is Live!": "Socrative PRO ist Live!",
    "Socrative Pro Order Confirmation": "Socrative Pro-Order-Bestätigung",
    "Socrative Requirements": "Socrative Anforderungen",
    "Socrative Reset Password": "Socrative Kennwort zurücksetzen",
    "Socrative shared quiz": "Socrative gemeinsamen Quiz",
    "Socrative, Inc. 175 West 200 South, Suite 1000, Salt Lake City, Utah 84101, USA": "Socrative, Inc. 175 West 200 Süd, Suite 1000, Salt Lake City, Utah 84101, USA",
    "Space Race countdown timer": "Space Race Countdown-Timer",
    "Standard:": "Standard:",
    "Student Answer": "Studenten Antwort",
    "Student ID": "Studenten ID",
    "Student Login for Rostered Rooms": "Studenten Login für Rostered Rooms",
    "Student Name": "Name des Studenten",
    "Student Names": "Schülernamen",
    "Student names disabled": "Studentennamen deaktiviert",
    "Tan": "Bräunen",
    "Teachers can use the steps below to activate": "Lehrer können die folgenden Schritte zum Aktivieren verwenden",
    "Teal": "Teal",
    "Team": "Mannschaft",
    "Terms & Conditions": "Terms & amp; Bedingungen",
    "Terms &amp; Conditions": "Bedingungen & amp; Bedingungen",
    "Thank you": "Vielen Dank",
    "The Socrative Team": "Das Socrative-Team",
    "The are too few answers for MC type question at row %d": "Das sind zu wenige Antworten für MC-Typ Frage in Zeile% d",
    "The excel file is broken": "Die Excel-Datei ist beschädigt",
    "The report is too big to be sent via email , so here is the link to download it:": "Der Bericht ist zu groß, per E-Mail gesendet werden soll, so ist hier die Verbindung es zum Download:",
    "The user {{ display_name }} with the email address {{ email_address }} wants to share his quiz with you.": "Der Benutzer {{display_name}} mit der E-Mail-Adresse {{email_address}} möchte mit Ihnen sein Quiz zu teilen.",
    "There are not enough rows in the excel template": "Es gibt nicht genügend Zeilen in der Excel-Vorlage",
    "There are too few columns on row 2. Please use the template provided by Socrative": "Es gibt zu wenige Spalten auf Zeile 2. Bitte verwenden Sie die Vorlage zur Verfügung gestellt von Socrative",
    "Tips, articles,<br />ideas &amp; more": "Tipps, Artikel, <br /> Ideen & amp; Mehr",
    "To import his quiz , please access the following url :": "Zu seinem Quiz importieren, rufen Sie bitte die folgende URL:",
    "Top features": "Top Funktionen",
    "Total Questions:": "Insgesamt Fragen:",
    "Total Score (0 - 100)": "Insgesamt Score (0 - 100)",
    "Totally got it": "Völlig hat es",
    "USA": "USA",
    "Untitled quiz": "Untitled Quiz",
    "Untitled_quiz": "Untitled_quiz",
    "Up to 10 private or public rooms": "Bis zu 10 private oder öffentliche Räume",
    "Upgrade": "Aktualisierung",
    "Violet": "Violett",
    "Votes": "Stimmen",
    "Washington State Sales": "Washington State Verkäufe",
    "Watch a Video": "Ein Video angucken",
    "We are happy to have you in the community.<br>  Your transaction receipt will be sent in a separate email, so please check your spam/junk folder if you are unable to locate it.": "Wir freuen uns, Sie in der Community begrüßen zu können. <br> Ihre Transaktionsquittung wird in einer separaten E-Mail verschickt. Bitte überprüfen Sie Ihren Spam / Junk-Ordner, wenn Sie ihn nicht finden können.",
    "We are happy to have you in the community.<br> Like you, we think it's important to visualize student understanding. That's why we've developed an easy-to-use tool for building assessments and seeing results in real-time. It's everything you need to improve instruction and help student learning.": "Wir freuen uns, Sie in der Gemeinde zu haben. <br> Sie mögen, wir denken, es ist wichtig, Schüler Verständnis zu visualisieren. Deshalb haben wir ein einfach zu bedienendes Werkzeug für den Aufbau von Einschätzungen und zu sehen, Ergebnisse in Echtzeit entwickelt haben. Es ist alles, was Sie brauchen Unterricht zu verbessern und das Lernen der Schüler zu helfen.",
    "We received a request to reset the password for your Socrative account. If you didn't request a password reset, please ignore this email. If you did; to reset your password, click the link below or copy and paste the entire URL into your browser.": "Wir haben eine Anfrage erhalten Sie das Passwort für Ihr Socrative Konto zurücksetzen. Wenn Sie kein Passwort-Reset angefordert haben, ignorieren Sie diese E-Mail. Wenn du. .. getan hast; Klicken Sie Ihr Passwort zurücksetzen, den Link unten oder kopieren und die gesamte URL in den Browser einfügen.",
    "Welcome to Socrative PRO!": "Willkommen bei Socrative PRO!",
    "Welcome to Socrative!": "Willkommen bei Socrative!",
    "What did you learn in today's class?": "Was haben Sie in der heutigen Klasse lernen?",
    "Yay, Socrative Reports": "Yay, Socrative Berichte",
    "Yellow": "Gelb",
    "Yes": "Ja",
    "You May Also Like...": "Sie können auch mögen ...",
    "Your license": "Deine Lizenz",
    "Your license key is:": "Ihr Lizenzschlüssel ist:",
    "Your report from Socrative": "Ihr Bericht von Socrative",
    "account type": "Konto Typ",
    "bad request": "Ungültige Anforderung",
    "button": "Taste",
    "copy": "Kopieren",
    "existing account": "bestehendes Konto",
    "for purchasing a": "zum Kauf eines",
    "has more than one seat.": "hat mehr als einen Sitzplatz.",
    "internal server error": "interner Serverfehler",
    "into the popup, and Click": "in das Popup und klicken Sie auf",
    "license": "Lizenz",
    "license key": "Lizenzschlüssel",
    "or": "oder",
    "profile": "Profil",
    "question at row %d doesn't have a correct type": "Frage in Zeile% d keinen richtigen Typ",
    "select": "wählen",
    "the answer at column %d row %d is too long": "Die Antwort in Spalte% d Zeile% d ist zu lang",
    "the quiz '%s' doesn't have any valid questions": "das Quiz '% s' hat keine gültigen Fragen",
    "the text for the question at row %d is too long": "der Text für die Frage in Zeile% d ist zu lang",
    "to their": "zu ihren",
    "via the menu in the top right": "über das Menü oben rechts",
    "years": "Jahre"
};
