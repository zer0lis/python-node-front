SOCRATIVE.translations = {
    "": "",
    "(Vote) {0}": "(투표) {0}",
    ", click": ", 클릭",
    ", click the orange": ", 오렌지색을 클릭하십시오.",
    ", go to": ",에 가라.",
    ", then complete registration": ", 등록 완료",
    "1 year": "일년",
    "150 student capacity per room": "방당 학생 수 150 명",
    "175 West 200 South, Suite 1000": "175 웨스트 200 사우스, 스위트 1000",
    "<b>Amount</b>": "<b> 금액 </ b>",
    "<b>Email</b>": "<b> 이메일 </ b>",
    "<b>Expires": "<b> 만료",
    "<b>License Key</b>": "<b> 라이센스 키 </ b>",
    "<b>Name</b>": "<b> 이름 </ b>",
    "<b>Order Number</b>": "<b> 주문 번호 </ b>",
    "<b>Organization</b>": "<b> 조직 </ b>",
    "<b>Payment Method</b>": "<b> 지불 방법 </ b>",
    "<b>Socrative, Inc</b>": "<b> Socrative, Inc </ b>",
    "<b>Subscription Length</b>": "<b> 구독 기간 </ b>",
    "<b>Thank you for going PRO!</b>": "<b> PRO에 감사드립니다. </ b>",
    "Account tab": "계정 탭",
    "Activate": "활성화",
    "Additional Resources": "추가 리소스",
    "Articles, step by steps</br> & screenshots": "기사, 단계별 가이드 </ br> 및 스크린 샷",
    "Blue": "푸른",
    "COMMON HELP ARTICLES": "일반적인 도움말 기사",
    "CREATE PASSWORD": "암호 만들기",
    "Class Scoring": "등급 점수",
    "Click the link for": "에 대한 링크를 클릭하십시오.",
    "Column calculations only reflect present students": "열 계산은 현재 학생 만 반영합니다.",
    "Copyright &copy; 2016 MasteryConnect, Inc. All Rights Reserved": "저작권 &copy; 2016 MasteryConnect, Inc.가 판권 소유",
    "Copyright &copy; 2017 MasteryConnect, Inc. All Rights Reserved": "저작권 및 사본; 2017 MasteryConnect, Inc. 판권 소유",
    "Create Account": "계정 만들기",
    "Crimson": "진홍",
    "Dimgray": "Dimgray",
    "EXISTING USERS": "기존 사용자",
    "Enter License Key": "라이센스 키 입력",
    "Enter the": "들어가다",
    "Expires": "만료",
    "Folders": "폴더",
    "From the": "로부터",
    "Get started by creating a password.": "암호를 만들어 시작하십시오.",
    "Getting Started with Socrative PRO": "Socrative PRO 시작하기",
    "Go here": "여기로 가라.",
    "Gold": "금",
    "Green": "녹색",
    "Have a nice day": "좋은 하루 되세요",
    "Have a nice day,": "좋은 하루 되세요,",
    "Help Center": "지원 센터",
    "Here is a receipt for your records.": "여기에 귀하의 기록 영수증이 있습니다.",
    "Here’s the report you requested.": "다음은 요청 된 보고서입니다.",
    "Hi %s,": "안녕하세요 %s 의,",
    "Hot Pink": "핫 핑크",
    "How to Retrieve a Receipt": "영수증을받는 방법",
    "How well did you understand today's material?": "얼마나 잘 오늘의 자료를 이해 했는가?",
    "If you have more questions about password reset, please visit our %s Help Center %s": "당신이 암호 재설정에 대한 자세한 문의 사항이있는 경우, 우리 %의 도움말 센터 % s을 (를) 방문하세요",
    "Indigo": "남빛",
    "KEEP ME IN THE LOOP": "루프 IN ME를 보관",
    "LEARN MORE": "자세히보기",
    "LICENSE INFORMATION": "라이센스 정보",
    "Least Correct Answers:": "최소 정답 :",
    "License Key": "라이센스 키",
    "Lime": "라임",
    "Login": "로그인",
    "Magenta": "마젠타",
    "Make the most of": "최대한 활용하십시오",
    "Maroon": "적갈색",
    "MasteryConnect is not required to, and does not, collect Washington             sales or use tax. Under Washington law, purchases are not tax-exempt merely because a seller is not required             to collect Washington's tax. Washington law requires Washington purchasers to review untaxed purchases and,             if any tax is owed, file a Washington use tax return and pay any tax due. Visit https://dor.wa.gov/consumerusetax             for more information.": "",
    "More Help Articles...": "기타 도움말 기사 ...",
    "Most Correct Answers:": "대부분의 답변 수정 :",
    "Multiple Rooms": "여러 방",
    "NEW USERS": "신규 사용자",
    "New premium features are now available.": "이제 새로운 프리미엄 기능을 사용할 수 있습니다.",
    "New to Socrative?": "Socrative에 처음 오셨습니까?",
    "No": "아니",
    "Not at all": "전혀",
    "Not very well": "별로야",
    "Number of correct answers": "정답의 수",
    "Oh snap, there are too many rows in your excel: %d, try to have under 1000": "스냅 오, 당신의 엑셀에 너무 많은 행이 % d를 1000에서이 시도",
    "On step 3 of registration,": "등록 3 단계에서,",
    "One Attempt": "한 시도",
    "Orange": "주황색",
    "Our Blog": "우리의 블로그",
    "Page %d of %d": "가 %d 의 페이지 %d 개",
    "Peach": "복숭아",
    "Please answer the teacher's question.": "교사의 질문에 대답하십시오.",
    "Presence": "존재",
    "Pretty well": "꽤 잘",
    "Privacy Policy": "개인 정보 정책",
    "Purchaser Name": "구매자 이름",
    "Question type or question text missing at row %d": "행 %d 개에서 누락 된 질문 유형이나 질문 텍스트",
    "Quiz names cannot exceed 128 characters. Please rename your quiz.": "퀴즈 이름은 128자를 초과 할 수 없습니다. 퀴즈의 이름을 변경하시기 바랍니다.",
    "Red": "빨간",
    "Renew": "고쳐 쓰다",
    "Report a Problem": "문제 신고",
    "Report a problem": "문제 신고",
    "Room:": "방:",
    "Rose": "장미 꽃",
    "Rosters": "로스터",
    "SUPPORT": "지원하다",
    "Salt Lake City, Utah 84101": "솔트 레이크 시티, 유타 84101",
    "Score:": "점수:",
    "Seats (Teacher Accounts)": "좌석 (교사 계정)",
    "Sharable links for easy student login": "쉬운 학생 로그인을위한 공유 가능한 링크",
    "Short Answer Activity": "짧은 답변 활동",
    "Sienna": "시에나 토",
    "Silver": "은",
    "Socrative <b>PRO</b> Receipt": "Socrative <b> PRO </ b> 영수증",
    "Socrative PRO is Live!": "Socrative PRO는 라이브입니다!",
    "Socrative Pro Order Confirmation": "Socrative 프로 주문 확인",
    "Socrative Requirements": "소크라테이트 요구 사항",
    "Socrative Reset Password": "Socrative 암호 재설정",
    "Socrative shared quiz": "Socrative 공유 퀴즈",
    "Socrative, Inc. 175 West 200 South, Suite 1000, Salt Lake City, Utah 84101, USA": "Socrative, Inc. 175 West 200 South, Suite 1000, 유타주 솔트 레이크 시티 84101, 미국",
    "Space Race countdown timer": "우주 레이스 카운트 다운 타이머",
    "Standard:": "표준:",
    "Student Answer": "학생의 답변",
    "Student ID": "학생 아이디",
    "Student Login for Rostered Rooms": "Rostered Rooms의 학생 로그인",
    "Student Name": "학생 이름",
    "Student Names": "학생 이름",
    "Student names disabled": "학생 이름은 사용할 수 없습니다",
    "Tan": "탠 껍질",
    "Teachers can use the steps below to activate": "교사는 아래 단계를 사용하여",
    "Teal": "물오리",
    "Team": "팀",
    "Terms & Conditions": "이용 약관",
    "Terms &amp; Conditions": "이용 약관 &amp; 정황",
    "Thank you": "고맙습니다",
    "The Socrative Team": "Socrative 팀",
    "The are too few answers for MC type question at row %d": "은 행 %d 개에서 MC 형 질문도 몇 가지 답변입니다",
    "The excel file is broken": "엑셀 파일이 깨",
    "The report is too big to be sent via email , so here is the link to download it:": "이 보고서는 이메일을 통해 전송하기에 너무 큰, 그래서 여기에서 다운로드 할 수있는 링크입니다 :",
    "The user {{ display_name }} with the email address {{ email_address }} wants to share his quiz with you.": "이메일 주소를 가진 사용자는 {{DISPLAY_NAME}} {{EMAIL_ADDRESS}}이 당신과 함께 자신의 퀴즈를 공유하고자합니다.",
    "There are not enough rows in the excel template": "엑셀 템플릿에 충분한 행이 없습니다",
    "There are too few columns on row 2. Please use the template provided by Socrative": "행 2. 너무 적은 열이 있습니다 Socrative에서 제공하는 템플릿을 사용하십시오",
    "Tips, articles,<br />ideas &amp; more": "팁, 기사, <br /> 아이디어 &amp; 더",
    "To import his quiz , please access the following url :": "그의 퀴즈를 가져 오려면 다음 URL을 보낼 수 :",
    "Top features": "주요 기능",
    "Total Questions:": "총 질문 :",
    "Total Score (0 - 100)": "총 점수 (0 - 100)",
    "Totally got it": "완전히 그것을 가지고",
    "USA": "미국",
    "Untitled quiz": "제목 없음 퀴즈",
    "Untitled_quiz": "Untitled_quiz",
    "Up to 10 private or public rooms": "최대 10 개의 개인 또는 공용 객실",
    "Upgrade": "업그레이드",
    "Violet": "제비꽃",
    "Votes": "투표",
    "Washington State Sales": "워싱턴 주 판매",
    "Watch a Video": "비디오보기",
    "We are happy to have you in the community.<br>  Your transaction receipt will be sent in a separate email, so please check your spam/junk folder if you are unable to locate it.": "커뮤니티에서 귀하를 보내 주셔서 감사합니다. <br> 거래 영수증은 별도 이메일로 발송되므로 스팸 / 정크 폴더를 찾을 수없는 경우 스팸 / 정크 폴더를 확인하십시오.",
    "We are happy to have you in the community.<br> Like you, we think it's important to visualize student understanding. That's why we've developed an easy-to-use tool for building assessments and seeing results in real-time. It's everything you need to improve instruction and help student learning.": "우리는 당신을 좋아 그림입니다. 지역 사회에서 당신이 행복 우리는 학생들의 이해를 시각화하는 데 중요하다고 생각합니다. 우리가 평가를 구축하고 실시간으로 결과를보고하기위한 사용하기 쉬운 도구를 개발 한 이유입니다. 그것은 당신이 명령을 개선하고 학생들의 학습을 돕기 위해 필요한 모든입니다.",
    "We received a request to reset the password for your Socrative account. If you didn't request a password reset, please ignore this email. If you did; to reset your password, click the link below or copy and paste the entire URL into your browser.": "우리는 당신의 Socrative 계정의 암호를 재설정 요청을 받았습니다. 암호 재설정을 요청하지 않은 경우,이 이메일을 무시하세요. 당신이 한 경우; 비밀번호를 재설정, 아래 링크를 클릭하거나 복사하여 브라우저에 전체 URL을 붙여 넣습니다.",
    "Welcome to Socrative PRO!": "Socrative PRO에 오신 것을 환영합니다!",
    "Welcome to Socrative!": "Socrative에 오신 것을 환영합니다!",
    "What did you learn in today's class?": "오늘의 수업에서 무엇을 배웠는가?",
    "Yay, Socrative Reports": "야호, Socrative 리포트",
    "Yellow": "노랑",
    "Yes": "예",
    "You May Also Like...": "당신은 또한 같은 수 있습니다 ...",
    "Your license": "귀하의 라이센스",
    "Your license key is:": "라이센스 키 :",
    "Your report from Socrative": "Socrative에서 보고서",
    "account type": "계정 유형",
    "bad request": "잘못된 요청",
    "button": "단추",
    "copy": "부",
    "existing account": "기존 계정",
    "for purchasing a": "구매를위한",
    "has more than one seat.": "하나 이상의 좌석이 있습니다.",
    "internal server error": "인터넷 서버 오류",
    "into the popup, and Click": "팝업 창에",
    "license": "특허",
    "license key": "라이센스 키",
    "or": "또는",
    "profile": "윤곽",
    "question at row %d doesn't have a correct type": "행 %d 개에서 질문은 올바른 형식이 없습니다",
    "select": "고르다",
    "the answer at column %d row %d is too long": "열 %d 의 행 %d 에의 대답은 너무 깁니다",
    "the quiz '%s' doesn't have any valid questions": "퀴즈 '%s'은 (는) 유효한 질문이 없습니다",
    "the text for the question at row %d is too long": "행 %d 개에서 질문에 대한 텍스트가 너무 깁니다",
    "to their": "그들의",
    "via the menu in the top right": "오른쪽 상단의 메뉴를 통해",
    "years": "연령"
};
