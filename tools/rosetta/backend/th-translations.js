SOCRATIVE.translations = {
    "": "",
    "(Vote) {0}": "(เสียง) {0}",
    ", click": "คลิก",
    ", click the orange": "คลิกสีส้ม",
    ", go to": ", ไปที่",
    ", then complete registration": "แล้วลงทะเบียนเสร็จสมบูรณ์",
    "1 year": "1 ปี",
    "150 student capacity per room": "ความจุ 150 คนต่อห้อง",
    "175 West 200 South, Suite 1000": "175 West 200 South, Suite 1000",
    "<b>Amount</b>": "<b> จำนวน </ b>",
    "<b>Email</b>": "<b> อีเมล์ </ b>",
    "<b>Expires": "<b> วันที่หมดอายุ",
    "<b>License Key</b>": "<b> คีย์ใบอนุญาต </ b>",
    "<b>Name</b>": "<b> ชื่อ </ b>",
    "<b>Order Number</b>": "<b> เลขที่ใบสั่งซื้อ </ b>",
    "<b>Organization</b>": "<b> องค์กร </ b>",
    "<b>Payment Method</b>": "<b> วิธีการชำระเงิน </ b>",
    "<b>Socrative, Inc</b>": "<b> Socrative, Inc </ b>",
    "<b>Subscription Length</b>": "<b> ระยะเวลาการสมัครสมาชิก </ b>",
    "<b>Thank you for going PRO!</b>": "<b> ขอบคุณที่ PRO! </ b>",
    "Account tab": "แท็บบัญชี",
    "Activate": "กระตุ้น",
    "Additional Resources": "แหล่งข้อมูลเพิ่มเติม",
    "Articles, step by steps</br> & screenshots": "บทความทีละขั้นตอน </ br> & สกรีนช็อต",
    "Blue": "สีน้ำเงิน",
    "COMMON HELP ARTICLES": "บทความช่วยเหลือทั่วไป",
    "CREATE PASSWORD": "สร้างรหัสผ่าน",
    "Class Scoring": "ระดับเกณฑ์การให้คะแนน",
    "Click the link for": "คลิกลิงก์สำหรับ",
    "Column calculations only reflect present students": "การคำนวณคอลัมน์จะแสดงเฉพาะนักเรียนปัจจุบันเท่านั้น",
    "Copyright &copy; 2016 MasteryConnect, Inc. All Rights Reserved": "ลิขสิทธิ์ & copy; 2016 MasteryConnect, Inc. สงวนลิขสิทธิ์",
    "Copyright &copy; 2017 MasteryConnect, Inc. All Rights Reserved": "ลิขสิทธิ์ & copy; 2017 MasteryConnect, Inc. สงวนลิขสิทธิ์",
    "Create Account": "สร้างบัญชี",
    "Crimson": "แดงเข้ม",
    "Dimgray": "Dimgray",
    "EXISTING USERS": "ผู้ใช้ที่มีอยู่",
    "Enter License Key": "ป้อนคีย์ใบอนุญาต",
    "Enter the": "ป้อนข้อมูล",
    "Expires": "วันที่หมดอายุ",
    "Folders": "โฟลเดอร์",
    "From the": "จาก",
    "Get started by creating a password.": "เริ่มต้นด้วยการสร้างรหัสผ่าน",
    "Getting Started with Socrative PRO": "เริ่มต้นใช้ Socrative PRO",
    "Go here": "มานี่",
    "Gold": "ทอง",
    "Green": "สีเขียว",
    "Have a nice day": "ขอให้มีความสุขมาก ๆ ในวันนี้นะ",
    "Have a nice day,": "มีวันที่ดี",
    "Help Center": "ศูนย์ช่วยเหลือ",
    "Here is a receipt for your records.": "นี่คือใบเสร็จรับเงินสำหรับบันทึกของคุณ",
    "Here’s the report you requested.": "นี่คือรายงานที่คุณร้องขอ",
    "Hi %s,": "สวัสดี% s,",
    "Hot Pink": "ร้อนสีชมพู",
    "How to Retrieve a Receipt": "วิธีการรับใบเสร็จรับเงิน",
    "How well did you understand today's material?": "วิธีที่ดีที่คุณไม่เข้าใจวัสดุวันนี้?",
    "If you have more questions about password reset, please visit our %s Help Center %s": "หากคุณมีคำถามเพิ่มเติมเกี่ยวกับการตั้งค่ารหัสผ่านกรุณาเยี่ยมชมของเรา% s ศูนย์ช่วยเหลือ% s",
    "Indigo": "คราม",
    "KEEP ME IN THE LOOP": "ให้ฉันอยู่ในวง",
    "LEARN MORE": "เรียนรู้เพิ่มเติม",
    "LICENSE INFORMATION": "ข้อมูลใบอนุญาต",
    "Least Correct Answers:": "คำตอบที่ถูกต้องอย่างน้อย:",
    "License Key": "คีย์ใบอนุญาต",
    "Lime": "มะนาว",
    "Login": "เข้าสู่ระบบ",
    "Magenta": "สีม่วงแดงเข้ม",
    "Make the most of": "ใช้ประโยชน์สูงสุดจาก",
    "Maroon": "สีน้ำตาลแดง",
    "MasteryConnect is not required to, and does not, collect Washington             sales or use tax. Under Washington law, purchases are not tax-exempt merely because a seller is not required             to collect Washington's tax. Washington law requires Washington purchasers to review untaxed purchases and,             if any tax is owed, file a Washington use tax return and pay any tax due. Visit https://dor.wa.gov/consumerusetax             for more information.": "",
    "More Help Articles...": "บทความช่วยเหลือเพิ่มเติม ...",
    "Most Correct Answers:": "คำตอบที่ถูกต้องมากที่สุด:",
    "Multiple Rooms": "หลายห้อง",
    "NEW USERS": "ผู้ใช้ใหม่",
    "New premium features are now available.": "ขณะนี้มีคุณลักษณะระดับพรีเมียมใหม่",
    "New to Socrative?": "ใหม่เพื่อสังคม?",
    "No": "ไม่",
    "Not at all": "ไม่ใช่เลย",
    "Not very well": "ไม่ได้เป็นอย่างดี",
    "Number of correct answers": "จำนวนตอบที่ถูกต้อง",
    "Oh snap, there are too many rows in your excel: %d, try to have under 1000": "โอ้สแน็ปมีจำนวนแถวมากเกินไปใน Excel ของคุณ:% d, พยายามที่จะมีภายใต้ 1000",
    "On step 3 of registration,": "ในขั้นตอนที่ 3 ของการลงทะเบียน",
    "One Attempt": "ความพยายามครั้งเดียว",
    "Orange": "สีส้ม",
    "Our Blog": "บล็อกของเรา",
    "Page %d of %d": "หน้า% d จาก% d",
    "Peach": "พีช",
    "Please answer the teacher's question.": "กรุณาตอบคำถามของครู",
    "Presence": "การมี",
    "Pretty well": "ดีใช้ได้",
    "Privacy Policy": "นโยบายความเป็นส่วนตัว",
    "Purchaser Name": "ชื่อผู้ซื้อ",
    "Question type or question text missing at row %d": "พิมพ์คำถามหรือข้อความคำถามที่ขาดหายไปที่แถว% d",
    "Quiz names cannot exceed 128 characters. Please rename your quiz.": "ชื่อแบบทดสอบต้องไม่เกิน 128 ตัวอักษร กรุณาเปลี่ยนชื่อการทดสอบของคุณ",
    "Red": "สีแดง",
    "Renew": "ต่ออายุ",
    "Report a Problem": "รายงานปัญหา",
    "Report a problem": "รายงานปัญหา",
    "Room:": "ห้องพัก:",
    "Rose": "ดอกกุหลาบ",
    "Rosters": "กะ",
    "SUPPORT": "สนับสนุน",
    "Salt Lake City, Utah 84101": "Salt Lake City, Utah 84101",
    "Score:": "คะแนน:",
    "Seats (Teacher Accounts)": "ที่นั่ง (บัญชีสำหรับครู)",
    "Sharable links for easy student login": "การเชื่อมโยงที่ชาญฉลาดสำหรับการเข้าสู่ระบบของนักเรียนที่ง่าย",
    "Short Answer Activity": "กิจกรรมคำตอบสั้น ๆ",
    "Sienna": "สีน้ำตาล",
    "Silver": "เงิน",
    "Socrative <b>PRO</b> Receipt": "การประชาสัมพันธ์ <b> PRO </ b>",
    "Socrative PRO is Live!": "Socrative PRO คือ Live!",
    "Socrative Pro Order Confirmation": "การยืนยันคำสั่ง Socrative Pro",
    "Socrative Requirements": "ความต้องการทางสังคม",
    "Socrative Reset Password": "Socrative รีเซ็ตรหัสผ่าน",
    "Socrative shared quiz": "Socrative แบบทดสอบที่ใช้ร่วมกัน",
    "Socrative, Inc. 175 West 200 South, Suite 1000, Salt Lake City, Utah 84101, USA": "Socrative, Inc. 175 West 200 South, Suite 1000, Salt Lake City, Utah 84101, USA",
    "Space Race countdown timer": "จับเวลาถอยหลัง Space Race",
    "Standard:": "มาตรฐาน:",
    "Student Answer": "คำตอบของนักเรียน",
    "Student ID": "รหัสนักศึกษา",
    "Student Login for Rostered Rooms": "เข้าสู่ระบบสำหรับห้อง Rostered",
    "Student Name": "ชื่อนักเรียน",
    "Student Names": "รายชื่อนักศึกษา",
    "Student names disabled": "ชื่อนักเรียนที่ปิดใช้งาน",
    "Tan": "สีน้ำตาล",
    "Teachers can use the steps below to activate": "ครูสามารถใช้ขั้นตอนด้านล่างเพื่อเปิดใช้งาน",
    "Teal": "นกเป็ดน้ำ",
    "Team": "ทีม",
    "Terms & Conditions": "ข้อตกลงและเงื่อนไข",
    "Terms &amp; Conditions": "ข้อตกลง & amp; เงื่อนไข",
    "Thank you": "ขอขอบคุณ",
    "The Socrative Team": "ทีม Socrative",
    "The are too few answers for MC type question at row %d": "มีคำตอบที่น้อยเกินไปสำหรับประเภทคำถาม MC ที่แถว% d",
    "The excel file is broken": "ไฟล์ excel เสีย",
    "The report is too big to be sent via email , so here is the link to download it:": "รายงานมีขนาดใหญ่เกินกว่าที่จะถูกส่งผ่านทางอีเมล, ดังนั้นนี่คือการเชื่อมโยงไปดาวน์โหลดได้:",
    "The user {{ display_name }} with the email address {{ email_address }} wants to share his quiz with you.": "ผู้ใช้ {{}} DISPLAY_NAME กับที่อยู่อีเมล {{EMAIL_ADDRESS}} ต้องการแบ่งปันการตอบคำถามของเขากับคุณ",
    "There are not enough rows in the excel template": "มีไม่เพียงพอที่แถวในแม่แบบ Excel",
    "There are too few columns on row 2. Please use the template provided by Socrative": "มีคอลัมน์ที่น้อยเกินไปในแถว 2. กรุณาใช้แม่แบบที่มีให้โดย Socrative",
    "Tips, articles,<br />ideas &amp; more": "เคล็ดลับ, บทความ, ความคิด <br /> & amp; มากกว่า",
    "To import his quiz , please access the following url :": "ที่จะนำเข้าการตอบคำถามของเขาโปรดเข้าไปที่ URL ต่อไปนี้:",
    "Top features": "คุณลักษณะยอดนิยม",
    "Total Questions:": "คำถามทั้งหมด:",
    "Total Score (0 - 100)": "คะแนนรวม (0 - 100)",
    "Totally got it": "ทั้งหมดได้รับมัน",
    "USA": "สหรัฐอเมริกา",
    "Untitled quiz": "ตอบคำถามไม่ได้ตั้งชื่อ",
    "Untitled_quiz": "Untitled_quiz",
    "Up to 10 private or public rooms": "ห้องส่วนตัวหรือห้องส่วนตัวสูงสุด 10 ห้อง",
    "Upgrade": "อัพเกรด",
    "Violet": "สีม่วง",
    "Votes": "โหวต",
    "Washington State Sales": "การขายของรัฐวอชิงตัน",
    "Watch a Video": "ดูวิดีโอ",
    "We are happy to have you in the community.<br>  Your transaction receipt will be sent in a separate email, so please check your spam/junk folder if you are unable to locate it.": "เรายินดีที่จะมีคุณอยู่ในชุมชน <br> ใบเสร็จรับเงินของคุณจะถูกส่งไปในอีเมลแยกต่างหากดังนั้นโปรดตรวจสอบโฟลเดอร์สแปม / ขยะของคุณหากคุณไม่สามารถค้นหาได้",
    "We are happy to have you in the community.<br> Like you, we think it's important to visualize student understanding. That's why we've developed an easy-to-use tool for building assessments and seeing results in real-time. It's everything you need to improve instruction and help student learning.": "เรามีความยินดีที่จะมีคุณในชุมชน. ดาวน์โหลดเช่นเดียวกับคุณที่เราคิดว่ามันเป็นสิ่งสำคัญที่จะเห็นภาพความเข้าใจของนักเรียน นั่นเป็นเหตุผลที่เราได้พัฒนาเครื่องมือที่ง่ายต่อการใช้งานสำหรับการสร้างการประเมินและเห็นผลในเวลาจริง มันเป็นทุกสิ่งที่คุณจำเป็นต้องปรับปรุงการเรียนการสอนและช่วยให้เรียนรู้ของนักเรียน",
    "We received a request to reset the password for your Socrative account. If you didn't request a password reset, please ignore this email. If you did; to reset your password, click the link below or copy and paste the entire URL into your browser.": "เราได้รับคำขอเพื่อรีเซ็ตรหัสผ่านสำหรับบัญชี Socrative ของคุณ หากคุณไม่ได้ขอรีเซ็ตรหัสผ่านโปรดละเว้นอีเมลนี้ หากคุณไม่; เพื่อรีเซ็ตรหัสผ่านของคุณคลิกลิงค์ด้านล่างหรือคัดลอกและวาง URL ทั้งหมดลงในเบราว์เซอร์ของคุณ",
    "Welcome to Socrative PRO!": "ยินดีต้อนรับสู่ Socrative PRO!",
    "Welcome to Socrative!": "ยินดีต้อนรับสู่ Socrative!",
    "What did you learn in today's class?": "สิ่งที่คุณได้เรียนรู้ในชั้นเรียนในวันนี้หรือไม่?",
    "Yay, Socrative Reports": "ยาย Socrative รายงาน",
    "Yellow": "สีเหลือง",
    "Yes": "ใช่",
    "You May Also Like...": "คุณอาจจะชอบ...",
    "Your license": "ใบอนุญาตของคุณ",
    "Your license key is:": "คีย์ใบอนุญาตของคุณคือ:",
    "Your report from Socrative": "รายงานของคุณจาก Socrative",
    "account type": "ประเภทบัญชี",
    "bad request": "คำขอที่ไม่ดี",
    "button": "ปุ่ม",
    "copy": "สำเนา",
    "existing account": "บัญชีที่มีอยู่",
    "for purchasing a": "สำหรับการซื้อ a",
    "has more than one seat.": "มีที่นั่งมากกว่าหนึ่งที่นั่ง",
    "internal server error": "ข้อผิดพลาดภายในเซิร์ฟเวอร์",
    "into the popup, and Click": "ลงในป๊อปอัปและคลิก",
    "license": "อนุญาต",
    "license key": "คีย์ใบอนุญาต",
    "or": "หรือ",
    "profile": "ข้อมูลส่วนตัว",
    "question at row %d doesn't have a correct type": "คำถามที่แถว% d ไม่ได้ชนิดที่ถูกต้อง",
    "select": "เลือก",
    "the answer at column %d row %d is too long": "คำตอบที่คอลัมน์% d แถว% d ยาวเกินไป",
    "the quiz '%s' doesn't have any valid questions": "แบบทดสอบ '% s' ไม่ได้มีคำถามที่ถูกต้อง",
    "the text for the question at row %d is too long": "ข้อความสำหรับคำถามที่แถว% d ยาวเกินไป",
    "to their": "ของพวกเขา",
    "via the menu in the top right": "ผ่านทางเมนูด้านบนขวา",
    "years": "ปี"
};
