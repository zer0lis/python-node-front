var rosetta = require('../core/rosetta.js'),
    fs      = require('fs'),
    sinon   = require('sinon'),
    expect  = require('chai').expect;

describe('rosetta', function() {
    
    afterEach(function() {
        rosetta.options = {};
    });
    
    describe('initialize()', function() {
        
        beforeEach(function() {
            this.statSyncStub = sinon.stub(fs, 'statSync');
        });
        
        it('should add an \'options\' property to the \'rosetta\' object', function() {
            rosetta.initialize({});
            expect(rosetta.hasOwnProperty('options')).to.be.true;
        });
        
        it('should add a \'languages\' property to the \'rosetta\' object', function() {
            rosetta.initialize({});
            expect(rosetta.hasOwnProperty('languages')).to.be.true;
        });
        
        it('should add a \'directories\' property to the \'rosetta\' object', function() {
            rosetta.initialize({});
            expect(rosetta.hasOwnProperty('directories')).to.be.true;
        });
        
        it('should add a \'translated\' property to the \'rosetta\' object', function() {
            rosetta.initialize({});
            expect(rosetta.hasOwnProperty('translated')).to.be.true;
        });
        
        it('should add an \'untranslated\' property to the \'rosetta\' object', function() {
            rosetta.initialize({});
            expect(rosetta.hasOwnProperty('untranslated')).to.be.true;
        });
        
        it('should create the \'new-translations\' and \'untranslated\' directories if necessary', function() {
            rosetta.initialize({});
            expect(this.statSyncStub.callCount).to.equal(2);
        });
        
        describe('when the \'find\' option is passed', function() {
            
            before(function() {
                this.findUntranslatedStub = sinon.stub(rosetta, 'findUntranslated');
            });
            
            it('should begin the process of finding and writing all untranslated strings', function() {
                rosetta.initialize({find: true});
                expect(this.findUntranslatedStub.called).to.be.true;
            });
            
            after(function() {
                this.findUntranslatedStub.restore();
            });
            
        });
        
        describe('when the \'deploy\' option was passed', function() {
            
            before(function() {
                this.deployTranslationsStub = sinon.stub(rosetta, 'deployTranslations');
            });
            
            it('should begin the process of reading and deploying the new translation strings', function() {
                rosetta.initialize({deploy: true});
                expect(this.deployTranslationsStub.called).to.be.true;
            });
            
            after(function() {
                this.deployTranslationsStub.restore();
            });
            
        });
        
        afterEach(function() {
            this.statSyncStub.restore();
        });
        
    });
    
    describe('isSet()', function() {
        
        before(function() {
            rosetta.options = {find: null};
        });
        
        it('should return true when an option is set, even if it has no value', function() {
            expect(rosetta.isSet('find')).to.be.true;
        });
        
    });
    
    describe('findUntranslated()', function() {
        
        before(function() {
            this.getExistingTranslationsStub = sinon.stub(rosetta, 'getExistingTranslations');
            this.readDirectoryStub = sinon.stub(rosetta, 'readDirectory');
            this.writeUntranslatedStringsStub = sinon.stub(rosetta, 'writeUntranslatedStrings');
            rosetta.findUntranslated();
        });
        
        it('should read the existing translations into memory', function() {
            expect(this.getExistingTranslationsStub.called).to.be.true;
        });
        
        it('should read the rosetta.directories to find untranslated strings', function() {
            expect(this.readDirectoryStub.callCount).to.equal(8);
        });
        
        it('should write the untranslated strings to file', function() {
            expect(this.writeUntranslatedStringsStub.called).to.be.true;
        });
        
        after(function() {
            this.getExistingTranslationsStub.restore();
            this.readDirectoryStub.restore();
            this.writeUntranslatedStringsStub.restore();
        });
        
    });
    
    describe('readTranslationFile()', function() {
        
        describe('when true is passed', function() {
            
            beforeEach(function() {
                this.readFileSyncSpy = sinon.spy(fs, 'readFileSync');
            });
            
            it('should read an existing translation file', function() {
                rosetta.readTranslationFile('es-mx', true);
                expect(this.readFileSyncSpy.calledWith('../../common/static/js/translations/es-mx-translations.js', {encoding: 'utf8'})).to.be.true;
            });
            
            it('should return an object containing existing translation strings', function() {
                var translations = rosetta.readTranslationFile('es-mx', true);
                expect(translations['zoom']).to.equal('acercar');
            });
            
            afterEach(function() {
                this.readFileSyncSpy.restore();
            });
            
        });
        
        describe('when false is passed', function() {
            
            beforeEach(function() {
                this.readFileSyncStub = sinon.stub(fs, 'readFileSync', function() {
                    return '"test test test": "prueba prueba prueba"';
                });
            });
            
            it('should read a new translation file', function() {
                rosetta.readTranslationFile('es-mx', false);
                expect(this.readFileSyncStub.calledWith('./new-translations/es-mx.txt', {encoding: 'utf8'})).to.be.true;
            });
            
            it('should return an object containing existing translation strings', function() {
                var translations = rosetta.readTranslationFile('es-mx', false);
                expect(translations['test test test']).to.equal('prueba prueba prueba');
            });
            
            afterEach(function() {
                this.readFileSyncStub.restore();
            });
            
        });
        
    });
    
    describe('getExistingTranslations()', function() {
        
        before(function() {
            this.readTranslationFileSpy = sinon.spy(rosetta, 'readTranslationFile');
            rosetta.getExistingTranslations();
        });
        
        it('should read all the existing translation files', function() {
            expect(this.readTranslationFileSpy.callCount).to.equal(8);
        });
        
        it('should store the \'es-mx\' translations on the rosetta object', function() {
            expect(rosetta.translated['es-mx']['zoom']).to.equal('acercar');
        });
        
        it('should store the \'es-mx\' translations on the rosetta object', function() {
            expect(rosetta.translated['es']['zoom']).to.equal('acercar');
        });
        
        it('should store the \'fr-ca\' translations on the rosetta object', function() {
            expect(rosetta.translated['fr-ca']['zoom']).to.equal('zoom');
        });
        
        it('should store the \'fr\' translations on the rosetta object', function() {
            expect(rosetta.translated['fr']['zoom']).to.equal('zoom');
        });
        
        it('should store the \'ko\' translations on the rosetta object', function() {
            expect(rosetta.translated['ko']['zoom']).to.equal('확대/축소');
        });
        
        it('should store the \'nl\' translations on the rosetta object', function() {
            expect(rosetta.translated['nl']['zoom']).to.equal('inzoomen');
        });
        
        it('should store the \'pt-br\' translations on the rosetta object', function() {
            expect(rosetta.translated['pt-br']['zoom']).to.equal('aproximar');
        });
        
        it('should store the \'zh-cn\' translations on the rosetta object', function() {
            expect(rosetta.translated['zh-cn']['zoom']).to.equal('缩放');
        });
        
        after(function() {
            this.readTranslationFileSpy.restore();
        });
        
    });
    
    describe('readJavaScriptFile()', function() {
        
        beforeEach(function() {
            this.readFileSyncSpy = sinon.spy(fs, 'readFileSync');
        });
        
        it('should read the specified file', function() {
            rosetta.readJavaScriptFile('./test/mock/file/file.js');
            expect(this.readFileSyncSpy.calledWith('./test/mock/file/file.js', {encoding: 'utf8'})).to.be.true;
        });
        
        it('should return an array of translate() strings found in the file', function() {
            var strings = rosetta.readJavaScriptFile('./test/mock/file/file.js');
            expect(strings[0]).to.equal('This is a mock translation.');
        });
        
        it('should escape double quotes in translate() strings', function() {
            var strings = rosetta.readJavaScriptFile('./test/mock/file/file.js');
            expect(strings[1]).to.equal('This string has \\"double quotes\\" in it.');
        });
        
        afterEach(function() {
            this.readFileSyncSpy.restore();
        });
        
    });
    
    describe('readDirectory()', function() {
        
        beforeEach(function() {
            this.readdirSyncSpy = sinon.spy(fs, 'readdirSync');
            this.lstatSyncSpy = sinon.spy(fs, 'lstatSync');
        });
        
        it('should read the contents of the target directory', function() {
            rosetta.readDirectory('./test/mock/dir');
            expect(this.readdirSyncSpy.calledWith('./test/mock/dir')).to.be.true;
        });
        
        describe('when a directory is found', function() {
            
            before(function() {
                this.readDirectorySpy = sinon.spy(rosetta, 'readDirectory');
            });
            
            it('should recurse', function() {
                rosetta.readDirectory('./test/mock/dir');
                expect(this.lstatSyncSpy.calledWith('./test/mock/dir/subdir')).to.be.true;
                expect(this.readDirectorySpy.callCount).to.equal(2);
            });
            
            after(function() {
                this.readDirectorySpy.restore();
            });
            
        });
        
        describe('when a valid JavaScript file is found', function() {
            
            beforeEach(function() {
                rosetta.languages = ['es-mx', 'es', 'fr-ca', 'fr', 'ko', 'nl', 'pt-br', 'zh-cn'];
                rosetta.translated = {};
                rosetta.untranslated = {};
                for (var i = 0; i < rosetta.languages.length; i++) {
                    var language = rosetta.languages[i];
                    rosetta.translated[language] = {};
                }
                this.readJavaScriptFileSpy = sinon.spy(rosetta, 'readJavaScriptFile');
            });
            
            it('should read it', function() {
                rosetta.readDirectory('./test/mock/file');
                expect(this.readJavaScriptFileSpy.calledWith('./test/mock/file/file.js')).to.be.true;
            });
            
            it('should add untranslated strings to the \'untranslated\' property', function() {
                rosetta.readDirectory('./test/mock/file');
                expect(rosetta.untranslated['es-mx']['This is a mock translation.']).to.equal('');
            });
            
            afterEach(function() {
                this.readJavaScriptFileSpy.restore();
            });
            
        });
        
        describe('when a JavaScript filename ends in \'-translations.js\'', function() {
            
            before(function() {
                this.readJavaScriptFileSpy = sinon.spy(rosetta, 'readJavaScriptFile');
            });
            
            it('should ignore it', function() {
                rosetta.readDirectory('./test/mock/ignore');
                expect(this.readJavaScriptFileSpy.callCount).to.equal(0);
            });
            
            after(function() {
                this.readJavaScriptFileSpy.restore();
            });
            
        });
        
        describe('when a non-JavaScript file is found', function() {
            
            before(function() {
                this.readJavaScriptFileSpy = sinon.spy(rosetta, 'readJavaScriptFile');
            });
            
            it('should ignore it', function() {
                rosetta.readDirectory('./test/mock/dir/subdir');
                expect(this.readJavaScriptFileSpy.callCount).to.equal(0);
            });
            
            after(function() {
                this.readJavaScriptFileSpy.restore();
            });
            
        });
        
        afterEach(function() {
            this.readdirSyncSpy.restore();
            this.lstatSyncSpy.restore();
        });
        
    });
    
    describe('shouldIgnoreFile()', function() {
        
        describe('should return true', function() {
            
            it('when the filename ends in \'-translations.js\'', function() {
                expect(rosetta.shouldIgnoreFile('./test/mock/ignore/mock-translations.js')).to.be.true;
            });
            
            it('when the filename does not end in \'.js\'', function() {
                expect(rosetta.shouldIgnoreFile('./test/mock/dir/subdir/file.txt')).to.be.true;
            });
            
        });
        
    });
    
    describe('writeUntranslatedStrings()', function() {
        
        before(function() {
            this.writeFileSyncStub = sinon.stub(fs, 'writeFileSync');
            rosetta.untranslated = {
                "es-mx": {"Test": "", "Testing": ""},
                "es":    {"Test": "", "Testing": ""},
                "fr-ca": {"Test": "", "Testing": ""},
                "fr":    {"Test": "", "Testing": ""},
                "ko":    {"Test": "", "Testing": ""},
                "nl":    {"Test": "", "Testing": ""},
                "pt-br": {"Test": "", "Testing": ""},
                "zh-cn": {"Test": "", "Testing": ""}
            };
            
        });
        
        it('should write one file for each language that has untranslated strings', function() {
            rosetta.writeUntranslatedStrings();
            expect(this.writeFileSyncStub.callCount).to.equal(8);
        });
        
        after(function() {
            this.writeFileSyncStub.restore();
        });
        
    });
    
    describe('deployTranslations()', function() {
        
        before(function() {
            this.getExistingTranslationsStub = sinon.stub(rosetta, 'getExistingTranslations');
            this.mergeNewTranslationsStub = sinon.stub(rosetta, 'mergeNewTranslations');
            this.writeUpdatedTranslationsStub = sinon.stub(rosetta, 'writeUpdatedTranslations');
            rosetta.deployTranslations();
        });
        
        it('should read the existing translations into memory', function() {
            expect(this.getExistingTranslationsStub.called).to.be.true;
        });
        
        it('should merge the new translations into the existing', function() {
            expect(this.mergeNewTranslationsStub.called).to.be.true;
        });
        
        it('should write the new translation files', function() {
            expect(this.writeUpdatedTranslationsStub.called).to.be.true;
        });
        
        after(function() {
            this.getExistingTranslationsStub.restore();
            this.mergeNewTranslationsStub.restore();
            this.writeUpdatedTranslationsStub.restore();
        });
        
    });
    
    describe('mergeNewTranslations()', function() {
        
        before(function() {
            this.readTranslationFileStub = sinon.stub(rosetta, 'readTranslationFile');
            this.assignStub = sinon.stub(Object, 'assign');
            rosetta.translated = {
                "es-mx": {"Test": ""},
                "es":    {"Test": ""},
                "fr-ca": {"Test": ""},
                "fr":    {"Test": ""},
                "ko":    {"Test": ""},
                "nl":    {"Test": ""},
                "pt-br": {"Test": ""},
                "zh-cn": {"Test": ""}
            };
            rosetta.mergeNewTranslations();
        });
        
        it('should read all the new translation files', function() {
            expect(this.readTranslationFileStub.callCount).to.equal(8);
        });
        
        it('should merge the new translations with the existing', function() {
            expect(this.assignStub.calledWith(rosetta.translated['es-mx'])).to.be.true;
        });
        
        after(function() {
            this.readTranslationFileStub.restore();
            this.assignStub.restore();
        });
        
    });
    
    describe('writeUpdatedTranslations()', function() {
        
        before(function() {
            this.sortSpy = sinon.spy(Array.prototype, 'sort');
            this.writeFileSyncStub = sinon.stub(fs, 'writeFileSync');
            rosetta.translated = {
                "es-mx": {"yes": "sí",  "no": "no"},
                "es":    {"yes": "sí",  "no": "no"},
                "fr-ca": {"yes": "oui", "no": "non"},
                "fr":    {"yes": "oui", "no": "non"},
                "ko":    {"yes": "예",  "no": "아니"},
                "nl":    {"yes": "ja",  "no": "nee"},
                "pt-br": {"yes": "sim", "no": "não"},
                "zh-cn": {"yes": "是",  "no": "没有"}
            };
            rosetta.writeUpdatedTranslations();
        });
        
        it('should store the English strings in an array for each language and sort them', function() {
            expect(this.sortSpy.callCount).to.equal(8);
        });
        
        it('should write each new translation file', function() {
            expect(this.writeFileSyncStub.callCount).to.equal(8);
        });
        
        after(function() {
            this.sortSpy.restore();
            this.writeFileSyncStub.restore();
        });
        
    });
    
});
