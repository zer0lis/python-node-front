let spawnSync = require('child_process').spawnSync,
    pug = require('pug'),
    fs = require('fs'),
    request = require('request'),
    startingMin = -1,
    startingDesired = -1;

const BACK_END_AUTO_SCALING_GROUP_NAME = 'backEndAutoScalingGroup';

class Push {
    
    constructor() {
        this.environments = {
            rob: {
                BACKEND_HOST: 'https://rob-api.socrative.com',
                MARKETING_HOST: 'https://appstaging-marketing.socrative.com',
                PUBNUB_SUBSCRIBE_KEY: 'sub-c-bd4e8b32-c63a-11e3-b872-02ee2ddab7fe',
                STATIC_HOST: 'socrative-static-rob',
                STATIC_URL: 'https://rob-assets.socrative.com/',
                STRIPE_KEY: 'pk_test_61k3mqhhLBt3E9o1Ltdin58Y',
                TEACHER_SERVICE: 'https://teacher-dev.socrative.com',
                WEB_HOST: 'rob'
            },
            prod: {
                BACKEND_HOST: 'https://api.socrative.com',
                MARKETING_HOST: 'https://www.socrative.com',
                PUBNUB_SUBSCRIBE_KEY: 'sub-c-6dcf3cd2-13e8-11e3-8f5a-02ee2ddab7fe',
                STATIC_HOST: 'socrative-production-static-web',
                STATIC_URL: 'https://assets.socrative.com/',
                STRIPE_KEY: 'pk_live_AlBzuQWH5Hg2UwK58L1zPZjC',
                TEACHER_SERVICE: 'https://teacher.socrative.com',
                WEB_HOST: 'b'
            }
        };
        this.indexFiles = 0;
    }
    
    execute(options) {
        push.options = options;
        if (push.isSet('image')) {
            push.createImage();
        } else if (push.isSet('status')) {
            push.getStatus();
        } else if (push.isSet('config')) {
            push.createLaunchConfig();
        } else if (push.isSet('update')) {
            push.updateAutoScaling();
        } else if (push.isSet('load')) {
            push.checkLoadBalancer();
        } else if (push.isSet('term')) {
            push.terminate();
        } else if (push.isSet('front')) {
            push.getNewFrontEndVersion();
        }
    }

    /**
     * Return true if an option was passed on the command line, false otherwise.
     * @param name The name of the command line option.
     * @returns {boolean}
     */
    isSet(name) {
        return push.options.hasOwnProperty(name);
    }

    /**
     * Return true if an option passed on the command line has a value, false otherwise.
     * @param name The name of the command line option.
     * @returns {boolean}
     */
    hasValue(name) {
        return push.isSet(name) && push.options[name] !== null;
    }
    
    /**
     * Print an error to the console and exit.
     * @param {string} message The error message to print to the console.
     */
    exitWithError(message) {
        console.error('\n' + message + '\n');
        process.exit(1);
    }
    
    /**
     * Return a string that matches the 'betaYYYYMMDD-name' pattern.
     * @returns {string}
     */
    getBetaName() {
        let today = new Date(),
            month = today.getMonth() + 1,
            date  = today.getDate();
        
        // Add a leading zero to the month and date, if necessary.
        month = month < 10 ? ('0' + month) : month;
        date  = date  < 10 ? ('0' + date)  : date;
        
        return 'beta' + today.getFullYear() + month + date + '-' + push.options.name;
    }

    /**
     * Wrapper for spawning the AWS command-line interface in a child process.
     * @param options Array of strings containing the options to pass to the AWS command-line interface.
     * @returns {object} Object whose properties contain data from the AWS child process, such as stdout/stderr. See this for more:
     *                   https://nodejs.org/api/child_process.html#child_process_child_process_spawnsync_command_args_options
     */
    spawnAWS(options) {
        return spawnSync('aws', options);
    }

    /**
     * Parse the JSON result of an aws command and return the 'stdout' property.
     * @param result The result of an aws command.
     * @returns {object}
     */
    getOutput(result) {
        if (result.stdout) {
            return JSON.parse(result.stdout.toString());
        } else {
            push.exitWithError('Unknown error spawning AWS process.');
        }
    }
    
    setStartingMinAndDesired() {
        let result = push.spawnAWS(['autoscaling', 'describe-auto-scaling-groups', '--auto-scaling-group-names', BACK_END_AUTO_SCALING_GROUP_NAME]);
        
        if (result.status !== 0) {
            push.exitWithError(result.stderr ? 'Error setting min and desired capacity: ' + result.stderr.toString() : 'Unknown error setting min and desired capacity.');
        }
        
        let output = push.getOutput(result);
        
        if (output.AutoScalingGroups && output.AutoScalingGroups[0]) {
            startingMin = output.AutoScalingGroups[0].MinSize;
            startingDesired = output.AutoScalingGroups[0].DesiredCapacity;
            console.log('Starting min is', startingMin);
            console.log('Starting desired is', startingDesired);
        } else {
            push.exitWithError('Unknown error setting starting min/desired.');
        }
    }

    /**
     * Start a back-end deploy by creating an image of the BACK_0001 instance.
     * Requires --name and --min.
     */
    createImage() {
        if (!push.hasValue('name')) {
            push.exitWithError('Please provide a name for the new image.');
        }
        
        if (!push.hasValue('min') || !/^\d+$/.test(push.options.min)) {
            push.exitWithError('Please provide the new minimum number of instances.');
        }
        
        push.setStartingMinAndDesired();
        
        let result = push.spawnAWS(['ec2', 'create-image', '--instance-id', 'i-086fba829760efe9a', '--name', push.getBetaName()]),
            output = push.getOutput(result);
        
        if (output.ImageId) {
            console.log('Image ID:', output.ImageId);
            push.options.id = output.ImageId;
            push.getStatus();
        } else {
            if (result.stderr) {
                console.error('Error creating image:');
                push.exitWithError(result.stderr.toString());
            } else {
                push.exitWithError('Unknown error creating image.');
            }
        }
    }

    /**
     * Get the status (available, pending, failed) of a newly created image using a 1-minute backoff algorithm.
     * Requires --id
     */
    getStatus() {
        if (!push.hasValue('id')) {
            push.exitWithError('Please provide the ID of the image whose status you want.');
            return; // Necessary for unit tests because of the setTimeout() call below.
        }
        
        if (!push.hasValue('name')) {
            push.exitWithError('Please provide a name for the new launch configuration.');
        }
        
        if (!push.hasValue('min') || !/^\d+$/.test(push.options.min)) {
            push.exitWithError('Please provide the new minimum number of instances.');
        }
        
        if (startingMin === -1 || startingDesired === -1) {
            push.setStartingMinAndDesired();
        }
        
        console.log('Waiting 60 seconds for the next status check...');
        
        setTimeout(() => {
            let result = push.spawnAWS(['ec2', 'describe-images', '--image-ids', push.options.id]),
                output = push.getOutput(result);
            
            if (output.Images && output.Images.length) {
                let image = output.Images[0];
                if (image && image.State) {
                    console.log('Image Status:', image.State);
                    if (image.State.toLowerCase() === 'pending') {
                        push.getStatus();
                    } else if (image.State.toLowerCase() === 'available') {
                        push.createLaunchConfig();
                    }
                } else {
                    push.exitWithError('Unknown error getting image status.');
                }
            }
        }, 1000 * 60);
    }

    /**
     * Return the oldest launch configuration whose name starts with 'beta'.
     * @param configs Array of launch configurations.
     * @returns {string} The name of the old launch configuration.
     */
    getOldLaunchConfig(configs) {
        let betaConfigs = [];
        
        for (let i = 0; i < configs.length; i++) {
            let config = configs[i];
            if (config.LaunchConfigurationName && /^beta/.test(config.LaunchConfigurationName) && config.CreatedTime) {
                betaConfigs.push(config);
            }
        }
        
        betaConfigs.sort((a, b) => {
            let aDate = new Date(a.CreatedTime),
                bDate = new Date(b.CreatedTime);
            
            return aDate.getTime() - bDate.getTime();
        });
        
        return betaConfigs[0].LaunchConfigurationName;
    }
    
    deleteLaunchConfig() {
        let result = push.spawnAWS(['autoscaling', 'describe-launch-configurations', '--max-items', 20]),
            output = push.getOutput(result);
        
        if (output.LaunchConfigurations && output.LaunchConfigurations.length) {
            let config = push.getOldLaunchConfig(output.LaunchConfigurations);
            console.log('Deleting launch configuration:', config);
            push.spawnAWS(['autoscaling', 'delete-launch-configuration', '--launch-configuration-name', config]);
        }
    }
    
    createLaunchConfig() {
        if (!push.hasValue('id')) {
            push.exitWithError('Please provide an image ID for the new launch configuration.');
        }
        
        if (!push.hasValue('name')) {
            push.exitWithError('Please provide a name for the new launch configuration.');
        }
        
        if (!push.hasValue('min') || !/^\d+$/.test(push.options.min)) {
            push.exitWithError('Please provide the new minimum number of instances.');
        }
        
        if (startingMin === -1 || startingDesired === -1) {
            push.setStartingMinAndDesired();
        }
        
        push.deleteLaunchConfig();
        
        let mapping = JSON.stringify([
            {
                DeviceName: '/dev/xvda',
                Ebs: {
                    DeleteOnTermination: true,
                    VolumeSize: 8,
                    VolumeType: 'gp2'
                }
            }
        ]);
        
        let createResult = push.spawnAWS(['autoscaling', 'create-launch-configuration', '--image-id', push.options.id, '--instance-type', 'm3.medium', '--launch-configuration-name', push.getBetaName(), '--instance-monitoring', 'Enabled=false', '--block-device-mappings', mapping, '--security-groups', 'sg-76d22203', '--key-name', 'ec2-keypair']);
        
        if (createResult.status !== 0) {
            push.exitWithError(createResult.stderr ? 'Error creating launch configuration: ' + createResult.stderr.toString() : 'Unknown error creating launch configuration.');
        }
        
        push.updateAutoScaling();
    }
    
    updateAutoScaling() {
        if (!push.hasValue('name')) {
            push.exitWithError('Please provide a launch configuration name.');
        }
        
        if (!push.hasValue('min') || !/^\d+$/.test(push.options.min)) {
            push.exitWithError('Please provide the new minimum number of instances.');
        }
        
        if (startingMin === -1 || startingDesired === -1) {
            push.setStartingMinAndDesired();
        }
        
        let updateResult = push.spawnAWS(['autoscaling', 'update-auto-scaling-group', '--auto-scaling-group-name', BACK_END_AUTO_SCALING_GROUP_NAME, '--launch-configuration-name', push.getBetaName(), '--min-size', push.options.min, '--desired-capacity', push.options.min]);
        
        if (updateResult.status !== 0) {
            push.exitWithError(updateResult.stderr ? 'Error updating auto scaling: ' + updateResult.stderr.toString() : 'Unknown error updating auto scaling.');
        }
        
        push.checkLoadBalancer();
    }
    
    checkLoadBalancer() {
        if (!push.hasValue('min') || !/^\d+$/.test(push.options.min)) {
            push.exitWithError('Please provide the new minimum number of instances.');
        }
        
        console.log('Waiting 60 seconds for the next load balancer check...');
        
        setTimeout(() => {
            let result = push.spawnAWS(['elbv2', 'describe-target-health', '--target-group-arn', 'arn:aws:elasticloadbalancing:us-east-1:437477382898:targetgroup/back-end-target-group/0d99fdfd74de70f9']),
                output = push.getOutput(result);
            
            if (output.TargetHealthDescriptions && output.TargetHealthDescriptions.length > 0) {
                if (!push.allHealthy(output.TargetHealthDescriptions)) {
                    push.checkLoadBalancer();
                } else {
                    console.log('All new instances are InService');
                    push.terminate();
                }
            }
        }, 60000);
    }
    
    allHealthy(instances) {
        if (instances.length < push.options.min) {
            return false;
        }
        
        let allReady = true;
        
        for (let i = 0; i < instances.length; i++) {
            if (!instances[i].TargetHealth || !instances[i].TargetHealth.State || instances[i].TargetHealth.State !== 'healthy') {
                allReady = false;
                break;
            }
        }
        return allReady;
    }
    
    terminate() {
        if (!push.hasValue('name')) { // Required for the call to getOldInstanceIds() below.
            push.exitWithError('Please provide a launch configuration name.');
        }
        
        let result = push.spawnAWS(['autoscaling', 'describe-auto-scaling-groups', '--auto-scaling-group-name', BACK_END_AUTO_SCALING_GROUP_NAME]),
            output = push.getOutput(result);
        
        if (output
            && output.AutoScalingGroups
            && output.AutoScalingGroups[0]
            && output.AutoScalingGroups[0].Instances
            && output.AutoScalingGroups[0].Instances.length) {
            let oldInstanceIds = push.getOldInstanceIds(output.AutoScalingGroups[0].Instances);
            
            if (oldInstanceIds.length > 0) {
                console.log('Terminating old instances:');
                for (let i = 0; i < oldInstanceIds.length; i++) {
                    let id = oldInstanceIds[i];
                    console.log(id);
                    
                    let terminateResult = push.spawnAWS(['autoscaling', 'terminate-instance-in-auto-scaling-group', '--instance-id', id, '--no-should-decrement-desired-capacity']);
                    if (terminateResult.status !== 0) {
                        push.exitWithError(terminateResult.stderr ? 'Error terminating instance: ' + terminateResult.stderr.toString() : 'Unknown error terminating instance.');
                    }
                }
                push.restoreMinAndDesired();
            }
        }
    }
    
    getOldInstanceIds(instances) {
        let newBetaName = push.getBetaName(),
            oldInstanceIds = [];
        
        for (let i = 0; i < instances.length; i++) {
            if (instances[i].LaunchConfigurationName !== newBetaName) {
                oldInstanceIds.push(instances[i].InstanceId);
            }
        }
        return oldInstanceIds;
    }
    
    restoreMinAndDesired() {
        if (startingMin === -1 || startingDesired === -1) {
            push.exitWithError('Cannot restore auto scaling to starting min/desired. At this point it\'s impossible to know what the starting values were.');
        }
        
        console.log('Restoring min to', startingMin, 'and desired to', startingDesired);
        
        let updateResult = push.spawnAWS(['autoscaling', 'update-auto-scaling-group', '--auto-scaling-group-name', BACK_END_AUTO_SCALING_GROUP_NAME, '--min-size', startingMin, '--desired-capacity', startingDesired]);
        
        if (updateResult.status !== 0) {
            push.exitWithError(updateResult.stderr ? 'Error restoring min and desired: ' + updateResult.stderr.toString() : 'Unknown error restoring min and desired.');
        }
    }
    
    
/*------------------------------*/
/*    Code for Pushing to S3    */
/*------------------------------*/
    
    getNewFrontEndVersion() {
        if (!push.hasValue('env')) {
            push.exitWithError('Please specify --env to indicate which front-end environment should receive the push.');
        }
        
        if (!push.environments[push.options.env]) {
            push.exitWithError('Unknown environment. Use --help to see supported environments.');
        }
        
        if (!push.options.version) {
            if (push.options.revert) {
                push.exitWithError('Cannot revert without --version.');
            }
            
            request(`https://${push.environments[push.options.env].WEB_HOST}.socrative.com/version.txt`, (error, response, body) => {
                if (error) {
                    push.exitWithError('Error looking up current version: ' + error);
                }
                
                let result = body.match(/^(\d+\.\d+\.)(\d+)$/);
                
                if (result && result[1] && result[2]) {
                    let currentVersion = parseInt(result[2]);
                    
                    if (isNaN(currentVersion)) {
                        push.exitWithError('Error looking up current version. Use --version or make sure the target environment has a version.txt file in the root.');
                    }
                    
                    push.options.version = result[1] + (currentVersion + 1);
                    
                    console.log('New version will be', push.options.version);
                    
                    push.deployFrontEnd();
                } else {
                    push.exitWithError('Error looking up current version. Use --version or make sure the target environment has a version.txt file in the root.');
                }
            });
        } else {
            console.log('New version will be', push.options.version);
            
            if (!/^\d+\.\d+\.\d+$/.test(push.options.version)) {
                push.exitWithError('Version number format must be: X.Y.Z');
            }
            
            if (push.options.revert) {
                push.revertFrontEnd();
            } else {
                push.deployFrontEnd();
            }
        }
    }
    
    revertFrontEnd() {
        process.chdir('../../');
        push.writeWebFiles();
    }
    
    deployFrontEnd() {
        console.log('Executing webpack build...');
        
        process.env.STATIC_URL_PATH = `${push.environments[push.options.env].STATIC_URL}${push.options.version}/js/`;
        process.chdir('../../');
        
        let result = spawnSync('node_modules/.bin/webpack', ['-p']);
        
        if (result.status === 0) {
            push.writeWebFiles();
        } else {
            push.exitWithError('Error: webpack -p failed.');
        }
    }
    
    writeWebFiles() {
        let htmlDir = './clients/app/assets/html',
            studentLoginDir = `${htmlDir}/login/student`,
            teacherLoginDir = `${htmlDir}/login/teacher`,
            studentDir = `${htmlDir}/student`,
            teacherDir = `${htmlDir}/teacher`;
        
        this.createDir(htmlDir);
        this.createDir(studentLoginDir);
        this.createDir(teacherLoginDir);
        this.createDir(studentDir);
        this.createDir(teacherDir);
        
        fs.writeFileSync(`${htmlDir}/version.txt`, push.options.version);
        
        let props = push.environments[push.options.env];
        props.VERSION = push.options.version;
        
        let templateDir = './clients/app/templates';
        
        push.writeIndexFile(`${templateDir}/base.pug`, props, `${htmlDir}/index.html`);
        push.writeIndexFile(`${templateDir}/student-login.pug`, props, `${studentLoginDir}/index.html`);
        push.writeIndexFile(`${templateDir}/teacher-login.pug`, props, `${teacherLoginDir}/index.html`);
        push.writeIndexFile(`${templateDir}/student.pug`, props, `${studentDir}/index.html`);
        push.writeIndexFile(`${templateDir}/teacher.pug`, props, `${teacherDir}/index.html`);
    }
    
    createDir(dir) {
        try {
            fs.accessSync(dir);
        } catch(error) {
            spawnSync('mkdir', ['-p', dir]);
        }
    }
    
    writeIndexFile(src, props, dst) {
        let render = pug.compileFile(src);
        
        fs.writeFileSync(dst, render(props));
        
        push.indexFiles++;
        
        if (push.indexFiles === 5) {
            if (push.options.revert) {
                push.syncWebFiles();
            } else {
                push.syncStaticFiles();
                push.syncWebFiles();
            }
        }
    }
    
    syncStaticFiles() {
        console.log('Pushing static files to s3...');
        
        let staticBucket = push.environments[push.options.env].STATIC_HOST,
            version = push.options.version,
            result = push.spawnAWS(['s3', 'sync', './clients/app/assets/files', 's3://'+staticBucket+'/'+ version]);
        
        if (result.status !== 0) {
            push.exitWithError('AWS error syncing static files.');
        }
    }
    
    syncWebFiles() {
        console.log('Pushing web files to s3...');
        
        let result = push.spawnAWS(['s3', 'sync', './clients/app/assets/html', `s3://${push.environments[push.options.env].WEB_HOST}.socrative.com`]);
        
        if (result.status !== 0) {
            push.exitWithError('AWS error syncing web files.');
        }
    }
    
}

let push = new Push();
module.exports = push;
