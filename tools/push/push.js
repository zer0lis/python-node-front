let CommandLineArgs = require('command-line-args'),
    CommandLineUsage = require('command-line-usage'),
    push = require('./core/push.js');

let optionDefinitions = [
    {name: 'help',    alias: 'h', type: Boolean, description: 'Display this message and exit'},
    
    /* Options for deploying the front end to S3 */
    
    {name: 'front',   alias: 'f', type: Boolean, description: 'Used with --env and --version to push to S3'},
    {name: 'env',     alias: 'e', type: String,  description: 'Specify the target front-end S3 environment (requires --front). Supported arguments are:\nrob\nprod'},
    {name: 'version', alias: 'v', type: String,  description: 'Specify the version number for an S3 front end (requires --front and --env)'},
    {name: 'revert',  alias: 'r', type: Boolean, description: 'Revert an S3 front-end to a previous version (requires --front, --env, and --version)'},
    
    /* Options for deploying the Python back end */
    
    {name: 'name',    alias: 'n', type: String,  description: 'Specify the last part of the name of an image or launch configuration'},
    {name: 'min',     alias: 'm', type: Number,  description: 'The new minimum and desired number of instances (used with --image and --name)'},
    {name: 'id',                  type: String,  description: 'Used with --status and --config to specify an image ID'},
    {name: 'image',   alias: 'i', type: Boolean, description: 'Start a back-end deploy by creating an image (requires --name and --min)'},
    {name: 'status',  alias: 's', type: Boolean, description: 'Resume a back-end deploy at the point of checking the status of the newly created image (requires --id, --name, and --min)'},
    {name: 'config',  alias: 'c', type: Boolean, description: 'Resume a back-end deploy at the point of creating a new launch configuration (requires --id, --name, and --min)'},
    {name: 'update',  alias: 'u', type: Boolean, description: 'Resume a back-end deploy at the point of updating the auto scaling group to use the new launch configuration, min, and desired capacity (requires --name and --min)'},
    {name: 'load',    alias: 'l', type: Boolean, description: 'Repeatedly check the load balancer until all instances are InService (requires --min)'},
    {name: 'term',    alias: 't', type: Boolean, description: 'Terminate old instances (requires --name)'}
];

let options = CommandLineArgs(optionDefinitions);

if (options.help) {
    console.log(CommandLineUsage([
        {
            header: 'Socrative Push',
            content: 'A tool for pushing Socrative deployments to AWS.'
        },
        {
            header: 'Options',
            optionList: optionDefinitions
        }
    ]));
    process.exit(0);
}

push.execute(options);
