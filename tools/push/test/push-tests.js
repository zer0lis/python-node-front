var push      = require('../core/push.js'),
    request   = require('request'),
    templates = require('magic-templates'),
    sinon     = require('sinon'),
    expect    = require('chai').expect;

describe('push', function() {
    
    beforeEach(function() {
        this.spawnAWSStub = sinon.stub(push, 'spawnAWS', function() {
            return {
                pid: 1,
                output: [],
                stdout: '',
                stderr: '',
                status: 1,
                signal: '',
                error: {}
            };
        });
    });
    
    afterEach(function() {
        this.spawnAWSStub.restore();
    });
    
    describe('initialize()', function() {
        
        beforeEach(function() {
            this.createImageStub = sinon.stub(push, 'createImage');
            this.getStatusStub = sinon.stub(push, 'getStatus');
            this.createLaunchConfigStub = sinon.stub(push, 'createLaunchConfig');
            this.updateAutoScalingStub = sinon.stub(push, 'updateAutoScaling');
            this.checkLoadBalancerStub = sinon.stub(push, 'checkLoadBalancer');
            this.terminateStub = sinon.stub(push, 'terminate');
            this.getNewFrontEndVersionStub = sinon.stub(push, 'getNewFrontEndVersion');
        });
        
        describe('should create an image', function() {
            
            it('when the --image option is passed', function() {
                push.initialize({image: true});
                expect(this.createImageStub.called).to.be.true;
            });
            
        });
        
        describe('should get the status of an image', function() {
            
            it('when the --status option is passed', function() {
                push.initialize({status: true});
                expect(this.getStatusStub.called).to.be.true;
            });
            
        });
        
        describe('should create a launch configuration', function() {
            
            it('when the --config option is passed', function() {
                push.initialize({config: true});
                expect(this.createLaunchConfigStub.called).to.be.true;
            });
            
        });
        
        describe('should update a launch configuration', function() {
            
            it('when the --update option is passed', function() {
                push.initialize({update: true});
                expect(this.updateAutoScalingStub.called).to.be.true;
            });
            
        });
        
        describe('should check a load balancer', function() {
            
            it('when the --load option is passed', function() {
                push.initialize({load: true});
                expect(this.checkLoadBalancerStub.called).to.be.true;
            });
            
        });
        
        describe('should terminate old instances', function() {
            
            it('when the --term option is passed', function() {
                push.initialize({term: true});
                expect(this.terminateStub.called).to.be.true;
            })
            
        });
        
        describe('should get the new front-end version', function() {
            
            it('when the --front option is passed with --env', function() {
                push.initialize({front: true, env: 'rob'});
                expect(this.getNewFrontEndVersionStub.called).to.be.true;
            });
            
        });
        
        afterEach(function() {
            this.createImageStub.restore();
            this.getStatusStub.restore();
            this.createLaunchConfigStub.restore();
            this.updateAutoScalingStub.restore();
            this.checkLoadBalancerStub.restore();
            this.terminateStub.restore();
            this.getNewFrontEndVersionStub.restore();
            push.options = {};
        });
        
    });
    
    describe('isSet()', function() {
        
        before(function() {
            push.options = {image: null};
        });
        
        it('should return true when an option is set, even if it has no value', function() {
            expect(push.isSet('image')).to.be.true;
        });
        
        after(function() {
            push.options = {};
        });
        
    });
    
    describe('hasValue()', function() {
        
        describe('should return true', function() {
            
            before(function() {
                push.options = {image: 'front'};
            });
            
            it('when the option is set and has a value', function() {
                expect(push.hasValue('image')).to.be.true;
            });
            
            after(function() {
                push.options = {};
            });
            
        });
        
        describe('should return false', function() {
            
            it('when the option is not set', function() {
                expect(push.hasValue('image')).not.to.be.true;
            });
            
        });
        
        describe('should return false', function() {
            
            before(function() {
                push.options = {image: null};
            });
            
            it('when the option is set but has no value', function() {
                expect(push.hasValue('image')).not.to.be.true;
            });
            
            after(function() {
                push.options = {};
            });
            
        });
        
    });
    
    describe('exitWithError()', function() {
        
        before(function() {
            this.errorStub = sinon.stub(console, 'error');
            this.exitStub = sinon.stub(process, 'exit');
            this.message = 'test';
            push.exitWithError(this.message);
        });
        
        it('should print an error to the console', function() {
            expect(this.errorStub.calledWith('\n' + this.message + '\n')).to.be.true;
        });
        
        it('should exit', function() {
            expect(this.exitStub.calledWith(1)).to.be.true;
        });
        
        after(function() {
            this.errorStub.restore();
            this.exitStub.restore();
        });
        
    });
    
    describe('getBetaName()', function() {
        
        before(function() {
            push.options = {name: 'test'}
        });
        
        it('should return a string following the \'betaYYYYMMDD-name\' pattern', function() {
            var today = new Date(),
                year  = today.getFullYear(),
                month = today.getMonth() + 1,
                date  = today.getDate();
            
            // Add a leading zero to the month and date, if necessary.
            month = month < 10 ? ('0' + month) : month;
            date  = date  < 10 ? ('0' + date)  : date;
            
            year  = year.toString();
            month = month.toString();
            date  = date.toString();
            
            var results = /(beta)(\d{4})(\d{2})(\d{2})(\-test)/.exec(push.getBetaName());
            expect(results.length).to.equal(6); // One for the full match, plus five for the capture groups.
            expect(results[1]).to.equal('beta');
            expect(results[2]).to.equal(year);
            expect(results[3]).to.equal(month);
            expect(results[4]).to.equal(date);
            expect(results[5]).to.equal('-test');
        });
        
        after(function() {
            push.options = {};
        });
        
    });
    
    describe('getOutput()', function() {
        
        before(function() {
            this.exitWithErrorStub = sinon.stub(push, 'exitWithError');
        });
        
        it('should return the AWS stdout', function() {
            var result = {stdout: '{}'};
            push.getOutput(result);
            expect(this.exitWithErrorStub.called).not.to.be.true;
        });
        
        after(function() {
            this.exitWithErrorStub.restore();
        });
        
    });
    
    describe('createImage()', function() {
        
        beforeEach(function() {
            this.getOutputStub = sinon.stub(push, 'getOutput', function() {return {}});
            this.exitWithErrorStub = sinon.stub(push, 'exitWithError');
        });
        
        describe('should exit with an error', function() {
            
            before(function() {
                push.options = {
                    image: true,
                    name: null,
                    back: true
                };
            });
            
            it('if the --name option was passed without a name', function() {
                push.createImage();
                expect(this.exitWithErrorStub.called).to.be.true;
            });
            
            after(function() {
                push.options = {};
            });
            
        });
        
        describe('should create an image', function() {
            
            before(function() {
                push.options = {
                    image: true,
                    name: 'test',
                    back: true
                };
                this.getStatusStub = sinon.stub(push, 'getStatus');
            });
            
            it('from back2', function() {
                var today = new Date(),
                    month = today.getMonth() + 1,
                    date  = today.getDate();
                
                month = month < 10 ? ('0' + month) : month;
                date  = date  < 10 ? ('0' + date) :  date;
                
                var imageName = 'beta' + today.getFullYear() + month + date + '-' + push.options.name;
                
                push.createImage();
                expect(this.spawnAWSStub.calledWith(['ec2', 'create-image', '--instance-id', 'i-67f3528e', '--name', imageName])).to.be.true;
            });
            
            after(function() {
                push.options = {};
                this.getStatusStub.restore();
            });
            
        });
        
        afterEach(function() {
            this.getOutputStub.restore();
            this.exitWithErrorStub.restore();
        });
        
    });
    
    describe('getStatus()', function() {
        
        describe('should exit with an error', function() {
            
            before(function() {
                push.options = {
                    status: true,
                    id: null
                };
                this.getOutputStub = sinon.stub(push, 'getOutput', function() {return {}});
                this.exitWithErrorStub = sinon.stub(push, 'exitWithError');
            });
            
            it('if the --id option was passed without an image id', function() {
                push.getStatus();
                expect(this.exitWithErrorStub.called).to.be.true;
            });
            
            after(function() {
                push.options = {};
                this.getOutputStub.restore();
                this.exitWithErrorStub.restore();
            });
            
        });
        
    });
    
    describe('getOldLaunchConfig()', function() {
        
        it('should return the oldest launch configuration whose name starts with \'beta\'', function() {
            var configs = [
                {"CreatedTime": "2015-06-22T18:02:25.493Z", "LaunchConfigurationName": "awseb-Socrative-Large-424yFBx02Z"},
                {"CreatedTime": "2014-06-27T03:45:54.576Z", "LaunchConfigurationName": "beta-06262014"},
                {"CreatedTime": "2014-09-19T02:10:10.426Z", "LaunchConfigurationName": "beta-09282014-take2"},
                {"CreatedTime": "2015-01-09T23:18:29.680Z", "LaunchConfigurationName": "beta01092015"},
                {"CreatedTime": "2015-01-22T23:16:23.472Z", "LaunchConfigurationName": "beta01222015"},
                {"CreatedTime": "2015-01-29T23:24:32.465Z", "LaunchConfigurationName": "beta01292015"},
                {"CreatedTime": "2015-01-30T00:54:29.746Z", "LaunchConfigurationName": "beta01292015a"},
                {"CreatedTime": "2015-01-30T23:35:09.363Z", "LaunchConfigurationName": "beta01302015"},
                {"CreatedTime": "2015-02-06T00:15:02.848Z", "LaunchConfigurationName": "beta02052015"},
                {"CreatedTime": "2015-02-06T01:33:50.203Z", "LaunchConfigurationName": "beta02052015a"}
            ];
            expect(push.getOldLaunchConfig(configs)).to.equal('beta-06262014');
        });
        
    });
    
    describe('deleteLaunchConfig()', function() {
        
        before(function() {
            this.getOutputStub = sinon.stub(push, 'getOutput', function() {return {LaunchConfigurations: [{}]}});
            this.getOldLaunchConfigStub = sinon.stub(push, 'getOldLaunchConfig', function() {return 'beta-06262014'});
        });
        
        it('should get an old launch config and delete it', function() {
            push.deleteLaunchConfig();
            expect(this.getOldLaunchConfigStub.called).to.be.true;
            expect(this.spawnAWSStub.calledWith(['autoscaling', 'describe-launch-configurations', '--max-items', 10])).to.be.true;
            expect(this.spawnAWSStub.calledWith(['autoscaling', 'delete-launch-configuration', '--launch-configuration-name', 'beta-06262014'])).to.be.true;
        });
        
        after(function() {
            this.getOutputStub.restore();
            this.getOldLaunchConfigStub.restore();
        });
        
    });
    
    describe('createLaunchConfig()', function() {
        
        beforeEach(function() {
            this.exitWithErrorStub = sinon.stub(push, 'exitWithError');
            this.deleteLaunchConfigStub = sinon.stub(push, 'deleteLaunchConfig');
            this.updateAutoScalingStub = sinon.stub(push, 'updateAutoScaling');
        });
        
        describe('should exit with an error', function() {
            
            before(function() {
                push.options = {
                    config: true,
                    id: 'amd-1234abcd',
                    name: 'test'
                };
            });
            
            it('if neither --front nor --back was specified for the launch configuration', function() {
                push.createLaunchConfig();
                expect(this.exitWithErrorStub.called).to.be.true;
            });
            
        });
        
        describe('should exit with an error', function() {
            
            before(function() {
                push.options = {
                    config: true,
                    id: null,
                    name: 'test',
                    front: true
                };
            });
            
            it('if no image id was specified for the launch configuration', function() {
                push.createLaunchConfig();
                expect(this.exitWithErrorStub.called).to.be.true;
            });
            
        });
        
        describe('should exit with an error', function() {
            
            before(function() {
                push.options = {
                    config: true,
                    id: 'ami-1234abcd',
                    name: null,
                    front: true
                };
            });
            
            it('if no name was specified for the launch configuration', function() {
                push.createLaunchConfig();
                expect(this.exitWithErrorStub.called).to.be.true;
            });
            
        });
        
        describe('should delete', function() {
            
            it('an old launch configuration', function() {
                push.createLaunchConfig();
                expect(this.deleteLaunchConfigStub.called).to.be.true;
            });
            
        });
        
        describe('should create a new launch configuration', function() {
            
            before(function() {
                this.getBetaNameStub = sinon.stub(push, 'getBetaName', function() {return 'beta20151130-test'});
                push.options = {
                    config: true,
                    id: 'ami-1234abcd',
                    name: 'test',
                    front: true
                };
            });
            
            it('for the front end', function() {
                push.createLaunchConfig();
                expect(this.spawnAWSStub.calledWith(['autoscaling', 'create-launch-configuration', '--image-id', 'ami-1234abcd', '--instance-type', 'm3.medium', '--launch-configuration-name', 'beta20151130-test', '--instance-monitoring', 'Enabled=false', '--block-device-mappings', '[{\"DeviceName\":\"/dev/sda1\",\"Ebs\":{\"DeleteOnTermination\":true,\"VolumeSize\":8,\"VolumeType\":\"gp2\"}}]', '--security-groups', 'sg-10d6207a', '--key-name', 'soc-keypair-2016-10'])).to.be.true;
            });
            
            after(function() {
                this.getBetaNameStub.restore();
            });
            
        });
        
        describe('should create a new launch configuration', function() {
            
            before(function() {
                this.getBetaNameStub = sinon.stub(push, 'getBetaName', function() {return 'beta20151130-test'});
                push.options = {
                    config: true,
                    id: 'ami-1234abcd',
                    name: 'test',
                    back: true
                };
            });
            
            it('for the back end', function() {
                push.createLaunchConfig();
                expect(this.spawnAWSStub.calledWith(['autoscaling', 'create-launch-configuration', '--image-id', 'ami-1234abcd', '--instance-type', 'm3.medium', '--launch-configuration-name', 'beta20151130-test', '--instance-monitoring', 'Enabled=false', '--block-device-mappings', '[{\"DeviceName\":\"/dev/xvda\",\"Ebs\":{\"DeleteOnTermination\":true,\"VolumeSize\":8,\"VolumeType\":\"gp2\"}}]', '--security-groups', 'sg-10d6207a', '--key-name', 'soc-keypair-2016-10'])).to.be.true;
            });
            
            after(function() {
                this.getBetaNameStub.restore();
            });
            
        });
        
        describe('should update auto scaling', function() {
            
            before(function() {
                this.getBetaNameStub = sinon.stub(push, 'getBetaName', function() {return 'beta20151130-test'});
                push.options = {
                    config: true,
                    id: 'ami-1234abcd',
                    name: 'test',
                    front: true
                };
            });
            
            it('by calling updateAutoScaling()', function() {
                push.createLaunchConfig();
                expect(this.updateAutoScalingStub.called).to.be.true;
            });
            
            after(function() {
                this.getBetaNameStub.restore();
            });
            
        });
        
        afterEach(function() {
            this.exitWithErrorStub.restore();
            this.deleteLaunchConfigStub.restore();
            this.updateAutoScalingStub.restore();
            push.options = {};
        });
        
    });
    
    describe('updateAutoScaling()', function() {
        
        beforeEach(function() {
            this.exitWithErrorStub = sinon.stub(push, 'exitWithError');
            this.checkLoadBalancerStub = sinon.stub(push, 'checkLoadBalancer');
        });
        
        describe('should exit with an error', function() {
            
            before(function() {
                push.options = {
                    update: true,
                    name: 'test'
                };
            });
            
            it('if neither --front nor --back was specified for the launch configuration', function() {
                push.updateAutoScaling();
                expect(this.exitWithErrorStub.called).to.be.true;
            });
            
        });
        
        describe('should exit with an error', function() {
            
            before(function() {
                push.options = {
                    update: true,
                    name: null,
                    front: true
                };
            });
            
            it('if no name was specified for the launch configuration', function() {
                push.updateAutoScaling();
                expect(this.exitWithErrorStub.called).to.be.true;
            });
            
        });
        
        describe('should update auto scaling to use a new launch confirugation and a desired capacity of 16', function() {
            
            before(function() {
                this.getBetaNameStub = sinon.stub(push, 'getBetaName', function() {return 'beta20151130-test'});
                push.options = {
                    update: true,
                    name: 'test',
                    front: true
                };
            });
            
            it('for the front end', function() {
                push.updateAutoScaling();
                expect(this.spawnAWSStub.calledWith(['autoscaling', 'update-auto-scaling-group', '--auto-scaling-group-name', 'betaProduction', '--launch-configuration-name', 'beta20151130-test', '--desired-capacity', '16'])).to.be.true;
            });
            
            after(function() {
                this.getBetaNameStub.restore();
            });
            
        });
        
        describe('should start checking the load balancer', function() {
            
            before(function() {
                this.getBetaNameStub = sinon.stub(push, 'getBetaName', function() {return 'beta20151130-test'});
                push.options = {
                    update: true,
                    name: 'test',
                    front: true
                };
            });
            
            it('by calling checkLoadBalancer()', function() {
                push.updateAutoScaling();
                expect(this.checkLoadBalancerStub.called).to.be.true;
            });
            
            after(function() {
                this.getBetaNameStub.restore();
            });
            
        });
        
        afterEach(function() {
            this.exitWithErrorStub.restore();
            this.checkLoadBalancerStub.restore();
            push.options = {};
        });
        
    });
    
    describe('checkLoadBalancer()', function() {
        
        describe('should exit with an error', function() {
            
            before(function() {
                this.exitWithErrorStub = sinon.stub(push, 'exitWithError');
                this.getOutputStub = sinon.stub(push, 'getOutput', function() {return {}});
                this.allInServiceStub = sinon.stub(push, 'allInService', function() {return true});
            });
            
            it('if neither --front nor --back was specified', function() {
                push.checkLoadBalancer();
                expect(this.exitWithErrorStub.called).to.be.true;
            });
            
            after(function() {
                this.exitWithErrorStub.restore();
                this.getOutputStub.restore();
                this.allInServiceStub.restore();
            });
            
        });
        
    });
    
    describe('allInService()', function() {
        
        describe('should return false', function() {
            
            it('if there are less than 12 instances', function() {
                var instances = [];
                expect(push.allInService(instances)).not.to.be.true;
            });
            
            it('when one or more instances does not have the \'InService\' state', function() {
                var instances = [
                    {State: 'InService'}, {State: 'InService'}, {State: 'InService'}, {State: 'InService'}, {State: 'InService'}, {State: 'InService'}, 
                    {State: 'InService'}, {State: 'InService'}, {State: 'InService'}, {State: 'InService'}, {State: 'InService'}, {State: 'OutOfService'}
                ];
                expect(push.allInService(instances)).not.to.be.true;
            });
            
        });
        
        describe('should return true', function() {
            
            it('when all instances have the \'InService\' state', function() {
                var instances = [
                    {State: 'InService'}, {State: 'InService'}, {State: 'InService'}, {State: 'InService'}, {State: 'InService'}, {State: 'InService'}, 
                    {State: 'InService'}, {State: 'InService'}, {State: 'InService'}, {State: 'InService'}, {State: 'InService'}, {State: 'InService'}
                ];
                expect(push.allInService(instances)).to.be.true;
            });
            
        });
        
    });
    
    describe('getOldInstanceIds()', function() {
        
        before(function() {
            this.getBetaNameStub = sinon.stub(push, 'getBetaName', function() {return 'beta20151202-FrontPush4'});
        });
        
        it('should return an array of the old instance IDs', function() {
            var instances = [
                {
                    LaunchConfigurationName: 'beta20151202-FrontPush3',
                    InstanceId: '3a'
                },
                {
                    LaunchConfigurationName: 'beta20151202-FrontPush4',
                    InstanceId: '4a'
                },
                {
                    LaunchConfigurationName: 'beta20151202-FrontPush4',
                    InstanceId: '4b'
                },
                {
                    LaunchConfigurationName: 'beta20151202-FrontPush3',
                    InstanceId: '3b'
                }
            ];
            var result = push.getOldInstanceIds(instances);
            expect(result[0]).to.equal('3a');
            expect(result[1]).to.equal('3b');
        });
        
        after(function() {
            this.getBetaNameStub.restore();
        });
        
    });
    
    describe('terminate()', function() {
        
        beforeEach(function() {
            this.exitWithErrorStub = sinon.stub(push, 'exitWithError');
            this.getBetaNameStub = sinon.stub(push, 'getBetaName', function() {return 'beta20151202-FrontPush4'});
            this.getOutputStub = sinon.stub(push, 'getOutput', function() {
                return {
                    AutoScalingGroups: [
                        {
                            Instances: [
                                {
                                    LaunchConfigurationName: 'beta20151202-FrontPush3',
                                    InstanceId: '3a'
                                },
                                {
                                    LaunchConfigurationName: 'beta20151202-FrontPush4',
                                    InstanceId: '4a'
                                },
                                {
                                    LaunchConfigurationName: 'beta20151202-FrontPush4',
                                    InstanceId: '4b'
                                },
                                {
                                    LaunchConfigurationName: 'beta20151202-FrontPush3',
                                    InstanceId: '3b'
                                }
                            ]
                        }
                    ]
                }
            });
        });
        
        describe('should exit with an error', function() {
            
            before(function() {
                push.options = {
                    name: 'FrontPush4',
                    term: true
                };
            });
            
            it('if neither --front nor --back was specified', function() {
                push.terminate();
                expect(this.exitWithErrorStub.called).to.be.true;
            });
            
        });
        
        describe('should exit with an error', function() {
            
            before(function() {
                push.options = {
                    front: true,
                    term: true
                };
            });
            
            it('if --name was not specified', function() {
                push.terminate();
                expect(this.exitWithErrorStub.called).to.be.true;
            });
            
        });
        
        describe('should get a list of old instance IDs', function() {
            
            before(function() {
                push.options = {
                    name: 'FrontPush4',
                    term: true,
                    front: true
                };
            });
            
            it('for the front end', function() {
                push.terminate();
                expect(this.spawnAWSStub.calledWith(['autoscaling', 'describe-auto-scaling-groups', '--auto-scaling-group-name', 'betaProduction'])).to.be.true;
            });
            
        });
        
        describe('should get a list of old instance IDs', function() {
            
            before(function() {
                push.options = {
                    name: 'FrontPush4',
                    term: true,
                    back: true
                };
            });
            
            it('for the back end', function() {
                push.terminate();
                expect(this.spawnAWSStub.calledWith(['autoscaling', 'describe-auto-scaling-groups', '--auto-scaling-group-name', 'betaProdBackend'])).to.be.true;
            });
            
        });
        
        describe('should terminate', function() {
            
            before(function() {
                push.options = {
                    name: 'FrontPush4',
                    term: true,
                    front: true
                };
            });
            
            it('old instances', function() {
                push.terminate();
                expect(this.spawnAWSStub.calledWith(['autoscaling', 'terminate-instance-in-auto-scaling-group', '--instance-id', '3a', '--should-decrement-desired-capacity'])).to.be.true;
                expect(this.spawnAWSStub.calledWith(['autoscaling', 'terminate-instance-in-auto-scaling-group', '--instance-id', '3b', '--should-decrement-desired-capacity'])).to.be.true;
            });
            
        });
        
        afterEach(function() {
            this.exitWithErrorStub.restore();
            this.getBetaNameStub.restore();
            this.getOutputStub.restore();
            push.options = {};
        });
        
    });
    
    describe('getVersionTextUrl()', function() {
        
        it('should return \'https://rob.socrative.com/version.txt\' when the environment is \'rob\'', function() {
            push.options.env = 'rob';
            expect(push.getVersionTextUrl()).to.equal('https://rob.socrative.com/version.txt');
        });
        
        it('should return \'https://b.socrative.com/version.txt\' when the environment is \'prod\'', function() {
            push.options.env = 'prod';
            expect(push.getVersionTextUrl()).to.equal('https://b.socrative.com/version.txt');
        });
        
        afterEach(function() {
            push.options = {};
        });
        
    });
    
    describe('getNewFrontEndVersion()', function() {
        
        beforeEach(function() {
            this.getVersionTextUrlStub = sinon.stub(push, 'getVersionTextUrl');
            this.requestStub = sinon.stub(push, 'request');
            this.deployFrontEndStub = sinon.stub(push, 'deployFrontEnd');
            this.exitWithErrorStub = sinon.stub(push, 'exitWithError');
        });
        
        describe('when --env was not specified', function() {
            
            before(function() {
                push.options = {
                    front: true
                };
            });
            
            it('should exit with an error', function() {
                push.getNewFrontEndVersion();
                expect(this.exitWithErrorStub.called).to.be.true;
            });
            
        });
        
        describe('when an unknown environment was specified', function() {
            
            before(function() {
                push.options = {
                    front: true,
                    env: 'bogus'
                };
            });
            
            it('should exit with an error', function() {
                push.getNewFrontEndVersion();
                expect(this.exitWithErrorStub.called).to.be.true;
            });
            
        });
        
        describe('when --version was not specified', function() {
            
            before(function() {
                push.options = {
                    front: true,
                    env: 'rob'
                };
            });
            
            describe('when --revert was passed', function() {
                
                before(function() {
                    push.options.revert = true;
                });
                
                it('should exit with an error', function() {
                    push.getNewFrontEndVersion();
                    expect(this.exitWithErrorStub.called).to.be.true;
                });
                
            });
            
            describe('when --revert was not passed', function() {
                
                it('should look up the current version by calling request()', function() {
                    push.getNewFrontEndVersion();
                });
                
            });
            
        });
        
        describe('when --version was specified', function() {
            
            describe('and --revert was passed', function() {
                
                before(function() {
                    this.revertFrontEndStub = sinon.stub(push, 'revertFrontEnd');
                    push.options = {
                        front: true,
                        env: 'rob',
                        version: '2.2.22',
                        revert: true
                    };
                });
                
                it('should revert', function() {
                    push.getNewFrontEndVersion();
                    expect(this.revertFrontEndStub.called).to.be.true;
                });
                
                after(function() {
                    this.revertFrontEndStub.restore();
                });
                
            });
            
            describe('and --revert was not passed', function() {
                
                before(function() {
                    push.options = {
                        front: true,
                        env: 'rob',
                        version: '2.2.22'
                    };
                });
                
                it('should revert', function() {
                    push.getNewFrontEndVersion();
                    expect(this.deployFrontEndStub.called).to.be.true;
                });
                
            });
            
        });
        
        afterEach(function() {
            this.getVersionTextUrlStub.restore();
            this.requestStub.restore();
            this.deployFrontEndStub.restore();
            this.exitWithErrorStub.restore();
            push.options = {};
        });
        
    });
    
    describe('revertFrontEnd()', function() {
        
        before(function() {
            this.writeWebFilesStub = sinon.stub(push, 'writeWebFiles');
        });
        
        it('should write the web files', function() {
            push.revertFrontEnd();
            expect(this.writeWebFilesStub.called).to.be.true;
        });
        
        after(function() {
            this.writeWebFilesStub.restore();
        });
        
    });
    
    describe('deployFrontEnd()', function() {
        
        beforeEach(function() {
            this.writeWebFilesStub = sinon.stub(push, 'writeWebFiles');
            this.chdirStub = sinon.stub(process, 'chdir');
            this.exitWithErrorStub = sinon.stub(push, 'exitWithError');
        });
        
        describe('when the webpack build succeeds', function() {
            
            before(function() {
                this.doWebpackBuildStub = sinon.stub(push, 'doWebpackBuild', function() {return {status: 0}});
                this.syncStaticFilesStub = sinon.stub(push, 'syncStaticFiles');
            });
            
            it('should write the web files', function() {
                push.deployFrontEnd();
                expect(this.doWebpackBuildStub.called).to.be.true;
                expect(this.writeWebFilesStub.called).to.be.true;
            });

            after(function () {
                this.doWebpackBuildStub.restore();
                this.syncStaticFilesStub.restore();
            });
            
        });

        describe('when the webpack build fails', function () {

            before(function () {
                this.doWebpackBuildStub = sinon.stub(push, 'doWebpackBuild', function () {return {status: 1}});
                this.syncStaticFilesStub = sinon.stub(push, 'syncStaticFiles');
            });

            it('should exit with an error', function () {
                push.deployFrontEnd();
                expect(this.doWebpackBuildStub.called).to.be.true;
                expect(this.exitWithErrorStub.called).to.be.true;
            });

            after(function () {
                this.doWebpackBuildStub.restore();
                this.syncStaticFilesStub.restore();
            });

        });
        
        

        afterEach(function () {
            this.writeWebFilesStub.restore();
            this.chdirStub.restore();
            this.exitWithErrorStub.restore();
        });
        
    });
    
    describe('doWebpackBuild()', function() {
        
        before(function() {
            this.chdirStub = sinon.stub(process, 'chdir');
            this.spawnWebpackStub = sinon.stub(push, 'spawnWebpack', function () {return {status: 0}});
            process.env.STATIC_URL_PATH = '';
            push.options = {
                front: true,
                env: 'rob',
                version: '2.2.36'
            };
            push.doWebpackBuild();
        });

        it('should set the STATIC_URL_PATH environment variable', function () {
            expect(process.env.STATIC_URL_PATH).to.equal('https://rob-assets.socrative.com/2.2.36/js/');
        });
        
        it('should change to the webpack directory', function() {
            expect(this.chdirStub.calledWith('../webpack')).to.be.true;
        });
        
        it('should spawn the actual webpack build', function() {
            expect(this.spawnWebpackStub.called).to.be.true;
        });
        
        after(function() {
            this.chdirStub.restore();
            this.spawnWebpackStub.restore();
            push.options = {};
        });
        
    });
    
    describe('writeWebFiles()', function() {
        
        before(function() {
            this.setTemplatesDirStub = sinon.stub(templates, 'setTemplatesDir');
            this.setDebugStub = sinon.stub(templates, 'setDebug');
            this.writeIndexFileStub = sinon.stub(push, 'writeIndexFile');
            this.writeVersionFileStub = sinon.stub(push, 'writeVersionFile');
            push.options = {
                env: 'rob',
                version: '2.2.36'
            };
            this.context = push.getContext();
            push.writeWebFiles();
        });
        
        it('should set the template directory', function() {
            expect(this.setTemplatesDirStub.calledWith('../../common/templates')).to.be.true;
        });
        
        it('should disable template debugging', function() {
            expect(this.setDebugStub.calledWith(false)).to.be.true;
        });
        
        it('should write the dual login index.html file', function() {
            expect(this.writeIndexFileStub.calledWith('../../common/templates/index.html', this.context, '../../socrative/static/html/index.html')).to.be.true;
        });

        it('should write the student login index.html file', function () {
            expect(this.writeIndexFileStub.calledWith('../../common/templates/index.html', this.context, '../../socrative/static/html/login/student/index.html')).to.be.true;
        });
        
        it('should write the teacher login index.html file', function () {
            expect(this.writeIndexFileStub.calledWith('../../common/templates/index.html', this.context, '../../socrative/static/html/login/teacher/index.html')).to.be.true;
        });

        it('should write the student index.html file', function () {
            expect(this.writeIndexFileStub.calledWith('../../common/templates/student.html', this.context, '../../socrative/static/html/student/index.html')).to.be.true;
        });

        it('should write the teacher index.html file', function () {
            expect(this.writeIndexFileStub.calledWith('../../common/templates/teacher.html', this.context, '../../socrative/static/html/teacher/index.html')).to.be.true;
        });

        it('should write the version.txt file', function () {
            expect(this.writeVersionFileStub.calledWith('../../socrative/static/html/version.txt')).to.be.true;
        });
        
        after(function() {
            this.setTemplatesDirStub.restore();
            this.setDebugStub.restore();
            this.writeIndexFileStub.restore();
            this.writeVersionFileStub.restore();
            push.options = {};
        });
        
    });
    
    describe('getContext()', function() {
        
        describe('when the environment is \'rob\'', function() {
            
            before(function() {
                push.options = {
                    env: 'rob'
                };
                this.context = push.getContext();
            });
            
            it('should set STATIC_URL to \'https://rob-assets.socrative.com/\'', function() {
                expect(this.context.STATIC_URL).to.equal('https://rob-assets.socrative.com/');
            });
            
            it('should set BACKEND_HOST to \'https://rob-api.socrative.com\'', function() {
                expect(this.context.BACKEND_HOST).to.equal('https://rob-api.socrative.com');
            });

            it('should set TEACHER_SERVICE to \'https://teacher-dev.socrative.com\'', function() {
                expect(this.context.TEACHER_SERVICE).to.equal('https://teacher-dev.socrative.com');
            });
            
            it('should set PUBNUB_SUBSCRIBE_KEY to \'sub-c-bd4e8b32-c63a-11e3-b872-02ee2ddab7fe\'', function() {
                expect(this.context.PUBNUB_SUBSCRIBE_KEY).to.equal('sub-c-bd4e8b32-c63a-11e3-b872-02ee2ddab7fe');
            });
            
            it('should set SEGMENT_IO_KEY to \'shr0uvtu8w\'', function() {
                expect(this.context.SEGMENT_IO_KEY).to.equal('shr0uvtu8w');
            });
            
        });
        
        describe('when the environment is \'prod\'', function() {
            
            before(function() {
                push.options = {
                    env: 'prod'
                };
                this.context = push.getContext();
            });
            
            it('should set STATIC_URL to \'https://assets.socrative.com/\'', function() {
                expect(this.context.STATIC_URL).to.equal('https://assets.socrative.com/');
            });
            
            it('should set BACKEND_HOST to \'https://api.socrative.com\'', function() {
                expect(this.context.BACKEND_HOST).to.equal('https://api.socrative.com');
            });

            it('should set TEACHER_SERVICE to \'https://teacher.socrative.com\'', function() {
                expect(this.context.TEACHER_SERVICE).to.equal('https://teacher.socrative.com');
            });
            
        });
        
        afterEach(function() {
            push.options = {};
        });
        
    });
    
    describe('syncFiles()', function() {
        
        beforeEach(function() {
            push.indexFiles = 5;
            this.syncStaticFilesStub = sinon.stub(push, 'syncStaticFiles');
            this.syncWebFilesStub = sinon.stub(push, 'syncWebFiles');
        });
        
        describe('when the --revert option was passed', function() {
            
            before(function() {
                push.options = {
                    revert: true
                };
            });
            
            it('should only sync the web files', function() {
                push.syncFiles();
                expect(this.syncStaticFilesStub.called).not.to.be.true;
                expect(this.syncWebFilesStub.called).to.be.true;
            });

        });
        
        describe('when the --revert option was not passed', function() {
            
            it('should sync web files', function() {
                push.syncFiles();
                expect(this.syncWebFilesStub.called).to.be.true;
            });

        });

        afterEach(function () {
            delete push.indexFiles;
            this.syncStaticFilesStub.restore();
            this.syncWebFilesStub.restore();
            push.options = {};
        });
        
    });
    
    describe('syncStaticFiles()', function() {
        
        before(function() {
            this.exitWithErrorStub = sinon.stub(push, 'exitWithError');
            push.options = {
                env: 'rob',
                version: '2.2.36'
            };
        });
        
        it('should call aws s3 sync', function() {
            push.syncStaticFiles();
            expect(this.spawnAWSStub.calledWith(['s3', 'sync', '../../socrative/static/2.2.6', 's3://socrative-static-rob/2.2.36'])).to.be.true;
        });

        after(function () {
            this.exitWithErrorStub.restore();
            push.options = {};
        });
        
    });
    
    describe('syncWebFiles()', function() {

        before(function () {
            this.exitWithErrorStub = sinon.stub(push, 'exitWithError');
            push.options = {
                env: 'rob'
            };
        });

        it('should call aws s3 sync', function () {
            push.syncWebFiles();
            expect(this.spawnAWSStub.calledWith(['s3', 'sync', '../../socrative/static/html', 's3://rob.socrative.com'])).to.be.true;
        });
        
        after(function () {
            this.exitWithErrorStub.restore();
            push.options = {};
        });
        
    });
    
});
