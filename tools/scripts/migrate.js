/*
    migrate.js - A tool for running migrations in elastic beanstalk environments.
    
                 This tool must be executed from the project root (usually /var/app/current in elastic beanstalk).
 */

'use strict';

let spawnSync = require('child_process').spawnSync,
    spawn = require('child_process').spawn,
    envVars = ['NODE_ENV', 'SOCRATIVE_DB_HOST', 'SOCRATIVE_DB_USERNAME', 'SOCRATIVE_DB_PASSWORD'];

let exitWithError = (message, status) => {
    process.stderr.write(`${message}\n`);
    process.exit(status || 1);
};

let setEnvVars = () => {
    if (process.argv.length != 3) {
        exitWithError('Usage: sudo node tools/scripts/migrate.js <up | down>');
    }
    
    let env = spawnSync('/opt/elasticbeanstalk/bin/get-config', ['environment']);
    
    if (env.status !== 0) {
        exitWithError('Error getting environment config: ' + env.stderr.toString(), env.status);
    }
    
    try {
        env = JSON.parse(env.stdout);
    } catch (error) {
        exitWithError('Error parsing environment config: ' + error);
    }
    
    for (let envVar of envVars) {
        if (!env[envVar]) {
            exitWithError('Error: ' + envVar + ' is not set.');
        } else {
            process.env[envVar] = env[envVar];
        }
    }
};

let runMigration = () => {
    let dbMigrate = spawn('db-migrate', [process.argv[2]]);
    dbMigrate.stdout.on('data', (data) => {
        process.stdout.write(data.toString());
    });
    dbMigrate.stderr.on('data', (data) => {
        process.stderr.write(data.toString());
    });
    dbMigrate.on('close', (code) => {
        if (code !== 0) {
            exitWithError('Error: db-migrate did not execute successfully.', code);
        }
    });
};

setEnvVars();
runMigration();
