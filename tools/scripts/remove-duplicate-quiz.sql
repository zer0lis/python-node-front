-- This file is not intended to be executed as a script. It is
-- a collection of the queries that must be executed to remove
-- the duplicate quiz described in SOC-1846.

-- Since we're dealing with corrupt data, or a possibly corrupt
-- quizzes_quiz table, each query will be executed manually and
-- separately so that the results can be carefully evaluated.

DELETE FROM quizzes_question_resources WHERE question_id IN (SELECT question_id FROM quizzes_question WHERE quiz_id = 10557462);

DELETE FROM quizzes_answer WHERE question_id IN (SELECT question_id FROM quizzes_question WHERE quiz_id = 10557462);

DELETE FROM quizzes_question WHERE quiz_id = 10557462;

BEGIN;
SET LOCAL enable_indexscan = off;
SET LOCAL enable_bitmapscan = off;
SET LOCAL enable_indexonlyscan = off;
DELETE FROM quizzes_quiz WHERE id = 10557462;
COMMIT;
