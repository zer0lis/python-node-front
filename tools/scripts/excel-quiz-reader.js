'use strict';

let xlsx = require('xlsx'),
    fs = require('fs');

/*  
    This is how we calculate the number of questions in the quiz:
        
        1. We call xlsx.utils.sheet_to_json(worksheet) to get an array of the rows in the spreadsheet.
        
        2. The xlsx package:
            (a) Skips the first row, because it's considered a header.
            (b) Counts each remaining row whose first cell is not empty.
        
    The result is that rows 3 and 6 are included in the count. However, since those
    rows are not questions, we subtract 2 from the length to get the total number
    of questions.
*/

process.on('message', (message) => {
    let result = {id: message.id};
    try {
        let workbook = xlsx.readFile(message.filename);
        result.worksheet = workbook.Sheets[workbook.SheetNames[0]];
        result.numQuestions = xlsx.utils.sheet_to_json(result.worksheet).length - 2;
    } catch (error) {
        result.error = error;
    } finally {
        process.send(result);
        fs.unlink(message.filename, () => {});
    }
});
