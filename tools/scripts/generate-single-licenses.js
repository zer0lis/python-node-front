/*
    A script to generate single licenses for all existing, non-bulk, PRO users.
    Only to be executed after licenses have been created for all bulk PRO users.
 */

'use strict';

process.env.NODE_CONFIG_DIR = '../../shared/config';
process.env.NODE_DEBUG = '';

let exitWithError = (message, status) => {
    process.stderr.write(`${message}\n`);
    process.exit(status || 1);
};

/*---------------------------------*/
/*    Set Environment Variables    */
/*---------------------------------*/

let spawnSync = require('child_process').spawnSync;

let env = spawnSync('/opt/elasticbeanstalk/bin/get-config', ['environment']);
if (env.status !== 0) {
    exitWithError(`Error getting environment config: ${env.stderr.toString()}`, env.status);
}

try {
    env = JSON.parse(env.stdout);
} catch (error) {
    exitWithError(`Error parsing environment config: ${error}`);
}

for (let envVar in env) {
    if (env.hasOwnProperty(envVar)) {
        process.env[envVar] = env[envVar];
    }
}

/*---------------------------------*/

let Client = require('pg').Client,
    Cursor = require('pg-cursor'),
    config = require('config').get('config'),
    licenseDao = require('../../shared/daos/LicenseDao'),
    licenseProvider = require('../../shared/providers/LicenseProvider'),
    activationProvider = require('../../shared/providers/ActivationProvider');

let client = new Client({
    database: config.db.name,
    host: config.db.host,
    password: config.db.password,
    port: config.db.port,
    user: config.db.username
});

client.connect((connectError) => {
    if (connectError) {
        exitWithError(`Error connecting to Postgres: ${connectError}`);
    }
});

let mainQuery = `
    SELECT u.id, u.email, u.organization_type, r.expiration_date
    FROM socrative_users_socrativeuser u
    JOIN socrative_users_teacherrole r ON r.user_id = u.id
    WHERE r.level = 1
    AND r.is_hidden is FALSE
    AND NOT EXISTS (
        SELECT 1
        FROM license_activations a
        WHERE u.id = a.user_id
    )
`;

let count = 0,
    total = 0;

let getTotal = async () => {
    try {
        let result = await client.query(
            `SELECT count(*)
             FROM (
                 ${mainQuery}
             ) AS free_users;`
        );
        total = result.rows[0].count;
        process.stdout.write(`Total number of users to be upgraded: ${total}\n`);
    } catch (error) {
        client.end();
        exitWithError(`Error getting total: ${error}`);
    }
};

let generateLicense = (cursor) => {
    let status = 0;
    cursor.read(1, async (readError, rows) => {
        let shouldExit = false;
        
        if (readError) {
            process.stderr.write(`Error reading the cursor: ${readError}\n`);
            status = 1;
            shouldExit = true;
        }
        
        if (!rows.length) {
            process.stdout.write(`License data generated for ${count} users\n`);
            shouldExit = true;
        }
        
        if (shouldExit) {
            client.end();
            process.exit(status);
        }
        
        let data = rows[0];
        
        let price = 4999;
        if (data.organization_type && data.organization_type.toLowerCase() === 'k12') {
            price = 2999;
        }
        
        let expirationDate = new Date(data.expiration_date),
            minExpDate = new Date('8/1/2017'),
            purchaseDate = new Date(data.expiration_date);
        
        if (expirationDate.getTime() < minExpDate.getTime()) {
            expirationDate = minExpDate;
        }
        
        purchaseDate.setFullYear(purchaseDate.getFullYear() - 1);
    
        let key = licenseProvider.createLicenseKey();
        
        let insertProps = {
            buyerId: data.id,
            key: key,
            expirationDate: expirationDate,
            couponId: null,
            autoRenew: false,
            years: 1,
            pricePerSeat: price,
            customerId: null,
            stripeResponse: {},
            seats: 1,
            totalPrice: price,
            purchaseDate: purchaseDate
        };
        
        let licenseId = await licenseDao.insertLicense(insertProps);
        
        if (!licenseId) {
            client.end();
            exitWithError(`Error creating a license for ${data.email}`);
        }
    
        let request = {
            body: {
                userId: data.id,
                key: key
            }
        };
        
        let result = await activationProvider.activateLicenseForUser(request);
        
        if (result.data.error) {
            client.end();
            exitWithError(`Error activating a license for ${data.email}: ${result.data.error}\n`);
        } else {
            count++;
            process.stdout.write(`Upgraded ${count} of ${total}\n`);
            generateLicense(cursor);
        }
    });
};

getTotal();
generateLicense(client.query(new Cursor(mainQuery)));
