/*
    A script to move stripe_charge_response data from the licenses table to the license_transactions table.
 */

'use strict';

process.env.NODE_CONFIG_DIR = '../../shared/config';
process.env.NODE_DEBUG = '';

let exitWithError = (message, status) => {
    process.stderr.write(`${message}\n`);
    process.exit(status || 1);
};

/*---------------------------------*/
/*    Set Environment Variables    */
/*---------------------------------*/

let spawnSync = require('child_process').spawnSync,
    env = spawnSync('/opt/elasticbeanstalk/bin/get-config', ['environment']);

if (env.status !== 0) {
    exitWithError(`Error getting environment config: ${env.stderr.toString()}`, env.status);
}

try {
    env = JSON.parse(env.stdout);
} catch (error) {
    exitWithError(`Error parsing environment config: ${error}`);
}

for (let envVar in env) {
    if (env.hasOwnProperty(envVar)) {
        process.env[envVar] = env[envVar];
    }
}

/*---------------------------------*/

let Client = require('pg').Client,
    Cursor = require('pg-cursor'),
    config = require('config').get('config'),
    count = 0;

let cursorClient = new Client({
    database: config.db.name,
    host: config.db.host,
    password: config.db.password,
    port: config.db.port,
    user: config.db.username
});

let updateClient = new Client({
    database: config.db.name,
    host: config.db.host,
    password: config.db.password,
    port: config.db.port,
    user: config.db.username
});

let connect = async () => {
    try {
        await cursorClient.connect();
        await updateClient.connect();
    } catch (error) {
        exitWithError(`Error connecting to database: ${error}`);
    }
};

let moveStripeChargeResponseData = (cursor) => {
    let status = 0;
    
    cursor.read(1, async (readError, rows) => {
        let shouldExit = false;
        
        if (readError) {
            process.stderr.write(`Error reading the cursor: ${readError}\n`);
            status = 1;
            shouldExit = true;
        }
        
        if (rows.length === 0) {
            process.stdout.write(`Stripe charge response data moved for ${count} licenses\n`);
            shouldExit = true;
        }
        
        if (shouldExit) {
            cursorClient.end();
            updateClient.end();
            process.exit(status);
        }
        
        let license = rows[0];
        
        try {
            let result = await updateClient.query(
                `UPDATE license_transactions
                 SET stripe_charge_response = $1
                 WHERE license_id = $2`,
                [license.stripe_charge_response, license.id]
            );
            
            if (!result || result.rowCount === 0) {
                exitWithError(`Unknown error moving Stripe charge response data for license id ${license.id}\n`);
            }
        } catch (error) {
            exitWithError(`Error moving Stripe charge response data for license id ${license.id}: ${error}\n`);
        }
        
        count++;
        process.stdout.write(`Stripe charge response data moved for license id ${license.id}\n`);
        moveStripeChargeResponseData(cursor);
    });
};

let main = async () => {
    await connect();
    
    moveStripeChargeResponseData(cursorClient.query(new Cursor(
        `SELECT id, stripe_charge_response
         FROM licenses
         WHERE stripe_charge_response NOT IN ('', '{}', 'nothing')`
    )));
};

main();
