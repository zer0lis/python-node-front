'use strict';

let CommandLineArgs = require('command-line-args'),
    CommandLineUsage = require('command-line-usage'),
    spawnSync = require('child_process').spawnSync;

class Deploy {
    
    constructor() {
        this.serviceNames = ['admin', 'teacher'];
        
        this.optionList = [
            {name: 'build', alias: 'b', type: Boolean, description: 'Build a zip archive for a Socrative service that can be deployed to elastic beanstalk (requires --service).'},
            {name: 'help', alias: 'h', type: Boolean, description: 'Display this message and exit.'},
            {name: 'service', alias: 's', type: String, description: `Name of a Socrative service. Valid service names are:\n${this.serviceNames.join('\n')}`, typeLabel: 'name'}
        ];
        
        this.usage = [
            {
                header: 'Socrative Deploy',
                content: 'A tool for building and deploying Socrative clients and services.'
            },
            {
                header: 'Options',
                optionList: this.optionList
            }
        ];
        
        this.options = CommandLineArgs(this.optionList);
    }
    
    prepareBuild() {
        this.setArchiveName();
        
        process.chdir('../../');
        
        spawnSync('mkdir', ['-p', `build/${this.archiveName}`]);
        spawnSync('mkdir', [`build/${this.archiveName}/clients`]);
        spawnSync('mkdir', [`build/${this.archiveName}/services`]);
        spawnSync('mkdir', [`build/${this.archiveName}/tools`]);
    
        spawnSync('cp', ['database.json', `build/${this.archiveName}`]);
        spawnSync('cp', ['package.json', `build/${this.archiveName}`]);
        
        spawnSync('cp', ['-r', 'migrations', `build/${this.archiveName}`]);
        spawnSync('cp', ['-r', 'shared', `build/${this.archiveName}`]);
        spawnSync('cp', ['-r', 'tools/scripts', `build/${this.archiveName}/tools`]);
        
        // Copy the service's .ebextensions to the root, then copy the service directory, then delete its nested .ebextensions:
        spawnSync('cp', ['-r', `services/${this.options.service}/.ebextensions`, `build/${this.archiveName}`]);
        spawnSync('cp', ['-r', `services/${this.options.service}`, `build/${this.archiveName}/services`]);
        spawnSync('rm', ['-rf', `build/${this.archiveName}/services/${this.options.service}/.ebextensions`]);
    }
    
    setArchiveName() {
        let date = new Date();
        this.archiveName = `${this.options.service}-${date.toISOString().replace('T', '-').replace(/:/g, '').substr(0, 17)}`;
    }
    
    buildAdmin() {
        process.chdir('clients/admin');
        spawnSync('gulp', ['build']);
        process.chdir('../../');
        spawnSync('cp', ['-r', 'clients/admin', `build/${this.archiveName}/clients`]);
    }
    
    createArchive() {
        process.chdir(`build/${this.archiveName}`);
        spawnSync('zip', ['-r', `../${this.archiveName}.zip`, '.']);
        spawnSync('rm', ['-rf', `../${this.archiveName}`]);
        console.log(`\nBuild success! Archive file is: build/${this.archiveName}.zip\n`);
    }
    
    execute() {
        if (this.options.help) {
            console.log(CommandLineUsage(this.usage));
            process.exit(0);
        }
        
        if (this.options.build) {
            if (!this.options.service) {
                this.exitWithError('Please specify which service to build (use --service).');
            }
            
            this.options.service = this.options.service.toLowerCase();
            
            if (!this.serviceNames.includes(this.options.service)) {
                this.exitWithError(`Invalid service name: ${this.options.service}`);
            }
    
            this.prepareBuild();
            
            if (this.options.service === 'admin') {
                this.buildAdmin();
            }
            
            this.createArchive();
        }
    }
    
    exitWithError(message) {
        console.error(`\n${message}\n`);
        process.exit(1);
    }
    
}

let deploy = new Deploy();
deploy.execute();
