#!/bin/bash

echo "START TIME: `date '+%m-%d-%Y %T.%s%3N'`" &> /home/ec2-user/crons/vacuum.log

psql -h $PROD_AURORA_CLUSTER_ENDPOINT -U $POSTGRES_USER $POSTGRES_DATABASE -c 'VACUUM (VERBOSE, ANALYZE)' &>> /home/ec2-user/crons/vacuum.log

echo "END TIME: `date '+%m-%d-%Y %T.%s%3N'`" &>> /home/ec2-user/crons/vacuum.log
