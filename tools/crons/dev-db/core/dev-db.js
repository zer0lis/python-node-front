let RDS = require('aws-sdk/clients/rds'),
    EC2 = require('aws-sdk/clients/ec2'),
    EB = require('aws-sdk/clients/elasticbeanstalk'),
    request = require('request-promise-native');

let rds = new RDS(),
    ec2 = new EC2(),
    eb = new EB();

let clusterCheckMinutes = 10,
    databaseCheckMinutes = 3;

class DevDatabase {
    
    execute(options) {
        devdb.options = options;
        
        if (devdb.isSet('create')) {
            devdb.create();
        } else if (devdb.isSet('delete')) {
            devdb.del();
        }
    }
    
    /**
     * Return true if an option was passed on the command line, false otherwise.
     * @param name The name of the command line option.
     * @returns {boolean}
     */
    isSet(name) {
        return devdb.options.hasOwnProperty(name);
    }
    
    /**
     * Print an error to the console and exit.
     * @param {string} message The error message to print to the console.
     */
    exitWithError(message) {
        devdb.log('ERROR', message);
        process.exit(1);
    }
    
    getTimestamp() {
        let now = new Date(),
            month = now.getMonth() + 1 < 10 ? `0${now.getMonth() + 1}` : now.getMonth() + 1,
            date = now.getDate() < 10 ? `0${now.getDate()}` : now.getDate(),
            year = now.getFullYear(),
            hours = now.getHours() < 10 ? `0${now.getHours()}` : now.getHours(),
            minutes = now.getMinutes() < 10 ? `0${now.getMinutes()}` : now.getMinutes(),
            seconds = now.getSeconds() < 10 ? `0${now.getSeconds()}` : now.getSeconds(),
            milliseconds = now.getMilliseconds();
        
        if (milliseconds < 10) {
            milliseconds = `00${milliseconds}`;
        } else if (milliseconds < 100) {
            milliseconds = `0${milliseconds}`;
        }
        
        return `${month}-${date}-${year} ${hours}:${minutes}:${seconds}.${milliseconds}`;
    }
    
    info(message) {
        devdb.log('INFO', message);
    }
    
    log(type, message) {
        let handle = type === 'ERROR' ? process.stderr : process.stdout;
        handle.write(`${devdb.getTimestamp()} [${type}] ${message}\n`);
    }
    
    async create() {
        devdb.info('Starting dev database cluster and instance creation.');
        
        let instances = [];
        
        try {
            instances = await devdb.getDBInstances();
        } catch (error) {
            devdb.exitWithError(error);
        }
        
        if (instances.length === 0) {
            devdb.exitWithError('Could not get database instances.');
        }
        
        for (let instance of instances) {
            if (instance.DBInstanceIdentifier === 'socrative-dev') {
                devdb.info('Dev database instance already exists. Nothing to do.');
                process.exit(0);
            }
        }
        
        devdb.info('Retrieving most recent prod snapshot.');
        
        let snapshots = [],
            latestSnapshot = null;
        
        try {
            snapshots = await devdb.getDBSnapshots();
        } catch (error) {
            devdb.exitWithError(error);
        }
        
        for (let snapshot of snapshots) {
            if (snapshot.Status === 'available') {
                snapshot.SnapshotCreateTime = new Date(snapshot.SnapshotCreateTime);
                
                if (Number.isNaN(snapshot.SnapshotCreateTime.getTime())) {
                    continue;
                }
                
                if (latestSnapshot === null) {
                    latestSnapshot = snapshot;
                    continue;
                }
                
                if (snapshot.SnapshotCreateTime.getTime() > latestSnapshot.SnapshotCreateTime.getTime()) {
                    latestSnapshot = snapshot;
                }
            }
        }
        
        if (latestSnapshot === null) {
            devdb.exitWithError('Could not get most recent prod snapshot.');
        }
        
        devdb.info(`Restoring prod snapshot "${latestSnapshot.DBClusterSnapshotIdentifier}" into dev cluster.`);
        
        try {
            await devdb.restoreSnapshot(latestSnapshot.DBClusterSnapshotIdentifier);
        } catch (error) {
            devdb.exitWithError(error);
        }
        
        devdb.info('Checking status of dev cluster.');
        
        devdb.checkClusterStatus();
    }
    
    getDBInstances() {
        return new Promise((resolve, reject) => {
            rds.describeDBInstances({}, (error, data) => {
                if (error) {
                    reject(error);
                } else {
                    if (data && data.DBInstances) {
                        resolve(data.DBInstances);
                    } else {
                        reject('Invalid data from rds.describeDBInstances()');
                    }
                }
            });
        });
    }
    
    getDBSnapshots() {
        return new Promise((resolve, reject) => {
            let params = {
                DBClusterIdentifier: 'socrative-cluster',
                SnapshotType: 'automated'
            };
            
            rds.describeDBClusterSnapshots(params, (error, data) => {
                if (error) {
                    reject(error);
                } else {
                    if (data && data.DBClusterSnapshots) {
                        resolve(data.DBClusterSnapshots);
                    } else {
                        reject('Invalid data from rds.describeDBClusterSnapshots()');
                    }
                }
            });
        });
    }
    
    restoreSnapshot(snapshotIdentifier) {
        return new Promise((resolve, reject) => {
            let params = {
                DBClusterIdentifier: 'socrative-dev-cluster',
                DBSubnetGroupName: 'socrative-vpc-database-subnet-group',
                Engine: 'aurora-postgresql',
                SnapshotIdentifier: snapshotIdentifier,
                VpcSecurityGroupIds: ['sg-c6db2bb3']
            };
            
            rds.restoreDBClusterFromSnapshot(params, (error, data) => {
                if (error) {
                    reject(error);
                } else {
                    if (data && data.DBCluster) {
                        resolve();
                    } else {
                        reject('Invalid data from rds.restoreDBClusterFromSnapshot()');
                    }
                }
            });
        });
    }
    
    async checkClusterStatus() {
        let clusters = [];
        
        try {
            clusters = await devdb.getDBClusters();
        } catch (error) {
            devdb.exitWithError(error);
        }
        
        if (clusters.length === 0) {
            devdb.exitWithError('Could not get database clusters.');
        }
        
        let clusterExists = false;
        
        for (let cluster of clusters) {
            if (cluster.DBClusterIdentifier === 'socrative-dev-cluster') {
                clusterExists = true;
                
                if (cluster.Status !== 'available') {
                    devdb.info('Dev cluster not available yet.');
                    devdb.info(`Checking again in ${clusterCheckMinutes} minutes...`);
                    
                    await devdb.sleep(1000 * 60 * clusterCheckMinutes);
                    
                    clusterCheckMinutes--;
                    
                    if (clusterCheckMinutes === 1) {
                        clusterCheckMinutes = 2;
                    }
                    
                    devdb.checkClusterStatus();
                } else {
                    devdb.info('Creating dev database instance.');
                    
                    try {
                        await devdb.createDBInstance();
                    } catch (error) {
                        devdb.exitWithError(error);
                    }
                    
                    devdb.info('Checking status of dev database instance.');
                    
                    devdb.checkDBInstanceStatus();
                }
            }
        }
        
        if (!clusterExists) {
            devdb.exitWithError('There is no socrative-dev-cluster.');
        }
    }
    
    createDBInstance() {
        let params = {
            AutoMinorVersionUpgrade: false,
            DBClusterIdentifier: 'socrative-dev-cluster',
            DBInstanceClass: 'db.r4.large',
            DBInstanceIdentifier: 'socrative-dev',
            DBSubnetGroupName: 'socrative-vpc-database-subnet-group',
            Engine: 'aurora-postgresql'
        };
        
        return new Promise((resolve, reject) => {
            rds.createDBInstance(params, (error, data) => {
                if (error) {
                    reject(error);
                } else {
                    if (data && data.DBInstance) {
                        resolve();
                    } else {
                        reject('Invalid data from rds.createDBInstance()');
                    }
                }
            });
        });
    }
    
    async checkDBInstanceStatus() {
        let instances = [];
        
        try {
            instances = await devdb.getDBInstances();
        } catch (error) {
            devdb.exitWithError(error);
        }
        
        if (instances.length === 0) {
            devdb.exitWithError('Could not get database instances.');
        }
        
        for (let instance of instances) {
            if (instance.DBInstanceIdentifier === 'socrative-dev') {
                if (instance.DBInstanceStatus !== 'available') {
                    devdb.info('Dev database instance not available yet.');
                    devdb.info(`Checking again in ${databaseCheckMinutes} minutes...`);
                    
                    await devdb.sleep(1000 * 60 * databaseCheckMinutes);
                    
                    databaseCheckMinutes--;
                    
                    if (databaseCheckMinutes === 1) {
                        databaseCheckMinutes = 2;
                    }
                    
                    devdb.checkDBInstanceStatus();
                } else {
                    devdb.info('Rebooting dev backend instance.');
                    
                    try {
                        await devdb.rebootBackendInstance();
                    } catch (error) {
                        devdb.exitWithError(error);
                    }
                    
                    try {
                        let environmentNames = await this.getEnvironmentNames();
                        
                        devdb.info('Restarting app server for dev teacher service.');
                        await devdb.restartAppServer(environmentNames.teacher);
                        
                        devdb.info('Restarting app server for dev admin service.');
                        await devdb.restartAppServer(environmentNames.admin);
                    } catch (error) {
                        devdb.exitWithError(error);
                    }
                    
                    devdb.info('Waiting 5 minutes for API instances to restart...');
                    
                    await devdb.sleep(1000 * 60 * 5);
                    
                    await devdb.sendToSlack('Dev environment should be up and running for the day.');
                    
                    devdb.info('Done!');
                    
                    process.exit(0);
                }
            }
        }
    }
    
    async sendToSlack(message) {
        await request.post('https://slack.com/api/chat.postMessage', {
            form: {
                as_user: true,
                channel: '#socrative-deployment',
                text: message,
                token: 'xoxb-303615101713-cSWxIMSotTcAodpP3sc4lF6w'
            }
        });
    }
    
    rebootBackendInstance() {
        return new Promise((resolve, reject) => {
            ec2.rebootInstances({InstanceIds: ['i-0a16c084fef65ad1e']}, (error, data) => {
                if (error) {
                    reject(error);
                } else {
                    if (data) {
                        resolve();
                    } else {
                        reject('Invalid data from ec2.rebootInstances()');
                    }
                }
            });
        });
    }
    
    getEnvironmentNames() {
        return new Promise((resolve, reject) => {
            eb.describeEnvironments({}, (error, data) => {
                if (error) {
                    reject(error);
                } else {
                    if (data && data.Environments && data.Environments.length > 0) {
                        let environmentNames = {admin: null, teacher: null};
                        
                        for (let environment of data.Environments) {
                            if (/admin-dev/.test(environment.CNAME)) {
                                environmentNames.admin = environment.EnvironmentName;
                            } else if (/teacher-dev/.test(environment.CNAME)) {
                                environmentNames.teacher = environment.EnvironmentName;
                            }
                        }
                        
                        if (!environmentNames.admin) {
                            reject('Could not get dev admin service environment name.');
                        } else if (!environmentNames.teacher) {
                            reject('Could not get dev teacher service environment name.');
                        } else {
                            resolve(environmentNames);
                        }
                    } else {
                        reject('Invalid data from eb.describeEnvironments()');
                    }
                }
            });
        });
    }
    
    restartAppServer(environmentName) {
        return new Promise((resolve, reject) => {
            eb.restartAppServer({EnvironmentName: environmentName}, (error, data) => {
                if (error) {
                    reject(error);
                } else {
                    if (data) {
                        resolve();
                    } else {
                        reject('Invalid data from eb.restartAppServer()');
                    }
                }
            });
        });
    }
    
    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    
    getDBClusters() {
        let params = {
            DBClusterIdentifier: 'socrative-dev-cluster'
        };
        
        return new Promise((resolve, reject) => {
            rds.describeDBClusters(params, (error, data) => {
                if (error) {
                    reject(error);
                } else {
                    if (data && data.DBClusters) {
                        resolve(data.DBClusters);
                    } else {
                        reject('Invalid data from rds.describeDBClusters()');
                    }
                }
            });
        });
    }
    
    async del() {
        devdb.info('Announcing dev database shutdown.');
        
        await devdb.sendToSlack('Dev database going down in 5 minutes unless someone stops me...');
        
        devdb.info('Waiting 5 minutes before shutdown...');
        
        await devdb.sleep(1000 * 60 * 5);
        
        try {
            // If a file named "keep-dev-running" exists in the same directory as this dev-db.js
            // file (i.e. the "core" directory), delete the "keep-dev-running" file and exit.
            
            fs.unlinkSync(`${__dirname}/keep-dev-running`);
            
            devdb.info('Dev database should keep running. Nothing to do.');
            
            await devdb.sendToSlack('Dev database will keep running.');
            
            process.exit(0);
        } catch (error) {
            devdb.info('Deleting dev database.');
        }
        
        let instances = [];
        
        try {
            instances = await devdb.getDBInstances();
        } catch (error) {
            devdb.exitWithError(error);
        }
        
        if (instances.length === 0) {
            devdb.exitWithError('Could not get database instances.');
        }
        
        let devDatabaseRunnning = false;
        
        for (let instance of instances) {
            if (instance.DBInstanceIdentifier === 'socrative-dev') {
                devDatabaseRunnning = true;
                break;
            }
        }
        
        if (!devDatabaseRunnning) {
            devdb.info('Dev database does not exist. Nothing to delete.');
            process.exit(0);
        }
        
        try {
            await devdb.deleteDBInstance();
        } catch (error) {
            devdb.exitWithError(error);
        }
        
        devdb.info('Deleting dev cluster.');
        
        try {
            await devdb.deleteCluster();
        } catch (error) {
            devdb.exitWithError(error);
        }
        
        devdb.info('Done!');
        
        process.exit(0);
    }
    
    deleteDBInstance() {
        let params = {
            DBInstanceIdentifier: 'socrative-dev',
            SkipFinalSnapshot: true
        };
        
        return new Promise((resolve, reject) => {
            rds.deleteDBInstance(params, (error, data) => {
                if (error) {
                    reject(error);
                } else {
                    if (data) {
                        resolve();
                    } else {
                        reject('Invalid data from rds.deleteDBInstance()');
                    }
                }
            });
        });
    }
    
    deleteCluster() {
        let params = {
            DBClusterIdentifier: 'socrative-dev-cluster',
            SkipFinalSnapshot: true
        };
        
        return new Promise((resolve, reject) => {
            rds.deleteDBCluster(params, (error, data) => {
                if (error) {
                    reject(error);
                } else {
                    if (data) {
                        resolve();
                    } else {
                        reject('Invalid data from rds.deleteDBCluster()');
                    }
                }
            });
        });
    }
    
}

let devdb = new DevDatabase();
module.exports = devdb;
