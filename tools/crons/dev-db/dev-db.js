let CommandLineArgs = require('command-line-args'),
    CommandLineUsage = require('command-line-usage'),
    devdb = require('./core/dev-db.js');

let optionDefinitions = [
    {name: 'help', alias: 'h', type: Boolean, description: 'Display this message and exit'},
    {name: 'create', alias: 'c', type: Boolean, description: 'Attempt to create the dev database'},
    {name: 'delete', alias: 'd', type: Boolean, description: 'Attempt to delete the dev database'}
];

let options = CommandLineArgs(optionDefinitions);

if (options.help) {
    console.log(CommandLineUsage([
        {
            header: 'Socrative Dev Database',
            content: 'A tool for managing the Socrative dev database.'
        },
        {
            header: 'Options',
            optionList: optionDefinitions
        }
    ]));
    
    process.exit(0);
}

devdb.execute(options);
